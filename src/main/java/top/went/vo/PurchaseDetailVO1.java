package top.went.vo;

import top.went.pojo.OrderDetailEntity;
import top.went.pojo.PurDetailEntity;

import java.util.ArrayList;

public class PurchaseDetailVO1 {
    private ArrayList<PurDetailEntity> datas;
    private Long id;
    public PurchaseDetailVO1() {
    }

    public ArrayList<PurDetailEntity> getDatas() {
        return datas;
    }

    public void setDatas(ArrayList<PurDetailEntity> datas) {
        this.datas = datas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
