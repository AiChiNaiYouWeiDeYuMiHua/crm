package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import org.springframework.web.bind.annotation.RequestParam;
import top.went.pojo.PlanPayDetailEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

public class PlanPayDetailVO {
    private Integer ppdId;
    private BigDecimal ppdMoney;
    private Page page;
    @JSONField(format = "yyyy-MM-dd")
    private Date ppdDate;
    private Long ppdTerms;
    private String ppdState;
    private Long userId;
    private String userName;
    private Integer cusId;
    private Integer purId;
    private String purTheme;
    private String cusName;
    private String from;
    private String to;
    private String[] termsList;


    public PlanPayDetailVO() {

    }

    public PlanPayDetailVO(Integer purId) {
        this.purId = purId;
    }

    public PlanPayDetailVO(PlanPayDetailEntity planPayDetailEntity) {
        if (planPayDetailEntity != null) {
            this.ppdId = planPayDetailEntity.getPpdId();
            this.ppdMoney = planPayDetailEntity.getPpdMoney();
            this.ppdMoney = planPayDetailEntity.getPpdMoney();
            this.ppdTerms = planPayDetailEntity.getPpdTerms();
            this.ppdState = planPayDetailEntity.getPpdState();
            this.ppdDate = planPayDetailEntity.getPpdDate();
        }
        if (planPayDetailEntity.getTbCustomerByCusId() != null) {
            this.cusId = planPayDetailEntity.getTbCustomerByCusId().getCusId();
            this.cusName = planPayDetailEntity.getTbCustomerByCusId().getCusName();
        }
        if (planPayDetailEntity.getTbUserByUserId() != null) {
            this.userId = planPayDetailEntity.getTbUserByUserId().getUserId();
            this.userName = planPayDetailEntity.getTbUserByUserId().getUserName();
        }
        if (planPayDetailEntity.getTbPurchaseByPurId() != null) {
            this.purId = planPayDetailEntity.getTbPurchaseByPurId().getPurId();
            this.purTheme = planPayDetailEntity.getTbPurchaseByPurId().getPurTheme();
        }
    }

    public Integer getPpdId() {
        return ppdId;
    }

    public void setPpdId(Integer ppdId) {
        this.ppdId = ppdId;
    }

    public BigDecimal getPpdMoney() {
        return ppdMoney;
    }

    public void setPpdMoney(BigDecimal ppdMoney) {
        this.ppdMoney = ppdMoney;
    }

    public Page getPage() {
        if (page == null)
            page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Date getPpdDate() {
        return ppdDate;
    }

    public void setPpdDate(Date ppdDate) {
        this.ppdDate = ppdDate;
    }

    public Long getPpdTerms() {
        return ppdTerms;
    }

    public void setPpdTerms(Long ppdTerms) {
        this.ppdTerms = ppdTerms;
    }

    public String getPpdState() {
        return ppdState;
    }

    public void setPpdState(String ppdState) {
        this.ppdState = ppdState;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public Integer getPurId() {
        return purId;
    }

    public void setPurId(Integer purId) {
        this.purId = purId;
    }

    public String getPurTheme() {
        return purTheme;
    }

    public void setPurTheme(String purTheme) {
        this.purTheme = purTheme;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String[] getTermsList() {
        return termsList;
    }

    public void setTermsList(String[] termsList) {
        this.termsList = termsList;
    }

    @Override
    public String toString() {
        return "PlanPayDetailVO{" +
                "ppdId=" + ppdId +
                ", ppdMoney=" + ppdMoney +
                ", ppdDate=" + ppdDate +
                ", ppdTerms=" + ppdTerms +
                ", ppdState='" + ppdState + '\'' +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", cusId=" + cusId +
                ", purId=" + purId +
                ", purTheme='" + purTheme + '\'' +
                ", cusName='" + cusName + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", termsList=" + Arrays.toString(termsList) +
                ", page=" + page +
                '}';
    }
}
