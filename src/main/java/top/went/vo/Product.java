package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.ProductEntity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 产品信息
 */
public class Product {
    /** 产品基本信息*/
    private ProductEntity productEntity;
    private Integer id;
    private String pfName;
    private String pfCode;
    private String pfImf;
    private BigDecimal pfPrice;
    private BigDecimal pfCost;
    private String pfUnit;
    private Long pfMin;
    private Long pfMax;
    private Long number;
    private Long status;
    private String pfWeigtUnit;
    private boolean isBase;
    private String pfOther;
    private Double pfWeight;
    private Long pfUnitCal;

    private Byte pfBatch;
    @JSONField(format = "yyyy-MM-dd")
    private Date pfDate;
    @JSONField(format = "yyyy-MM-dd")
    private Date pfExpiry;
    private String pfManufacturer;
    private String pfApprovalNumber;

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPfName() {
        return pfName;
    }

    public void setPfName(String pfName) {
        this.pfName = pfName;
    }

    public String getPfCode() {
        return pfCode;
    }

    public void setPfCode(String pfCode) {
        this.pfCode = pfCode;
    }

    public String getPfImf() {
        return pfImf;
    }

    public void setPfImf(String pfImf) {
        this.pfImf = pfImf;
    }

    public BigDecimal getPfPrice() {
        return pfPrice;
    }

    public void setPfPrice(BigDecimal pfPrice) {
        this.pfPrice = pfPrice;
    }

    public BigDecimal getPfCost() {
        return pfCost;
    }

    public void setPfCost(BigDecimal pfCost) {
        this.pfCost = pfCost;
    }

    public String getPfUnit() {
        return pfUnit;
    }

    public void setPfUnit(String pfUnit) {
        this.pfUnit = pfUnit;
    }

    public Long getPfMin() {
        return pfMin;
    }

    public void setPfMin(Long pfMin) {
        this.pfMin = pfMin;
    }

    public Long getPfMax() {
        return pfMax;
    }

    public void setPfMax(Long pfMax) {
        this.pfMax = pfMax;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public boolean isBase() {
        return isBase;
    }

    public String getPfWeigtUnit() {
        return pfWeigtUnit;
    }

    public void setPfWeigtUnit(String pfWeigtUnit) {
        this.pfWeigtUnit = pfWeigtUnit;
    }

    public void setBase(boolean base) {
        isBase = base;
    }

    public String getPfOther() {
        return pfOther;
    }

    public void setPfOther(String pfOther) {
        this.pfOther = pfOther;
    }

    public Double getPfWeight() {
        return pfWeight;
    }

    public void setPfWeight(Double pfWeight) {
        this.pfWeight = pfWeight;
    }

    public Long getPfUnitCal() {
        return pfUnitCal;
    }

    public void setPfUnitCal(Long pfUnitCal) {
        this.pfUnitCal = pfUnitCal;
    }

    public Byte getPfBatch() {
        return pfBatch;
    }

    public void setPfBatch(Byte pfBatch) {
        this.pfBatch = pfBatch;
    }

    public Date getPfDate() {
        return pfDate;
    }

    public void setPfDate(Date pfDate) {
        this.pfDate = pfDate;
    }

    public Date getPfExpiry() {
        return pfExpiry;
    }

    public void setPfExpiry(Date pfExpiry) {
        this.pfExpiry = pfExpiry;
    }

    public String getPfManufacturer() {
        return pfManufacturer;
    }

    public void setPfManufacturer(String pfManufacturer) {
        this.pfManufacturer = pfManufacturer;
    }

    public String getPfApprovalNumber() {
        return pfApprovalNumber;
    }

    public void setPfApprovalNumber(String pfApprovalNumber) {
        this.pfApprovalNumber = pfApprovalNumber;
    }
}
