package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.PurchaseReceiptEntity;

import java.util.Date;

public class PurchaseReceiptVO{
    private Integer purrId;
    private String purrType;
    private String purrIsPay;
    private Long magicDelete;
    @JSONField(format = "yyyy-MM-dd")
    private Date purrDate;
    private  long purrTerms;
    private String purrTheme;
    private Long userId;
    private String userName;
    private Integer cusId;
    private String cusName;
    private Date from;
    private Date to;
    private Double purrMoney;
    private Page page;
    private Integer purId;
    private String purTheme;

    public Date getFrom() {
        return from;
    }

    public PurchaseReceiptVO() {
    }

    public PurchaseReceiptVO(Integer purId) {
        this.purId = purId;
    }

    public PurchaseReceiptVO(PurchaseReceiptEntity purchaseReceiptEntity){
        if(purchaseReceiptEntity!=null) {
            this.purrId = purchaseReceiptEntity.getPurrId();
            this.purrType = purchaseReceiptEntity.getPurrType();
            this.purrIsPay = purchaseReceiptEntity.getPurrIsPay();
            this.magicDelete = purchaseReceiptEntity.getMagicDelete();
            this.purrDate = purchaseReceiptEntity.getPurrDate();
            this.purrMoney = purchaseReceiptEntity.getPurrMoney()==null?0: purchaseReceiptEntity.getPurrMoney();
            this.purrTheme=purchaseReceiptEntity.getPurrTheme()==null?"":purchaseReceiptEntity.getPurrTheme();
            this.purrTerms=purchaseReceiptEntity.getPurrTerms()==null?0:purchaseReceiptEntity.getPurrTerms();
        }
        if(purchaseReceiptEntity.getTbPurchaseByPurId()!=null){
            this.purId=purchaseReceiptEntity.getTbPurchaseByPurId().getPurId();
            this.purTheme=purchaseReceiptEntity.getTbPurchaseByPurId().getPurTheme();
        }
        if(purchaseReceiptEntity.getTbCustomerByCusId()!=null){
            this.cusId=purchaseReceiptEntity.getTbCustomerByCusId().getCusId();
            this.cusName=purchaseReceiptEntity.getTbCustomerByCusId().getCusName();
        }
        if(purchaseReceiptEntity.getTbUserByUserId()!=null){
            this.userId=purchaseReceiptEntity.getTbUserByUserId().getUserId();
            this.userName=purchaseReceiptEntity.getTbUserByUserId().getUserName();
        }
    }
    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Integer getPurrId() {
        return purrId;
    }

    public void setPurrId(Integer purrId) {
        this.purrId = purrId;
    }

    public String getPurrType() {
        return purrType;
    }

    public void setPurrType(String purrType) {
        this.purrType = purrType;
    }

    public String getPurrIsPay() {
        return purrIsPay;
    }

    public void setPurrIsPay(String purrIsPay) {
        this.purrIsPay = purrIsPay;
    }

    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    public Date getPurrDate() {
        return purrDate;
    }

    public void setPurrDate(Date purrDate) {
        this.purrDate = purrDate;
    }

    public String getPurrTheme() {
        return purrTheme;
    }

    public void setPurrTheme(String purrTheme) {
        this.purrTheme = purrTheme;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Double getPurrMoney() {
        return purrMoney;
    }

    public void setPurrMoney(Double purrMoney) {
        this.purrMoney = purrMoney;
    }

    public Page getPage() {
        if (page == null)
            page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Integer getPurId() {
        return purId;
    }

    public void setPurId(Integer purId) {
        this.purId = purId;
    }

    public String getPurTheme() {
        return purTheme;
    }

    public void setPurTheme(String purTheme) {
        this.purTheme = purTheme;
    }

    public long getPurrTerms() {
        return purrTerms;
    }

    public void setPurrTerms(long purrTerms) {
        this.purrTerms = purrTerms;
    }

    @Override
    public String toString() {
        return "PurchaseReceiptVO{" +
                "purrId=" + purrId +
                ", purrType='" + purrType + '\'' +
                ", purrIsPay='" + purrIsPay + '\'' +
                ", magicDelete=" + magicDelete +
                ", purrDate=" + purrDate +
                ", purrTerms=" + purrTerms +
                ", purrTheme='" + purrTheme + '\'' +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", cusId=" + cusId +
                ", cusName='" + cusName + '\'' +
                ", from=" + from +
                ", to=" + to +
                ", purrMoney=" + purrMoney +
                ", page=" + page +
                ", purId=" + purId +
                ", purTheme='" + purTheme + '\'' +
                '}';
    }
}
