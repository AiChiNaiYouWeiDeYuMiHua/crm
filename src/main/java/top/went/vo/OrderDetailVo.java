package top.went.vo;


import top.went.pojo.OrderDetailEntity;

import java.util.ArrayList;

public class OrderDetailVo {
    private ArrayList<OrderDetailEntity> datas;
    private Long id;
    public OrderDetailVo() {
    }

    public ArrayList<OrderDetailEntity> getDatas() {
        return datas;
    }

    public void setDatas(ArrayList<OrderDetailEntity> datas) {
        this.datas = datas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

