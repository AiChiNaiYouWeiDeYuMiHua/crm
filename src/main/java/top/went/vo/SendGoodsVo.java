package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.AddressEntity;

import java.sql.Date;


/**
 * 发货单
 */
public class SendGoodsVo {
    private Integer dgId;
    @JSONField(format = "yyyy-MM-dd")
    private Date dgDate;
    private Long dgPkgNumber;
    private String dgModel;
    private Long dgWeight;
    private String dgLogistice;
    private String dgLogisticId;
    private Long dgFreight;
    private String dgFreightModal;
    private String dgOther;
    private AddressEntity tbAddressByAddressId;
    private Long userId;
    private String userName;
    private Integer orderId;
    private String orderTitle;
    private Integer cusId;
    private String cusName;
    private String dgNumber;
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDgNumber() {
        return dgNumber;
    }

    public void setDgNumber(String dgNumber) {
        this.dgNumber = dgNumber;
    }

    public Integer getDgId() {
        return dgId;
    }

    public void setDgId(Integer dgId) {
        this.dgId = dgId;
    }

    public Date getDgDate() {
        return dgDate;
    }

    public void setDgDate(Date dgDate) {
        this.dgDate = dgDate;
    }

    public Long getDgPkgNumber() {
        return dgPkgNumber;
    }

    public void setDgPkgNumber(Long dgPkgNumber) {
        this.dgPkgNumber = dgPkgNumber;
    }

    public String getDgModel() {
        return dgModel;
    }

    public void setDgModel(String dgModel) {
        this.dgModel = dgModel;
    }

    public Long getDgWeight() {
        return dgWeight;
    }

    public void setDgWeight(Long dgWeight) {
        this.dgWeight = dgWeight;
    }

    public String getDgLogistice() {
        return dgLogistice;
    }

    public void setDgLogistice(String dgLogistice) {
        this.dgLogistice = dgLogistice;
    }

    public String getDgLogisticId() {
        return dgLogisticId;
    }

    public void setDgLogisticId(String dgLogisticId) {
        this.dgLogisticId = dgLogisticId;
    }

    public Long getDgFreight() {
        return dgFreight;
    }

    public void setDgFreight(Long dgFreight) {
        this.dgFreight = dgFreight;
    }

    public String getDgFreightModal() {
        return dgFreightModal;
    }

    public void setDgFreightModal(String dgFreightModal) {
        this.dgFreightModal = dgFreightModal;
    }

    public String getDgOther() {
        return dgOther;
    }

    public void setDgOther(String dgOther) {
        this.dgOther = dgOther;
    }

    public AddressEntity getTbAddressByAddressId() {
        return tbAddressByAddressId;
    }

    public void setTbAddressByAddressId(AddressEntity tbAddressByAddressId) {
        this.tbAddressByAddressId = tbAddressByAddressId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }
}
