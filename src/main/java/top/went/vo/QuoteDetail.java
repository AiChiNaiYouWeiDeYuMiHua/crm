package top.went.vo;

import top.went.pojo.QuoteDetailEntity;

public class QuoteDetail extends QuoteDetailEntity {

    private String productName;
    private String productModal;
    private String pfName;
    private String pfUnit;

    private Long pdId;

    private Long qId;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPdId() {
        return pdId;
    }

    public void setPdId(Long pdId) {
        this.pdId = pdId;
    }

    public Long getqId() {
        return qId;
    }

    public void setqId(Long qId) {
        this.qId = qId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductModal() {
        return productModal;
    }

    public void setProductModal(String productModal) {
        this.productModal = productModal;
    }

    public String getPfName() {
        return pfName;
    }

    public void setPfName(String pfName) {
        this.pfName = pfName;
    }

    public String getPfUnit() {
        return pfUnit;
    }

    public void setPfUnit(String pfUnit) {
        this.pfUnit = pfUnit;
    }
}
