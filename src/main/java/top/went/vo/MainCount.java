package top.went.vo;

import java.util.Objects;

public class MainCount {
    private long cus;
    private long opp;
    private long quote;
    private long order;
    private long pur;
    private long ship;
    private long sale;

    public long getCus() {
        return cus;
    }

    public void setCus(long cus) {
        this.cus = cus;
    }

    public long getOpp() {
        return opp;
    }

    public void setOpp(long opp) {
        this.opp = opp;
    }

    public long getQuote() {
        return quote;
    }

    public void setQuote(long quote) {
        this.quote = quote;
    }

    public long getOrder() {
        return order;
    }

    public void setOrder(long order) {
        this.order = order;
    }

    public long getPur() {
        return pur;
    }

    public void setPur(long pur) {
        this.pur = pur;
    }

    public long getShip() {
        return ship;
    }

    public void setShip(long ship) {
        this.ship = ship;
    }

    public long getSale() {
        return sale;
    }

    public void setSale(long sale) {
        this.sale = sale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MainCount mainCount = (MainCount) o;
        return cus == mainCount.cus &&
                opp == mainCount.opp &&
                quote == mainCount.quote &&
                order == mainCount.order &&
                pur == mainCount.pur &&
                ship == mainCount.ship &&
                sale == mainCount.sale;
    }

    @Override
    public int hashCode() {

        return Objects.hash(cus, opp, quote, order, pur, ship, sale);
    }

    @Override
    public String toString() {
        return "MainCount{" +
                "cus=" + cus +
                ", opp=" + opp +
                ", quote=" + quote +
                ", order=" + order +
                ", pur=" + pur +
                ", ship=" + ship +
                ", sale=" + sale +
                '}';
    }
}
