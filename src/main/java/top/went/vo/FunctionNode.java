package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.FcTypeEntity;
import top.went.pojo.FunctionsEntity;

import java.util.List;
import java.util.Objects;

public class FunctionNode extends FcTypeEntity {

    @JSONField(name = "nodes")
    private List<FunctionsEntity> fcs;

    public List<FunctionsEntity> getFcs() {
        return fcs;
    }

    public void setFcs(List<FunctionsEntity> fcs) {
        this.fcs = fcs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FunctionNode that = (FunctionNode) o;
        return Objects.equals(fcs, that.fcs);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), fcs);
    }

    @Override
    public String toString() {
        return "FunctionNode{" +
                "fcs=" + fcs +
                '}';
    }
}
