package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.ReturnGoodsDetailEntity;
import top.went.pojo.ReturnGoodsEntity;
import top.went.utils.StringUtils;

import java.math.BigDecimal;
import java.sql.Date;

public class ReturnGoodsDetailVo {
    private String title;
    private String pfName;
    private String productName;
    private String productModalName;
    private BigDecimal pfPrice;
    private String unit;
    private Integer rgdId;
    private String rgdReason;
    private Long rgdNum;
    private String rgdRemarks;
    private Integer rgdOutwhNum;
    private Double rgdMoney;
    private Long magicDelete;
    private Integer rgId;
    private Integer purId;
    private String rgTheme;
    private Integer pdId;
    private String pdTitle;
    private Long pdNum;
    @JSONField(format = "yyyy-MM-dd")
    private Date rgReturnTime;
    private BigDecimal pdPrice;
    private BigDecimal pdMoney;
    private BigDecimal pdTotal;
    private Integer pfId;
    private Integer productId;
    private String from;
    private String to;
    private Page page;
    public ReturnGoodsDetailVo() {
    }
    public ReturnGoodsDetailVo(ReturnGoodsDetailEntity returnGoodsDetailEntity) {
        if (returnGoodsDetailEntity != null) {
            this.rgdId = returnGoodsDetailEntity.getRgdId();
            this.rgdReason = returnGoodsDetailEntity.getRgdReason();
            this.rgdNum = returnGoodsDetailEntity.getRgdNum();
            this.rgdRemarks = returnGoodsDetailEntity.getRgdRemarks();
            this.magicDelete = returnGoodsDetailEntity.getMagicDelete();
            this.rgdMoney = returnGoodsDetailEntity.getRgdMoney();
            this.rgdOutwhNum = returnGoodsDetailEntity.getRgdOutwhNum();
        }
        if (returnGoodsDetailEntity.getTbReturnGoodsByRgId() != null) {
            this.rgId = returnGoodsDetailEntity.getTbReturnGoodsByRgId().getRgId();
            this.rgTheme = returnGoodsDetailEntity.getTbReturnGoodsByRgId().getRgTheme();
            this.rgReturnTime = returnGoodsDetailEntity.getTbReturnGoodsByRgId().getRgReturnTime();
        }
        if (returnGoodsDetailEntity.getPurDetailEntity() != null) {
            this.pdId = returnGoodsDetailEntity.getPurDetailEntity().getPdId();
            this.pdTitle = returnGoodsDetailEntity.getPurDetailEntity().getPdTitle();
            this.pdNum = returnGoodsDetailEntity.getPurDetailEntity().getPdNum();
            this.pdPrice = returnGoodsDetailEntity.getPurDetailEntity().getPdPrice();
            this.pdMoney = returnGoodsDetailEntity.getPurDetailEntity().getPdMoney();
            this.pdTotal = returnGoodsDetailEntity.getPurDetailEntity().getPdTotal();
            this.purId=returnGoodsDetailEntity.getPurDetailEntity().getTbPurchaseByPurId().getPurId();
        }
        if(returnGoodsDetailEntity.getTbProductFormatByPfId()!=null){
            this.pfId=returnGoodsDetailEntity.getTbProductFormatByPfId().getPfId();
            this.pfName = returnGoodsDetailEntity.getTbProductFormatByPfId().getPfName();
            this.unit = returnGoodsDetailEntity.getTbProductFormatByPfId().getPfUnit();
        }
        if(returnGoodsDetailEntity.getTbProductByProductId()!=null){
            this.productName=returnGoodsDetailEntity.getTbProductByProductId().getProductName();
            this.productModalName=returnGoodsDetailEntity.getTbProductByProductId().getProductModel();
        }
        this.title = StringUtils.getStringBuffer(returnGoodsDetailEntity.getTbProductFormatByPfId()).toString();
    }

    public Integer getRgdId() {
        return rgdId;
    }

    public void setRgdId(Integer rgdId) {
        this.rgdId = rgdId;
    }

    public String getRgdReason() {
        return rgdReason;
    }

    public void setRgdReason(String rgdReason) {
        this.rgdReason = rgdReason;
    }

    public Long getRgdNum() {
        return rgdNum;
    }

    public void setRgdNum(Long rgdNum) {
        this.rgdNum = rgdNum;
    }

    public String getRgdRemarks() {
        return rgdRemarks;
    }

    public void setRgdRemarks(String rgdRemarks) {
        this.rgdRemarks = rgdRemarks;
    }

    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    public Integer getRgId() {
        return rgId;
    }

    public void setRgId(Integer rgId) {
        this.rgId = rgId;
    }

    public String getRgTheme() {
        return rgTheme;
    }

    public void setRgTheme(String rgTheme) {
        this.rgTheme = rgTheme;
    }

    public Integer getPdId() {
        return pdId;
    }

    public void setPdId(Integer pdId) {
        this.pdId = pdId;
    }

    public String getPdTitle() {
        return pdTitle;
    }

    public void setPdTitle(String pdTitle) {
        this.pdTitle = pdTitle;
    }

    public Long getPdNum() {
        return pdNum;
    }

    public void setPdNum(Long pdNum) {
        this.pdNum = pdNum;
    }

    public BigDecimal getPdPrice() {
        return pdPrice;
    }

    public void setPdPrice(BigDecimal pdPrice) {
        this.pdPrice = pdPrice;
    }

    public BigDecimal getPdMoney() {
        return pdMoney;
    }

    public void setPdMoney(BigDecimal pdMoney) {
        this.pdMoney = pdMoney;
    }

    public BigDecimal getPdTotal() {
        return pdTotal;
    }

    public void setPdTotal(BigDecimal pdTotal) {
        this.pdTotal = pdTotal;
    }

    public Page getPage() {
        if (page == null)
            page = new Page();
        return page;
    }

    public BigDecimal getPfPrice() {
        return pfPrice;
    }

    public void setPfPrice(BigDecimal pfPrice) {
        this.pfPrice = pfPrice;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Integer getRgdOutwhNum() {
        return rgdOutwhNum;
    }

    public void setRgdOutwhNum(Integer rgdOutwhNum) {
        this.rgdOutwhNum = rgdOutwhNum;
    }

    public Double getRgdMoney() {
        return rgdMoney;
    }

    public void setRgdMoney(Double rgdMoney) {
        this.rgdMoney = rgdMoney;
    }

    public Date getRgReturnTime() {
        return rgReturnTime;
    }

    public void setRgReturnTime(Date rgReturnTime) {
        this.rgReturnTime = rgReturnTime;
    }


    public Integer getPfId() {
        return pfId;
    }

    public void setPfId(Integer pfId) {
        this.pfId = pfId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getFrom1() {
        return from;
    }

    public void setFrom1(String from) {
        this.from = from;
    }

    public String getTo1() {
        return to;
    }

    public void setTo1(String to) {
        this.to = to;
    }

    public Integer getPurId() {
        return purId;
    }

    public void setPurId(Integer purId) {
        this.purId = purId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        if (this.title == null) {
            StringBuffer sb = StringUtils.getStringBuffer(productName, productModalName, pfName, unit);
            this.title = sb.toString();
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPfName() {
        return pfName;
    }

    public void setPfName(String pfName) {
        this.pfName = pfName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductModalName() {
        return productModalName;
    }

    public void setProductModalName(String productModalName) {
        this.productModalName = productModalName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
