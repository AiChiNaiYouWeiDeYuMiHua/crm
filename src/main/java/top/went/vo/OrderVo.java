package top.went.vo;

import top.went.pojo.OrderEntity;

import java.math.BigDecimal;

/**
 * 订单/合同
 */
public class OrderVo extends OrderEntity {
    /** 发货金额*/
    private BigDecimal sendMoney;
    /** 回款金额*/
    private BigDecimal returnMoney;
    /** 毛利*/
    private BigDecimal maori = BigDecimal.ZERO;
    /** 开发票金额*/
    private BigDecimal billMOney;
    /** 预计毛利*/
    private BigDecimal forecastMaori;
    /** 发货*/
    private String sendGoods;
    /** 退货*/
    private String returnGoods;
    /** 总金额*/
    private BigDecimal total;

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getSendMoney() {
        return sendMoney;
    }

    public void setSendMoney(BigDecimal sendMoney) {
        this.sendMoney = sendMoney;
    }

    public BigDecimal getReturnMoney() {
        return returnMoney;
    }

    public void setReturnMoney(BigDecimal returnMoney) {
        this.returnMoney = returnMoney;
    }

    public BigDecimal getMaori() {
        return maori;
    }

    public void setMaori(BigDecimal maori) {
        this.maori = maori;
    }

    public BigDecimal getBillMOney() {
        return billMOney;
    }

    public void setBillMOney(BigDecimal billMOney) {
        this.billMOney = billMOney;
    }

    public BigDecimal getForecastMaori() {
        return forecastMaori;
    }

    public void setForecastMaori(BigDecimal forecastMaori) {
        this.forecastMaori = forecastMaori;
    }

    public String getSendGoods() {
        return sendGoods;
    }

    public void setSendGoods(String sendGoods) {
        this.sendGoods = sendGoods;
    }

    public String getReturnGoods() {
        if (this.returnGoods == null)
            this.returnGoods = "无退货";
        return returnGoods;
    }

    public void setReturnGoods(String returnGoods) {
        this.returnGoods = returnGoods;
    }

    @Override
    public String toString() {
        return super.toString()+"OrderVo{" +
                "sendMoney=" + sendMoney +
                ", returnMoney=" + returnMoney +
                ", maori=" + maori +
                ", billMOney=" + billMOney +
                ", forecastMaori=" + forecastMaori +
                ", sendGoods=" + sendGoods +
                ", returnGoods=" + returnGoods +
                '}';
    }
}
