package top.went.vo;

import top.went.pojo.DemandEntity;

import java.sql.Date;

public class DemandSonVo {
    private DemandEntity demandEntity;
    private Date recordTimeFrom;
    private Date recordTimeTo;

    private String familyDa;
    private String kidDa;

    private String searchText;
    private Long searchTextType;

    private Page page;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getFamilyDa() {
        return familyDa;
    }

    public void setFamilyDa(String familyDa) {
        this.familyDa = familyDa;
    }

    public String getKidDa() {
        return kidDa;
    }

    public void setKidDa(String kidDa) {
        this.kidDa = kidDa;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Long getSearchTextType() {
        return searchTextType;
    }

    public void setSearchTextType(Long searchTextType) {
        this.searchTextType = searchTextType;
    }

    public DemandEntity getDemandEntity() {
        return demandEntity;
    }

    public void setDemandEntity(DemandEntity demandEntity) {
        this.demandEntity = demandEntity;
    }

    public Date getRecordTimeFrom() {
        return recordTimeFrom;
    }

    public void setRecordTimeFrom(Date recordTimeFrom) {
        this.recordTimeFrom = recordTimeFrom;
    }

    public Date getRecordTimeTo() {
        return recordTimeTo;
    }

    public void setRecordTimeTo(Date recordTimeTo) {
        this.recordTimeTo = recordTimeTo;
    }
}
