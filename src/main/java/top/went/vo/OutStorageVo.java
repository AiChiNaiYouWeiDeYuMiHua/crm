package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.*;

import java.sql.Date;
import java.sql.Time;

/**
 * 出库
 */
public class OutStorageVo {
    private Integer wpullId;
    private String wpullName;
    @JSONField(format = "yyyy-MM-dd")
    private Date wpullDate;
    private Integer wpullStatus;
    @JSONField(format = "yyyy-MM-dd")
    private Date wpullExecute;
    private String wpullOther;
    private Integer orderId;
    private String orderName;
    private Long userId;
    private String userName;
    private Long executeId;
    private String executName;
    private Long wareHouseId;
    private String wareHouseName;
    private Integer rgId;
    private String rgName;
    private Integer cusId;
    private String cusName;


    public Integer getWpullId() {
        return wpullId;
    }

    public void setWpullId(Integer wpullId) {
        this.wpullId = wpullId;
    }

    public String getWpullName() {
        return wpullName;
    }

    public void setWpullName(String wpullName) {
        this.wpullName = wpullName;
    }

    public Date getWpullDate() {
        return wpullDate;
    }

    public void setWpullDate(Date wpullDate) {
        this.wpullDate = wpullDate;
    }

    public Integer getWpullStatus() {
        return wpullStatus;
    }

    public void setWpullStatus(Integer wpullStatus) {
        this.wpullStatus = wpullStatus;
    }

    public Date getWpullExecute() {
        return wpullExecute;
    }

    public void setWpullExecute(Date wpullExecute) {
        this.wpullExecute = wpullExecute;
    }

    public String getWpullOther() {
        return wpullOther;
    }

    public void setWpullOther(String wpullOther) {
        this.wpullOther = wpullOther;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getExecuteId() {
        return executeId;
    }

    public void setExecuteId(Long executeId) {
        this.executeId = executeId;
    }

    public String getExecutName() {
        return executName;
    }

    public void setExecutName(String executName) {
        this.executName = executName;
    }

    public Long getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(Long wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public Integer getRgId() {
        return rgId;
    }

    public void setRgId(Integer rgId) {
        this.rgId = rgId;
    }

    public String getRgName() {
        return rgName;
    }

    public void setRgName(String rgName) {
        this.rgName = rgName;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }
}
