package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.ReturnGoodsEntity;

import java.sql.Time;
import java.util.Date;

public class ReturnGoodsVO {
    private Integer rgId;
    private String rgTheme;
    private String rgOddNumbers;
    @JSONField(format = "yyyy-MM-dd")
    private Date rgReturnTime;
    private String rgState;
    private String rgAccessories;
    private Long magicDelete;
    private Double planMoney;
    private Double toMoney;
    private Long rgType;
    private Date from;
    private Date to;
    private Page page;
    private Integer orderId;
    private String orderTitle;
    private Integer cusId;
    private String cusName;
    private Integer userId;
    private String userName;
    private Integer purId;
    private String purTheme;
    private String putState;
    private String payBackState;
    private String stateList[];
    private String whName;

    public ReturnGoodsVO() {
    }

    public ReturnGoodsVO(Integer purId) {
        this.purId = purId;
    }
    public ReturnGoodsVO(Long orderId) {
        this.orderId = orderId.intValue();
    }

    public ReturnGoodsVO(ReturnGoodsEntity returnGoodsEntity) {
        if(returnGoodsEntity.getTbOrderByOrderId()!=null){
            this.orderId=returnGoodsEntity.getTbOrderByOrderId().getOrderId();
            this.orderTitle=returnGoodsEntity.getTbOrderByOrderId().getOrderTitle();
           if(returnGoodsEntity.getTbOrderByOrderId().getTbCustomerByCusId()!=null){
               this.cusId=returnGoodsEntity.getTbOrderByOrderId().getTbCustomerByCusId().getCusId();
               this.cusName=returnGoodsEntity.getTbOrderByOrderId().getTbCustomerByCusId().getCusName();
           }
        }
        if(returnGoodsEntity.getTbPurchaseByPurId()!=null){
            this.purId=returnGoodsEntity.getTbPurchaseByPurId().getPurId();
            this.purTheme=returnGoodsEntity.getTbPurchaseByPurId().getPurTheme();
            if(returnGoodsEntity.getTbPurchaseByPurId().getTbCustomerByCusId()!=null){
                this.cusId=returnGoodsEntity.getTbPurchaseByPurId().getTbCustomerByCusId().getCusId();
                this.cusName=returnGoodsEntity.getTbPurchaseByPurId().getTbCustomerByCusId().getCusName();
            }
            if(returnGoodsEntity.getTbPurchaseByPurId().getTbWarehouseByWhId()!=null){
                this.whName=returnGoodsEntity.getTbPurchaseByPurId().getTbWarehouseByWhId().getWhName();
            }
        }
        if(returnGoodsEntity.getTbUserByUserId()!=null){
            this.userId=Math.toIntExact(returnGoodsEntity.getTbUserByUserId().getUserId());
        }
        if(returnGoodsEntity!=null){
            this.rgId=returnGoodsEntity.getRgId();
            this.rgType=returnGoodsEntity.getRgType();
            this.rgOddNumbers=returnGoodsEntity.getRgOddNumbers();
            this.rgReturnTime=returnGoodsEntity.getRgReturnTime();
            this.rgState=returnGoodsEntity.getRgState();
            this.rgAccessories=returnGoodsEntity.getRgAccessories();
            this.magicDelete=returnGoodsEntity.getMagicDelete();
            this.rgTheme=returnGoodsEntity.getRgTheme();
            this.putState=returnGoodsEntity.getPutState();
            this.payBackState=returnGoodsEntity.getPayBackState();
            this.planMoney=returnGoodsEntity.getPlanMoney();
            this.toMoney=returnGoodsEntity.getToMoney();
        }
    }
    public Integer getRgId() {
        return rgId;
    }

    public void setRgId(Integer rgId) {
        this.rgId = rgId;
    }

    public String getRgOddNumbers() {
        return rgOddNumbers;
    }

    public void setRgOddNumbers(String rgOddNumbers) {
        this.rgOddNumbers = rgOddNumbers;
    }

    public Date getRgReturnTime() {
        return rgReturnTime;
    }

    public void setRgReturnTime(Time rgReturnTime) {
        this.rgReturnTime = rgReturnTime;
    }

    public String getRgState() {
        return rgState;
    }

    public void setRgState(String rgState) {
        this.rgState = rgState;
    }

    public String getRgAccessories() {
        return rgAccessories;
    }

    public void setRgAccessories(String rgAccessories) {
        this.rgAccessories = rgAccessories;
    }

    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    public Long getRgType() {
        return rgType;
    }

    public void setRgType(Long rgType) {
        this.rgType = rgType;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getRgTheme() {
        return rgTheme;
    }

    public void setRgTheme(String rgTheme) {
        this.rgTheme = rgTheme;
    }

    public Page getPage() {
        if (page == null)
            page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getPurId() {
        return purId;
    }

    public void setPurId(Integer purId) {
        this.purId = purId;
    }

    public String getPurTheme() {
        return purTheme;
    }

    public void setPurTheme(String purTheme) {
        this.purTheme = purTheme;
    }

    public Double getPlanMoney() {
        return planMoney;
    }

    public void setPlanMoney(Double planMoney) {
        this.planMoney = planMoney;
    }

    public Double getToMoney() {
        return toMoney;
    }

    public void setToMoney(Double toMoney) {
        this.toMoney = toMoney;
    }

    public void setRgReturnTime(Date rgReturnTime) {
        this.rgReturnTime = rgReturnTime;
    }

    public String[] getStateList() {
        return stateList;
    }

    public void setStateList(String[] stateList) {
        this.stateList = stateList;
    }

    public String getPutState() {
        return putState;
    }

    public void setPutState(String putState) {
        this.putState = putState;
    }

    public String getPayBackState() {
        return payBackState;
    }

    public void setPayBackState(String payBackState) {
        this.payBackState = payBackState;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }
}
