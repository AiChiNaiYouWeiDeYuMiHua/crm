package top.went.vo;

import top.went.pojo.ProductFormatEntity;
import top.went.utils.StringUtils;

/**
 * 出入库明细
 */
public class DetailOutVo {
    private Long wdId;
    private ProductFormatEntity tbProductFormatByPfId;
    private Long productId;
    private String productTitle;
    private Long planNumber;
    private Long inNumber;
    private Long outNumber;
    private Long whNumber;
    private Long whAllNumber;
    private String wdOther;
    private Boolean wdState;
    private Long whId;

    public Long getWdId() {
        return wdId;
    }

    public void setWdId(Long wdId) {
        this.wdId = wdId;
    }

    public ProductFormatEntity getTbProductFormatByPfId() {
        return tbProductFormatByPfId;
    }

    public void setTbProductFormatByPfId(ProductFormatEntity tbProductFormatByPfId) {
        this.tbProductFormatByPfId = tbProductFormatByPfId;
    }

    public Long getProductId() {
        if (getTbProductFormatByPfId() != null)
            this.productId = getTbProductFormatByPfId().getPfId().longValue();
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductTitle() {
        if (getTbProductFormatByPfId() != null)
            this.productTitle = StringUtils.getStringBuffer(getTbProductFormatByPfId()).toString();
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public Long getPlanNumber() {
        return planNumber;
    }

    public void setPlanNumber(Long planNumber) {
        this.planNumber = planNumber;
    }

    public Long getInNumber() {
        return inNumber;
    }

    public void setInNumber(Long inNumber) {
        this.inNumber = inNumber;
    }

    public Long getOutNumber() {
        return outNumber;
    }

    public void setOutNumber(Long outNumber) {
        this.outNumber = outNumber;
    }

    public Long getWhNumber() {
        return whNumber;
    }

    public void setWhNumber(Long whNumber) {
        this.whNumber = whNumber;
    }

    public Long getWhAllNumber() {
        return whAllNumber;
    }

    public void setWhAllNumber(Long whAllNumber) {
        this.whAllNumber = whAllNumber;
    }

    public String getWdOther() {
        return wdOther;
    }

    public void setWdOther(String wdOther) {
        this.wdOther = wdOther;
    }

    public Boolean getWdState() {
        return wdState;
    }

    public void setWdState(Boolean wdState) {
        this.wdState = wdState;
    }

    public Long getWhId() {
        return whId;
    }

    public void setWhId(Long whId) {
        this.whId = whId;
    }
}
