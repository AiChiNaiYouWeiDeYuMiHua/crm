package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.PlanPayBackEntity;
import top.went.pojo.PlanPayDetailEntity;

import java.math.BigDecimal;
import java.util.Date;

public class PlanPayBackVO  {
    private Integer ppbId;
    private BigDecimal ppbMoney;
    @JSONField(format = "yyyy-MM-dd")
    private Date ppbDate;
    private Long ppbTerms;
    private Long magicDelete;
    private Long ppbType;
    private Date from;
    private Date to;
    private Long userId;
    private String userName;
    private Integer cusId;
    private String cusName;
    private Integer orderId;
    private String orderTitle;
    private Page page;
    private String termsList[];
    public PlanPayBackVO() {
    }

    public PlanPayBackVO(Integer orderId) {
        this.orderId = orderId;
    }

    public PlanPayBackVO(PlanPayBackEntity payBackEntity) {
        if(payBackEntity.getPpbId()!=null){
            this.ppbId=payBackEntity.getPpbId();
        }
        if(payBackEntity.getPpbMoney()!=null){
            this.ppbMoney=payBackEntity.getPpbMoney();
        }
        if(payBackEntity.getPpbDate()!=null){
            this.ppbDate=payBackEntity.getPpbDate();
        }
        if(payBackEntity.getPpbTerms()!=null){
            this.ppbTerms=payBackEntity.getPpbTerms();
        }
        if(payBackEntity.getPpbType()!=null){
            this.ppbType=payBackEntity.getPpbType();
        }
        if(payBackEntity.getMagicDelete()!=null){
            this.magicDelete=payBackEntity.getMagicDelete();
        }
        if (payBackEntity.getTbUserByUserId() != null) {
            this.userId = payBackEntity.getTbUserByUserId().getUserId();
            this.userName = payBackEntity.getTbUserByUserId().getUserName();
        }
        if(payBackEntity.getTbCustomerByCusId() !=null){
            this.cusId = payBackEntity.getTbCustomerByCusId().getCusId();
            this.cusName=payBackEntity.getTbCustomerByCusId().getCusName();
        }
       if(payBackEntity.getTbOrderByOrderId() !=null){
           this.orderId=payBackEntity.getTbOrderByOrderId().getOrderId();
           this.orderTitle=payBackEntity.getTbOrderByOrderId().getOrderTitle();
       }
    }

    public Date getFrom() {
        return from;
    }

    public Integer getPpbId() {
        return ppbId;
    }

    public void setPpbId(Integer ppbId) {
        this.ppbId = ppbId;
    }

    public BigDecimal getPpbMoney() {
        return ppbMoney;
    }

    public void setPpbMoney(BigDecimal ppbMoney) {
        this.ppbMoney = ppbMoney;
    }

    public Date getPpbDate() {
        return ppbDate;
    }

    public void setPpbDate(Date ppbDate) {
        this.ppbDate = ppbDate;
    }

    public Long getPpbTerms() {
        return ppbTerms;
    }

    public void setPpbTerms(Long ppbTerms) {
        this.ppbTerms = ppbTerms;
    }

    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    public Long getPpbType() {
        return ppbType;
    }

    public void setPpbType(Long ppbType) {
        this.ppbType = ppbType;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Page getPage() {
        if (this.page == null)
            this.page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String[] getTermsList() {
        return termsList;
    }

    public void setTermsList(String[] termsList) {
        this.termsList = termsList;
    }
}
