package top.went.vo;

import top.went.pojo.QuoteDetailEntity;

import java.util.ArrayList;

public class QuoteDetailEditVo {
    private ArrayList<QuoteDetailEntity> datas;
    private Long id;

    public ArrayList<QuoteDetailEntity> getDatas() {
        return datas;
    }

    public void setDatas(ArrayList<QuoteDetailEntity> datas) {
        this.datas = datas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
