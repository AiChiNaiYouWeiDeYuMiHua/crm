package top.went.vo;

import top.went.pojo.CompetitorEntity;

public class CompetitorSonVo {
    private CompetitorEntity competitorEntity;

    private String searchText;
    private Page page;

    private String familyDa;
    private String kidDa;

    public String getFamilyDa() {
        return familyDa;
    }

    public void setFamilyDa(String familyDa) {
        this.familyDa = familyDa;
    }

    public String getKidDa() {
        return kidDa;
    }

    public void setKidDa(String kidDa) {
        this.kidDa = kidDa;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public CompetitorEntity getCompetitorEntity() {
        return competitorEntity;
    }

    public void setCompetitorEntity(CompetitorEntity competitorEntity) {
        this.competitorEntity = competitorEntity;
    }

}
