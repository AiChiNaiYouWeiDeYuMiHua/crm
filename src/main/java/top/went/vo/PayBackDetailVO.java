package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.PayBackDetailEntity;
import top.went.pojo.PlanPayBackEntity;

import java.sql.Time;
import java.util.Date;

public class PayBackDetailVO {
    private Integer pbdId;
    private Double pbdMoney;
    private Long pbdTerms;
    @JSONField(format = "yyyy-MM-dd")
    private Date pbdDate;
    private Long magicDelete;
    private Boolean pbdType;
    private Integer orderId;
    private String orderTitle;
    private Integer mtId;
    private String mtTheme;
    private Long userId;
    private String userName;
    private Integer ppbId;
    private Integer cusId;
    private String cusName;
    private Page page;
    private String termsList[];
    private Date from;
    private Date to;
    private Integer rgId;

    public PayBackDetailVO() {
    }

    public PayBackDetailVO(Integer orderId) {
        this.orderId = orderId;
    }

    public PayBackDetailVO(PayBackDetailEntity payBackDetailEntity) {
        if(payBackDetailEntity!=null) {
            this.pbdId = payBackDetailEntity.getPbdId();
            this.pbdMoney = payBackDetailEntity.getPbdMoney();
            this.pbdDate = payBackDetailEntity.getPbdDate();
            this.pbdTerms = payBackDetailEntity.getPbdTerms();
            this.pbdType = payBackDetailEntity.getPbdType();
            this.magicDelete = payBackDetailEntity.getMagicDelete();
        }
        if (payBackDetailEntity.getTbUserByUserId() != null) {
            this.userId = payBackDetailEntity.getTbUserByUserId().getUserId();
            this.userName = payBackDetailEntity.getTbUserByUserId().getUserName();
        }
        if(payBackDetailEntity.getTbMaintainOrderByMtId() !=null){
            this.mtId = payBackDetailEntity.getTbMaintainOrderByMtId().getMtId();
            this.mtTheme=payBackDetailEntity.getTbMaintainOrderByMtId().getMtTheme();
        }
        if(payBackDetailEntity.getTbOrderByOrderId() !=null){
            this.orderId=payBackDetailEntity.getTbOrderByOrderId().getOrderId();
            this.orderTitle=payBackDetailEntity.getTbOrderByOrderId().getOrderTitle();
        }
        if(payBackDetailEntity.getTbPlanPayBackByPpbId()!=null){
            this.ppbId=payBackDetailEntity.getTbPlanPayBackByPpbId().getPpbId();
        }
        if(payBackDetailEntity.getTbCustomerByCusId()!=null){
            this.cusId=payBackDetailEntity.getTbCustomerByCusId().getCusId();
            this.cusName=payBackDetailEntity.getTbCustomerByCusId().getCusName();
        }
        if(payBackDetailEntity.getReturnGoodsEntity()!=null){
            this.rgId=payBackDetailEntity.getReturnGoodsEntity().getRgId();
        }
    }

    public Integer getPbdId() {
        return pbdId;
    }

    public void setPbdId(Integer pbdId) {
        this.pbdId = pbdId;
    }

    public Double getPbdMoney() {
        return pbdMoney;
    }

    public void setPbdMoney(Double pbdMoney) {
        this.pbdMoney = pbdMoney;
    }

    public Long getPbdTerms() {
        return pbdTerms;
    }

    public void setPbdTerms(Long pbdTerms) {
        this.pbdTerms = pbdTerms;
    }

    public Date getPbdDate() {
        return pbdDate;
    }

    public void setPbdDate(Date pbdDate) {
        this.pbdDate = pbdDate;
    }

    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    public Boolean getPbdType() {
        return pbdType;
    }

    public void setPbdType(Boolean pbdType) {
        this.pbdType = pbdType;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public Integer getMtId() {
        return mtId;
    }

    public void setMtId(Integer mtId) {
        this.mtId = mtId;
    }

    public String getMtTheme() {
        return mtTheme;
    }

    public void setMtTheme(String mtTheme) {
        this.mtTheme = mtTheme;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getPpbId() {
        return ppbId;
    }

    public void setPpbId(Integer ppbId) {
        this.ppbId = ppbId;
    }

    public Page getPage() {
        if (this.page == null)
            this.page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String[] getTermsList() {
        return termsList;
    }

    public void setTermsList(String[] termsList) {
        this.termsList = termsList;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Integer getRgId() {
        return rgId;
    }

    public void setRgId(Integer rgId) {
        this.rgId = rgId;
    }
}
