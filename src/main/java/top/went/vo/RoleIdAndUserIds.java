package top.went.vo;

import java.util.List;
import java.util.Objects;

public class RoleIdAndUserIds {
    private Long roleId;
    private List<Long> userIds;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleIdAndUserIds that = (RoleIdAndUserIds) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(userIds, that.userIds);
    }

    @Override
    public int hashCode() {

        return Objects.hash(roleId, userIds);
    }

    @Override
    public String toString() {
        return "RoleIdAndUserIds{" +
                "roleId=" + roleId +
                ", userIds=" + userIds +
                '}';
    }
}
