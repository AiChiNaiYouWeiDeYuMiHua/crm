package top.went.vo;

import top.went.pojo.SolutionEntity;

import java.sql.Date;

/**
 * 解决方案Vo
 */
public class SolutionVo {
    private SolutionEntity solutionEntity;
    private Date submitTimeFrom;
    private Date submitTimeTo;

    private String searchText;
    private Page page;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public SolutionEntity getSolutionEntity() {
        return solutionEntity;
    }

    public void setSolutionEntity(SolutionEntity solutionEntity) {
        this.solutionEntity = solutionEntity;
    }

    public Date getSubmitTimeFrom() {
        return submitTimeFrom;
    }

    public void setSubmitTimeFrom(Date submitTimeFrom) {
        this.submitTimeFrom = submitTimeFrom;
    }

    public Date getSubmitTimeTo() {
        return submitTimeTo;
    }

    public void setSubmitTimeTo(Date submitTimeTo) {
        this.submitTimeTo = submitTimeTo;
    }
}
