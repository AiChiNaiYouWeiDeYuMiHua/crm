package top.went.vo;

import top.went.pojo.ProductCategoryEntity;
import top.went.pojo.ProductEntity;
import top.went.pojo.ProductFormatEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 产品类别树
 */
public class ProductTree{
    private String text;
    private List<ProductTree> nodes;
    private Long id;
    private Boolean selectable = true;

    public ProductTree() {
    }

    public ProductTree(String text, Long id) {
        this.text = text;
        this.id = id;
    }

    public ProductTree(String text, Long id, Boolean selectable) {
        this.text = text;
        this.id = id;
        this.selectable = selectable;
    }

    public ProductTree(String text, List<ProductFormatEntity> nodes, Long id) {
        this.text = text;
        this.nodes = new ArrayList<>();
        if (nodes!=null)
            for (ProductFormatEntity formatEntity : nodes) {
                if (!formatEntity.getLogicDelete() && !formatEntity.getPfStatus())
                    this.nodes.add(new ProductTree(formatter(formatEntity), null,false));
            }
        if (this.nodes.size() <=0)
            this.nodes = null;
        this.id = id;
    }

    public ProductTree(ProductCategoryEntity categoryEntity) {
        this.id = categoryEntity.getPcId();
        this.text = categoryEntity.getPcName();
        this.nodes = new ArrayList<>();
        for (ProductCategoryEntity c : categoryEntity.getTbProductCategoriesByPcId()){
            if (!c.getLogicDelete())
                this.nodes.add(new ProductTree(c));
        }

        if (categoryEntity.getTbProductsByPcId() != null) {
            for (ProductEntity c : categoryEntity.getTbProductsByPcId()) {
                if (!c.getLogicDelete() && c.getTbProductFormatsByProductId() != null && c.getTbProductFormatsByProductId().size() > 0) {
                    for (ProductFormatEntity formatEntity : c.getTbProductFormatsByProductId()) {
                        if (formatEntity.getLogicDelete() != null &&!formatEntity.getLogicDelete() && formatEntity.getPfStatus() != null && !formatEntity.getPfStatus())
                            this.nodes.add(new ProductTree(formatter(formatEntity), null, false));
                    }
                }
            }
        }
        if (nodes.size() <=0)
            this.nodes = null;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ProductTree> getNodes() {
        return nodes;
    }

    public void setNodes(List<ProductTree> nodes) {
        this.nodes = nodes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getSelectable() {
        return selectable;
    }

    public void setSelectable(Boolean selectable) {
        this.selectable = selectable;
    }

    private String formatter(ProductFormatEntity formatEntity){
        String click = "returnProduct(\""+getStringBuffer(formatEntity)+"\","+formatEntity.getPfId()+","+formatEntity.getPfPrice()+","+formatEntity.getTbProductByProductId().getProductStock()+")";
        StringBuilder sb = new StringBuilder();
        sb.append("<img src='/img/pdata1.gif'/><span>" +
                "<a href='javascript:;' onclick="+click+" style='color: #000;margin-left: 5px'>"+getStringBuffer(formatEntity)+"</a>");
        if (!formatEntity.getTbProductByProductId().getProductStock())
            sb.append("<i class='fa fa fa-shopping-cart' style='margin-left: 5px'></i>");
        sb.append("<a href='javascript:;' onclick='openProduct("+formatEntity.getPfId()+")' target='_blank' style='margin-left: 5px;color: #505458;'><i class='fa fa-comment-o'>详情</i></a><span><br>");
        return sb.toString();
    }

    /**
     * 获取title

     * @return
     */
    private String getStringBuffer(ProductFormatEntity formatEntity) {
        String productName = formatEntity.getTbProductByProductId().getProductName();
        String productModalName = formatEntity.getTbProductByProductId().getProductModel();
        String formaterName = formatEntity.getPfName();
        String util = formatEntity.getPfName();
        StringBuffer sb = new StringBuffer();
        if (productName != null && productName.length() >0)
            sb.append(productName +"/");
        if (productModalName != null && productModalName.length() >0)
            sb.append(productModalName +"/");
        if (formaterName != null && formaterName.length() >0)
            sb.append(formaterName +"/");
        int o = sb.lastIndexOf("/");
        sb.deleteCharAt(o);
        if (util != null && util.length() >0)
            sb.append(productName +"/");
        return sb.toString();
    }
}
