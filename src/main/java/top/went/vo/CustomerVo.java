package top.went.vo;

import top.went.pojo.CustomerEntity;

import java.util.Map;

public class CustomerVo extends CustomerEntity {
    private String cusName;
    private String cusAbbreviation;
    private String cusType;
    private String userName;
    private String cusLevel;
    private String cusIndusty;
    private String cusStage;
    private String cuslifeCycle;

    private Integer limit;
    private Integer offset;
    private String family;
    private String lifeCycle;
    private String kid;
    private String field;
    private String all;

    //第一个
    private Map type;
    //第二个
    private Map level;
    //第三个
    private Map come;


    public CustomerVo(String lifeCycle, String kid, String field, String all) {
        this.lifeCycle = lifeCycle;
        this.kid = kid;
        this.field = field;
        this.all = all;
    }

    public String getKid() {
        return kid;
    }

    public void setKid(String kid) {
        this.kid = kid;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }

    public CustomerVo() {
    }

    public CustomerVo(String cusName, String cusAbbreviation, String cusType, String userName, String cuslifeCycle, String cusLevel, String cusIndusty, String cusStage) {
        this.cusName = cusName;
        this.cusAbbreviation = cusAbbreviation;
        this.cusType = cusType;
        this.userName = userName;
        this.lifeCycle = lifeCycle;
        this.cusLevel = cusLevel;
        this.cusIndusty = cusIndusty;
        this.cusStage = cusStage;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusAbbreviation() {
        return cusAbbreviation;
    }

    public void setCusAbbreviation(String cusAbbreviation) {
        this.cusAbbreviation = cusAbbreviation;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLifeCycle() {
        return lifeCycle;
    }

    public void setLifeCycle(String lifeCycle) {
        this.lifeCycle = lifeCycle;
    }

    public String getCusLevel() {
        return cusLevel;
    }

    public void setCusLevel(String cusLevel) {
        this.cusLevel = cusLevel;
    }

    public String getCusIndusty() {
        return cusIndusty;
    }

    public void setCusIndusty(String cusIndusty) {
        this.cusIndusty = cusIndusty;
    }

    public String getCusStage() {
        return cusStage;
    }

    public void setCusStage(String cusStage) {
        this.cusStage = cusStage;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getcuslifeCycle() {
        return cuslifeCycle;
    }

    public void setCuslifeCycle(String cuslifeCycle) {
        this.cuslifeCycle = cuslifeCycle;
    }

    public Map getType() {
        return type;
    }

    public void setType(Map type) {
        this.type = type;
    }

    public Map getLevel() {
        return level;
    }

    public void setLevel(Map level) {
        this.level = level;
    }

    public Map getCome() {
        return come;
    }

    public void setCome(Map come) {
        this.come = come;
    }

}
