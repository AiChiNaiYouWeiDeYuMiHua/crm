package top.went.vo;

import top.went.pojo.OrderEntity;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 * 订单查询
 */
public class OrderSearch extends OrderEntity{
    private Page page;
    private Long categorySimple;
    private String type;
    private String searchText;

    private List<Long> users;
    private List<String> payWays;
    private List<String> returnWays;
    private List<String> categorys;

    private BigDecimal totalLt;
    private BigDecimal totalRt;
    private BigDecimal moriLt;
    private BigDecimal moriRt;
    private BigDecimal returnMoneyLt;
    private BigDecimal returnMoneyRt;

    private Date startLt;
    private Date startRt;
    private Date stopLt;
    private Date stopRt;

    private Long cusId;
    private String userByName;

    public Page getPage() {
        if (this.page == null)
            this.page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Long getCategorySimple() {
        return categorySimple;
    }

    public void setCategorySimple(Long categorySimple) {
        this.categorySimple = categorySimple;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSearchText() {
        if (searchText == null || this.searchText.length() <=0)
            searchText = "%%";
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = "%"+searchText+"%";
    }

    public List<Long> getUsers() {
        return users;
    }

    public void setUsers(List<Long> users) {
        this.users = users;
    }

    public List<String> getPayWays() {
        return payWays;
    }

    public void setPayWays(List<String> payWays) {
        this.payWays = payWays;
    }

    public List<String> getReturnWays() {
        return returnWays;
    }

    public void setReturnWays(List<String> returnWays) {
        this.returnWays = returnWays;
    }

    public List<String> getCategorys() {
        return categorys;
    }

    public void setCategorys(List<String> categorys) {
        this.categorys = categorys;
    }

    public BigDecimal getTotalLt() {
        return totalLt;
    }

    public void setTotalLt(BigDecimal totalLt) {
        this.totalLt = totalLt;
    }

    public BigDecimal getTotalRt() {
        return totalRt;
    }

    public void setTotalRt(BigDecimal totalRt) {
        this.totalRt = totalRt;
    }

    public BigDecimal getMoriLt() {
        return moriLt;
    }

    public void setMoriLt(BigDecimal moriLt) {
        this.moriLt = moriLt;
    }

    public BigDecimal getMoriRt() {
        return moriRt;
    }

    public void setMoriRt(BigDecimal moriRt) {
        this.moriRt = moriRt;
    }

    public BigDecimal getReturnMoneyLt() {
        return returnMoneyLt;
    }

    public void setReturnMoneyLt(BigDecimal returnMoneyLt) {
        this.returnMoneyLt = returnMoneyLt;
    }

    public BigDecimal getReturnMoneyRt() {
        return returnMoneyRt;
    }

    public void setReturnMoneyRt(BigDecimal returnMoneyRt) {
        this.returnMoneyRt = returnMoneyRt;
    }

    public Date getStartLt() {
        return startLt;
    }

    public void setStartLt(Date startLt) {
        this.startLt = startLt;
    }

    public Date getStartRt() {
        return startRt;
    }

    public void setStartRt(Date startRt) {
        this.startRt = startRt;
    }

    public Date getStopLt() {
        return stopLt;
    }

    public void setStopLt(Date stopLt) {
        this.stopLt = stopLt;
    }

    public Date getStopRt() {
        return stopRt;
    }

    public void setStopRt(Date stopRt) {
        this.stopRt = stopRt;
    }

    public Long getCusId() {
        return cusId;
    }

    public void setCusId(Long cusId) {
        this.cusId = cusId;
    }

    public String getUserByName() {
        return userByName;
    }

    public void setUserByName(String userByName) {
        this.userByName = userByName;
    }
}
