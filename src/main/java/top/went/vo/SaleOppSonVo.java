package top.went.vo;

import top.went.pojo.SaleOppEntity;

import java.sql.Date;

/**
 * 销售机会Vo
 */
public class SaleOppSonVo {

    private SaleOppEntity saleOppEntity;
    private Date discoveryTimeFrom;
    private Date discoveryTimeTo;
    private Date expectedTimeFrom;
    private Date expectedTimeTo;
    private Date updateTimeFrom;
    private Date updateTimeTo;
    private Date createTimeFrom;
    private Date createTimeTo;

    private String familyDa;
    private String kidDa;

    private String searchText;
    private Long searchTextType;

    private Page page;


    public String getFamilyDa() {
        return familyDa;
    }

    public void setFamilyDa(String familyDa) {
        this.familyDa = familyDa;
    }

    public String getKidDa() {
        return kidDa;
    }

    public Date getExpectedTimeTo() {
        return expectedTimeTo;
    }

    public void setExpectedTimeTo(Date expectedTimeTo) {
        this.expectedTimeTo = expectedTimeTo;
    }

    public void setKidDa(String kidDa) {
        this.kidDa = kidDa;
    }

    public Long getSearchTextType() {
        return searchTextType;
    }

    public void setSearchTextType(Long searchTextType) {
        this.searchTextType = searchTextType;
    }


    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }



    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public SaleOppEntity getSaleOppEntity() {
        return saleOppEntity;
    }

    public void setSaleOppEntity(SaleOppEntity saleOppEntity) {
        this.saleOppEntity = saleOppEntity;
    }

    public Date getDiscoveryTimeFrom() {
        return discoveryTimeFrom;
    }

    public void setDiscoveryTimeFrom(Date discoveryTimeFrom) {
        this.discoveryTimeFrom = discoveryTimeFrom;
    }

    public Date getDiscoveryTimeTo() {
        return discoveryTimeTo;
    }

    public void setDiscoveryTimeTo(Date discoveryTimeTo) {
        this.discoveryTimeTo = discoveryTimeTo;
    }

    public Date getExpectedTimeFrom() {
        return expectedTimeFrom;
    }

    public void setExpectedTimeFrom(Date expectedTimeFrom) {
        this.expectedTimeFrom = expectedTimeFrom;
    }


    public Date getUpdateTimeFrom() {
        return updateTimeFrom;
    }

    public void setUpdateTimeFrom(Date updateTimeFrom) {
        this.updateTimeFrom = updateTimeFrom;
    }

    public Date getUpdateTimeTo() {
        return updateTimeTo;
    }

    public void setUpdateTimeTo(Date updateTimeTo) {
        this.updateTimeTo = updateTimeTo;
    }

    public Date getCreateTimeFrom() {
        return createTimeFrom;
    }

    public void setCreateTimeFrom(Date createTimeFrom) {
        this.createTimeFrom = createTimeFrom;
    }

    public Date getCreateTimeTo() {
        return createTimeTo;
    }

    public void setCreateTimeTo(Date createTimeTo) {
        this.createTimeTo = createTimeTo;
    }

}
