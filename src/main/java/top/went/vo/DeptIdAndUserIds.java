package top.went.vo;

import java.util.List;
import java.util.Objects;

public class DeptIdAndUserIds {
    private Long deptId;
    private List<Long> userIds;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeptIdAndUserIds that = (DeptIdAndUserIds) o;
        return Objects.equals(deptId, that.deptId) &&
                Objects.equals(userIds, that.userIds);
    }

    @Override
    public int hashCode() {

        return Objects.hash(deptId, userIds);
    }

    @Override
    public String toString() {
        return "DeptIdAndUserIds{" +
                "deptId=" + deptId +
                ", userIds=" + userIds +
                '}';
    }
}
