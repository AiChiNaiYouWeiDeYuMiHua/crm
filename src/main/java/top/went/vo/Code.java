package top.went.vo;

/**
 * JSON返回状态码
 */
public class Code {
    private int code;
    private String status;
    private String msg;
    private Object data;
    public Code() {
    }
    public Code(int code, String status, String msg) {
        this.code = code;
        this.status = status;
        this.msg = msg;
    }

    public Code(int code, String status, String msg, Object data) {
        this.code = code;
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public static Code success(){
        return new Code(200,"success","");
    }
    public static Code success(String msg){
        return new Code(200,"success",msg);
    }
    public static Code success(Object data){
        return new Code(200,"success",null,data);
    }
    public static Code success(String msg, Object data){
        return new Code(200,"success",msg,data);
    }
    public static Code fail(){
        return new Code(400,"fail","");
    }
    public static Code fail(String msg){
        return new Code(400,"fail",msg);
    }
    public static Code fail(Exception msg){
        return new Code(400,"fail",msg.getMessage());
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
