package top.went.vo;

import top.went.pojo.DeptEntity;

import java.util.Objects;

public class DeptTable extends DeptEntity {
    private String deptManager;
    private int deptCount;

    public String getDeptManager() {
        return deptManager;
    }

    public void setDeptManager(String deptManager) {
        this.deptManager = deptManager;
    }

    public int getDeptCount() {
        return deptCount;
    }

    public void setDeptCount(int deptCount) {
        this.deptCount = deptCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DeptTable deptTable = (DeptTable) o;
        return deptCount == deptTable.deptCount &&
                Objects.equals(deptManager, deptTable.deptManager);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), deptManager, deptCount);
    }

    @Override
    public String toString() {
        return "DeptTable{" +
                "deptManager='" + deptManager + '\'' +
                ", deptCount=" + deptCount +
                '}';
    }
}
