package top.went.vo;

import java.util.List;

public class PageEntity<T> {
    private Long total;
    private List<T> rows;

    public PageEntity(Long total, List<T> rows) {
        this.total = total;
        this.rows = rows;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "PageEntity{" +
                "total=" + total +
                ", rows=" + rows +
                '}';
    }
}
