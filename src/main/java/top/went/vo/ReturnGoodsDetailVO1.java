package top.went.vo;

import top.went.pojo.ReturnGoodsDetailEntity;

import java.util.ArrayList;

public class ReturnGoodsDetailVO1 {
    private ArrayList<ReturnGoodsDetailEntity> datas;
    private Long id;

    public ReturnGoodsDetailVO1() {
    }

    public ArrayList<ReturnGoodsDetailEntity> getDatas() {
        return datas;
    }

    public void setDatas(ArrayList<ReturnGoodsDetailEntity> datas) {
        this.datas = datas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
