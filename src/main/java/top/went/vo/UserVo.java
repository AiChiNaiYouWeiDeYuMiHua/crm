package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.UserEntity;

import java.sql.Date;
import java.util.Objects;

public class UserVo extends UserEntity {
    @JSONField(name = "nodeid")
    private Long userId;
    @JSONField(name = "text")
    private String userName;
    private String userSex;
    private String userAddress;
    private String userTel;
    private String userEmail;
    private Date hiredate;
    private Date birthday;
    private String userCode;
    private Long deptId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserVo userVo = (UserVo) o;
        return Objects.equals(userId, userVo.userId) &&
                Objects.equals(userName, userVo.userName) &&
                Objects.equals(userSex, userVo.userSex) &&
                Objects.equals(userAddress, userVo.userAddress) &&
                Objects.equals(userTel, userVo.userTel) &&
                Objects.equals(userEmail, userVo.userEmail) &&
                Objects.equals(hiredate, userVo.hiredate) &&
                Objects.equals(birthday, userVo.birthday) &&
                Objects.equals(userCode, userVo.userCode) &&
                Objects.equals(deptId, userVo.deptId);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Date getHiredate() {
        return hiredate;
    }

    public void setHiredate(Date hiredate) {
        this.hiredate = hiredate;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, userName, userSex, userAddress, userTel, userEmail, hiredate, birthday, userCode, deptId);
    }

    @Override
    public String toString() {
        return "UserVo{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userSex='" + userSex + '\'' +
                ", userAddress='" + userAddress + '\'' +
                ", userTel='" + userTel + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", hiredate=" + hiredate +
                ", birthday=" + birthday +
                ", userCode='" + userCode + '\'' +
                ", deptId=" + deptId +
                '}';
    }
}
