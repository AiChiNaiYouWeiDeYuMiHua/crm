package top.went.vo;

import java.util.Map;

/**
 * table分页参数
 */
public class Page {
    private Integer page;
    private Integer size;
    private String sortName;
    private String sortOrder;

    public Integer getPage() {
        if (page == null || page <0)
            this.page = 0;
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        if (this.size == null ||this.size <= 0)
            this.size = 10;
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * 排序
     * @param map
     * @return
     */
    public String getSort(Map<String,String> map){
        System.out.println(map);
        System.out.println(map.get(this.sortName) +" "+this.sortOrder);
        if (this.sortName != null && map.containsKey(this.sortName) && ("asc".equals(this.sortOrder)||"desc".equals(this.sortOrder))){
            return map.get(this.sortName) +" "+this.sortOrder;
        }
        return "";
    }
}
