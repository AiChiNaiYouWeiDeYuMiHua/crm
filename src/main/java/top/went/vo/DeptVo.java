package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.DeptEntity;
import top.went.pojo.UserEntity;

import java.util.List;

public class DeptVo extends DeptEntity {
    @JSONField(name = "nodes")
    private List<UserVo> list;

    public List<UserVo> getList() {
        return list;
    }

    public void setList(List<UserVo> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "DeptVo{" +
                "list=" + list +
                '}';
    }
}
