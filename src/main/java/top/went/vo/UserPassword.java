package top.went.vo;

import top.went.pojo.UserEntity;

import java.util.Objects;

public class UserPassword extends UserVo {
    private String userPassOld;
    private String userPassNew1;
    private String userPassNew2;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getUserPassOld() {
        return userPassOld;
    }

    public void setUserPassOld(String userPassOld) {
        this.userPassOld = userPassOld;
    }

    public String getUserPassNew1() {
        return userPassNew1;
    }

    public void setUserPassNew1(String userPassNew1) {
        this.userPassNew1 = userPassNew1;
    }

    public void setUserPassNew2(String userPassNew2) {
        this.userPassNew2 = userPassNew2;
    }

    public String getUserPassNew2() {

        return userPassNew2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserPassword that = (UserPassword) o;
        return selected == that.selected &&
                Objects.equals(userPassOld, that.userPassOld) &&
                Objects.equals(userPassNew1, that.userPassNew1) &&
                Objects.equals(userPassNew2, that.userPassNew2);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), userPassOld, userPassNew1, userPassNew2, selected);
    }

    @Override
    public String toString() {
        return "UserPassword{" +
                "userPassOld='" + userPassOld + '\'' +
                ", userPassNew1='" + userPassNew1 + '\'' +
                ", userPassNew2='" + userPassNew2 + '\'' +
                ", selected=" + selected +
                '}';
    }
}
