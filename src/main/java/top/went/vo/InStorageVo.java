package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;

import java.sql.Date;

/**
 * 入库
 */
public class InStorageVo {
    private Integer wpId;
    private String wpName;
    @JSONField(format = "yyyy-MM-dd")
    private Date wpDate;
    private Boolean wpStatus;
    @JSONField(format = "yyyy-MM-dd")
    private Date wpExecute;
    private String wpOther;
    private Boolean logicDelete;
    private Integer purchaseId;
    private String purchaseName;
    private Long userId;
    private String userName;
    private Long executeId;
    private String executName;
    private Long wareHouseId;
    private String wareHouseName;
    private Integer rgId;
    private String rgName;
    private Integer cusId;
    private String cusName;

    public Integer getWpId() {
        return wpId;
    }

    public void setWpId(Integer wpId) {
        this.wpId = wpId;
    }

    public String getWpName() {
        return wpName;
    }

    public void setWpName(String wpName) {
        this.wpName = wpName;
    }

    public Date getWpDate() {
        return wpDate;
    }

    public void setWpDate(Date wpDate) {
        this.wpDate = wpDate;
    }

    public Boolean getWpStatus() {
        return wpStatus;
    }

    public void setWpStatus(Boolean wpStatus) {
        this.wpStatus = wpStatus;
    }

    public Date getWpExecute() {
        return wpExecute;
    }

    public void setWpExecute(Date wpExecute) {
        this.wpExecute = wpExecute;
    }

    public String getWpOther() {
        return wpOther;
    }

    public void setWpOther(String wpOther) {
        this.wpOther = wpOther;
    }

    public Boolean getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Boolean logicDelete) {
        this.logicDelete = logicDelete;
    }

    public Integer getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Integer purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getPurchaseName() {
        return purchaseName;
    }

    public void setPurchaseName(String purchaseName) {
        this.purchaseName = purchaseName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getExecuteId() {
        return executeId;
    }

    public void setExecuteId(Long executeId) {
        this.executeId = executeId;
    }

    public String getExecutName() {
        return executName;
    }

    public void setExecutName(String executName) {
        this.executName = executName;
    }

    public Long getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(Long wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public Integer getRgId() {
        return rgId;
    }

    public void setRgId(Integer rgId) {
        this.rgId = rgId;
    }

    public String getRgName() {
        return rgName;
    }

    public void setRgName(String rgName) {
        this.rgName = rgName;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }
}
