package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.MaintainOrderEntity;
import top.went.pojo.PayBackDetailEntity;

import java.util.Date;

public class MaintainVO {
    private Integer mtId;
    private String mtTheme;
    @JSONField(format = "yyyy-MM-dd")
    private Date mtDate;
    @JSONField(format = "yyyy-MM-dd")
    private Date mtCompleteDate;
    @JSONField(format = "yyyy-MM-dd")
    private Date mtProDate;
    private String mtSchedule;
    private Double mtPay;
    private String mtState;
    private String mtAccessories;
    private Long magicDelete;
    private Long mtIsProtect;
    private String mtDescript;
    private String mtExtract;
    private String mtTel;
    private String mtName;
    private Integer userId;
    private String userName;
    private Integer cusId;
    private String cusName;
    private Integer consId;
    private String consName;
    private Date from1;
    private Date to1;
    private Date from2;
    private Date to2;
    private Date from3;
    private Date to3;
    private Page page;
    private Integer pfId;
    private String pfName;
    private String stateList[];
    private Integer orderId;
    private String orderTitle;


    public MaintainVO() {
    }

    public MaintainVO(MaintainOrderEntity maintainOrderEntity) {
        if(maintainOrderEntity!=null) {
            this.mtId = maintainOrderEntity.getMtId();
            this.mtTheme = maintainOrderEntity.getMtTheme();
            this.mtDate = maintainOrderEntity.getMtDate();
            this.mtCompleteDate = maintainOrderEntity.getMtCompleteDate();
            this.mtProDate = maintainOrderEntity.getMtProDate();
            this.mtSchedule = maintainOrderEntity.getMtSchedule();
            this.magicDelete = maintainOrderEntity.getMagicDelete();
            this.mtPay = maintainOrderEntity.getMtPay();
            this.mtState = maintainOrderEntity.getMtState();
            this.mtAccessories = maintainOrderEntity.getMtAccessories();
            this.mtIsProtect = maintainOrderEntity.getMtIsProtect();
            this.mtDescript = maintainOrderEntity.getMtDescript();
            this.mtExtract = maintainOrderEntity.getMtExtract();
            this.mtTel = maintainOrderEntity.getMtTel();
            this.mtName = maintainOrderEntity.getMtName();
        }
        if (maintainOrderEntity.getTbUserByUserId() != null) {
            this.userId = Math.toIntExact(maintainOrderEntity.getTbUserByUserId().getUserId());
            this.userName = maintainOrderEntity.getTbUserByUserId().getUserName();
        }
        if(maintainOrderEntity.getTbCustomerByCusId()!=null){
            this.cusId=maintainOrderEntity.getTbCustomerByCusId().getCusId();
            this.cusName=maintainOrderEntity.getTbCustomerByCusId().getCusName();
        }
        if(maintainOrderEntity.getTbProductByProductId()!=null){
            this.pfId=maintainOrderEntity.getTbProductByProductId().getPfId();
            this.pfName=maintainOrderEntity.getTbProductByProductId().getPfName();
        }
        if(maintainOrderEntity.getOrderEntity()!=null){
            this.orderId=maintainOrderEntity.getOrderEntity().getOrderId();
            this.orderTitle=maintainOrderEntity.getOrderEntity().getOrderTitle();
        }
    }

    public Integer getMtId() {
        return mtId;
    }

    public void setMtId(Integer mtId) {
        this.mtId = mtId;
    }

    public String getMtTheme() {
        return mtTheme;
    }

    public void setMtTheme(String mtTheme) {
        this.mtTheme = mtTheme;
    }

    public Date getMtDate() {
        return mtDate;
    }

    public void setMtDate(Date mtDate) {
        this.mtDate = mtDate;
    }

    public Date getMtCompleteDate() {
        return mtCompleteDate;
    }

    public void setMtCompleteDate(Date mtCompleteDate) {
        this.mtCompleteDate = mtCompleteDate;
    }
    public Date getMtProDate() {
        return mtProDate;
    }

    public void setMtProDate(Date mtProDate) {
        this.mtProDate = mtProDate;
    }

    public String getMtSchedule() {
        return mtSchedule;
    }

    public void setMtSchedule(String mtSchedule) {
        this.mtSchedule = mtSchedule;
    }

    public Double getMtPay() {
        return mtPay;
    }

    public void setMtPay(Double mtPay) {
        this.mtPay = mtPay;
    }

    public String getMtState() {
        return mtState;
    }

    public void setMtState(String mtState) {
        this.mtState = mtState;
    }

    public String getMtAccessories() {
        return mtAccessories;
    }

    public void setMtAccessories(String mtAccessories) {
        this.mtAccessories = mtAccessories;
    }

    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    public Long getMtIsProtect() {
        return mtIsProtect;
    }

    public void setMtIsProtect(Long mtIsProtect) {
        this.mtIsProtect = mtIsProtect;
    }

    public String getMtDescript() {
        return mtDescript;
    }

    public void setMtDescript(String mtDescript) {
        this.mtDescript = mtDescript;
    }

    public String getMtExtract() {
        return mtExtract;
    }

    public void setMtExtract(String mtExtract) {
        this.mtExtract = mtExtract;
    }

    public String getMtTel() {
        return mtTel;
    }

    public void setMtTel(String mtTel) {
        this.mtTel = mtTel;
    }

    public String getMtName() {
        return mtName;
    }

    public void setMtName(String mtName) {
        this.mtName = mtName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Date getFrom1() {
        return from1;
    }

    public void setFrom1(Date from1) {
        this.from1 = from1;
    }

    public Date getTo1() {
        return to1;
    }

    public void setTo1(Date to1) {
        this.to1 = to1;
    }

    public Date getFrom2() {
        return from2;
    }

    public void setFrom2(Date from2) {
        this.from2 = from2;
    }

    public Date getTo2() {
        return to2;
    }

    public void setTo2(Date to2) {
        this.to2 = to2;
    }

    public Date getFrom3() {
        return from3;
    }

    public void setFrom3(Date from3) {
        this.from3 = from3;
    }

    public Date getTo3() {
        return to3;
    }

    public void setTo3(Date to3) {
        this.to3 = to3;
    }

    public Page getPage() {
        if (page == null)
            page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }


    public Integer getPfId() {
        return pfId;
    }

    public void setPfId(Integer pfId) {
        this.pfId = pfId;
    }

    public String getPfName() {
        return pfName;
    }

    public void setPfName(String pfName) {
        this.pfName = pfName;
    }

    public String[] getStateList() {
        return stateList;
    }

    public void setStateList(String[] stateList) {
        this.stateList = stateList;
    }

    public Integer getConsId() {
        return consId;
    }

    public void setConsId(Integer consId) {
        this.consId = consId;
    }

    public String getConsName() {
        return consName;
    }

    public void setConsName(String consName) {
        this.consName = consName;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }
}
