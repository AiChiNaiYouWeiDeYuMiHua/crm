package top.went.vo;

import top.went.pojo.QuoteEntity;

public class Quote2DetailVo extends QuoteEntity {

    private Double totalMoney;

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }
}
