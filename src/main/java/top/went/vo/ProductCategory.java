package top.went.vo;


import top.went.pojo.ProductCategoryEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 产品类别
 */
public class ProductCategory {
    private String text;
    private List<ProductCategory> nodes;
    private Long id;
    public ProductCategory() {
    }


    public ProductCategory(String text, Long id) {
        this.text = text;
        this.id = id;
    }

    public ProductCategory(ProductCategoryEntity categoryEntity) {
        this.id = categoryEntity.getPcId();
        this.text = categoryEntity.getPcName();
        this.nodes = new ArrayList<>();
        for (ProductCategoryEntity c : categoryEntity.getTbProductCategoriesByPcId()){
            if (!c.getLogicDelete())
                nodes.add(new ProductCategory(c));
        }
        if (this.nodes.size() <=0)
            this.nodes = null;
    }

    public ProductCategory(ProductCategoryEntity categoryEntity, boolean b) {
        this.id = categoryEntity.getPcId();
        this.text = categoryEntity.getPcName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ProductCategory> getNodes() {
        return nodes;
    }

    public void setNodes(List<ProductCategory> nodes) {
        this.nodes = nodes;
    }

    @Override
    public String toString() {
        return "ProductCategory{" +
                "text='" + text + '\'' +
                ", nodes=" + nodes +
                ", id=" + id +
                '}';
    }
}
