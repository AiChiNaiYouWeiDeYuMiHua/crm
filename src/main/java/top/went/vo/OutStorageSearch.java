package top.went.vo;

public class OutStorageSearch extends OutStorageVo {
    private Page page;
    private Long categorySimple;
    private String type;
    private String searchText;

    public Page getPage() {
        if (this.page == null)
            this.page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Long getCategorySimple() {
        return categorySimple;
    }

    public void setCategorySimple(Long categorySimple) {
        this.categorySimple = categorySimple;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSearchText() {
        if (searchText == null || this.searchText.length() <=0)
            searchText = "%%";
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = "%"+searchText+"%";
    }
}
