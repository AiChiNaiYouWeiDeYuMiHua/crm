package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.PurchaseEntity;

import java.util.Date;

public class PurchaseVO {
    private Integer purId;
    private String purTheme;
    private String purOddNumbers;
    @JSONField(format = "yyyy-MM-dd")
    private Date purDate;
    private String purState;
    private String purBackRemark;
    @JSONField(format = "yyyy-MM-dd")
    private Date purProjectedDate;
    private String purRemarks;
    private Long magicDelete;
    private String purAccessories;
    private Integer purOk;
    private Integer userId;
    private String userName;
    private Integer cusId;
    private String cusName;
    private Integer whId;
    private String whName;
    private Date from1;
    private Date to1;
    private Date from2;
    private Date to2;
    private Page page;
    private String stateList[];
    private String wareList[];

    public PurchaseVO() {
    }

    public PurchaseVO(PurchaseEntity purchaseEntity){
        if(purchaseEntity!=null){
            this.purId=purchaseEntity.getPurId();
            this.purTheme=purchaseEntity.getPurTheme();
            this.purOddNumbers=purchaseEntity.getPurOddNumbers();
            this.purDate=purchaseEntity.getPurDate();
            this.purState=purchaseEntity.getPurState();
            this.purProjectedDate=purchaseEntity.getPurProjectedDate();
            this.purRemarks=purchaseEntity.getPurRemarks();
            this.magicDelete=purchaseEntity.getMagicDelete();
            this.purAccessories=purchaseEntity.getPurAccessories();
            this.purOk=purchaseEntity.getPurOk();
            this.purRemarks=purchaseEntity.getPurRemarks();
        }
        if (purchaseEntity.getTbUserByUserId() != null) {
            this.userId = Math.toIntExact(purchaseEntity.getTbUserByUserId().getUserId());
            this.userName = purchaseEntity.getTbUserByUserId().getUserName();
        }
        if(purchaseEntity.getTbCustomerByCusId()!=null){
            this.cusId=purchaseEntity.getTbCustomerByCusId().getCusId();
            this.cusName=purchaseEntity.getTbCustomerByCusId().getCusName();
        }
        if(purchaseEntity.getTbWarehouseByWhId()!=null){
            this.whId=Math.toIntExact(purchaseEntity.getTbWarehouseByWhId().getWhId());
            this.whName=purchaseEntity.getTbWarehouseByWhId().getWhName();
        }

    }
    public Integer getPurId() {
        return purId;
    }

    public void setPurId(Integer purId) {
        this.purId = purId;
    }

    public String getPurTheme() {
        return purTheme;
    }

    public void setPurTheme(String purTheme) {
        this.purTheme = purTheme;
    }

    public String getPurOddNumbers() {
        return purOddNumbers;
    }

    public void setPurOddNumbers(String purOddNumbers) {
        this.purOddNumbers = purOddNumbers;
    }

    public Date getPurDate() {
        return purDate;
    }

    public void setPurDate(Date purDate) {
        this.purDate = purDate;
    }

    public String getPurState() {
        return purState;
    }

    public void setPurState(String purState) {
        this.purState = purState;
    }

    public Date getPurProjectedDate() {
        return purProjectedDate;
    }

    public void setPurProjectedDate(Date purProjectedDate) {
        this.purProjectedDate = purProjectedDate;
    }

    public String getPurRemarks() {
        return purRemarks;
    }

    public void setPurRemarks(String purRemarks) {
        this.purRemarks = purRemarks;
    }

    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    public String getPurAccessories() {
        return purAccessories;
    }

    public void setPurAccessories(String purAccessories) {
        this.purAccessories = purAccessories;
    }

    public Integer getPurOk() {
        return purOk;
    }

    public void setPurOk(Integer purOk) {
        this.purOk = purOk;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getWarId() {
        return whId;
    }

    public void setWarId(Integer warId) {
        this.whId = warId;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public Date getFrom1() {
        return from1;
    }

    public void setFrom1(Date from1) {
        this.from1 = from1;
    }

    public Date getTo1() {
        return to1;
    }

    public void setTo1(Date to1) {
        this.to1 = to1;
    }

    public Date getFrom2() {
        return from2;
    }

    public void setFrom2(Date from2) {
        this.from2 = from2;
    }

    public Date getTo2() {
        return to2;
    }

    public void setTo2(Date to2) {
        this.to2 = to2;
    }

    public Page getPage() {
        if (page == null)
            page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Integer getWhId() {
        return whId;
    }

    public void setWhId(Integer whId) {
        this.whId = whId;
    }

    public String[] getStateList() {
        return stateList;
    }

    public void setStateList(String[] stateList) {
        this.stateList = stateList;
    }

    public String[] getWareList() {
        return wareList;
    }

    public void setWareList(String[] wareList) {
        this.wareList = wareList;
    }

    public String getPurBackRemark() {
        return purBackRemark;
    }

    public void setPurBackRemark(String purBackRemark) {
        this.purBackRemark = purBackRemark;
    }
}
