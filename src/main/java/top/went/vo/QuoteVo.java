package top.went.vo;

import top.went.pojo.QuoteEntity;

import java.sql.Date;

public class QuoteVo {
    private QuoteEntity quoteEntity;
    private Date quoteTimeFrom;
    private Date quoteTimeTo;

    public QuoteEntity getQuoteEntity() {
        return quoteEntity;
    }

    public void setQuoteEntity(QuoteEntity quoteEntity) {
        this.quoteEntity = quoteEntity;
    }

    public Date getQuoteTimeFrom() {
        return quoteTimeFrom;
    }

    public void setQuoteTimeFrom(Date quoteTimeFrom) {
        this.quoteTimeFrom = quoteTimeFrom;
    }

    public Date getQuoteTimeTo() {
        return quoteTimeTo;
    }

    public void setQuoteTimeTo(Date quoteTimeTo) {
        this.quoteTimeTo = quoteTimeTo;
    }

    private String searchText;

    public Long getSearchTextType() {
        return searchTextType;
    }

    public void setSearchTextType(Long searchTextType) {
        this.searchTextType = searchTextType;
    }

    private Long searchTextType;

    private Page page;

    private String familyDa;
    private String kidDa;

    public String getFamilyDa() {
        return familyDa;
    }

    public void setFamilyDa(String familyDa) {
        this.familyDa = familyDa;
    }

    public String getKidDa() {
        return kidDa;
    }

    public void setKidDa(String kidDa) {
        this.kidDa = kidDa;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }


}
