package top.went.vo;

import top.went.pojo.QuoteDetailEntity;

import java.sql.Date;

/**
 * 报价明细查询vo
 */
public class QuoteDetailVo extends QuoteDetailEntity {
    private Date createTimeFrom;
    private Date createTimeTo;
    private String searchText;
    private Page page;
    private String familyDa;
    private String kidDa;

    public Date getCreateTimeFrom() {
        return createTimeFrom;
    }

    public void setCreateTimeFrom(Date createTimeFrom) {
        this.createTimeFrom = createTimeFrom;
    }

    public Date getCreateTimeTo() {
        return createTimeTo;
    }

    public void setCreateTimeTo(Date createTimeTo) {
        this.createTimeTo = createTimeTo;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getFamilyDa() {
        return familyDa;
    }

    public void setFamilyDa(String familyDa) {
        this.familyDa = familyDa;
    }

    public String getKidDa() {
        return kidDa;
    }

    public void setKidDa(String kidDa) {
        this.kidDa = kidDa;
    }
}
