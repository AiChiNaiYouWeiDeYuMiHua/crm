package top.went.vo;

import top.went.pojo.ProductFormatEntity;

import java.math.BigDecimal;
import java.sql.Date;


/**
 * 查询产品
 */
public class ProductSearch {
    private ProductFormatEntity format;
    private Date birthFrom;
    private Date birthTo;
    private Date expireFrom;
    private Date expireTo;

    private BigDecimal costRt;
    private BigDecimal costLt;

    private BigDecimal priceRt;
    private BigDecimal priceLt;

    private Long categorySimple;
    private Long type;
    private String searchText;

    private Page page;

    public ProductFormatEntity getFormat() {
        return format;
    }

    public void setFormat(ProductFormatEntity format) {
        this.format = format;
    }

    public Date getBirthFrom() {
        return birthFrom;
    }

    public void setBirthFrom(Date birthFrom) {
        this.birthFrom = birthFrom;
    }

    public Date getBirthTo() {
        return birthTo;
    }

    public void setBirthTo(Date birthTo) {
        this.birthTo = birthTo;
    }

    public Date getExpireFrom() {
        return expireFrom;
    }

    public void setExpireFrom(Date expireFrom) {
        this.expireFrom = expireFrom;
    }

    public Date getExpireTo() {
        return expireTo;
    }

    public void setExpireTo(Date expireTo) {
        this.expireTo = expireTo;
    }


    public Long getCategorySimple() {
        return categorySimple;
    }

    public void setCategorySimple(Long categorySimple) {
        this.categorySimple = categorySimple;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getSearchText() {
        if (this.searchText == null || this.searchText.length() <=0)
            return "%%";
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = "%"+searchText+"%";
    }

    public Page getPage() {
        if (this.page == null)
            this.page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public BigDecimal getCostRt() {
        return costRt;
    }

    public void setCostRt(BigDecimal costRt) {
        this.costRt = costRt;
    }

    public BigDecimal getCostLt() {
        return costLt;
    }

    public void setCostLt(BigDecimal costLt) {
        this.costLt = costLt;
    }

    public BigDecimal getPriceRt() {
        return priceRt;
    }

    public void setPriceRt(BigDecimal priceRt) {
        this.priceRt = priceRt;
    }

    public BigDecimal getPriceLt() {
        return priceLt;
    }

    public void setPriceLt(BigDecimal priceLt) {
        this.priceLt = priceLt;
    }



    @Override
    public String toString() {
        return "ProductSearch{" +
                "format=" + format +
                ", birthFrom=" + birthFrom +
                ", birthTo=" + birthTo +
                ", expireFrom=" + expireFrom +
                ", expireTo=" + expireTo +
                ", categorySimple=" + categorySimple +
                ", type=" + type +
                ", searchText='" + searchText + '\'' +
                ", page=" + page +
                '}';
    }
}
