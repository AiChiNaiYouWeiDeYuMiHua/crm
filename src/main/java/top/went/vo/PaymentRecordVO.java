package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.PaymentRecordsEntity;

import java.math.BigDecimal;
import java.util.Date;

public class PaymentRecordVO {
    private Double prMoney;
    private Long prTerms;
    @JSONField(format = "yyyy-MM-dd")
    private Date prDate;
    private String prDate1;
    private String prIsTicket;
    private Long magicDelete;
    private Integer prId;
    private Integer prType;
    private Date from;
    private Date to;
    private Long userId;
    private String userName;
    private Integer cusId;
    private String cusName;
    private Page page;
    private String termsList[];
    private Integer purId;
    private String purThemes;
    private Integer rgId;
    private String rgTheme;
    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Double getPrMoney() {
        return prMoney;
    }

    public PaymentRecordVO() {
    }

    public PaymentRecordVO(Integer purId) {
        this.purId = purId;
    }

    public PaymentRecordVO(PaymentRecordsEntity paymentRecordsEntity){
        if(paymentRecordsEntity.getPrId()!=null){
            this.prId=paymentRecordsEntity.getPrId();
            this.prTerms=paymentRecordsEntity.getPrTerms();
            this.prMoney=paymentRecordsEntity.getPrMoney();
            this.prDate=paymentRecordsEntity.getPrDate();
            this.prIsTicket=paymentRecordsEntity.getPrIsTicket();
            this.magicDelete=paymentRecordsEntity.getMagicDelete();
            this.prType=paymentRecordsEntity.getPrType();
        }
        if(paymentRecordsEntity.getTbUserByUserId()!=null){
            this.userId=paymentRecordsEntity.getTbUserByUserId().getUserId();
            this.userName=paymentRecordsEntity.getTbUserByUserId().getUserName();
        }
        if(paymentRecordsEntity.getTbCustomerByCusId()!=null){
            this.cusId=paymentRecordsEntity.getTbCustomerByCusId().getCusId();
            this.cusName=paymentRecordsEntity.getTbCustomerByCusId().getCusName();
        }
        if(paymentRecordsEntity.getTbPurchaseByPurId()!=null){
            this.purId=paymentRecordsEntity.getTbPurchaseByPurId().getPurId();
            this.purThemes=paymentRecordsEntity.getTbPurchaseByPurId().getPurTheme();
        }
        if(paymentRecordsEntity.getTbReturnGoodsByRgId()!=null){
            this.rgId=paymentRecordsEntity.getTbReturnGoodsByRgId().getRgId();
            this.rgTheme=paymentRecordsEntity.getTbReturnGoodsByRgId().getRgTheme();
        }
    }
    public void setPrMoney(Double prMoney) {
        this.prMoney = prMoney;
    }

    public Long getPrTerms() {
        return prTerms;
    }

    public void setPrTerms(Long prTerms) {
        this.prTerms = prTerms;
    }

    public Date getPrDate() {
        return prDate;
    }

    public void setPrDate(Date prDate) {
        this.prDate = prDate;
    }

    public String getPrIsTicket() {
        return prIsTicket;
    }

    public void setPrIsTicket(String prIsTicket) {
        this.prIsTicket = prIsTicket;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    public Integer getPrId() {
        return prId;
    }

    public void setPrId(Integer prId) {
        this.prId = prId;
    }

    public Page getPage() {
        if (page == null)
            page = new Page();
        return page;
    }

    public String[] getTermsList() {
        return termsList;
    }

    public void setTermsList(String termsList[]) {
        this.termsList = termsList;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Integer getPurId() {
        return purId;
    }

    public void setPurId(Integer purId) {
        this.purId = purId;
    }

    public Integer getPrType() {
        return prType;
    }

    public void setPrType(Integer prType) {
        this.prType = prType;
    }

    public String getPrDate1() {
        return prDate1;
    }

    public void setPrDate1(String prDate1) {
        this.prDate1 = prDate1;
    }

    public String getPurThemes() {
        return purThemes;
    }

    public void setPurThemes(String purThemes) {
        this.purThemes = purThemes;
    }

    public Integer getRgId() {
        return rgId;
    }

    public void setRgId(Integer rgId) {
        this.rgId = rgId;
    }

    public String getRgTheme() {
        return rgTheme;
    }

    public void setRgTheme(String rgTheme) {
        this.rgTheme = rgTheme;
    }
}
