package top.went.vo;

import java.util.List;
import java.util.Objects;

public class RoleIdAndFcIds {
    private Long roleId;
    private List<Long> fcIds;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public List<Long> getFcIds() {
        return fcIds;
    }

    public void setFcIds(List<Long> fcIds) {
        this.fcIds = fcIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleIdAndFcIds that = (RoleIdAndFcIds) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(fcIds, that.fcIds);
    }

    @Override
    public int hashCode() {

        return Objects.hash(roleId, fcIds);
    }

    @Override
    public String toString() {
        return "RoleIdAndFcIds{" +
                "roleId=" + roleId +
                ", fcId=" + fcIds +
                '}';
    }
}
