package top.went.vo;

import top.went.pojo.ContactsEntity;

import java.util.List;

public class ContactVo extends ContactsEntity {
    //上传数据
    //上级联系人
    private List<String> cotsPoint;
    private String cusName;
    private String projName;

    //搜索联系人数据
    private Integer limit;
    private Integer offset;
    private String bjxLetter;
    private String bjxFirstName;
    private String kid;
    private String all;

    public ContactVo(Integer limit, Integer offset, String bjxLetter, String bjxFirstName, String kid, String all) {
        this.limit = limit;
        this.offset = offset;
        this.bjxLetter = bjxLetter;
        this.bjxFirstName = bjxFirstName;
        this.kid = kid;
        this.all = all;
    }

    public ContactVo() {
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getBjxLetter() {
        return bjxLetter;
    }

    public void setBjxLetter(String bjxLetter) {
        this.bjxLetter = bjxLetter;
    }

    public String getBjxFirstName() {
        return bjxFirstName;
    }

    public void setBjxFirstName(String bjxFirstName) {
        this.bjxFirstName = bjxFirstName;
    }

    public String getKid() {
        return kid;
    }

    public void setKid(String kid) {
        this.kid = kid;
    }

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }

    public List<String> getCotsPoint() {
        return cotsPoint;
    }

    public void setCotsPoint(List<String> cotsPoint) {
        this.cotsPoint = cotsPoint;
    }

    public String getProjName() {
        return projName;
    }

    public void setProjName(String projName) {
        this.projName = projName;
    }

    @Override
    public String toString() {
        return "ContactVo{" +
                "cotsPoint=" + cotsPoint +
                ", cusName='" + cusName + '\'' +
                ", projName='" + projName + '\'' +
                ", limit=" + limit +
                ", offset=" + offset +
                ", bjxLetter='" + bjxLetter + '\'' +
                ", bjxFirstName='" + bjxFirstName + '\'' +
                ", kid='" + kid + '\'' +
                ", all='" + all + '\'' +
                '}';
    }
}
