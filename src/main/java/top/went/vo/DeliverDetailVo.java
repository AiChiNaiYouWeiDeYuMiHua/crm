package top.went.vo;

import top.went.pojo.DeliverDetailEntity;
import top.went.utils.StringUtils;

public class DeliverDetailVo extends DeliverDetailEntity {
    private String title;
    public String getTitle() {
        if (this.title == null)
            this.title = StringUtils.getStringBuffer(getFormatEntity()).toString();
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
