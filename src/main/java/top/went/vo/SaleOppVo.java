package top.went.vo;

import top.went.pojo.*;

import java.sql.Date;

/**
 * 销售机会Vo
 */
public class SaleOppVo {

    private SaleOppEntity saleOppEntity;
    private Date discoveryTimeFrom;
    private Date discoveryTimeTo;
    private Date expectedTimeFrom;
    private Date expectedTimeTo;
    private Date updateTimeFrom;
    private Date updateTimeTo;
    private String[] oppSourceList;
    private String[] stageList;
    private Long[] oppStatusList;
    private Long[] priorityList;
    private Date createTimeFrom;
    private Date createTimeTo;
    private String[] classificationList;
    private String[] intendProductList;

    private String familyDa;
    private String kidDa;

    private String searchText;
    private Long searchTextType;

    private Page page;

    private Long index;

    public Date getExpectedTimeTo() {
        return expectedTimeTo;
    }

    public void setExpectedTimeTo(Date expectedTimeTo) {
        this.expectedTimeTo = expectedTimeTo;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public SaleOppVo(){

    }
    public SaleOppVo(SaleOppSonVo saleOppSonVo){
        this.saleOppEntity = saleOppSonVo.getSaleOppEntity();
        this.discoveryTimeFrom = saleOppSonVo.getDiscoveryTimeFrom();
        this.discoveryTimeTo = saleOppSonVo.getDiscoveryTimeTo();
        this.expectedTimeFrom = saleOppSonVo.getExpectedTimeFrom();
        this.expectedTimeTo = saleOppSonVo.getExpectedTimeTo();
        this.updateTimeFrom = saleOppSonVo.getUpdateTimeFrom();
        this.updateTimeTo = saleOppSonVo.getUpdateTimeTo();
        this.createTimeFrom = saleOppSonVo.getCreateTimeFrom();
        this.createTimeTo = saleOppSonVo.getCreateTimeTo();
        this.familyDa = saleOppSonVo.getFamilyDa();
        this.kidDa = saleOppSonVo.getKidDa();
        this.searchText = saleOppSonVo.getSearchText();
        this.searchTextType = saleOppSonVo.getSearchTextType();
        this.page = saleOppSonVo.getPage();
    }

    public String getFamilyDa() {
        return familyDa;
    }

    public void setFamilyDa(String familyDa) {
        this.familyDa = familyDa;
    }

    public String getKidDa() {
        return kidDa;
    }

    public void setKidDa(String kidDa) {
        this.kidDa = kidDa;
    }

    public Long getSearchTextType() {
        return searchTextType;
    }

    public void setSearchTextType(Long searchTextType) {
        this.searchTextType = searchTextType;
    }


    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }



    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public SaleOppEntity getSaleOppEntity() {
        return saleOppEntity;
    }

    public void setSaleOppEntity(SaleOppEntity saleOppEntity) {
        this.saleOppEntity = saleOppEntity;
    }

    public Date getDiscoveryTimeFrom() {
        return discoveryTimeFrom;
    }

    public void setDiscoveryTimeFrom(Date discoveryTimeFrom) {
        this.discoveryTimeFrom = discoveryTimeFrom;
    }

    public Date getDiscoveryTimeTo() {
        return discoveryTimeTo;
    }

    public void setDiscoveryTimeTo(Date discoveryTimeTo) {
        this.discoveryTimeTo = discoveryTimeTo;
    }

    public Date getExpectedTimeFrom() {
        return expectedTimeFrom;
    }

    public void setExpectedTimeFrom(Date expectedTimeFrom) {
        this.expectedTimeFrom = expectedTimeFrom;
    }

    public Date getGetExpectedTimeTo() {
        return expectedTimeTo;
    }

    public void setGetExpectedTimeTo(Date getExpectedTimeTo) {
        this.expectedTimeTo = getExpectedTimeTo;
    }

    public Date getUpdateTimeFrom() {
        return updateTimeFrom;
    }

    public void setUpdateTimeFrom(Date updateTimeFrom) {
        this.updateTimeFrom = updateTimeFrom;
    }

    public Date getUpdateTimeTo() {
        return updateTimeTo;
    }

    public void setUpdateTimeTo(Date updateTimeTo) {
        this.updateTimeTo = updateTimeTo;
    }

    public String[] getOppSourceList() {
        return oppSourceList;
    }

    public void setOppSourceList(String[] oppSourceList) {
        this.oppSourceList = oppSourceList;
    }

    public String[] getStageList() {
        return stageList;
    }

    public void setStageList(String[] stageList) {
        this.stageList = stageList;
    }

    public Long[] getOppStatusList() {
        return oppStatusList;
    }

    public void setOppStatusList(Long[] oppStatusList) {
        this.oppStatusList = oppStatusList;
    }

    public Long[] getPriorityList() {
        return priorityList;
    }

    public void setPriorityList(Long[] priorityList) {
        this.priorityList = priorityList;
    }

    public Date getCreateTimeFrom() {
        return createTimeFrom;
    }

    public void setCreateTimeFrom(Date createTimeFrom) {
        this.createTimeFrom = createTimeFrom;
    }

    public Date getCreateTimeTo() {
        return createTimeTo;
    }

    public void setCreateTimeTo(Date createTimeTo) {
        this.createTimeTo = createTimeTo;
    }

    public String[] getClassificationList() {
        return classificationList;
    }

    public void setClassificationList(String[] classificationList) {
        this.classificationList = classificationList;
    }

    public String[] getIntendProductList() {
        return intendProductList;
    }

    public void setIntendProductList(String[] intendProductList) {
        this.intendProductList = intendProductList;
    }
}
