package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.OrderDetailEntity;
import top.went.pojo.ProductFormatEntity;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.zip.DeflaterOutputStream;

/**
 * 订单详情表格
 */
public class OrderDetail {
    private Integer id;
    private String title;
    private String productName;
    private String productModalName;
    private String formaterName;
    private Long formatterId;
    private String util;
    private Long number;
    private Long pay;
    private Long noPay;
    private BigDecimal price;
    private BigDecimal total;
    private String other;
    @JSONField(serialize = false)
    private BigDecimal profit;
    @JSONField(serialize = false)
    private ProductFormatEntity formatEntity;
    private Integer cusId;
    private String cusName;
    private Integer orderId;
    private String orderName;
    @JSONField(format = "yyyy-MM-dd")
    private Date date;
    private Integer userId;
    private String userName;


    public OrderDetail() {
    }

    public OrderDetail(Integer id,String productName, String productModalName, String formaterName, Long formatterId, String util,
                       Long number, BigDecimal price, BigDecimal total, Long pay,String other,BigDecimal profit,
                       ProductFormatEntity formatEntity,Date date) {
        this.id = id;
        this.productName = productName;
        this.productModalName = productModalName;
        this.formaterName = formaterName;
        this.formatterId = formatterId;
        this.util = util;
        this.number = number;
        this.price = price;
        this.total = total;
        this.other = other;
        this.profit = profit;
        this.formatEntity = formatEntity;
        this.date = date;
        if (pay != null)
            this.pay = pay;
        else
            this.pay = 0L;
        this.noPay = this.number - this.pay;
        StringBuffer sb = getStringBuffer(productName, productModalName, formaterName, util);
        this.title = sb.toString();
    }

    public OrderDetail(OrderDetailEntity detailEntity, Long pay) {
        this(
                detailEntity.getOdId(),
                detailEntity.getFormatEntity().getTbProductByProductId().getProductName(),
                detailEntity.getFormatEntity().getTbProductByProductId().getProductModel(),
                detailEntity.getFormatEntity().getPfName(),
                detailEntity.getFormatEntity().getPfId().longValue(),
                detailEntity.getFormatEntity().getPfUnit(),
                detailEntity.getOdNumber(),
                detailEntity.getOdPrice(),
                detailEntity.getOdTotal(),
                pay,
                detailEntity.getOdOther(),
                detailEntity.getOdProfit(),
                detailEntity.getFormatEntity(),
                detailEntity.getDate()
        );
    }

    /**
     * 获取title
     * @param productName
     * @param productModalName
     * @param formaterName
     * @param util
     * @return
     */
    private StringBuffer getStringBuffer(String productName, String productModalName, String formaterName, String util) {
        StringBuffer sb = new StringBuffer();
        if (productName != null && productName.length() >0)
            sb.append(productName +"/");
        if (productModalName != null && productModalName.length() >0)
            sb.append(productModalName +"/");
        if (formaterName != null && formaterName.length() >0)
            sb.append(formaterName +"/");
        int o = sb.lastIndexOf("/");
        if (o > 0)
            sb.deleteCharAt(o);
        if (util != null && util.length() >0)
            sb.append(productName +"/");
        return sb;
    }



    public String getTitle() {
        if (this.title == null)
            this.title = getStringBuffer(this.productName,this.productModalName,
                    this.formaterName,this.util).toString();
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductModalName() {
        return productModalName;
    }

    public void setProductModalName(String productModalName) {
        this.productModalName = productModalName;
    }

    public String getFormaterName() {
        return formaterName;
    }

    public void setFormaterName(String formaterName) {
        this.formaterName = formaterName;
    }

    public Long getFormatterId() {
        return formatterId;
    }

    public void setFormatterId(Long formatterId) {
        this.formatterId = formatterId;
    }

    public String getUtil() {
        return util;
    }

    public void setUtil(String util) {
        this.util = util;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Long getPay() {
        return pay;
    }

    public void setPay(Long pay) {
        this.pay = pay;
    }

    public Long getNoPay() {
        return noPay;
    }

    public void setNoPay(Long noPay) {
        this.noPay = noPay;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public ProductFormatEntity getFormatEntity() {
        return formatEntity;
    }

    public void setFormatEntity(ProductFormatEntity formatEntity) {
        this.formatEntity = formatEntity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
