package top.went.vo;


import top.went.pojo.ProductFormatEntity;
import top.went.pojo.WpDetailEntity;
import top.went.utils.StringUtils;


public class WpDetailVo extends WpDetailEntity {
  private Long productId;
  private String productTitle;
  private String wareHouseName;
  private String userName;
  private Long customerId;
  private String customerName;

    public Long getProductId() {
        if (getTbProductFormatByPfId() != null)
            this.productId = Long.valueOf(getTbProductFormatByPfId().getPfId());
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductTitle() {
        if (getTbProductFormatByPfId() != null)
            this.productTitle = StringUtils.getStringBuffer(getTbProductFormatByPfId()).toString();
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getWareHouseName() {
        if (getTbWarehouseByWhId() != null)
            this.wareHouseName = getTbWarehouseByWhId().getWhName();
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public String getUserName() {
        if (getTbUserByUserId() != null)
            this.userName = getTbUserByUserId().getUserName();
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getCustomerId() {
        if (getTbCustomerByCusId() != null)
            this.customerId = Long.valueOf(getTbCustomerByCusId().getCusId());
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        if (getTbCustomerByCusId() != null)
            this.customerName = getTbCustomerByCusId().getCusName();
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

}
