package top.went.vo;

import java.util.List;

/**
 * 仓库产品信息
 */
public class WareHouse {
    private Long id;
    private String productName;
    private String productModa;
    private String producFormater;
    private List<String> wareHoses;
    private Long number;
    private Long wareHose;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductModa() {
        return productModa;
    }

    public void setProductModa(String productModa) {
        this.productModa = productModa;
    }

    public String getProducFormater() {
        return producFormater;
    }

    public void setProducFormater(String producFormater) {
        this.producFormater = producFormater;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public List<String> getWareHoses() {
        return wareHoses;
    }

    public void setWareHoses(List<String> wareHoses) {
        this.wareHoses = wareHoses;
    }

    public Long getWareHose() {
        return wareHose;
    }

    public void setWareHose(Long wareHose) {
        this.wareHose = wareHose;
    }
}
