package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.PurchaseReceiptEntity;
import top.went.pojo.ReceiptRecordsEntity;

import java.sql.Time;
import java.util.Date;

public class ReceiptRecordsVO {
    private Integer rrId;
    private String rrContent;
    private String rrType;
    private Double rrMoney;
    private Long rrNumber;
    @JSONField(format = "yyyy-MM-dd")
    private Date rrDate;
    private Long rrDelete;
    private Page page;
    private Date from;
    private Date to;
    private Integer pbdId;
    private Integer cusId;
    private String cusName;
    private Integer userId;
    private String userName;
    private Integer orderId;
    private String orderTitle;
    private String typeList[];

    public ReceiptRecordsVO() {
    }

    public ReceiptRecordsVO(Integer orderId) {
        this.orderId = orderId;
    }

    public ReceiptRecordsVO(ReceiptRecordsEntity receiptRecordsEntity){
        if(receiptRecordsEntity.getRrId()!=null) {
            this.rrId = receiptRecordsEntity.getRrId();
            this.rrContent = receiptRecordsEntity.getRrContent();
            this.rrType = receiptRecordsEntity.getRrType();
            this.rrMoney = receiptRecordsEntity.getRrMoney();
            this.rrNumber = receiptRecordsEntity.getRrNumber();
            this.rrDate = receiptRecordsEntity.getRrDate();
            this.rrDelete = receiptRecordsEntity.getRrDelete();
        }
        if(receiptRecordsEntity.getTbCustomerByCusId()!=null){
            this.cusId=receiptRecordsEntity.getTbCustomerByCusId().getCusId();
            this.cusName=receiptRecordsEntity.getTbCustomerByCusId().getCusName();
        }
        if(receiptRecordsEntity.getTbUserByUserId()!=null){
            this.userId=Math.toIntExact(receiptRecordsEntity.getTbUserByUserId().getUserId());
            this.userName=receiptRecordsEntity.getTbUserByUserId().getUserName();
        }
        if(receiptRecordsEntity.getTbOrderByOrderId()!=null){
            this.orderId=receiptRecordsEntity.getTbOrderByOrderId().getOrderId();
            this.orderTitle=receiptRecordsEntity.getTbOrderByOrderId().getOrderTitle();
        }
        if(receiptRecordsEntity.getTbPayBackDetailsByRrId()!=null){
            this.pbdId=receiptRecordsEntity.getTbPayBackDetailsByRrId().getPbdId();
        }
    }
    public Integer getRrId() {
        return rrId;
    }

    public void setRrId(Integer rrId) {
        this.rrId = rrId;
    }

    public String getRrContent() {
        return rrContent;
    }

    public void setRrContent(String rrContent) {
        this.rrContent = rrContent;
    }

    public String getRrType() {
        return rrType;
    }

    public void setRrType(String rrType) {
        this.rrType = rrType;
    }

    public Double getRrMoney() {
        return rrMoney;
    }

    public void setRrMoney(Double rrMoney) {
        this.rrMoney = rrMoney;
    }

    public Long getRrNumber() {
        return rrNumber;
    }

    public void setRrNumber(Long rrNumber) {
        this.rrNumber = rrNumber;
    }

    public Date getRrDate() {
        return rrDate;
    }

    public void setRrDate(Date rrDate) {
        this.rrDate = rrDate;
    }

    public Long getRrDelete() {
        return rrDelete;
    }

    public void setRrDelete(Long rrDelete) {
        this.rrDelete = rrDelete;
    }

    public Page getPage() {
        if (this.page == null)
            this.page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Integer getPbdId() {
        return pbdId;
    }

    public void setPbdId(Integer pbdId) {
        this.pbdId = pbdId;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String[] getTypeList() {
        return typeList;
    }

    public void setTypeList(String[] typeList) {
        this.typeList = typeList;
    }
}
