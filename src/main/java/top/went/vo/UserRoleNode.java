package top.went.vo;

import top.went.pojo.RoleEntity;
import top.went.pojo.UserEntity;

import java.util.List;
import java.util.Objects;

public class UserRoleNode extends UserEntity {
    private List<RoleEntity> list;

    public List<RoleEntity> getList() {
        return list;
    }

    public void setList(List<RoleEntity> list) {
        this.list = list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserRoleNode that = (UserRoleNode) o;
        return Objects.equals(list, that.list);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), list);
    }

    @Override
    public String toString() {
        return "UserRoleNode{" +
                "list=" + list +
                '}';
    }
}
