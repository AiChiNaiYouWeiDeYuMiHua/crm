package top.went.vo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.pojo.ProductFormatEntity;
import top.went.utils.StringUtils;

import java.util.List;

/**
 * 选择产品出库
 */
public class ChooseWarehouseVo {
    @JSONField(serialize = false)
    private ProductFormatEntity formatEntity;
    @JSONField(serialize = false)
    private Long productId;
    private Long noPay;
    private List<WareHouse> wareHouses;
    private String title;

    public String getTitle() {
        if (this.title == null && this.formatEntity != null)
            this.title = StringUtils.getStringBuffer(formatEntity).toString();
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ProductFormatEntity getFormatEntity() {
        return formatEntity;
    }

    public void setFormatEntity(ProductFormatEntity formatEntity) {
        this.formatEntity = formatEntity;
    }

    public Long getNoPay() {
        return noPay;
    }

    public void setNoPay(Long noPay) {
        this.noPay = noPay;
    }

    public List<WareHouse> getWareHouses() {
        return wareHouses;
    }

    public void setWareHouses(List<WareHouse> wareHouses) {
        this.wareHouses = wareHouses;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "ChooseWarehouseVo{" +
                "formatEntity=" + formatEntity +
                ", noPay=" + noPay +
                ", wareHouses=" + wareHouses +
                ", title='" + title + '\'' +
                '}';
    }
}
