package top.went.vo;

import top.went.pojo.ProductFormatEntity;
import top.went.pojo.PurDetailEntity;
import top.went.utils.StringUtils;

import java.math.BigDecimal;
import java.util.Date;

import static top.went.utils.StringUtils.getStringBuffer;

public class PurchaseDetailVO {
    public int getNoIn;
    private String title;
    private Integer pdId;
    private Long pdNum;
    private BigDecimal pdMoney;
    private Long magicDelete;
    private BigDecimal pdPrice;
    private BigDecimal pdTotal;
    private BigDecimal pdProfit;
    private Integer purId;
    private String purTheme;
    private String purState;
    private Date purDate;
    private String pdOther;
    private Page page;
    private Date from;
    private Date to;
    private Integer pfId;
    private String pfName;
    private String productName;
    private String productModalName;
    private String unit;
    private Integer pdWpNum;
    private Integer pdNoIn;
    private Integer userId;
    private Integer cusId;
    private Integer outWh;

    public PurchaseDetailVO() {
    }


    public PurchaseDetailVO(PurDetailEntity purDetailEntity) {
        if (purDetailEntity != null) {
            this.pdId = purDetailEntity.getPdId();
            this.pdNum = purDetailEntity.getPdNum() == null ? 0 : purDetailEntity.getPdNum();
            this.pdMoney = purDetailEntity.getPdMoney() == null ? BigDecimal.valueOf(0) : purDetailEntity.getPdMoney();
            this.magicDelete = purDetailEntity.getMagicDelete();
            this.pdPrice = purDetailEntity.getPdPrice() == null ? BigDecimal.valueOf(0) : purDetailEntity.getPdPrice();
            this.pdTotal = purDetailEntity.getPdTotal() == null ? BigDecimal.valueOf(0) : purDetailEntity.getPdTotal();
            this.pdProfit = purDetailEntity.getPdProfit() == null ? BigDecimal.valueOf(0) : purDetailEntity.getPdProfit();
            this.pdOther = purDetailEntity.getPdOther();
        }
        if (purDetailEntity.getTbPurchaseByPurId() != null) {
            this.purId = purDetailEntity.getTbPurchaseByPurId().getPurId();
            this.purTheme = purDetailEntity.getTbPurchaseByPurId().getPurTheme();
            this.purState = purDetailEntity.getTbPurchaseByPurId().getPurState();
            if (purDetailEntity.getTbPurchaseByPurId().getTbCustomerByCusId() != null)
                this.cusId = purDetailEntity.getTbPurchaseByPurId().getTbCustomerByCusId().getCusId();
            if (purDetailEntity.getTbPurchaseByPurId().getTbUserByUserId() != null)
                this.userId = Math.toIntExact(purDetailEntity.getTbPurchaseByPurId().getTbUserByUserId().getUserId());
        }
        if(purDetailEntity.getTbProductByProductId()!=null){
            this.productName=purDetailEntity.getTbProductByProductId().getProductName();
            this.productModalName=purDetailEntity.getTbProductByProductId().getProductModel();
        }
        if (purDetailEntity.getTbProductFormatByPfId() != null) {
            this.pfId = purDetailEntity.getTbProductFormatByPfId().getPfId();
            this.pfName = purDetailEntity.getTbProductFormatByPfId().getPfName();
            this.unit = purDetailEntity.getTbProductFormatByPfId().getPfUnit();
        }
        StringBuffer sb = getStringBuffer(productName, productModalName, pfName, unit);
        this.title = sb.toString();
    }

    public PurchaseDetailVO(PurDetailEntity purDetailEntity, int in) {
         this(purDetailEntity);
        this.pdWpNum=in;
        this.pdNoIn =Math.toIntExact(this.pdNum - in);

    }

    /**
     * 获取title
     *
     * @param productName
     * @param productModalName
     * @param formaterName
     * @param util
     * @return
     */
    private StringBuffer getStringBuffer(String productName, String productModalName, String formaterName, String util) {
        StringBuffer sb = new StringBuffer();
        if (productName != null && productName.length() > 0)
            sb.append(productName + "/");
        if (productModalName != null && productModalName.length() > 0)
            sb.append(productModalName + "/");
        if (formaterName != null && formaterName.length() > 0)
            sb.append(formaterName + "/");
        if (util != null && util.length() > 0)
            sb.append(productName + "/");
        return sb;
    }

    public String getTitle() {
        return this.title;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPdId() {
        return pdId;
    }

    public void setPdId(Integer pdId) {
        this.pdId = pdId;
    }

    public Long getPdNum() {
        return pdNum;
    }

    public void setPdNum(Long pdNum) {
        this.pdNum = pdNum;
    }

    public BigDecimal getPdMoney() {
        return pdMoney;
    }

    public void setPdMoney(BigDecimal pdMoney) {
        this.pdMoney = pdMoney;
    }

    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    public BigDecimal getPdPrice() {
        return pdPrice;
    }

    public void setPdPrice(BigDecimal pdPrice) {
        this.pdPrice = pdPrice;
    }

    public Integer getPurId() {
        return purId;
    }

    public void setPurId(Integer purId) {
        this.purId = purId;
    }

    public String getPurTheme() {
        return purTheme;
    }

    public void setPurTheme(String purTheme) {
        this.purTheme = purTheme;
    }

    public Page getPage() {
        if (page == null)
            page = new Page();
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Date getPurDate() {
        return purDate;
    }

    public void setPurDate(Date purDate) {
        this.purDate = purDate;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Integer getPfId() {
        return pfId;
    }

    public void setPfId(Integer pfId) {
        this.pfId = pfId;
    }

    public String getPfName() {
        return pfName;
    }

    public void setPfName(String pfName) {
        this.pfName = pfName;
    }

    public BigDecimal getPdTotal() {
        return pdTotal;
    }

    public void setPdTotal(BigDecimal pdTotal) {
        this.pdTotal = pdTotal;
    }

    public BigDecimal getPdProfit() {
        return pdProfit;
    }

    public void setPdProfit(BigDecimal pdProfit) {
        this.pdProfit = pdProfit;
    }

    public String getPdOther() {
        return pdOther;
    }

    public void setPdOther(String pdOther) {
        this.pdOther = pdOther;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductModalName() {
        return productModalName;
    }

    public void setProductModalName(String productModalName) {
        this.productModalName = productModalName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPurState() {
        return purState;
    }

    public void setPurState(String purState) {
        this.purState = purState;
    }

    public Integer getPdWpNum() {
        return pdWpNum;
    }

    public void setPdWpNum(Integer pdWpNum) {
        this.pdWpNum = pdWpNum;
    }

    public Integer getPdNoIn() {
        return pdNoIn;
    }

    public void setPdNoIn(Integer pdNoIn) {
        this.pdNoIn = pdNoIn;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public Integer getOutWh() {
        return outWh;
    }

    public void setOutWh(Integer outWh) {
        this.outWh = outWh;
    }

    public int getGetNoIn() {
        return getNoIn;
    }

    public void setGetNoIn(int getNoIn) {
        this.getNoIn = getNoIn;
    }
}
