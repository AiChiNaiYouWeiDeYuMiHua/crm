package top.went.vo;

import top.went.pojo.DemandEntity;

import java.sql.Date;
import java.util.List;

public class DemandVo {
    private DemandEntity demandEntity;
    private Date recordTimeFrom;
    private Date recordTimeTo;
    private Long[] importanceList;

    private String familyDa;
    private String kidDa;

    private String searchText;
    private Long searchTextType;

    private Page page;

    public DemandVo(){

    }
    public DemandVo(DemandSonVo demandSonVo){
        this.demandEntity = demandSonVo.getDemandEntity();
        this.recordTimeFrom = demandSonVo.getRecordTimeFrom();
        this.recordTimeTo = demandSonVo.getRecordTimeTo();
        this.familyDa = demandSonVo.getFamilyDa();
        this.kidDa = demandSonVo.getKidDa();
        this.searchText = demandSonVo.getSearchText();
//        this.searchTextType = demandSonVo.getSearchTextType();
        this.page = demandSonVo.getPage();
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getFamilyDa() {
        return familyDa;
    }

    public void setFamilyDa(String familyDa) {
        this.familyDa = familyDa;
    }

    public String getKidDa() {
        return kidDa;
    }

    public void setKidDa(String kidDa) {
        this.kidDa = kidDa;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Long getSearchTextType() {
        return searchTextType;
    }

    public void setSearchTextType(Long searchTextType) {
        this.searchTextType = searchTextType;
    }

    public Long[] getImportanceList() {
        return importanceList;
    }

    public void setImportanceList(Long[] importanceList) {
        this.importanceList = importanceList;
    }

    public DemandEntity getDemandEntity() {
        return demandEntity;
    }

    public void setDemandEntity(DemandEntity demandEntity) {
        this.demandEntity = demandEntity;
    }

    public Date getRecordTimeFrom() {
        return recordTimeFrom;
    }

    public void setRecordTimeFrom(Date recordTimeFrom) {
        this.recordTimeFrom = recordTimeFrom;
    }

    public Date getRecordTimeTo() {
        return recordTimeTo;
    }

    public void setRecordTimeTo(Date recordTimeTo) {
        this.recordTimeTo = recordTimeTo;
    }
}
