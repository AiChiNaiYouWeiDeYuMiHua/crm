package top.went.vo;

import top.went.pojo.WpDetailEntity;

import java.util.ArrayList;

/**
 * 出入库明细
 */
public class WarehouseDetailVo {
    private Long id;
    private ArrayList<WpDetailEntity> datas;
    private Boolean type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArrayList<WpDetailEntity> getDatas() {
        return datas;
    }

    public void setDatas(ArrayList<WpDetailEntity> datas) {
        this.datas = datas;
    }

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

}
