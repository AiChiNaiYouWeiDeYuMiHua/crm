package top.went.vo;

import top.went.pojo.RoleEntity;

import java.util.Objects;

public class UserRoleVo extends RoleEntity {
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserRoleVo that = (UserRoleVo) o;
        return selected == that.selected;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), selected);
    }

    @Override
    public String toString() {
        return "UserRoleVo{" +
                "selected=" + selected +
                '}';
    }
}
