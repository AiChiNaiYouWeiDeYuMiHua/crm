package top.went.vo;

import java.util.List;
import java.util.Objects;

public class UserIdAndRoleIds {
    private Long userId;
    private List<Long> roleIds;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserIdAndRoleIds that = (UserIdAndRoleIds) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(roleIds, that.roleIds);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, roleIds);
    }

    @Override
    public String toString() {
        return "UserIdAndRoleIds{" +
                "userId=" + userId +
                ", roleIds=" + roleIds +
                '}';
    }
}
