package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "project_expect", schema = "USER_CRM")
public class ProjectExpectEntity {
    private Integer pexpId;
    @JSONField(format = "yyyy-MM-dd")
    private Date pexpDate;
    private Integer pexpMoney;
    private String pexpPossibility;
    private ProjectEntity tbProjectByProjId;
    private Collection<ProjectEntity> tbProjectsByPexpId;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "pexp_id", nullable = false, precision = 0)
    public Integer getPexpId() {
        return pexpId;
    }

    public void setPexpId(Integer pexpId) {
        this.pexpId = pexpId;
    }

    @Basic
    @Column(name = "pexp_date", nullable = true)
    public Date getPexpDate() {
        return pexpDate;
    }

    public void setPexpDate(Date pexpDate) {
        this.pexpDate = pexpDate;
    }

    @Basic
    @Column(name = "pexp_money", nullable = true, precision = 0)
    public Integer getPexpMoney() {
        return pexpMoney;
    }

    public void setPexpMoney(Integer pexpMoney) {
        this.pexpMoney = pexpMoney;
    }

    @Basic
    @Column(name = "pexp_possibility", nullable = true, length = 20)
    public String getPexpPossibility() {
        return pexpPossibility;
    }

    public void setPexpPossibility(String pexpPossibility) {
        this.pexpPossibility = pexpPossibility;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectExpectEntity that = (ProjectExpectEntity) o;
        return Objects.equals(pexpId, that.pexpId) &&
                Objects.equals(pexpDate, that.pexpDate) &&
                Objects.equals(pexpMoney, that.pexpMoney) &&
                Objects.equals(pexpPossibility, that.pexpPossibility);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pexpId, pexpDate, pexpMoney, pexpPossibility);
    }

    @ManyToOne
    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id")
    public ProjectEntity getTbProjectByProjId() {
        return tbProjectByProjId;
    }

    public void setTbProjectByProjId(ProjectEntity tbProjectByProjId) {
        this.tbProjectByProjId = tbProjectByProjId;
    }

    @OneToMany(mappedBy = "projectExpectByPexpId")
    public Collection<ProjectEntity> getTbProjectsByPexpId() {
        return tbProjectsByPexpId;
    }

    public void setTbProjectsByPexpId(Collection<ProjectEntity> tbProjectsByPexpId) {
        this.tbProjectsByPexpId = tbProjectsByPexpId;
    }

    @Override
    public String toString() {
        return "ProjectExpectEntity{" +
                "pexpId=" + pexpId +
                ", pexpDate=" + pexpDate +
                ", pexpMoney=" + pexpMoney +
                ", pexpPossibility='" + pexpPossibility + '\'' +
                ", tbProjectByProjId=" + tbProjectByProjId +
                ", tbProjectsByPexpId=" + tbProjectsByPexpId +
                '}';
    }
}
