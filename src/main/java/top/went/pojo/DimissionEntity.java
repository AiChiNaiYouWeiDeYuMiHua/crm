package top.went.pojo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_dimission", schema = "USER_CRM", catalog = "")
public class DimissionEntity {
    private Long dimissionId;
    private String dimissionName;
    private Long dimissionIsDel;
    private DimissionTypeEntity tbDimissionTypeByDimiTypeId;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    @Column(name = "dimission_id", nullable = false, precision = 0)
    public Long getDimissionId() {
        return dimissionId;
    }

    public void setDimissionId(Long dimissionId) {
        this.dimissionId = dimissionId;
    }

    @Basic
    @Column(name = "dimission_name", nullable = true, length = 20)
    public String getDimissionName() {
        return dimissionName;
    }

    public void setDimissionName(String dimissionName) {
        this.dimissionName = dimissionName;
    }

    @Basic
    @Column(name = "dimission_is_del", nullable = true, precision = 0)
    public Long getDimissionIsDel() {
        return dimissionIsDel;
    }

    public void setDimissionIsDel(Long dimissionIsDel) {
        this.dimissionIsDel = dimissionIsDel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DimissionEntity that = (DimissionEntity) o;
        return Objects.equals(dimissionId, that.dimissionId) &&
                Objects.equals(dimissionName, that.dimissionName) &&
                Objects.equals(dimissionIsDel, that.dimissionIsDel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dimissionId, dimissionName, dimissionIsDel);
    }

    @ManyToOne
    @JoinColumn(name = "dimi_type_id", referencedColumnName = "dimi_type_id")
    public DimissionTypeEntity getTbDimissionTypeByDimiTypeId() {
        return tbDimissionTypeByDimiTypeId;
    }

    public void setTbDimissionTypeByDimiTypeId(DimissionTypeEntity tbDimissionTypeByDimiTypeId) {
        this.tbDimissionTypeByDimiTypeId = tbDimissionTypeByDimiTypeId;
    }

    @Override
    public String toString() {
        return "DimissionEntity{" +
                "dimissionId=" + dimissionId +
                ", dimissionName='" + dimissionName + '\'' +
                ", dimissionIsDel=" + dimissionIsDel +
                ", tbDimissionTypeByDimiTypeId=" + tbDimissionTypeByDimiTypeId +
                '}';
    }
}
