package top.went.pojo;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "tb_return_goods_detail", schema = "USER_CRM", catalog = "")
public class ReturnGoodsDetailEntity {
    private Integer rgdId;
    private String rgdReason;
    private Long rgdNum;
    private Double rgdMoney;
    private String rgdRemarks;
    private Integer rgdOutwhNum;
    private Long magicDelete;
    private ReturnGoodsEntity tbReturnGoodsByRgId;
    private PurDetailEntity purDetailEntity;
    private ProductFormatEntity tbProductFormatByPfId;
    private ProductEntity tbProductByProductId;

    public ReturnGoodsDetailEntity() {
    }

    public ReturnGoodsDetailEntity(Long rgdNum, Double rgdMoney, Integer rgdOutwhNum,
                                   Long magicDelete, ReturnGoodsEntity tbReturnGoodsByRgId, ProductFormatEntity tbProductFormatByPfId) {
        this.rgdNum = rgdNum;
        this.rgdMoney = rgdMoney;
        this.rgdOutwhNum = rgdOutwhNum;
        this.magicDelete = magicDelete;
        this.tbReturnGoodsByRgId = tbReturnGoodsByRgId;
        this.tbProductFormatByPfId = tbProductFormatByPfId;
    }

    @Id
    @Column(name = "rgd_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getRgdId() {
        return rgdId;
    }

    public void setRgdId(Integer rgdId) {
        this.rgdId = rgdId;
    }

    @Basic
    @Column(name = "rgd_reason", nullable = true, length = 100)
    public String getRgdReason() {
        return rgdReason;
    }

    public void setRgdReason(String rgdReason) {
        this.rgdReason = rgdReason;
    }

    @Basic
    @Column(name = "rgd_num", nullable = true, precision = 0)
    public Long getRgdNum() {
        return rgdNum;
    }

    public void setRgdNum(Long rgdNum) {
        this.rgdNum = rgdNum;
    }

    @Basic
    @Column(name = "rgd_remarks", nullable = true, length = 100)
    public String getRgdRemarks() {
        return rgdRemarks;
    }

    public void setRgdRemarks(String rgdRemarks) {
        this.rgdRemarks = rgdRemarks;
    }

    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReturnGoodsDetailEntity that = (ReturnGoodsDetailEntity) o;
        return Objects.equals(rgdId, that.rgdId) &&
                Objects.equals(rgdReason, that.rgdReason) &&
                Objects.equals(rgdNum, that.rgdNum) &&
                Objects.equals(rgdRemarks, that.rgdRemarks) &&
                Objects.equals(magicDelete, that.magicDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rgdId, rgdReason, rgdNum, rgdRemarks, magicDelete);
    }

    @ManyToOne
    @JoinColumn(name = "rg_id", referencedColumnName = "rg_id")
    public ReturnGoodsEntity getTbReturnGoodsByRgId() {
        return tbReturnGoodsByRgId;
    }

    public void setTbReturnGoodsByRgId(ReturnGoodsEntity tbReturnGoodsByRgId) {
        this.tbReturnGoodsByRgId = tbReturnGoodsByRgId;
    }

    @ManyToOne
    @JoinColumn(name = "pd_id", referencedColumnName = "pd_id")
    public PurDetailEntity getPurDetailEntity() {
        return purDetailEntity;
    }
    @ManyToOne
    @JoinColumn(name = "pf_id", referencedColumnName = "pf_id")
    public ProductFormatEntity getTbProductFormatByPfId() {
        return tbProductFormatByPfId;
    }

    public void setTbProductFormatByPfId(ProductFormatEntity tbProductFormatByPfId) {
        this.tbProductFormatByPfId = tbProductFormatByPfId;
    }

    public void setPurDetailEntity(PurDetailEntity purDetailEntity) {
        this.purDetailEntity = purDetailEntity;
    }
    @Basic
    @Column(name = "rgd_money", nullable = true, precision = 0)
    public Double getRgdMoney() {
        return rgdMoney;
    }

    public void setRgdMoney(Double rgdMoney) {
        this.rgdMoney = rgdMoney;
    }

    @Basic
    @Column(name = "rgd_outwh_num", nullable = true, precision = 0)
    public Integer getRgdOutwhNum() {
        return rgdOutwhNum;
    }

    public void setRgdOutwhNum(Integer rgdOutwhNum) {
        this.rgdOutwhNum = rgdOutwhNum;
    }
    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    public ProductEntity getTbProductByProductId() {
        return tbProductByProductId;
    }

    public void setTbProductByProductId(ProductEntity tbProductByProductId) {
        this.tbProductByProductId = tbProductByProductId;
    }
}
