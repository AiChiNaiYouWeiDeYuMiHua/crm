package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_complaint", schema = "USER_CRM")
public class ComplaintEntity {
    private Integer complainId;
    private String complainTheme;
    private String complainType;
    private String complainBewrite;
    @JSONField(format = "yyyy-MM-dd")
    private Date complainDate;
    private String complainEmergency;
    private String complainProcess;
    private String complainResults;
    private String complainTimeSpent;
    private String compainBackComfimed;
    private String compainRemark;
    private Integer complainIsDelete;
    private String complainBack;
    private ContactsEntity tbContactsByCotsId;
    private UserEntity tbUserByUserId;
    private CustomerEntity tbCustomerByCusId;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "complain_id", nullable = false, precision = 0)
    public Integer getComplainId() {
        return complainId;
    }

    public void setComplainId(Integer complainId) {
        this.complainId = complainId;
    }

    @Basic
    @Column(name = "complain_theme", nullable = true, length = 20)
    public String getComplainTheme() {
        return complainTheme;
    }

    public void setComplainTheme(String complainTheme) {
        this.complainTheme = complainTheme;
    }

    @Basic
    @Column(name = "complain_type", nullable = true, length = 20)
    public String getComplainType() {
        return complainType;
    }

    public void setComplainType(String complainType) {
        this.complainType = complainType;
    }

    @Basic
    @Column(name = "complain_bewrite", nullable = true, length = 2000)
    public String getComplainBewrite() {
        return complainBewrite;
    }

    public void setComplainBewrite(String complainBewrite) {
        this.complainBewrite = complainBewrite;
    }

    @Basic
    @Column(name = "complain_date", nullable = true)
    public Date getComplainDate() {
        return complainDate;
    }

    public void setComplainDate(Date complainDate) {
        this.complainDate = complainDate;
    }

    @Basic
    @Column(name = "complain_emergency", nullable = true, length = 10)
    public String getComplainEmergency() {
        return complainEmergency;
    }

    public void setComplainEmergency(String complainEmergency) {
        this.complainEmergency = complainEmergency;
    }

    @Basic
    @Column(name = "complain_process", nullable = true, length = 500)
    public String getComplainProcess() {
        return complainProcess;
    }

    public void setComplainProcess(String complainProcess) {
        this.complainProcess = complainProcess;
    }

    @Basic
    @Column(name = "complain_results", nullable = true, length = 20)
    public String getComplainResults() {
        return complainResults;
    }

    public void setComplainResults(String complainResults) {
        this.complainResults = complainResults;
    }

    @Basic
    @Column(name = "complain_time_spent", nullable = true)
    public String getComplainTimeSpent() {
        return complainTimeSpent;
    }

    public void setComplainTimeSpent(String complainTimeSpent) {
        this.complainTimeSpent = complainTimeSpent;
    }

    @Basic
    @Column(name = "compain_back_comfimed", nullable = true, length = 500)
    public String getCompainBackComfimed() {
        return compainBackComfimed;
    }

    public void setCompainBackComfimed(String compainBackComfimed) {
        this.compainBackComfimed = compainBackComfimed;
    }

    @Basic
    @Column(name = "compain_remark", nullable = true, length = 2000)
    public String getCompainRemark() {
        return compainRemark;
    }

    public void setCompainRemark(String compainRemark) {
        this.compainRemark = compainRemark;
    }

    @Basic
    @Column(name = "complain_is_delete", nullable = true)
    public Integer getComplainIsDelete() {
        return complainIsDelete;
    }

    public void setComplainIsDelete(Integer complainIsDelete) {
        this.complainIsDelete = complainIsDelete;
    }

    @Basic
    @Column(name = "complain_back", nullable = true, length = 200)
    public String getComplainBack() {
        return complainBack;
    }

    public void setComplainBack(String complainBack) {
        this.complainBack = complainBack;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComplaintEntity that = (ComplaintEntity) o;
        return Objects.equals(complainId, that.complainId) &&
                Objects.equals(complainTheme, that.complainTheme) &&
                Objects.equals(complainType, that.complainType) &&
                Objects.equals(complainBewrite, that.complainBewrite) &&
                Objects.equals(complainDate, that.complainDate) &&
                Objects.equals(complainEmergency, that.complainEmergency) &&
                Objects.equals(complainProcess, that.complainProcess) &&
                Objects.equals(complainResults, that.complainResults) &&
                Objects.equals(complainTimeSpent, that.complainTimeSpent) &&
                Objects.equals(compainBackComfimed, that.compainBackComfimed) &&
                Objects.equals(compainRemark, that.compainRemark);
    }

    @Override
    public String toString() {
        return "ComplaintEntity{" +
                "complainId=" + complainId +
                ", complainTheme='" + complainTheme + '\'' +
                ", complainType='" + complainType + '\'' +
                ", complainBewrite='" + complainBewrite + '\'' +
                ", complainDate=" + complainDate +
                ", complainEmergency='" + complainEmergency + '\'' +
                ", complainProcess='" + complainProcess + '\'' +
                ", complainResults='" + complainResults + '\'' +
                ", complainTimeSpent='" + complainTimeSpent + '\'' +
                ", complainBackComfimed='" + compainBackComfimed + '\'' +
                ", complainRemark='" + compainRemark + '\'' +
                ", complainIsDelete=" + complainIsDelete +
                ", complainBack='" + complainBack + '\'' +
                ", tbContactsByCotsId=" + tbContactsByCotsId +
                ", tbUserByUserId=" + tbUserByUserId +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(complainId, complainTheme, complainType, complainBewrite, complainDate, complainEmergency, complainProcess, complainResults, complainTimeSpent, compainBackComfimed, compainRemark);
    }

    @ManyToOne
    @JoinColumn(name = "cots_id", referencedColumnName = "cots_id")
    public ContactsEntity getTbContactsByCotsId() {
        return tbContactsByCotsId;
    }

    public void setTbContactsByCotsId(ContactsEntity tbContactsByCotsId) {
        this.tbContactsByCotsId = tbContactsByCotsId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }
}
