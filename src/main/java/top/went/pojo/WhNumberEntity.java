package top.went.pojo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_wh_number", schema = "USER_CRM", catalog = "")
public class WhNumberEntity {
    private Integer wnId;
    private Long wnNumber;
    private ProductFormatEntity tbProductFormatByPfId;
    private WarehouseEntity tbWarehouseByWhId;

    public WhNumberEntity() {
    }

    public WhNumberEntity(Long wnNumber, ProductFormatEntity tbProductFormatByPfId, WarehouseEntity tbWarehouseByWhId) {
        this.wnNumber = wnNumber;
        this.tbProductFormatByPfId = tbProductFormatByPfId;
        this.tbWarehouseByWhId = tbWarehouseByWhId;
    }

    @Id
    @Column(name = "wn_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getWnId() {
        return wnId;
    }

    public void setWnId(Integer wnId) {
        this.wnId = wnId;
    }

    @Basic
    @Column(name = "wn_number", nullable = true, precision = 0)
    public Long getWnNumber() {
        return wnNumber;
    }

    public void setWnNumber(Long wnNumber) {
        this.wnNumber = wnNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WhNumberEntity that = (WhNumberEntity) o;
        return Objects.equals(wnId, that.wnId) &&
                Objects.equals(wnNumber, that.wnNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wnId, wnNumber);
    }

    @ManyToOne
    @JoinColumn(name = "pf_id", referencedColumnName = "pf_id")
    public ProductFormatEntity getTbProductFormatByPfId() {
        return tbProductFormatByPfId;
    }

    public void setTbProductFormatByPfId(ProductFormatEntity tbProductFormatByPfId) {
        this.tbProductFormatByPfId = tbProductFormatByPfId;
    }

    @ManyToOne
    @JoinColumn(name = "wh_id", referencedColumnName = "wh_id")
    public WarehouseEntity getTbWarehouseByWhId() {
        return tbWarehouseByWhId;
    }

    public void setTbWarehouseByWhId(WarehouseEntity tbWarehouseByWhId) {
        this.tbWarehouseByWhId = tbWarehouseByWhId;
    }

    @Override
    public String toString() {
        return "WhNumberEntity{" +
                "wnId=" + wnId +
                ", wnNumber=" + wnNumber +
                ", tbProductFormatByPfId=" + tbProductFormatByPfId +
                ", tbWarehouseByWhId=" + tbWarehouseByWhId +
                '}';
    }
}
