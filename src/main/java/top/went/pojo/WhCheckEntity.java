package top.went.pojo;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_wh_check", schema = "USER_CRM", catalog = "")
public class WhCheckEntity {
    private Long wcId;
    private Time wcDate;
    private Boolean wcStatus;
    private String wcOther;
    private Boolean logicDelete;
    private UserEntity tbUserByUserId;
    private WarehouseEntity tbWarehouseByWhId;

    @Id
    @Column(name = "wc_id", nullable = false, precision = 0)
    public Long getWcId() {
        return wcId;
    }

    public void setWcId(Long wcId) {
        this.wcId = wcId;
    }

    @Basic
    @Column(name = "wc_date", nullable = true)
    public Time getWcDate() {
        return wcDate;
    }

    public void setWcDate(Time wcDate) {
        this.wcDate = wcDate;
    }

    @Basic
    @Column(name = "wc_status", nullable = true, precision = 0)
    public Boolean getWcStatus() {
        return wcStatus;
    }

    public void setWcStatus(Boolean wcStatus) {
        this.wcStatus = wcStatus;
    }

    @Basic
    @Column(name = "wc_other", nullable = true, length = 100)
    public String getWcOther() {
        return wcOther;
    }

    public void setWcOther(String wcOther) {
        this.wcOther = wcOther;
    }

    @Basic
    @Column(name = "logic_delete", nullable = true, precision = 0)
    public Boolean getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Boolean logicDelete) {
        this.logicDelete = logicDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WhCheckEntity that = (WhCheckEntity) o;
        return Objects.equals(wcId, that.wcId) &&
                Objects.equals(wcDate, that.wcDate) &&
                Objects.equals(wcStatus, that.wcStatus) &&
                Objects.equals(wcOther, that.wcOther) &&
                Objects.equals(logicDelete, that.logicDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wcId, wcDate, wcStatus, wcOther, logicDelete);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "wh_id", referencedColumnName = "wh_id")
    public WarehouseEntity getTbWarehouseByWhId() {
        return tbWarehouseByWhId;
    }

    public void setTbWarehouseByWhId(WarehouseEntity tbWarehouseByWhId) {
        this.tbWarehouseByWhId = tbWarehouseByWhId;
    }
}
