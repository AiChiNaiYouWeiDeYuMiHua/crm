package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "tb_receipt_records", schema = "USER_CRM", catalog = "")
public class ReceiptRecordsEntity {
    private Integer rrId;
    private String rrContent;
    private String rrType;
    private Double rrMoney;
    private Long rrNumber;
    @JSONField(format = "yyyy-MM-dd")
    private Date rrDate;
    private Long rrDelete;
    private PayBackDetailEntity tbPayBackDetailsByRrId;
    private CustomerEntity tbCustomerByCusId;
    private UserEntity tbUserByUserId;
    private OrderEntity tbOrderByOrderId;

    @Id
    @Column(name = "rr_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getRrId() {
        return rrId;
    }

    public void setRrId(Integer rrId) {
        this.rrId = rrId;
    }

    @Basic
    @Column(name = "rr_content", nullable = true, length = 100)
    public String getRrContent() {
        return rrContent;
    }

    public void setRrContent(String rrContent) {
        this.rrContent = rrContent;
    }

    @Basic
    @Column(name = "rr_type", nullable = true, length = 20)
    public String getRrType() {
        return rrType;
    }

    public void setRrType(String rrType) {
        this.rrType = rrType;
    }

    @Basic
    @Column(name = "rr_money", nullable = true, precision = 2)
    public Double getRrMoney() {
        return rrMoney;
    }

    public void setRrMoney(Double rrMonry) {
        this.rrMoney = rrMonry;
    }

    @Basic
    @Column(name = "rr_number", nullable = true, precision = 0)
    public Long getRrNumber() {
        return rrNumber;
    }

    public void setRrNumber(Long rrNumber) {
        this.rrNumber = rrNumber;
    }

    @Basic
    @Column(name = "rr_date", nullable = true)
    public Date getRrDate() {
        return rrDate;
    }

    public void setRrDate(Date rrDate) {
        this.rrDate = rrDate;
    }

    @Basic
    @Column(name = "rr_delete", nullable = true, precision = 0)
    public Long getRrDelete() {
        return rrDelete;
    }

    public void setRrDelete(Long rrDelete) {
        this.rrDelete = rrDelete;
    }



    @ManyToOne
    @JoinColumn(name = "pbd_id", referencedColumnName = "pbd_id")
    public PayBackDetailEntity getTbPayBackDetailsByRrId() {
        return tbPayBackDetailsByRrId;
    }

    public void setTbPayBackDetailsByRrId(PayBackDetailEntity tbPayBackDetailsByRrId) {
        this.tbPayBackDetailsByRrId = tbPayBackDetailsByRrId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    public OrderEntity getTbOrderByOrderId() {
        return tbOrderByOrderId;
    }

    public void setTbOrderByOrderId(OrderEntity tbOrderByOrderId) {
        this.tbOrderByOrderId = tbOrderByOrderId;
    }
}
