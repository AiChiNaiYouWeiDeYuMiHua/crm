package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_contacts", schema = "USER_CRM")
public class ContactsEntity {
    private Integer cotsId;
    private String cotsName;
    private String cotsPhoto;
    private String cotsSex;
    private String cotsClassification;
    private String cotsBusiness;
    private String cotsProject;
    private String cotsTechnical;
    private String cotsType;
    private String cotsDepartment;
    private Long cotsWorkphone;
    private String cotsPlace;
    private Long cotsPersonalPhone;
    private String cotsMsnQq;
    private String cotsFax;
    private String cotsZipcode;
    private String cotsWechart;
    private String cotsInterest;
    private String cotsCardtype;
    private String cotsIdnum;
    private Date cotsCreateDate;
    private Date cotsModifyDate;
    private String cotsHabits;
    private String cotsCharacter;
    private String cotsSocialCharacteristics;
    private Long cotsAnnualIncome;
    private String cotsConsumptionHabits;
    private Integer cotsIsDelete;
    private String cotsRemark;
    private String cotsHome;
    private String cotsUpcots;
//    private Collection<CareEntity> tbCaresByCotsId;
    private Collection<ComplaintEntity> tbComplaintsByCotsId;
    private CustomerEntity tbCustomerByCusId;
    private ServeEntity tbServeByServeId;
//    private Collection<MemorialDayEntity> tbMemorialDaysByCotsId;
//    private Collection<ProjectEntity> projectEntities;
//    private Collection<QuoteEntity> tbQuotesByCotsId;
//    private Collection<SaleOppEntity> tbSaleOppsByCotsId;
//    private Collection<TaskActionEntity> tbTaskActionsByCotsId;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "cots_id", nullable = false, precision = 0)
    public Integer getCotsId() {
        return cotsId;
    }

    public void setCotsId(Integer cotsId) {
        this.cotsId = cotsId;
    }

    @Basic
    @Column(name = "cots_name", nullable = false, length = 10)
    public String getCotsName() {
        return cotsName;
    }

    public void setCotsName(String cotsName) {
        this.cotsName = cotsName;
    }

    @Basic
    @Column(name = "cots_photo", nullable = true, length = 100)
    public String getCotsPhoto() {
        return cotsPhoto;
    }

    public void setCotsPhoto(String cotsPhoto) {
        this.cotsPhoto = cotsPhoto;
    }

    @Basic
    @Column(name = "cots_sex", nullable = true, length = 2)
    public String getCotsSex() {
        return cotsSex;
    }

    public void setCotsSex(String cotsSex) {
        this.cotsSex = cotsSex;
    }

    @Basic
    @Column(name = "cots_classification", nullable = true, length = 10)
    public String getCotsClassification() {
        return cotsClassification;
    }

    public void setCotsClassification(String cotsClassification) {
        this.cotsClassification = cotsClassification;
    }

    @Basic
    @Column(name = "cots_business", nullable = true, length = 20)
    public String getCotsBusiness() {
        return cotsBusiness;
    }

    public void setCotsBusiness(String cotsBusiness) {
        this.cotsBusiness = cotsBusiness;
    }

    @Basic
    @Column(name = "cots_project", nullable = true, length = 20)
    public String getCotsProject() {
        return cotsProject;
    }

    public void setCotsProject(String cotsProject) {
        this.cotsProject = cotsProject;
    }

    @Basic
    @Column(name = "cots_technical", nullable = true, length = 20)
    public String getCotsTechnical() {
        return cotsTechnical;
    }

    public void setCotsTechnical(String cotsTechnical) {
        this.cotsTechnical = cotsTechnical;
    }

    @Basic
    @Column(name = "cots_type", nullable = true, length = 20)
    public String getCotsType() {
        return cotsType;
    }

    public void setCotsType(String cotsType) {
        this.cotsType = cotsType;
    }

    @Basic
    @Column(name = "cots_department", nullable = true, length = 20)
    public String getCotsDepartment() {
        return cotsDepartment;
    }

    public void setCotsDepartment(String cotsDepartment) {
        this.cotsDepartment = cotsDepartment;
    }

    @Basic
    @Column(name = "cots_workphone", nullable = true, precision = 0)
    public Long getCotsWorkphone() {
        return cotsWorkphone;
    }

    public void setCotsWorkphone(Long cotsWorkphone) {
        this.cotsWorkphone = cotsWorkphone;
    }

    @Basic
    @Column(name = "cots_place", nullable = true, length = 50)
    public String getCotsPlace() {
        return cotsPlace;
    }

    public void setCotsPlace(String cotsPlace) {
        this.cotsPlace = cotsPlace;
    }

    @Basic
    @Column(name = "cots_personal_phone", nullable = true, precision = 0)
    public Long getCotsPersonalPhone() {
        return cotsPersonalPhone;
    }

    public void setCotsPersonalPhone(Long cotsPersonalPhone) {
        this.cotsPersonalPhone = cotsPersonalPhone;
    }

    @Basic
    @Column(name = "cots_MSNQQ", nullable = true, length = 20)
    public String getCotsMsnQq() {
        return cotsMsnQq;
    }

    public void setCotsMsnQq(String cotsMsnQq) {
        this.cotsMsnQq = cotsMsnQq;
    }

    @Basic
    @Column(name = "cots_fax", nullable = true, length = 20)
    public String getCotsFax() {
        return cotsFax;
    }

    public void setCotsFax(String cotsFax) {
        this.cotsFax = cotsFax;
    }

    @Basic
    @Column(name = "cots_zipcode", nullable = true, length = 50)
    public String getCotsZipcode() {
        return cotsZipcode;
    }

    public void setCotsZipcode(String cotsZipcode) {
        this.cotsZipcode = cotsZipcode;
    }

    @Basic
    @Column(name = "cots_wechart", nullable = true, length = 25)
    public String getCotsWechart() {
        return cotsWechart;
    }

    public void setCotsWechart(String cotsWechart) {
        this.cotsWechart = cotsWechart;
    }

    @Basic
    @Column(name = "cots_interest", nullable = true, length = 100)
    public String getCotsInterest() {
        return cotsInterest;
    }

    public void setCotsInterest(String cotsInterest) {
        this.cotsInterest = cotsInterest;
    }

    @Basic
    @Column(name = "cots_cardtype", nullable = true, length = 20)
    public String getCotsCardtype() {
        return cotsCardtype;
    }

    public void setCotsCardtype(String cotsCardtype) {
        this.cotsCardtype = cotsCardtype;
    }

    @Basic
    @Column(name = "cots_idnum", nullable = true, length = 25)
    public String getCotsIdnum() {
        return cotsIdnum;
    }

    public void setCotsIdnum(String cotsIdnum) {
        this.cotsIdnum = cotsIdnum;
    }

    @Basic
    @Column(name = "cots_create_date", nullable = true)
    public Date getCotsCreateDate() {
        return cotsCreateDate;
    }

    public void setCotsCreateDate(Date cotsCreateDate) {
        this.cotsCreateDate = cotsCreateDate;
    }

    @Basic
    @Column(name = "cots_modify_date", nullable = true)
    public Date getCotsModifyDate() {
        return cotsModifyDate;
    }

    public void setCotsModifyDate(Date cotsModifyDate) {
        this.cotsModifyDate = cotsModifyDate;
    }

    @Basic
    @Column(name = "cots_habits", nullable = true, length = 100)
    public String getCotsHabits() {
        return cotsHabits;
    }

    public void setCotsHabits(String cotsHabits) {
        this.cotsHabits = cotsHabits;
    }

    @Basic
    @Column(name = "cots_character", nullable = true, length = 50)
    public String getCotsCharacter() {
        return cotsCharacter;
    }

    public void setCotsCharacter(String cotsCharacter) {
        this.cotsCharacter = cotsCharacter;
    }

    @Basic
    @Column(name = "cots_social_characteristics", nullable = true, length = 100)
    public String getCotsSocialCharacteristics() {
        return cotsSocialCharacteristics;
    }

    public void setCotsSocialCharacteristics(String cotsSocialCharacteristics) {
        this.cotsSocialCharacteristics = cotsSocialCharacteristics;
    }

    @Basic
    @Column(name = "cots_annual_income", nullable = true, precision = 0)
    public Long getCotsAnnualIncome() {
        return cotsAnnualIncome;
    }

    public void setCotsAnnualIncome(Long cotsAnnualIncome) {
        this.cotsAnnualIncome = cotsAnnualIncome;
    }

    @Basic
    @Column(name = "cots_consumption_habits", nullable = true, length = 50)
    public String getCotsConsumptionHabits() {
        return cotsConsumptionHabits;
    }

    public void setCotsConsumptionHabits(String cotsConsumptionHabits) {
        this.cotsConsumptionHabits = cotsConsumptionHabits;
    }

    @Basic
    @Column(name = "cots_is_delete", nullable = true, precision = 0)
    public Integer getCotsIsDelete() {
        return cotsIsDelete;
    }

    public void setCotsIsDelete(Integer cotsIsDelete) {
        this.cotsIsDelete = cotsIsDelete;
    }

    @Basic
    @Column(name = "cots_remark", nullable = true)
    public String getCotsRemark() {
        return cotsRemark;
    }

    public void setCotsRemark(String cotsRemark) {
        this.cotsRemark = cotsRemark;
    }

    @Basic
    @Column(name = "cots_home", nullable = true)
    public String getCotsHome() {
        return cotsHome;
    }

    public void setCotsHome(String cotsHome) {
        this.cotsHome = cotsHome;
    }

    @Basic
    @Column(name = "cots_upcots", nullable = true)
    public String getCotsUpcots() {
        return cotsUpcots;
    }

    public void setCotsUpcots(String cotsUpcots) {
        this.cotsUpcots = cotsUpcots;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactsEntity that = (ContactsEntity) o;
        return Objects.equals(cotsId, that.cotsId) &&
                Objects.equals(cotsName, that.cotsName) &&
                Objects.equals(cotsPhoto, that.cotsPhoto) &&
                Objects.equals(cotsSex, that.cotsSex) &&
                Objects.equals(cotsClassification, that.cotsClassification) &&
                Objects.equals(cotsBusiness, that.cotsBusiness) &&
                Objects.equals(cotsProject, that.cotsProject) &&
                Objects.equals(cotsTechnical, that.cotsTechnical) &&
                Objects.equals(cotsType, that.cotsType) &&
                Objects.equals(cotsDepartment, that.cotsDepartment) &&
                Objects.equals(cotsWorkphone, that.cotsWorkphone) &&
                Objects.equals(cotsPlace, that.cotsPlace) &&
                Objects.equals(cotsPersonalPhone, that.cotsPersonalPhone) &&
                Objects.equals(cotsMsnQq, that.cotsMsnQq) &&
                Objects.equals(cotsFax, that.cotsFax) &&
                Objects.equals(cotsZipcode, that.cotsZipcode) &&
                Objects.equals(cotsWechart, that.cotsWechart) &&
                Objects.equals(cotsInterest, that.cotsInterest) &&
                Objects.equals(cotsCardtype, that.cotsCardtype) &&
                Objects.equals(cotsIdnum, that.cotsIdnum) &&
                Objects.equals(cotsCreateDate, that.cotsCreateDate) &&
                Objects.equals(cotsModifyDate, that.cotsModifyDate) &&
                Objects.equals(cotsHabits, that.cotsHabits) &&
                Objects.equals(cotsCharacter, that.cotsCharacter) &&
                Objects.equals(cotsSocialCharacteristics, that.cotsSocialCharacteristics) &&
                Objects.equals(cotsAnnualIncome, that.cotsAnnualIncome) &&
                Objects.equals(cotsConsumptionHabits, that.cotsConsumptionHabits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cotsId, cotsName, cotsPhoto, cotsSex, cotsClassification, cotsBusiness, cotsProject, cotsTechnical, cotsType, cotsDepartment, cotsWorkphone, cotsPlace, cotsPersonalPhone, cotsMsnQq, cotsFax, cotsZipcode, cotsWechart, cotsInterest, cotsCardtype, cotsIdnum, cotsCreateDate, cotsModifyDate, cotsHabits, cotsCharacter, cotsSocialCharacteristics, cotsAnnualIncome, cotsConsumptionHabits);
    }

    @Override
    public String toString() {
        return "ContactsEntity{" +
                "cotsId=" + cotsId +
                ", cotsName='" + cotsName + '\'' +
                ", cotsPhoto='" + cotsPhoto + '\'' +
                ", cotsSex='" + cotsSex + '\'' +
                ", cotsClassification='" + cotsClassification + '\'' +
                ", cotsBusiness='" + cotsBusiness + '\'' +
                ", cotsProject='" + cotsProject + '\'' +
                ", cotsTechnical='" + cotsTechnical + '\'' +
                ", cotsType='" + cotsType + '\'' +
                ", cotsDepartment='" + cotsDepartment + '\'' +
                ", cotsWorkphone=" + cotsWorkphone +
                ", cotsPlace='" + cotsPlace + '\'' +
                ", cotsPersonalPhone=" + cotsPersonalPhone +
                ", cotsMsnQq='" + cotsMsnQq + '\'' +
                ", cotsFax='" + cotsFax + '\'' +
                ", cotsZipcode='" + cotsZipcode + '\'' +
                ", cotsWechart='" + cotsWechart + '\'' +
                ", cotsInterest='" + cotsInterest + '\'' +
                ", cotsCardtype='" + cotsCardtype + '\'' +
                ", cotsIdnum='" + cotsIdnum + '\'' +
                ", cotsCreateDate=" + cotsCreateDate +
                ", cotsModifyDate=" + cotsModifyDate +
                ", cotsHabits='" + cotsHabits + '\'' +
                ", cotsCharacter='" + cotsCharacter + '\'' +
                ", cotsSocialCharacteristics='" + cotsSocialCharacteristics + '\'' +
                ", cotsAnnualIncome=" + cotsAnnualIncome +
                ", cotsConsumptionHabits='" + cotsConsumptionHabits + '\'' +
                ", cotsIsDelete=" + cotsIsDelete +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbServeByServeId=" + tbServeByServeId +
                '}';
    }

    //    @OneToMany(mappedBy = "tbContactsByCotsId")
//    public Collection<CareEntity> getTbCaresByCotsId() {
//        return tbCaresByCotsId;
//    }
//
//    public void setTbCaresByCotsId(Collection<CareEntity> tbCaresByCotsId) {
//        this.tbCaresByCotsId = tbCaresByCotsId;
//    }
//
//    @OneToMany(mappedBy = "tbContactsByCotsId")
//    public Collection<ComplaintEntity> getTbComplaintsByCotsId() {
//        return tbComplaintsByCotsId;
//    }
//
//    public void setTbComplaintsByCotsId(Collection<ComplaintEntity> tbComplaintsByCotsId) {
//        this.tbComplaintsByCotsId = tbComplaintsByCotsId;
//    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "serve_id", referencedColumnName = "serve_id")
    public ServeEntity getTbServeByServeId() {
        return tbServeByServeId;
    }

    public void setTbServeByServeId(ServeEntity tbServeByServeId) {
        this.tbServeByServeId = tbServeByServeId;
    }

//    @OneToMany(mappedBy = "tbContactsByCotsId")
//    public Collection<MemorialDayEntity> getTbMemorialDaysByCotsId() {
//        return tbMemorialDaysByCotsId;
//    }
//
//    public void setTbMemorialDaysByCotsId(Collection<MemorialDayEntity> tbMemorialDaysByCotsId) {
//        this.tbMemorialDaysByCotsId = tbMemorialDaysByCotsId;
//    }

//    @ManyToMany(mappedBy = "contactsEntities")
//    public Collection<ProjectEntity> getProjectEntities() {
//        return projectEntities;
//    }
//
//    public void setProjectEntities(Collection<ProjectEntity> projectEntities) {
//        this.projectEntities = projectEntities;
//    }
//
//    @OneToMany(mappedBy = "tbContactsBy接收人Id")
//    public Collection<QuoteEntity> getTbQuotesByCotsId() {
//        return tbQuotesByCotsId;
//    }
//
//    public void setTbQuotesByCotsId(Collection<QuoteEntity> tbQuotesByCotsId) {
//        this.tbQuotesByCotsId = tbQuotesByCotsId;
//    }
//
//    @OneToMany(mappedBy = "tbContactsBy联系人Id")
//    public Collection<SaleOppEntity> getTbSaleOppsByCotsId() {
//        return tbSaleOppsByCotsId;
//    }
//
//    public void setTbSaleOppsByCotsId(Collection<SaleOppEntity> tbSaleOppsByCotsId) {
//        this.tbSaleOppsByCotsId = tbSaleOppsByCotsId;
//    }
//
//    @OneToMany(mappedBy = "tbContactsBy联系人Id")
//    public Collection<TaskActionEntity> getTbTaskActionsByCotsId() {
//        return tbTaskActionsByCotsId;
//    }
//
//    public void setTbTaskActionsByCotsId(Collection<TaskActionEntity> tbTaskActionsByCotsId) {
//        this.tbTaskActionsByCotsId = tbTaskActionsByCotsId;
//    }
}
