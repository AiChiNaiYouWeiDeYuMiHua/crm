package top.went.pojo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "role_fc", schema = "USER_CRM", catalog = "")
public class RoleFcEntity {

    @Id
    @Column(name = "role_fc_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    private Long roleFcId;
    @Basic
    @Column(name = "role_fc_is_del", nullable = true, length = 1)
    private Long roleFcIsDel;
    @ManyToOne
    @JoinColumn(name = "fc_id", referencedColumnName = "fc_id")
    private FunctionsEntity tbFunctionsByFcId;
    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    private RoleEntity tbRoleByRoleId;

    public Long getRoleFcId() {
        return roleFcId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleFcEntity that = (RoleFcEntity) o;
        return Objects.equals(roleFcId, that.roleFcId) &&
                Objects.equals(roleFcIsDel, that.roleFcIsDel) &&
                Objects.equals(tbFunctionsByFcId, that.tbFunctionsByFcId) &&
                Objects.equals(tbRoleByRoleId, that.tbRoleByRoleId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(roleFcId, roleFcIsDel, tbFunctionsByFcId, tbRoleByRoleId);
    }

    public Long getRoleFcIsDel() {

        return roleFcIsDel;
    }

    public void setRoleFcIsDel(Long roleFcIsDel) {
        this.roleFcIsDel = roleFcIsDel;
    }

    public void setRoleFcId(Long roleFcId) {
        this.roleFcId = roleFcId;
    }

    public FunctionsEntity getTbFunctionsByFcId() {
        return tbFunctionsByFcId;
    }

    public void setTbFunctionsByFcId(FunctionsEntity tbFunctionsByFcId) {
        this.tbFunctionsByFcId = tbFunctionsByFcId;
    }

    public RoleEntity getTbRoleByRoleId() {
        return tbRoleByRoleId;
    }

    public void setTbRoleByRoleId(RoleEntity tbRoleByRoleId) {
        this.tbRoleByRoleId = tbRoleByRoleId;
    }

    @Override
    public String toString() {
        return "RoleFcEntity{" +
                "roleFcId=" + roleFcId +
                ", roleFcIsDel=" + roleFcIsDel +
                ", tbFunctionsByFcId=" + tbFunctionsByFcId +
                ", tbRoleByRoleId=" + tbRoleByRoleId +
                '}';
    }
}
