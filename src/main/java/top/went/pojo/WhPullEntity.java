package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_wh_pull", schema = "USER_CRM", catalog = "")
public class WhPullEntity {
    private Integer wpullId;
    private String wpullName;
    private Date wpullDate;
    private Integer wpullStatus;
    private Date wpullExecute;
    private String wpullOther;
    private Boolean logicDelete;
    private Collection<DeliverGoodsEntity> tbDeliverGoodsByWpullId;
    private OrderEntity tbOrderByOrderId;
    private UserEntity tbUserByUserId;
    private UserEntity tbUserByTbUserId;
    private WarehouseEntity tbWarehouseByWhId;
    private ReturnGoodsEntity tbReturnGoodsByRgId;

    public WhPullEntity() {
    }

    public WhPullEntity(String wpullName, Date wpullDate, Integer wpullStatus, String wpullOther, Boolean logicDelete,
                        OrderEntity tbOrderByOrderId, ReturnGoodsEntity returnGoodsEntity,UserEntity tbUserByUserId, WarehouseEntity tbWarehouseByWhId) {
        this.wpullName = wpullName;
        this.wpullDate = wpullDate;
        this.wpullStatus = wpullStatus;
        this.wpullOther = wpullOther;
        this.logicDelete = logicDelete;
        this.tbOrderByOrderId = tbOrderByOrderId;
        this.tbReturnGoodsByRgId = returnGoodsEntity;
        this.tbUserByUserId = tbUserByUserId;
        this.tbWarehouseByWhId = tbWarehouseByWhId;
    }

    @Id
    @Column(name = "wpull_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getWpullId() {
        return wpullId;
    }

    public void setWpullId(Integer wpullId) {
        this.wpullId = wpullId;
    }

    @Basic
    @Column(name = "wpull_name", nullable = true, length = 30)
    public String getWpullName() {
        return wpullName;
    }

    public void setWpullName(String wpullName) {
        this.wpullName = wpullName;
    }

    @Basic
    @Column(name = "wpull_date", nullable = true)
    public Date getWpullDate() {
        return wpullDate;
    }

    public void setWpullDate(Date wpullDate) {
        this.wpullDate = wpullDate;
    }

    @Basic
    @Column(name = "wpull_status", nullable = true, precision = 0)
    public Integer getWpullStatus() {
        return wpullStatus;
    }

    public void setWpullStatus(Integer wpullStatus) {
        this.wpullStatus = wpullStatus;
    }

    @Basic
    @Column(name = "wpull_execute", nullable = true)
    public Date getWpullExecute() {
        return wpullExecute;
    }

    public void setWpullExecute(Date wpullExecute) {
        this.wpullExecute = wpullExecute;
    }

    @Basic
    @Column(name = "wpull_other", nullable = true, length = 100)
    public String getWpullOther() {
        return wpullOther;
    }

    public void setWpullOther(String wpullOther) {
        this.wpullOther = wpullOther;
    }

    @Basic
    @Column(name = "logic_delete", nullable = true, precision = 0)
    public Boolean getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Boolean logicDelete) {
        this.logicDelete = logicDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WhPullEntity that = (WhPullEntity) o;
        return Objects.equals(wpullId, that.wpullId) &&
                Objects.equals(wpullName, that.wpullName) &&
                Objects.equals(wpullDate, that.wpullDate) &&
                Objects.equals(wpullStatus, that.wpullStatus) &&
                Objects.equals(wpullExecute, that.wpullExecute) &&
                Objects.equals(wpullOther, that.wpullOther) &&
                Objects.equals(logicDelete, that.logicDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wpullId, wpullName, wpullDate, wpullStatus, wpullExecute, wpullOther, logicDelete);
    }

    @OneToMany(mappedBy = "tbWhPullByWpullId")
    public Collection<DeliverGoodsEntity> getTbDeliverGoodsByWpullId() {
        return tbDeliverGoodsByWpullId;
    }

    public void setTbDeliverGoodsByWpullId(Collection<DeliverGoodsEntity> tbDeliverGoodsByWpullId) {
        this.tbDeliverGoodsByWpullId = tbDeliverGoodsByWpullId;
    }

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    public OrderEntity getTbOrderByOrderId() {
        return tbOrderByOrderId;
    }

    public void setTbOrderByOrderId(OrderEntity tbOrderByOrderId) {
        this.tbOrderByOrderId = tbOrderByOrderId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "tb__user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByTbUserId() {
        return tbUserByTbUserId;
    }

    public void setTbUserByTbUserId(UserEntity tbUserByTbUserId) {
        this.tbUserByTbUserId = tbUserByTbUserId;
    }

    @ManyToOne
    @JoinColumn(name = "wh_id", referencedColumnName = "wh_id")
    public WarehouseEntity getTbWarehouseByWhId() {
        return tbWarehouseByWhId;
    }

    public void setTbWarehouseByWhId(WarehouseEntity tbWarehouseByWhId) {
        this.tbWarehouseByWhId = tbWarehouseByWhId;
    }

    @ManyToOne
    @JoinColumn(name = "rg_id", referencedColumnName = "rg_id")
    public ReturnGoodsEntity getTbReturnGoodsByRgId() {
        return tbReturnGoodsByRgId;
    }

    public void setTbReturnGoodsByRgId(ReturnGoodsEntity tbReturnGoodsByRgId) {
        this.tbReturnGoodsByRgId = tbReturnGoodsByRgId;
    }
}
