package top.went.pojo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "TB_APPROVAL_LOGS", schema = "USER_CRM", catalog = "")
public class ApprovalLogsEntity {
    private Integer logsId;
    private Timestamp logsTime;
    private String logsContent;
    private UserEntity tbUserByUserId;
    private ApprovalEntity tbApprovalByApprovalId;
    private String category;

    public ApprovalLogsEntity() {
    }

    public ApprovalLogsEntity(Timestamp logsTime, String logsContent, UserEntity tbUserByUserId, ApprovalEntity tbApprovalByApprovalId) {
        this.logsTime = logsTime;
        this.logsContent = logsContent;
        this.tbUserByUserId = tbUserByUserId;
        this.tbApprovalByApprovalId = tbApprovalByApprovalId;
    }

    @Basic
    @Id
    @Column(name = "LOGS_ID", nullable = true, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getLogsId() {
        return logsId;
    }

    public void setLogsId(Integer logsId) {
        this.logsId = logsId;
    }

    @Basic
    @Column(name = "LOGS_TIME", nullable = true)
    public Timestamp getLogsTime() {
        return logsTime;
    }

    public void setLogsTime(Timestamp logsTime) {
        this.logsTime = logsTime;
    }

    @Basic
    @Column(name = "LOGS_CONTENT", nullable = true, precision = 0)
    public String getLogsContent() {
        return logsContent;
    }

    public void setLogsContent(String  logsContent) {
        this.logsContent = logsContent;
    }

    @Basic
    @Column(name = "logs_category", nullable = true, precision = 0)
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApprovalLogsEntity that = (ApprovalLogsEntity) o;
        return Objects.equals(logsId, that.logsId) &&
                Objects.equals(logsTime, that.logsTime) &&
                Objects.equals(logsContent, that.logsContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(logsId, logsTime, logsContent);
    }

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "APPROVAL_ID", referencedColumnName = "APPROVAL_ID")
    public ApprovalEntity getTbApprovalByApprovalId() {
        return tbApprovalByApprovalId;
    }

    public void setTbApprovalByApprovalId(ApprovalEntity tbApprovalByApprovalId) {
        this.tbApprovalByApprovalId = tbApprovalByApprovalId;
    }
}
