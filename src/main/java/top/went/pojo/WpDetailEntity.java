package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_wp_detail", schema = "USER_CRM", catalog = "")
public class WpDetailEntity {
    private Long wdId;
    private Integer wpId;
    private Long outNumber;
    private Long inNumber;
    private Long planNumber;
    @JSONField(format = "yyyy-MM-dd")
    private Date wdExecute;
    private Integer wdState;
    private Integer wdType;
    private String wdOther;
    private BigDecimal wdPrice;
    private BigDecimal wdTotal;
    private BigDecimal wdCost;
    @JSONField(serialize = false)
    private ProductFormatEntity tbProductFormatByPfId;
    @JSONField(serialize = false)
    private WarehouseEntity tbWarehouseByWhId;
    @JSONField(serialize = false)
    private UserEntity tbUserByUserId;
    @JSONField(serialize = false)
    private CustomerEntity tbCustomerByCusId;

    public WpDetailEntity() {
    }

    public WpDetailEntity(Integer wpId, Long outNumber, Long planNumber, Integer wdState, Integer wdType,
                          String wdOther, BigDecimal wdPrice, BigDecimal wdTotal, BigDecimal wdCost,
                          ProductFormatEntity tbProductFormatByPfId, WarehouseEntity tbWarehouseByWhId,
                          UserEntity tbUserByUserId, CustomerEntity tbCustomerByCusId) {
        this.wpId = wpId;
        this.outNumber = outNumber;
        this.planNumber = planNumber;
        this.wdState = wdState;
        this.wdType = wdType;
        this.wdOther = wdOther;
        this.wdPrice = wdPrice;
        this.wdTotal = wdTotal;
        this.wdCost = wdCost;
        this.tbProductFormatByPfId = tbProductFormatByPfId;
        this.tbWarehouseByWhId = tbWarehouseByWhId;
        this.tbUserByUserId = tbUserByUserId;
        this.tbCustomerByCusId = tbCustomerByCusId;
    }
    public WpDetailEntity(Integer wpId, int inNUmber, int planNumber, Integer wdState, Integer wdType,
                          String wdOther, BigDecimal wdPrice, BigDecimal wdTotal, BigDecimal wdCost,
                          Integer tbProductFormatByPfId, WarehouseEntity tbWarehouseByWhId,
                          UserEntity tbUserByUserId, CustomerEntity tbCustomerByCusId) {
        this.wpId = wpId;
        this.inNumber = Long.valueOf(inNUmber);
        this.planNumber = Long.valueOf(planNumber);
        this.wdState = wdState;
        this.wdType = wdType;
        this.wdOther = wdOther;
        this.wdPrice = wdPrice;
        this.wdTotal = wdTotal;
        this.wdCost = wdCost;
        this.tbProductFormatByPfId = new ProductFormatEntity();
        this.tbProductFormatByPfId.setPfId(tbProductFormatByPfId);
        this.tbWarehouseByWhId = tbWarehouseByWhId;
        this.tbUserByUserId = tbUserByUserId;
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @Id
    @Column(name = "wd_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Long getWdId() {
        return wdId;
    }

    public void setWdId(Long wdId) {
        this.wdId = wdId;
    }

    @Basic
    @Column(name = "wp_id", nullable = true, precision = 0)
    public Integer getWpId() {
        return wpId;
    }

    public void setWpId(Integer wpId) {
        this.wpId = wpId;
    }

    @Basic
    @Column(name = "out_number", nullable = true, precision = 0)
    public Long getOutNumber() {
        return outNumber;
    }

    public void setOutNumber(Long outNumber) {
        this.outNumber = outNumber;
    }

    @Basic
    @Column(name = "in_number", nullable = true, precision = 0)
    public Long getInNumber() {
        return inNumber;
    }

    public void setInNumber(Long inNumber) {
        this.inNumber = inNumber;
    }


    @Basic
    @Column(name = "wd_price")
    public BigDecimal getWdPrice() {
        return wdPrice;
    }

    public void setWdPrice(BigDecimal wdPrice) {
        this.wdPrice = wdPrice;
    }

    @Basic
    @Column(name = "wd_total")
    public BigDecimal getWdTotal() {
        return wdTotal;
    }

    public void setWdTotal(BigDecimal wdTotal) {
        this.wdTotal = wdTotal;
    }

    @Basic
    @Column(name = "wd_cost")
    public BigDecimal getWdCost() {
        return wdCost;
    }

    public void setWdCost(BigDecimal wdCost) {
        this.wdCost = wdCost;
    }

    @Basic
    @Column(name = "plan_number", nullable = true, precision = 0)
    public Long getPlanNumber() {
        return planNumber;
    }

    public void setPlanNumber(Long planNumber) {
        this.planNumber = planNumber;
    }

    @Basic
    @Column(name = "wd_execute", nullable = true)
    public Date getWdExecute() {
        return wdExecute;
    }

    public void setWdExecute(Date wdExecute) {
        this.wdExecute = wdExecute;
    }

    @Basic
    @Column(name = "wd_state", nullable = true, precision = 0)
    public Integer getWdState() {
        return wdState;
    }

    public void setWdState(Integer wdState) {
        this.wdState = wdState;
    }

    @Basic
    @Column(name = "wd_type", nullable = true, precision = 0)
    public Integer getWdType() {
        return wdType;
    }

    public void setWdType(Integer wdType) {
        this.wdType = wdType;
    }

    @Basic
    @Column(name = "wd_other", nullable = true, length = 100)
    public String getWdOther() {
        return wdOther;
    }

    public void setWdOther(String wdOther) {
        this.wdOther = wdOther;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WpDetailEntity that = (WpDetailEntity) o;
        return Objects.equals(wdId, that.wdId) &&
                Objects.equals(wpId, that.wpId) &&
                Objects.equals(outNumber, that.outNumber) &&
                Objects.equals(inNumber, that.inNumber) &&
                Objects.equals(wdExecute, that.wdExecute) &&
                Objects.equals(wdState, that.wdState) &&
                Objects.equals(wdType, that.wdType) &&
                Objects.equals(wdOther, that.wdOther);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wdId, wpId, outNumber, inNumber, wdExecute, wdState, wdType, wdOther);
    }

    @ManyToOne
    @JoinColumn(name = "pf_id", referencedColumnName = "pf_id")
    public ProductFormatEntity getTbProductFormatByPfId() {
        return tbProductFormatByPfId;
    }

    public void setTbProductFormatByPfId(ProductFormatEntity tbProductFormatByPfId) {
        this.tbProductFormatByPfId = tbProductFormatByPfId;
    }

    @ManyToOne
    @JoinColumn(name = "wh_id", referencedColumnName = "wh_id")
    public WarehouseEntity getTbWarehouseByWhId() {
        return tbWarehouseByWhId;
    }

    public void setTbWarehouseByWhId(WarehouseEntity tbWarehouseByWhId) {
        this.tbWarehouseByWhId = tbWarehouseByWhId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }
}
