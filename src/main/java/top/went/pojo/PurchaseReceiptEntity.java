package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_purchase_receipt", schema = "USER_CRM", catalog = "")
public class PurchaseReceiptEntity {
    private Integer purrId;
    private String purrType;
    private String purrIsPay;
    private Long magicDelete;
    @JSONField(format = "yyyy-MM-dd")
    private Date purrDate;
    private  Long purrTerms;
    private String purrTheme;
    private Double purrMoney;
    private UserEntity tbUserByUserId;
   private PaymentRecordsEntity tbPaymentRecordsByPrId;
    private CustomerEntity tbCustomerByCusId;
    private PurchaseEntity tbPurchaseByPurId;

    @Id
    @Column(name = "purr_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getPurrId() {
        return purrId;
    }

    public void setPurrId(Integer purrId) {
        this.purrId = purrId;
    }




    @Basic
    @Column(name = "purr_type", nullable = true, length = 20)
    public String getPurrType() {
        return purrType;
    }

    public void setPurrType(String purrType) {
        this.purrType = purrType;
    }

    @Basic
    @Column(name = "purr_is_pay", nullable = true, length = 20)
    public String getPurrIsPay() {
        return purrIsPay;
    }

    public void setPurrIsPay(String purrIsPay) {
        this.purrIsPay = purrIsPay;
    }

    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    @Basic
    @Column(name = "purr_theme", nullable = true, precision = 0)
    public String getPurrTheme() {
        return purrTheme;
    }

    public void setPurrTheme(String purrTheme) {
        this.purrTheme = purrTheme;
    }

    @Basic
    @Column(name = "purr_date", nullable = true, precision = 0)
    public Date getPurrDate() {
        return purrDate;
    }

    public void setPurrDate(Date purrDate) {
        this.purrDate = purrDate;
    }


    @Override
    public int hashCode() {
        return Objects.hash(purrId, purrType, purrIsPay, magicDelete);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }



    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "pur_id", referencedColumnName = "pur_id")
    public PurchaseEntity getTbPurchaseByPurId() {
        return tbPurchaseByPurId;
    }

    public void setTbPurchaseByPurId(PurchaseEntity tbPurchaseByPurId) {
        this.tbPurchaseByPurId = tbPurchaseByPurId;
    }

    @ManyToOne
    @JoinColumn(name = "pr_id", referencedColumnName = "pr_id")
    public PaymentRecordsEntity getTbPaymentRecordsByPrId() {
        return tbPaymentRecordsByPrId;
    }

    public void setTbPaymentRecordsByPrId(PaymentRecordsEntity tbPaymentRecordsByPrId) {
        this.tbPaymentRecordsByPrId = tbPaymentRecordsByPrId;
    }
    @Basic
    @Column(name = "purr_money", nullable = true, precision = 0)
    public Double getPurrMoney() {
        return purrMoney;
    }

    public void setPurrMoney(Double purrMoney) {
        this.purrMoney = purrMoney;
    }
    @Basic
    @Column(name = "purr_terms", nullable = true, precision = 0)
    public Long getPurrTerms() {
        return purrTerms;
    }

    public void setPurrTerms(Long purrTerms) {
        this.purrTerms = purrTerms;
    }

    @Override
    public String toString() {
        return "PurchaseReceiptEntity{" +
                "purrId=" + purrId +
                ", purrType='" + purrType + '\'' +
                ", purrIsPay='" + purrIsPay + '\'' +
                ", magicDelete=" + magicDelete +
                ", purrDate=" + purrDate +
                ", purrTheme='" + purrTheme + '\'' +
                ", tbUserByUserId=" + tbUserByUserId +
                ", tbPaymentRecordsByPrId=" + tbPaymentRecordsByPrId +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbPurchaseByPurId=" + tbPurchaseByPurId +
                '}';
    }
}
