package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_serve", schema = "USER_CRM")
public class ServeEntity {
    private Integer serveId;
    private String serveTheme;
    private String serveType;
    private String serveWay;
    @JSONField(format = "yyyy-MM-dd")
    private Date serveStartTime;
    private String serveTimeSpent;
    private String serveState;
    private String serveContent;
    private String serveBack;
    private String serveRemarks;
    private String serveContact;
    private Integer serveIsDelete;
//    private Collection<ContactsEntity> tbContactsByServeId;
//    private Collection<CustomerEntity> tbCustomersByServeId;
    private CustomerEntity tbCustomerByCusId;
    private UserEntity tbUserByUserId;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "serve_id", nullable = false, precision = 0)
    public Integer getServeId() {
        return serveId;
    }

    public void setServeId(Integer serveId) {
        this.serveId = serveId;
    }

    @Basic
    @Column(name = "serve_theme", nullable = true, length = 100)
    public String getServeTheme() {
        return serveTheme;
    }

    public void setServeTheme(String serveTheme) {
        this.serveTheme = serveTheme;
    }

    @Basic
    @Column(name = "serve_type", nullable = true, length = 30)
    public String getServeType() {
        return serveType;
    }

    public void setServeType(String serveType) {
        this.serveType = serveType;
    }

    @Basic
    @Column(name = "serve_way", nullable = true, length = 30)
    public String getServeWay() {
        return serveWay;
    }

    public void setServeWay(String serveWay) {
        this.serveWay = serveWay;
    }

    @Basic
    @Column(name = "serve_start_time", nullable = true)
    public Date getServeStartTime() {
        return serveStartTime;
    }

    public void setServeStartTime(Date serveStartTime) {
        this.serveStartTime = serveStartTime;
    }

    @Basic
    @Column(name = "serve_time_spent", nullable = true)
    public String getServeTimeSpent() {
        return serveTimeSpent;
    }

    public void setServeTimeSpent(String serveTimeSpent) {
        this.serveTimeSpent = serveTimeSpent;
    }

    @Basic
    @Column(name = "serve_state", nullable = true, length = 10)
    public String getServeState() {
        return serveState;
    }

    public void setServeState(String serveState) {
        this.serveState = serveState;
    }

    @Basic
    @Column(name = "serve_content", nullable = true, length = 500)
    public String getServeContent() {
        return serveContent;
    }

    public void setServeContent(String serveContent) {
        this.serveContent = serveContent;
    }

    @Basic
    @Column(name = "serve_back", nullable = true, length = 500)
    public String getServeBack() {
        return serveBack;
    }

    public void setServeBack(String serveBack) {
        this.serveBack = serveBack;
    }

    @Basic
    @Column(name = "serve_remarks", nullable = true, length = 1000)
    public String getServeRemarks() {
        return serveRemarks;
    }

    public void setServeRemarks(String serveRemarks) {
        this.serveRemarks = serveRemarks;
    }

    @Basic
    @Column(name = "serve_contact", nullable = true, length = 15)
    public String getServeContact() {
        return serveContact;
    }

    public void setServeContact(String serveContact) {
        this.serveContact = serveContact;
    }

    @Basic
    @Column(name = "serve_is_delete", nullable = true, length = 15)
    public Integer getServeIsDelete() {
        return serveIsDelete;
    }

    public void setServeIsDelete(Integer serveIsDelete) {
        this.serveIsDelete = serveIsDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServeEntity that = (ServeEntity) o;
        return Objects.equals(serveId, that.serveId) &&
                Objects.equals(serveTheme, that.serveTheme) &&
                Objects.equals(serveType, that.serveType) &&
                Objects.equals(serveWay, that.serveWay) &&
                Objects.equals(serveStartTime, that.serveStartTime) &&
                Objects.equals(serveTimeSpent, that.serveTimeSpent) &&
                Objects.equals(serveState, that.serveState) &&
                Objects.equals(serveContent, that.serveContent) &&
                Objects.equals(serveBack, that.serveBack) &&
                Objects.equals(serveRemarks, that.serveRemarks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serveId, serveTheme, serveType, serveWay, serveStartTime, serveTimeSpent, serveState, serveContent, serveBack, serveRemarks);
    }
//
//    @OneToMany(mappedBy = "tbServeByServeId")
//    public Collection<ContactsEntity> getTbContactsByServeId() {
//        return tbContactsByServeId;
//    }
//
//    public void setTbContactsByServeId(Collection<ContactsEntity> tbContactsByServeId) {
//        this.tbContactsByServeId = tbContactsByServeId;
//    }
//
//    @OneToMany(mappedBy = "tbServeByServeId")
//    public Collection<CustomerEntity> getTbCustomersByServeId() {
//        return tbCustomersByServeId;
//    }
//
//    public void setTbCustomersByServeId(Collection<CustomerEntity> tbCustomersByServeId) {
//        this.tbCustomersByServeId = tbCustomersByServeId;
//    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @Override
    public String toString() {
        return "ServeEntity{" +
                "serveId=" + serveId +
                ", serveTheme='" + serveTheme + '\'' +
                ", serveType='" + serveType + '\'' +
                ", serveWay='" + serveWay + '\'' +
                ", serveStartTime=" + serveStartTime +
                ", serveTimeSpent='" + serveTimeSpent + '\'' +
                ", serveState='" + serveState + '\'' +
                ", serveContent='" + serveContent + '\'' +
                ", serveBack='" + serveBack + '\'' +
                ", serveRemarks='" + serveRemarks + '\'' +
                ", serveContact='" + serveContact + '\'' +
                ", serveIsDelete=" + serveIsDelete +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbUserByUserId=" + tbUserByUserId +
                '}';
    }
}
