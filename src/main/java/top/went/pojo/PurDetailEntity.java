package top.went.pojo;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "tb_pur_detail", schema = "USER_CRM", catalog = "")
public class PurDetailEntity {
    private Integer pdId;
    private String pdTitle;
    private Long pdNum;
    private BigDecimal pdMoney;
    private Long magicDelete;
    private BigDecimal pdPrice;
    private BigDecimal pdTotal;
    private BigDecimal pdProfit;
    private String pdOther;
    private Integer pdWpNum;
    private Integer noIn;
    private Integer outWh;
    private PurchaseEntity tbPurchaseByPurId;
    private ProductFormatEntity tbProductFormatByPfId;
    private ProductEntity tbProductByProductId;

    @Id
    @Column(name = "pd_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getPdId() {
        return pdId;
    }

    public void setPdId(Integer pdId) {
        this.pdId = pdId;
    }


    @Basic
    @Column(name = "pd_num", nullable = true, precision = 0)
    public Long getPdNum() {
        return pdNum;
    }

    public void setPdNum(Long pdNum) {
        this.pdNum = pdNum;
    }

    @Basic
    @Column(name = "pd_money", nullable = true, precision = 2)
    public BigDecimal getPdMoney() {
        return pdMoney;
    }

    public void setPdMoney(BigDecimal pdMoney) {
        this.pdMoney = pdMoney;
    }

    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    @Basic
    @Column(name = "pd_price", nullable = true, precision = 2)
    public BigDecimal getPdPrice() {
        return pdPrice;
    }

    public void setPdPrice(BigDecimal pdPrice) {
        this.pdPrice = pdPrice;
    }

    @Basic
    @Column(name = "pd_total", nullable = true, precision = 2)
    public BigDecimal getPdTotal() {
        return pdTotal;
    }

    public void setPdTotal(BigDecimal pdTotal) {
        this.pdTotal = pdTotal;
    }
    @Basic
    @Column(name = "pd_profit", nullable = true, precision = 2)
    public BigDecimal getPdProfit() {
        return pdProfit;
    }

    public void setPdProfit(BigDecimal pdProfit) {
        this.pdProfit = pdProfit;
    }
    @Basic
    @Column(name = "pd_other", nullable = true, length = 100)
    public String getPdOther() {
        return pdOther;
    }

    public void setPdOther(String pdOther) {
        this.pdOther = pdOther;
    }


    @ManyToOne
    @JoinColumn(name = "pur_id", referencedColumnName = "pur_id")
    public PurchaseEntity getTbPurchaseByPurId() {
        return tbPurchaseByPurId;
    }

    public void setTbPurchaseByPurId(PurchaseEntity tbPurchaseByPurId) {
        this.tbPurchaseByPurId = tbPurchaseByPurId;
    }
    @Basic
    @Column(name = "no_in", nullable = true, precision = 2)
    public Integer getNoIn() {
        return noIn;
    }

    public void setNoIn(Integer noIn) {
        this.noIn = noIn;
    }
    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    public ProductEntity getTbProductByProductId() {
        return tbProductByProductId;
    }

    public void setTbProductByProductId(ProductEntity tbProductByProductId) {
        this.tbProductByProductId = tbProductByProductId;
    }

    @ManyToOne
    @JoinColumn(name = "pf_id", referencedColumnName = "pf_id")
    public ProductFormatEntity getTbProductFormatByPfId() {
        return tbProductFormatByPfId;
    }

    public void setTbProductFormatByPfId(ProductFormatEntity tbProductFormatByPfId) {
        this.tbProductFormatByPfId = tbProductFormatByPfId;
    }
    @Basic
    @Column(name = "pd_title", nullable = true, precision = 2)
    public String getPdTitle() {
        return pdTitle;
    }

    public void setPdTitle(String pdTitle) {
        this.pdTitle = pdTitle;
    }
    @Basic
    @Column(name = "pd_wp_num", nullable = true, precision = 2)
    public Integer getPdWpNum() {
        return pdWpNum==null?0:pdWpNum;
    }

    public void setPdWpNum(Integer pdWpNum) {
        this.pdWpNum = pdWpNum;
    }

    @Basic
    @Column(name = "out_wh", nullable = true, precision = 2)
    public Integer getOutWh() {
        return outWh==null?0:outWh;
    }

    public void setOutWh(Integer outWh) {
        this.outWh = outWh;
    }
}
