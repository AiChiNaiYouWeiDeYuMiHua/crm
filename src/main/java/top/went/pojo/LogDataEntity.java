package top.went.pojo;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_log_data", schema = "USER_CRM", catalog = "")
public class LogDataEntity {
    private Integer logDataId;
    private String logDataTable;
    private String logDataOld;
    private String logDataNew;
    private Time logDataTime;
    private UserEntity tbUserByUserId;

    @Id
    @Column(name = "log_data_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    public Integer getLogDataId() {
        return logDataId;
    }

    public void setLogDataId(Integer logDataId) {
        this.logDataId = logDataId;
    }

    @Basic
    @Column(name = "log_data_table", nullable = true, length = 30)
    public String getLogDataTable() {
        return logDataTable;
    }

    public void setLogDataTable(String logDataTable) {
        this.logDataTable = logDataTable;
    }

    @Basic
    @Column(name = "log_data_old", nullable = true, length = 500)
    public String getLogDataOld() {
        return logDataOld;
    }

    public void setLogDataOld(String logDataOld) {
        this.logDataOld = logDataOld;
    }

    @Basic
    @Column(name = "log_data_new", nullable = true, length = 500)
    public String getLogDataNew() {
        return logDataNew;
    }

    public void setLogDataNew(String logDataNew) {
        this.logDataNew = logDataNew;
    }

    @Basic
    @Column(name = "log_data_time", nullable = true)
    public Time getLogDataTime() {
        return logDataTime;
    }

    public void setLogDataTime(Time logDataTime) {
        this.logDataTime = logDataTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LogDataEntity that = (LogDataEntity) o;
        return Objects.equals(logDataId, that.logDataId) &&
                Objects.equals(logDataTable, that.logDataTable) &&
                Objects.equals(logDataOld, that.logDataOld) &&
                Objects.equals(logDataNew, that.logDataNew) &&
                Objects.equals(logDataTime, that.logDataTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(logDataId, logDataTable, logDataOld, logDataNew, logDataTime);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }
}
