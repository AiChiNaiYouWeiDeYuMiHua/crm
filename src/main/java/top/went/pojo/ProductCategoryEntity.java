package top.went.pojo;



import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_product_category", schema = "USER_CRM", catalog = "")
public class ProductCategoryEntity {
    private Long pcId;
    private String pcName;
    private Boolean logicDelete;
    private Collection<ProductEntity> tbProductsByPcId;
    private ProductCategoryEntity tbProductCategoryByTbPcId;

    private Collection<ProductCategoryEntity> tbProductCategoriesByPcId;

    public ProductCategoryEntity() {
    }

    public ProductCategoryEntity(Long pcId) {
        this.pcId = pcId;
        this.logicDelete = false;
    }

    public ProductCategoryEntity(String pcName, ProductCategoryEntity tbProductCategoryByTbPcId) {
        this.pcName = pcName;
        this.tbProductCategoryByTbPcId = tbProductCategoryByTbPcId;
        this.logicDelete = false;
    }

    public ProductCategoryEntity(Long pcId, String pcName) {
        this.pcId = pcId;
        this.pcName = pcName;
        this.logicDelete = false;
    }

    @Id
    @Column(name = "pc_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Long getPcId() {
        return pcId;
    }

    public void setPcId(Long pcId) {
        this.pcId = pcId;
    }

    @Basic
    @Column(name = "pc_name", nullable = true, length = 20)
    public String getPcName() {
        return pcName;
    }

    public void setPcName(String pcName) {
        this.pcName = pcName;
    }

    @Basic
    @Column(name = "LOGIC_DELETE", nullable = true, precision = 0)
    public Boolean getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Boolean logicDelete) {
        this.logicDelete = logicDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductCategoryEntity that = (ProductCategoryEntity) o;
        return Objects.equals(pcId, that.pcId) &&
                Objects.equals(pcName, that.pcName) &&
                Objects.equals(logicDelete, that.logicDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pcId, pcName, logicDelete);
    }

    @OneToMany(mappedBy = "tbProductCategoryByPcId", fetch = FetchType.EAGER)
    public Collection<ProductEntity> getTbProductsByPcId() {
        return tbProductsByPcId;
    }

    public void setTbProductsByPcId(Collection<ProductEntity> tbProductsByPcId) {
        this.tbProductsByPcId = tbProductsByPcId;
    }

    @ManyToOne
    @JoinColumn(name = "tb__pc_id", referencedColumnName = "pc_id")
    public ProductCategoryEntity getTbProductCategoryByTbPcId() {
        return tbProductCategoryByTbPcId;
    }

    public void setTbProductCategoryByTbPcId(ProductCategoryEntity tbProductCategoryByTbPcId) {
        this.tbProductCategoryByTbPcId = tbProductCategoryByTbPcId;
    }

    @OneToMany(mappedBy = "tbProductCategoryByTbPcId", fetch = FetchType.EAGER)
    public Collection<ProductCategoryEntity> getTbProductCategoriesByPcId() {
        return tbProductCategoriesByPcId;
    }

    public void setTbProductCategoriesByPcId(Collection<ProductCategoryEntity> tbProductCategoriesByPcId) {
        this.tbProductCategoriesByPcId = tbProductCategoriesByPcId;
    }

    @Override
    public String toString() {
        return "ProductCategoryEntity{" +
                "pcId=" + pcId +
                ", pcName='" + pcName + '\'' +
                ", logicDelete=" + logicDelete +
                ", tbProductsByPcId=" + tbProductsByPcId +
                '}';
    }
}
