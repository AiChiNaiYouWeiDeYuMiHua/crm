package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_user", schema = "USER_CRM", catalog = "")
public class UserEntity {
    private Long userId;
    private String userName;
    private String userPassword;
    private String userSex;
    private Date userBirthday;
    private String userAddress;
    private Date hiredate;
    private String userTel;
    private String userEmail;
    private String userPhoto;
    private Long userIsDimission;
    private String openid;
    private String userCode;
//    private Collection<AllocationEntity> tbAllocationsByUserId;
//    private Collection<AllocationEntity> tbAllocationsByUserId_0;
//    private Collection<CareEntity> tbCaresByUserId;
//    private Collection<ComplaintEntity> tbComplaintsByUserId;
//    private Collection<CustomerEntity> tbCustomersByUserId;
//    private Collection<DailyRecodeEntity> tbDailyRecodesByUserId;
//    private Collection<DeliverGoodsEntity> tbDeliverGoodsByUserId;
//    private Collection<LogEntity> tbLogsByUserId;
//    private Collection<LogDataEntity> tbLogDataByUserId;
//    private Collection<MaintainOrderEntity> tbMaintainOrdersByUserId;
//    private Collection<OrderEntity> tbOrdersByUserId;
//    private Collection<OrderEntity> tbOrdersByUserId_0;
//    private Collection<PayBackDetailEntity> tbPayBackDetailsByUserId;
//    private Collection<PaymentRecordsEntity> tbPaymentRecordsByUserId;
//    private Collection<PlanPayBackEntity> tbPlanPayBacksByUserId;
//    private Collection<PlanPayDetailEntity> tbPlanPayDetailsByUserId;
//    private Collection<ProjectEntity> tbProjectsByUserId;
//    private Collection<PurchaseEntity> tbPurchasesByUserId;
//    private Collection<PurchaseReceiptEntity> tbPurchaseReceiptsByUserId;
//    private Collection<QuoteEntity> tbQuotesByUserId;
//    private Collection<ReceiptRecordsEntity> tbReceiptRecordsByUserId;
//    private Collection<SaleOppEntity> tbSaleOppsByUserId;
//    private Collection<ServeEntity> tbServesByUserId;
//    private Collection<TaskActionEntity> tbTaskActionsByUserId;
//    private Collection<TravelEntity> tbTravelsByUserId;
    private DeptEntity tbDeptByDeptId;
    @JSONField(serialize = false)
    private Collection<UserRoleEntity> tbUserRolesByUserId;
//    private Collection<WhCheckEntity> tbWhChecksByUserId;
//    private Collection<WhPullEntity> tbWhPullsByUserId;
//    private Collection<WhPullEntity> tbWhPullsByUserId_0;
//    private Collection<WhPutEntity> tbWhPutsByUserId;
//    private Collection<WhPutEntity> tbWhPutsByUserId_0;
//    private Collection<WpDetailEntity> tbWpDetailsByUserId;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    @Column(name = "user_id", nullable = false, precision = 0)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "user_name", nullable = true, length = 20)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "user_password", nullable = true, length = 20)
    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Basic
    @Column(name = "user_sex", nullable = true, length = 2)
    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    @Basic
    @Column(name = "user_birthday", nullable = true)
    @JSONField(format = "yyyy-MM-dd")
    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    @Basic
    @Column(name = "user_address", nullable = true, length = 60)
    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    @Basic
    @Column(name = "hiredate", nullable = true)
    @JSONField(format = "yyyy-MM-dd")
    public Date getHiredate() {
        return hiredate;
    }

    public void setHiredate(Date hiredate) {
        this.hiredate = hiredate;
    }

    @Basic
    @Column(name = "user_tel", nullable = true, length = 11)
    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    @Basic
    @Column(name = "user_email", nullable = true, length = 50)
    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Basic
    @Column(name = "user_photo", nullable = true, length = 100)
    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    @Basic
    @Column(name = "user_is_dimission", nullable = true, precision = 0)
    public Long getUserIsDimission() {
        return userIsDimission;
    }

    public void setUserIsDimission(Long userIsDimission) {
        this.userIsDimission = userIsDimission;
    }

    @Basic
    @Column(name = "openid", nullable = true, length = 200)
    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    @Basic
    @Column(name = "user_code", nullable = true, length = 18)
    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<UserRoleEntity> getTbUserRolesByUserId() {
//        return tbUserRolesByUserId;
//    }
//
//    public void setTbUserRolesByUserId(Collection<UserRoleEntity> tbUserRolesByUserId) {
//        this.tbUserRolesByUserId = tbUserRolesByUserId;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(userPassword, that.userPassword) &&
                Objects.equals(userSex, that.userSex) &&
                Objects.equals(userBirthday, that.userBirthday) &&
                Objects.equals(userAddress, that.userAddress) &&
                Objects.equals(hiredate, that.hiredate) &&
                Objects.equals(userTel, that.userTel) &&
                Objects.equals(userEmail, that.userEmail) &&
                Objects.equals(userPhoto, that.userPhoto) &&
                Objects.equals(userIsDimission, that.userIsDimission) &&
                Objects.equals(openid, that.openid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userName, userPassword, userSex, userBirthday, userAddress, hiredate, userTel, userEmail, userPhoto, userIsDimission, openid);
    }

//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<AllocationEntity> getTbAllocationsByUserId() {
//        return tbAllocationsByUserId;
//    }
//
//    public void setTbAllocationsByUserId(Collection<AllocationEntity> tbAllocationsByUserId) {
//        this.tbAllocationsByUserId = tbAllocationsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByTbUserId")
//    public Collection<AllocationEntity> getTbAllocationsByUserId_0() {
//        return tbAllocationsByUserId_0;
//    }
//
//    public void setTbAllocationsByUserId_0(Collection<AllocationEntity> tbAllocationsByUserId_0) {
//        this.tbAllocationsByUserId_0 = tbAllocationsByUserId_0;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<CareEntity> getTbCaresByUserId() {
//        return tbCaresByUserId;
//    }
//
//    public void setTbCaresByUserId(Collection<CareEntity> tbCaresByUserId) {
//        this.tbCaresByUserId = tbCaresByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<ComplaintEntity> getTbComplaintsByUserId() {
//        return tbComplaintsByUserId;
//    }
//
//    public void setTbComplaintsByUserId(Collection<ComplaintEntity> tbComplaintsByUserId) {
//        this.tbComplaintsByUserId = tbComplaintsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<CustomerEntity> getTbCustomersByUserId() {
//        return tbCustomersByUserId;
//    }
//
//    public void setTbCustomersByUserId(Collection<CustomerEntity> tbCustomersByUserId) {
//        this.tbCustomersByUserId = tbCustomersByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<DailyRecodeEntity> getTbDailyRecodesByUserId() {
//        return tbDailyRecodesByUserId;
//    }
//
//    public void setTbDailyRecodesByUserId(Collection<DailyRecodeEntity> tbDailyRecodesByUserId) {
//        this.tbDailyRecodesByUserId = tbDailyRecodesByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<DeliverGoodsEntity> getTbDeliverGoodsByUserId() {
//        return tbDeliverGoodsByUserId;
//    }
//
//    public void setTbDeliverGoodsByUserId(Collection<DeliverGoodsEntity> tbDeliverGoodsByUserId) {
//        this.tbDeliverGoodsByUserId = tbDeliverGoodsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<LogEntity> getTbLogsByUserId() {
//        return tbLogsByUserId;
//    }
//
//    public void setTbLogsByUserId(Collection<LogEntity> tbLogsByUserId) {
//        this.tbLogsByUserId = tbLogsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<LogDataEntity> getTbLogDataByUserId() {
//        return tbLogDataByUserId;
//    }
//
//    public void setTbLogDataByUserId(Collection<LogDataEntity> tbLogDataByUserId) {
//        this.tbLogDataByUserId = tbLogDataByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<MaintainOrderEntity> getTbMaintainOrdersByUserId() {
//        return tbMaintainOrdersByUserId;
//    }
//
//    public void setTbMaintainOrdersByUserId(Collection<MaintainOrderEntity> tbMaintainOrdersByUserId) {
//        this.tbMaintainOrdersByUserId = tbMaintainOrdersByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<OrderEntity> getTbOrdersByUserId() {
//        return tbOrdersByUserId;
//    }
//
//    public void setTbOrdersByUserId(Collection<OrderEntity> tbOrdersByUserId) {
//        this.tbOrdersByUserId = tbOrdersByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByTbUserId")
//    public Collection<OrderEntity> getTbOrdersByUserId_0() {
//        return tbOrdersByUserId_0;
//    }
//
//    public void setTbOrdersByUserId_0(Collection<OrderEntity> tbOrdersByUserId_0) {
//        this.tbOrdersByUserId_0 = tbOrdersByUserId_0;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<PayBackDetailEntity> getTbPayBackDetailsByUserId() {
//        return tbPayBackDetailsByUserId;
//    }
//
//    public void setTbPayBackDetailsByUserId(Collection<PayBackDetailEntity> tbPayBackDetailsByUserId) {
//        this.tbPayBackDetailsByUserId = tbPayBackDetailsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<PaymentRecordsEntity> getTbPaymentRecordsByUserId() {
//        return tbPaymentRecordsByUserId;
//    }
//
//    public void setTbPaymentRecordsByUserId(Collection<PaymentRecordsEntity> tbPaymentRecordsByUserId) {
//        this.tbPaymentRecordsByUserId = tbPaymentRecordsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<PlanPayBackEntity> getTbPlanPayBacksByUserId() {
//        return tbPlanPayBacksByUserId;
//    }
//
//    public void setTbPlanPayBacksByUserId(Collection<PlanPayBackEntity> tbPlanPayBacksByUserId) {
//        this.tbPlanPayBacksByUserId = tbPlanPayBacksByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<PlanPayDetailEntity> getTbPlanPayDetailsByUserId() {
//        return tbPlanPayDetailsByUserId;
//    }
//
//    public void setTbPlanPayDetailsByUserId(Collection<PlanPayDetailEntity> tbPlanPayDetailsByUserId) {
//        this.tbPlanPayDetailsByUserId = tbPlanPayDetailsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<ProjectEntity> getTbProjectsByUserId() {
//        return tbProjectsByUserId;
//    }
//
//    public void setTbProjectsByUserId(Collection<ProjectEntity> tbProjectsByUserId) {
//        this.tbProjectsByUserId = tbProjectsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<PurchaseEntity> getTbPurchasesByUserId() {
//        return tbPurchasesByUserId;
//    }
//
//    public void setTbPurchasesByUserId(Collection<PurchaseEntity> tbPurchasesByUserId) {
//        this.tbPurchasesByUserId = tbPurchasesByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<PurchaseReceiptEntity> getTbPurchaseReceiptsByUserId() {
//        return tbPurchaseReceiptsByUserId;
//    }
//
//    public void setTbPurchaseReceiptsByUserId(Collection<PurchaseReceiptEntity> tbPurchaseReceiptsByUserId) {
//        this.tbPurchaseReceiptsByUserId = tbPurchaseReceiptsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<QuoteEntity> getTbQuotesByUserId() {
//        return tbQuotesByUserId;
//    }
//
//    public void setTbQuotesByUserId(Collection<QuoteEntity> tbQuotesByUserId) {
//        this.tbQuotesByUserId = tbQuotesByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<ReceiptRecordsEntity> getTbReceiptRecordsByUserId() {
//        return tbReceiptRecordsByUserId;
//    }
//
//    public void setTbReceiptRecordsByUserId(Collection<ReceiptRecordsEntity> tbReceiptRecordsByUserId) {
//        this.tbReceiptRecordsByUserId = tbReceiptRecordsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<SaleOppEntity> getTbSaleOppsByUserId() {
//        return tbSaleOppsByUserId;
//    }
//
//    public void setTbSaleOppsByUserId(Collection<SaleOppEntity> tbSaleOppsByUserId) {
//        this.tbSaleOppsByUserId = tbSaleOppsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<ServeEntity> getTbServesByUserId() {
//        return tbServesByUserId;
//    }
//
//    public void setTbServesByUserId(Collection<ServeEntity> tbServesByUserId) {
//        this.tbServesByUserId = tbServesByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<TaskActionEntity> getTbTaskActionsByUserId() {
//        return tbTaskActionsByUserId;
//    }
//
//    public void setTbTaskActionsByUserId(Collection<TaskActionEntity> tbTaskActionsByUserId) {
//        this.tbTaskActionsByUserId = tbTaskActionsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<TravelEntity> getTbTravelsByUserId() {
//        return tbTravelsByUserId;
//    }
//
//    public void setTbTravelsByUserId(Collection<TravelEntity> tbTravelsByUserId) {
//        this.tbTravelsByUserId = tbTravelsByUserId;
//    }
//
    @ManyToOne
    @JoinColumn(name = "dept_id", referencedColumnName = "dept_id")
    public DeptEntity getTbDeptByDeptId() {
        return tbDeptByDeptId;
    }

    public void setTbDeptByDeptId(DeptEntity tbDeptByDeptId) {
        this.tbDeptByDeptId = tbDeptByDeptId;
    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<WhCheckEntity> getTbWhChecksByUserId() {
//        return tbWhChecksByUserId;
//    }
//
//    public void setTbWhChecksByUserId(Collection<WhCheckEntity> tbWhChecksByUserId) {
//        this.tbWhChecksByUserId = tbWhChecksByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<WhPullEntity> getTbWhPullsByUserId() {
//        return tbWhPullsByUserId;
//    }
//
//    public void setTbWhPullsByUserId(Collection<WhPullEntity> tbWhPullsByUserId) {
//        this.tbWhPullsByUserId = tbWhPullsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByTbUserId")
//    public Collection<WhPullEntity> getTbWhPullsByUserId_0() {
//        return tbWhPullsByUserId_0;
//    }
//
//    public void setTbWhPullsByUserId_0(Collection<WhPullEntity> tbWhPullsByUserId_0) {
//        this.tbWhPullsByUserId_0 = tbWhPullsByUserId_0;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<WhPutEntity> getTbWhPutsByUserId() {
//        return tbWhPutsByUserId;
//    }
//
//    public void setTbWhPutsByUserId(Collection<WhPutEntity> tbWhPutsByUserId) {
//        this.tbWhPutsByUserId = tbWhPutsByUserId;
//    }
//
//    @OneToMany(mappedBy = "tbUserByTbUserId")
//    public Collection<WhPutEntity> getTbWhPutsByUserId_0() {
//        return tbWhPutsByUserId_0;
//    }
//
//    public void setTbWhPutsByUserId_0(Collection<WhPutEntity> tbWhPutsByUserId_0) {
//        this.tbWhPutsByUserId_0 = tbWhPutsByUserId_0;
//    }
//
//    @OneToMany(mappedBy = "tbUserByUserId")
//    public Collection<WpDetailEntity> getTbWpDetailsByUserId() {
//        return tbWpDetailsByUserId;
//    }
//
//    public void setTbWpDetailsByUserId(Collection<WpDetailEntity> tbWpDetailsByUserId) {
//        this.tbWpDetailsByUserId = tbWpDetailsByUserId;
//    }


    @Override
    public String toString() {
        return "UserEntity{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userSex='" + userSex + '\'' +
                ", userBirthday=" + userBirthday +
                ", userAddress='" + userAddress + '\'' +
                ", hiredate=" + hiredate +
                ", userTel='" + userTel + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userPhoto='" + userPhoto + '\'' +
                ", userIsDimission=" + userIsDimission +
                ", openid='" + openid + '\'' +
                ", userCode='" + userCode + '\'' +
                ", tbDeptByDeptId=" + tbDeptByDeptId +
                ", tbUserRolesByUserId=" + tbUserRolesByUserId +
                '}';
    }
}
