package top.went.pojo;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_role", schema = "USER_CRM", catalog = "")
public class RoleEntity {
    private Long roleId;
    private String roleName;
    private Long roleIsDel;
    private Collection<DeptRoleEntity> tbDeptRolesByRoleId;
    private Collection<UserRoleEntity> tbUserRolesByRoleId;
    private Collection<RoleFcEntity> tbRoleFcsByRoleId;
    //    private Collection<DeptEntity> deptEntities;
//    private Collection<FunctionsEntity> functionsEntities;
    private Collection<MessageEntity> tbMessagesByRoleId;

    @Id
    @Column(name = "role_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "role_name", nullable = true, length = 20)
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Basic
    @Column(name = "role_is_del", nullable = true, precision = 0)
    public Long getRoleIsDel() {
        return roleIsDel;
    }

    public void setRoleIsDel(Long roleIsDel) {
        this.roleIsDel = roleIsDel;
    }

//    @OneToMany(mappedBy = "tbRoleByRoleId")
//    public Collection<DeptRoleEntity> getTbDeptRolesByRoleId() {
//        return tbDeptRolesByRoleId;
//    }
//
//    public void setTbDeptRolesByRoleId(Collection<DeptRoleEntity> tbDeptRolesByRoleId) {
//        this.tbDeptRolesByRoleId = tbDeptRolesByRoleId;
//    }
//
//    @OneToMany(mappedBy = "tbRoleByRoleId")
//    public Collection<RoleFcEntity> getTbRoleFcsByRoleId() {
//        return tbRoleFcsByRoleId;
//    }
//
//    public void setTbRoleFcsByRoleId(Collection<RoleFcEntity> tbRoleFcsByRoleId) {
//        this.tbRoleFcsByRoleId = tbRoleFcsByRoleId;
//    }
//
//    @OneToMany(mappedBy = "tbRoleByRoleId")
//    public Collection<UserRoleEntity> getTbUserRolesByRoleId() {
//        return tbUserRolesByRoleId;
//    }
//
//    public void setTbUserRolesByRoleId(Collection<UserRoleEntity> tbUserRolesByRoleId) {
//        this.tbUserRolesByRoleId = tbUserRolesByRoleId;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleEntity that = (RoleEntity) o;
        return Objects.equals(roleId, that.roleId) &&
                Objects.equals(roleName, that.roleName) &&
                Objects.equals(roleIsDel, that.roleIsDel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, roleName, roleIsDel);
    }

//    @ManyToMany(mappedBy = "roleEntities")
//    public Collection<DeptEntity> getDeptEntities() {
//        return deptEntities;
//    }
//
//    public void setDeptEntities(Collection<DeptEntity> deptEntities) {
//        this.deptEntities = deptEntities;
//    }
//
//    @ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(name = "role_fc", joinColumns = {
//            @JoinColumn(name = "role_id")}, inverseJoinColumns = {
//            @JoinColumn(name = "fc_id")})
//    public Collection<FunctionsEntity> getFunctionsEntities() {
//        return functionsEntities;
//    }
//
//    public void setFunctionsEntities(Collection<FunctionsEntity> functionsEntities) {
//        this.functionsEntities = functionsEntities;
//    }
//
//    @OneToMany(mappedBy = "tbRoleByRoleId")
//    public Collection<MessageEntity> getTbMessagesByRoleId() {
//        return tbMessagesByRoleId;
//    }
//
//    public void setTbMessagesByRoleId(Collection<MessageEntity> tbMessagesByRoleId) {
//        this.tbMessagesByRoleId = tbMessagesByRoleId;
//    }

    @Override
    public String toString() {
        return "RoleEntity{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                ", roleIsDel=" + roleIsDel +
                ", tbDeptRolesByRoleId=" + tbDeptRolesByRoleId +
                ", tbUserRolesByRoleId=" + tbUserRolesByRoleId +
                ", tbRoleFcsByRoleId=" + tbRoleFcsByRoleId +
                ", tbMessagesByRoleId=" + tbMessagesByRoleId +
                '}';
    }
}
