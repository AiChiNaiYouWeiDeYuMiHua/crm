package top.went.pojo;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_addr", schema = "USER_CRM", catalog = "")
public class AddrEntity {
    private Integer addrId;
    private String addrCountry;
    private String addrProvence;
    private String addrArea;
    private Integer addrZipCode;
    private CustomerEntity tbCustomerByCusId;
    private Collection<CustomerEntity> tbCustomersByAddrId;

    @Id
    @Column(name = "addr_id", nullable = false, precision = 0)
    public Integer getAddrId() {
        return addrId;
    }

    public void setAddrId(Integer addrId) {
        this.addrId = addrId;
    }

    @Basic
    @Column(name = "addr_country", nullable = true, length = 20)
    public String getAddrCountry() {
        return addrCountry;
    }

    public void setAddrCountry(String addrCountry) {
        this.addrCountry = addrCountry;
    }

    @Basic
    @Column(name = "addr_provence", nullable = true, length = 20)
    public String getAddrProvence() {
        return addrProvence;
    }

    public void setAddrProvence(String addrProvence) {
        this.addrProvence = addrProvence;
    }

    @Basic
    @Column(name = "addr_area", nullable = true, length = 20)
    public String getAddrArea() {
        return addrArea;
    }

    public void setAddrArea(String addrArea) {
        this.addrArea = addrArea;
    }

    @Basic
    @Column(name = "addr_zip_code", nullable = true, precision = 0)
    public Integer getAddrZipCode() {
        return addrZipCode;
    }

    public void setAddrZipCode(Integer addrZipCode) {
        this.addrZipCode = addrZipCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddrEntity that = (AddrEntity) o;
        return Objects.equals(addrId, that.addrId) &&
                Objects.equals(addrCountry, that.addrCountry) &&
                Objects.equals(addrProvence, that.addrProvence) &&
                Objects.equals(addrArea, that.addrArea) &&
                Objects.equals(addrZipCode, that.addrZipCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addrId, addrCountry, addrProvence, addrArea, addrZipCode);
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @OneToMany(mappedBy = "tbAddrByAddrId")
    public Collection<CustomerEntity> getTbCustomersByAddrId() {
        return tbCustomersByAddrId;
    }

    public void setTbCustomersByAddrId(Collection<CustomerEntity> tbCustomersByAddrId) {
        this.tbCustomersByAddrId = tbCustomersByAddrId;
    }
}
