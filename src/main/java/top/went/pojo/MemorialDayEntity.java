package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_memorial_day", schema = "USER_CRM")
public class MemorialDayEntity {
    private Integer medId;
    private String medType;
    @JSONField(format = "yyyy-MM-dd")
    private Date medMemorialDate;
    @JSONField(format = "yyyy-MM-dd")
    private Date medNextPoint;
    @JSONField(format = "yyyy-MM-dd")
    private Date medCreatetimr;
    private String medRemark;
    private Integer medIsDelete;
    private CustomerEntity tbCustomerByCusId;
    private ContactsEntity tbContactsByCotsId;


    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "med_id", nullable = false, precision = 0)
    public Integer getMedId() {
        return medId;
    }

    public void setMedId(Integer medId) {
        this.medId = medId;
    }

    @Basic
    @Column(name = "med_type", nullable = true, length = 20)
    public String getMedType() {
        return medType;
    }

    public void setMedType(String medType) {
        this.medType = medType;
    }

    @Basic
    @Column(name = "med_memorial_date", nullable = true)
    public Date getMedMemorialDate() {
        return medMemorialDate;
    }

    public void setMedMemorialDate(Date medMemorialDate) {
        this.medMemorialDate = medMemorialDate;
    }

    @Basic
    @Column(name = "med_next_point", nullable = true)
    public Date getMedNextPoint() {
        return medNextPoint;
    }

    public void setMedNextPoint(Date medNextPoint) {
        this.medNextPoint = medNextPoint;
    }

    @Basic
    @Column(name = "med_createtimr", nullable = true)
    public Date getMedCreatetimr() {
        return medCreatetimr;
    }

    public void setMedCreatetimr(Date medCreatetimr) {
        this.medCreatetimr = medCreatetimr;
    }

    @Basic
    @Column(name = "med_remark", nullable = true)
    public String getMedRemark() {
        return medRemark;
    }

    public void setMedRemark(String medRemark) {
        this.medRemark = medRemark;
    }

    @Basic
    @Column(name = "med_is_delete", nullable = true)
    public Integer getMedIsDelete() {
        return medIsDelete;
    }

    public void setMedIsDelete(Integer medIsDelete) {
        this.medIsDelete = medIsDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemorialDayEntity that = (MemorialDayEntity) o;
        return Objects.equals(medId, that.medId) &&
                Objects.equals(medType, that.medType) &&
                Objects.equals(medMemorialDate, that.medMemorialDate) &&
                Objects.equals(medNextPoint, that.medNextPoint) &&
                Objects.equals(medCreatetimr, that.medCreatetimr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(medId, medType, medMemorialDate, medNextPoint, medCreatetimr);
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "cots_id", referencedColumnName = "cots_id")
    public ContactsEntity getTbContactsByCotsId() {
        return tbContactsByCotsId;
    }

    public void setTbContactsByCotsId(ContactsEntity tbContactsByCotsId) {
        this.tbContactsByCotsId = tbContactsByCotsId;
    }
}
