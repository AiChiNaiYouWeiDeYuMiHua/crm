package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "TB_APPROVAL", schema = "USER_CRM", catalog = "")
public class ApprovalEntity {
    private Integer approvalId;
    private String approvalType;
    private String approvalTitle;
    private String approvalStatus;
    @JSONField(format = "yyyy-MM-dd")
    private Date approvalApplyTime;
    @JSONField(format = "yyyy-MM-dd")
    private Date approvalTime;
    private String approvalContent;
    private Integer approvalOrderId;
    private String approvalResult;
    private UserEntity tbUserByApprovalReceiverId;
    private UserEntity tbUserByApprovalUserId;
    @JSONField(serialize = false)
    private Collection<ApprovalLogsEntity> tbApprovalLogsByApprovalId;

    @Id
    @Column(name = "APPROVAL_ID", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Integer approvalId) {
        this.approvalId = approvalId;
    }

    @Basic
    @Column(name = "APPROVAL_TYPE", nullable = true, length = 20)
    public String getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(String approvalType) {
        this.approvalType = approvalType;
    }

    @Basic
    @Column(name = "APPROVAL_TITLE", nullable = true, length = 50)
    public String getApprovalTitle() {
        return approvalTitle;
    }

    public void setApprovalTitle(String approvalTitle) {
        this.approvalTitle = approvalTitle;
    }

    @Basic
    @Column(name = "APPROVAL_STATUS", nullable = true, length = 8)
    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Basic
    @Column(name = "APPROVAL_APPLY_TIME", nullable = true)
    public Date getApprovalApplyTime() {
        return approvalApplyTime;
    }

    public void setApprovalApplyTime(Date approvalApplyTime) {
        this.approvalApplyTime = approvalApplyTime;
    }

    @Basic
    @Column(name = "APPROVAL_TIME", nullable = true)
    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    @Basic
    @Column(name = "APPROVAL_CONTENT", nullable = true, length = 200)
    public String getApprovalContent() {
        return approvalContent;
    }

    public void setApprovalContent(String approvalContent) {
        this.approvalContent = approvalContent;
    }

    @Basic
    @Column(name = "APPROVAL_ORDER_ID", nullable = true, precision = 0)
    public Integer getApprovalOrderId() {
        return approvalOrderId;
    }

    public void setApprovalOrderId(Integer approvalOrderId) {
        this.approvalOrderId = approvalOrderId;
    }

    @Basic
    @Column(name = "APPROVAL_RESULT", nullable = true, length = 8)
    public String getApprovalResult() {
        return approvalResult;
    }

    public void setApprovalResult(String approvalResult) {
        this.approvalResult = approvalResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApprovalEntity that = (ApprovalEntity) o;
        return Objects.equals(approvalId, that.approvalId) &&
                Objects.equals(approvalType, that.approvalType) &&
                Objects.equals(approvalTitle, that.approvalTitle) &&
                Objects.equals(approvalStatus, that.approvalStatus) &&
                Objects.equals(approvalApplyTime, that.approvalApplyTime) &&
                Objects.equals(approvalTime, that.approvalTime) &&
                Objects.equals(approvalContent, that.approvalContent) &&
                Objects.equals(approvalOrderId, that.approvalOrderId) &&
                Objects.equals(approvalResult, that.approvalResult);
    }

    @Override
    public int hashCode() {
        return Objects.hash(approvalId, approvalType, approvalTitle, approvalStatus, approvalApplyTime, approvalTime, approvalContent, approvalOrderId, approvalResult);
    }

    @ManyToOne
    @JoinColumn(name = "APPROVAL_RECEIVER_ID", referencedColumnName = "USER_ID")
    public UserEntity getTbUserByApprovalReceiverId() {
        return tbUserByApprovalReceiverId;
    }

    public void setTbUserByApprovalReceiverId(UserEntity tbUserByApprovalReceiverId) {
        this.tbUserByApprovalReceiverId = tbUserByApprovalReceiverId;
    }

    @ManyToOne
    @JoinColumn(name = "APPROVAL_USER_ID", referencedColumnName = "USER_ID")
    public UserEntity getTbUserByApprovalUserId() {
        return tbUserByApprovalUserId;
    }

    public void setTbUserByApprovalUserId(UserEntity tbUserByApprovalUserId) {
        this.tbUserByApprovalUserId = tbUserByApprovalUserId;
    }

    @OneToMany(mappedBy = "tbApprovalByApprovalId",fetch = FetchType.EAGER)
    public Collection<ApprovalLogsEntity> getTbApprovalLogsByApprovalId() {
        return tbApprovalLogsByApprovalId;
    }

    public void setTbApprovalLogsByApprovalId(Collection<ApprovalLogsEntity> tbApprovalLogsByApprovalId) {
        this.tbApprovalLogsByApprovalId = tbApprovalLogsByApprovalId;
    }
}
