package top.went.pojo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "dept_announce", schema = "USER_CRM", catalog = "")
public class DeptAnnounceEntity {
    private Integer announceIdId;
    private DeptEntity tbDeptByDeptId;

    @Id
    @Column(name = "announce_id\r\n   id", nullable = false, precision = 0)
    public Integer getAnnounceIdId() {
        return announceIdId;
    }

    public void setAnnounceIdId(Integer announceIdId) {
        this.announceIdId = announceIdId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeptAnnounceEntity that = (DeptAnnounceEntity) o;
        return Objects.equals(announceIdId, that.announceIdId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(announceIdId);
    }

    @ManyToOne
    @JoinColumn(name = "dept_id", referencedColumnName = "dept_id", nullable = false)
    public DeptEntity getTbDeptByDeptId() {
        return tbDeptByDeptId;
    }

    public void setTbDeptByDeptId(DeptEntity tbDeptByDeptId) {
        this.tbDeptByDeptId = tbDeptByDeptId;
    }
}
