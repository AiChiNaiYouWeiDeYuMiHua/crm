package top.went.pojo;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_dimission_type", schema = "USER_CRM", catalog = "")
public class DimissionTypeEntity {
    private Long dimiTypeId;
    private String dimiTypeName;
    private Collection<DimissionEntity> tbDimissionsByDimiTypeId;

    @Id
    @Column(name = "dimi_type_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    public Long getDimiTypeId() {
        return dimiTypeId;
    }

    public void setDimiTypeId(Long dimiTypeId) {
        this.dimiTypeId = dimiTypeId;
    }

    @Basic
    @Column(name = "dimi_type_name", nullable = true, length = 50)
    public String getDimiTypeName() {
        return dimiTypeName;
    }

    public void setDimiTypeName(String dimiTypeName) {
        this.dimiTypeName = dimiTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DimissionTypeEntity that = (DimissionTypeEntity) o;
        return Objects.equals(dimiTypeId, that.dimiTypeId) &&
                Objects.equals(dimiTypeName, that.dimiTypeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dimiTypeId, dimiTypeName);
    }

//    @OneToMany(mappedBy = "tbDimissionTypeByDimiTypeId")
//    public Collection<DimissionEntity> getTbDimissionsByDimiTypeId() {
//        return tbDimissionsByDimiTypeId;
//    }
//
//    public void setTbDimissionsByDimiTypeId(Collection<DimissionEntity> tbDimissionsByDimiTypeId) {
//        this.tbDimissionsByDimiTypeId = tbDimissionsByDimiTypeId;
//    }

    @Override
    public String toString() {
        return "DimissionTypeEntity{" +
                "dimiTypeId=" + dimiTypeId +
                ", dimiTypeName='" + dimiTypeName + '\'' +
                ", tbDimissionsByDimiTypeId=" + tbDimissionsByDimiTypeId +
                '}';
    }
}
