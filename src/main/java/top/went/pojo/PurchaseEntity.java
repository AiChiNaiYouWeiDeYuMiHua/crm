package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_purchase", schema = "USER_CRM", catalog = "")
public class PurchaseEntity {
    private Integer purId;
    private String purTheme;
    private String purOddNumbers;
    @JSONField(format = "yyyy-MM-dd")
    private Date purDate;
    private String purState;
    private String purBackRemark;
    @JSONField(format = "yyyy-MM-dd")
    private Date purProjectedDate;
    private String purRemarks;
    private Long magicDelete;
    private String purAccessories;
    private Integer purOk;
    private Collection<PurDetailEntity> tbPurDetailsByPurId;
    private UserEntity tbUserByUserId;
    private CustomerEntity tbCustomerByCusId;
    private WarehouseEntity tbWarehouseByWhId;
    private Collection<PurchaseReceiptEntity> tbPurchaseReceiptsByPurId;
    private Collection<ReturnGoodsEntity> tbReturnGoodsByPurId;
    private Collection<PurDetailEntity> tbPurDetailByPdId;
    private Collection<WhPutEntity> tbWhPutsByPurId;

    @Id
    @Column(name = "pur_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getPurId() {
        return purId;
    }

    public void setPurId(Integer purId) {
        this.purId = purId;
    }

    @Basic
    @Column(name = "pur_theme", nullable = true, length = 20)
    public String getPurTheme() {
        return purTheme;
    }

    public void setPurTheme(String purTheme) {
        this.purTheme = purTheme;
    }

    @Basic
    @Column(name = "pur_odd_numbers", nullable = true, length = 20)
    public String getPurOddNumbers() {
        return purOddNumbers;
    }

    public void setPurOddNumbers(String purOddNumbers) {
        this.purOddNumbers = purOddNumbers;
    }

    @Basic
    @Column(name = "pur_date", nullable = true)
    public Date getPurDate() {
        return purDate;
    }

    public void setPurDate(Date purDate) {
        this.purDate = purDate;
    }

    @Basic
    @Column(name = "pur_state", nullable = true, length = 20)
    public String getPurState() {
        return purState;
    }

    public void setPurState(String purState) {
        this.purState = purState;
    }

    @Basic
    @Column(name = "pur_projected_date", nullable = true)
    public Date getPurProjectedDate() {
        return purProjectedDate;
    }

    public void setPurProjectedDate(Date purProjectedDate) {
        this.purProjectedDate = purProjectedDate;
    }

    @Basic
    @Column(name = "pur_remarks", nullable = true, length = 100)
    public String getPurRemarks() {
        return purRemarks;
    }

    public void setPurRemarks(String purRemarks) {
        this.purRemarks = purRemarks;
    }

    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    @Basic
    @Column(name = "pur_accessories", nullable = true, length = 100)
    public String getPurAccessories() {
        return purAccessories;
    }

    public void setPurAccessories(String purAccessories) {
        this.purAccessories = purAccessories;
    }

    @Basic
    @Column(name = "pur_ok", nullable = true, precision = 0)
    public Integer getPurOk() {
        return purOk;
    }

    public void setPurOk(Integer purOk) {
        this.purOk = purOk;
    }
    @Basic
    @Column(name = "pur_back_remark", nullable = true, precision = 0)
    public String getPurBackRemark() {
        return purBackRemark;
    }

    public void setPurBackRemark(String purBackRemark) {
        this.purBackRemark = purBackRemark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseEntity that = (PurchaseEntity) o;
        return Objects.equals(purId, that.purId) &&
                Objects.equals(purTheme, that.purTheme) &&
                Objects.equals(purOddNumbers, that.purOddNumbers) &&
                Objects.equals(purDate, that.purDate) &&
                Objects.equals(purState, that.purState) &&
                Objects.equals(purProjectedDate, that.purProjectedDate) &&
                Objects.equals(purRemarks, that.purRemarks) &&
                Objects.equals(magicDelete, that.magicDelete) &&
                Objects.equals(purAccessories, that.purAccessories) &&
                Objects.equals(purOk, that.purOk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(purId, purTheme, purOddNumbers, purDate, purState, purProjectedDate, purRemarks, magicDelete, purAccessories, purOk);
    }

    @OneToMany(mappedBy = "tbPurchaseByPurId")
    public Collection<PurDetailEntity> getTbPurDetailsByPurId() {
        return tbPurDetailsByPurId;
    }

    public void setTbPurDetailsByPurId(Collection<PurDetailEntity> tbPurDetailsByPurId) {
        this.tbPurDetailsByPurId = tbPurDetailsByPurId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "wh_id", referencedColumnName = "wh_id")
    public WarehouseEntity getTbWarehouseByWhId() {
        return tbWarehouseByWhId;
    }

    public void setTbWarehouseByWhId(WarehouseEntity tbWarehouseByWhId) {
        this.tbWarehouseByWhId = tbWarehouseByWhId;
    }

    @OneToMany(mappedBy = "tbPurchaseByPurId")
    public Collection<PurchaseReceiptEntity> getTbPurchaseReceiptsByPurId() {
        return tbPurchaseReceiptsByPurId;
    }

    public void setTbPurchaseReceiptsByPurId(Collection<PurchaseReceiptEntity> tbPurchaseReceiptsByPurId) {
        this.tbPurchaseReceiptsByPurId = tbPurchaseReceiptsByPurId;
    }

    @OneToMany(mappedBy = "tbPurchaseByPurId")
    public Collection<ReturnGoodsEntity> getTbReturnGoodsByPurId() {
        return tbReturnGoodsByPurId;
    }

    public void setTbReturnGoodsByPurId(Collection<ReturnGoodsEntity> tbReturnGoodsByPurId) {
        this.tbReturnGoodsByPurId = tbReturnGoodsByPurId;
    }

    @OneToMany(mappedBy = "tbPurchaseByPurId")
    public Collection<WhPutEntity> getTbWhPutsByPurId() {
        return tbWhPutsByPurId;
    }

    public void setTbWhPutsByPurId(Collection<WhPutEntity> tbWhPutsByPurId) {
        this.tbWhPutsByPurId = tbWhPutsByPurId;
    }
    @OneToMany(mappedBy = "tbPurchaseByPurId")
    public Collection<PurDetailEntity> getTbPurDetailByPdId() {
        return tbPurDetailByPdId;
    }

    public void setTbPurDetailByPdId(Collection<PurDetailEntity> tbPurDetailByPdId) {
        this.tbPurDetailByPdId = tbPurDetailByPdId;
    }
}
