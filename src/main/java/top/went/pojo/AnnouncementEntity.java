package top.went.pojo;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_announcement", schema = "USER_CRM", catalog = "")
public class AnnouncementEntity {
    private Integer announceIdId;
    private String announceTitle;
    private String announceContent;
    private Long announceIsTopping;
    private String announceUser;
    private Time announceTime;
    private String announceAnnex;
    private String announceType;
    private Long announceIsDel;

    @Id
    @Column(name = "announce_id\r\n   id", nullable = false, precision = 0)
    public Integer getAnnounceIdId() {
        return announceIdId;
    }

    public void setAnnounceIdId(Integer announceIdId) {
        this.announceIdId = announceIdId;
    }

    @Basic
    @Column(name = "announce_title", nullable = true, length = 50)
    public String getAnnounceTitle() {
        return announceTitle;
    }

    public void setAnnounceTitle(String announceTitle) {
        this.announceTitle = announceTitle;
    }

    @Basic
    @Column(name = "announce_content", nullable = true)
    public String getAnnounceContent() {
        return announceContent;
    }

    public void setAnnounceContent(String announceContent) {
        this.announceContent = announceContent;
    }

    @Basic
    @Column(name = "announce_is_topping", nullable = true, precision = 0)
    public Long getAnnounceIsTopping() {
        return announceIsTopping;
    }

    public void setAnnounceIsTopping(Long announceIsTopping) {
        this.announceIsTopping = announceIsTopping;
    }

    @Basic
    @Column(name = "announce_user", nullable = true, length = 20)
    public String getAnnounceUser() {
        return announceUser;
    }

    public void setAnnounceUser(String announceUser) {
        this.announceUser = announceUser;
    }

    @Basic
    @Column(name = "announce_time", nullable = true)
    public Time getAnnounceTime() {
        return announceTime;
    }

    public void setAnnounceTime(Time announceTime) {
        this.announceTime = announceTime;
    }

    @Basic
    @Column(name = "announce_annex", nullable = true, length = 2000)
    public String getAnnounceAnnex() {
        return announceAnnex;
    }

    public void setAnnounceAnnex(String announceAnnex) {
        this.announceAnnex = announceAnnex;
    }

    @Basic
    @Column(name = "announce_type", nullable = true, length = 20)
    public String getAnnounceType() {
        return announceType;
    }

    public void setAnnounceType(String announceType) {
        this.announceType = announceType;
    }

    @Basic
    @Column(name = "announce_is_del", nullable = true, precision = 0)
    public Long getAnnounceIsDel() {
        return announceIsDel;
    }

    public void setAnnounceIsDel(Long announceIsDel) {
        this.announceIsDel = announceIsDel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnnouncementEntity that = (AnnouncementEntity) o;
        return Objects.equals(announceIdId, that.announceIdId) &&
                Objects.equals(announceTitle, that.announceTitle) &&
                Objects.equals(announceContent, that.announceContent) &&
                Objects.equals(announceIsTopping, that.announceIsTopping) &&
                Objects.equals(announceUser, that.announceUser) &&
                Objects.equals(announceTime, that.announceTime) &&
                Objects.equals(announceAnnex, that.announceAnnex) &&
                Objects.equals(announceType, that.announceType) &&
                Objects.equals(announceIsDel, that.announceIsDel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(announceIdId, announceTitle, announceContent, announceIsTopping, announceUser, announceTime, announceAnnex, announceType, announceIsDel);
    }
}
