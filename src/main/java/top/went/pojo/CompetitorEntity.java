package top.went.pojo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_competitor", schema = "USER_CRM", catalog = "")
public class CompetitorEntity {
    private Long competitorId;
    private String company;
    private Long price;
    private Long competitiveAbility;
    private String competitiveProduct;
    private String disadvantage;
    private String strategy;
    private String remarks;
    private Long isDelete;
    private SaleOppEntity tbSaleOppByOppId;

    @Id
    @Column(name = "competitor_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="myseq")
    @SequenceGenerator(name="myseq",sequenceName="seq",allocationSize=1)
    public Long getCompetitorId() {
        return competitorId;
    }

    public void setCompetitorId(Long competitorId) {
        this.competitorId = competitorId;
    }

    @Basic
    @Column(name = "company", nullable = true, length = 30)
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Basic
    @Column(name = "price", nullable = true, precision = 2)
    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    @Basic
    @Column(name = "competitive_ability", nullable = true, precision = 0)
    public Long getCompetitiveAbility() {
        return competitiveAbility;
    }

    public void setCompetitiveAbility(Long competitiveAbility) {
        this.competitiveAbility = competitiveAbility;
    }

    @Basic
    @Column(name = "competitive_product", nullable = true)
    public String getCompetitiveProduct() {
        return competitiveProduct;
    }

    public void setCompetitiveProduct(String competitiveProduct) {
        this.competitiveProduct = competitiveProduct;
    }

    @Basic
    @Column(name = "disadvantage", nullable = true)
    public String getDisadvantage() {
        return disadvantage;
    }

    public void setDisadvantage(String disadvantage) {
        this.disadvantage = disadvantage;
    }

    @Basic
    @Column(name = "strategy", nullable = true)
    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    @Basic
    @Column(name = "remarks", nullable = true, length = 100)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "is_delete", nullable = true, precision = 0)
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompetitorEntity that = (CompetitorEntity) o;
        return Objects.equals(competitorId, that.competitorId) &&
                Objects.equals(company, that.company) &&
                Objects.equals(price, that.price) &&
                Objects.equals(competitiveAbility, that.competitiveAbility) &&
                Objects.equals(competitiveProduct, that.competitiveProduct) &&
                Objects.equals(disadvantage, that.disadvantage) &&
                Objects.equals(strategy, that.strategy) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(isDelete, that.isDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(competitorId, company, price, competitiveAbility, competitiveProduct, disadvantage, strategy, remarks, isDelete);
    }

    @ManyToOne
    @JoinColumn(name = "opp_id", referencedColumnName = "opp_id")
    public SaleOppEntity getTbSaleOppByOppId() {
        return tbSaleOppByOppId;
    }

    public void setTbSaleOppByOppId(SaleOppEntity tbSaleOppByOppId) {
        this.tbSaleOppByOppId = tbSaleOppByOppId;
    }
}
