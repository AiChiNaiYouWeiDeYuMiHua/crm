package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_sale_opp", schema = "USER_CRM", catalog = "")
public class SaleOppEntity {
    private Long oppId;
    private String oppTheme;
    @JSONField(format = "yyyy-MM-dd")
    private Date updateTime;
    private Long oppStatus;
    private String classification;
    @JSONField(format = "yyyy-MM-dd")
    private Date discoveryTime;
    private String oppSource;
    private String customerDemand;
    @JSONField(format = "yyyy-MM-dd")
    private Date expectedTime;
    private String possibility;
    private String intendProduct;
    private Double expectedAmount;
    private Long priority;
    private String stage;
    private String stageRemarks;
    private String trackLog;
    private Long isDelete;
//    @JSONField(serialize = false)
    private Collection<CompetitorEntity> tbCompetitorsByOppId;
//    @JSONField(serialize = false)
    private Collection<DemandEntity> tbDemandsByOppId;
//    @JSONField(serialize = false)
    private Collection<QuoteEntity> tbQuotesByOppId;
    private CustomerEntity tbCustomerByCusId;
    private UserEntity tbUserByUserId;
    private ContactsEntity tbContactsBy联系人Id;
//    @JSONField(serialize = false)
    private Collection<SolutionEntity> tbSolutionsByOppId;
    /*private Collection<TaskActionEntity> tbTaskActionsByOppId;
    private Collection<TravelEntity> tbTravelsByOppId;*/

    @JSONField(format = "yyyy-MM-dd")
    private Date stageStartTime;
    @JSONField(format = "yyyy-MM-dd")
    private Date createTime;

    @Basic
    @Column(name = "create_time", nullable = true)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "stage_start_time", nullable = true)
    public Date getStageStartTime() {
        return stageStartTime;
    }

    public void setStageStartTime(Date stageStartTime) {
        this.stageStartTime = stageStartTime;
    }

    @Id
    @Column(name = "opp_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="myseq")
    @SequenceGenerator(name="myseq",sequenceName="seq",allocationSize=1)
    public Long getOppId() {
        return oppId;
    }

    public void setOppId(Long oppId) {
        this.oppId = oppId;
    }

    @Basic
    @Column(name = "opp_theme", nullable = true, length = 30)
    public String getOppTheme() {
        return oppTheme;
    }

    public void setOppTheme(String oppTheme) {
        this.oppTheme = oppTheme;
    }

    @Basic
    @Column(name = "update_time", nullable = true)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "opp_status", nullable = true, precision = 0)
    public Long getOppStatus() {
        return oppStatus;
    }

    public void setOppStatus(Long oppStatus) {
        this.oppStatus = oppStatus;
    }

    @Basic
    @Column(name = "classification", nullable = true, length = 30)
    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    @Basic
    @Column(name = "discovery_time", nullable = true)
    public Date getDiscoveryTime() {
        return discoveryTime;
    }

    public void setDiscoveryTime(Date discoveryTime) {
        this.discoveryTime = discoveryTime;
    }

    @Basic
    @Column(name = "opp_source", nullable = true, length = 20)
    public String getOppSource() {
        return oppSource;
    }

    public void setOppSource(String oppSource) {
        this.oppSource = oppSource;
    }

    @Basic
    @Column(name = "customer_demand", nullable = true, length = 100)
    public String getCustomerDemand() {
        return customerDemand;
    }

    public void setCustomerDemand(String customerDemand) {
        this.customerDemand = customerDemand;
    }

    @Basic
    @Column(name = "expected_time", nullable = true)
    public Date getExpectedTime() {
        return expectedTime;
    }

    public void setExpectedTime(Date expectedTime) {
        this.expectedTime = expectedTime;
    }

    @Basic
    @Column(name = "possibility", nullable = true, length = 10)
    public String getPossibility() {
        return possibility;
    }

    public void setPossibility(String possibility) {
        this.possibility = possibility;
    }

    @Basic
    @Column(name = "intend_product", nullable = true, length = 50)
    public String getIntendProduct() {
        return intendProduct;
    }

    public void setIntendProduct(String intendProduct) {
        this.intendProduct = intendProduct;
    }

    @Basic
    @Column(name = "expected_amount", nullable = true, precision = 2)
    public Double getExpectedAmount() {
        return expectedAmount;
    }

    public void setExpectedAmount(Double expectedAmount) {
        this.expectedAmount = expectedAmount;
    }

    @Basic
    @Column(name = "priority", nullable = true, precision = 0)
    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    @Basic
    @Column(name = "stage", nullable = true, length = 10)
    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    @Basic
    @Column(name = "stage_remarks", nullable = true)
    public String getStageRemarks() {
        return stageRemarks;
    }

    public void setStageRemarks(String stageRemarks) {
        this.stageRemarks = stageRemarks;
    }

    @Basic
    @Column(name = "track_log", nullable = true)
    public String getTrackLog() {
        return trackLog;
    }

    public void setTrackLog(String trackLog) {
        this.trackLog = trackLog;
    }

    @Basic
    @Column(name = "is_delete", nullable = true, precision = 0)
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaleOppEntity that = (SaleOppEntity) o;
        return Objects.equals(oppId, that.oppId) &&
                Objects.equals(oppTheme, that.oppTheme) &&
                Objects.equals(updateTime, that.updateTime) &&
                Objects.equals(oppStatus, that.oppStatus) &&
                Objects.equals(classification, that.classification) &&
                Objects.equals(discoveryTime, that.discoveryTime) &&
                Objects.equals(oppSource, that.oppSource) &&
                Objects.equals(customerDemand, that.customerDemand) &&
                Objects.equals(expectedTime, that.expectedTime) &&
                Objects.equals(possibility, that.possibility) &&
                Objects.equals(intendProduct, that.intendProduct) &&
                Objects.equals(expectedAmount, that.expectedAmount) &&
                Objects.equals(priority, that.priority) &&
                Objects.equals(stage, that.stage) &&
                Objects.equals(stageRemarks, that.stageRemarks) &&
                Objects.equals(trackLog, that.trackLog) &&
                Objects.equals(isDelete, that.isDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oppId, oppTheme, updateTime, oppStatus, classification, discoveryTime, oppSource, customerDemand, expectedTime, possibility, intendProduct, expectedAmount, priority, stage, stageRemarks, trackLog, isDelete);
    }

    @OneToMany(mappedBy = "tbSaleOppByOppId")
    public Collection<CompetitorEntity> getTbCompetitorsByOppId() {
        return tbCompetitorsByOppId;
    }

    public void setTbCompetitorsByOppId(Collection<CompetitorEntity> tbCompetitorsByOppId) {
        this.tbCompetitorsByOppId = tbCompetitorsByOppId;
    }

    @OneToMany(mappedBy = "tbSaleOppByOppId")
    public Collection<DemandEntity> getTbDemandsByOppId() {
        return tbDemandsByOppId;
    }

    public void setTbDemandsByOppId(Collection<DemandEntity> tbDemandsByOppId) {
        this.tbDemandsByOppId = tbDemandsByOppId;
    }

    @OneToMany(mappedBy = "tbSaleOppByOppId")
    public Collection<QuoteEntity> getTbQuotesByOppId() {
        return tbQuotesByOppId;
    }

    public void setTbQuotesByOppId(Collection<QuoteEntity> tbQuotesByOppId) {
        this.tbQuotesByOppId = tbQuotesByOppId;
    }
    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "联系人id", referencedColumnName = "cots_id")
    public ContactsEntity getTbContactsBy联系人Id() {
        return tbContactsBy联系人Id;
    }

    public void setTbContactsBy联系人Id(ContactsEntity tbContactsBy联系人Id) {
        this.tbContactsBy联系人Id = tbContactsBy联系人Id;
    }

    @OneToMany(mappedBy = "tbSaleOppByOppId")
    public Collection<SolutionEntity> getTbSolutionsByOppId() {
        return tbSolutionsByOppId;
    }

    public void setTbSolutionsByOppId(Collection<SolutionEntity> tbSolutionsByOppId) {
        this.tbSolutionsByOppId = tbSolutionsByOppId;
    }

//    @OneToMany(mappedBy = "tbSaleOppByOppId")
//    public Collection<TaskActionEntity> getTbTaskActionsByOppId() {
//        return tbTaskActionsByOppId;
//    }
//
//    public void setTbTaskActionsByOppId(Collection<TaskActionEntity> tbTaskActionsByOppId) {
//        this.tbTaskActionsByOppId = tbTaskActionsByOppId;
//    }
//
//    @OneToMany(mappedBy = "tbSaleOppByOppId")
//    public Collection<TravelEntity> getTbTravelsByOppId() {
//        return tbTravelsByOppId;
//    }
//
//    public void setTbTravelsByOppId(Collection<TravelEntity> tbTravelsByOppId) {
//        this.tbTravelsByOppId = tbTravelsByOppId;
//    }


    @Override
    public String toString() {
        return "SaleOppEntity{" +
                "oppId=" + oppId +
                ", oppTheme='" + oppTheme + '\'' +
                ", updateTime=" + updateTime +
                ", oppStatus=" + oppStatus +
                ", classification='" + classification + '\'' +
                ", discoveryTime=" + discoveryTime +
                ", oppSource='" + oppSource + '\'' +
                ", customerDemand='" + customerDemand + '\'' +
                ", expectedTime=" + expectedTime +
                ", possibility='" + possibility + '\'' +
                ", intendProduct='" + intendProduct + '\'' +
                ", expectedAmount=" + expectedAmount +
                ", priority=" + priority +
                ", stage='" + stage + '\'' +
                ", stageRemarks='" + stageRemarks + '\'' +
                ", trackLog='" + trackLog + '\'' +
                ", isDelete=" + isDelete +
                ", tbCompetitorsByOppId=" + tbCompetitorsByOppId +
                ", tbDemandsByOppId=" + tbDemandsByOppId +
                ", tbQuotesByOppId=" + tbQuotesByOppId +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbUserByUserId=" + tbUserByUserId +
                ", tbContactsBy联系人Id=" + tbContactsBy联系人Id +
                ", tbSolutionsByOppId=" + tbSolutionsByOppId +
                ", stageStartTime=" + stageStartTime +
                ", createTime=" + createTime +
                '}';
    }
}
