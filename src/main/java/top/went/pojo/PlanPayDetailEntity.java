package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Collection;
import java.sql.Date;
import java.util.Objects;

/**
 * 计划付款表
 */
@Entity
@Table(name = "tb_plan_pay_detail", schema = "USER_CRM", catalog = "")
public class PlanPayDetailEntity {
    private Integer ppdId;
    private BigDecimal ppdMoney;
    @JSONField(format = "yyyy-MM-dd")
    private Date ppdDate;
    private Long ppdTerms;
    private String ppdState;
    private Long magicDelete;
    private Collection<PaymentRecordsEntity> tbPaymentRecordsByPpdId;
    private UserEntity tbUserByUserId;
    private CustomerEntity tbCustomerByCusId;
    private PurchaseEntity tbPurchaseByPurId;

    public PlanPayDetailEntity() {
    }

    public PlanPayDetailEntity(Integer ppdId, BigDecimal ppdMoney, Date ppdDate, Long ppdTerms, String ppdState, Long magicDelete, Collection<PaymentRecordsEntity> tbPaymentRecordsByPpdId, UserEntity tbUserByUserId, CustomerEntity tbCustomerByCusId, PurchaseEntity tbPurchaseByPurId) {
        this.ppdId = ppdId;
        this.ppdMoney = ppdMoney;
        this.ppdDate = ppdDate;
        this.ppdTerms = ppdTerms;
        this.ppdState = ppdState;
        this.magicDelete = magicDelete;
        this.tbPaymentRecordsByPpdId = tbPaymentRecordsByPpdId;
        this.tbUserByUserId = tbUserByUserId;
        this.tbCustomerByCusId = tbCustomerByCusId;
        this.tbPurchaseByPurId = tbPurchaseByPurId;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "ppd_id", nullable = false, precision = 0)
    public Integer getPpdId() {
        return ppdId;
    }

    public void setPpdId(Integer ppdId) {
        this.ppdId = ppdId;
    }

    @Basic
    @Column(name = "ppd_money", nullable = true, precision = 2)
    public BigDecimal getPpdMoney() {
        return ppdMoney;
    }

    public void setPpdMoney(BigDecimal ppdMoney) {
        this.ppdMoney = ppdMoney;
    }

    @Basic
    @Column(name = "ppd_date", nullable = true)
    public Date getPpdDate() {
        return ppdDate;
    }

    public void setPpdDate(Date ppdDate) {
        this.ppdDate = ppdDate;
    }

    @Basic
    @Column(name = "ppd_terms", nullable = true, precision = 0)
    public Long getPpdTerms() {
        return ppdTerms;
    }

    public void setPpdTerms(Long ppdTerms) {
        this.ppdTerms = ppdTerms;
    }


    @Basic
    @Column(name = "ppd_state", nullable = true, length = 20)
    public String getPpdState() {
        return ppdState;
    }

    public void setPpdState(String ppdState) {
        this.ppdState = ppdState;
    }

    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanPayDetailEntity that = (PlanPayDetailEntity) o;
        return Objects.equals(ppdId, that.ppdId) &&
                Objects.equals(ppdMoney, that.ppdMoney) &&
                Objects.equals(ppdDate, that.ppdDate) &&
                Objects.equals(ppdTerms, that.ppdTerms) &&
                Objects.equals(ppdState, that.ppdState) &&
                Objects.equals(magicDelete, that.magicDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ppdId, ppdMoney, ppdDate, ppdTerms, ppdState, magicDelete);
    }

    @OneToMany(mappedBy = "tbPlanPayDetailByPpdId")
    public Collection<PaymentRecordsEntity> getTbPaymentRecordsByPpdId() {
        return tbPaymentRecordsByPpdId;
    }

    public void setTbPaymentRecordsByPpdId(Collection<PaymentRecordsEntity> tbPaymentRecordsByPpdId) {
        this.tbPaymentRecordsByPpdId = tbPaymentRecordsByPpdId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "pur_id", referencedColumnName = "pur_id")
    public PurchaseEntity getTbPurchaseByPurId() {
        return tbPurchaseByPurId;
    }

    public void setTbPurchaseByPurId(PurchaseEntity tbPurchaseByPurId) {
        this.tbPurchaseByPurId = tbPurchaseByPurId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @Override
    public String toString() {
        return "PlanPayDetailEntity{" +
                "ppdId=" + ppdId +
                ", ppdMoney=" + ppdMoney +
                ", ppdDate=" + ppdDate +
                ", ppdTerms=" + ppdTerms +
                ", ppdState='" + ppdState + '\'' +
                ", magicDelete=" + magicDelete +
                ", tbPaymentRecordsByPpdId=" + tbPaymentRecordsByPpdId +
                ", tbUserByUserId=" + tbUserByUserId +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbPurchaseByPurId=" + tbPurchaseByPurId +
                '}';
    }


}
