package top.went.pojo;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_order_detail", schema = "USER_CRM", catalog = "")
public class OrderDetailEntity {
    private Integer odId;
    private Long odNumber;
    private BigDecimal odPrice;
    private BigDecimal odTotal;
    private String odOther;
    private OrderEntity tbOrderByOrderId;
    private ProductFormatEntity formatEntity;
    private BigDecimal odProfit;
    private Date date;
    public OrderDetailEntity() {
    }

    public OrderDetailEntity(Long odNumber, BigDecimal odPrice, BigDecimal odTotal, String odOther,
                             OrderEntity tbOrderByOrderId, ProductFormatEntity formatEntity, BigDecimal odProfit, Date date) {
        this.odNumber = odNumber;
        this.odPrice = odPrice;
        this.odTotal = odTotal;
        this.odOther = odOther;
        this.tbOrderByOrderId = tbOrderByOrderId;
        this.formatEntity = formatEntity;
        this.odProfit = odProfit;
        this.date = date;
    }

    public OrderDetailEntity(OrderEntity tbOrderByOrderId, ProductFormatEntity formatEntity) {
        this.odNumber = 0L;
        this.odPrice = formatEntity.getPfPrice();
        this.odTotal = BigDecimal.ZERO;
        this.tbOrderByOrderId = tbOrderByOrderId;
        this.formatEntity = formatEntity;
        this.odProfit = BigDecimal.ZERO;
    }

    @Id
    @Column(name = "od_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getOdId() {
        return odId;
    }

    public void setOdId(Integer odId) {
        this.odId = odId;
    }

    @Basic
    @Column(name = "od_date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "od_number", nullable = true, precision = 0)
    public Long getOdNumber() {
        return odNumber;
    }

    public void setOdNumber(Long odNumber) {
        this.odNumber = odNumber;
    }

    @Basic
    @Column(name = "od_price", nullable = true, precision = 2)
    public BigDecimal getOdPrice() {
        return odPrice;
    }

    public void setOdPrice(BigDecimal odPrice) {
        this.odPrice = odPrice;
    }


    @Basic
    @Column(name = "od_profit", nullable = true, precision = 2)
    public BigDecimal getOdProfit() {
        return odProfit;
    }

    public void setOdProfit(BigDecimal odProfit) {
        this.odProfit = odProfit;
    }

    @Basic
    @Column(name = "od_total", nullable = true, precision = 2)
    public BigDecimal getOdTotal() {
        return odTotal;
    }

    public void setOdTotal(BigDecimal odTotal) {
        this.odTotal = odTotal;
    }

    @Basic
    @Column(name = "od_other", nullable = true, length = 100)
    public String getOdOther() {
        return odOther;
    }

    public void setOdOther(String odOther) {
        this.odOther = odOther;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderDetailEntity that = (OrderDetailEntity) o;
        return Objects.equals(odId, that.odId) &&
                Objects.equals(odNumber, that.odNumber) &&
                Objects.equals(odPrice, that.odPrice) &&
                Objects.equals(odTotal, that.odTotal) &&
                Objects.equals(odOther, that.odOther);
    }

    @Override
    public int hashCode() {
        return Objects.hash(odId, odNumber, odPrice, odTotal, odOther);
    }

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    public OrderEntity getTbOrderByOrderId() {
        return tbOrderByOrderId;
    }

    public void setTbOrderByOrderId(OrderEntity tbOrderByOrderId) {
        this.tbOrderByOrderId = tbOrderByOrderId;
    }

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "pf_id")
    public ProductFormatEntity getFormatEntity() {
        return formatEntity;
    }

    public void setFormatEntity(ProductFormatEntity formatEntity) {
        this.formatEntity = formatEntity;
    }
}
