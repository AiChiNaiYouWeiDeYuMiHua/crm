package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_demand", schema = "USER_CRM", catalog = "")
public class DemandEntity {
    private Long demandId;
    private String demandTheme;
    @JSONField(format = "yyyy-MM-dd")
    private Date recordTime;
    private Long importance;
    private String demandContent;
    private Long isDelete;
    private SaleOppEntity tbSaleOppByOppId;

    @Id
    @Column(name = "demand_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="myseq")
    @SequenceGenerator(name="myseq",sequenceName="seq",allocationSize=1)
    public Long getDemandId() {
        return demandId;
    }

    public void setDemandId(Long demandId) {
        this.demandId = demandId;
    }

    @Basic
    @Column(name = "demand_theme", nullable = true, length = 30)
    public String getDemandTheme() {
        return demandTheme;
    }

    public void setDemandTheme(String demandTheme) {
        this.demandTheme = demandTheme;
    }

    @Basic
    @Column(name = "record_time", nullable = true)
    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    @Basic
    @Column(name = "importance", nullable = true, precision = 0)
    public Long getImportance() {
        return importance;
    }

    public void setImportance(Long importance) {
        this.importance = importance;
    }

    @Basic
    @Column(name = "demand_content", nullable = true)
    public String getDemandContent() {
        return demandContent;
    }

    public void setDemandContent(String demandContent) {
        this.demandContent = demandContent;
    }

    @Basic
    @Column(name = "is_delete", nullable = true, precision = 0)
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DemandEntity that = (DemandEntity) o;
        return Objects.equals(demandId, that.demandId) &&
                Objects.equals(demandTheme, that.demandTheme) &&
                Objects.equals(recordTime, that.recordTime) &&
                Objects.equals(importance, that.importance) &&
                Objects.equals(demandContent, that.demandContent) &&
                Objects.equals(isDelete, that.isDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(demandId, demandTheme, recordTime, importance, demandContent, isDelete);
    }

    @ManyToOne
    @JoinColumn(name = "opp_id", referencedColumnName = "opp_id")
    public SaleOppEntity getTbSaleOppByOppId() {
        return tbSaleOppByOppId;
    }

    public void setTbSaleOppByOppId(SaleOppEntity tbSaleOppByOppId) {
        this.tbSaleOppByOppId = tbSaleOppByOppId;
    }

    @Override
    public String toString() {
        return "DemandEntity{" +
                "demandId=" + demandId +
                ", demandTheme='" + demandTheme + '\'' +
                ", recordTime=" + recordTime +
                ", importance=" + importance +
                ", demandContent='" + demandContent + '\'' +
                ", isDelete=" + isDelete +
                ", tbSaleOppByOppId=" + tbSaleOppByOppId +
                '}';
    }

}
