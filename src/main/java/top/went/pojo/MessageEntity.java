package top.went.pojo;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_message", schema = "USER_CRM", catalog = "")
public class MessageEntity {
    private Integer messageId;
    private String messageType;
    private String messageContent;
    private Time messageTime;
    private RoleEntity tbRoleByRoleId;

    @Id
    @Column(name = "message_id", nullable = false, precision = 0)
    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    @Basic
    @Column(name = "message_type", nullable = true, length = 20)
    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    @Basic
    @Column(name = "message_content", nullable = true, length = 100)
    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    @Basic
    @Column(name = "message_time", nullable = true)
    public Time getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(Time messageTime) {
        this.messageTime = messageTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageEntity that = (MessageEntity) o;
        return Objects.equals(messageId, that.messageId) &&
                Objects.equals(messageType, that.messageType) &&
                Objects.equals(messageContent, that.messageContent) &&
                Objects.equals(messageTime, that.messageTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageId, messageType, messageContent, messageTime);
    }

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    public RoleEntity getTbRoleByRoleId() {
        return tbRoleByRoleId;
    }

    public void setTbRoleByRoleId(RoleEntity tbRoleByRoleId) {
        this.tbRoleByRoleId = tbRoleByRoleId;
    }
}
