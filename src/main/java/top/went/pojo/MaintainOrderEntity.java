package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_maintain_order", schema = "USER_CRM", catalog = "")
public class MaintainOrderEntity {
    private Integer mtId;
    private String mtTheme;
    @JSONField(format = "yyyy-MM-dd")
    private Date mtDate;
    @JSONField(format = "yyyy-MM-dd")
    private Date mtCompleteDate;
    private String mtSchedule;
    private Double mtPay;
    private String mtState;
    private String mtAccessories;
    private Long magicDelete;
    private Long mtIsProtect;
    private String mtDescript;
    @JSONField(format = "yyyy-MM-dd")
    private Date mtProDate;
    private String mtExtract;
    private String mtTel;
    private String mtName;
    private CustomerEntity tbCustomerByCusId;
    private UserEntity tbUserByUserId;
    private Collection<PayBackDetailEntity> tbPayBackDetailsByMtId;
    private Collection<PlanPayBackEntity> tbPlanPayBacksByMtId;
    private ProductFormatEntity tbProductByProductId;
    private OrderEntity orderEntity;

    @Id
    @Column(name = "mt_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getMtId() {
        return mtId;
    }

    public void setMtId(Integer mtId) {
        this.mtId = mtId;
    }

    @Basic
    @Column(name = "mt_theme", nullable = true, length = 20)
    public String getMtTheme() {
        return mtTheme;
    }

    public void setMtTheme(String mtTheme) {
        this.mtTheme = mtTheme;
    }

    @Basic
    @Column(name = "mt_date", nullable = true)
    public Date getMtDate() {
        return mtDate;
    }

    public void setMtDate(Date mtDate) {
        this.mtDate = mtDate;
    }

    @Basic
    @Column(name = "mt_complete_date", nullable = true)
    public Date getMtCompleteDate() {
        return mtCompleteDate;
    }

    public void setMtCompleteDate(Date mtCompleteDate) {
        this.mtCompleteDate = mtCompleteDate;
    }

    @Basic
    @Column(name = "mt_schedule", nullable = true, length = 20)
    public String getMtSchedule() {
        return mtSchedule;
    }

    public void setMtSchedule(String mtSchedule) {
        this.mtSchedule = mtSchedule;
    }

    @Basic
    @Column(name = "mt_pay", nullable = true, precision = 2)
    public Double getMtPay() {
        return mtPay;
    }

    public void setMtPay(Double mtPay) {
        this.mtPay = mtPay;
    }

    @Basic
    @Column(name = "mt_state", nullable = true, length = 20)
    public String getMtState() {
        return mtState;
    }

    public void setMtState(String mtState) {
        this.mtState = mtState;
    }

    @Basic
    @Column(name = "mt_accessories", nullable = true, length = 100)
    public String getMtAccessories() {
        return mtAccessories;
    }

    public void setMtAccessories(String mtAccessories) {
        this.mtAccessories = mtAccessories;
    }

    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    @Basic
    @Column(name = "mt_is_protect", nullable = true, precision = 0)
    public Long getMtIsProtect() {
        return mtIsProtect;
    }

    public void setMtIsProtect(Long mtIsProtect) {
        this.mtIsProtect = mtIsProtect;
    }

    @Basic
    @Column(name = "mt_descript", nullable = true, length = 100)
    public String getMtDescript() {
        return mtDescript;
    }

    public void setMtDescript(String mtDescript) {
        this.mtDescript = mtDescript;
    }

    @Basic
    @Column(name = "mt_pro_date", nullable = true)
    public Date getMtProDate() {
        return mtProDate;
    }

    public void setMtProDate(Date mtProDate) {
        this.mtProDate = mtProDate;
    }

    @Basic
    @Column(name = "mt_extract", nullable = true, length = 100)
    public String getMtExtract() {
        return mtExtract;
    }

    public void setMtExtract(String mtExtract) {
        this.mtExtract = mtExtract;
    }

    @Basic
    @Column(name = "mt_tel", nullable = true, length = 20)
    public String getMtTel() {
        return mtTel;
    }

    public void setMtTel(String mtTel) {
        this.mtTel = mtTel;
    }

    @Basic
    @Column(name = "mt_name", nullable = true, length = 20)
    public String getMtName() {
        return mtName;
    }

    public void setMtName(String mtName) {
        this.mtName = mtName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MaintainOrderEntity that = (MaintainOrderEntity) o;
        return Objects.equals(mtId, that.mtId) &&
                Objects.equals(mtTheme, that.mtTheme) &&
                Objects.equals(mtDate, that.mtDate) &&
                Objects.equals(mtCompleteDate, that.mtCompleteDate) &&
                Objects.equals(mtSchedule, that.mtSchedule) &&
                Objects.equals(mtPay, that.mtPay) &&
                Objects.equals(mtState, that.mtState) &&
                Objects.equals(mtAccessories, that.mtAccessories) &&
                Objects.equals(magicDelete, that.magicDelete) &&
                Objects.equals(mtIsProtect, that.mtIsProtect) &&
                Objects.equals(mtDescript, that.mtDescript) &&
                Objects.equals(mtProDate, that.mtProDate) &&
                Objects.equals(mtExtract, that.mtExtract) &&
                Objects.equals(mtTel, that.mtTel) &&
                Objects.equals(mtName, that.mtName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mtId, mtTheme, mtDate, mtCompleteDate, mtSchedule, mtPay, mtState, mtAccessories, magicDelete, mtIsProtect, mtDescript, mtProDate, mtExtract, mtTel, mtName);
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @OneToMany(mappedBy = "tbMaintainOrderByMtId")
    public Collection<PayBackDetailEntity> getTbPayBackDetailsByMtId() {
        return tbPayBackDetailsByMtId;
    }

    public void setTbPayBackDetailsByMtId(Collection<PayBackDetailEntity> tbPayBackDetailsByMtId) {
        this.tbPayBackDetailsByMtId = tbPayBackDetailsByMtId;
    }

    @OneToMany(mappedBy = "tbMaintainOrderByMtId")
    public Collection<PlanPayBackEntity> getTbPlanPayBacksByMtId() {
        return tbPlanPayBacksByMtId;
    }

    public void setTbPlanPayBacksByMtId(Collection<PlanPayBackEntity> tbPlanPayBacksByMtId) {
        this.tbPlanPayBacksByMtId = tbPlanPayBacksByMtId;
    }
    @ManyToOne
    @JoinColumn(name = "pf_id", referencedColumnName = "pf_id")
    public ProductFormatEntity getTbProductByProductId() {
        return tbProductByProductId;
    }

    public void setTbProductByProductId(ProductFormatEntity tbProductByProductId) {
        this.tbProductByProductId = tbProductByProductId;
    }

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    public OrderEntity getOrderEntity() {
        return orderEntity;
    }

    public void setOrderEntity(OrderEntity orderEntity) {
        this.orderEntity = orderEntity;
    }
}
