package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import sun.plugin2.message.Serializer;

import javax.persistence.*;
import java.sql.Time;
import java.util.Collection;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_customer", schema = "USER_CRM")
public class CustomerEntity {
    private Integer cusId;
    private String cusName;
    private String cusAbbreviation;
    private String cusType;
    private String cusProprietor;
    private String cusLifecycle;
    private String cusLevel;
    private String cusCome;
    private String cusStage;
    private String cusIndustry;
    private String cusCredit;
    private Long cusPhone;
    private String cusNet;
    private Long cusAdvance;
    private String cusAddr;
    @JSONField(format = "yyyy-MM-dd")
    private Date cusDate;
    private Date cusUpdataDate;
    private String cusDetermine;
    private String cusClass;
    private Long cusAdvanceMoney;
    private Integer cusAdvanceNum;
    private String cusExistDate;
    private String cusDcStatus;
    private String cusKnot;
    private Long cusIsDelete;
    private String cusSignDate;
    private String cusSynopsis;
    private String cusArea;
    private String cusRemark;
    private String cusMSNQQ;
    @JSONField(format = "yyyy-MM-dd")
    private Date cusCreateDate;
    private String cusFiles;
//    private Collection<AddrEntity> tbAddrsByCusId;
//    private Collection<CareEntity> tbCaresByCusId;
//    private Collection<ComplaintEntity> tbComplaintsByCusId;
    @JSONField(serialize = false)
    private Collection<ContactsEntity> tbContactsByCusId;
    private ServeEntity tbServeByServeId;
    private UserEntity tbUserByUserId;
    private AddrEntity tbAddrByAddrId;
//    private Collection<DeliverDetailEntity> tbDeliverDetailsByCusId;
//    private Collection<MaintainOrderEntity> tbMaintainOrdersByCusId;
//    private Collection<MemorialDayEntity> tbMemorialDaysByCusId;
//    private Collection<OrderEntity> tbOrdersByCusId;
//    private Collection<PaymentRecordsEntity> tbPaymentRecordsByCusId;
//    private Collection<PlanPayBackEntity> tbPlanPayBacksByCusId;
//    private Collection<PlanPayDetailEntity> tbPlanPayDetailsByCusId;
//    private Collection<ProjectEntity> projectEntities;
//    private Collection<PurchaseEntity> tbPurchasesByCusId;
//    private Collection<PurchaseReceiptEntity> tbPurchaseReceiptsByCusId;
//    private Collection<QuoteEntity> tbQuotesByCusId;
//    private Collection<ReceiptRecordsEntity> tbReceiptRecordsByCusId;
//    private Collection<ReturnGoodsEntity> tbReturnGoodsByCusId;
//    private Collection<SaleOppEntity> tbSaleOppsByCusId;
//    private Collection<ServeEntity> tbServesByCusId;
//    private Collection<TaskActionEntity> tbTaskActionsByCusId;
//    private Collection<TravelEntity> tbTravelsByCusId;
//    private Collection<WpDetailEntity> tbWpDetailsByCusId;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "cus_id", nullable = false, precision = 0)
    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    @Basic
    @Column(name = "cus_name", nullable = true, length = 50)
    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    @Basic
    @Column(name = "cus_abbreviation", nullable = true, length = 50)
    public String getCusAbbreviation() {
        return cusAbbreviation;
    }

    public void setCusAbbreviation(String cusAbbreviation) {
        this.cusAbbreviation = cusAbbreviation;
    }

    @Basic
    @Column(name = "cus_type", nullable = true, length = 10)
    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    @Basic
    @Column(name = "cus_proprietor", nullable = true, length = 20)
    public String getCusProprietor() {
        return cusProprietor;
    }

    public void setCusProprietor(String cusProprietor) {
        this.cusProprietor = cusProprietor;
    }

    @Basic
    @Column(name = "cus_lifecycle", nullable = true, length = 10)
    public String getCusLifecycle() {
        return cusLifecycle;
    }

    public void setCusLifecycle(String cusLifecycle) {
        this.cusLifecycle = cusLifecycle;
    }

    @Basic
    @Column(name = "cus_level", nullable = true, length = 10)
    public String getCusLevel() {
        return cusLevel;
    }

    public void setCusLevel(String cusLevel) {
        this.cusLevel = cusLevel;
    }

    @Basic
    @Column(name = "cus_come", nullable = true, length = 100)
    public String getCusCome() {
        return cusCome;
    }

    public void setCusCome(String cusCome) {
        this.cusCome = cusCome;
    }

    @Basic
    @Column(name = "cus_stage", nullable = true, length = 20)
    public String getCusStage() {
        return cusStage;
    }

    public void setCusStage(String cusStage) {
        this.cusStage = cusStage;
    }

    @Basic
    @Column(name = "cus_industry", nullable = true, length = 50)
    public String getCusIndustry() {
        return cusIndustry;
    }

    public void setCusIndustry(String cusIndustry) {
        this.cusIndustry = cusIndustry;
    }

    @Basic
    @Column(name = "cus_credit", nullable = true, length = 10)
    public String getCusCredit() {
        return cusCredit;
    }

    public void setCusCredit(String cusCredit) {
        this.cusCredit = cusCredit;
    }

    @Basic
    @Column(name = "cus_phone", nullable = true, precision = 0)
    public Long getCusPhone() {
        return cusPhone;
    }

    public void setCusPhone(Long cusPhone) {
        this.cusPhone = cusPhone;
    }

    @Basic
    @Column(name = "cus_net", nullable = true, length = 100)
    public String getCusNet() {
        return cusNet;
    }

    public void setCusNet(String cusNet) {
        this.cusNet = cusNet;
    }

    @Basic
    @Column(name = "cus_advance", nullable = true, precision = 0)
    public Long getCusAdvance() {
        return cusAdvance;
    }

    public void setCusAdvance(Long cusAdvance) {
        this.cusAdvance = cusAdvance;
    }

    @Basic
    @Column(name = "cus_addr", nullable = true, length = 30)
    public String getCusAddr() {
        return cusAddr;
    }

    public void setCusAddr(String cusAddr) {
        this.cusAddr = cusAddr;
    }

    @Basic
    @Column(name = "cus_date", nullable = true)
    public Date getCusDate() {
        return cusDate;
    }

    public void setCusDate(Date cusDate) {
        this.cusDate = cusDate;
    }

    @Basic
    @Column(name = "cus_updata_date", nullable = true)
    public Date getCusUpdataDate() {
        return cusUpdataDate;
    }

    public void setCusUpdataDate(Date cusUpdataDate) {
        this.cusUpdataDate = cusUpdataDate;
    }

    @Basic
    @Column(name = "cus_create_date", nullable = true)
    public Date getCusCreateDate() {
        return cusCreateDate;
    }

    public void setCusCreateDate(Date cusCreateDate) {
        this.cusCreateDate = cusCreateDate;
    }

    @Basic
    @Column(name = "cus_determine", nullable = true, length = 10)
    public String getCusDetermine() {
        return cusDetermine;
    }

    public void setCusDetermine(String cusDetermine) {
        this.cusDetermine = cusDetermine;
    }

    @Basic
    @Column(name = "cus_class", nullable = true, length = 10)
    public String getCusClass() {
        return cusClass;
    }

    public void setCusClass(String cusClass) {
        this.cusClass = cusClass;
    }

    @Basic
    @Column(name = "cus_advance_money", nullable = true, precision = 0)
    public Long getCusAdvanceMoney() {
        return cusAdvanceMoney;
    }

    public void setCusAdvanceMoney(Long cusAdvanceMoney) {
        this.cusAdvanceMoney = cusAdvanceMoney;
    }

    @Basic
    @Column(name = "cus_advance_num", nullable = true, precision = 0)
    public Integer getCusAdvanceNum() {
        return cusAdvanceNum;
    }

    public void setCusAdvanceNum(Integer cusAdvanceNum) {
        this.cusAdvanceNum = cusAdvanceNum;
    }

    @Basic
    @Column(name = "cus_exist_date", nullable = true, precision = 0)
    public String getCusExistDate() {
        return cusExistDate;
    }

    public void setCusExistDate(String cusExistDate) {
        this.cusExistDate = cusExistDate;
    }

    @Basic
    @Column(name = "cus_dc_status", nullable = true, length = 10)
    public String getCusDcStatus() {
        return cusDcStatus;
    }

    public void setCusDcStatus(String cusDcStatus) {
        this.cusDcStatus = cusDcStatus;
    }

    @Basic
    @Column(name = "cus_knot", nullable = true, length = 20)
    public String getCusKnot() {
        return cusKnot;
    }

    public void setCusKnot(String cusKnot) {
        this.cusKnot = cusKnot;
    }

    @Basic
    @Column(name = "cus_is_delete", nullable = true, precision = 0)
    public Long getCusIsDelete() {
        return cusIsDelete;
    }

    public void setCusIsDelete(Long cusIsDelete) {
        this.cusIsDelete = cusIsDelete;
    }

    @Basic
    @Column(name = "cus_sign_date", nullable = true, length = 20)
    public String getCusSignDate() {
        return cusSignDate;
    }

    public void setCusSignDate(String cusSignDate) {
        this.cusSignDate = cusSignDate;
    }

    @Basic
    @Column(name = "cus_synopsis", nullable = true, length = 2000)
    public String getCusSynopsis() {
        return cusSynopsis;
    }

    public void setCusSynopsis(String cusSynopsis) {
        this.cusSynopsis = cusSynopsis;
    }

    @Basic
    @Column(name = "cus_area", nullable = true, length = 100)
    public String getCusArea() {
        return cusArea;
    }

    public void setCusArea(String cusArea) {
        this.cusArea = cusArea;
    }

    @Basic
    @Column(name = "cus_remark", nullable = true, length = 1000)
    public String getCusRemark() {
        return cusRemark;
    }

    public void setCusRemark(String cusRemark) {
        this.cusRemark = cusRemark;
    }

    @Basic
    @Column(name = "cus_msnqq", nullable = true, length = 1000)
    public String getCusMSNQQ() {
        return cusMSNQQ;
    }

    public void setCusMSNQQ(String cusMSNQQ) {
        this.cusMSNQQ = cusMSNQQ;
    }

    @Basic
    @Column(name = "cus_files", nullable = true)
    public String getCusFiles() {
        return cusFiles;
    }

    public void setCusFiles(String cusFiles) {
        this.cusFiles = cusFiles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerEntity that = (CustomerEntity) o;
        return Objects.equals(cusId, that.cusId) &&
                Objects.equals(cusName, that.cusName) &&
                Objects.equals(cusAbbreviation, that.cusAbbreviation) &&
                Objects.equals(cusType, that.cusType) &&
                Objects.equals(cusProprietor, that.cusProprietor) &&
                Objects.equals(cusLifecycle, that.cusLifecycle) &&
                Objects.equals(cusLevel, that.cusLevel) &&
                Objects.equals(cusCome, that.cusCome) &&
                Objects.equals(cusStage, that.cusStage) &&
                Objects.equals(cusIndustry, that.cusIndustry) &&
                Objects.equals(cusCredit, that.cusCredit) &&
                Objects.equals(cusPhone, that.cusPhone) &&
                Objects.equals(cusNet, that.cusNet) &&
                Objects.equals(cusAdvance, that.cusAdvance) &&
                Objects.equals(cusAddr, that.cusAddr) &&
                Objects.equals(cusDate, that.cusDate) &&
                Objects.equals(cusUpdataDate, that.cusUpdataDate) &&
                Objects.equals(cusDetermine, that.cusDetermine) &&
                Objects.equals(cusClass, that.cusClass) &&
                Objects.equals(cusAdvanceMoney, that.cusAdvanceMoney) &&
                Objects.equals(cusAdvanceNum, that.cusAdvanceNum) &&
                Objects.equals(cusExistDate, that.cusExistDate) &&
                Objects.equals(cusDcStatus, that.cusDcStatus) &&
                Objects.equals(cusKnot, that.cusKnot);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cusId, cusName, cusAbbreviation, cusType, cusProprietor, cusLifecycle, cusLevel, cusCome, cusStage, cusIndustry, cusCredit, cusPhone, cusNet, cusAdvance, cusAddr, cusDate, cusUpdataDate, cusDetermine, cusClass, cusAdvanceMoney, cusAdvanceNum, cusExistDate, cusDcStatus, cusKnot);
    }

    @Override
    public String toString() {
        return "CustomerEntity{" +
                "cusId=" + cusId +
                ", cusName='" + cusName + '\'' +
                ", cusAbbreviation='" + cusAbbreviation + '\'' +
                ", cusType='" + cusType + '\'' +
                ", cusProprietor='" + cusProprietor + '\'' +
                ", cusLifecycle='" + cusLifecycle + '\'' +
                ", cusLevel='" + cusLevel + '\'' +
                ", cusCome='" + cusCome + '\'' +
                ", cusStage='" + cusStage + '\'' +
                ", cusIndustry='" + cusIndustry + '\'' +
                ", cusCredit='" + cusCredit + '\'' +
                ", cusPhone=" + cusPhone +
                ", cusNet='" + cusNet + '\'' +
                ", cusAdvance=" + cusAdvance +
                ", cusAddr='" + cusAddr + '\'' +
                ", cusDate=" + cusDate +
                ", cusUpdataDate=" + cusUpdataDate +
                ", cusDetermine='" + cusDetermine + '\'' +
                ", cusClass='" + cusClass + '\'' +
                ", cusAdvanceMoney=" + cusAdvanceMoney +
                ", cusAdvanceNum=" + cusAdvanceNum +
                ", cusExistDate=" + cusExistDate +
                ", cusDcStatus='" + cusDcStatus + '\'' +
                ", cusKnot='" + cusKnot + '\'' +
                ", tbServeByServeId=" + tbServeByServeId +
                ", tbUserByUserId=" + tbUserByUserId +
                ", tbAddrByAddrId=" + tbAddrByAddrId +
                '}';
    }

    //    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<AddrEntity> getTbAddrsByCusId() {
//        return tbAddrsByCusId;
//    }
//
//    public void setTbAddrsByCusId(Collection<AddrEntity> tbAddrsByCusId) {
//        this.tbAddrsByCusId = tbAddrsByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<CareEntity> getTbCaresByCusId() {
//        return tbCaresByCusId;
//    }
//
//    public void setTbCaresByCusId(Collection<CareEntity> tbCaresByCusId) {
//        this.tbCaresByCusId = tbCaresByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<ComplaintEntity> getTbComplaintsByCusId() {
//        return tbComplaintsByCusId;
//    }
//
//    public void setTbComplaintsByCusId(Collection<ComplaintEntity> tbComplaintsByCusId) {
//        this.tbComplaintsByCusId = tbComplaintsByCusId;
//    }
//
    @OneToMany(mappedBy = "tbCustomerByCusId")
    public Collection<ContactsEntity> getTbContactsByCusId() {
        return tbContactsByCusId;
    }

    public void setTbContactsByCusId(Collection<ContactsEntity> tbContactsByCusId) {
        this.tbContactsByCusId = tbContactsByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "serve_id", referencedColumnName = "serve_id")
    public ServeEntity getTbServeByServeId() {
        return tbServeByServeId;
    }

    public void setTbServeByServeId(ServeEntity tbServeByServeId) {
        this.tbServeByServeId = tbServeByServeId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "addr_id", referencedColumnName = "addr_id")
    public AddrEntity getTbAddrByAddrId() {
        return tbAddrByAddrId;
    }

    public void setTbAddrByAddrId(AddrEntity tbAddrByAddrId) {
        this.tbAddrByAddrId = tbAddrByAddrId;
    }

//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<DeliverDetailEntity> getTbDeliverDetailsByCusId() {
//        return tbDeliverDetailsByCusId;
//    }
//
//    public void setTbDeliverDetailsByCusId(Collection<DeliverDetailEntity> tbDeliverDetailsByCusId) {
//        this.tbDeliverDetailsByCusId = tbDeliverDetailsByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<MaintainOrderEntity> getTbMaintainOrdersByCusId() {
//        return tbMaintainOrdersByCusId;
//    }
//
//    public void setTbMaintainOrdersByCusId(Collection<MaintainOrderEntity> tbMaintainOrdersByCusId) {
//        this.tbMaintainOrdersByCusId = tbMaintainOrdersByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<MemorialDayEntity> getTbMemorialDaysByCusId() {
//        return tbMemorialDaysByCusId;
//    }
//
//    public void setTbMemorialDaysByCusId(Collection<MemorialDayEntity> tbMemorialDaysByCusId) {
//        this.tbMemorialDaysByCusId = tbMemorialDaysByCusId;
//    }

//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<OrderEntity> getTbOrdersByCusId() {
//        return tbOrdersByCusId;
//    }
//
//    public void setTbOrdersByCusId(Collection<OrderEntity> tbOrdersByCusId) {
//        this.tbOrdersByCusId = tbOrdersByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<PaymentRecordsEntity> getTbPaymentRecordsByCusId() {
//        return tbPaymentRecordsByCusId;
//    }
//
//    public void setTbPaymentRecordsByCusId(Collection<PaymentRecordsEntity> tbPaymentRecordsByCusId) {
//        this.tbPaymentRecordsByCusId = tbPaymentRecordsByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<PlanPayBackEntity> getTbPlanPayBacksByCusId() {
//        return tbPlanPayBacksByCusId;
//    }
//
//    public void setTbPlanPayBacksByCusId(Collection<PlanPayBackEntity> tbPlanPayBacksByCusId) {
//        this.tbPlanPayBacksByCusId = tbPlanPayBacksByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<PlanPayDetailEntity> getTbPlanPayDetailsByCusId() {
//        return tbPlanPayDetailsByCusId;
//    }
//
//    public void setTbPlanPayDetailsByCusId(Collection<PlanPayDetailEntity> tbPlanPayDetailsByCusId) {
//        this.tbPlanPayDetailsByCusId = tbPlanPayDetailsByCusId;
//    }
//
//    @ManyToMany(mappedBy = "customerEntities")
//    public Collection<ProjectEntity> getProjectEntities() {
//        return projectEntities;
//    }
//
//    public void setProjectEntities(Collection<ProjectEntity> projectEntities) {
//        this.projectEntities = projectEntities;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<PurchaseEntity> getTbPurchasesByCusId() {
//        return tbPurchasesByCusId;
//    }
//
//    public void setTbPurchasesByCusId(Collection<PurchaseEntity> tbPurchasesByCusId) {
//        this.tbPurchasesByCusId = tbPurchasesByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<PurchaseReceiptEntity> getTbPurchaseReceiptsByCusId() {
//        return tbPurchaseReceiptsByCusId;
//    }
//
//    public void setTbPurchaseReceiptsByCusId(Collection<PurchaseReceiptEntity> tbPurchaseReceiptsByCusId) {
//        this.tbPurchaseReceiptsByCusId = tbPurchaseReceiptsByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<QuoteEntity> getTbQuotesByCusId() {
//        return tbQuotesByCusId;
//    }
//
//    public void setTbQuotesByCusId(Collection<QuoteEntity> tbQuotesByCusId) {
//        this.tbQuotesByCusId = tbQuotesByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<ReceiptRecordsEntity> getTbReceiptRecordsByCusId() {
//        return tbReceiptRecordsByCusId;
//    }
//
//    public void setTbReceiptRecordsByCusId(Collection<ReceiptRecordsEntity> tbReceiptRecordsByCusId) {
//        this.tbReceiptRecordsByCusId = tbReceiptRecordsByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<ReturnGoodsEntity> getTbReturnGoodsByCusId() {
//        return tbReturnGoodsByCusId;
//    }
//
//    public void setTbReturnGoodsByCusId(Collection<ReturnGoodsEntity> tbReturnGoodsByCusId) {
//        this.tbReturnGoodsByCusId = tbReturnGoodsByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<SaleOppEntity> getTbSaleOppsByCusId() {
//        return tbSaleOppsByCusId;
//    }
//
//    public void setTbSaleOppsByCusId(Collection<SaleOppEntity> tbSaleOppsByCusId) {
//        this.tbSaleOppsByCusId = tbSaleOppsByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<ServeEntity> getTbServesByCusId() {
//        return tbServesByCusId;
//    }
//
//    public void setTbServesByCusId(Collection<ServeEntity> tbServesByCusId) {
//        this.tbServesByCusId = tbServesByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<TaskActionEntity> getTbTaskActionsByCusId() {
//        return tbTaskActionsByCusId;
//    }
//
//    public void setTbTaskActionsByCusId(Collection<TaskActionEntity> tbTaskActionsByCusId) {
//        this.tbTaskActionsByCusId = tbTaskActionsByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<TravelEntity> getTbTravelsByCusId() {
//        return tbTravelsByCusId;
//    }
//
//    public void setTbTravelsByCusId(Collection<TravelEntity> tbTravelsByCusId) {
//        this.tbTravelsByCusId = tbTravelsByCusId;
//    }
//
//    @OneToMany(mappedBy = "tbCustomerByCusId")
//    public Collection<WpDetailEntity> getTbWpDetailsByCusId() {
//        return tbWpDetailsByCusId;
//    }
//
//    public void setTbWpDetailsByCusId(Collection<WpDetailEntity> tbWpDetailsByCusId) {
//        this.tbWpDetailsByCusId = tbWpDetailsByCusId;
//    }
}
