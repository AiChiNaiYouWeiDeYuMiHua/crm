package top.went.pojo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_wh_put", schema = "USER_CRM", catalog = "")
public class WhPutEntity {
    private Integer wpId;
    private String wpName;
    private Date wpDate;
    private Integer wpStatus;
    private Date wpExecute;
    private String wpOther;
    private Boolean logicDelete;
    private WarehouseEntity tbWarehouseByWhId;
    private UserEntity tbUserByUserId;
    private ReturnGoodsEntity tbReturnGoodsByRgId;
    private UserEntity tbUserByTbUserId;
    private PurchaseEntity tbPurchaseByPurId;

    public WhPutEntity() {
    }

    public WhPutEntity(String wpName, Date wpDate, Integer wpStatus, String wpOther, Boolean logicDelete,
                       WarehouseEntity tbWarehouseByWhId, UserEntity tbUserByUserId, ReturnGoodsEntity tbReturnGoodsByRgId) {
        this.wpName = wpName;
        this.wpDate = wpDate;
        this.wpStatus = wpStatus;
        this.wpOther = wpOther;
        this.logicDelete = logicDelete;
        this.tbWarehouseByWhId = tbWarehouseByWhId;
        this.tbUserByUserId = tbUserByUserId;
        this.tbReturnGoodsByRgId = tbReturnGoodsByRgId;
    }

    public WhPutEntity(String wpName, Date wpDate, Integer wpStatus, String wpOther, Boolean logicDelete,
                       WarehouseEntity tbWarehouseByWhId, UserEntity tbUserByUserId, PurchaseEntity tbPurchaseByPurId) {
        this.wpName = wpName;
        this.wpDate = wpDate;
        this.wpStatus = wpStatus;
        this.wpOther = wpOther;
        this.logicDelete = logicDelete;
        this.tbWarehouseByWhId = tbWarehouseByWhId;
        this.tbUserByUserId = tbUserByUserId;
        this.tbPurchaseByPurId = tbPurchaseByPurId;
    }

    @Id
    @Column(name = "wp_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getWpId() {
        return wpId;
    }

    public void setWpId(Integer wpId) {
        this.wpId = wpId;
    }

    @Basic
    @Column(name = "wp_name", nullable = true, length = 30)
    public String getWpName() {
        return wpName;
    }

    public void setWpName(String wpName) {
        this.wpName = wpName;
    }

    @Basic
    @Column(name = "wp_date", nullable = true)
    public Date getWpDate() {
        return wpDate;
    }

    public void setWpDate(Date wpDate) {
        this.wpDate = wpDate;
    }

    @Basic
    @Column(name = "wp_status", nullable = true, precision = 0)
    public Integer getWpStatus() {
        return wpStatus;
    }

    public void setWpStatus(Integer wpStatus) {
        this.wpStatus = wpStatus;
    }

    @Basic
    @Column(name = "wp_execute", nullable = true)
    public Date getWpExecute() {
        return wpExecute;
    }

    public void setWpExecute(Date wpExecute) {
        this.wpExecute = wpExecute;
    }

    @Basic
    @Column(name = "wp_other", nullable = true, length = 100)
    public String getWpOther() {
        return wpOther;
    }

    public void setWpOther(String wpOther) {
        this.wpOther = wpOther;
    }

    @Basic
    @Column(name = "logic_delete", nullable = true, precision = 0)
    public Boolean getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Boolean logicDelete) {
        this.logicDelete = logicDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WhPutEntity that = (WhPutEntity) o;
        return Objects.equals(wpId, that.wpId) &&
                Objects.equals(wpName, that.wpName) &&
                Objects.equals(wpDate, that.wpDate) &&
                Objects.equals(wpStatus, that.wpStatus) &&
                Objects.equals(wpExecute, that.wpExecute) &&
                Objects.equals(wpOther, that.wpOther) &&
                Objects.equals(logicDelete, that.logicDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wpId, wpName, wpDate, wpStatus, wpExecute, wpOther, logicDelete);
    }

    @ManyToOne
    @JoinColumn(name = "wh_id", referencedColumnName = "wh_id")
    public WarehouseEntity getTbWarehouseByWhId() {
        return tbWarehouseByWhId;
    }

    public void setTbWarehouseByWhId(WarehouseEntity tbWarehouseByWhId) {
        this.tbWarehouseByWhId = tbWarehouseByWhId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "rg_id", referencedColumnName = "rg_id")
    public ReturnGoodsEntity getTbReturnGoodsByRgId() {
        return tbReturnGoodsByRgId;
    }

    public void setTbReturnGoodsByRgId(ReturnGoodsEntity tbReturnGoodsByRgId) {
        this.tbReturnGoodsByRgId = tbReturnGoodsByRgId;
    }

    @ManyToOne
    @JoinColumn(name = "tb__user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByTbUserId() {
        return tbUserByTbUserId;
    }

    public void setTbUserByTbUserId(UserEntity tbUserByTbUserId) {
        this.tbUserByTbUserId = tbUserByTbUserId;
    }

    @ManyToOne
    @JoinColumn(name = "pur_id", referencedColumnName = "pur_id")
    public PurchaseEntity getTbPurchaseByPurId() {
        return tbPurchaseByPurId;
    }

    public void setTbPurchaseByPurId(PurchaseEntity tbPurchaseByPurId) {
        this.tbPurchaseByPurId = tbPurchaseByPurId;
    }
}
