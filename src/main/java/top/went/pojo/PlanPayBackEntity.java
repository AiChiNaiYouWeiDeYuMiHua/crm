package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Collection;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_plan_pay_back", schema = "USER_CRM", catalog = "")
public class PlanPayBackEntity {
    private Integer ppbId;
    private BigDecimal ppbMoney;
    @JSONField(format = "yyyy-MM-dd")
    private Date ppbDate;
    private Long ppbTerms;
    private Long magicDelete;
    private Long ppbType;

    private Collection<PayBackDetailEntity> tbPayBackDetailsByPpbId;

    private UserEntity tbUserByUserId;

    private CustomerEntity tbCustomerByCusId;

    private MaintainOrderEntity tbMaintainOrderByMtId;

    private OrderEntity tbOrderByOrderId;

    @Id
    @Column(name = "ppb_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getPpbId() {
        return ppbId;
    }

    public void setPpbId(Integer ppbId) {
        this.ppbId = ppbId;
    }

    @Basic
    @Column(name = "ppb_money", nullable = true, precision = 2)
    public BigDecimal getPpbMoney() {
        return ppbMoney;
    }

    public void setPpbMoney(BigDecimal ppbMoney) {
        this.ppbMoney = ppbMoney;
    }

    @Basic
    @Column(name = "ppb_date", nullable = true)
    public Date getPpbDate() {
        return ppbDate;
    }

    public void setPpbDate(Date ppbDate) {
        this.ppbDate = ppbDate;
    }

    @Basic
    @Column(name = "ppb_terms", nullable = true, precision = 0)
    public Long getPpbTerms() {
        return ppbTerms;
    }

    public void setPpbTerms(Long ppbTerms) {
        this.ppbTerms = ppbTerms;
    }

    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    @Basic
    @Column(name = "ppb_type", nullable = true, precision = 0)
    public Long getPpbType() {
        return ppbType;
    }

    public void setPpbType(Long ppbType) {
        this.ppbType = ppbType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanPayBackEntity that = (PlanPayBackEntity) o;
        return Objects.equals(ppbId, that.ppbId) &&
                Objects.equals(ppbMoney, that.ppbMoney) &&
                Objects.equals(ppbDate, that.ppbDate) &&
                Objects.equals(ppbTerms, that.ppbTerms) &&
                Objects.equals(magicDelete, that.magicDelete) &&
                Objects.equals(ppbType, that.ppbType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ppbId, ppbMoney, ppbDate, ppbTerms, magicDelete, ppbType);
    }

    @OneToMany(mappedBy = "tbPlanPayBackByPpbId")
    public Collection<PayBackDetailEntity> getTbPayBackDetailsByPpbId() {
        return tbPayBackDetailsByPpbId;
    }

    public void setTbPayBackDetailsByPpbId(Collection<PayBackDetailEntity> tbPayBackDetailsByPpbId) {
        this.tbPayBackDetailsByPpbId = tbPayBackDetailsByPpbId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "mt_id", referencedColumnName = "mt_id")
    public MaintainOrderEntity getTbMaintainOrderByMtId() {
        return tbMaintainOrderByMtId;
    }

    public void setTbMaintainOrderByMtId(MaintainOrderEntity tbMaintainOrderByMtId) {
        this.tbMaintainOrderByMtId = tbMaintainOrderByMtId;
    }

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    public OrderEntity getTbOrderByOrderId() {
        return tbOrderByOrderId;
    }

    public void setTbOrderByOrderId(OrderEntity tbOrderByOrderId) {
        this.tbOrderByOrderId = tbOrderByOrderId;
    }

    @Override
    public String toString() {
        return "PlanPayBackEntity{" +
                "ppbId=" + ppbId +
                ", ppbMoney=" + ppbMoney +
                ", ppbDate=" + ppbDate +
                ", ppbTerms=" + ppbTerms +
                ", magicDelete=" + magicDelete +
                ", ppbType=" + ppbType +
                '}';
    }
}
