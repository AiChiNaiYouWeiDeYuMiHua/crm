package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_solution", schema = "USER_CRM", catalog = "")
public class SolutionEntity {
    private Long solutionId;
    private String solutionTheme;
    @JSONField(format = "yyyy-MM-dd")
    private Date submitTime;
    private String solutionContent;
    private String customerFeedback;
    private String annex;
    private Long isDelete;
    private SaleOppEntity tbSaleOppByOppId;

    @Id
    @Column(name = "solution_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="myseq")
    @SequenceGenerator(name="myseq",sequenceName="seq",allocationSize=1)
    public Long getSolutionId() {
        return solutionId;
    }

    public void setSolutionId(Long solutionId) {
        this.solutionId = solutionId;
    }

    @Basic
    @Column(name = "solution_theme", nullable = true, length = 30)
    public String getSolutionTheme() {
        return solutionTheme;
    }

    public void setSolutionTheme(String solutionTheme) {
        this.solutionTheme = solutionTheme;
    }

    @Basic
    @Column(name = "submit_time", nullable = true)
    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    @Basic
    @Column(name = "solution_content", nullable = true)
    public String getSolutionContent() {
        return solutionContent;
    }

    public void setSolutionContent(String solutionContent) {
        this.solutionContent = solutionContent;
    }

    @Basic
    @Column(name = "customer_feedback", nullable = true)
    public String getCustomerFeedback() {
        return customerFeedback;
    }

    public void setCustomerFeedback(String customerFeedback) {
        this.customerFeedback = customerFeedback;
    }

    @Basic
    @Column(name = "annex", nullable = true, length = 50)
    public String getAnnex() {
        return annex;
    }

    public void setAnnex(String annex) {
        this.annex = annex;
    }

    @Basic
    @Column(name = "is_delete", nullable = true, precision = 0)
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SolutionEntity that = (SolutionEntity) o;
        return Objects.equals(solutionId, that.solutionId) &&
                Objects.equals(solutionTheme, that.solutionTheme) &&
                Objects.equals(submitTime, that.submitTime) &&
                Objects.equals(solutionContent, that.solutionContent) &&
                Objects.equals(customerFeedback, that.customerFeedback) &&
                Objects.equals(annex, that.annex) &&
                Objects.equals(isDelete, that.isDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(solutionId, solutionTheme, submitTime, solutionContent, customerFeedback, annex, isDelete);
    }

    @ManyToOne
    @JoinColumn(name = "opp_id", referencedColumnName = "opp_id")
    public SaleOppEntity getTbSaleOppByOppId() {
        return tbSaleOppByOppId;
    }

    public void setTbSaleOppByOppId(SaleOppEntity tbSaleOppByOppId) {
        this.tbSaleOppByOppId = tbSaleOppByOppId;
    }

    @Override
    public String toString() {
        return "SolutionEntity{" +
                "solutionId=" + solutionId +
                ", solutionTheme='" + solutionTheme + '\'' +
                ", submitTime=" + submitTime +
                ", solutionContent='" + solutionContent + '\'' +
                ", customerFeedback='" + customerFeedback + '\'' +
                ", annex='" + annex + '\'' +
                ", isDelete=" + isDelete +
                ", tbSaleOppByOppId=" + tbSaleOppByOppId +
                '}';
    }
}
