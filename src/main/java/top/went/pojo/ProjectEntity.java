package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_project", schema = "USER_CRM")
public class ProjectEntity {
    private Integer projId;
    private String projTheme;
    private String projState;
    private String projStage;
    private String projPresale;
    @JSONField(format = "yyyy-MM-dd")
    private Date projEntryDate;
    private String projOutline;
    private String projCustomers;
    private Integer projIsDelete;
    private String projType;
    private String projExpectPossibility;
    @JSONField(format = "yyyy-MM-dd")
    private Date projExpectDate;
    private Integer projExpectMoney;
    private Collection<ProjectExpectEntity> projectExpectsByProjId;

    private Collection<CustomerEntity> customerEntities;
    private Collection<ContactsEntity> contactsEntities;

    private UserEntity tbUserByUserId;
    private ProjectExpectEntity projectExpectByPexpId;
    private CustomerEntity tbCustomerByCusId;
    private ContactsEntity tbContactsByCotsId;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "proj_id", nullable = false, precision = 0)
    public Integer getProjId() {
        return projId;
    }

    public void setProjId(Integer projId) {
        this.projId = projId;
    }

    @Basic
    @Column(name = "proj_theme", nullable = true, length = 20)
    public String getProjTheme() {
        return projTheme;
    }

    public void setProjTheme(String projTheme) {
        this.projTheme = projTheme;
    }

    @Basic
    @Column(name = "proj_state", nullable = true, length = 20)
    public String getProjState() {
        return projState;
    }

    public void setProjState(String projState) {
        this.projState = projState;
    }

    @Basic
    @Column(name = "proj_stage", nullable = true, length = 10)
    public String getProjStage() {
        return projStage;
    }

    public void setProjStage(String projStage) {
        this.projStage = projStage;
    }

    @Basic
    @Column(name = "proj_presale", nullable = true, length = 20)
    public String getProjPresale() {
        return projPresale;
    }

    public void setProjPresale(String projPresale) {
        this.projPresale = projPresale;
    }

    @Basic
    @Column(name = "proj_entry_date", nullable = true)
    public Date getProjEntryDate() {
        return projEntryDate;
    }

    public void setProjEntryDate(Date projEntryDate) {
        this.projEntryDate = projEntryDate;
    }

    @Basic
    @Column(name = "proj_outline", nullable = true, length = 300)
    public String getProjOutline() {
        return projOutline;
    }

    public void setProjOutline(String projOutline) {
        this.projOutline = projOutline;
    }

    @Basic
    @Column(name = "proj_customers", nullable = true, length = 100)
    public String getProjCustomers() {
        return projCustomers;
    }

    public void setProjCustomers(String projCustomers) {
        this.projCustomers = projCustomers;
    }

    @Basic
    @Column(name = "proj_is_delete", nullable = true)
    public Integer getProjIsDelete() {
        return projIsDelete;
    }

    public void setProjIsDelete(Integer projIsDelete) {
        this.projIsDelete = projIsDelete;
    }

    @Basic
    @Column(name = "proj_type", nullable = true)
    public String getProjType() {
        return projType;
    }

    public void setProjType(String projType) {
        this.projType = projType;
    }

    @Basic
    @Column(name = "proj_expect_possibility", nullable = true)
    public String getProjExpectPossibility() {
        return projExpectPossibility;
    }

    public void setProjExpectPossibility(String projExpectPossibility) {
        this.projExpectPossibility = projExpectPossibility;
    }

    @Basic
    @Column(name = "proj_expect_date", nullable = true)
    public Date getProjExpectDate() {
        return projExpectDate;
    }

    public void setProjExpectDate(Date projExpectDate) {
        this.projExpectDate = projExpectDate;
    }

    @Basic
    @Column(name = "proj_expect_money", nullable = true)
    public Integer getProjExpectMoney() {
        return projExpectMoney;
    }

    public void setProjExpectMoney(Integer projExpectMoney) {
        this.projExpectMoney = projExpectMoney;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectEntity that = (ProjectEntity) o;
        return Objects.equals(projId, that.projId) &&
                Objects.equals(projTheme, that.projTheme) &&
                Objects.equals(projState, that.projState) &&
                Objects.equals(projStage, that.projStage) &&
                Objects.equals(projPresale, that.projPresale) &&
                Objects.equals(projEntryDate, that.projEntryDate) &&
                Objects.equals(projOutline, that.projOutline) &&
                Objects.equals(projCustomers, that.projCustomers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projId, projTheme, projState, projStage, projPresale, projEntryDate, projOutline, projCustomers);
    }

    @OneToMany(mappedBy = "tbProjectByProjId")
    public Collection<ProjectExpectEntity> getProjectExpectsByProjId() {
        return projectExpectsByProjId;
    }

    public void setProjectExpectsByProjId(Collection<ProjectExpectEntity> projectExpectsByProjId) {
        this.projectExpectsByProjId = projectExpectsByProjId;
    }

    @ManyToMany
    @JoinTable(name = "tb_proj_cus", joinColumns = {
            @JoinColumn(name = "proj_id")}, inverseJoinColumns = {
            @JoinColumn(name = "cus_id")})
    public Collection<CustomerEntity> getCustomerEntities() {
        return customerEntities;
    }

    public void setCustomerEntities(Collection<CustomerEntity> customerEntities) {
        this.customerEntities = customerEntities;
    }

    @ManyToMany
    @JoinTable(name = "tb_proj_mid", joinColumns = {
            @JoinColumn(name = "proj_id")}, inverseJoinColumns = {
            @JoinColumn(name = "cots_id")})
    public Collection<ContactsEntity> getContactsEntities() {
        return contactsEntities;
    }

    public void setContactsEntities(Collection<ContactsEntity> contactsEntities) {
        this.contactsEntities = contactsEntities;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "pexp_id", referencedColumnName = "pexp_id")
    public ProjectExpectEntity getProjectExpectByPexpId() {
        return projectExpectByPexpId;
    }

    public void setProjectExpectByPexpId(ProjectExpectEntity projectExpectByPexpId) {
        this.projectExpectByPexpId = projectExpectByPexpId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "cots_id", referencedColumnName = "cots_id")
    public ContactsEntity getTbContactsByCotsId() {
        return tbContactsByCotsId;
    }

    public void setTbContactsByCotsId(ContactsEntity tbContactsByCotsId) {
        this.tbContactsByCotsId = tbContactsByCotsId;
    }

    @Override
    public String toString() {
        return "ProjectEntity{" +
                "projId=" + projId +
                ", projTheme='" + projTheme + '\'' +
                ", projState='" + projState + '\'' +
                ", projStage='" + projStage + '\'' +
                ", projPresale='" + projPresale + '\'' +
                ", projEntryDate=" + projEntryDate +
                ", projOutline='" + projOutline + '\'' +
                ", projCustomers='" + projCustomers + '\'' +
                ", projIsDelete=" + projIsDelete +
                ", projType='" + projType + '\'' +
                ", projectExpectsByProjId=" + projectExpectsByProjId +
                ", customerEntities=" + customerEntities +
                ", contactsEntities=" + contactsEntities +
                ", tbUserByUserId=" + tbUserByUserId +
                ", projectExpectByPexpId=" + projectExpectByPexpId +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbContactsByCotsId=" + tbContactsByCotsId +
                '}';
    }
}
