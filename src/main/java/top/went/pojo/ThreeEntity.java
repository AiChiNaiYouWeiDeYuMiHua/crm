package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "tb_three", schema = "USER_CRM")
public class ThreeEntity {
    private Integer threeId;
    private String threeContent;
    @JSONField(format = "yyyy-MM-dd")
    private Date threeUpdatatime;
    private CustomerEntity tbCustomerByCusId;
    private UserEntity tbUserByUserId;


    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "three_id", nullable = false, precision = 0)
    public Integer getThreeId() {
        return threeId;
    }

    public void setThreeId(Integer threeId) {
        this.threeId = threeId;
    }

    @Basic
    @Column(name = "three_content", nullable = true)
    public String getThreeContent() {
        return threeContent;
    }

    public void setThreeContent(String threeContent) {
        this.threeContent = threeContent;
    }

    @Basic
    @Column(name = "three_updatatime", nullable = true)
    public Date getThreeUpdatatime() {
        return threeUpdatatime;
    }

    public void setThreeUpdatatime(Date threeUpdatatime) {
        this.threeUpdatatime = threeUpdatatime;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @Override
    public String toString() {
        return "ThreeEntity{" +
                "threeId=" + threeId +
                ", threeContent='" + threeContent + '\'' +
                ", threeUpdatatime=" + threeUpdatatime +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbUserByUserId=" + tbUserByUserId +
                '}';
    }
}
