package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_order", schema = "USER_CRM", catalog = "")
public class OrderEntity {
    private Integer orderId;
    private String orderTitle;
    private String orderNumber;
    private String orderCategory;
    private String orderPayWay;
    private BigDecimal orderTotal;
    @JSONField(format = "yyyy-MM-dd")
    private Date orderDate;
    @JSONField(format = "yyyy-MM-dd")
    private Date orderLatestDate;
    @JSONField(format = "yyyy-MM-dd")
    private Date orderNewDate;
    private int orderStatus;
    private Boolean orderType;
    private String orderOther;
    private String orderEnclosure;
    private String orderKnotWay;
    private Boolean loginDelete;
    private int orderOk;
    @JSONField(serialize = false)
    private Collection<DeliverDetailEntity> tbDeliverDetailsByOrderId;
    private UserEntity tbUserByUserId;
    private AddressEntity tbAddressByAddressId;
    private CustomerEntity tbCustomerByCusId;
    private UserEntity tbUserByTbUserId;
    private SaleOppEntity tbQuoteByQuoteId;
    private ContactsEntity contactsEntity;
    private ProjectEntity projectEntity;
    @JSONField(serialize = false)
    private Collection<OrderDetailEntity> tbOrderDetailsByOrderId;
    @JSONField(serialize = false)
    private Collection<PayBackDetailEntity> tbPayBackDetailsByOrderId;
    @JSONField(serialize = false)
    private Collection<PlanPayBackEntity> tbPlanPayBacksByOrderId;
    @JSONField(serialize = false)
    private Collection<ReceiptRecordsEntity> tbReceiptRecordsByOrderId;
    @JSONField(serialize = false)
    private Collection<ReturnGoodsEntity> tbReturnGoodsByOrderId;
    @JSONField(serialize = false)
    private Collection<WhPullEntity> tbWhPullsByOrderId;

    public OrderEntity() {
    }

    public OrderEntity(String orderTitle, String orderNumber, BigDecimal orderTotal, Date orderNewDate, int orderStatus,
                       Boolean orderType, String orderOther, Boolean loginDelete, int orderOk, UserEntity tbUserByUserId,
                       CustomerEntity tbCustomerByCusId, SaleOppEntity tbQuoteByQuoteId,ContactsEntity contactsEntity) {
        this.orderTitle = orderTitle;
        this.orderNumber = orderNumber;
        this.orderTotal = orderTotal;
        this.orderNewDate = orderNewDate;
        this.orderStatus = orderStatus;
        this.orderType = orderType;
        this.orderOther = orderOther;
        this.loginDelete = loginDelete;
        this.orderOk = orderOk;
        this.tbUserByUserId = tbUserByUserId;
        this.tbCustomerByCusId = tbCustomerByCusId;
        this.tbQuoteByQuoteId = tbQuoteByQuoteId;
        this.contactsEntity = contactsEntity;
    }

    @Id
    @Column(name = "order_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }


    @Basic
    @Column(name = "order_number", nullable = true, precision = 0)
    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Basic
    @Column(name = "order_type", nullable = true, precision = 0)
    public Boolean getOrderType() {
        return orderType;
    }

    public void setOrderType(Boolean orderType) {
        this.orderType = orderType;
    }

    @Basic
    @Column(name = "order_category", nullable = true, length = 20)
    public String getOrderCategory() {
        return orderCategory;
    }

    public void setOrderCategory(String orderCategory) {
        this.orderCategory = orderCategory;
    }

    @Basic
    @Column(name = "order_pay_way", nullable = true, length = 20)
    public String getOrderPayWay() {
        return orderPayWay;
    }

    public void setOrderPayWay(String orderPayWay) {
        this.orderPayWay = orderPayWay;
    }

    @Basic
    @Column(name = "order_total", nullable = true, precision = 2)
    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    @Basic
    @Column(name = "order_date", nullable = true)
    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Basic
    @Column(name = "order_latest_date", nullable = true)
    public Date getOrderLatestDate() {
        return orderLatestDate;
    }

    public void setOrderLatestDate(Date orderLatestDate) {
        this.orderLatestDate = orderLatestDate;
    }

    @Basic
    @Column(name = "order_new_date", nullable = true)
    public Date getOrderNewDate() {
        return orderNewDate;
    }

    public void setOrderNewDate(Date orderNewDate) {
        this.orderNewDate = orderNewDate;
    }

    @Basic
    @Column(name = "order_knot_way")
    public String getOrderKnotWay() {
        return orderKnotWay;
    }

    public void setOrderKnotWay(String orderKnotWay) {
        this.orderKnotWay = orderKnotWay;
    }

    @Basic
    @Column(name = "order_status", nullable = true, precision = 0)
    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }


    @Basic
    @Column(name = "order_title", nullable = true, precision = 50)
    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    @Basic
    @Column(name = "order_other", nullable = true, length = 100)
    public String getOrderOther() {
        return orderOther;
    }

    public void setOrderOther(String orderOther) {
        this.orderOther = orderOther;
    }

    @Basic
    @Column(name = "order_enclosure", nullable = true, length = 200)
    public String getOrderEnclosure() {
        return orderEnclosure;
    }

    public void setOrderEnclosure(String orderEnclosure) {
        this.orderEnclosure = orderEnclosure;
    }

    @Basic
    @Column(name = "login_delete", nullable = true, precision = 0)
    public Boolean getLoginDelete() {
        return loginDelete;
    }

    public void setLoginDelete(Boolean loginDelete) {
        this.loginDelete = loginDelete;
    }

    @Basic
    @Column(name = "order_ok", nullable = true, precision = 0)
    public int getOrderOk() {
        return orderOk;
    }

    public void setOrderOk(int orderOk) {
        this.orderOk = orderOk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderEntity that = (OrderEntity) o;
        return Objects.equals(orderId, that.orderId) &&
                Objects.equals(orderNumber, that.orderNumber) &&
                Objects.equals(orderCategory, that.orderCategory) &&
                Objects.equals(orderPayWay, that.orderPayWay) &&
                Objects.equals(orderTotal, that.orderTotal) &&
                Objects.equals(orderDate, that.orderDate) &&
                Objects.equals(orderLatestDate, that.orderLatestDate) &&
                Objects.equals(orderStatus, that.orderStatus) &&
                Objects.equals(orderType, that.orderType) &&
                Objects.equals(orderOther, that.orderOther) &&
                Objects.equals(orderEnclosure, that.orderEnclosure) &&
                Objects.equals(loginDelete, that.loginDelete) &&
                Objects.equals(orderOk, that.orderOk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, orderNumber, orderCategory, orderPayWay, orderTotal, orderDate, orderLatestDate, orderStatus, orderType, orderOther, orderEnclosure, loginDelete, orderOk);
    }

    @OneToMany(mappedBy = "tbOrderByOrderId")
    public Collection<DeliverDetailEntity> getTbDeliverDetailsByOrderId() {
        return tbDeliverDetailsByOrderId;
    }

    public void setTbDeliverDetailsByOrderId(Collection<DeliverDetailEntity> tbDeliverDetailsByOrderId) {
        this.tbDeliverDetailsByOrderId = tbDeliverDetailsByOrderId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }


    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "proj_id")
    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }

    @ManyToOne
    @JoinColumn(name = "CONTACT_ID", referencedColumnName = "COTS_ID")
    public ContactsEntity getContactsEntity() {
        return contactsEntity;
    }

    public void setContactsEntity(ContactsEntity contactsEntity) {
        this.contactsEntity = contactsEntity;
    }


    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "address_id")
    public AddressEntity getTbAddressByAddressId() {
        return tbAddressByAddressId;
    }

    public void setTbAddressByAddressId(AddressEntity tbAddressByAddressId) {
        this.tbAddressByAddressId = tbAddressByAddressId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "tb__user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByTbUserId() {
        return tbUserByTbUserId;
    }

    public void setTbUserByTbUserId(UserEntity tbUserByTbUserId) {
        this.tbUserByTbUserId = tbUserByTbUserId;
    }

    @ManyToOne
    @JoinColumn(name = "quote_id", referencedColumnName = "opp_id")
    public SaleOppEntity getTbQuoteByQuoteId() {
        return tbQuoteByQuoteId;
    }

    public void setTbQuoteByQuoteId(SaleOppEntity tbQuoteByQuoteId) {
        this.tbQuoteByQuoteId = tbQuoteByQuoteId;
    }

    @OneToMany(mappedBy = "tbOrderByOrderId")
    public Collection<OrderDetailEntity> getTbOrderDetailsByOrderId() {
        return tbOrderDetailsByOrderId;
    }

    public void setTbOrderDetailsByOrderId(Collection<OrderDetailEntity> tbOrderDetailsByOrderId) {
        this.tbOrderDetailsByOrderId = tbOrderDetailsByOrderId;
    }

    @OneToMany(mappedBy = "tbOrderByOrderId")
    public Collection<PayBackDetailEntity> getTbPayBackDetailsByOrderId() {
        return tbPayBackDetailsByOrderId;
    }

    public void setTbPayBackDetailsByOrderId(Collection<PayBackDetailEntity> tbPayBackDetailsByOrderId) {
        this.tbPayBackDetailsByOrderId = tbPayBackDetailsByOrderId;
    }

    @OneToMany(mappedBy = "tbOrderByOrderId")
    public Collection<PlanPayBackEntity> getTbPlanPayBacksByOrderId() {
        return tbPlanPayBacksByOrderId;
    }

    public void setTbPlanPayBacksByOrderId(Collection<PlanPayBackEntity> tbPlanPayBacksByOrderId) {
        this.tbPlanPayBacksByOrderId = tbPlanPayBacksByOrderId;
    }

    @OneToMany(mappedBy = "tbOrderByOrderId")
    public Collection<ReceiptRecordsEntity> getTbReceiptRecordsByOrderId() {
        return tbReceiptRecordsByOrderId;
    }

    public void setTbReceiptRecordsByOrderId(Collection<ReceiptRecordsEntity> tbReceiptRecordsByOrderId) {
        this.tbReceiptRecordsByOrderId = tbReceiptRecordsByOrderId;
    }

    @OneToMany(mappedBy = "tbOrderByOrderId")
    public Collection<ReturnGoodsEntity> getTbReturnGoodsByOrderId() {
        return tbReturnGoodsByOrderId;
    }

    public void setTbReturnGoodsByOrderId(Collection<ReturnGoodsEntity> tbReturnGoodsByOrderId) {
        this.tbReturnGoodsByOrderId = tbReturnGoodsByOrderId;
    }

    @OneToMany(mappedBy = "tbOrderByOrderId")
    public Collection<WhPullEntity> getTbWhPullsByOrderId() {
        return tbWhPullsByOrderId;
    }

    public void setTbWhPullsByOrderId(Collection<WhPullEntity> tbWhPullsByOrderId) {
        this.tbWhPullsByOrderId = tbWhPullsByOrderId;
    }

}
