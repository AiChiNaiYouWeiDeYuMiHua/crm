package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_product_format", schema = "USER_CRM", catalog = "")
public class ProductFormatEntity {
    private Integer pfId;
    private String pfName;
    private Long pfUnitCal;
    private BigDecimal pfPrice;
    private BigDecimal pfCost;
    private Boolean pfStatus;
    private String pfCode;
    private Byte pfBatch;
    private Date pfDate;
    private Date pfExpiry;
    private String pfManufacturer;
    private String pfApprovalNumber;
    private Long pfWeight;
    private String pfImf;
    private String pfOther;
    private Boolean logicDelete;
    private Long pfMin;
    private Long pfMax;
    private String pfUnit;
    private String pfWeigtUnit;
    private boolean isBase;
    private ProductEntity tbProductByProductId;
//    @JSONField(serialize = false)
    private Collection<QuoteDetailEntity> tbQuoteDetailsByPfId;
    @JSONField(serialize = false)
    private Collection<ReturnGoodsDetailEntity> tbReturnGoodsDetailsByPfId;
    @JSONField(serialize = false)
    private Collection<WhNumberEntity> tbWhNumbersByPfId;
    @JSONField(serialize = false)
    private Collection<WpDetailEntity> tbWpDetailsByPfId;

    @Id
    @Column(name = "pf_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getPfId() {
        return pfId;
    }

    public void setPfId(Integer pfId) {
        this.pfId = pfId;
    }

    @Basic
    @Column(name = "pf_name", nullable = true, length = 30)
    public String getPfName() {
        return pfName;
    }

    public void setPfName(String pfName) {
        this.pfName = pfName;
    }

    @Basic
    @Column(name = "pf_unit_cal", nullable = true, precision = 0)
    public Long getPfUnitCal() {
        return pfUnitCal;
    }

    public void setPfUnitCal(Long pfUnitCal) {
        this.pfUnitCal = pfUnitCal;
    }

    @Basic
    @Column(name = "pf_price", nullable = true, precision = 2)
    public BigDecimal getPfPrice() {
        return pfPrice;
    }

    public void setPfPrice(BigDecimal pfPrice) {
        this.pfPrice = pfPrice;
    }

    @Basic
    @Column(name = "pf_cost", nullable = true, precision = 2)
    public BigDecimal getPfCost() {
        return pfCost;
    }

    public void setPfCost(BigDecimal pfCost) {
        this.pfCost = pfCost;
    }

    @Basic
    @Column(name = "pf_status", nullable = true, precision = 0)
    public Boolean getPfStatus() {
        return pfStatus;
    }

    public void setPfStatus(Boolean pfStatus) {
        this.pfStatus = pfStatus;
    }

    @Basic
    @Column(name = "pf_code", nullable = true, length = 30)
    public String getPfCode() {
        return pfCode;
    }

    public void setPfCode(String pfCode) {
        this.pfCode = pfCode;
    }

    @Basic
    @Column(name = "pf_batch", nullable = true, precision = 0)
    public Byte getPfBatch() {
        return pfBatch;
    }

    public void setPfBatch(Byte pfBatch) {
        this.pfBatch = pfBatch;
    }

    @Basic
    @Column(name = "pf_date", nullable = true)
    public Date getPfDate() {
        return pfDate;
    }

    public void setPfDate(Date pfDate) {
        this.pfDate = pfDate;
    }

    @Basic
    @Column(name = "pf_expiry", nullable = true)
    public Date getPfExpiry() {
        return pfExpiry;
    }

    public void setPfExpiry(Date pfExpiry) {
        this.pfExpiry = pfExpiry;
    }

    @Basic
    @Column(name = "pf_manufacturer", nullable = true, length = 40)
    public String getPfManufacturer() {
        return pfManufacturer;
    }

    public void setPfManufacturer(String pfManufacturer) {
        this.pfManufacturer = pfManufacturer;
    }

    @Basic
    @Column(name = "pf_approval_number", nullable = true, length = 30)
    public String getPfApprovalNumber() {
        return pfApprovalNumber;
    }

    public void setPfApprovalNumber(String pfApprovalNumber) {
        this.pfApprovalNumber = pfApprovalNumber;
    }

    @Basic
    @Column(name = "pf_weight", nullable = true, precision = 2)
    public Long getPfWeight() {
        return pfWeight;
    }

    public void setPfWeight(Long pfWeight) {
        this.pfWeight = pfWeight;
    }

    @Basic
    @Column(name = "pf_imf", nullable = true, length = 40)
    public String getPfImf() {
        return pfImf;
    }

    public void setPfImf(String pfImf) {
        this.pfImf = pfImf;
    }

    @Basic
    @Column(name = "pf_other", nullable = true)
    public String getPfOther() {
        return pfOther;
    }

    public void setPfOther(String pfOther) {
        this.pfOther = pfOther;
    }

    @Basic
    @Column(name = "logic_delete", nullable = true, precision = 0)
    public Boolean getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Boolean logicDelete) {
        this.logicDelete = logicDelete;
    }

    @Basic
    @Column(name = "pf_min", nullable = true, precision = 0)
    public Long getPfMin() {
        return pfMin;
    }

    public void setPfMin(Long pfMin) {
        this.pfMin = pfMin;
    }

    @Basic
    @Column(name = "pf_max", nullable = true, precision = 0)
    public Long getPfMax() {
        return pfMax;
    }

    public void setPfMax(Long pfMax) {
        this.pfMax = pfMax;
    }

    @Basic
    @Column(name = "pf_unit", nullable = true, length = 10)
    public String getPfUnit() {
        return pfUnit;
    }

    public void setPfUnit(String pfUnit) {
        this.pfUnit = pfUnit;
    }

    @Basic
    @Column(name = "pf_weigt_unit", nullable = true, length = 6)
    public String getPfWeigtUnit() {
        return pfWeigtUnit;
    }

    public void setPfWeigtUnit(String pfWeigtUnit) {
        this.pfWeigtUnit = pfWeigtUnit;
    }

    @Basic
    @Column(name = "pf_base")
    public boolean isBase() {
        return isBase;
    }

    public void setBase(boolean base) {
        isBase = base;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductFormatEntity that = (ProductFormatEntity) o;
        return isBase == that.isBase &&
                Objects.equals(pfId, that.pfId) &&
                Objects.equals(pfName, that.pfName) &&
                Objects.equals(pfUnitCal, that.pfUnitCal) &&
                Objects.equals(pfPrice, that.pfPrice) &&
                Objects.equals(pfCost, that.pfCost) &&
                Objects.equals(pfStatus, that.pfStatus) &&
                Objects.equals(pfCode, that.pfCode) &&
                Objects.equals(pfBatch, that.pfBatch) &&
                Objects.equals(pfDate, that.pfDate) &&
                Objects.equals(pfExpiry, that.pfExpiry) &&
                Objects.equals(pfManufacturer, that.pfManufacturer) &&
                Objects.equals(pfApprovalNumber, that.pfApprovalNumber) &&
                Objects.equals(pfWeight, that.pfWeight) &&
                Objects.equals(pfImf, that.pfImf) &&
                Objects.equals(pfOther, that.pfOther) &&
                Objects.equals(logicDelete, that.logicDelete) &&
                Objects.equals(pfMin, that.pfMin) &&
                Objects.equals(pfMax, that.pfMax) &&
                Objects.equals(pfUnit, that.pfUnit) &&
                Objects.equals(pfWeigtUnit, that.pfWeigtUnit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pfId, pfName, pfUnitCal, pfPrice, pfCost, pfStatus, pfCode, pfBatch, pfDate, pfExpiry, pfManufacturer, pfApprovalNumber, pfWeight, pfImf, pfOther, logicDelete, pfMin, pfMax, pfUnit, pfWeigtUnit, isBase);
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    public ProductEntity getTbProductByProductId() {
        return tbProductByProductId;
    }

    public void setTbProductByProductId(ProductEntity tbProductByProductId) {
        this.tbProductByProductId = tbProductByProductId;
    }

    @OneToMany(mappedBy = "tbProductFormatByPfId")
    public Collection<QuoteDetailEntity> getTbQuoteDetailsByPfId() {
        return tbQuoteDetailsByPfId;
    }

    public void setTbQuoteDetailsByPfId(Collection<QuoteDetailEntity> tbQuoteDetailsByPfId) {
        this.tbQuoteDetailsByPfId = tbQuoteDetailsByPfId;
    }

    @OneToMany(mappedBy = "tbProductFormatByPfId")
    public Collection<ReturnGoodsDetailEntity> getTbReturnGoodsDetailsByPfId() {
        return tbReturnGoodsDetailsByPfId;
    }

    public void setTbReturnGoodsDetailsByPfId(Collection<ReturnGoodsDetailEntity> tbReturnGoodsDetailsByPfId) {
        this.tbReturnGoodsDetailsByPfId = tbReturnGoodsDetailsByPfId;
    }

    @OneToMany(mappedBy = "tbProductFormatByPfId")
    public Collection<WhNumberEntity> getTbWhNumbersByPfId() {
        return tbWhNumbersByPfId;
    }

    public void setTbWhNumbersByPfId(Collection<WhNumberEntity> tbWhNumbersByPfId) {
        this.tbWhNumbersByPfId = tbWhNumbersByPfId;
    }

    @OneToMany(mappedBy = "tbProductFormatByPfId")
    public Collection<WpDetailEntity> getTbWpDetailsByPfId() {
        return tbWpDetailsByPfId;
    }

    public void setTbWpDetailsByPfId(Collection<WpDetailEntity> tbWpDetailsByPfId) {
        this.tbWpDetailsByPfId = tbWpDetailsByPfId;
    }

    @Override
    public String toString() {
        return "ProductFormatEntity{" +
                "pfId=" + pfId +
                ", pfName='" + pfName + '\'' +
                ", pfUnitCal=" + pfUnitCal +
                ", pfPrice=" + pfPrice +
                ", pfCost=" + pfCost +
                ", pfStatus=" + pfStatus +
                ", pfCode='" + pfCode + '\'' +
                ", pfBatch=" + pfBatch +
                ", pfDate=" + pfDate +
                ", pfExpiry=" + pfExpiry +
                ", pfManufacturer='" + pfManufacturer + '\'' +
                ", pfApprovalNumber='" + pfApprovalNumber + '\'' +
                ", pfWeight=" + pfWeight +
                ", pfImf='" + pfImf + '\'' +
                ", pfOther='" + pfOther + '\'' +
                ", logicDelete=" + logicDelete +
                ", pfMin=" + pfMin +
                ", pfMax=" + pfMax +
                ", pfUnit='" + pfUnit + '\'' +
                ", pfWeigtUnit='" + pfWeigtUnit + '\'' +
                ", isBase=" + isBase +
                ", tbProductByProductId=" + tbProductByProductId +
                ", tbQuoteDetailsByPfId=" + tbQuoteDetailsByPfId +
                ", tbReturnGoodsDetailsByPfId=" + tbReturnGoodsDetailsByPfId +
                ", tbWhNumbersByPfId=" + tbWhNumbersByPfId +
                ", tbWpDetailsByPfId=" + tbWpDetailsByPfId +
                '}';
    }
}
