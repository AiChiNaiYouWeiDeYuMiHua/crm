package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_pay_back_detail", schema = "USER_CRM", catalog = "")
public class PayBackDetailEntity {
    private Integer pbdId;
    private Double pbdMoney;
    private Long pbdTerms;
    @JSONField(format = "yyyy-MM-dd")
    private Date pbdDate;
    private Long magicDelete;
    private Boolean pbdType;
    private OrderEntity tbOrderByOrderId;
    private MaintainOrderEntity tbMaintainOrderByMtId;
    private UserEntity tbUserByUserId;
    private PlanPayBackEntity tbPlanPayBackByPpbId;
    private CustomerEntity tbCustomerByCusId;
    private ReturnGoodsEntity returnGoodsEntity;


    @Id
    @Column(name = "pbd_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getPbdId() {
        return pbdId;
    }

    public void setPbdId(Integer pbdId) {
        this.pbdId = pbdId;
    }

    @Basic
    @Column(name = "pbd_money", nullable = true, precision = 2)
    public Double getPbdMoney() {
        return pbdMoney;
    }

    public void setPbdMoney(Double pbdMoney) {
        this.pbdMoney = pbdMoney;
    }

    @Basic
    @Column(name = "pbd_terms", nullable = true, precision = 0)
    public Long getPbdTerms() {
        return pbdTerms;
    }

    public void setPbdTerms(Long pbdTerms) {
        this.pbdTerms = pbdTerms;
    }

    @Basic
    @Column(name = "pbd_date", nullable = true)
    public Date getPbdDate() {
        return pbdDate;
    }

    public void setPbdDate(Date pbdDate) {
        this.pbdDate = pbdDate;
    }

    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    @Basic
    @Column(name = "pbd_type", nullable = true, precision = 0)
    public Boolean getPbdType() {
        return pbdType;
    }

    public void setPbdType(Boolean pbdType) {
        this.pbdType = pbdType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PayBackDetailEntity that = (PayBackDetailEntity) o;
        return Objects.equals(pbdId, that.pbdId) &&
                Objects.equals(pbdMoney, that.pbdMoney) &&
                Objects.equals(pbdTerms, that.pbdTerms) &&
                Objects.equals(pbdDate, that.pbdDate) &&
                Objects.equals(magicDelete, that.magicDelete) &&
                Objects.equals(pbdType, that.pbdType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pbdId, pbdMoney, pbdTerms, pbdDate, magicDelete, pbdType);
    }

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    public OrderEntity getTbOrderByOrderId() {
        return tbOrderByOrderId;
    }

    public void setTbOrderByOrderId(OrderEntity tbOrderByOrderId) {
        this.tbOrderByOrderId = tbOrderByOrderId;
    }

    @ManyToOne
    @JoinColumn(name = "mt_id", referencedColumnName = "mt_id")
    public MaintainOrderEntity getTbMaintainOrderByMtId() {
        return tbMaintainOrderByMtId;
    }

    public void setTbMaintainOrderByMtId(MaintainOrderEntity tbMaintainOrderByMtId) {
        this.tbMaintainOrderByMtId = tbMaintainOrderByMtId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "ppb_id", referencedColumnName = "ppb_id")
    public PlanPayBackEntity getTbPlanPayBackByPpbId() {
        return tbPlanPayBackByPpbId;
    }

    public void setTbPlanPayBackByPpbId(PlanPayBackEntity tbPlanPayBackByPpbId) {
        this.tbPlanPayBackByPpbId = tbPlanPayBackByPpbId;
    }
    @ManyToOne
    @JoinColumn(name = "rg_id", referencedColumnName = "rg_id")
    public ReturnGoodsEntity getReturnGoodsEntity() {
        return returnGoodsEntity;
    }

    public void setReturnGoodsEntity(ReturnGoodsEntity returnGoodsEntity) {
        this.returnGoodsEntity = returnGoodsEntity;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }
}
