package top.went.pojo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "dept_role", schema = "USER_CRM", catalog = "")
public class DeptRoleEntity {
    @Id
    @Column(name = "role_dept_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    private Long roleDeptId;
    @Basic
    @Column(name = "role_dept_is_del", nullable = true, length = 1)
    private Long roleDeptIsDel;
    @ManyToOne
    @JoinColumn(name = "dept_id", referencedColumnName = "dept_id")
    private DeptEntity tbDeptByDeptId;
    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    private RoleEntity tbRoleByRoleId;

    public Long getRoleDeptIsDel() {
        return roleDeptIsDel;
    }

    public void setRoleDeptIsDel(Long roleDeptIsDel) {
        this.roleDeptIsDel = roleDeptIsDel;
    }

    public Long getRoleDeptId() {
        return roleDeptId;
    }

    public void setRoleDeptId(Long roleDeptId) {
        this.roleDeptId = roleDeptId;
    }

    public DeptEntity getTbDeptByDeptId() {
        return tbDeptByDeptId;
    }

    public void setTbDeptByDeptId(DeptEntity tbDeptByDeptId) {
        this.tbDeptByDeptId = tbDeptByDeptId;
    }

    public RoleEntity getTbRoleByRoleId() {
        return tbRoleByRoleId;
    }

    public void setTbRoleByRoleId(RoleEntity tbRoleByRoleId) {
        this.tbRoleByRoleId = tbRoleByRoleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeptRoleEntity that = (DeptRoleEntity) o;
        return Objects.equals(roleDeptId, that.roleDeptId) &&
                Objects.equals(roleDeptIsDel, that.roleDeptIsDel) &&
                Objects.equals(tbDeptByDeptId, that.tbDeptByDeptId) &&
                Objects.equals(tbRoleByRoleId, that.tbRoleByRoleId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(roleDeptId, roleDeptIsDel, tbDeptByDeptId, tbRoleByRoleId);
    }

    @Override
    public String toString() {
        return "DeptRoleEntity{" +
                "roleDeptId=" + roleDeptId +
                ", roleDeptIsDel=" + roleDeptIsDel +
                ", tbDeptByDeptId=" + tbDeptByDeptId +
                ", tbRoleByRoleId=" + tbRoleByRoleId +
                '}';
    }
}
