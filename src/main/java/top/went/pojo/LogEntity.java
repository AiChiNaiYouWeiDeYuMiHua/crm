package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_log", schema = "USER_CRM", catalog = "")
public class LogEntity implements Serializable {
    private Long logId;
    private String logLoginIp;
    private String logOptionContent;
    @JSONField(format = "yyyy-MM-dd")
    private Date logOptionTime;
    private String logOptionType;
    private String logRemark;
    private UserEntity tbUserByUserId;

    @Id
    @Column(name = "log_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    @Basic
    @Column(name = "log_login_ip", nullable = true, length = 30)
    public String getLogLoginIp() {
        return logLoginIp;
    }

    public void setLogLoginIp(String logLoginIp) {
        this.logLoginIp = logLoginIp;
    }

    @Basic
    @Column(name = "log_option_content", nullable = true, length = 50)
    public String getLogOptionContent() {
        return logOptionContent;
    }

    public void setLogOptionContent(String logOptionContent) {
        this.logOptionContent = logOptionContent;
    }

    @Basic
    @Column(name = "log_option_time", nullable = true)
    public Date getLogOptionTime() {
        return logOptionTime;
    }

    public void setLogOptionTime(Date logOptionTime) {
        this.logOptionTime = logOptionTime;
    }

    @Basic
    @Column(name = "log_option_type", nullable = true, length = 20)
    public String getLogOptionType() {
        return logOptionType;
    }

    public void setLogOptionType(String logOptionType) {
        this.logOptionType = logOptionType;
    }

    @Basic
    @Column(name = "log_remark", nullable = true, length = 50)
    public String getLogRemark() {
        return logRemark;
    }

    public void setLogRemark(String logRemark) {
        this.logRemark = logRemark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LogEntity logEntity = (LogEntity) o;
        return Objects.equals(logId, logEntity.logId) &&
                Objects.equals(logLoginIp, logEntity.logLoginIp) &&
                Objects.equals(logOptionContent, logEntity.logOptionContent) &&
                Objects.equals(logOptionTime, logEntity.logOptionTime) &&
                Objects.equals(logOptionType, logEntity.logOptionType) &&
                Objects.equals(logRemark, logEntity.logRemark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(logId, logLoginIp, logOptionContent, logOptionTime, logOptionType, logRemark);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @Override
    public String toString() {
        return "LogEntity{" +
                "logId=" + logId +
                ", logLoginIp='" + logLoginIp + '\'' +
                ", logOptionContent='" + logOptionContent + '\'' +
                ", logOptionTime=" + logOptionTime +
                ", logOptionType='" + logOptionType + '\'' +
                ", logRemark='" + logRemark + '\'' +
                ", tbUserByUserId=" + tbUserByUserId +
                '}';
    }
}
