package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_fc_type", schema = "USER_CRM", catalog = "")
public class FcTypeEntity {
    private Long fcTypeId;
    @JSONField(name = "text")
    private String fcTypeName;
    private Collection<FunctionsEntity> tbFunctionsByFcTypeId;

    @Id
    @Column(name = "fc_type_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    public Long getFcTypeId() {
        return fcTypeId;
    }

    public void setFcTypeId(Long fcTypeId) {
        this.fcTypeId = fcTypeId;
    }

    @Basic
    @Column(name = "fc_type_name", nullable = true, length = 20)
    public String getFcTypeName() {
        return fcTypeName;
    }

    public void setFcTypeName(String fcTypeName) {
        this.fcTypeName = fcTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FcTypeEntity that = (FcTypeEntity) o;
        return Objects.equals(fcTypeId, that.fcTypeId) &&
                Objects.equals(fcTypeName, that.fcTypeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fcTypeId, fcTypeName);
    }

    @OneToMany(mappedBy = "tbFcTypeByFcTypeId")
    public Collection<FunctionsEntity> getTbFunctionsByFcTypeId() {
        return tbFunctionsByFcTypeId;
    }

    public void setTbFunctionsByFcTypeId(Collection<FunctionsEntity> tbFunctionsByFcTypeId) {
        this.tbFunctionsByFcTypeId = tbFunctionsByFcTypeId;
    }
}
