package top.went.pojo;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_daily_recode", schema = "USER_CRM", catalog = "")
public class DailyRecodeEntity {
    private Integer dailyRecodeId;
    private String dailyRecodeContent;
    private String dailyRecodeAccessories;
    private String dailyPlanContent;
    private String dailyPlanAccessories;
    private Time dailyRecodeTime;
    private Long dailyRecodeType;
    private Long dailyIsDel;
    private UserEntity tbUserByUserId;

    @Id
    @Column(name = "daily_recode_id", nullable = false, precision = 0)
    public Integer getDailyRecodeId() {
        return dailyRecodeId;
    }

    public void setDailyRecodeId(Integer dailyRecodeId) {
        this.dailyRecodeId = dailyRecodeId;
    }

    @Basic
    @Column(name = "daily_recode_content", nullable = true)
    public String getDailyRecodeContent() {
        return dailyRecodeContent;
    }

    public void setDailyRecodeContent(String dailyRecodeContent) {
        this.dailyRecodeContent = dailyRecodeContent;
    }

    @Basic
    @Column(name = "daily_recode_accessories", nullable = true, length = 2000)
    public String getDailyRecodeAccessories() {
        return dailyRecodeAccessories;
    }

    public void setDailyRecodeAccessories(String dailyRecodeAccessories) {
        this.dailyRecodeAccessories = dailyRecodeAccessories;
    }

    @Basic
    @Column(name = "daily_plan_content", nullable = true)
    public String getDailyPlanContent() {
        return dailyPlanContent;
    }

    public void setDailyPlanContent(String dailyPlanContent) {
        this.dailyPlanContent = dailyPlanContent;
    }

    @Basic
    @Column(name = "daily_plan_accessories", nullable = true, length = 2000)
    public String getDailyPlanAccessories() {
        return dailyPlanAccessories;
    }

    public void setDailyPlanAccessories(String dailyPlanAccessories) {
        this.dailyPlanAccessories = dailyPlanAccessories;
    }

    @Basic
    @Column(name = "daily_recode_time", nullable = true)
    public Time getDailyRecodeTime() {
        return dailyRecodeTime;
    }

    public void setDailyRecodeTime(Time dailyRecodeTime) {
        this.dailyRecodeTime = dailyRecodeTime;
    }

    @Basic
    @Column(name = "daily_recode_type", nullable = true, precision = 0)
    public Long getDailyRecodeType() {
        return dailyRecodeType;
    }

    public void setDailyRecodeType(Long dailyRecodeType) {
        this.dailyRecodeType = dailyRecodeType;
    }

    @Basic
    @Column(name = "daily_is_del", nullable = true, precision = 0)
    public Long getDailyIsDel() {
        return dailyIsDel;
    }

    public void setDailyIsDel(Long dailyIsDel) {
        this.dailyIsDel = dailyIsDel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DailyRecodeEntity that = (DailyRecodeEntity) o;
        return Objects.equals(dailyRecodeId, that.dailyRecodeId) &&
                Objects.equals(dailyRecodeContent, that.dailyRecodeContent) &&
                Objects.equals(dailyRecodeAccessories, that.dailyRecodeAccessories) &&
                Objects.equals(dailyPlanContent, that.dailyPlanContent) &&
                Objects.equals(dailyPlanAccessories, that.dailyPlanAccessories) &&
                Objects.equals(dailyRecodeTime, that.dailyRecodeTime) &&
                Objects.equals(dailyRecodeType, that.dailyRecodeType) &&
                Objects.equals(dailyIsDel, that.dailyIsDel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dailyRecodeId, dailyRecodeContent, dailyRecodeAccessories, dailyPlanContent, dailyPlanAccessories, dailyRecodeTime, dailyRecodeType, dailyIsDel);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }
}
