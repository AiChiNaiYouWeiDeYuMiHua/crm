package top.went.pojo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_user_role", schema = "USER_CRM", catalog = "")
public class UserRoleEntity {
    @Id
    @Column(name = "user_role_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    private Long userRoleId;
    @Basic
    @Column(name = "user_role_is_del", nullable = true, length = 20)
    private Long userRoleIsDel;
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private UserEntity tbUserByUserId;
    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    private RoleEntity tbRoleByRoleId;

    @Override
    public String toString() {
        return "UserRoleEntity{" +
                "userRoleId=" + userRoleId +
                ", userRoleIsDel=" + userRoleIsDel +
                ", tbUserByUserId=" + tbUserByUserId +
                ", tbRoleByRoleId=" + tbRoleByRoleId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRoleEntity that = (UserRoleEntity) o;
        return Objects.equals(userRoleId, that.userRoleId) &&
                Objects.equals(userRoleIsDel, that.userRoleIsDel) &&
                Objects.equals(tbUserByUserId, that.tbUserByUserId) &&
                Objects.equals(tbRoleByRoleId, that.tbRoleByRoleId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userRoleId, userRoleIsDel, tbUserByUserId, tbRoleByRoleId);
    }

    public Long getUserRoleIsDel() {

        return userRoleIsDel;
    }

    public void setUserRoleIsDel(Long userRoleIsDel) {
        this.userRoleIsDel = userRoleIsDel;
    }

    public Long getUserRoleId() {

        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        userRoleId = userRoleId;
    }

    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    public RoleEntity getTbRoleByRoleId() {
        return tbRoleByRoleId;
    }

    public void setTbRoleByRoleId(RoleEntity tbRoleByRoleId) {
        this.tbRoleByRoleId = tbRoleByRoleId;
    }

}
