package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_dept", schema = "USER_CRM", catalog = "")
public class DeptEntity {
    private Long deptId;
    private String deptName;
    private Long deptIsDel;
    private Collection<DeptRoleEntity> tbDeptRolesByDeptId;
//    private Collection<DeptAnnounceEntity> deptAnnouncesByDeptId;
//    private Collection<RoleEntity> roleEntities;
    private Collection<UserEntity> tbUsersByDeptId;

    @Id
    @Column(name = "dept_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    @Basic
    @Column(name = "dept_name", nullable = true, length = 20)
    @JSONField(name = "text")
    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Basic
    @Column(name = "dept_is_del", nullable = true, precision = 0)
    public Long getDeptIsDel() {
        return deptIsDel;
    }

    public void setDeptIsDel(Long deptIsDel) {
        this.deptIsDel = deptIsDel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeptEntity that = (DeptEntity) o;
        return Objects.equals(deptId, that.deptId) &&
                Objects.equals(deptName, that.deptName) &&
                Objects.equals(deptIsDel, that.deptIsDel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deptId, deptName, deptIsDel);
    }

    /*@OneToMany(mappedBy = "tbDeptByDeptId")
    public Collection<DeptRoleEntity> getTbDeptRolesByDeptId() {
        return tbDeptRolesByDeptId;
    }

    public void setTbDeptRolesByDeptId(Collection<DeptRoleEntity> tbDeptRolesByDeptId) {
        this.tbDeptRolesByDeptId = tbDeptRolesByDeptId;
    }*/

    //    @OneToMany(mappedBy = "tbDeptByDeptId")
//    public Collection<DeptAnnounceEntity> getDeptAnnouncesByDeptId() {
//        return deptAnnouncesByDeptId;
//    }
//
//    public void setDeptAnnouncesByDeptId(Collection<DeptAnnounceEntity> deptAnnouncesByDeptId) {
//        this.deptAnnouncesByDeptId = deptAnnouncesByDeptId;
//    }

//    @ManyToMany
//    @JoinTable(name = "dept_role", joinColumns = {
//            @JoinColumn(name = "dept_id")}, inverseJoinColumns = {
//            @JoinColumn(name = "role_id")})
//    public Collection<RoleEntity> getRoleEntities() {
//        return roleEntities;
//    }
//
//    public void setRoleEntities(Collection<RoleEntity> roleEntities) {
//        this.roleEntities = roleEntities;
//    }

//    @OneToMany(mappedBy = "tbDeptByDeptId")
//    public Collection<UserEntity> getTbUsersByDeptId() {
//        return tbUsersByDeptId;
//    }
//
//    public void setTbUsersByDeptId(Collection<UserEntity> tbUsersByDeptId) {
//        this.tbUsersByDeptId = tbUsersByDeptId;
//    }


    @Override
    public String toString() {
        return "DeptEntity{" +
                "deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", deptIsDel=" + deptIsDel +
                ", tbDeptRolesByDeptId=" + tbDeptRolesByDeptId +
                ", tbUsersByDeptId=" + tbUsersByDeptId +
                '}';
    }
}
