package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_payment_records", schema = "USER_CRM", catalog = "")
public class PaymentRecordsEntity {
    private Double prMoney;
    private Long prTerms;
    @JSONField(format = "yyyy-MM-dd")
    private Date prDate;
    private String prIsTicket;
    private Long magicDelete;
    private Integer prId;
    private Integer prType;
    private UserEntity tbUserByUserId;
    private CustomerEntity tbCustomerByCusId;
    private PlanPayDetailEntity tbPlanPayDetailByPpdId;
    private PurchaseEntity tbPurchaseByPurId;
    private ReturnGoodsEntity tbReturnGoodsByRgId;

    @Basic
    @Column(name = "pr_money", nullable = true, precision = 2)
    public Double getPrMoney() {
        return prMoney;
    }

    public void setPrMoney(Double prMoney) {
        this.prMoney = prMoney;
    }

    @Basic
    @Column(name = "pr_terms", nullable = true, precision = 0)
    public Long getPrTerms() {
        return prTerms;
    }

    public void setPrTerms(Long prTerms) {
        this.prTerms = prTerms;
    }

    @Override
    public String toString() {
        return "PaymentRecordsEntity{" +
                "prMoney=" + prMoney +
                ", prTerms=" + prTerms +
                ", prDate=" + prDate +
                ", prIsTicket='" + prIsTicket + '\'' +
                ", magicDelete=" + magicDelete +
                ", prId=" + prId +
                ", tbUserByUserId=" + tbUserByUserId +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbPlanPayDetailByPpdId=" + tbPlanPayDetailByPpdId +
                '}';
    }

    @Basic
    @Column(name = "pr_date", nullable = true)
    public Date getPrDate() {
        return prDate;
    }

    public void setPrDate(Date prDate) {
        this.prDate = prDate;
    }

    @Basic
    @Column(name = "pr_is_ticket", nullable = true, length = 20)
    public String getPrIsTicket() {
        return prIsTicket;
    }

    public void setPrIsTicket(String prIsTicket) {
        this.prIsTicket = prIsTicket;
    }


    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }
    @Basic
    @Column(name = "pr_type", nullable = true, length = 20)
    public Integer getPrType() {
        return prType;
    }

    public void setPrType(Integer prType) {
        this.prType = prType;
    }




    @Id
    @Column(name = "pr_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getPrId() {
        return prId;
    }

    public void setPrId(Integer prId) {
        this.prId = prId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentRecordsEntity that = (PaymentRecordsEntity) o;
        return Objects.equals(prMoney, that.prMoney) &&
                Objects.equals(prTerms, that.prTerms) &&
                Objects.equals(prDate, that.prDate) &&
                Objects.equals(prIsTicket, that.prIsTicket) &&
                Objects.equals(magicDelete, that.magicDelete) &&
                Objects.equals(prId, that.prId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prMoney, prTerms, prDate, prIsTicket, magicDelete, prId);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "ppd_id", referencedColumnName = "ppd_id")
    public PlanPayDetailEntity getTbPlanPayDetailByPpdId() {
        return tbPlanPayDetailByPpdId;
    }

    public void setTbPlanPayDetailByPpdId(PlanPayDetailEntity tbPlanPayDetailByPpdId) {
        this.tbPlanPayDetailByPpdId = tbPlanPayDetailByPpdId;
    }
    @ManyToOne
    @JoinColumn(name = "rg_id", referencedColumnName = "rg_id")
    public ReturnGoodsEntity getTbReturnGoodsByRgId() {
        return tbReturnGoodsByRgId;
    }

    public void setTbReturnGoodsByRgId(ReturnGoodsEntity tbReturnGoodsByRgId) {
        this.tbReturnGoodsByRgId = tbReturnGoodsByRgId;
    }

    @ManyToOne
    @JoinColumn(name = "pur_id", referencedColumnName = "pur_id")
    public PurchaseEntity getTbPurchaseByPurId() {
        return tbPurchaseByPurId;
    }

    public void setTbPurchaseByPurId(PurchaseEntity tbPurchaseByPurId) {
        this.tbPurchaseByPurId = tbPurchaseByPurId;
    }
}
