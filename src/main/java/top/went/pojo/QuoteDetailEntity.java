package top.went.pojo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_quote_detail", schema = "USER_CRM", catalog = "")
public class QuoteDetailEntity {
    private Long qdId;
    private Double unitPrice;
    private Double amount;
    private Double money;
    private String remarks;
    private Long isDelete;
    private ProductFormatEntity tbProductFormatByPfId;
    private QuoteEntity tbQuoteByQuoteId;

    private Double qdCost;

    @Basic
    @Column(name = "qd_cost", nullable = true, precision = 2)
    public Double getQdCost() {
        return qdCost;
    }

    public void setQdCost(Double qdCost) {
        this.qdCost = qdCost;
    }

    @Id
    @Column(name = "qd_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="myseq")
    @SequenceGenerator(name="myseq",sequenceName="seq",allocationSize=1)
    public Long getQdId() {
        return qdId;
    }

    public void setQdId(Long qdId) {
        this.qdId = qdId;
    }

    @Basic
    @Column(name = "unit_price", nullable = true, precision = 2)
    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "amount", nullable = true, precision = 2)
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "money", nullable = true, precision = 2)
    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    @Basic
    @Column(name = "remarks", nullable = true, length = 100)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "is_delete", nullable = true, precision = 0)
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuoteDetailEntity that = (QuoteDetailEntity) o;
        return Objects.equals(qdId, that.qdId) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(money, that.money) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(isDelete, that.isDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(qdId, unitPrice, amount, money, remarks, isDelete);
    }

    @ManyToOne
    @JoinColumn(name = "pf_id", referencedColumnName = "pf_id")
    public ProductFormatEntity getTbProductFormatByPfId() {
        return tbProductFormatByPfId;
    }

    public void setTbProductFormatByPfId(ProductFormatEntity tbProductFormatByPfId) {
        this.tbProductFormatByPfId = tbProductFormatByPfId;
    }

    @ManyToOne
    @JoinColumn(name = "quote_id", referencedColumnName = "quote_id")
    public QuoteEntity getTbQuoteByQuoteId() {
        return tbQuoteByQuoteId;
    }

    public void setTbQuoteByQuoteId(QuoteEntity tbQuoteByQuoteId) {
        this.tbQuoteByQuoteId = tbQuoteByQuoteId;
    }

    @Override
    public String toString() {
        return "QuoteDetailEntity{" +
                "qdId=" + qdId +
                ", unitPrice=" + unitPrice +
                ", amount=" + amount +
                ", money=" + money +
                ", remarks='" + remarks + '\'' +
                ", isDelete=" + isDelete +
                ", tbProductFormatByPfId=" + tbProductFormatByPfId +
                ", tbQuoteByQuoteId=" + tbQuoteByQuoteId +
                '}';
    }
}
