package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import top.went.utils.StringUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_deliver_detail", schema = "USER_CRM", catalog = "")
public class DeliverDetailEntity {
    private Integer ddId;
    private Long ddNumber;
    private BigDecimal ddTotal;
    @JSONField(format = "yyyy-MM-dd")
    private Date ddDate;
    private BigDecimal ddCost;
    private BigDecimal ddPrice;
    private String ddOther;
    private Integer status;
    private ProductFormatEntity formatEntity;
    private CustomerEntity tbCustomerByCusId;
    private OrderEntity tbOrderByOrderId;
    private DeliverGoodsEntity tbDeliverGoodsByDgId;


    public DeliverDetailEntity() {
    }

    public DeliverDetailEntity(Long ddNumber, BigDecimal ddTotal, Date ddDate, BigDecimal ddCost,
                               BigDecimal ddPrice, String ddOther, Integer status, ProductFormatEntity formatEntity,
                               CustomerEntity tbCustomerByCusId, OrderEntity tbOrderByOrderId,
                                DeliverGoodsEntity deliverGoodsEntity) {
        this.ddNumber = ddNumber;
        this.ddTotal = ddTotal;
        this.ddDate = ddDate;
        this.ddCost = ddCost;
        this.ddPrice = ddPrice;
        this.ddOther = ddOther;
        this.status = status;
        this.formatEntity = formatEntity;
        this.tbCustomerByCusId = tbCustomerByCusId;
        this.tbOrderByOrderId = tbOrderByOrderId;
        this.tbDeliverGoodsByDgId = deliverGoodsEntity;
    }

    @Id
    @Column(name = "dd_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getDdId() {
        return ddId;
    }

    public void setDdId(Integer ddId) {
        this.ddId = ddId;
    }

    @Basic
    @Column(name = "dd_number", nullable = true, precision = 0)
    public Long getDdNumber() {
        return ddNumber;
    }

    public void setDdNumber(Long ddNumber) {
        this.ddNumber = ddNumber;
    }

    @Basic
    @Column(name = "dd_status", nullable = true, precision = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "dd_total", nullable = true, precision = 2)
    public BigDecimal getDdTotal() {
        return ddTotal;
    }

    public void setDdTotal(BigDecimal ddTotal) {
        this.ddTotal = ddTotal;
    }

    @Basic
    @Column(name = "dd_date", nullable = true)
    public Date getDdDate() {
        return ddDate;
    }

    public void setDdDate(Date ddDate) {
        this.ddDate = ddDate;
    }

    @Basic
    @Column(name = "dd_cost", nullable = true, precision = 2)
    public BigDecimal getDdCost() {
        return ddCost;
    }

    public void setDdCost(BigDecimal ddCost) {
        this.ddCost = ddCost;
    }

    @Basic
    @Column(name = "dd_price", nullable = true, precision = 2)
    public BigDecimal getDdPrice() {
        return ddPrice;
    }

    public void setDdPrice(BigDecimal ddPrice) {
        this.ddPrice = ddPrice;
    }

    @Basic
    @Column(name = "dd_other", nullable = true, length = 100)
    public String getDdOther() {
        return ddOther;
    }

    public void setDdOther(String ddOther) {
        this.ddOther = ddOther;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliverDetailEntity that = (DeliverDetailEntity) o;
        return Objects.equals(ddId, that.ddId) &&
                Objects.equals(ddNumber, that.ddNumber) &&
                Objects.equals(ddTotal, that.ddTotal) &&
                Objects.equals(ddDate, that.ddDate) &&
                Objects.equals(ddCost, that.ddCost) &&
                Objects.equals(ddOther, that.ddOther);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ddId, ddNumber, ddTotal, ddDate, ddCost, ddOther);
    }

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "pf_id")
    public ProductFormatEntity getFormatEntity() {
        return formatEntity;
    }

    public void setFormatEntity(ProductFormatEntity formatEntity) {
        this.formatEntity = formatEntity;
    }




    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    public OrderEntity getTbOrderByOrderId() {
        return tbOrderByOrderId;
    }

    public void setTbOrderByOrderId(OrderEntity tbOrderByOrderId) {
        this.tbOrderByOrderId = tbOrderByOrderId;
    }

    @ManyToOne
    @JoinColumn(name = "dg_id", referencedColumnName = "dg_id")
    public DeliverGoodsEntity getTbDeliverGoodsByDgId() {
        return tbDeliverGoodsByDgId;
    }

    public void setTbDeliverGoodsByDgId(DeliverGoodsEntity tbDeliverGoodsByDgId) {
        this.tbDeliverGoodsByDgId = tbDeliverGoodsByDgId;
    }


}
