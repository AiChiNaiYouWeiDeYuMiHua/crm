package top.went.pojo;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_address", schema = "USER_CRM", catalog = "")
public class AddressEntity {
    private Integer addressId;
    private String addressName;
    private String addressTel;
    private String addressProvince;
    private String addressCity;
    private String addressContent;
    private String addressCode;
    private String addressCounty;
    private Collection<DeliverGoodsEntity> tbDeliverGoodsByAddressId;
    private Collection<OrderEntity> tbOrdersByAddressId;

    @Id
    @Column(name = "address_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "address_name", nullable = true, length = 10)
    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    @Basic
    @Column(name = "address_tel", nullable = true, length = 11)
    public String getAddressTel() {
        return addressTel;
    }

    public void setAddressTel(String addressTel) {
        this.addressTel = addressTel;
    }

    @Basic
    @Column(name = "address_province", nullable = true, length = 20)
    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    @Basic
    @Column(name = "address_city", nullable = true, length = 20)
    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    @Basic
    @Column(name = "address_content", nullable = true, length = 100)
    public String getAddressContent() {
        return addressContent;
    }

    public void setAddressContent(String addressContent) {
        this.addressContent = addressContent;
    }

    @Basic
    @Column(name = "address_county", nullable = true, length = 20)
    public String getAddressCounty() {
        return addressCounty;
    }

    public void setAddressCounty(String addressCounty) {
        this.addressCounty = addressCounty;
    }

    @Basic
    @Column(name = "address_code", nullable = true, length = 10)
    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressEntity that = (AddressEntity) o;
        return Objects.equals(addressId, that.addressId) &&
                Objects.equals(addressName, that.addressName) &&
                Objects.equals(addressTel, that.addressTel) &&
                Objects.equals(addressProvince, that.addressProvince) &&
                Objects.equals(addressCity, that.addressCity) &&
                Objects.equals(addressContent, that.addressContent) &&
                Objects.equals(addressCode, that.addressCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addressId, addressName, addressTel, addressProvince, addressCity, addressContent, addressCode);
    }

    @OneToMany(mappedBy = "tbAddressByAddressId")
    public Collection<DeliverGoodsEntity> getTbDeliverGoodsByAddressId() {
        return tbDeliverGoodsByAddressId;
    }

    public void setTbDeliverGoodsByAddressId(Collection<DeliverGoodsEntity> tbDeliverGoodsByAddressId) {
        this.tbDeliverGoodsByAddressId = tbDeliverGoodsByAddressId;
    }

    @OneToMany(mappedBy = "tbAddressByAddressId")
    public Collection<OrderEntity> getTbOrdersByAddressId() {
        return tbOrdersByAddressId;
    }

    public void setTbOrdersByAddressId(Collection<OrderEntity> tbOrdersByAddressId) {
        this.tbOrdersByAddressId = tbOrdersByAddressId;
    }
}
