package top.went.pojo;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_travel", schema = "USER_CRM", catalog = "")
public class TravelEntity {
    private Long travelId;
    private String travelTheme;
    private Time startTime;
    private Time endTime;
    private String destination;
    private String peerPersonnel;
    private String transportation;
    private Long status;
    private String remarks;
    private Long isDelete;
    private CustomerEntity tbCustomerByCusId;
    private UserEntity tbUserByUserId;
    private SaleOppEntity tbSaleOppByOppId;

    @Id
    @Column(name = "travel_id", nullable = false, precision = 0)
    public Long getTravelId() {
        return travelId;
    }

    public void setTravelId(Long travelId) {
        this.travelId = travelId;
    }

    @Basic
    @Column(name = "travel_theme", nullable = true, length = 30)
    public String getTravelTheme() {
        return travelTheme;
    }

    public void setTravelTheme(String travelTheme) {
        this.travelTheme = travelTheme;
    }

    @Basic
    @Column(name = "start_time", nullable = true)
    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "end_time", nullable = true)
    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "destination", nullable = true, length = 30)
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Basic
    @Column(name = "peer_personnel", nullable = true, length = 30)
    public String getPeerPersonnel() {
        return peerPersonnel;
    }

    public void setPeerPersonnel(String peerPersonnel) {
        this.peerPersonnel = peerPersonnel;
    }

    @Basic
    @Column(name = "transportation", nullable = true, length = 30)
    public String getTransportation() {
        return transportation;
    }

    public void setTransportation(String transportation) {
        this.transportation = transportation;
    }

    @Basic
    @Column(name = "status", nullable = true, precision = 0)
    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    @Basic
    @Column(name = "remarks", nullable = true, length = 100)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "is_delete", nullable = true, precision = 0)
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TravelEntity that = (TravelEntity) o;
        return Objects.equals(travelId, that.travelId) &&
                Objects.equals(travelTheme, that.travelTheme) &&
                Objects.equals(startTime, that.startTime) &&
                Objects.equals(endTime, that.endTime) &&
                Objects.equals(destination, that.destination) &&
                Objects.equals(peerPersonnel, that.peerPersonnel) &&
                Objects.equals(transportation, that.transportation) &&
                Objects.equals(status, that.status) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(isDelete, that.isDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(travelId, travelTheme, startTime, endTime, destination, peerPersonnel, transportation, status, remarks, isDelete);
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "opp_id", referencedColumnName = "opp_id")
    public SaleOppEntity getTbSaleOppByOppId() {
        return tbSaleOppByOppId;
    }

    public void setTbSaleOppByOppId(SaleOppEntity tbSaleOppByOppId) {
        this.tbSaleOppByOppId = tbSaleOppByOppId;
    }
}
