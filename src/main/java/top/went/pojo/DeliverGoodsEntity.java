package top.went.pojo;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_deliver_goods", schema = "USER_CRM", catalog = "")
public class DeliverGoodsEntity {
    private Integer dgId;
    private Date dgDate;
    private Long dgPkgNumber;
    private String dgModel;
    private Long dgWeight;
    private String dgLogistice;
    private String dgLogisticId;
    private Long dgFreight;
    private String dgFreightModal;
    private String dgOther;
    private String dgNumber;
    private Boolean logicDelete;
    private Integer status;
    private Collection<DeliverDetailEntity> tbDeliverDetailsByDgId;
    private AddressEntity tbAddressByAddressId;
    private UserEntity tbUserByUserId;
    private WhPullEntity tbWhPullByWpullId;

    public DeliverGoodsEntity() {
    }

    public DeliverGoodsEntity(Date dgDate, String dgNumber, Boolean logicDelete, Integer status, AddressEntity tbAddressByAddressId, UserEntity tbUserByUserId, WhPullEntity tbWhPullByWpullId) {
        this.dgDate = dgDate;
        this.dgNumber = dgNumber;
        this.logicDelete = logicDelete;
        this.status = status;
        this.tbAddressByAddressId = tbAddressByAddressId;
        this.tbUserByUserId = tbUserByUserId;
        this.tbWhPullByWpullId = tbWhPullByWpullId;
    }

    @Id
    @Column(name = "dg_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getDgId() {
        return dgId;
    }

    public void setDgId(Integer dgId) {
        this.dgId = dgId;
    }

    @Basic
    @Column(name = "dg_status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "dg_date", nullable = true)
    public Date getDgDate() {
        return dgDate;
    }

    public void setDgDate(Date dgDate) {
        this.dgDate = dgDate;
    }

    @Basic
    @Column(name = "dg_pkg_number", nullable = true, precision = 0)
    public Long getDgPkgNumber() {
        return dgPkgNumber;
    }

    public void setDgPkgNumber(Long dgPkgNumber) {
        this.dgPkgNumber = dgPkgNumber;
    }

    @Basic
    @Column(name = "dg_model", nullable = true, length = 10)
    public String getDgModel() {
        return dgModel;
    }

    public void setDgModel(String dgModel) {
        this.dgModel = dgModel;
    }

    @Basic
    @Column(name = "dg_weight", nullable = true, precision = 2)
    public Long getDgWeight() {
        return dgWeight;
    }

    public void setDgWeight(Long dgWeight) {
        this.dgWeight = dgWeight;
    }

    @Basic
    @Column(name = "dg_logistice", nullable = true, length = 50)
    public String getDgLogistice() {
        return dgLogistice;
    }

    public void setDgLogistice(String dgLogistice) {
        this.dgLogistice = dgLogistice;
    }

    @Basic
    @Column(name = "dg_logistic_id", nullable = true, length = 30)
    public String getDgLogisticId() {
        return dgLogisticId;
    }

    public void setDgLogisticId(String dgLogisticId) {
        this.dgLogisticId = dgLogisticId;
    }

    @Basic
    @Column(name = "dg_freight", nullable = true, precision = 2)
    public Long getDgFreight() {
        return dgFreight;
    }

    public void setDgFreight(Long dgFreight) {
        this.dgFreight = dgFreight;
    }

    @Basic
    @Column(name = "dg_freight_modal", nullable = true, length = 10)
    public String getDgFreightModal() {
        return dgFreightModal;
    }

    public void setDgFreightModal(String dgFreightModal) {
        this.dgFreightModal = dgFreightModal;
    }

    @Basic
    @Column(name = "dg_other", nullable = true, length = 100)
    public String getDgOther() {
        return dgOther;
    }

    public void setDgOther(String dgOther) {
        this.dgOther = dgOther;
    }

    @Basic
    @Column(name = "dg_number", nullable = true, length = 100)
    public String getDgNumber() {
        return dgNumber;
    }

    public void setDgNumber(String dgNumber) {
        this.dgNumber = dgNumber;
    }

    @Basic
    @Column(name = "logic_delete", nullable = true, precision = 0)
    public Boolean getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Boolean logicDelete) {
        this.logicDelete = logicDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliverGoodsEntity that = (DeliverGoodsEntity) o;
        return Objects.equals(dgId, that.dgId) &&
                Objects.equals(dgDate, that.dgDate) &&
                Objects.equals(dgPkgNumber, that.dgPkgNumber) &&
                Objects.equals(dgModel, that.dgModel) &&
                Objects.equals(dgWeight, that.dgWeight) &&
                Objects.equals(dgLogistice, that.dgLogistice) &&
                Objects.equals(dgLogisticId, that.dgLogisticId) &&
                Objects.equals(dgFreight, that.dgFreight) &&
                Objects.equals(dgFreightModal, that.dgFreightModal) &&
                Objects.equals(dgOther, that.dgOther) &&
                Objects.equals(logicDelete, that.logicDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dgId, dgDate, dgPkgNumber, dgModel, dgWeight, dgLogistice, dgLogisticId, dgFreight, dgFreightModal, dgOther, logicDelete);
    }

    @OneToMany(mappedBy = "tbDeliverGoodsByDgId")
    public Collection<DeliverDetailEntity> getTbDeliverDetailsByDgId() {
        return tbDeliverDetailsByDgId;
    }

    public void setTbDeliverDetailsByDgId(Collection<DeliverDetailEntity> tbDeliverDetailsByDgId) {
        this.tbDeliverDetailsByDgId = tbDeliverDetailsByDgId;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "address_id")
    public AddressEntity getTbAddressByAddressId() {
        return tbAddressByAddressId;
    }

    public void setTbAddressByAddressId(AddressEntity tbAddressByAddressId) {
        this.tbAddressByAddressId = tbAddressByAddressId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "wpull_id", referencedColumnName = "wpull_id")
    public WhPullEntity getTbWhPullByWpullId() {
        return tbWhPullByWpullId;
    }

    public void setTbWhPullByWpullId(WhPullEntity tbWhPullByWpullId) {
        this.tbWhPullByWpullId = tbWhPullByWpullId;
    }
}
