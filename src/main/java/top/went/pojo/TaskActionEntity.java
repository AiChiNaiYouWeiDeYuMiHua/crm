package top.went.pojo;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_task_action", schema = "USER_CRM", catalog = "")
public class TaskActionEntity {
    private Long taskId;
    private Time deadline;
    private Boolean priority;
    private Time createTime;
    private Long isDelete;
    private String actionDescription;
    private Long taskStatus;
    private String recordType;
    private Long isTask;
    private UserEntity tbUserByUserId;
    private ContactsEntity tbContactsBy联系人Id;
    private CustomerEntity tbCustomerByCusId;
    private SaleOppEntity tbSaleOppByOppId;

    @Id
    @Column(name = "task_id", nullable = false, precision = 0)
    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "deadline", nullable = true)
    public Time getDeadline() {
        return deadline;
    }

    public void setDeadline(Time deadline) {
        this.deadline = deadline;
    }

    @Basic
    @Column(name = "priority", nullable = true, precision = 0)
    public Boolean getPriority() {
        return priority;
    }

    public void setPriority(Boolean priority) {
        this.priority = priority;
    }

    @Basic
    @Column(name = "create_time", nullable = true)
    public Time getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Time createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "is_delete", nullable = true, precision = 0)
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Basic
    @Column(name = "action_description", nullable = true)
    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    @Basic
    @Column(name = "task_status", nullable = true, precision = 0)
    public Long getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Long taskStatus) {
        this.taskStatus = taskStatus;
    }

    @Basic
    @Column(name = "record_type", nullable = true, length = 20)
    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    @Basic
    @Column(name = "is_task", nullable = true, precision = 0)
    public Long getIsTask() {
        return isTask;
    }

    public void setIsTask(Long isTask) {
        this.isTask = isTask;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskActionEntity that = (TaskActionEntity) o;
        return Objects.equals(taskId, that.taskId) &&
                Objects.equals(deadline, that.deadline) &&
                Objects.equals(priority, that.priority) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(isDelete, that.isDelete) &&
                Objects.equals(actionDescription, that.actionDescription) &&
                Objects.equals(taskStatus, that.taskStatus) &&
                Objects.equals(recordType, that.recordType) &&
                Objects.equals(isTask, that.isTask);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskId, deadline, priority, createTime, isDelete, actionDescription, taskStatus, recordType, isTask);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "联系人id", referencedColumnName = "cots_id")
    public ContactsEntity getTbContactsBy联系人Id() {
        return tbContactsBy联系人Id;
    }

    public void setTbContactsBy联系人Id(ContactsEntity tbContactsBy联系人Id) {
        this.tbContactsBy联系人Id = tbContactsBy联系人Id;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "opp_id", referencedColumnName = "opp_id")
    public SaleOppEntity getTbSaleOppByOppId() {
        return tbSaleOppByOppId;
    }

    public void setTbSaleOppByOppId(SaleOppEntity tbSaleOppByOppId) {
        this.tbSaleOppByOppId = tbSaleOppByOppId;
    }
}
