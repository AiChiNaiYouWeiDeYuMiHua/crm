package top.went.pojo;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_warehouse", schema = "USER_CRM", catalog = "")
public class WarehouseEntity {
    private Long whId;
    private String whName;
    private Boolean logicDelete;


    public WarehouseEntity() {
    }

    public WarehouseEntity(String whName) {
        this.whName = whName;
        this.logicDelete = false;
    }

    @Id
    @Column(name = "wh_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Long     getWhId() {
        return whId;
    }

    public void setWhId(Long whId) {
        this.whId = whId;
    }

    @Basic
    @Column(name = "wh_name", nullable = true, length = 20)
    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    @Basic
    @Column(name = "logic_delete", nullable = true, precision = 0)
    public Boolean getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Boolean logicDelete) {
        this.logicDelete = logicDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WarehouseEntity that = (WarehouseEntity) o;
        return Objects.equals(whId, that.whId) &&
                Objects.equals(whName, that.whName) &&
                Objects.equals(logicDelete, that.logicDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(whId, whName, logicDelete);
    }

//    @OneToMany(mappedBy = "tbWarehouseByWhId")
//    public Collection<AllocationEntity> getTbAllocationsByWhId() {
//        return tbAllocationsByWhId;
//    }
//
//    public void setTbAllocationsByWhId(Collection<AllocationEntity> tbAllocationsByWhId) {
//        this.tbAllocationsByWhId = tbAllocationsByWhId;
//    }
//
//    @OneToMany(mappedBy = "tbWarehouseByTbWhId")
//    public Collection<AllocationEntity> getTbAllocationsByWhId_0() {
//        return tbAllocationsByWhId_0;
//    }
//
//    public void setTbAllocationsByWhId_0(Collection<AllocationEntity> tbAllocationsByWhId_0) {
//        this.tbAllocationsByWhId_0 = tbAllocationsByWhId_0;
//    }
//
//    @OneToMany(mappedBy = "tbWarehouseByWhId")
//    public Collection<PurchaseEntity> getTbPurchasesByWhId() {
//        return tbPurchasesByWhId;
//    }
//
//    public void setTbPurchasesByWhId(Collection<PurchaseEntity> tbPurchasesByWhId) {
//        this.tbPurchasesByWhId = tbPurchasesByWhId;
//    }
//
//    @OneToMany(mappedBy = "tbWarehouseByWhId")
//    public Collection<WhCheckEntity> getTbWhChecksByWhId() {
//        return tbWhChecksByWhId;
//    }
//
//    public void setTbWhChecksByWhId(Collection<WhCheckEntity> tbWhChecksByWhId) {
//        this.tbWhChecksByWhId = tbWhChecksByWhId;
//    }
//
//    @OneToMany(mappedBy = "tbWarehouseByWhId")
//    public Collection<WhNumberEntity> getTbWhNumbersByWhId() {
//        return tbWhNumbersByWhId;
//    }
//
//    public void setTbWhNumbersByWhId(Collection<WhNumberEntity> tbWhNumbersByWhId) {
//        this.tbWhNumbersByWhId = tbWhNumbersByWhId;
//    }
//
//    @OneToMany(mappedBy = "tbWarehouseByWhId")
//    public Collection<WhPullEntity> getTbWhPullsByWhId() {
//        return tbWhPullsByWhId;
//    }
//
//    public void setTbWhPullsByWhId(Collection<WhPullEntity> tbWhPullsByWhId) {
//        this.tbWhPullsByWhId = tbWhPullsByWhId;
//    }
//
//    @OneToMany(mappedBy = "tbWarehouseByWhId")
//    public Collection<WhPutEntity> getTbWhPutsByWhId() {
//        return tbWhPutsByWhId;
//    }
//
//    public void setTbWhPutsByWhId(Collection<WhPutEntity> tbWhPutsByWhId) {
//        this.tbWhPutsByWhId = tbWhPutsByWhId;
//    }
//
//    @OneToMany(mappedBy = "tbWarehouseByWhId")
//    public Collection<WpDetailEntity> getTbWpDetailsByWhId() {
//        return tbWpDetailsByWhId;
//    }
//
//    public void setTbWpDetailsByWhId(Collection<WpDetailEntity> tbWpDetailsByWhId) {
//        this.tbWpDetailsByWhId = tbWpDetailsByWhId;
//    }
}
