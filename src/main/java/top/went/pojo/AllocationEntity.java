package top.went.pojo;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_allocation", schema = "USER_CRM", catalog = "")
public class AllocationEntity {
    private Integer allocationId;
    private String allocationName;
    private Time allocationDate;
    private String addressPhone;
    private Boolean allocationStatus;
    private String allocationOther;
    private Boolean loginDelete;
    private UserEntity tbUserByUserId;
    private UserEntity tbUserByTbUserId;
    private WarehouseEntity tbWarehouseByWhId;
    private WarehouseEntity tbWarehouseByTbWhId;

    @Id
    @Column(name = "allocation_id", nullable = false, precision = 0)
    public Integer getAllocationId() {
        return allocationId;
    }

    public void setAllocationId(Integer allocationId) {
        this.allocationId = allocationId;
    }

    @Basic
    @Column(name = "allocation_name", nullable = true, length = 30)
    public String getAllocationName() {
        return allocationName;
    }

    public void setAllocationName(String allocationName) {
        this.allocationName = allocationName;
    }

    @Basic
    @Column(name = "allocation_date", nullable = true)
    public Time getAllocationDate() {
        return allocationDate;
    }

    public void setAllocationDate(Time allocationDate) {
        this.allocationDate = allocationDate;
    }

    @Basic
    @Column(name = "address_phone", nullable = true, length = 15)
    public String getAddressPhone() {
        return addressPhone;
    }

    public void setAddressPhone(String addressPhone) {
        this.addressPhone = addressPhone;
    }

    @Basic
    @Column(name = "allocation_status", nullable = true, precision = 0)
    public Boolean getAllocationStatus() {
        return allocationStatus;
    }

    public void setAllocationStatus(Boolean allocationStatus) {
        this.allocationStatus = allocationStatus;
    }

    @Basic
    @Column(name = "allocation_other", nullable = true, length = 100)
    public String getAllocationOther() {
        return allocationOther;
    }

    public void setAllocationOther(String allocationOther) {
        this.allocationOther = allocationOther;
    }

    @Basic
    @Column(name = "login_delete", nullable = true, precision = 0)
    public Boolean getLoginDelete() {
        return loginDelete;
    }

    public void setLoginDelete(Boolean loginDelete) {
        this.loginDelete = loginDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AllocationEntity that = (AllocationEntity) o;
        return Objects.equals(allocationId, that.allocationId) &&
                Objects.equals(allocationName, that.allocationName) &&
                Objects.equals(allocationDate, that.allocationDate) &&
                Objects.equals(addressPhone, that.addressPhone) &&
                Objects.equals(allocationStatus, that.allocationStatus) &&
                Objects.equals(allocationOther, that.allocationOther) &&
                Objects.equals(loginDelete, that.loginDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(allocationId, allocationName, allocationDate, addressPhone, allocationStatus, allocationOther, loginDelete);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "tb__user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByTbUserId() {
        return tbUserByTbUserId;
    }

    public void setTbUserByTbUserId(UserEntity tbUserByTbUserId) {
        this.tbUserByTbUserId = tbUserByTbUserId;
    }

    @ManyToOne
    @JoinColumn(name = "wh_id", referencedColumnName = "wh_id")
    public WarehouseEntity getTbWarehouseByWhId() {
        return tbWarehouseByWhId;
    }

    public void setTbWarehouseByWhId(WarehouseEntity tbWarehouseByWhId) {
        this.tbWarehouseByWhId = tbWarehouseByWhId;
    }

    @ManyToOne
    @JoinColumn(name = "tb__wh_id", referencedColumnName = "wh_id")
    public WarehouseEntity getTbWarehouseByTbWhId() {
        return tbWarehouseByTbWhId;
    }

    public void setTbWarehouseByTbWhId(WarehouseEntity tbWarehouseByTbWhId) {
        this.tbWarehouseByTbWhId = tbWarehouseByTbWhId;
    }
}
