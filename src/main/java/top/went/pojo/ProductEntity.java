package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_product", schema = "USER_CRM", catalog = "")
public class ProductEntity {
    private Integer productId;
    private String productName;
    private Boolean productStock;
    private String productModel;
    private Boolean logicDelete;
    @JSONField(serialize = false)
    private ProductCategoryEntity tbProductCategoryByPcId;
    @JSONField(serialize = false)
    private Collection<ProductFormatEntity> tbProductFormatsByProductId;

    @Id
    @Column(name = "product_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "product_name", nullable = true, length = 30)
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Basic
    @Column(name = "product_stock", nullable = true, precision = 0)
    public Boolean getProductStock() {
        return productStock;
    }

    public void setProductStock(Boolean productStock) {
        this.productStock = productStock;
    }

    @Basic
    @Column(name = "product_model", nullable = true, length = 30)
    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    @Basic
    @Column(name = "logic_delete", nullable = true, precision = 0)
    public Boolean getLogicDelete() {
        return logicDelete;
    }

    public void setLogicDelete(Boolean logicDelete) {
        this.logicDelete = logicDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductEntity that = (ProductEntity) o;
        return Objects.equals(productId, that.productId) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(productStock, that.productStock) &&
                Objects.equals(productModel, that.productModel) &&
                Objects.equals(logicDelete, that.logicDelete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productName, productStock, productModel, logicDelete);
    }


    @ManyToOne
    @JoinColumn(name = "pc_id", referencedColumnName = "pc_id")
    public ProductCategoryEntity getTbProductCategoryByPcId() {
        return tbProductCategoryByPcId;
    }

    public void setTbProductCategoryByPcId(ProductCategoryEntity tbProductCategoryByPcId) {
        this.tbProductCategoryByPcId = tbProductCategoryByPcId;
    }

    @OneToMany(mappedBy = "tbProductByProductId")
    public Collection<ProductFormatEntity> getTbProductFormatsByProductId() {
        return tbProductFormatsByProductId;
    }

    public void setTbProductFormatsByProductId(Collection<ProductFormatEntity> tbProductFormatsByProductId) {
        this.tbProductFormatsByProductId = tbProductFormatsByProductId;
    }
}
