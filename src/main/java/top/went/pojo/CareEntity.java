package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "tb_care", schema = "USER_CRM")
public class CareEntity {
    private Integer careId;
    private String careTheme;
    @JSONField(format = "yyyy-MM-dd")
    private Date careTime;
    private String careType;
    private String careContent;
    private String careRemark;
    private Integer careIsDelete;
    private String careBack;
    private ContactsEntity tbContactsByCotsId;
    private CustomerEntity tbCustomerByCusId;
    private UserEntity tbUserByUserId;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    @Column(name = "care_id", nullable = false, precision = 0)
    public Integer getCareId() {
        return careId;
    }

    public void setCareId(Integer careId) {
        this.careId = careId;
    }

    @Basic
    @Column(name = "care_theme", nullable = true, length = 200)
    public String getCareTheme() {
        return careTheme;
    }

    public void setCareTheme(String careTheme) {
        this.careTheme = careTheme;
    }

    @Basic
    @Column(name = "care_time", nullable = true)
    public Date getCareTime() {
        return careTime;
    }

    public void setCareTime(Date careTime) {
        this.careTime = careTime;
    }

    @Basic
    @Column(name = "care_type", nullable = true, length = 20)
    public String getCareType() {
        return careType;
    }

    public void setCareType(String careType) {
        this.careType = careType;
    }

    @Basic
    @Column(name = "care_content", nullable = true, length = 1000)
    public String getCareContent() {
        return careContent;
    }

    public void setCareContent(String careContent) {
        this.careContent = careContent;
    }

    @Basic
    @Column(name = "care_remark", nullable = true, length = 2000)
    public String getCareRemark() {
        return careRemark;
    }

    public void setCareRemark(String careRemark) {
        this.careRemark = careRemark;
    }

    @Basic
    @Column(name = "care_is_delete", nullable = true)
    public Integer getCareIsDelete() {
        return careIsDelete;
    }

    public void setCareIsDelete(Integer careIsDelete) {
        this.careIsDelete = careIsDelete;
    }

    @Basic
    @Column(name = "care_back", nullable = true)
    public String getCareBack() {
        return careBack;
    }

    public void setCareBack(String careBack) {
        this.careBack = careBack;
    }

    @Override
    public String toString() {
        return "CareEntity{" +
                "careId=" + careId +
                ", careTheme='" + careTheme + '\'' +
                ", careTime=" + careTime +
                ", careType='" + careType + '\'' +
                ", careContent='" + careContent + '\'' +
                ", careRemark='" + careRemark + '\'' +
                ", careIsDelete=" + careIsDelete +
                ", tbContactsByCotsId=" + tbContactsByCotsId +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbUserByUserId=" + tbUserByUserId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CareEntity that = (CareEntity) o;
        return Objects.equals(careId, that.careId) &&
                Objects.equals(careTheme, that.careTheme) &&
                Objects.equals(careTime, that.careTime) &&
                Objects.equals(careType, that.careType) &&
                Objects.equals(careContent, that.careContent) &&
                Objects.equals(careRemark, that.careRemark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(careId, careTheme, careTime, careType, careContent, careRemark);
    }

    @ManyToOne
    @JoinColumn(name = "cots_id", referencedColumnName = "cots_id")
    public ContactsEntity getTbContactsByCotsId() {
        return tbContactsByCotsId;
    }

    public void setTbContactsByCotsId(ContactsEntity tbContactsByCotsId) {
        this.tbContactsByCotsId = tbContactsByCotsId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }
}
