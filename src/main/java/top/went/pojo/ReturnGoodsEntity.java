package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Time;
import java.util.Collection;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_return_goods", schema = "USER_CRM", catalog = "")
public class ReturnGoodsEntity {
    private Integer rgId;
    private String rgOddNumbers;
    @JSONField(format = "yyyy-MM-dd")
    private Date rgReturnTime;
    private String rgState;
    private String rgAccessories;
    private Long magicDelete;
    private Long rgType;
    private String rgTheme;
    private OrderEntity tbOrderByOrderId;
    private PurchaseEntity tbPurchaseByPurId;
    private UserEntity tbUserByUserId;
    private Double planMoney;
    private Double toMoney;
    private String putState;
    private String payBackState;
    private Collection<ReturnGoodsDetailEntity> tbReturnGoodsDetailsByRgId;
    private Collection<WhPullEntity> tbWhPullsByRgId;
    private Collection<WhPutEntity> tbWhPutsByRgId;
    private WarehouseEntity warehouseEntity;

    @Id
    @Column(name = "rg_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="mseq")
    @SequenceGenerator(name="mseq",sequenceName="seq",allocationSize=1)
    public Integer getRgId() {
        return rgId;
    }

    public void setRgId(Integer rgId) {
        this.rgId = rgId;
    }

    @Basic
    @Column(name = "rg_odd_numbers", nullable = true, length = 20)
    public String getRgOddNumbers() {
        return rgOddNumbers;
    }

    public void setRgOddNumbers(String rgOddNumbers) {
        this.rgOddNumbers = rgOddNumbers;
    }


    @Basic
    @Column(name = "rg_return_time", nullable = true)
    public Date getRgReturnTime() {
        return rgReturnTime;
    }

    public void setRgReturnTime(Date rgReturnTime) {
        this.rgReturnTime = rgReturnTime;
    }

    @Basic
    @Column(name = "rg_state", nullable = true, length = 20)
    public String getRgState() {
        return rgState;
    }

    public void setRgState(String rgState) {
        this.rgState = rgState;
    }

    @Basic
    @Column(name = "rg_accessories", nullable = true, length = 100)
    public String getRgAccessories() {
        return rgAccessories;
    }

    public void setRgAccessories(String rgAccessories) {
        this.rgAccessories = rgAccessories;
    }

    @Basic
    @Column(name = "magic_delete", nullable = true, precision = 0)
    public Long getMagicDelete() {
        return magicDelete;
    }

    public void setMagicDelete(Long magicDelete) {
        this.magicDelete = magicDelete;
    }

    @Basic
    @Column(name = "rg_type", nullable = true, precision = 0)
    public Long getRgType() {
        return rgType;
    }

    public void setRgType(Long rgType) {
        this.rgType = rgType;
    }
    @Basic
    @Column(name = "rg_theme", nullable = true, precision = 0)
    public String getRgTheme() {
        return rgTheme;
    }

    public void setRgTheme(String rgTheme) {
        this.rgTheme = rgTheme;
    }
    @Basic
    @Column(name = "plan_money", nullable = true, precision = 0)
    public Double getPlanMoney() {
        return planMoney;
    }

    public void setPlanMoney(Double planMoney) {
        this.planMoney = planMoney;
    }
    @Basic
    @Column(name = "to_money", nullable = true, precision = 0)
    public Double getToMoney() {
        return toMoney;
    }

    public void setToMoney(Double toMoney) {
        this.toMoney = toMoney;
    }
    @Basic
    @Column(name = "put_state", nullable = true, precision = 0)
    public String getPutState() {
        return putState;
    }

    public void setPutState(String putState) {
        this.putState = putState;
    }

    @Basic
    @Column(name = "pay_back_state", nullable = true, precision = 0)
    public String getPayBackState() {
        return payBackState;
    }

    public void setPayBackState(String payBackState) {
        this.payBackState = payBackState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReturnGoodsEntity that = (ReturnGoodsEntity) o;
        return Objects.equals(rgId, that.rgId) &&
                Objects.equals(rgOddNumbers, that.rgOddNumbers) &&
                Objects.equals(rgReturnTime, that.rgReturnTime) &&
                Objects.equals(rgState, that.rgState) &&
                Objects.equals(rgAccessories, that.rgAccessories) &&
                Objects.equals(magicDelete, that.magicDelete) &&
                Objects.equals(rgType, that.rgType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rgId, rgOddNumbers, rgReturnTime, rgState, rgAccessories, magicDelete, rgType);
    }


    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    public OrderEntity getTbOrderByOrderId() {
        return tbOrderByOrderId;
    }

    public void setTbOrderByOrderId(OrderEntity tbOrderByOrderId) {
        this.tbOrderByOrderId = tbOrderByOrderId;
    }

    @ManyToOne
    @JoinColumn(name = "warehouse_id", referencedColumnName = "wh_id")
    public WarehouseEntity getWarehouseEntity() {
        return warehouseEntity;
    }

    public void setWarehouseEntity(WarehouseEntity warehouseEntity) {
        this.warehouseEntity = warehouseEntity;
    }

    @ManyToOne
    @JoinColumn(name = "pur_id", referencedColumnName = "pur_id")
    public PurchaseEntity getTbPurchaseByPurId() {
        return tbPurchaseByPurId;
    }

    public void setTbPurchaseByPurId(PurchaseEntity tbPurchaseByPurId) {
        this.tbPurchaseByPurId = tbPurchaseByPurId;
    }

    @OneToMany(mappedBy = "tbReturnGoodsByRgId")
    public Collection<ReturnGoodsDetailEntity> getTbReturnGoodsDetailsByRgId() {
        return tbReturnGoodsDetailsByRgId;
    }

    public void setTbReturnGoodsDetailsByRgId(Collection<ReturnGoodsDetailEntity> tbReturnGoodsDetailsByRgId) {
        this.tbReturnGoodsDetailsByRgId = tbReturnGoodsDetailsByRgId;
    }

    @OneToMany(mappedBy = "tbReturnGoodsByRgId")
    public Collection<WhPullEntity> getTbWhPullsByRgId() {
        return tbWhPullsByRgId;
    }

    public void setTbWhPullsByRgId(Collection<WhPullEntity> tbWhPullsByRgId) {
        this.tbWhPullsByRgId = tbWhPullsByRgId;
    }
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @OneToMany(mappedBy = "tbReturnGoodsByRgId")
    public Collection<WhPutEntity> getTbWhPutsByRgId() {
        return tbWhPutsByRgId;
    }

    public void setTbWhPutsByRgId(Collection<WhPutEntity> tbWhPutsByRgId) {
        this.tbWhPutsByRgId = tbWhPutsByRgId;
    }
}
