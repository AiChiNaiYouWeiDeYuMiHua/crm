package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tb_functions", schema = "USER_CRM", catalog = "")
public class FunctionsEntity {
    @JSONField(name = "nodeid")
    private Long fcId;
    @JSONField(name = "text")
    private String fcName;
    //    private Collection<RoleEntity> roleFcEntities;
    private Collection<RoleFcEntity> tbRoleFcsByFcId;
    private FcTypeEntity tbFcTypeByFcTypeId;

    @Id
    @Column(name = "fc_id", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "myseq")
    @SequenceGenerator(name = "myseq", sequenceName = "seq", allocationSize = 1)
    public Long getFcId() {
        return fcId;
    }

    public void setFcId(Long fcId) {
        this.fcId = fcId;
    }

    @Basic
    @Column(name = "fc_name", nullable = true, length = 50)
    public String getFcName() {
        return fcName;
    }

    public void setFcName(String fcName) {
        this.fcName = fcName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FunctionsEntity that = (FunctionsEntity) o;
        return Objects.equals(fcId, that.fcId) &&
                Objects.equals(fcName, that.fcName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fcId, fcName);
    }

    @OneToMany(mappedBy = "tbFunctionsByFcId")
    public Collection<RoleFcEntity> getTbRoleFcsByFcId() {
        return tbRoleFcsByFcId;
    }

    public void setTbRoleFcsByFcId(Collection<RoleFcEntity> tbRoleFcsByFcId) {
        this.tbRoleFcsByFcId = tbRoleFcsByFcId;
    }

//    @ManyToMany(mappedBy = "functionsEntities")
//    public Collection<RoleEntity> getRoleFcEntities() {
//        return roleFcEntities;
//    }
//
//    public void setRoleFcEntities(Collection<RoleEntity> roleFcEntities) {
//        this.roleFcEntities = roleFcEntities;
//    }

    @ManyToOne
    @JoinColumn(name = "fc_type_id", referencedColumnName = "fc_type_id")
    public FcTypeEntity getTbFcTypeByFcTypeId() {
        return tbFcTypeByFcTypeId;
    }

    public void setTbFcTypeByFcTypeId(FcTypeEntity tbFcTypeByFcTypeId) {
        this.tbFcTypeByFcTypeId = tbFcTypeByFcTypeId;
    }
}
