package top.went.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "tb_quote", schema = "USER_CRM", catalog = "")
public class QuoteEntity {
    private Long quoteId;
    private String quoteTheme;
    private String quoteNumber;
    private String classification;
    @JSONField(format = "yyyy-MM-dd")
    private Date quoteTime;
    private Double totalQuote;
    private String quotationContact;
    private String remarks;
    private String annex;
    private Long approvalStatus;
    private Long isDelete;
    private Boolean cusOk;
    @JSONField(serialize = false)
    private Collection<OrderEntity> tbOrdersByQuoteId;
    private CustomerEntity tbCustomerByCusId;
    private SaleOppEntity tbSaleOppByOppId;
    private UserEntity tbUserByUserId;
    private ContactsEntity tbContactsBy接收人Id;
//    @JSONField(serialize = false)
    private Collection<QuoteDetailEntity> tbQuoteDetailsByQuoteId;

    private Double expectedProfit;
    private Date plan_start_time;
    private Date plan_end_time;
    private Long isOrder;

    @Basic
    @Column(name = "plan_start_time", nullable = true)
    public Date getPlan_start_time() {
        return plan_start_time;
    }

    public void setPlan_start_time(Date plan_start_time) {
        this.plan_start_time = plan_start_time;
    }

    @Basic
    @Column(name = "plan_end_time", nullable = true)
    public Date getPlan_end_time() {
        return plan_end_time;
    }

    public void setPlan_end_time(Date plan_end_time) {
        this.plan_end_time = plan_end_time;
    }

    @Basic
    @Column(name = "is_order", nullable = true, precision = 0)
    public Long getIsOrder() {
        return isOrder;
    }

    public void setIsOrder(Long isOrder) {
        this.isOrder = isOrder;
    }

    @Basic
    @Column(name = "expected_profit", nullable = true, precision = 2)
    public Double getExpectedProfit() {
        return expectedProfit;
    }

    public void setExpectedProfit(Double expectedProfit) {
        this.expectedProfit = expectedProfit;
    }

    @Id
    @Column(name = "quote_id", nullable = false, precision = 0)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="myseq")
    @SequenceGenerator(name="myseq",sequenceName="seq",allocationSize=1)
    public Long getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(Long quoteId) {
        this.quoteId = quoteId;
    }

    @Basic
    @Column(name = "quote_theme", nullable = true, length = 30)
    public String getQuoteTheme() {
        return quoteTheme;
    }

    public void setQuoteTheme(String quoteTheme) {
        this.quoteTheme = quoteTheme;
    }

    @Basic
    @Column(name = "quote_number", nullable = true, length = 30)
    public String getQuoteNumber() {
        return quoteNumber;
    }

    public void setQuoteNumber(String quoteNumber) {
        this.quoteNumber = quoteNumber;
    }

    @Basic
    @Column(name = "classification", nullable = true, length = 30)
    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    @Basic
    @Column(name = "quote_time", nullable = true)
    public Date getQuoteTime() {
        return quoteTime;
    }

    public void setQuoteTime(Date quoteTime) {
        this.quoteTime = quoteTime;
    }

    @Basic
    @Column(name = "total_quote", nullable = true, precision = 2)
    public Double getTotalQuote() {
        return totalQuote;
    }

    public void setTotalQuote(Double totalQuote) {
        this.totalQuote = totalQuote;
    }

    @Basic
    @Column(name = "quotation_contact", nullable = true, length = 30)
    public String getQuotationContact() {
        return quotationContact;
    }

    public void setQuotationContact(String quotationContact) {
        this.quotationContact = quotationContact;
    }

    @Basic
    @Column(name = "remarks", nullable = true, length = 100)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "annex", nullable = true, length = 50)
    public String getAnnex() {
        return annex;
    }

    public void setAnnex(String annex) {
        this.annex = annex;
    }

    @Basic
    @Column(name = "is_delete", nullable = true, precision = 0)
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Basic
    @Column(name = "approval_status", nullable = true, precision = 0)
    public Long getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(Long approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Basic
    @Column(name = "cus_ok", nullable = true, precision = 0)
    public Boolean getCusOk() {
        return cusOk;
    }

    public void setCusOk(Boolean cusOk) {
        this.cusOk = cusOk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuoteEntity that = (QuoteEntity) o;
        return Objects.equals(quoteId, that.quoteId) &&
                Objects.equals(quoteTheme, that.quoteTheme) &&
                Objects.equals(quoteNumber, that.quoteNumber) &&
                Objects.equals(classification, that.classification) &&
                Objects.equals(quoteTime, that.quoteTime) &&
                Objects.equals(totalQuote, that.totalQuote) &&
                Objects.equals(quotationContact, that.quotationContact) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(annex, that.annex) &&
                Objects.equals(isDelete, that.isDelete) &&
                Objects.equals(cusOk, that.cusOk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quoteId, quoteTheme, quoteNumber, classification, quoteTime, totalQuote, quotationContact, remarks, annex, isDelete, cusOk);
    }

    @OneToMany(mappedBy = "tbQuoteByQuoteId")
    public Collection<OrderEntity> getTbOrdersByQuoteId() {
        return tbOrdersByQuoteId;
    }

    public void setTbOrdersByQuoteId(Collection<OrderEntity> tbOrdersByQuoteId) {
        this.tbOrdersByQuoteId = tbOrdersByQuoteId;
    }

    @ManyToOne
    @JoinColumn(name = "cus_id", referencedColumnName = "cus_id")
    public CustomerEntity getTbCustomerByCusId() {
        return tbCustomerByCusId;
    }

    public void setTbCustomerByCusId(CustomerEntity tbCustomerByCusId) {
        this.tbCustomerByCusId = tbCustomerByCusId;
    }

    @ManyToOne
    @JoinColumn(name = "opp_id", referencedColumnName = "opp_id")
    public SaleOppEntity getTbSaleOppByOppId() {
        return tbSaleOppByOppId;
    }

    public void setTbSaleOppByOppId(SaleOppEntity tbSaleOppByOppId) {
        this.tbSaleOppByOppId = tbSaleOppByOppId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getTbUserByUserId() {
        return tbUserByUserId;
    }

    public void setTbUserByUserId(UserEntity tbUserByUserId) {
        this.tbUserByUserId = tbUserByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "接收人id", referencedColumnName = "cots_id")
    public ContactsEntity getTbContactsBy接收人Id() {
        return tbContactsBy接收人Id;
    }

    public void setTbContactsBy接收人Id(ContactsEntity tbContactsBy接收人Id) {
        this.tbContactsBy接收人Id = tbContactsBy接收人Id;
    }

    @OneToMany(mappedBy = "tbQuoteByQuoteId")
    public Collection<QuoteDetailEntity> getTbQuoteDetailsByQuoteId() {
        return tbQuoteDetailsByQuoteId;
    }

    public void setTbQuoteDetailsByQuoteId(Collection<QuoteDetailEntity> tbQuoteDetailsByQuoteId) {
        this.tbQuoteDetailsByQuoteId = tbQuoteDetailsByQuoteId;
    }

    @Override
    public String toString() {
        return "QuoteEntity{" +
                "quoteId=" + quoteId +
                ", quoteTheme='" + quoteTheme + '\'' +
                ", quoteNumber='" + quoteNumber + '\'' +
                ", classification='" + classification + '\'' +
                ", quoteTime=" + quoteTime +
                ", totalQuote=" + totalQuote +
                ", quotationContact='" + quotationContact + '\'' +
                ", remarks='" + remarks + '\'' +
                ", annex='" + annex + '\'' +
                ", approvalStatus=" + approvalStatus +
                ", isDelete=" + isDelete +
                ", cusOk=" + cusOk +
                ", tbOrdersByQuoteId=" + tbOrdersByQuoteId +
                ", tbCustomerByCusId=" + tbCustomerByCusId +
                ", tbSaleOppByOppId=" + tbSaleOppByOppId +
                ", tbUserByUserId=" + tbUserByUserId +
                ", tbContactsBy接收人Id=" + tbContactsBy接收人Id +
                ", tbQuoteDetailsByQuoteId=" + tbQuoteDetailsByQuoteId +
                ", expectedProfit=" + expectedProfit +
                '}';
    }
}
