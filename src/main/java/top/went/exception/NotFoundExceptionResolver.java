package top.went.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.vo.Code;

/**
 * 页面没有找到
 */
@ControllerAdvice
@ResponseBody
public class NotFoundExceptionResolver  {
    @ExceptionHandler(NotFoundException.class)
    public Code resolveException(Exception e) {
        e.printStackTrace();
        return Code.fail(e.getMessage());
    }
}
