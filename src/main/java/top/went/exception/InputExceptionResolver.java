package top.went.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.vo.Code;

/**
 * 处理输入异常
 */
@ControllerAdvice
@ResponseBody
public class InputExceptionResolver {
    @ExceptionHandler(InputException.class)
    public Code error(Exception e){
        e.printStackTrace();
        return Code.fail(e);
    }
}
