package top.went.controller;

import com.alibaba.fastjson.JSON;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.PlanPayDetailEntity;
import top.went.pojo.PurchaseEntity;
import top.went.pojo.UserEntity;
import top.went.service.PlanPayDetailService;
import top.went.service.PurchaseService;
import top.went.service.UserService;
import top.went.vo.*;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/sale")
@ResponseBody
public class PlanPayDetailController {
    @Autowired
    private PlanPayDetailService planPayDetailService;
    @Autowired
    private PurchaseService purchaseService;

    @GetMapping("/mainPlanPayDetail")
    public ModelAndView mainPlanPayDetail() {
        return new ModelAndView("sale/MainPlanPayDetail");
    }

    @GetMapping("/modal")
    public ModelAndView modal() {
        return new ModelAndView("sale/modal/super_query_model");
    }

    @GetMapping("/modal1")
    public ModelAndView modal1() {
        return new ModelAndView("modal");
    }

    @GetMapping("/add_modal")
    public ModelAndView addModal(String purId,HttpSession session) throws NotFoundException, ServiceException {
        ModelAndView mv = new ModelAndView();
        PurchaseEntity purchaseEntity=new PurchaseEntity();
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        if(purId!=null&&purId.length()>0){
            purchaseEntity=purchaseService.load(Integer.valueOf(purId));
            mv.addObject("purchaseEntity", purchaseEntity);
        }
        mv.addObject("user", userEntity);
        mv.setViewName("sale/modal/add_plan_pay_model");
        return mv;
    }

    @GetMapping("/detail")
    public ModelAndView planPayDetail() {
        return new ModelAndView("sale/planPayDetail");
    }

    /**
     * 添加计划付款
     *
     * @param planPayDetailEntity
     * @throws ServiceException
     */
    @PostMapping("/ppd_insert")
    public Code ppd_insert(PlanPayDetailEntity planPayDetailEntity) throws ServiceException {
        planPayDetailEntity.setMagicDelete(0l);
        if (planPayDetailService.ppd_insert(planPayDetailEntity))
            return Code.success("添加计划付款成功");
        return Code.fail();
    }

    /**
     * 删除计划付款
     *
     * @param ppdId
     * @throws ServiceException
     */
    @PostMapping("/ppd_delete")
    @ResponseBody
    public Code ppd_delete(int ppdId) throws ServiceException, NotFoundException {
            planPayDetailService.ppd_logicDelete(ppdId);
                return Code.success("删除计划付款成功");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/deleteall")
    @ResponseBody
    public Code deleteAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for (int i = 0; i < ids.length; i++)
            if (ids[i] == null || ids[i].toString().length() <= 0)
                throw new InputException("计划付款id错误");
        planPayDetailService.ppd_logicDeleteAll(ids);
        return Code.success("计划付款批量删除成功");
    }

    /**
     * 查询所有计划付款
     *
     * @return
     * @throws ServiceException
     */
    @PostMapping("/ppd_findAll")
    @ResponseBody
    public PageEntity<PlanPayDetailVO> findAll(@RequestBody top.went.vo.Page page) throws ServiceException {
        return planPayDetailService.ppd_findAll(page.getSize(), page.getPage());
    }

    /**
     * 根据计划日期查询所有计划付款
     *
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    @PostMapping("/ppd_findAllByDate")
    @ResponseBody
    public PageEntity<PlanPayDetailVO> findAllByDate(Integer pageSize, Integer pageNumber, String ppd_date) throws ServiceException {
        System.out.println("pageSize" + pageSize + " " + "pageNumber" + pageNumber);
        if (null == pageSize || null == pageNumber) {
            pageSize = 10;
            pageNumber = 1;
        }
        return planPayDetailService.ppd_findByPpdDate(pageSize, pageNumber, ppd_date.toString());
    }


    /**
     * 加载
     *
     * @param ppdId
     * @return
     */
    @GetMapping("pur_load")
    @ResponseBody
    public Code load(Integer ppdId) throws ServiceException {
        PlanPayDetailVO planPayDetailVO = null;
            planPayDetailVO = planPayDetailService.load1(ppdId);
        return Code.success(planPayDetailVO);
    }

    /**
     * 加载所有
     *
     * @return
     */
    @RequestMapping("/all")
    public PageEntity<PlanPayDetailEntity> loadAll(@RequestBody PlanPayDetailVO planPayDetailVO) {
        return planPayDetailService.findAllByManyConditions(planPayDetailVO);
    }

    /**
     * 查看详情
     *
     * @param ppdId
     * @return
     */
    @RequestMapping("/detail1/{ppdId}/")
    public ModelAndView detail(@PathVariable("ppdId") int ppdId) {
        if (new Integer(ppdId) == null) {
            return new ModelAndView("404");
        }
        ModelAndView mv = new ModelAndView("sale/planPayDetail");
        PlanPayDetailEntity planPayDetailEntity = null;
        try {
            planPayDetailEntity = planPayDetailService.load(ppdId);
            if (planPayDetailEntity == null)
                return new ModelAndView("404");
        } catch (ServiceException e) {
            e.printStackTrace();
            return new ModelAndView("404");
        }
        mv.addObject("info", planPayDetailEntity);
        return mv;
    }

    /**
     * 根据客户id查询采购单
     * @param cusId
     * @return
     */
    @RequestMapping("/queryPurByCus")
    public Code queryPurByCus(Integer cusId){
    PurchaseVO purchaseVO=new PurchaseVO();
    purchaseVO.setCusId(cusId);
    return Code.success(purchaseService.findAllByManyConditions(purchaseVO).getRows());
    }
}
