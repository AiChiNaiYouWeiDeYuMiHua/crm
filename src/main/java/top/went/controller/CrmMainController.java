package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import top.went.exception.ServiceException;
import top.went.pojo.UserEntity;
import top.went.service.*;
import top.went.vo.Code;

import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.serial.SerialArray;
import java.math.BigDecimal;

@Controller
public class CrmMainController {

    @Autowired
    private CrmMainService crmMainService;

    @GetMapping("/to_login")
    public String toLogin() {
        return "login";
    }

    @GetMapping("/index")
    public String index() {
        return "index";
    }

    @GetMapping("/main")
    public String tomain() {
        return "main";
    }

    @GetMapping("/404")
    public String to404() {
        return "404";
    }

    @GetMapping("/500")
    public String to500() {
        return "500";
    }

    @GetMapping("/layout")
    public String layout(HttpServletRequest request) {
        request.getSession().invalidate();
        return "redirect:/to_login";
    }

    @GetMapping("/indexv1")
    public String indexv1() {
        return "admin/indexv1";
    }

    @GetMapping("/setFF")
    @ResponseBody
    public Code setFF() throws ServiceException {
        return Code.success("加载成功", crmMainService.countFF());
    }

    @GetMapping("/set_countmy")
    @ResponseBody
    public Code setCountMy(Long userId) throws ServiceException {
//        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        UserEntity user = (UserEntity) request.getSession().getAttribute("user");
        return Code.success("加载成功", crmMainService.countMy(userId));
    }

    @GetMapping("/set_countfirm")
    @ResponseBody
    public Code setCountFirm() throws ServiceException {
        return Code.success("加载成功", crmMainService.countFirm());
    }
}
