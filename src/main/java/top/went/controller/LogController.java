package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.exception.ServiceException;
import top.went.pojo.LogEntity;
import top.went.service.LogService;
import top.went.vo.PageEntity;

import java.sql.Date;

@Controller
public class LogController {
    @Autowired
    private LogService logService;

    @GetMapping("/to_log_list")
    public String toLogList() {
        return "admin/log_list";
    }

    @GetMapping("/log_find_all")
    @ResponseBody
    public PageEntity<LogEntity> findAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        return logService.findAll(pageNumber, pageSize);
    }

    @GetMapping("/log_find_all_user_time")
    @ResponseBody
    public PageEntity<LogEntity> findAllByuserAndTime(Long userId, Date logOptionTime, Integer pageNumber, Integer pageSize)
            throws ServiceException {
        return logService.findAllByUserAndTime(userId, logOptionTime, pageNumber, pageSize);
    }

    @GetMapping("/log_find_all_user")
    @ResponseBody
    public PageEntity<LogEntity> findAllByUser(Long userId, Integer pageNumber, Integer pageSize)
            throws ServiceException {
        return logService.findAllByUser(userId, pageNumber, pageSize);
    }

    @GetMapping("/log_find_all_time")
    @ResponseBody
    public PageEntity<LogEntity> findAllByTime(Date logOptionTime, Integer pageNumber, Integer pageSize)
            throws ServiceException {
        return logService.findAllByTime(logOptionTime, pageNumber, pageSize);
    }

}
