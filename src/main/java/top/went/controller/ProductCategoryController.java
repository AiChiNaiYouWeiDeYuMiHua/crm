package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.ProductCategoryEntity;
import top.went.service.ProductCategoryService;
import top.went.vo.Code;
import top.went.vo.ProductCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * 产品类别controller
 */
@Controller
@RequestMapping("/sale/product")
@ResponseBody
public class ProductCategoryController {
    @Autowired
    private ProductCategoryService categoryService;

    /**
     * 加载类别树
     * @return
     */
    @RequestMapping("/category/tree")
    public Code loadCategory(){
        List<ProductCategory> data = new ArrayList<>();
        data.add(categoryService.loadList());
        return Code.success(data);
    }

    /**
     * 加载所有产品
     * @return
     */
    @RequestMapping("/category/all")
    public Code loadAll(){
        return Code.success(categoryService.loadAll());
    }



    /**
     * 加载类别面包屑
     * @param id
     * @return
     * @throws NotFoundException
     * @throws InputException
     */
    @RequestMapping("/category/load/{id}/")
    public Code load(@PathVariable("id") Long id) throws NotFoundException, InputException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("参数错误");
        System.out.println(id);
        return Code.success(categoryService.loadVo(id));
    }

    /**
     * 添加类别
     * @param id
     * @param name
     * @return
     * @throws NotFoundException
     * @throws InputException
     * @throws ServiceException
     */
    @PostMapping("/category/add")
    @SystemLog(module = "产品类别",methods = "添加产品类别")
    public Code add(Long id,String name) throws NotFoundException, InputException, ServiceException {
        if (name == null || name.length() <=0 || id == null || id.toString().length() <=0)
            throw new InputException("参数错误");
        return Code.success("添加成功",categoryService.addCategory(new ProductCategoryEntity(name,new ProductCategoryEntity(id))));
    }

    /**
     * 修改类别名称
     * @param id
     * @param name
     * @return
     * @throws NotFoundException
     * @throws InputException
     * @throws ServiceException
     */
    @PostMapping("/category/modify")
    @SystemLog(module = "产品类别",methods = "修改类别名称")
    public Code mdify(Long id, String name) throws NotFoundException, InputException, ServiceException {
        if (name == null || name.length() <=0 || id == null || id.toString().length() <=0)
            throw new InputException("参数错误");
        return Code.success("修改成功",categoryService.modifyCategory(new ProductCategoryEntity(id,name)));
    }

    /**
     * 删除类别
     * @param id
     * @return
     * @throws NotFoundException
     * @throws InputException
     * @throws ServiceException
     */
    @PostMapping("/category/delete")
    @SystemLog(module = "产品类别",methods = "删除产品类别")
    public Code delete(Long id) throws NotFoundException, InputException, ServiceException {
        if ( id == null || id.toString().length() <=0)
            throw new InputException("参数错误");
        return Code.success("删除成功",categoryService.deleteCategory(id));
    }
}
