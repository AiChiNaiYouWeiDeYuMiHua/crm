package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.db.dao.PurchaseDao;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.OrderDetailEntity;
import top.went.pojo.PurDetailEntity;
import top.went.pojo.ReturnGoodsDetailEntity;
import top.went.pojo.ReturnGoodsEntity;
import top.went.service.PurchaseDetailService;
import top.went.service.ReturnGoodsDetailService;
import top.went.service.ReturnGoodsService;
import top.went.utils.CheckForm;
import top.went.vo.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/sale")
@ResponseBody
public class ReturnGoodsDetailController {
    @Autowired
    private ReturnGoodsDetailService returnGoodsDetailService;
    @Autowired
    private ReturnGoodsService returnGoodsService;
    @Autowired
    private PurchaseDetailService purchaseDetailService;
    @Autowired
    private PurchaseDao purchaseDao;

    @GetMapping("/mainReturnGoodsDetail")
    public ModelAndView mainPurchaseDetail() {
        return new ModelAndView("sale/mainReturnGoodsDetail");
    }

    @GetMapping("/rgdAddModal")
    public ModelAndView modal() {
        return new ModelAndView("sale/modal/add_rgd_model");
    }

    @GetMapping("/rgdQueryModal")
    public ModelAndView rrQueryModal() {
        return new ModelAndView("sale/modal/rgd_super_query_model");
    }

    @GetMapping("/rgdDetail")
    public ModelAndView purchaseReceiptDetail() {
        return new ModelAndView("sale/rgdDetail");
    }

    /**
     * 添加退货明细
     *
     * @param returnGoodsDetailEntity
     * @throws ServiceException
     */
    @PostMapping("/rgd_insert")
    @ResponseBody
    public Code rr_insert(ReturnGoodsDetailEntity returnGoodsDetailEntity) throws ServiceException {
        returnGoodsDetailEntity.setMagicDelete(0l);
        returnGoodsDetailEntity.setRgdOutwhNum(0);
        if (returnGoodsDetailService.rgd_insert(returnGoodsDetailEntity))
            return Code.success("添加退货明细");
        return Code.fail();
    }

    /**
     * 删除退货明细
     *
     * @param rgdId
     * @throws ServiceException
     */
    @GetMapping("/rgddelete")
    @ResponseBody
    public Code rgd_delete(Integer rgdId) throws ServiceException, NotFoundException {
            returnGoodsDetailService.rgd_logicDelete(rgdId);
                return Code.success("删除退货明细成功");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/rgddeleteall")
    @ResponseBody
    public Code deleteAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for (int i = 0; i < ids.length; i++)
            if (ids[i] == null || ids[i].toString().length() <= 0)
                throw new InputException("id错误");
        returnGoodsDetailService.pd_logicDeleteAll(ids);
        return Code.success("批量删除成功");
    }

    /**
     * 查询所有退货明细
     *
     * @return
     * @throws ServiceException
     */
    @PostMapping("/rgdfindAll")
    @ResponseBody
    public PageEntity<ReturnGoodsDetailVo> findAll(@RequestBody top.went.vo.Page page) throws ServiceException {
        return returnGoodsDetailService.rgd_findAll(page.getSize(), page.getPage());
    }

    /**
     * 查询所有退货明细
     *
     * @return
     * @throws ServiceException
     */
    @PostMapping("/rgdfindAll1")
    @ResponseBody
    public List<ReturnGoodsDetailVo> findAll1(@RequestBody top.went.vo.Page page) throws ServiceException {
        return returnGoodsDetailService.rgd_findAll(page.getSize(), page.getPage()).getRows();
    }

    /**
     * 加载
     *
     * @param rgdId
     * @return
     */
    @GetMapping("rgdload")
    @ResponseBody
    public Code load(Integer rgdId) {
        ReturnGoodsDetailVo returnGoodsDetailVo = null;
        try {
            returnGoodsDetailVo = returnGoodsDetailService.load1(rgdId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return Code.success(returnGoodsDetailVo);
    }

    /**
     * 加载所有
     *
     * @return
     */
    @RequestMapping("/rgdall")
    @ResponseBody
    public PageEntity<ReturnGoodsDetailVo> loadAll(@RequestBody ReturnGoodsDetailVo returnGoodsDetailVo) {
        return returnGoodsDetailService.findAllByManyConditions(returnGoodsDetailVo);
    }

    /**
     * 加载所有
     *
     * @return
     */
    @RequestMapping("/rgdall1")
    @ResponseBody
    public List<ReturnGoodsDetailVo> loadAll1(Integer rgId,Integer type) {
        return returnGoodsDetailService.loadDetails(rgId,type);
    }

    /**
     * 查看详情
     *
     * @param rgdId
     * @return
     */
    @RequestMapping("/rgddetail1/{rgdId}/")
    public ModelAndView detail(@PathVariable("rgdId") int rgdId) {
        if (new Integer(rgdId) == null) {
            return new ModelAndView("404");
        }
        ModelAndView mv = new ModelAndView("sale/purchaseDetailDetail");
        ReturnGoodsDetailEntity returnGoodsDetailEntity = null;
        try {
            returnGoodsDetailEntity = returnGoodsDetailService.load(rgdId);
            if (returnGoodsDetailEntity == null)
                return new ModelAndView("404");
        } catch (ServiceException e) {
            e.printStackTrace();
            return new ModelAndView("404");
        }
        mv.addObject("info", returnGoodsDetailEntity);
        return mv;
    }

    /**
     * 保存退货单明细数据
     *
     * @param returnGoodsDetailVO1
     * @return
     */
    @PostMapping("/rgddetail/save")
    public Code saveDetail(ReturnGoodsDetailVO1 returnGoodsDetailVO1) throws InputException, NotFoundException, ServiceException {
        List<ReturnGoodsDetailEntity> returnGoodsDetailEntities = returnGoodsDetailVO1.getDatas();
       returnGoodsDetailService.modifyDetail(returnGoodsDetailEntities);
        return Code.success();
    }

    /**
     * 将采购明细转退货明细
     */
    @GetMapping("/rgdsave1")
    public Code loadAll(int rgId, Integer purId) throws ServiceException, NotFoundException {
        returnGoodsDetailService.loadAll(rgId, purId);
        return  Code.success();
    }
    /**
     * 将采购明细转退货明细
     */
    @GetMapping("/orderToRg")
    public Code orderToRg(int rgId, Integer orderId) throws ServiceException, NotFoundException {
        returnGoodsDetailService.orderToRg(rgId, orderId);
        return  Code.success();
    }
}
