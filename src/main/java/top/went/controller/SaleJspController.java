package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import top.went.exception.NotFoundException;
import top.went.service.ProductService;

/**
 * 售中页面转发
 */
@RequestMapping("/sale")
@Controller
public class SaleJspController {

    @Autowired
    private ProductService productService;

    /**
     * 产品分类跳转
     * @return
     */
    @RequestMapping("/product/category")
    public String productCategory(){
        return "sale/product_category";
    }
    /**
     * 产品分类跳转
     * @return
     */
    @RequestMapping("/product/category/modal")
    public String productCategoryModal(){
        return "sale/modal/product_category";
    }

    /**
     * 添加产品
     * @return
     */
    @RequestMapping("/product/add")
    public String productAdd(){
        return "sale/modal/add_product";
    }

    /**
     * 添加产品规格
     * @return
     */
    @RequestMapping("/product/formatter/add")
    public String productFormatterAdd(){
        return "sale/modal/add_product_formatter";
    }

    /**
     * 产品高级搜索
     * @return
     */
    @RequestMapping("/product/search")
    public String productSearch(){
        return "sale/modal/search_product";
    }


    /**
     * 订单高级搜索
     * @return
     */
    @RequestMapping("/order/search")
    public String orderSearch(){
        return "sale/modal/search_order";
    }
    /**
     * 产品table
     * @return
     */
    @RequestMapping("/product")
    public String product(){
        return "sale/product_view";
    }

    /**
     * 订单table
     * @return
     */
    @RequestMapping("/order")
    public String order(){
        return "sale/order_view";
    }

    /**
     * 添加合同
     * @return
     */
    @RequestMapping("/contract/add")
    public String contractAdd(){
        return "sale/modal/add_contract";
    }

    /**
     * 添加订单
     * @return
     */
    @RequestMapping("/order/add")
    public String orderAdd(){
        return "sale/modal/add_order";
    }

    @RequestMapping("/test")
    public String test(){
        return "sale/purchase";
    }

    /**
     * 订单明细
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/orderDetail")
    public String orderDeatil(Long id, Model model){
        model.addAttribute("id",id);
        return "sale/table/orderDeatilTable";
    }
    /**
     * 订单明细
     * @return
     */
    @RequestMapping("/order/to_detail")
    public String orderDeatilView(){
        return "sale/order_detail_view";
    }
    /**
     * 选择产品
     * @return
     */
    @RequestMapping("/chooseProduct")
    public String chooseProduct(){
        return "sale/modal/choose_product";
    }

    /**
     * 出库单table
     * @return
     */
    @RequestMapping("/warehouse/out")
    public String outStorage(){
        return "sale/out_storage_view";
    }

    /**
     * 入库单table
     * @return
     */
    @RequestMapping("/warehouse/in")
    public String inStorage(){
        return "sale/in_storage_view";
    }
    /**
     * 添加出库单
     * @return
     */
    @RequestMapping("/warehouse/out/add")
    public String outStorageAdd(){
        return "sale/modal/add_out_storage";
    }

    /**
     * 添加入库单
     * @return
     */
    @RequestMapping("/warehouse/in/add")
    public String inStorageAdd(){
        return "sale/modal/in_out_storage";
    }
    /**
     * 库存流水table
     * @return
     */
    @RequestMapping("/warehouse/detail")
    public String detailStorage(){
        return "sale/detail_storage_view";
    }

    /**
     * 出库明细table
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/outDetail")
    public String outDetail(Long id,Model model){
        model.addAttribute("id",id);
        return "sale/table/outDeatilTable";
    }
    /**
     * 入库明细table
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/inDetail")
    public String inDetail(Long id,Model model){
        model.addAttribute("id",id);
        return "sale/table/inDeatilTable";
    }

    /**
     * 发货单table
     * @return
     */
    @RequestMapping("/warehouse/send")
    public String send(){
        return "sale/send_goods_view";
    }
    /**
     * 发货明细table
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/sendDetail")
    public String sendDetail(Long id,Model model){
        model.addAttribute("id",id);
        return "sale/table/sendDeatilTable";
    }
    /**
     * 发货明细table
     * @param id
     * @return
     */
    @RequestMapping("/send/view")
    public String sendView(Long id){
        return "sale/sendGoods_detail_view";
    }

    /**
     * 发货明细table
     * @param id
     * @return
     */
    @RequestMapping("/send/add")
    public String sendAdd(Long id,Model model){
        model.addAttribute("id",id);
        return "sale/modal/modify_send";
    }

}
