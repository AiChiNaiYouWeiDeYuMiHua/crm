package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.aop.SystemLog;
import top.went.db.dao.WhPullDao;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.*;
import top.went.service.WareHouseOutIn;
import top.went.service.WareHouseService;
import top.went.service.WarehouseSend;
import top.went.utils.CheckForm;
import top.went.vo.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * 仓库
 */
@Controller
@RequestMapping("/sale/warehouse")
@ResponseBody
public class WareHouseController {

    @Autowired
    private WareHouseService wareHouseService;
    @Autowired
    private WareHouseOutIn wareHouseOutIn;
    @Autowired
    private WarehouseSend warehouseSend;

    /**
     * 加载产品在库记录
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     */
    @RequestMapping("/product")
    public List<Map<String,Object>> loadWareHouse(Long id) throws InputException, NotFoundException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("产品信息错误");
        return wareHouseService.load(id);
    }

    /**
     * 加载所有仓库
     * @return
     */
    @RequestMapping("/wh")
    public Code loadAllWareHouse(){
        return Code.success(wareHouseService.loadAll());
    }
    /**
     * 加载所有数据
     * @param orderSearch
     * @return
     */
    @RequestMapping("/out/all")
    public PageEntity<OutStorageVo> findAll(OutStorageSearch orderSearch){
        return wareHouseService.findOutAll(orderSearch);
    }
    /**
     * 加载所有数据
     * @param orderSearch
     * @return
     */
    @RequestMapping("/in/all")
    public PageEntity<InStorageVo> findAll(InStorageSearch orderSearch){
        return wareHouseService.findINAll(orderSearch);
    }

    /**
     * 保存出库单
     * @param whPullEntity
     * @return
     */
    @RequestMapping("/out/save")
    @SystemLog(module = "仓库",methods = "添加/修改出库单")
    public Code saveOut(WhPullEntity whPullEntity) throws InputException, NotFoundException, ServiceException {
        CheckForm.checkWareHouseOut(whPullEntity);
        if (whPullEntity.getWpullId() != null && whPullEntity.getWpullId().toString().length() >0){
            wareHouseService.modifyOutbound(whPullEntity);
            return Code.success("修改出库单成功");
        }else {
            wareHouseService.addOut(whPullEntity);
            return Code.success("添加出库单成功");
        }
    }

    /**
     * 保存入库单
     * @param whPutEntity
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/in/save")
    @SystemLog(module = "仓库",methods = "添加/修改入库单")
    public Code InOut(WhPutEntity whPutEntity) throws InputException, NotFoundException, ServiceException {
        CheckForm.checkWareHouseIn(whPutEntity);
        if (whPutEntity.getWpId() != null && whPutEntity.getWpId().toString().length() >0){
            wareHouseService.modifyInbound(whPutEntity);
            return Code.success("修改入库单成功");
        }else {
            wareHouseService.addIn(whPutEntity);
            return Code.success("添加入库单成功");
        }
    }

    /**
     * 加载出库单
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     */
    @RequestMapping("/out/load")
    public Code loadOut(Long id) throws InputException, NotFoundException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("出库单信息错误");
        return Code.success(wareHouseService.loadOneOut(id.intValue()));
    }

    /**
     * 加载入库单
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     */
    @RequestMapping("/in/load")
    public Code loadIn(Long id) throws InputException, NotFoundException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("入库单信息错误");
        return Code.success(wareHouseService.loadOneIn(id.intValue()));
    }
    /**
     * 加载入库单
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     */
    @RequestMapping("/send/load")
    public Code loadSend(Long id) throws InputException, NotFoundException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("入库单信息错误");
        return Code.success(warehouseSend.loadOneSend(id));
    }

    /**
     * 删除出库单
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/out/delete")
    @SystemLog(module = "仓库",methods = "删除出库单")
    public Code deleteOut(Long id) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("出库单信息错误");
        wareHouseService.deleteOut(id.intValue());
        return Code.success();
    }

    /**
     * 删除入库单
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/in/delete")
    @SystemLog(module = "仓库",methods = "删除入库单")
    public Code deleteIn(Long id) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("入库单信息错误");
        wareHouseService.deleteIn(id.intValue());
        return Code.success();
    }

    /**
     * 批量删除入库单
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/in/delete/all")
    @SystemLog(module = "仓库",methods = "批量删除入库单")
    public Code deleteOutAll(@RequestParam("ids[]") Integer[] ids) throws InputException, NotFoundException, ServiceException {
        if (ids.length<=0)
            throw new InputException("入库单信息错误");
        wareHouseService.deleteInAll(ids);
        return Code.success();
    }

    /**
     * 加载所有库存流水
     * @param search
     * @return
     */
    @PostMapping("/detail/all")
    public PageEntity<WpDetailVo> loadAllDetail(WpDeatilSearch search){
        return wareHouseService.findDetailAll(search);
    }
    /**
     * 加载所有发货单
     * @param search
     * @return
     */
    @RequestMapping("/send/all")
    public PageEntity<SendGoodsVo> loadAllDetail(SendGoodsSearch search){
        return warehouseSend.findSend(search);
    }

    /**
     * 加载所有发货明细
     * @param search
     * @return
     */
    @RequestMapping("/send/detail/all")
    public PageEntity<DeliverDetailEntity> loadAllDetail(SendGoodsDetail search){
        return warehouseSend.findSend(search);
    }
    /**
     * 出库单详情
     * @param id
     * @return
     */
    @RequestMapping("/out/info/{id}/")
    public ModelAndView outInfo(@PathVariable("id") Long id){
        return getModelAndView(id,"sale/outStorageInfo",0);
    }
    /**
     * 入库单详情
     * @param id
     * @return
     */
    @RequestMapping("/in/info/{id}/")
    public ModelAndView inInfo(@PathVariable("id") Long id){
        return getModelAndView(id,"sale/inStorageInfo",1);
    }

    /**
     * 发库单详情
     * @param id
     * @return
     */
    @RequestMapping("/send/info/{id}/")
    public ModelAndView sendInfo(@PathVariable("id") Long id){
        return getModelAndView(id,"sale/snedGoodsInfo",2);
    }


        private ModelAndView getModelAndView(@PathVariable("id") Long id, String s,int type) {
        ModelAndView modelAndView = new ModelAndView();
        if (id == null)
            return  new ModelAndView("404");
        try {
            Object order = null;
            if (type == 0)
             order = wareHouseService.loadOneOut(id.intValue());
            else if (type == 1)
                order = wareHouseService.loadOneIn(id.intValue());
            else if (type == 2)
                order = warehouseSend.loadOneSend(id);
            if (order == null)
                return new ModelAndView("404");
            modelAndView.addObject("info",order);
            modelAndView.setViewName(s);
        } catch (Exception e) {
            e.printStackTrace();
            return new ModelAndView("404");
        }
        return modelAndView;
    }


    /**
     * 加载出库明细
     * @param id
     * @return
     * @throws NotFoundException
     * @throws InputException
     */
    @RequestMapping("/out/detail")
    public Code loadOutDetail(Long id) throws NotFoundException, InputException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("出库单信息错误");
        return Code.success(wareHouseOutIn.loadOutDetail(id));
    }

    /**
     * 加载入库明细
     * @param id
     * @return
     * @throws NotFoundException
     * @throws InputException
     */
    @RequestMapping("/in/detail")
    public Code loadInDetail(Long id) throws NotFoundException, InputException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("出库单信息错误");
        return Code.success(wareHouseOutIn.loadInDetail(id));
    }

    /**
     * 加载发货明细
     * @param id
     * @return
     * @throws NotFoundException
     * @throws InputException
     */
    @RequestMapping("/send/detail")
    public Code loadSendDetail(Long id) throws NotFoundException, InputException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("发货单信息错误");
        return Code.success(warehouseSend.loadDetailSend(id));
    }

    @RequestMapping("/detail/view")
    public ModelAndView write(Long id,Boolean type){
        ModelAndView modelAndView = new ModelAndView();
        if (id == null)
            return  new ModelAndView("404");
        try {
            OutStorageVo out = null;
            InStorageVo in = null;
            if (!type) {
                 out = wareHouseService.loadOneOut(id.intValue());
            }else
                in = wareHouseService.loadOneIn(id.intValue());
            if (out == null && in == null)
                return new ModelAndView("404");
            modelAndView.addObject("info",type?in:out);
            modelAndView.addObject("type",type);
            modelAndView.setViewName("sale/WarehouseDeatil");
        } catch (Exception e) {
            e.printStackTrace();
            return new ModelAndView("404");
        }
        return modelAndView;
    }

    /**
     * 修改库存明细
     * @param vos
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("/detail/save")
    @SystemLog(module = "仓库",methods = "修改流水数据")
    public Code modifyDetail(WarehouseDetailVo vos) throws InputException, NotFoundException, ServiceException {
        if (vos.getId() == null || vos.getType() == null)
            throw new InputException("明细数据错误");
        CheckForm.checkWareHouseDetail(vos);
        wareHouseOutIn.modifyDetail(vos);
        return Code.success();
    }

    /**
     * 确认入库或者出库
     * @param vos
     * @return
     */
    @PostMapping("/detail/enter")
    @SystemLog(module = "仓库",methods = "确认入库/出库")
    public Code enter(WarehouseDetailVo vos,HttpSession session) throws InputException, NotFoundException, ServiceException {
        if (vos.getId() == null || vos.getType() == null)
            throw new InputException("明细数据错误");
        CheckForm.checkWareHouseDetail(vos);
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        wareHouseOutIn.modifyDetail(vos);
        wareHouseOutIn.enter(vos,userEntity);
        return Code.success();
    }

    /**
     * 撤销出库入库
     * @param id
     * @param type
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("/cancel/{type}/")
    @SystemLog(module = "仓库",methods = "撤销入库/出库")
    public Code cancel(Long id, @PathVariable("type") String type) throws InputException, NotFoundException, ServiceException {
        if (id == null || id == null)
            throw new InputException("明细数据错误");
        if ("out".equals(type))
            wareHouseOutIn.cancel(id,0);
        else if ("in".equals(type))
            wareHouseOutIn.cancel(id,1);
        return Code.success();
    }

    /**
     * 修改发货单
     * @param goodsEntity
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("/send/save")
    @SystemLog(module = "仓库",methods = "修改发货单")
    public Code sendSave(DeliverGoodsEntity goodsEntity) throws NotFoundException, ServiceException {
        CheckForm.checkDeliver(goodsEntity);
        warehouseSend.modifyDeliver(goodsEntity);
        return Code.success("修改发货单成功");
    }

    /**
     * 发货
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("/send/ok")
    @SystemLog(module = "仓库",methods = "发货")
    public Code sendGoods(Integer id, HttpSession session) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("发货单信息错误");
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        warehouseSend.sendGoods(id,userEntity);
        return Code.success();
    }
    /**
     * 撤销发货
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("/send/cancel")
    @SystemLog(module = "仓库",methods = "撤销发货")
    public Code cancelSendGoods(Integer id) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("发货单信息错误");
        warehouseSend.cancelSend(id);
        return Code.success();
    }
    /**
     * 发货
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("/out/send/ok")
    @SystemLog(module = "仓库",methods = "发货")
    public Code sendGoodsByPull(Integer id,HttpSession session) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("发货单信息错误");
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        warehouseSend.sendGoodsByPull(id,userEntity);
        return Code.success();
    }
    /**
     * 撤销发货
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("/out/send/cancel")
    @SystemLog(module = "仓库",methods = "撤销发货")
    public Code cancelSendGoodsByPull(Integer id) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("发货单信息错误");
        warehouseSend.cancelSendGoodsByPull(id);
        return Code.success();
    }

    @RequestMapping("/order")
    public ModelAndView chooseWarehouse(Long id,Model model) throws NotFoundException{
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("sale/chooseOut");
        if (id == null || id.toString().length() <= 0)
            modelAndView.setViewName("404");
        if (!wareHouseOutIn.haveNoPull(id)) {
            modelAndView.setViewName("sale/havepull");
        }
        if (!wareHouseOutIn.canOut(id))
            modelAndView.setViewName("404");

        model.addAttribute("id",id);
        return modelAndView;
    }

    /**
     * 订单生成出库单
     * @param id
     * @return
     * @throws InputException
     */
    @RequestMapping("/order/out")
    public Code orderOut(Long id, Long warehouseName) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("订单信息错误");
        if (warehouseName == null || warehouseName.toString().length() <=0)
            throw new InputException("选择仓库错误");
        WhPullEntity whPullEntity = wareHouseOutIn.orderOut(id,warehouseName);
        return Code.success(whPullEntity.getWpullId());
    }
    /**
     * 采购单生成入库单
     * @param id
     * @return
     * @throws InputException
     */
    @RequestMapping("/purse/in")

    public Code purseIn(Long id) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("采购单信息错误");
        WhPutEntity whPutEntity = wareHouseOutIn.purchaseIn(id);
        return Code.success(whPutEntity.getWpId());
    }
    /**
     * 采购单退货单生成出库单
     * @param id
     * @return
     * @throws InputException
     */
    @RequestMapping("/return/out")
    public Code returnOut(Integer id) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("采购单退货单信息错误");
        WhPullEntity whPullEntity = wareHouseOutIn.returnOut(id);
        return Code.success(whPullEntity.getWpullId());
    }
    /**
     * 订单退货生成入库单
     * @param id
     * @return
     * @throws InputException
     */
    @RequestMapping("/return/in")
    public Code returnIn(Integer id) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("订单退货单信息错误");
        WhPutEntity whPutEntity = wareHouseOutIn.returnIn(id);
        return Code.success(whPutEntity.getWpId());
    }

    /**
     * 订单将生成出库单的信息
     * @param id
     * @return
     * @throws InputException
     */
    @RequestMapping("/order/nopay")
    public List<ChooseWarehouseVo> chooseWarehouse(Long id) throws InputException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("订单信息错误");
        return wareHouseOutIn.chooseWarehouse(id);
    }
}
