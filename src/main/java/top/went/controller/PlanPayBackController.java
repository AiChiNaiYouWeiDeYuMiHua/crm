package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.OrderEntity;
import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PlanPayBackEntity;
import top.went.service.OrderService;
import top.went.service.PlanPayBackService;
import top.went.vo.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/sale")
@ResponseBody
public class PlanPayBackController {
    @Autowired
    private PlanPayBackService planPayBackService;
    @Autowired
    private OrderService orderService;

    @GetMapping("/mainPlanPayBackDetail")
    public ModelAndView mainPlanPayBackDetail() {
        return new ModelAndView("sale/MainPlanPayBack");
    }

    @GetMapping("/ppbQueryModal")
    public ModelAndView modal() {
        return new ModelAndView("sale/modal/ppb_super_query_model");
    }

    @GetMapping("/ppbaddmodal")
    public ModelAndView addModal(Integer orderId) throws NotFoundException {
        ModelAndView mv = new ModelAndView();
        OrderEntity orderEntity = new OrderEntity();
        if (orderId != null) {
            orderEntity = orderService.load(Long.valueOf(orderId));
            mv.addObject("orderEntity", orderEntity);
        }
        mv.setViewName("sale/modal/add_plan_pay_back_model");
        return mv;
    }

    @GetMapping("/ppbdetail")
    public ModelAndView planPayDetail() {
        return new ModelAndView("sale/planPayBackDetail");
    }
    /**
     * 根据订单id查找所有计划回款
     *
     * @param orderId
     * @param
     * @return
     * @throws ServiceException
     */
    @PostMapping("/ppb_findAllByOrderIdDate")
    @ResponseBody
    public List<PlanPayBackEntity> findAllByOrderId(Integer orderId) throws ServiceException {
        PlanPayBackVO planPayBackVO = new PlanPayBackVO();
        planPayBackVO.setOrderId(orderId);
        return planPayBackService.findAllByManyConditions(planPayBackVO).getRows();
    }
    /**
     * 添加计划回款
     *
     * @param planPayBackEntity
     * @return
     * @throws ServiceException
     */
    @PostMapping("/ppbinsert")
    @ResponseBody
    public Code ppb_insert(PlanPayBackEntity planPayBackEntity) throws ServiceException {
        planPayBackEntity.setMagicDelete(0l);
        if (planPayBackService.ppb_insert(planPayBackEntity))
            return Code.success("操作成功");
        return Code.fail();
    }

    /**
     * 删除
     *
     * @param ppb_id
     * @return
     * @throws ServiceException
     */
    @PostMapping("/ppbdelete")
    @ResponseBody
    public Code ppb_delete(int ppb_id) throws ServiceException, NotFoundException {
        planPayBackService.ppb_delete(ppb_id);
        return Code.success("删除计划回款成功");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/ppbdeleteall")
    @ResponseBody
    public Code deleteAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for (int i = 0; i < ids.length; i++)
            if (ids[i] == null || ids[i].toString().length() <= 0)
                throw new InputException("计划回款id错误");
        planPayBackService.ppb_logicDeleteAll(ids);
        return Code.success("计划回款批量删除成功");
    }

    /**
     * 查找所有计划回款
     *
     * @param page
     * @return
     * @throws ServiceException
     */
    @PostMapping("/ppbfindAll")
    @ResponseBody
    public PageEntity<PlanPayBackVO> ppb_findAll(@RequestBody top.went.vo.Page page) throws ServiceException {
        return planPayBackService.ppb_findAll(page.getSize(), page.getPage());
    }

    /**
     * 根据日期查找所有计划回款
     *
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    @PostMapping("/ppb_findAllByDate")
    @ResponseBody
    public PageEntity<PlanPayBackVO> findAllByDate(Integer pageSize, Integer pageNumber, String ppb_date) throws ServiceException {
        System.out.println("pageSize" + pageSize + " " + "pageNumber" + pageNumber);
        if (null == pageSize || null == pageNumber) {
            pageSize = 10;
            pageNumber = 1;
        }
        return planPayBackService.ppb_findAllByDate(ppb_date, pageSize, pageNumber);
    }



    /**
     * 加载
     *
     * @param ppbId
     * @return
     */
    @GetMapping("ppbload")
    @ResponseBody
    public Code load(Integer ppbId) {
        PlanPayBackVO planPayBackVO = null;
        try {
            planPayBackVO = planPayBackService.load1(ppbId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return Code.success(planPayBackVO);
    }

    /**
     * 加载所有
     *
     * @return
     */
    @RequestMapping("/ppball")
    public PageEntity<PlanPayBackEntity> loadAll(@RequestBody PlanPayBackVO planPayBackVO) throws ServiceException {
        System.out.println(planPayBackVO);
        return planPayBackService.findAllByManyConditions(planPayBackVO);
    }

    /**
     * 查看详情
     *
     * @param ppbId
     * @return
     */
    @RequestMapping("/ppbdetail1/{ppbId}/")
    public ModelAndView detail(@PathVariable("ppbId") int ppbId) {
        if (new Integer(ppbId) == null) {
            return new ModelAndView("404");
        }
        ModelAndView mv = new ModelAndView("sale/planPayBackDetail");
        PlanPayBackEntity planPayBackEntity = null;
        try {
            planPayBackEntity = planPayBackService.load(ppbId);
            if (planPayBackEntity == null)
                return new ModelAndView("404");
        } catch (ServiceException e) {
            e.printStackTrace();
            return new ModelAndView("404");
        }
        mv.addObject("info", planPayBackEntity);
        return mv;
    }
}