package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.OrderEntity;
import top.went.pojo.PurchaseEntity;
import top.went.pojo.PurchaseReceiptEntity;
import top.went.pojo.ReceiptRecordsEntity;
import top.went.service.OrderService;
import top.went.service.ReceiptRecordsService;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.PurchaseReceiptVO;
import top.went.vo.ReceiptRecordsVO;

import java.util.List;

@Controller
@RequestMapping("/sale")
@ResponseBody
public class ReceiptRecordsCotroller {
@Autowired
    private ReceiptRecordsService receiptRecordsService;
    @Autowired
    private OrderService orderService;
    @GetMapping("/mainReceiptRecordsDetail")
    public ModelAndView mainPurchaseReceiptDetail(){
        return new ModelAndView("sale/MainReceiptRecords");
    }

    @GetMapping("/rrAddModal")
    public ModelAndView modal(Integer orderId) throws NotFoundException {
        ModelAndView mv = new ModelAndView();
        OrderEntity orderEntity = new OrderEntity();
        if(orderId!=null){
            orderEntity=orderService.load(Long.valueOf(orderId));
            mv.addObject("orderEntity", orderEntity);
        }
        mv.setViewName("sale/modal/add_receipt_records_model");
        return mv;
    }
    @GetMapping("/rrQueryModal")
    public ModelAndView rrQueryModal(){
        return new ModelAndView("sale/modal/rr_super_query_model");
    }

    @GetMapping("/rrDetail")
    public ModelAndView  purchaseReceiptDetail(){
        return new ModelAndView("sale/receiptRecordsDetail");
    }

    /**
     * 根据订单id查询所有开票
     * @param
     * @param
     * @return
     * @throws ServiceException
     */
    @PostMapping("/rr_findAllByOrderId")
    @ResponseBody
    public List<ReceiptRecordsEntity> findAllByOrderId(Integer orderId) throws ServiceException {
       ReceiptRecordsVO receiptRecordsVO =new ReceiptRecordsVO();
       receiptRecordsVO.setOrderId(orderId);
        return receiptRecordsService.findAllByManyConditions(receiptRecordsVO).getRows();
    }
    /**
     * 添加开票
     * @param  receiptRecordsEntity
     * @throws ServiceException
     */
    @PostMapping("/rr_insert")
    @ResponseBody
    public Code rr_insert(ReceiptRecordsEntity receiptRecordsEntity) throws ServiceException {
        receiptRecordsEntity.setRrDelete(0l);
        if(receiptRecordsService.rr_insert(receiptRecordsEntity))
            return Code.success("添加开票");
        return Code.fail();
    }

    /**
     * 删除开票
     * @param rrId
     * @throws ServiceException
     */
    @PostMapping("/rrdelete")
    @ResponseBody
    public Code rr_delete(Integer rrId) throws ServiceException {
        try {
            if(receiptRecordsService.rr_logicDelete(rrId))
                return Code.success("删除开票成功");
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return Code.fail();
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/rrdeleteall")
    @ResponseBody
    public Code  deleteAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for(int i=0;i<ids.length;i++)
            if(ids[i]==null||ids[i].toString().length()<=0)
                throw new InputException("id错误");
        receiptRecordsService.rr_logicDeleteAll(ids);
        return Code.success("批量删除成功");
    }

    /**
     * 查询所有开票
     * @return
     * @throws ServiceException
     */
    @PostMapping("/rrfindAll")
    @ResponseBody
    public PageEntity<ReceiptRecordsVO> findAll(@RequestBody top.went.vo.Page page) throws ServiceException {
        return receiptRecordsService.rr_findAll(page.getSize(),page.getPage());
    }
    /**
     * 根据日期查询所有开票
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    @PostMapping("/rr_findAllByDate")
    @ResponseBody
    public PageEntity<ReceiptRecordsVO> findAllByDate(Integer pageSize,Integer pageNumber,String rr_date) throws ServiceException {
        System.out.println("pageSize"+pageSize+" "+"pageNumber"+pageNumber);
        if (null == pageSize || null == pageNumber){
            pageSize = 10;
            pageNumber = 1;
        }
        return receiptRecordsService.rr_findByPpdDate(pageSize, pageNumber, rr_date.toString());
    }


    /**
     * 加载
     * @param rrId
     * @return
     */
    @GetMapping("rrload")
    @ResponseBody
    public Code load(Integer rrId){
        ReceiptRecordsVO receiptRecordsVO=null;
        try {
            receiptRecordsVO =  receiptRecordsService.load1(rrId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return Code.success(receiptRecordsVO);
    }
    /**
     * 加载所有
     * @return
     */
    @RequestMapping("/rrall")
    @ResponseBody
    public PageEntity<ReceiptRecordsEntity> loadAll(@RequestBody ReceiptRecordsVO receiptRecordsVO) {
        System.out.println(receiptRecordsVO);
        return receiptRecordsService.findAllByManyConditions(receiptRecordsVO);
    }

    /**
     * 查看详情
     * @param rrId
     * @return
     */
    @RequestMapping("/rrdetail1/{rrId}/")
    public ModelAndView detail(@PathVariable("rrId")int rrId){
        if (new Integer(rrId)== null) {
            return  new ModelAndView("404");
        }
        ModelAndView mv=new ModelAndView("sale/receiptRecordsDetail");
        ReceiptRecordsEntity receiptRecordsEntity=null;
        try {
            receiptRecordsEntity = receiptRecordsService.load(rrId);
            if(receiptRecordsEntity==null)
                return  new ModelAndView("404");
        } catch (ServiceException e) {
            e.printStackTrace();
            return  new ModelAndView("404");
        }
        mv.addObject("info",receiptRecordsEntity);
        return mv;
    }
}
