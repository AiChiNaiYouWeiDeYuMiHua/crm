package top.went.controller.ykm;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class JspControllerYkm {

    @GetMapping("to_modal")
    public String toModal() {
        return "modal";
    }

    @GetMapping("to_ykm_modal")
    public String toYkmModal(){
        return "ykm/modal";
    }


    //==============销售机会==================

    @GetMapping("/to_ykm_sale_opp_index")
    public String saleoppIndex(){
        return "ykm/sale_opp_index";
    }

    @GetMapping("to_productInfo")
    public String toProductInfo(){
        return "productInfo";
    }

    @GetMapping("sale_opp_add")
    public String toSaleOppAddModal(){
        return "/ykm/modal/sale_opp_add_modal";
    }

    @GetMapping("to_sale_opp_senior")
    public String toSaleOppSeniorModal(){
        return "/ykm/modal/sale_opp_senior_modal";
    }

    @GetMapping("/sale_opp_info")
    public String toSaleOppInfo(){
        return "ykm/saleOppInfo";
    }

    //=================详细需求=================

    @GetMapping("to_ykm_demand_index")
    public String demandIndex(){
        return "ykm/demand_index";
    }

    @GetMapping("demand_add")
    public String toDemandAddModal(){
        return "/ykm/modal/demand_add_modal";
    }

    @GetMapping("to_demand_senior")
    public String toDemandSeniorModal(){
        return "/ykm/modal/demand_senior_modal";
    }


    //===================解决方案===================
    @GetMapping("to_ykm_solution_index")
    public String solutionIndex(){
        return "/ykm/solution_index";
    }

    @GetMapping("solution_add")
    public String toSolutionAddModal(){
        return "/ykm/modal/solution_add_modal";
    }

    @GetMapping("to_solution_senior")
    public String toSolutionSeniorModal(){
        return "/ykm/modal/solution_senior_modal";
    }

    //==================竞争对手======================
    @GetMapping("to_ykm_competitor_index")
    public String competitorIndex(){
        return "/ykm/competitor_index";
    }
    @GetMapping("competitor_add")
    public String toCompetitorAddModal(){
        return "/ykm/modal/competitor_add_modal";
    }
    @GetMapping("to_competitor_senior")
    public String toCompetitorSeniorModal(){
        return "/ykm/modal/competitor_senior_modal";
    }

    //===================报价==========================
    @GetMapping("to_ykm_quote_index")
    public String quoteIndex(){
        return "/ykm/quote_index";
    }
    @GetMapping("quote_add")
    public String toQuoteAddModal(){
        return "/ykm/modal/quote_add_modal";
    }
    @GetMapping("to_quote_senior")
    public String toQuoteSeniorModal(){
        return "/ykm/modal/quote_senior_modal";
    }
    @GetMapping("quote_info")
    public String toQuoteInfo(){
        return "ykm/quoteInfo";
    }

    /**
     * 报价明细table
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/quoteDetail")
    public String quoteDetail(Long id, Model model){
        model.addAttribute("id",id);
        return "ykm/table/quoteDetailTable";
    }

    //====================报价明细====================
    @GetMapping("to_ykm_quote_detail_index")
    public String quoteDetailIndex(){ return "/ykm/quote_detail_index"; }
    @GetMapping("to_quote_detail_senior")
    public String toQuoteDetailSeniorModal(){ return "/ykm/modal/quote_detail_senior_modal"; }



    @PostMapping("testTest")
    public String test(Date hiredate){
        System.out.println(hiredate);
        return "ykm/modal/sale_opp_senior_modal";
    }

    @GetMapping("to_test")
    public String t(){
        return "ykm/test";
    }

}
