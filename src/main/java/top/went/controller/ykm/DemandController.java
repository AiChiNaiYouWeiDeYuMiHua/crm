package top.went.controller.ykm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.DemandEntity;
import top.went.service.DemandService;
import top.went.vo.Code;
import top.went.vo.DemandSonVo;
import top.went.vo.DemandVo;
import top.went.vo.PageEntity;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/demand/")
@ResponseBody
public class DemandController {
    @Autowired
    private DemandService demandService;


    /**
     * 高级查询
     * @param demandSonVo
     * @return
     * @throws InputException
     */
    @RequestMapping("senior")
    public PageEntity<DemandEntity> findAllSeniorSearch(@RequestParam(value = "importanceList[]", required = false) Long[] importanceList, DemandSonVo demandSonVo) throws InputException {
        if(demandSonVo == null)
            System.out.println("你接受了啥？");
        if(demandSonVo.getPage() == null)
            throw new InputException("参数有误");
        DemandVo demandVo = new DemandVo(demandSonVo);
        if(importanceList != null){
            demandVo.setImportanceList(importanceList);
        }
        return demandService.findAllSeniorSearch(demandVo,demandVo.getPage().getPage(),demandVo.getPage().getSize());
    }

    /**
     * 加载需求
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     */
    @GetMapping("demand_load")
    @ResponseBody
    public Code load(Long id) throws InputException, NotFoundException {
        if(id == null){
            throw new InputException("输入错误");
        }
        return Code.success(demandService.load(id));
    }

    /**
     * 添加/修改详细需求
     * @param demandEntity
     * @return
     * @throws ServiceException
     * @throws NotFoundException
     */
    @PostMapping("demand_addMethod")
    @ResponseBody
    @SystemLog(module = "详细需求", methods = "新增/修改")
    public Code addAndModify(DemandEntity demandEntity) throws ServiceException, NotFoundException {
        System.out.println(demandEntity);
        if (demandEntity == null) {
            System.out.println("呵呵：我为空？");
            return Code.fail("什么鬼,没东西呀！");
        }
        if(demandEntity.getDemandId() != null){
            demandService.modifyDemand(demandEntity);
            return Code.success("修改成功！");
        } else {
            demandEntity.setIsDelete(0L);
            if(demandEntity.getImportance() == null){
                demandEntity.setImportance(0L);
            }
            if(demandEntity.getRecordTime() == null){
                demandEntity.setRecordTime(new Date(new java.util.Date().getTime()));
            }
            demandService.addDemand(demandEntity);
            return Code.success("新增成功！");
        }
    }

    /**
     * 快速查询需求（重要程度、需求主题）
     * @param pageNumber
     * @param pageSize
     * @param importance
     * @param demandTheme
     * @return
     * @throws InputException
     */
    @ResponseBody
    @GetMapping("demand_findAll")
    public Map<String, Object> findAllDemands(Integer pageNumber, Integer pageSize, String importance, String demandTheme) throws InputException {
        System.out.println(pageNumber+pageSize+importance+demandTheme);
        if (null == pageSize || null == pageNumber){
            pageSize = 10;
            pageNumber = 1;
        }
        Long imp = null;
        try {
            imp = Long.parseLong(importance);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new InputException("数据输入异常");
        }
        DemandEntity demandEntity = new DemandEntity();
        demandEntity.setImportance(imp);
        demandEntity.setDemandTheme(demandTheme);
        Page<DemandEntity> page = demandService.loadListByPageAndParams(pageNumber, pageSize, demandEntity);
        int total = page.getTotalPages();
        Map<String, Object> map = new HashMap<>();
        System.out.println("total:"+total);
        map.put("total",total);
        map.put("rows",page.getContent());
        return map;
    }

    /**
     * 删除
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("delete")
    @SystemLog(module = "详细需求", methods = "删除")
    public Code deleteDemand(Long id) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("id错误!");
        demandService.deleteDemand(id);
        return Code.success("删除成功！");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @GetMapping("deleteSelected")
    @SystemLog(module = "详细需求", methods = "批量删除")
    public Code deleteSelected(@RequestParam(value = "ids[]")Long[] ids) throws InputException, NotFoundException, ServiceException {
        for(int i=0; i<ids.length; i++)
            if (ids[i] == null || ids[i].toString().length()<=0)
                throw new InputException("id错误");
        demandService.deleteSelected(ids);
        return Code.success("删除成功");
    }
}
