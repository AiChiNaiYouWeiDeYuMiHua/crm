package top.went.controller.ykm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.CompetitorEntity;
import top.went.service.CompetitorService;
import top.went.vo.Code;
import top.went.vo.CompetitorSonVo;
import top.went.vo.CompetitorVo;
import top.went.vo.PageEntity;

@Controller
@RequestMapping("/competitor")
@ResponseBody
public class CompetitorController {
    @Autowired
    private CompetitorService competitorService;

    /**
     * 加载竞争对手
     * @param id
     * @return
     * @throws NotFoundException
     */
    @GetMapping("load")
    public Code load(Long id) throws NotFoundException {
        if(id == null){
            throw new NotFoundException("输入错误！");
        }
        return Code.success(competitorService.load(id));
    }

    /**
     * 高级查询
     * @param competitiveAbilityArray
     * @param competitorSonVo
     * @return
     * @throws InputException
     */
    @RequestMapping("senior")
    public PageEntity<CompetitorEntity> findAllSeniorSearch(@RequestParam(value = "competitiveAbilityArray[]", required = false) Long[] competitiveAbilityArray, CompetitorSonVo competitorSonVo) throws InputException {
        if(competitiveAbilityArray != null){
            System.out.println(competitiveAbilityArray.length);
        }
        if(competitorSonVo.getPage() == null){
            throw new InputException("输入错误！");
        }
        CompetitorVo competitorVo = new CompetitorVo();
        competitorVo.setCompetitorEntity(competitorSonVo.getCompetitorEntity());
        competitorVo.setPage(competitorSonVo.getPage());
        competitorVo.setSearchText(competitorSonVo.getSearchText());
        competitorVo.setFamilyDa(competitorSonVo.getFamilyDa());
        competitorVo.setKidDa(competitorSonVo.getKidDa());
        if(competitiveAbilityArray != null){
            competitorVo.setCompetitiveAbilityArray(competitiveAbilityArray);
        }
        return competitorService.findAllSeniorSearch(competitorVo);
    }


    /**
     * 添加/修改竞争对手
     * @param competitorEntity
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("add_modify")
    @SystemLog(module = "竞争对手", methods = "新增/修改")
    public Code addAndModify(CompetitorEntity competitorEntity) throws NotFoundException, ServiceException {
        System.out.println(competitorEntity);
        if(competitorEntity == null){
            return Code.fail("输入错误！");
        }
        if(competitorEntity.getCompetitorId() != null){
            competitorService.modify(competitorEntity);
            return Code.success("修改成功！");
        }
        else {
            competitorEntity.setIsDelete(0L);
            if(competitorEntity.getCompetitiveAbility() == null){
                competitorEntity.setCompetitiveAbility(0L);
            }
            if(competitorEntity.getPrice() == null){
                competitorEntity.setPrice(0L);
            }
            competitorService.add(competitorEntity);
            return Code.success("新增成功！");
        }
    }

    /**
     * 删除竞争对手
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @GetMapping("delete")
    @SystemLog(module = "竞争对手", methods = "删除")
    public Code delete(Long id) throws InputException, NotFoundException, ServiceException {
        if(id == null){
            throw new InputException("输入id错误！");
        }
        competitorService.delete(id);
        return Code.success("删除成功！");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @GetMapping("deleteSelected")
    @SystemLog(module = "竞争对手", methods = "批量删除")
    public Code deleteSelected(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for(int i=0; i< ids.length; i++){
            if (ids[i] == null || ids[i].toString().length()<=0)
                throw new InputException("id错误！");
        }
        competitorService.deleteSelected(ids);
        return Code.success("删除成功！");
    }
}
