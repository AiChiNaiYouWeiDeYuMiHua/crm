package top.went.controller.ykm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.SaleOppEntity;
import top.went.service.SaleOppService;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.SaleOppSonVo;
import top.went.vo.SaleOppVo;

import javax.servlet.http.HttpServletRequest;
import javax.sound.midi.Soundbank;
import java.util.*;

@Controller
public class SaleOppController {
    @Autowired
    private SaleOppService saleOppService;

    /**
     * 加载机会
     * @param oppId
     * @return
     * @throws InputException
     * @throws NotFoundException
     */
    @GetMapping("sale_opp_load")
    @ResponseBody
    public Code load(Long oppId) throws InputException, NotFoundException {
        if(oppId == null)
            throw new InputException("输入错误");
//        System.out.println("我要看对应需求："+saleOppService.load(oppId).getTbDemandsByOppId().size());
        return Code.success(saleOppService.load(oppId));
    }

    /**
     * 添加/编辑机会
     * @param saleOppEntity
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("sale_opp_addmethod")
    @ResponseBody
    @SystemLog(module = "销售机会", methods = "新增/修改")
    public Code addSaleOpp(SaleOppEntity saleOppEntity, HttpServletRequest request) throws NotFoundException, ServiceException {
//        System.out.println("session"+request.getSession().getAttribute("user"));
        if(saleOppEntity == null){
            return Code.fail("什么鬼,没东西呀！");
        }
        if(saleOppEntity.getOppId() != null){
            saleOppService.modifySaleOpp(saleOppEntity);
            return Code.success("修改成功！");
        }
        else {
            saleOppEntity.setUpdateTime(new java.sql.Date(new Date().getTime()));
            saleOppEntity.setIsDelete(0L);
            saleOppEntity.setCreateTime(new java.sql.Date(new Date().getTime()));
            saleOppEntity.setStageStartTime(new java.sql.Date(new Date().getTime()));
            saleOppService.addSaleOpp(saleOppEntity);
            return Code.success("新增成功！");
        }
    }

    /**
     * 删除
     * @param oppId
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("sale_opp_delete")
    @ResponseBody
    @SystemLog(module = "销售机会", methods = "删除")
    public Code deleteSaleOpp (Long oppId) throws InputException, NotFoundException, ServiceException {
        if (oppId == null || oppId.toString().length() <=0)
            throw new InputException("机会id错误!");
        saleOppService.deleteSaleOpp(oppId);
        return Code.success("删除成功！");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @GetMapping("sale_opp_deleteSelected")
    @ResponseBody
    @SystemLog(module = "销售机会", methods = "批量删除")
    public Code deleteAllSaleOpp(@RequestParam(value = "ids[]")Long[] ids) throws InputException, NotFoundException, ServiceException {
        for(int i=0; i<ids.length; i++)
            if (ids[i] == null || ids[i].toString().length()<=0)
                throw new InputException("销售机会id错误");
        saleOppService.deleteSelectedSaleOpp(ids);
        return Code.success("机会删除成功");
    }

    @GetMapping("sale_opp_fast")
    @ResponseBody
    public PageEntity<SaleOppEntity> loadListWhere(Integer pageNumber, Integer pageSize){
        return saleOppService.loadListWhere(pageNumber, pageSize, "","","","","",0L,9999L);
    }

    /**
     * 高级查询
     * @param saleOppSonVo
     * @return
     */
    @GetMapping("sale_opp_senior")
    @ResponseBody
    public PageEntity<SaleOppEntity> findAllSeniorSearch(@RequestParam(value = "oppSourceList[]", required = false) String [] oppSourceList,
                                                         @RequestParam(value = "stageList[]", required = false) String[] stageList,
                                                         @RequestParam(value = "oppStatusList[]", required = false) Long[] oppStatusList,
                                                         @RequestParam(value = "priorityList[]", required = false) Long[] priorityList,
                                                         @RequestParam(value = "classificationList[]", required = false) String[] classificationList,
                                                         @RequestParam(value = "intendProductList[]", required = false) String[] intendProductList,
                                                         SaleOppSonVo saleOppSonVo) throws InputException {

        if(saleOppSonVo.getPage() == null)
            throw new InputException("参数有误");
        SaleOppVo saleOppVo = new SaleOppVo(saleOppSonVo);
        if(oppSourceList != null){
            if(Arrays.asList(oppSourceList).contains("0")){
                saleOppVo.setIndex(0L);
            }
            saleOppVo.setOppSourceList(oppSourceList);
        }
        if(stageList != null){
            saleOppVo.setStageList(stageList);
        }
        if(oppStatusList != null){
            saleOppVo.setOppStatusList(oppStatusList);
        }
        if(priorityList != null){
            saleOppVo.setPriorityList(priorityList);
        }
        if(classificationList != null){
            saleOppVo.setClassificationList(classificationList);
        }
        if(oppSourceList != null){
            saleOppVo.setOppSourceList(oppSourceList);
        }
        return saleOppService.findAllSeniorSearch(saleOppVo, saleOppVo.getPage().getPage(), saleOppVo.getPage().getSize());
    }

    @GetMapping("sale_opp_page")
    @ResponseBody
    public PageEntity<SaleOppEntity> loadListPage(Integer pageNumber, Integer pageSize){
        return saleOppService.loadListPage(pageNumber, pageSize);
    }

    //根据客户id获取所有销售机会
    @PostMapping("getOppByCusId")
    @ResponseBody
    public List<SaleOppEntity> findAllByTbCustomerByCusIdAndIsDelete(@RequestParam(value = "cusId") Integer cusId) throws InputException {
        if(cusId == null)
            throw new InputException("输入错误");
        return saleOppService.findAllByTbCustomerByCusIdAndIsDelete(cusId);
    }

    //根据客户id获取所有销售机会
    @GetMapping("getOpp")
    @ResponseBody
    public List<SaleOppEntity> findAllByTbCustodmerByCusIdAndIsDelete(@RequestParam(value = "cusId") Long cusId) throws InputException {
        if(cusId == null)
            throw new InputException("输入错误");
        return saleOppService.loadAllByCusId(cusId);
    }

    @GetMapping("/aaa")
    @ResponseBody
    public SaleOppEntity loadDetailById(Long id) throws NotFoundException {
        return saleOppService.loadDetailById(id);
    }

    /**
     * 查看销售机会视图
     * @param id
     * @return
     * @throws NotFoundException
     */
    @RequestMapping("sale/opp/info/{id}")
    public ModelAndView info(@PathVariable("id") Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("ykm/saleOppInfo");
        if(id == null){
            return new ModelAndView("404");
        }
        SaleOppEntity saleOppEntity = saleOppService.loadDetailById(id);
        if(saleOppEntity == null){
            return new ModelAndView("404");
        }
        modelAndView.addObject("info",saleOppEntity);
        Iterator iterator = saleOppEntity.getTbDemandsByOppId().iterator();
        while(iterator.hasNext()){
            System.out.println("==="+iterator.next());
        }
        Iterator iterator1 = saleOppEntity.getTbSolutionsByOppId().iterator();
        while(iterator1.hasNext()){
            System.out.println("==="+iterator1.next());
        }
        return modelAndView;
    }

    @GetMapping("sale_opp_statistic")
    @ResponseBody
    public List statisticCus(String state) throws ServiceException {
        List<Map> list = saleOppService.findAllForStatistic(state);
        return list;
    }
}
