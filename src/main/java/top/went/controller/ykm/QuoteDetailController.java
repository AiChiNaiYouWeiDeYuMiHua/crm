package top.went.controller.ykm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.pojo.QuoteDetailEntity;
import top.went.pojo.QuoteEntity;
import top.went.service.QuoteDetailService;
import top.went.utils.StringUtils;
import top.went.vo.*;

@Controller
@RequestMapping("/quote/detail")
public class QuoteDetailController {
    @Autowired
    private QuoteDetailService quoteDetailService;

    /**
     * 加载报价
     * @param id
     * @return
     * @throws NotFoundException
     */
    @RequestMapping("load")
    @ResponseBody
    public Code load(Long id) throws NotFoundException {
        if(id == null){
            throw new NotFoundException("id未找到！");
        }
        return Code.success(quoteDetailService.load(id));
    }

    /**
     * 高级查询
     * @param quoteDetailVo
     * @return
     * @throws InputException
     */
    @RequestMapping("senior")
    @ResponseBody
    public PageEntity<QuoteDetailEntity> findAllSeniorSearch(QuoteDetailVo quoteDetailVo) throws InputException {
        if(quoteDetailVo.getPage() == null){
            throw new InputException("输入错误！");
        }
        PageEntity<QuoteDetailEntity> quoteDetailEntityPageEntity = quoteDetailService.findAllSeniorSearch(quoteDetailVo);
        for(QuoteDetailEntity quoteDetailEntity : quoteDetailEntityPageEntity.getRows()){
            //偷懒写法，将产品全称暂存到规格名中
            if(quoteDetailEntity.getTbProductFormatByPfId() != null){
                quoteDetailEntity.getTbProductFormatByPfId().setPfName(StringUtils.getStringBuffer(quoteDetailEntity.getTbProductFormatByPfId()).toString());
            }
        }
        return quoteDetailEntityPageEntity;
    }

}
