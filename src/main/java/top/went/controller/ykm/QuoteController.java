package top.went.controller.ykm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.QuoteDetailEntity;
import top.went.pojo.QuoteEntity;
import top.went.service.ApprovalService;
import top.went.service.OrderService;
import top.went.service.QuoteService;
import top.went.vo.*;

import java.sql.Date;
import java.util.List;

@Controller
@RequestMapping("/quote")

public class QuoteController {
    @Autowired
    private QuoteService quoteService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ApprovalService approvalService;

    /**
     * 加载报价
     * @param id
     * @return
     * @throws NotFoundException
     */
    @RequestMapping("load")
    @ResponseBody
    public Code load(Long id) throws NotFoundException {
        if(id == null){
            throw new NotFoundException("id未找到！");
        }
//        System.out.println("加载的："+quoteService.load(id).getTbQuoteDetailsByQuoteId().size());
        return Code.success(quoteService.load(id));
    }

    /**
     * 高级查询
     * @param quoteVo
     * @return
     * @throws InputException
     */
    @RequestMapping("senior")
    @ResponseBody
    public PageEntity<QuoteEntity> findAllSeniorSearch(QuoteVo quoteVo) throws InputException {
        if(quoteVo.getPage() == null){
            throw new InputException("输入错误！");
        }
        return quoteService.findAllSeniorSearch(quoteVo);
    }

    /**
     * 添加/编辑报价
     * @param quoteEntity
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("add_modify")
    @ResponseBody
    @SystemLog(module = "报价", methods = "新增/修改")
    public Code addAndModify(QuoteEntity quoteEntity) throws InputException, NotFoundException, ServiceException {
        if(quoteEntity == null){
            throw new InputException("输入错误！");
        }
        if(quoteEntity.getQuoteId() != null){
            quoteService.modify(quoteEntity);
            return Code.success("修改成功！");
        } else {
            quoteEntity.setIsDelete(0L);
            quoteEntity.setIsOrder(0L);
            quoteEntity.setApprovalStatus(0L);
            if(quoteEntity.getTotalQuote() == null){
                quoteEntity.setTotalQuote(0d);
            }
            if(quoteEntity.getQuoteTime() == null){
                quoteEntity.setQuoteTime(new Date(new java.util.Date().getTime()));
            }
            if(quoteEntity.getPlan_start_time() == null){
                quoteEntity.setPlan_start_time(new Date(new java.util.Date().getTime()));
            }
            System.out.println("新增的报价单："+quoteEntity);
            quoteService.add(quoteEntity);
            return Code.success("新增成功!");
        }
    }

    /**
     * 删除
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("delete")
    @ResponseBody
    @SystemLog(module = "报价", methods = "删除")
    public Code delete(Long id) throws InputException, NotFoundException, ServiceException {
        if(id == null){
            throw new InputException("输入有误！");
        }
        quoteService.delete(id);
        return Code.success("删除成功！");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @GetMapping("deleteSelected")
    @ResponseBody
    @SystemLog(module = "报价", methods = "批量删除")
    public Code deleteSelected(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for(int i=0; i< ids.length; i++){
            if (ids[i] == null || ids[i].toString().length()<=0)
                throw new InputException("id错误！");
        }
        quoteService.deleteSelected(ids);
        return Code.success("删除成功！");
    }

    @RequestMapping("/info/{id}/")
    public ModelAndView info(@PathVariable("id") Long id) throws NotFoundException {
        ModelAndView modelAndView = new ModelAndView("ykm/quoteInfo");
        if(id == null){
            return new ModelAndView("404");
        }
        QuoteEntity quoteEntity = quoteService.loadDetailById(id);
        if(quoteEntity == null){
            return new ModelAndView("404");
        }
        modelAndView.addObject("info",quoteEntity);
        modelAndView.addObject("approvals",approvalService.findApprovalById(id.intValue(),"报价单"));
        return modelAndView;
    }

    @RequestMapping("/test")
    @ResponseBody
    public QuoteEntity test(Long id) throws NotFoundException {
        return quoteService.loadDetailById(id);
    }

    @RequestMapping("/detail")
    @ResponseBody
    public List<QuoteDetail> detail(Long id) throws InputException, NotFoundException {
        if(id == null || id.toString().length() <= 0){
            throw new InputException("报价详细信息错误");
        }
        return quoteService.loadDetail(id);
    }

    /**
     * 编辑明细页面
     * @param id
     * @param model
     * @return
     * @throws NotFoundException
     */
    @RequestMapping("/to_quoteDetail")
    @SystemLog(module = "报价", methods = "新增/修改")
    public String toQuoteDetail(Long id, Model model) throws NotFoundException {
        model.addAttribute("info",quoteService.load(id));
        return "ykm/quoteDetail";
    }

    /**
     * 保存编辑明细
     * @param quoteDetailEditVo
     * @return
     * @throws InputException
     * @throws ServiceException
     */
    @PostMapping("/detail/save")
    @ResponseBody
    @SystemLog(module = "报价", methods = "编辑明细")
    public Code saveDetail(QuoteDetailEditVo quoteDetailEditVo) throws InputException, ServiceException, NotFoundException {
        if(quoteDetailEditVo.getId() == null){
            throw new InputException("报价单数据信息错误！");
        }
        List<QuoteDetailEntity> quoteDetailEntities = quoteDetailEditVo.getDatas();
        QuoteEntity quoteEntity = new QuoteEntity();
        quoteEntity.setQuoteId(quoteDetailEditVo.getId());
        if(quoteDetailEntities != null){
            for(int i=0; i<quoteDetailEntities.size(); i++){
                quoteDetailEntities.get(i).setTbQuoteByQuoteId(quoteEntity);
            }
        }
        quoteService.editDetail(quoteDetailEntities,quoteDetailEditVo.getId());
        return Code.success("保存明细成功！");
    }

    @RequestMapping("to_order")
    @ResponseBody
    @SystemLog(module = "报价", methods = "转成订单")
    public Code quoteToOrder(@RequestParam(value = "id") Long id) throws NotFoundException, ServiceException, InputException {
        if(id == null){
            throw  new InputException("输入错误！");
        }
        if(quoteService.loadDetail(id).size() <=0){
            return Code.fail("报价明细为空，请先编辑！");
        }
        orderService.quoteToOrder(id);
        return Code.success("转成订单成功！");
    }
}
