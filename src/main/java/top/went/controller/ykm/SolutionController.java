package top.went.controller.ykm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.SolutionEntity;
import top.went.service.SolutionService;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.SolutionVo;

import java.sql.Date;

@Controller
@RequestMapping("/solution")
@ResponseBody
public class SolutionController {
    @Autowired
    private SolutionService solutionService;

    /**
     * 高级查询
     * @param solutionVo
     * @return
     * @throws InputException
     */
    @RequestMapping("senior")
    public PageEntity<SolutionEntity> findAllSeniorSearch(SolutionVo solutionVo) throws InputException {

        if(solutionVo.getPage() == null){
            throw new InputException("参数有误！");
        }
        return solutionService.findAllSeniorSearch(solutionVo.getPage().getPage(), solutionVo.getPage().getSize(), solutionVo);
    }

    /**
     * 加载解决方案
     * @param id
     * @return
     * @throws NotFoundException
     */
    @GetMapping("load")
    public Code load(Long id) throws NotFoundException {
        if(id == null){
            throw new NotFoundException("输入错误");
        }
        return Code.success(solutionService.load(id));
    }

    /**
     * 添加/修改解决方案
     * @param solutionEntity
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    @PostMapping("add_modify")
    @SystemLog(module = "解决方案", methods = "新增/修改")
    public Code addAndModify(SolutionEntity solutionEntity) throws NotFoundException, ServiceException {
        if(solutionEntity == null){
            return Code.fail("你要玩啥？");
        }
        if(solutionEntity.getSolutionId() != null){
            solutionService.modifySolution(solutionEntity);
            return Code.success("修改成功!");
        } else {
            solutionEntity.setIsDelete(0L);
            if(solutionEntity.getSubmitTime() == null){
                solutionEntity.setSubmitTime(new Date(new java.util.Date().getTime()));
            }
            solutionService.addSolution(solutionEntity);
            return Code.success("新增成功！");
        }
    }

    /**
     * 删除解决方案
     * @param id
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @GetMapping("delete")
    @SystemLog(module = "解决方案", methods = "删除")
    public Code delete(Long id) throws InputException, NotFoundException, ServiceException {
        if(id == null || id.toString().length() <= 0){
            throw new InputException("id错误");
        }
        solutionService.deleteSolution(id);
        return Code.success("删除成功！");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @GetMapping("deleteSelected")
    @SystemLog(module = "解决方案", methods = "批量删除")
    public Code deleteSelected(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for(int i=0; i<ids.length; i++)
            if (ids[i] == null || ids[i].toString().length()<=0)
                throw new InputException("id错误");
        solutionService.deleteSelected(ids);
        return Code.success("删除成功！");
    }

}
