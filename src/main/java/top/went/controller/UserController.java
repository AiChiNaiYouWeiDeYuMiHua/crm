package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.UserEntity;
import top.went.service.UserService;
import top.went.utils.CheckForm;
import top.went.utils.Md5;
import top.went.utils.RegularUtils;
import top.went.utils.StringToDate;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.UserPassword;

import java.sql.Date;

@Controller
@RequestMapping("/admin")
@SessionAttributes({"userId", "userName"})
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/to_user_list")
    public String toUserList() {
        return "admin/user_list";
    }

    @GetMapping("/to_user_role_modal")
    public String toUserRoleModal() {
        return "admin/user_role_modal";
    }

    /**
     * 添加用户
     *
     * @param userEntity
     * @return
     * @throws ServiceException
     * @throws InputException
     */
    @PostMapping("/user_add")
    @ResponseBody
    @SystemLog(module = "系统中心", methods = "新增用户")
    public Code addUser(UserEntity userEntity) throws ServiceException, InputException {
        System.out.println(userEntity);
        UserEntity userEntity1 = CheckForm.checkUser(userEntity);
        System.out.println("用户参数" + userEntity1);
        if (userEntity1.getUserId() == null) {
            System.out.println("添加:" + userEntity1);
            return Code.success("添加成功", userService.addUser(userEntity1));
        } else {
            System.out.println("修改:" + userEntity1);
            return Code.success("修改成功", userService.modifyUser(userEntity1));
        }
    }

//    public static void main(String[] args) {
//        Date date = new Date(System.currentTimeMillis());
//        System.out.println(new Date(System.currentTimeMillis()).toString() + date);
//    }

    /**
     * 修改用户信息
     *
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    @PostMapping("/user_modify")
    @ResponseBody
    public Code modifyUser(UserEntity userEntity) throws ServiceException, InputException {
        System.out.println(userEntity);
        UserEntity userEntity1 = CheckForm.checkUser(userEntity);
        System.out.println("用户参数" + userEntity1);
        if (userService.modifyUser(userEntity1)) {
            return Code.success("修改用户信息成功");
        }
        return Code.fail();
    }

    /**
     * 注销用户
     *
     * @param userId
     * @return
     * @throws ServiceException
     */
    @PostMapping("/user_dimission")
    @ResponseBody
    @SystemLog(module = "系统中心", methods = "注销用户")
    public Code deleteUser(Long userId) throws ServiceException {
        if (userId == null)
            return Code.fail();
        return Code.success("注销用户成功", userService.dimissionUser(userId));
    }

    /**
     * 重置密码
     *
     * @param userId
     * @return
     * @throws ServiceException
     */
    @PostMapping("/user_password_reset")
    @ResponseBody
    @SystemLog(module = "系统中心", methods = "重置密码")
    public Code resetUserPassword(Long userId) throws ServiceException {
        if (userId == null)
            return Code.fail();
        return Code.success("重置密码成功", userService.resetUserPassword(userId));
    }

    /**
     * 修改用户密码
     *
     * @param userpwd
     * @return
     * @throws ServiceException
     */
    @PostMapping("/user_pwd_modify")
    @ResponseBody
    @SystemLog(module = "个人中心", methods = "修改密码")
    public Code modifyUserPassword(UserPassword userpwd)
            throws ServiceException, InputException {
        System.out.println("表单：" + userpwd + "阿大声道：" + userpwd.getUserId());
        UserEntity userEntity = userService.findUser(userpwd.getUserId());
        System.out.println("用户密码：" + userEntity);
        if (userpwd.getUserPassOld() == null) {
            return Code.success("原密码不能为空");
        }
        if (userpwd.getUserPassNew1() == null || userpwd.getUserPassNew2() == null) {
            return Code.success("新密码不能为空");
        }
        if (!RegularUtils.matchAccountPassword(userpwd.getUserPassOld())
                || !RegularUtils.matchAccountPassword(userpwd.getUserPassNew1())
                || !RegularUtils.matchAccountPassword(userpwd.getUserPassNew2())) {
            return Code.success();
        }
        if (!userEntity.getUserPassword().equals(Md5.encode(userpwd.getUserPassOld()))) {
            return Code.success("原密码错误");
        }
        if (!userpwd.getUserPassNew1().equals(userpwd.getUserPassNew2())) {
            return Code.success("两次密码不一致");
        }
        if (userService.modifyUserPassword(userpwd)) {
            return Code.success("修改用户密码成功");
        }
        return Code.fail();
    }

    /**
     * 根据姓名模糊查询用户
     *
     * @param userName
     * @return
     * @throws ServiceException
     */
    @GetMapping("/user_find_like_name")
    @ResponseBody
    public PageEntity<UserEntity> findUserLikeName(String userName, Integer pageSize,
                                                   Integer pageNumber) throws ServiceException {
        return userService.findUserLikeName(userName, pageNumber, pageSize);
    }

    /**
     * 查询指定部门的所有用户
     *
     * @param deptId
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    @GetMapping("/user_find_by_dept")
    @ResponseBody
    public PageEntity<UserEntity> findUserByDept(Long deptId, Integer pageSize, Integer pageNumber) throws ServiceException {
        return userService.findUsersByDept(deptId, pageNumber, pageSize);
    }

    @GetMapping("/user_load")
    @ResponseBody
    public Code findUserByUserId(Long userId) throws ServiceException {
        return Code.success("加载用户成功", userService.findUser(userId));
    }

    /**
     * 登录
     *
     * @param userName
     * @param userPassword
     * @param model
     * @return
     * @throws ServiceException
     */
    @PostMapping("/login_to_index1")
    @ResponseBody
    public Code login(String userName, String userPassword, Model model) throws ServiceException, InputException {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(userName);
        userEntity.setUserPassword(userPassword);
        UserEntity userEntity1 = userService.findUserByName(userEntity);
        System.out.println(userEntity1);
        if (userEntity1 == null) {
            System.out.println("用户名错误");
            return Code.success("用户名错误");
        }
        if (!RegularUtils.matchAccountPassword(userPassword)) {
            System.out.println("密码格式错误");
            return Code.success("密码格式错误");
        }
        if (!userEntity1.getUserPassword().equals(Md5.encode(userPassword))) {
            Code.fail("密码错误");
            return Code.success("密码错误");
        }

        model.addAttribute("userId", userEntity1.getUserId());
        model.addAttribute("userName", userEntity1.getUserName());
        return Code.success("登录成功");
    }

    @GetMapping("/user_table_all")
    @ResponseBody
    public PageEntity<UserEntity> findAll(Integer pageSize, Integer pageNumber) throws ServiceException {
        return userService.queryAllUser(pageNumber, pageSize);
    }

    @GetMapping("/to_user_model_add")
    public String userModelAdd() {
        return "admin/user_model_add";
    }


    @GetMapping("/to_select_user")
    public String toSelectUser() {
        return "admin/user_select_modal";
    }

    @GetMapping("/to_demo")
    public String toDemo() {
        return "demo";
    }

    @GetMapping("/go_to_select_user")
    public String gotoSelectUser() {
        return "admin/to_user_select_model";
    }

    /**
     * 选择用户modal
     *
     * @return
     */
    @GetMapping("/to_select_user_modal")
    public String toSelectUserModal() {
        return "admin/select_user_modal";
    }

    @GetMapping("/to_user_check")
    public String toUserCheck() {
        return "admin/user_check";
    }

    @GetMapping("/to_user_details")
    public String toUserDetails() {
        return "admin/user_details";
    }

    @GetMapping("/to_modify_password")
    public String toModifyPassword() {
        return "admin/modify_password";
    }
}
