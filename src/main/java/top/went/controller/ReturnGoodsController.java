package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.*;
import top.went.service.OrderService;
import top.went.service.PayBackDetailService;
import top.went.service.PurchaseService;
import top.went.service.ReturnGoodsService;
import top.went.vo.*;

import java.util.List;

@Controller
@RequestMapping("/sale")
@ResponseBody
public class ReturnGoodsController {
    @Autowired
    private ReturnGoodsService returnGoodsService;
    @Autowired
    private PayBackDetailService payBackDetailService;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private OrderService orderService;
    @GetMapping("/mainReturnGoods")
    public ModelAndView mainReturnGoods() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("sale/mainReturnGoods");
        return mv;
    }
    /**
     * 编辑退货明细
     * @return
     */
    @GetMapping("/returnGoodsProDetail/{rgId}")
    public ModelAndView mainReturnGoods1(@PathVariable("rgId") int rgId) throws ServiceException {
        ModelAndView mv = new ModelAndView();
        ReturnGoodsVO returnGoodsVO = returnGoodsService.loadBy(rgId);
        mv.addObject("info",returnGoodsVO);
        mv.addObject("id",rgId);
        mv.setViewName("sale/returnGoodsProDetail");
        return mv;
    }
    //采购退货
    @GetMapping("/rgAddModal")
    public ModelAndView modal(int purId) throws NotFoundException {
        ModelAndView mv = new ModelAndView();
        PurchaseEntity purchaseEntity = purchaseService.load(purId);
        mv.addObject("purchaseEntity",purchaseEntity);
        mv.setViewName("sale/modal/add_return_goods_model");
        return mv;
    }

    //退货告知
    @GetMapping("/rgAddModal11")
    public ModelAndView modal1() throws NotFoundException {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("sale/modal/add_rg_model");
        return mv;
    }

    //订单退货
    @GetMapping("/rgAddModal1")
    public ModelAndView modal1(int orderId) throws NotFoundException {
        ModelAndView mv = new ModelAndView();
        OrderEntity orderEntity = orderService.load((long) orderId);
        mv.addObject("orderEntity",orderEntity);
        mv.setViewName("sale/modal/add_rg_model1");
        return mv;
    }

    @GetMapping("/rgQueryModal")
    public ModelAndView purQueryModal() {
        return new ModelAndView("sale/modal/rg_super_query_model");
    }

    @GetMapping("/rgDetail")
    public ModelAndView purDetail() {
        return new ModelAndView("sale/rgDetail");
    }

    /**
     * 添加采购或订单退货单
     *
     * @param returnGoodsEntity
     * @throws ServiceException
     */
    @PostMapping("/rg_insert")
    @ResponseBody
    public Code rg_insert(ReturnGoodsEntity returnGoodsEntity,String type) throws ServiceException {
        Integer id=returnGoodsService.rg_insert(returnGoodsEntity, type);
        return Code.success(id);
    }

    /**
     * 删除退货单
     *
     * @param id
     * @throws ServiceException
     */
    @RequestMapping("/rgdelete")
    @ResponseBody
    public Code rg_delete(Integer id) throws ServiceException, NotFoundException {
           returnGoodsService.rgDelete(id);
                return Code.success("删除退货单成功");
    }

    /**
     * 加载
     *
     * @param rgId
     * @return
     */
    @GetMapping("rgload")
    @ResponseBody
    public Code load(int rgId) throws ServiceException {
        ReturnGoodsVO returnGoodsVO = new ReturnGoodsVO();
            returnGoodsVO = returnGoodsService.loadBy(rgId);
        return Code.success(returnGoodsVO);
    }

    /**
     * 加载所有
     *
     * @return
     */
    @RequestMapping("/rgall")
    public PageEntity<ReturnGoodsVO> loadAll(@RequestBody ReturnGoodsVO returnGoodsVO) {
        return returnGoodsService.findAll(returnGoodsVO);
    }
    /**
     * 查看详情
     *
     * @param rgId
     * @return
     */
    @RequestMapping("/rgdetail1/{rgId}/")
    public ModelAndView detail(@PathVariable("rgId") int rgId) throws NotFoundException {
        if (new Integer(rgId) == null) {
            return new ModelAndView("404");
        }
        ModelAndView mv = new ModelAndView("sale/returnGoodsDetail");
        ReturnGoodsEntity returnGoodsEntity = null;
        returnGoodsEntity = returnGoodsService.load( rgId);
        if (returnGoodsEntity == null)
            return new ModelAndView("404");
        return mv;
    }

}
