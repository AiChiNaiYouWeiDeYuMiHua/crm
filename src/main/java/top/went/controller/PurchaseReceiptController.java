package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.PlanPayDetailEntity;
import top.went.pojo.PurchaseEntity;
import top.went.pojo.PurchaseReceiptEntity;
import top.went.service.PlanPayDetailService;
import top.went.service.PurchaseReceiptService;
import top.went.service.PurchaseService;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.PlanPayDetailVO;
import top.went.vo.PurchaseReceiptVO;

@Controller
@RequestMapping("/sale")
@ResponseBody
public class PurchaseReceiptController {
    @Autowired
    private PurchaseReceiptService purchaseReceiptService;
    @Autowired
    private PurchaseService purchaseService;

    @GetMapping("/mainPurchaseReceiptDetail")
    public ModelAndView mainPurchaseReceiptDetail(){
        return new ModelAndView("sale/MainPurchaseReceipt");
    }

    @GetMapping("/purrAddModal")
    public ModelAndView modal(String purId) throws NotFoundException {
        ModelAndView mv = new ModelAndView();
        PurchaseEntity purchaseEntity=null;
        if(purId!=null&&purId.length()>0){
            purchaseEntity=purchaseService.load(Integer.valueOf(purId));
            mv.addObject("purchaseEntity", purchaseEntity);
        }
        mv.setViewName("sale/modal/add_purchase_receipt_model");
        return mv;
    }
    @GetMapping("/purrQueryModal")
    public ModelAndView purrQueryModal(){
        return new ModelAndView("sale/modal/purr_super_query_model");
    }
    @GetMapping("/purrDetail")
    public ModelAndView  purchaseReceiptDetail(){
        return new ModelAndView("sale/purchaseReceiptDetail");
    }


    /**
     * 添加采购发票
     * @param  purchaseReceiptEntity
     * @throws ServiceException
     */
    @PostMapping("/purr_insert")
    @ResponseBody
    public Code purr_insert(PurchaseReceiptEntity purchaseReceiptEntity) throws ServiceException {
        purchaseReceiptEntity.setMagicDelete(0l);
        if(purchaseReceiptService.purr_insert(purchaseReceiptEntity))
            return Code.success("添加采购发票成功");
        return Code.fail();
    }

    /**
     * 删除采购发票
     * @param purrId
     * @throws ServiceException
     */
    @PostMapping("/purrdelete")
    @ResponseBody
    public Code purr_delete(int purrId) throws ServiceException {
        try {
            if(purchaseReceiptService.purr_logicDelete(purrId))
                return Code.success("删除采购发票成功");
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return Code.fail();
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/purrdeleteall")
    @ResponseBody
    public Code  deleteAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
                throw new InputException("采购发票id错误");
        purchaseReceiptService.purr_logicDeleteAll(ids);
        return Code.success("采购发票批量删除成功");
    }

    /**
     * 查询所有采购发票
     * @return
     * @throws ServiceException
     */
    @PostMapping("/purrfindAll")
    @ResponseBody
    public PageEntity<PurchaseReceiptVO> findAll(@RequestBody top.went.vo.Page page) throws ServiceException {

        return purchaseReceiptService.purr_findAll(page.getSize(),page.getPage());
    }
    /**
     * 根据收票日期查询所有采购发票
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    @PostMapping("/purr_findAllByDate")
    @ResponseBody
    public PageEntity<PurchaseReceiptVO> findAllByDate(Integer pageSize,Integer pageNumber,String purr_date) throws ServiceException {
        System.out.println("pageSize"+pageSize+" "+"pageNumber"+pageNumber);
        if (null == pageSize || null == pageNumber){
            pageSize = 10;
            pageNumber = 1;
        }
        return purchaseReceiptService.purr_findByPpdDate(pageSize, pageNumber, purr_date.toString());
    }


    /**
     * 加载
     * @param purrId
     * @return
     */
    @GetMapping("purrload")
    @ResponseBody
    public Code load(Integer purrId){
        PurchaseReceiptVO purchaseReceiptVO=new PurchaseReceiptVO();
        try {
            purchaseReceiptVO =  purchaseReceiptService.load1(purrId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return Code.success(purchaseReceiptVO);
    }
    /**
     * 加载所有
     * @return
     */
    @RequestMapping("/purrall")
    public PageEntity<PurchaseReceiptEntity> loadAll(@RequestBody PurchaseReceiptVO purchaseReceiptVO) {
        System.out.println(purchaseReceiptVO);
        return purchaseReceiptService.findAllByManyConditions(purchaseReceiptVO);
    }

    /**
     * 查看详情
     * @param purrId
     * @return
     */
    @RequestMapping("/purrdetail1/{purrId}/")
    public ModelAndView detail(@PathVariable("purrId")int purrId){
        if (new Integer(purrId)== null) {
            return  new ModelAndView("404");
        }
        ModelAndView mv=new ModelAndView("sale/purchaseReceiptDetail");
        PurchaseReceiptEntity purchaseReceiptEntity=null;
        try {
            purchaseReceiptEntity = purchaseReceiptService.load(purrId);
            if(purchaseReceiptEntity==null)
                return  new ModelAndView("404");
        } catch (ServiceException e) {
            e.printStackTrace();
            return  new ModelAndView("404");
        }
        mv.addObject("info",purchaseReceiptEntity);
        return mv;
    }
}

