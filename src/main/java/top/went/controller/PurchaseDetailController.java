package top.went.controller;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.db.dao.PurchaseDao;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.PurDetailEntity;
import top.went.service.PurchaseDetailService;
import top.went.service.PurchaseService;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.PurchaseDetailVO;

import java.util.ArrayList;

@Controller
@RequestMapping("/sale")
@ResponseBody
public class PurchaseDetailController {
    @Autowired
    private PurchaseDetailService purchaseDetailService;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private PurchaseDao purchaseDao;
    @GetMapping("/mainPurchaseDetail")
    public ModelAndView mainPurchaseDetail(){
        return new ModelAndView("sale/mainPurchaseDetail");
    }
    @GetMapping("/pdAddModal")
    public ModelAndView modal(){
        return new ModelAndView("sale/modal/add_pd_model");
    }
    @GetMapping("/pdQueryModal")
    public ModelAndView rrQueryModal(){
        return new ModelAndView("sale/modal/pd_super_query_model");
    }
    @GetMapping("/pdDetail")
    public ModelAndView  purchaseReceiptDetail(){
        return new ModelAndView("sale/pdDetail");
    }
    /**
     * 添加采购明细
     * @param  purDetailEntity
     * @throws ServiceException
     */
    @PostMapping("/pd_insert")
    @ResponseBody
    public Code rr_insert(PurDetailEntity purDetailEntity) throws ServiceException {
        purDetailEntity.setMagicDelete(0l);
        if(purchaseDetailService.pd_insert(purDetailEntity))
            return Code.success("添加采购明细");
        return Code.fail();
    }

    /**
     * 删除采购明细
     * @param pdId
     * @throws ServiceException
     */
    @GetMapping("/pddelete")
    @ResponseBody
    public Code pd_delete(Integer pdId) throws ServiceException, NotFoundException {
            purchaseDetailService.pd_logicDelete(pdId);
        return Code.success("删除采购明细成功");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/pddeleteall")
    @ResponseBody
    public Code  deleteAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for(int i=0;i<ids.length;i++)
            if(ids[i]==null||ids[i].toString().length()<=0)
                throw new InputException("id错误");
        purchaseDetailService.pd_logicDeleteAll(ids);
        return Code.success("批量删除成功");
    }

    /**
     * 查询所有采购明细
     * @return
     * @throws ServiceException
     */
    @PostMapping("/pdfindAll")
    @ResponseBody
    public PageEntity<PurchaseDetailVO> findAll(@RequestBody top.went.vo.Page page) throws ServiceException {
        return purchaseDetailService.pd_findAll(page.getSize(),page.getPage());
    }

    /**
     * 加载
     * @param pdId
     * @return
     */
    @GetMapping("pdload")
    @ResponseBody
    public Code load(Integer pdId){
        PurchaseDetailVO purchaseDetailVO=null;
        try {
            purchaseDetailVO =  purchaseDetailService.load1(pdId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return Code.success(purchaseDetailVO);
    }
    /**
     * 加载所有
     * @return
     */
    @RequestMapping("/pdall")
    public PageEntity<PurDetailEntity> loadAll(@RequestBody PurchaseDetailVO purchaseDetailVO) {
        return purchaseDetailService.findAllByManyConditions(purchaseDetailVO);
    }
//    /**
//     * 加载所有
//     * @return
//     */
//    @RequestMapping("/pdall1")
//    public java.util.List<PurchaseDetailVO> loadAll1(Integer id) throws ServiceException {
//        PurchaseDetailVO purchaseDetailVO = new PurchaseDetailVO();
//        java.util.List<PurDetailEntity> purDetailEntities=new ArrayList<>();
//        java.util.List<PurchaseDetailVO> purchaseDetailVOS=new ArrayList<>();
//        if(id!=null){
//            purchaseDetailVO.setPurId(id);
//            purDetailEntities=purchaseDetailService.findAllByManyConditions(purchaseDetailVO).getRows();
//            System.out.println("888888888888888888888888888888888888         "+purDetailEntities.size());
//        }
//        for(PurDetailEntity purDetailEntity:purDetailEntities){
//            System.out.println("6666666666666666666666666666666666666666666666666666");
//            if(purDetailEntity.getTbPurchaseByPurId()!=null&&purDetailEntity.getTbPurchaseByPurId().getPurId()!=null&&purDetailEntity.getTbProductFormatByPfId()!=null&&purDetailEntity.getTbProductFormatByPfId().getPfId()!=null){
//                int num= purchaseDao.inCount(purDetailEntity.getTbPurchaseByPurId().getPurId(),purDetailEntity.getTbProductFormatByPfId().getPfId());
//                System.out.println("9999999999999999999999999999999999999         "+num);
//                if(num>0){
//                    purDetailEntity.setPdNum((long) num);
//                    purchaseDetailService.pd_insert(purDetailEntity);
//                    PurchaseDetailVO purchaseDetailVO1 = new PurchaseDetailVO(purDetailEntity);
//                    purchaseDetailVOS.add(purchaseDetailVO1);
//                }
//            }
//        }
//        return purchaseDetailVOS;
//    }
    /**
     * 查看详情
     * @param pdId
     * @return
     */
    @RequestMapping("/pddetail1/{pdId}/")
    public ModelAndView detail(@PathVariable("pdId")int pdId){
        if (new Integer(pdId)== null) {
            return  new ModelAndView("404");
        }
        ModelAndView mv=new ModelAndView("sale/purchaseDetailDetail");
        PurDetailEntity purDetailEntity=null;
        try {
            purDetailEntity = purchaseDetailService.load(pdId);
            if(purDetailEntity==null)
                return  new ModelAndView("404");
        } catch (ServiceException e) {
            e.printStackTrace();
            return  new ModelAndView("404");
        }
        mv.addObject("info",purDetailEntity);
        return mv;
    }
}
