package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.service.SaleStatistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sale/statistics")
public class SaleStatisticsController {
    @Autowired
    private SaleStatistics saleStatistics;

    /**
     * 统计合同/订单分布
     * @return
     */
    @RequestMapping("/order")
    @ResponseBody
    public List<Map<String,Object>> statisticsOrderType(Integer type){
        switch (type){
            case 1:
                return saleStatistics.statisticsOrderType();
            case 2:
                return saleStatistics.statisticsOrderCategory();
            case 3:
                return saleStatistics.statisticsOrderCategoryMoney();
            case 4:
                return saleStatistics.statisticsOrderStatus();
            case 5:
                return saleStatistics.statisticsOrderUserMOney();
            case 6:
                return saleStatistics.statisticsOrderUserfmori();
            case 7:
                return saleStatistics.statisticsOrderUsermori();
            case 8:
                return saleStatistics.statisticsOrderUserreturn();
            case 9:
                return saleStatistics.statisticsUser();
            case 10:
                return saleStatistics.statisticsOrderTime();
            case 11:
                return saleStatistics.statisticsOrderTimeMoney();
        }
        Map<String,Object> map = new HashMap<>();
        map.put("TYPE","其他");
        map.put("count","1");
        List<Map<String,Object>> list = new ArrayList();
        list.add(map);
        return list;
    }
    /**
     * 统计回款分布wsj
     * @return
     */
    @RequestMapping("/payBack/fenbu")
    @ResponseBody
    public List<Map<String,Object>> statisticspbdType(Integer type){
        switch (type){
            case 1:
                return saleStatistics.pbdstatisticsOrderCategoryMoney();
            case 2:
                return saleStatistics.pbdstatisticsByYear();
            case 3:
                return saleStatistics.pbdstatisticsByMonth();
            case 4:
                return saleStatistics.pbdstatisticsByDate();
            case 5:
                return saleStatistics.pbdstatisticsByCus();
        }
        Map<String,Object> map = new HashMap<>();
        map.put("TYPE","其他");
        map.put("count","1");
        List<Map<String,Object>> list = new ArrayList();
        list.add(map);
        return list;
    }
    /**
     * 统计计划回款分布wsj
     * @return
     */
    @RequestMapping("/planPayBack/fenbu")
    @ResponseBody
    public List<Map<String,Object>> statisticsppbdType(Integer type){
        switch (type){
            case 1:
                return saleStatistics.ppbdstatisticsOrderCategoryMoney();
            case 2:
                return saleStatistics.ppbdstatisticsByYear();
            case 3:
                return saleStatistics.ppbdstatisticsByMonth();
            case 4:
                return saleStatistics.ppbdstatisticsByDate();
            case 5:
                return saleStatistics.ppbdstatisticsByCus();
        }
        Map<String,Object> map = new HashMap<>();
        map.put("TYPE","其他");
        map.put("count","1");
        List<Map<String,Object>> list = new ArrayList();
        list.add(map);
        return list;
    }
    /**
     * 统计计划付款分布wsj
     * @return
     */
    @RequestMapping("/planPay/fenbu")
    @ResponseBody
    public List<Map<String,Object>> statisticsppddType(Integer type){
        switch (type){
            case 1:
                return saleStatistics.ppdstatisticsOrderCategoryMoney();
            case 2:
                return saleStatistics.ppdstatisticsByYear();
            case 3:
                return saleStatistics.ppdstatisticsByMonth();
            case 4:
                return saleStatistics.ppdstatisticsByDate();
            case 5:
                return saleStatistics.ppdstatisticsByCus();
        }
        Map<String,Object> map = new HashMap<>();
        map.put("TYPE","其他");
        map.put("count","1");
        List<Map<String,Object>> list = new ArrayList();
        list.add(map);
        return list;
    }
    /**
     * 统计付款分布wsj
     * @return
     */
    @RequestMapping("/payRecords/fenbu")
    @ResponseBody
    public List<Map<String,Object>> statisticsprType(Integer type){
        switch (type){
            case 1:
                return saleStatistics.ppdstatisticsOrderCategoryMoney1();
            case 2:
                return saleStatistics.ppdstatisticsByYear1();
            case 3:
                return saleStatistics.ppdstatisticsByMonth1();
            case 4:
                return saleStatistics.ppdstatisticsByDate1();
            case 5:
                return saleStatistics.ppdstatisticsByCus1();
        }
        Map<String,Object> map = new HashMap<>();
        map.put("TYPE","其他");
        map.put("count","1");
        List<Map<String,Object>> list = new ArrayList();
        list.add(map);
        return list;
    }
}
