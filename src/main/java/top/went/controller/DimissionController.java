package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.exception.ServiceException;
import top.went.pojo.DimissionEntity;
import top.went.service.DimissionService;
import top.went.vo.Code;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class DimissionController {

    @Autowired
    private DimissionService dimissionService;

    /**
     * 获取指定类型的所有系统参数
     *
     * @param typeId
     * @return
     * @throws ServiceException
     */
    @RequestMapping("/get_dimission")
    @ResponseBody
    public Code getDimission(Long typeId) throws ServiceException {
        return Code.success("加载成功", dimissionService.queryByType(typeId));
    }

    @GetMapping("/find_dimi")
    @ResponseBody
    public Code findDimi(Long typeId, Integer pageNumber, Integer pageSize) throws ServiceException {
        return Code.success("加载成功", dimissionService.findToList(typeId, pageNumber, pageSize));
    }

    @PostMapping("/dimi_modify")
    @ResponseBody
    public Code modifyDimi(DimissionEntity dimissionEntity) throws ServiceException {
        return Code.success("修改成功", dimissionService.modifyDimission(dimissionEntity));
    }

    @GetMapping("to_dimission")
    public String toDimi(Long typeId, HttpServletRequest request) {
        request.getSession().setAttribute("typeId", typeId);
        return "admin/dimission_list";
    }
}
