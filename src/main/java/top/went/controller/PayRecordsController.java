package top.went.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.db.mapper.PayRecordsMapper;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PlanPayDetailEntity;
import top.went.pojo.PurchaseEntity;
import top.went.pojo.ReturnGoodsEntity;
import top.went.service.PayRecordsService;
import top.went.service.PurchaseService;
import top.went.service.ReturnGoodsService;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.PaymentRecordVO;
import top.went.vo.PlanPayDetailVO;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

@Controller
@RequestMapping("/sale")
@ResponseBody
public class PayRecordsController {
    @Autowired
    private PayRecordsService payRecordsService;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private ReturnGoodsService returnGoodsService;

    @GetMapping("/payRecords")
    public ModelAndView mainPlanPayDetail(){
        return new ModelAndView("sale/mainPayRecords");
    }
    @GetMapping("/addPrModel")
    public ModelAndView addPrModel(String purId) throws NotFoundException {
        ModelAndView mv = new ModelAndView();
        PurchaseEntity purchaseEntity=null;
        if(purId!=null&&purId.length()>0){
            purchaseEntity=purchaseService.load(Integer.valueOf(purId));
            mv.addObject("purchaseEntity", purchaseEntity);
        }
        mv.setViewName("sale/modal/add_pay_records_model");
        return mv;
    }

    /**
     * 新增退款
     * @param rgId
     * @return
     * @throws NotFoundException
     */
    @GetMapping("/addPrModel1")
    public ModelAndView addPrModel1(String rgId) throws NotFoundException {
        ModelAndView mv = new ModelAndView();
        ReturnGoodsEntity returnGoodsEntity =null;
        if(rgId!=null&&rgId.length()>0){
            returnGoodsEntity=returnGoodsService.load(Integer.valueOf(rgId));
            mv.addObject("returnGoodsEntity", returnGoodsEntity);
        }
        mv.setViewName("sale/modal/add_pay_records_model1");
        return mv;
    }
    @GetMapping("/queryPrModel")
    public ModelAndView queryPrModel(){
        return new ModelAndView("sale/modal/pr_super_query_model");
    }
    @GetMapping("/prdetail")
    public ModelAndView prdetail(){
        return new ModelAndView("sale/payRecordsDetail");
    }
    /**
     * 添加付款记录
     paymentRecordsEntity
     * @return
     * @throws ServiceException
     */
    @PostMapping("/pr_insert")
    @ResponseBody
    public Code pr_insert(PaymentRecordsEntity paymentRecordsEntity)throws ServiceException{
        paymentRecordsEntity.setMagicDelete(0l);
        if(payRecordsService.pr_insert(paymentRecordsEntity))
            return Code.success("添加付款记录成功");
        return Code.fail();
    }

    /**
     * 删除付款记录
     * @param prId
     * @return
     * @throws ServiceException
     */
    @PostMapping("/prdelete")
    @ResponseBody
    public Code pr_delete(int prId) throws ServiceException{
        try {
            if(payRecordsService.pr_delete(prId))
            return Code.success("删除付款记录成功");
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return Code.fail();
    }
    /**
     * 批量删除
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/prdeleteall")
    @ResponseBody
    public Code  deleteAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
                throw new InputException("付款id错误");
        payRecordsService.pr_logicDeleteAll(ids);
        return Code.success("付款批量删除成功");
    }

    /**
     * 查询所有付款记录
     * @param page
     * @return
     * @throws ServiceException
     */
    @PostMapping("/prfindAll")
    @ResponseBody
    public PageEntity<PaymentRecordVO> findAll(@RequestBody top.went.vo.Page page) throws ServiceException {
        return payRecordsService.pr_findAll(page.getSize(), page.getPage());
    }
    /**
     * 根据日期查询所有付款
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    @PostMapping("/prfindAllByDate")
    @ResponseBody
    public PageEntity<PaymentRecordVO> findAllByDate(Integer pageSize,Integer pageNumber,String pr_date) throws ServiceException {
        System.out.println("pageSize"+pageSize+" "+"pageNumber"+pageNumber);
        if (null == pageSize || null == pageNumber){
            pageSize = 10;
            pageNumber = 1;
        }
        return payRecordsService.pr_findByPpdDate(pr_date,pageSize, pageNumber);
    }
    /**
     * 加载
     * @param prId
     * @return
     */
    @GetMapping("prload")
    @ResponseBody
    public Code load(Integer prId){
        PaymentRecordVO paymentRecordVO = null;
        try {
            paymentRecordVO =  payRecordsService.load1(prId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return Code.success(paymentRecordVO);
    }
    /**
     * 加载所有
     * @return
     */
    @RequestMapping("/prall")
    public PageEntity<PaymentRecordsEntity> loadAll(@RequestBody  PaymentRecordVO paymentRecordVO) {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1"+paymentRecordVO.getPrType()+"    "+paymentRecordVO.getPrDate());
        return payRecordsService.findAllByManyConditions(paymentRecordVO);
    }

    /**
     * 查看详情
     * @param prId
     * @return
     */
    @RequestMapping("/prdetail1/{prId}/")
    public ModelAndView detail(@PathVariable("prId")int prId){
        if (new Integer(prId)== null) {
            return  new ModelAndView("404");
        }
        ModelAndView mv=new ModelAndView("sale/payRecordsDetail");
        PaymentRecordsEntity paymentRecordsEntity=null;
        try {
            paymentRecordsEntity = payRecordsService.load(prId);
            if(paymentRecordsEntity==null)
                return  new ModelAndView("404");
        } catch (ServiceException e) {
            e.printStackTrace();
            return  new ModelAndView("404");
        }
        mv.addObject("info",paymentRecordsEntity);
        return mv;
    }
}
