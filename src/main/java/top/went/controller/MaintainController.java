package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.MaintainOrderEntity;
import top.went.service.MaintainService;
import top.went.service.OrderService;
import top.went.service.ProductService;
import top.went.utils.StringUtils;
import top.went.vo.Code;
import top.went.vo.MaintainVO;
import top.went.vo.PageEntity;

@Controller
@RequestMapping("/saleAfter")
@ResponseBody
public class MaintainController {
    @Autowired
    private MaintainService maintainService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;

    @GetMapping("/maintain")
    public ModelAndView maintain() {
        return new ModelAndView("sale/MainMaintain");
    }

    @GetMapping("/mtAddModal")
    public ModelAndView modal() {
        return new ModelAndView("sale/modal/add_mt_model");
    }

    @GetMapping("/mtQueryModal")
    public ModelAndView rrQueryModal() {
        return new ModelAndView("sale/modal/mt_super_query_model");
    }

    @GetMapping("/mtDetail")
    public ModelAndView purchaseReceiptDetail() {
        return new ModelAndView("sale/mtDetail");
    }

    /**
     * 添加维修工单
     *
     * @param maintainOrderEntity
     * @throws ServiceException
     */
    @PostMapping("/mt_insert")
    @ResponseBody
    public Code rr_insert(MaintainOrderEntity maintainOrderEntity) throws ServiceException {
        maintainOrderEntity.setMagicDelete(0l);
        if (maintainService.mt_insert(maintainOrderEntity))
            return Code.success("添加维修工单");
        return Code.fail();
    }

    /**
     * 删除维修工单
     *
     * @param mtId
     * @throws ServiceException
     */
    @PostMapping("/mtdelete")
    @ResponseBody
    public Code mt_delete(String mtId) throws ServiceException, NotFoundException {
        maintainService.mt_logicDelete(Integer.parseInt(mtId));
        return Code.success("删除维修工单成功");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/mtdeleteall")
    @ResponseBody
    public Code deleteAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for (int i = 0; i < ids.length; i++)
            if (ids[i] == null || ids[i].toString().length() <= 0)
                throw new InputException("id错误");
        maintainService.mt_logicDeleteAll(ids);
        return Code.success("批量删除成功");
    }

    /**
     * 查询所有维修工单
     *
     * @return
     * @throws ServiceException
     */
    @PostMapping("/mtfindAll")
    @ResponseBody
    public PageEntity<MaintainVO> findAll(@RequestBody top.went.vo.Page page) throws ServiceException {
        return maintainService.mt_findAll(page.getSize(), page.getPage());
    }

    /**
     * 根据状态和主题查询维修工单
     *
     * @return
     * @throws ServiceException
     */
    @PostMapping("/mt_findAllByStateAndTheme")
    @ResponseBody
    public PageEntity<MaintainVO> findAllByStateAndTheme(@RequestBody MaintainVO maintainVO) throws ServiceException {
        return maintainService.mt_findByStateAndTheme(maintainVO);
    }

    /**
     * 加载
     *
     * @param mtId
     * @return
     */
    @GetMapping("mtload")
    @ResponseBody
    public Code load(Integer mtId) {
        MaintainVO maintainVO = null;
        try {
            maintainVO = maintainService.load1(mtId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return Code.success(maintainVO);
    }

    /**
     * 加载所有
     *
     * @return
     */
    @RequestMapping("/mtall")
    public PageEntity<MaintainOrderEntity> loadAll(@RequestBody MaintainVO maintainVO) {
        return maintainService.findAllByManyConditions(maintainVO);
    }

    /**
     * 查看详情
     *
     * @param mtId
     * @return
     */
    @RequestMapping("/mtdetail1/{mtId}/")
    public  ModelAndView detail(@PathVariable("mtId") int mtId) throws NotFoundException {
        if (new Integer(mtId) == null) {
            return new ModelAndView("404");
        }
        ModelAndView mv = new ModelAndView("sale/maintainDetail");
        MaintainOrderEntity maintainOrderEntity = null;
        try {
            maintainOrderEntity = maintainService.load(mtId);
            if (maintainOrderEntity == null)
                return new ModelAndView("404");
        } catch (ServiceException e) {
            e.printStackTrace();
            return new ModelAndView("404");
        }
        StringBuffer title= StringUtils.getStringBuffer(productService.load(maintainOrderEntity.getTbProductByProductId().getPfId()));
        mv.addObject("info", maintainOrderEntity);
        mv.addObject("title", title);
        return mv;
    }

    /**
     * 根据客户查询订单
     *
     * @param id
     * @return
     */
    @GetMapping("/loadOrderByus1")
    @ResponseBody
    public Code loadOrderByus(Integer id) {
        return Code.success(orderService.loadOrderByus(id.longValue()));
    }

    /**
     * 根据订单id查询订单明细
     *
     * @param id
     * @return
     */
    @GetMapping("/loadProByOrderId")
    @ResponseBody
    public Code loadProByOrderId(Long id) {
        return Code.success(orderService.loadDeatil(id));
    }
}
