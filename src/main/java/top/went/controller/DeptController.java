package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.exception.InputException;
import top.went.exception.ServiceException;
import top.went.pojo.DeptEntity;
import top.went.service.DeptService;
import top.went.utils.CheckForm;
import top.went.vo.Code;
import top.went.vo.DeptIdAndUserIds;
import top.went.vo.DeptVo;

import java.util.List;

@Controller
public class DeptController {

    @Autowired
    private DeptService deptService;

    /**
     * 新增部门
     *
     * @param deptEntity1
     * @return
     * @throws InputException
     * @throws ServiceException
     */
    @PostMapping("/dept_add")
    @ResponseBody
    public Code addDept(DeptEntity deptEntity1) throws InputException, ServiceException {
        DeptEntity deptEntity = CheckForm.checkDept(deptEntity1);
        if (deptEntity.getDeptId() == null) {
            return Code.success("新增部门成功", deptService.addDept(deptEntity));
        } else {
            return Code.success("修改部门成功", deptService.modifyDept(deptEntity));
        }
//        if (deptService.addDept(deptEntity)) {
//            return Code.success("新增部门成功");
//        }
//        return Code.fail("新增部门失败");
    }

    /**
     * 删除部门
     *
     * @param deptId
     * @return
     * @throws ServiceException
     */
    @GetMapping("/dept_del")
    @ResponseBody
    public Code deleteDept(Long deptId) throws ServiceException {
        deptService.deleteDept(deptId);
        return Code.success("删除部门成功");
    }

    /**
     * 查找所有部门
     *
     * @return
     * @throws ServiceException
     */
    @GetMapping("/dept_list")
    @ResponseBody
    public List<DeptEntity> findDepts() throws ServiceException {
        return deptService.findDepts();
    }

    @GetMapping("/dept_load")
    @ResponseBody
    public Code deptLoad(Long deptId) throws ServiceException {
        return Code.success("加载部门成功", deptService.findOneById(deptId));
    }

    @GetMapping("/dept_table")
    @ResponseBody
    public List<DeptEntity> findDeptToTable() throws ServiceException {
        return deptService.findToTable();
    }

    @GetMapping("/dept_find_like_name")
    @ResponseBody
    public List<DeptEntity> findLikeName(String deptName) throws ServiceException {
        return deptService.findAllDeptNameLike(deptName);
    }

    /**
     * 加载部门树
     *
     * @return
     * @throws ServiceException
     */
    @GetMapping("/dept_user_tree")
    @ResponseBody
    public List<DeptVo> findUsersByDepts() throws ServiceException {
        return deptService.getUserNodes();
    }
    @PostMapping("/grant_user_to_dept")
    @ResponseBody
    public Code grantUserToDept(DeptIdAndUserIds ids)throws ServiceException{
        return Code.success("分配员工成功",deptService.grantUserToDept(ids));
    }

    @GetMapping("/to_dept_tree")
    public String toDeptTree() {
        return "admin/dept_tree";
    }

    @GetMapping("/to_dept")
    public String toDept() {
        return "admin/dept";
    }

    @GetMapping("/to_dept_add_modal")
    public String toDeptAddModal() {
        return "admin/dept_add_modal";
    }

    @GetMapping("/to_dept_add_user")
    public String toDeptAddUser() {
        return "admin/dept_add_user";
    }
}
