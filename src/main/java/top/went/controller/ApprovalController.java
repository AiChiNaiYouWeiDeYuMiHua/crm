package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import top.went.exception.ServiceException;
import top.went.pojo.ApprovalEntity;
import top.went.pojo.ApprovalLogsEntity;
import top.went.pojo.UserEntity;
import top.went.service.ApprovalService;
import top.went.utils.CheckForm;
import top.went.vo.Code;
import top.went.vo.PageEntity;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ApprovalController {
    @Autowired
    private ApprovalService approvalService;

    @GetMapping("/to_approval_list")
    public String toApprovalList() {
        return "admin/approval_list";
    }

    @GetMapping("to_approval_option_modal")
    public String toApprovalOptionModal() {
        return "admin/approval_option_modal";
    }

    @RequestMapping("/approval/boss")
    public String bossEnter(Long id,String type, Model model){
        model.addAttribute("id",id);
        model.addAttribute("type",type);
        return "sale/modal/approval/boss";
    }
    @RequestMapping("/approval/approval")
    public String approvalEnter(Long id,String type, Model model){
        model.addAttribute("id",id);
        model.addAttribute("type",type);
        return "sale/modal/approval/approval";
    }
    @RequestMapping("/approval/approval-ok")
    @ResponseBody
    public Code approvalEnter(ApprovalLogsEntity logsEntity,Integer status, HttpServletRequest request) throws ServiceException {
        CheckForm.checkApprovalLog(logsEntity);
        UserEntity user = (UserEntity) request.getSession().getAttribute("user");
        System.out.println("我是007"+status);
        approvalService.approval(logsEntity,user,status);
        return Code.success();
    }
    @RequestMapping("/approval/cancel")
    public String cancelEnter(Long id,String type, Model model){
        model.addAttribute("id",id);
        model.addAttribute("type",type);
        return "sale/modal/approval/cancel";
    }
    @RequestMapping("/approval/ok")
    public String okEnter(Long id,String type, Model model){
        model.addAttribute("id",id);
        model.addAttribute("type",type);
        return "sale/modal/approval/ok";
    }
    @RequestMapping("/approval/no")
    public String noEnter(Long id,String type, Model model){
        model.addAttribute("id",id);
        model.addAttribute("type",type);
        return "sale/modal/approval/no";
    }
    @RequestMapping("/approval/unlock")
    public String unlockEnter(Long id,String type, Model model){
        model.addAttribute("id",id);
        model.addAttribute("type",type);
        return "sale/modal/approval/unlock";
    }

    /**
     * 分页显示加载所有审批
     *
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws ServiceException
     */
    @GetMapping("/query_approval_all")
    @ResponseBody
    public PageEntity<ApprovalEntity> findApprovalAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        return approvalService.queryAll(pageNumber, pageSize);
    }

    /**
     * 根据申请者分页显示加载所有审批
     *
     * @param userId
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws ServiceException
     */
    @GetMapping("/query_approval_all_user")
    @ResponseBody
    public PageEntity<ApprovalEntity> findApprovalAllByUser(Long userId, Integer pageNumber, Integer pageSize) throws ServiceException {
        return approvalService.queryAllByUser(userId, pageNumber, pageSize);
    }

    /**
     * 根据类型分页显示加载所有审批
     *
     * @param type
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws ServiceException
     */
    @GetMapping("/query_approval_all_type")
    @ResponseBody
    public PageEntity<ApprovalEntity> findApprovalAllByType(String type, Integer pageNumber, Integer pageSize) throws ServiceException {
        return approvalService.queryAllByType(type, pageNumber, pageSize);
    }

    /**
     * 根据申请者和类型分页显示加载所有审批
     *
     * @param userId
     * @param type
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws ServiceException
     */
    @GetMapping("/query_approval_all_user_type")
    @ResponseBody
    public PageEntity<ApprovalEntity> findApprovalAllByUserAndType(Long userId, String type, Integer pageNumber, Integer pageSize) throws ServiceException {
        return approvalService.queryAllByUserAndType(userId, type, pageNumber, pageSize);
    }

    @GetMapping("/load_approval")
    @ResponseBody
    public Code load(Long approvalId) throws ServiceException {
        return Code.success("加载成功", approvalService.load(approvalId));
    }
}
