package top.went.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.ProductFormatEntity;
import top.went.pojo.WhNumberEntity;
import top.went.service.ProductService;
import top.went.utils.CheckForm;
import top.went.vo.*;

import java.util.List;


/**
 * 产品
 */
@RequestMapping("/sale/product")
@Controller
@ResponseBody
public class ProductController {
    @Autowired
    private ProductService productService;


    /**
     * 加载所有数据
     * @param search
     * @return
     */
    @RequestMapping("/all")
    public PageEntity<Product> loadAllBySimple(ProductSearch search) {
        return productService.findAll(search);
    }

    @RequestMapping("tree")
    public Code tree(){
        return Code.success(productService.findAllProduct());
    }
    /**
     * 加载产品所有规格
     * @param id
     * @return
     * @throws NotFoundException
     * @throws InputException
     */
    @RequestMapping("/formatter/all")
    public List<Product> loadFormatterAll(Long id) throws NotFoundException, InputException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("产品信息错误");
        return productService.loadFormatters(id);
    }

    @RequestMapping("/load")
    public Code load(Long id) throws InputException, NotFoundException {
        if (id == null)
            throw new InputException("输入错误");
        return Code.success(productService.load(id));
    }

    /**
     * 添加产品
     * @return
     */
    @PostMapping("/save")
    @SystemLog(module = "产品管理",methods = "添加/修改产品")
    public Code addProduct(ProductFormatEntity formatEntity) throws InputException, NotFoundException, ServiceException
    {

        ProductFormatEntity result =  CheckForm.checkProductForm(formatEntity,false);
        if (formatEntity.getPfId() != null)
            return Code.success("修改成功",productService.modifyProduct(formatEntity,false));
        else
            return Code.success("添加成功",productService.addProduct(result,false));
    }

    /**
     * 添加产品子规格
     * @param formatEntity
     * @return
     * @throws InputException
     */
    @PostMapping("/formatter/save")
    @SystemLog(module = "产品管理",methods = "添加/修改产品规格")
    public Code addProductFormate(ProductFormatEntity formatEntity) throws InputException, NotFoundException,
            ServiceException {
        ProductFormatEntity result =  CheckForm.checkProductForm(formatEntity,true);
        if (formatEntity.getPfId() != null) {
            productService.modifyProduct(formatEntity, true);
            return Code.success("修改成功");
        }
        else{
            productService.addProduct(result,true);
            return Code.success("添加成功");
        }

    }

    /**
     * 删除产品
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @SystemLog(module = "产品管理",methods = "删除产品")
    public Code  deleteProduct(Long id) throws InputException, NotFoundException, ServiceException {
        if (id == null || id.toString().length() <=0)
            throw new InputException("产品id错误");
        productService.deleteProduct(id.intValue());
        return Code.success("产品删除成功");
    }

    /**
     * 批量删除产品
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/deleteall")
    @SystemLog(module = "产品管理",methods = "批量删除产品")
    public Code  deleteProductAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
             throw new InputException("产品id错误");
        productService.deleteProductAll(ids);
        return Code.success("产品删除成功");
    }

    /**
     * 产品详情页面
     * @param id
     * @return
     */
    @RequestMapping("/info/{id}/")
    public ModelAndView info(@PathVariable("id") Long id){
        ModelAndView modelAndView = new ModelAndView("sale/productInfo");
        if (id == null)
            return  new ModelAndView("404");
        try {
            Product product = productService.load(id);
            System.out.println(product);
            if (product == null)
                return new ModelAndView("404");

            modelAndView.addObject("info",product);
        } catch (NotFoundException e) {
            e.printStackTrace();
            return new ModelAndView("404");
        }
        return modelAndView;
    }
}
