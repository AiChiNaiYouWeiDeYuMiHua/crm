package top.went.controller.hgh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.ServiceException;
import top.went.pojo.ServeEntity;
import top.went.service.ServeService;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.ServeVo;

@Controller
@RequestMapping("/serve/")
@ResponseBody
public class ServeController {
    @Autowired
    private ServeService serveService;

    @RequestMapping("add_serve")
    @SystemLog(module = "客户服务管理", methods = "新增客户服务")
    public Code addCots(ServeEntity serveEntity) throws ServiceException {
        if (serveEntity.getTbCustomerByCusId().getCusId() == null){
            serveEntity.setTbCustomerByCusId(null);
        }
        if (serveEntity.getTbUserByUserId().getUserId() == null){
            serveEntity.setTbUserByUserId(null);
        }
        return serveService.addCots(serveEntity);
    }

    @RequestMapping("delete_serve")
    @SystemLog(module = "客户服务管理", methods = "删除客户服务")
    public Code deleteCots(Integer id) throws ServiceException{
        return serveService.deleteCots(id);
    }

    @RequestMapping("update_serve")
    @SystemLog(module = "客户服务管理", methods = "更新客户服务")
    public Code updateCots(ServeEntity serveEntity) throws ServiceException{
        if (serveEntity.getTbCustomerByCusId().getCusId() == null){
            serveEntity.setTbCustomerByCusId(null);
        }
        if (serveEntity.getTbUserByUserId().getUserId() == null){
            serveEntity.setTbUserByUserId(null);
        }
        System.out.println(serveEntity);
        return serveService.updateCots(serveEntity);
    }

    @RequestMapping("load_serve")
    public ServeEntity loadCus(Integer id) throws ServiceException{
        ServeEntity serveEntity = serveService.getOneCus(id);
        return serveEntity;
    }

    @RequestMapping("serve_list")
    public PageEntity<ServeVo> getCotsListByPage(ServeVo serveVo)throws ServiceException {
        System.out.println(serveVo.getKid());
        return serveService.getCotsList(serveVo);
    }

    @RequestMapping("delete_all")
    @SystemLog(module = "客户服务管理", methods = "删除客户服务")
    public Code  deleteProductAll(@RequestParam(value = "ids[]") Integer[] ids) throws InputException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
                throw new InputException("客户id错误");
        serveService.deleteProductAll(ids);
        return Code.success("客户删除成功");
    }
}
