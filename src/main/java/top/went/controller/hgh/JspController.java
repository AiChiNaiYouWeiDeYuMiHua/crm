package top.went.controller.hgh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import top.went.exception.ServiceException;
import top.went.pojo.ContactsEntity;
import top.went.pojo.ThreeEntity;
import top.went.service.*;

import java.util.List;

@Controller
public class JspController {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private MemorialService memorialService;
    @Autowired
    private ContactsService contactsService;
    @Autowired
    private ServeService serveService;
    @Autowired
    private ComplaintService complaintService;
    @Autowired
    private CareService careService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ThreeService threeService;
    @Autowired
    private CustomerDetailService customerDetailService;
    /**
     * 客户
     * @return
     */
    @GetMapping("customer_view")
    public String showCus(){
        return "hgh/customer_view";
    }
    @GetMapping("addcus_modal")
    public String addCus(){
        return "hgh/modal/addcus_modal";
    }
    @GetMapping("transfor_modal")
    public String transforCus(){
        return "hgh/modal/transfor_modal";
    }
    @GetMapping("modify_modal")
    public String modifyCus(){
        return "hgh/modal/modify_modal";
    }
    @RequestMapping("customer_details")
    public String loadInfo(Integer id,Model model)  {
        try{
            List<ContactsEntity> contactsEntityList = contactsService.getCotsListByCusId(id);
            model.addAttribute("cusInfo", customerService.getOneCus(id));
            model.addAttribute("threeInfo",threeService.getByCusIdSin(id));
            model.addAttribute("showAll",customerDetailService.showAll(id));
            model.addAttribute("saleInfo",customerService.getOneSaleOpp(id));
            if(contactsEntityList != null){
                model.addAttribute("cotsInfo",contactsEntityList);
            }else {
                model.addAttribute("cotsInfo",null);
            }
            return "hgh/customer_details";
        }catch(Exception e){
            e.printStackTrace();
        }
        return "hgh/customer_details";
    }
    @RequestMapping("customer_check")
    public String cusCheck(){
        return "hgh/customer_check";
    }
    @RequestMapping("transforCusCheck_modal")
    public String transforCots2(){
        return "hgh/modal/transfor-check_modal";
    }
    @RequestMapping("customer_input")
    public String cusInput(){
        return "hgh/customer_input";
    }
    @RequestMapping("sanyike_modal")
    public String sanyike(Integer id,Model model)  {
        try{
            model.addAttribute("cusInfo", customerService.getOneCus(id));
            return "hgh/modal/sanyike_modal";
        }catch(Exception e){
            e.printStackTrace();
        }
        return "hgh/modal/sanyike_modal";
    }


    /**
     * 联系人
     */
    @RequestMapping("contact_view")
    public String contactView(){
        return "hgh/contact_view";
    }
    @RequestMapping("addcots_modal")
    public String addCots(){
        return "hgh/modal/addcots_modal";
    }
    @RequestMapping("upload_modal")
    public String upload(){
        return "hgh/modal/uploadfile_modal";
    }
    @RequestMapping("transforcots_modal")
    public String transforCots(){
        return "hgh/modal/transforcots_modal";
    }
    @RequestMapping("contact_detail")
    public String cotsDetail(Integer id,Model model)  {
        try{
            model.addAttribute("cotsInfo", contactsService.getOneCus(id));
            return "hgh/detail/contact_detail";
        }catch(Exception e){
            e.printStackTrace();
        }
        return "hgh/detail/contact_detail";
    }


    /**
     * 纪念日
     */
    @RequestMapping("memorial_view")
    public String memorialView(){
        return "hgh/memorial_view";
    }
    @RequestMapping("addmed_modal")
    public String addMed(){
        return "hgh/modal/addmed_modal";
    }
    @RequestMapping("memorial_detail")
    public String medDetail(Integer id,Model model)  {
        try{
            model.addAttribute("medInfo", memorialService.getOneCus(id));
            return "hgh/detail/memorial_detail";
        }catch(Exception e){
            e.printStackTrace();
        }
        return "hgh/detail/memorial_detail";
    }

    /**
     * 客户服务
     */

    @RequestMapping("serve_view")
    public String serveView(){
        return "hgh/serve_view";
    }
    @RequestMapping("addserve_modal")
    public String addServe(){
        return "hgh/modal/addserve_modal";
    }
    @RequestMapping("serve_detail")
    public String serveDetail(Integer id,Model model)  {
        try{
            model.addAttribute("serveInfo", serveService.getOneCus(id));
            return "hgh/detail/serve_detail";
        }catch(Exception e){
            e.printStackTrace();
        }
        return "hgh/detail/serve_detail";
    }

    /**
     * 投诉
     */
    @RequestMapping("complaint_view")
    public String complaintView(){
        return "hgh/complaint_view";
    }
    @RequestMapping("addcomplaint_modal")
    public String addComplaint(){
        return "hgh/modal/addcomplaint_modal";
    }
    @RequestMapping("complaint_detail")
    public String complaintDetail(Integer id,Model model)  {
        try{
            model.addAttribute("complaintInfo", complaintService.getOneCus(id));
            return "hgh/detail/complaint_detail";
        }catch(Exception e){
            e.printStackTrace();
        }
        return "hgh/detail/complaint_detail";
    }

    @RequestMapping("care_view")
    public String careView(){
        return "hgh/care_view";
    }
    @RequestMapping("addcare_modal")
    public String addCare(){
        return "hgh/modal/addcare_modal";
    }
    @RequestMapping("care_detail")
    public String careDetail(Integer id,Model model)  {
        try{
            model.addAttribute("careInfo", careService.getOneCus(id));
            return "hgh/detail/care_detail";
        }catch(Exception e){
            e.printStackTrace();
        }
        return "hgh/detail/care_detail";
    }

    /**
     * 项目
     */
    @RequestMapping("project_view")
    public String projView(){
        return "hgh/project_view";
    }
    @RequestMapping("addproject_modal")
    public String addProj(){
        return "hgh/modal/addproject_modal";
    }
    @RequestMapping("project_details")
    public String projDetail(Integer id,Model model)  {
        try{
            model.addAttribute("projectInfo", projectService.getOneCus(id));
            return "hgh/detail/project_details";
        }catch(Exception e){
            e.printStackTrace();
        }
        return "hgh/detail/project_details";
    }
}
