package top.went.controller.hgh;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.ServiceException;
import top.went.pojo.CareEntity;
import top.went.service.CareService;
import top.went.vo.CareVo;
import top.went.vo.Code;
import top.went.vo.PageEntity;

@Controller
@RequestMapping("/care/")
@ResponseBody
public class CareController {
    @Autowired
    private CareService careService;

    @RequestMapping("add_care")
    @SystemLog(module = "客户关怀管理", methods = "新增客户关怀")
    public Code addCots(CareEntity careEntity) throws ServiceException {
        if (careEntity.getTbCustomerByCusId().getCusId() == null){
            careEntity.setTbCustomerByCusId(null);
        }
        if (careEntity.getTbUserByUserId().getUserId() == null){
            careEntity.setTbUserByUserId(null);
        }
        if (careEntity.getTbContactsByCotsId().getCotsId() == null){
            careEntity.setTbContactsByCotsId(null);
        }
        return careService.addCots(careEntity);
    }

    @RequestMapping("delete_care")
    @SystemLog(module = "客户关怀管理", methods = "删除客户关怀")
    public Code deleteCots(Integer id) throws ServiceException{
        return careService.deleteCots(id);
    }

    @RequestMapping("update_care")
    @SystemLog(module = "客户关怀管理", methods = "更新客户关怀")
    public Code updateCots(CareEntity careEntity) throws ServiceException{
        if (careEntity.getTbCustomerByCusId().getCusId() == null){
            careEntity.setTbCustomerByCusId(null);
        }
        if (careEntity.getTbUserByUserId().getUserId() == null){
            careEntity.setTbUserByUserId(null);
        }
        if (careEntity.getTbContactsByCotsId().getCotsId() == null){
            careEntity.setTbContactsByCotsId(null);
        }
        System.out.println(careEntity);
        return careService.updateCots(careEntity);
    }

    @RequestMapping("load_care")
    public CareEntity loadCus(Integer id) throws ServiceException{
        CareEntity careEntity = careService.getOneCus(id);
        return careEntity;
    }

    @RequestMapping("care_list")
    public PageEntity<CareVo> getCotsListByPage(CareVo careVo)throws ServiceException {
        System.out.println(careVo.getKid());
        return careService.getCotsList(careVo);
    }

    @RequestMapping("delete_all")
    @SystemLog(module = "客户关怀管理", methods = "删除客户关怀")
    public Code deleteProductAll(@RequestParam(value = "ids[]") Integer[] ids) throws InputException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
                throw new InputException("客户id错误");
        careService.deleteProductAll(ids);
        return Code.success("客户删除成功");
    }
}
