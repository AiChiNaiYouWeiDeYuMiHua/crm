package top.went.controller.hgh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.exception.ServiceException;
import top.went.pojo.ThreeEntity;
import top.went.service.ThreeService;
import top.went.vo.Code;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/three/")
@ResponseBody
public class ThreeController {
    @Autowired
    private ThreeService threeService;

    @RequestMapping("add_three")
    public Code addThree(ThreeEntity threeEntity) {
        try {
            System.out.println(threeEntity);
            return threeService.addThree(threeEntity);
        } catch (ServiceException e) {
            e.printStackTrace();
            return Code.fail();
        }
    }

    @RequestMapping("get_by_cusId")
    public ThreeEntity getSin(Integer id){
        try {
            return threeService.getByCusIdSin(id);
        } catch (ServiceException e) {
            e.printStackTrace();
            return null;
        }
    }


    @GetMapping("customer_statistic")
    public List statisticCus(String state) throws ServiceException {
        List<Map> list = threeService.findAllForStatistic(state);
        return list;
    }

}
