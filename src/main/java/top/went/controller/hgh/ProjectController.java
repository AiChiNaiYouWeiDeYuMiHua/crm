package top.went.controller.hgh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.ServiceException;
import top.went.pojo.ProjectEntity;
import top.went.service.ProjectService;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.ProjectVo;

import java.util.List;

@Controller
@RequestMapping("/project/")
@ResponseBody
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @RequestMapping("add_project")
    @SystemLog(module = "项目管理", methods = "新增项目")
    public Code addCots(ProjectEntity projectEntity) throws ServiceException {
        System.out.println(projectEntity.getProjExpectDate());
        System.out.println(projectEntity.getProjExpectMoney());
        System.out.println(projectEntity.getProjExpectPossibility());
        if (projectEntity.getTbCustomerByCusId().getCusId() == null){
            projectEntity.setTbCustomerByCusId(null);
        }
        System.out.println(1);
        if (projectEntity.getTbUserByUserId().getUserId() == null){
            projectEntity.setTbUserByUserId(null);
        }
        return projectService.addCots(projectEntity);
    }

    @RequestMapping("delete_project")
    @SystemLog(module = "项目管理", methods = "删除项目")
    public Code deleteCots(Integer id) throws ServiceException{
        return projectService.deleteCots(id);
    }

    @RequestMapping("update_project")
    @SystemLog(module = "项目管理", methods = "更新项目")
    public Code updateCots(ProjectEntity projectEntity) throws ServiceException{
        if (projectEntity.getTbCustomerByCusId().getCusId() == null){
            projectEntity.setTbCustomerByCusId(null);
        }
        if (projectEntity.getTbUserByUserId().getUserId() == null){
            projectEntity.setTbUserByUserId(null);
        }
        System.out.println(projectEntity);
        return projectService.updateCots(projectEntity);
    }

    @RequestMapping("load_project")
    public ProjectEntity loadCus(Integer id) throws ServiceException{
        ProjectEntity projectEntity = projectService.getOneCus(id);
        return projectEntity;
    }

    @RequestMapping("project_list")
    public PageEntity<ProjectVo> getCotsListByPage(ProjectVo projectVo)throws ServiceException {
        System.out.println(projectVo.getKid());
        return projectService.getCotsList(projectVo);
    }

    @RequestMapping("delete_all")
    @SystemLog(module = "项目管理", methods = "删除项目")
    public Code deleteProductAll(@RequestParam(value = "ids[]") Integer[] ids) throws InputException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
                throw new InputException("客户id错误");
        projectService.deleteProductAll(ids);
        return Code.success("客户删除成功");
    }

    @RequestMapping("get_project")
    public List<ProjectVo> getProject(Integer id) throws InputException, ServiceException {
        try{
            List<ProjectVo> projectVos =  projectService.getProjByCusId(id);
            return projectVos;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
