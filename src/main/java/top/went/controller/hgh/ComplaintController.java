package top.went.controller.hgh;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.ServiceException;
import top.went.pojo.ComplaintEntity;
import top.went.service.ComplaintService;
import top.went.vo.Code;
import top.went.vo.ComplaintVo;
import top.went.vo.PageEntity;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

@Controller
@RequestMapping("/complaint/")
@ResponseBody
public class ComplaintController {
    @Autowired
    private ComplaintService complaintService;

    @RequestMapping("add_complaint")
    @SystemLog(module = "投诉管理", methods = "新增投诉")
    public Code addCots(ComplaintEntity complaintEntity) throws ServiceException {
        if (complaintEntity.getTbCustomerByCusId().getCusId() == null){
            complaintEntity.setTbCustomerByCusId(null);
        }
        if (complaintEntity.getTbUserByUserId().getUserId() == null){
            complaintEntity.setTbUserByUserId(null);
        }
        if (complaintEntity.getTbContactsByCotsId().getCotsId() == null){
            complaintEntity.setTbContactsByCotsId(null);
        }
        return complaintService.addCots(complaintEntity);
    }

    @RequestMapping("delete_complaint")
    @SystemLog(module = "投诉管理", methods = "删除投诉")
    public Code deleteCots(Integer id) throws ServiceException{
        return complaintService.deleteCots(id);
    }

    @RequestMapping("update_complaint")
    @SystemLog(module = "投诉管理", methods = "更新投诉")
    public Code updateCots(ComplaintEntity complaintEntity) throws ServiceException{
        if (complaintEntity.getTbCustomerByCusId().getCusId() == null){
            complaintEntity.setTbCustomerByCusId(null);
        }
        if (complaintEntity.getTbUserByUserId().getUserId() == null){
            complaintEntity.setTbUserByUserId(null);
        }
        if (complaintEntity.getTbContactsByCotsId().getCotsId() == null){
            complaintEntity.setTbContactsByCotsId(null);
        }
        System.out.println(complaintEntity);
        return complaintService.updateCots(complaintEntity);
    }

    @RequestMapping("load_complaint")
    public ComplaintEntity loadCus(Integer id) throws ServiceException{
        ComplaintEntity serveEntity = complaintService.getOneCus(id);
        return serveEntity;
    }

    @RequestMapping("complaint_list")
    public PageEntity<ComplaintVo> getCotsListByPage(ComplaintVo complaintVo)throws ServiceException {
        System.out.println(complaintVo.getKid());
        return complaintService.getCotsList(complaintVo);
    }

    @RequestMapping("delete_all")
    @SystemLog(module = "投诉管理", methods = "删除投诉")
    public Code  deleteProductAll(@RequestParam(value = "ids[]") Integer[] ids) throws InputException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
                throw new InputException("客户id错误");
        complaintService.deleteProductAll(ids);
        return Code.success("客户删除成功");
    }



}
