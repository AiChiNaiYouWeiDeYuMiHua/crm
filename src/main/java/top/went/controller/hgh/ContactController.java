package top.went.controller.hgh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.ServiceException;
import top.went.pojo.ContactsEntity;
import top.went.service.ContactsService;
import top.went.vo.Code;
import top.went.vo.ContactVo;
import top.went.vo.PageEntity;

import java.rmi.ServerException;
import java.util.*;

@Controller
@RequestMapping("/contact/")
@ResponseBody
public class ContactController {
    @Autowired
    private ContactsService contactsService;

    @RequestMapping("add_contact")
    @SystemLog(module = "联系人管理", methods = "新增联系人")
    public Code addCots(ContactsEntity contactsEntity) throws ServiceException{
        return contactsService.addCots(contactsEntity);
    }

    @RequestMapping("delete_contact")
    @SystemLog(module = "联系人管理", methods = "删除联系人")
    public Code deleteCots(Integer id) throws ServiceException{
        return contactsService.deleteCots(id);
    }

    @RequestMapping("update_contact")
    @SystemLog(module = "联系人管理", methods = "更新联系人")
    public Code updateCots(ContactsEntity contactsEntity) throws ServiceException{
        if(contactsEntity.getTbServeByServeId()==null){
            contactsEntity.setTbServeByServeId(null);
        }
        System.out.println(contactsEntity);
        return contactsService.updateCots(contactsEntity);
    }

    @RequestMapping("load_cots")
    public ContactsEntity loadCus(Integer id) throws ServiceException{
        ContactsEntity contactsEntity = contactsService.getOneCus(id);
        return contactsEntity;
    }

    @RequestMapping("contact_list")
    public PageEntity<ContactVo> getCotsListByPage(ContactVo contactVo)throws ServiceException {
        System.out.println(contactVo.getKid());
        return contactsService.getCotsList(contactVo);
    }

    @RequestMapping("delete_all")
    @SystemLog(module = "联系人管理", methods = "删除联系人")
    public Code  deleteProductAll(@RequestParam(value = "ids[]") Integer[] ids) throws InputException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
                throw new InputException("客户id错误");
        contactsService.deleteProductAll(ids);
        return Code.success("客户删除成功");
    }

    /**
     * 获取地址邮编电话
     * @param id
     * @return
     * @throws ServiceException
     */
    @RequestMapping("get_addrinfo")
    public Map getAddrInfo(Integer id) throws ServiceException {
        Map<String,String> map = contactsService.getAddrInfo(id);
        return map;
    }

}
