package top.went.controller.hgh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.ServiceException;
import top.went.pojo.ContactsEntity;
import top.went.pojo.MemorialDayEntity;
import top.went.service.MemorialService;
import top.went.vo.Code;
import top.went.vo.MemorialVo;
import top.went.vo.PageEntity;
import top.went.vo.ProjectVo;

import java.util.List;

@Controller
@RequestMapping("/memorial/")
@ResponseBody
public class MemorialController {
    @Autowired
    private MemorialService memorialService;

    @RequestMapping("add_memorial")
    @SystemLog(module = "纪念日管理", methods = "新增纪念日")
    public Code addCots(MemorialDayEntity memorialDayEntity) throws ServiceException {
        return memorialService.addCots(memorialDayEntity);
    }

    @RequestMapping("delete_memorial")
    @SystemLog(module = "纪念日管理", methods = "删除纪念日")
    public Code deleteCots(Integer id) throws ServiceException{

        System.out.println(id+"镏金");
        return memorialService.deleteCots(id);
    }

    @RequestMapping("update_memorial")
    @SystemLog(module = "纪念日管理", methods = "更新纪念日")
    public Code updateCots(MemorialDayEntity memorialDayEntity) throws ServiceException{
        if(memorialDayEntity.getTbCustomerByCusId()==null){
            memorialDayEntity.setTbCustomerByCusId(null);
        }
        if(memorialDayEntity.getTbContactsByCotsId()==null){
            memorialDayEntity.setTbContactsByCotsId(null);
        }
        System.out.println(memorialDayEntity);
        return memorialService.updateCots(memorialDayEntity);
    }

    @RequestMapping("load_memorial")
    public MemorialDayEntity loadCus(Integer id) throws ServiceException{
        MemorialDayEntity memorialDayEntity = memorialService.getOneCus(id);
        return memorialDayEntity;
    }

    @RequestMapping("memorial_list")
    public PageEntity<MemorialVo> getCotsListByPage(MemorialVo memorialVo)throws ServiceException {
        System.out.println(memorialVo.getKid());
        return memorialService.getCotsList(memorialVo);
    }

    @RequestMapping("delete_all")
    @SystemLog(module = "纪念日管理", methods = "删除纪念日")
    public Code  deleteProductAll(@RequestParam(value = "ids[]") Integer[] ids) throws InputException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
                throw new InputException("客户id错误");
        memorialService.deleteProductAll(ids);
        return Code.success("客户删除成功");
    }

    @RequestMapping("get_contact")
    public List<MemorialVo> getContact(Integer id) throws InputException, ServiceException {
       try{
           List<MemorialVo> memorialVos =  memorialService.getCotsByCusId(id);
           return memorialVos;
       }catch (Exception e){
           e.printStackTrace();
       }
       return null;
    }

}
