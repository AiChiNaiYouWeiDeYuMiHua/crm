package top.went.controller.hgh;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.went.aop.SystemLog;
import top.went.exception.InputException;
import top.went.exception.ServiceException;
import top.went.pojo.CustomerEntity;
import top.went.service.CustomerService;
import top.went.service.CustomerStatisticService;
import top.went.service.FileService;
import top.went.utils.ExcelImpl;
import top.went.vo.Code;
import top.went.vo.CustomerVo;
import top.went.vo.PageEntity;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/customer/")
@ResponseBody
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerStatisticService customerStatisticService;
    @Autowired
    private FileService fileService;

    @RequestMapping("customer_list")
    public PageEntity<CustomerVo> getCusListByPage(@RequestParam(value = "limit" ,defaultValue = "10") Integer limit,
                                                   @RequestParam(value = "offset",defaultValue = "0") Integer offset) throws ServiceException {

        return customerService.getCusList(limit,offset);
    }

    @PostMapping("add_customer")
    @SystemLog(module = "客户管理", methods = "新增客户")
    public Code addCustomer(CustomerEntity customerEntity) throws ServiceException{
        System.out.println(customerEntity);
        customerEntity.setCusIsDelete(1L);
        return customerService.addCustomer(customerEntity);
    }

    @PostMapping("delete_customer")
    @SystemLog(module = "客户管理", methods = "删除客户")
    public Code deleteCustomer(Integer id) throws ServiceException{
        return customerService.deleteCus(id);
    }

    @PostMapping("update_customer")
    @SystemLog(module = "客户管理", methods = "更新客户")
    public Code updateCustomer(CustomerEntity customerEntity) throws ServiceException{
        return customerService.updateCus(customerEntity);
    }

    @RequestMapping("superQuery")
    public PageInfo superQuery(CustomerEntity customerEntity,@RequestParam(value = "pageSize" ,defaultValue = "5") Integer pageSize,
                               @RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum) throws ServiceException{
        return customerService.fuzzySearch(pageSize,pageNum,customerEntity);
    }

    @RequestMapping("kid_of_find")
    public PageEntity<CustomerEntity> getKidFind(CustomerVo customerVo) throws ServiceException {

            String lifeCycleD;
            if(customerVo.getLifeCycle().trim().equals("全部客户")) {
                lifeCycleD = "";
            }else{
                lifeCycleD = customerVo.getLifeCycle().trim();
            }
            String field = "";
            if(customerVo.getFamily().trim().equals("我的")){
                field = "user_id";
            }else if(customerVo.getFamily().trim().equals("种类")){
                field = "cus_type";
            }else if(customerVo.getFamily().trim().equals("三一客定性")){
                field = "cus_determine";
            }else if(customerVo.getFamily().trim().equals("三一客定级")){
                field = "cus_class";
            }else if(customerVo.getFamily().trim().equals("三一客定量")){
                field = "cus_sign_date";
            }
            customerVo.setField(field);
            customerVo.setCuslifeCycle(lifeCycleD);
            //todo
            return customerService.findAllByKid(customerVo.getLimit(),customerVo.getOffset(),customerVo);
    }

    @RequestMapping("load_cus")
    public CustomerEntity loadCus(Integer id) throws ServiceException{
        CustomerEntity customerEntity = customerService.getOneCus(id);
        return customerEntity;
    }




    @RequestMapping("delete_all")
    @SystemLog(module = "客户管理", methods = "删除客户")
    public Code  deleteProductAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, ServiceException {
        for (int i = 0;i<ids.length;i++)
            if (ids[i] == null || ids[i].toString().length() <=0)
                throw new InputException("客户id错误");
        customerService.deleteProductAll(ids);
        return Code.success("客户删除成功");
    }

    @GetMapping("customer_statistic")
    public List statisticCus(String state) throws ServiceException {
        List<Map> list = customerStatisticService.findAllForStatistic(state);
        return list;
    }

            ExcelImpl excleImpl=new ExcelImpl();
    @RequestMapping(value="download")
    public @ResponseBody String dowm(HttpServletResponse response,String filename){
        response.setContentType("application/binary;charset=UTF-8");
        try{
            ServletOutputStream out=response.getOutputStream();
            try {
                //设置文件头：最后一个参数是设置下载文件名(这里我们叫：张三.pdf)
                response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode("客户录入.xls", "UTF-8"));
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            String[] titles = { "客户名称", "简称", "种类", "地区", "行业", "来源", "网址", "电话", "公司简介", "详细地址", "结款方式", "MSN（QQ）", "备注" };
            excleImpl.export(titles, out);
            return "success";
        } catch(Exception e){
            e.printStackTrace();
            return "导出信息失败";
        }
    }

    @RequestMapping("get_files")
    public String[] getFile(Integer id){
        String fileName[] = customerService.getFiles(id);
        return fileName;
    }

    @RequestMapping("save_three")
    @SystemLog(module = "客户管理", methods = "更新客户")
    public Code saveThree(@RequestParam(value = "three[]")Object[] three){
        Code code =customerService.saveThree(three);
        return code;
    }


}