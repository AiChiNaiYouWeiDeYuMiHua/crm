package top.went.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.OrderEntity;
import top.went.pojo.PayBackDetailEntity;
import top.went.pojo.PlanPayBackEntity;
import top.went.service.OrderService;
import top.went.service.PayBackDetailService;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.PayBackDetailVO;
import top.went.vo.PlanPayBackVO;

import java.util.List;

@Controller
@RequestMapping("/sale")
@ResponseBody
public class PayBackDetailController {
    @Autowired
    private PayBackDetailService payBackDetailService;
    @Autowired
    private OrderService orderService;

    @GetMapping("/mainPayBackDetail1")
    public ModelAndView mainPlanPayBackDetail() {
        return new ModelAndView("sale/MainPayBackDetail");
    }

    @GetMapping("/pbdQueryModal1")
    public ModelAndView modal() {
        return new ModelAndView("sale/modal/pbd_super_query_model");
    }

    @GetMapping("/pbdaddmodal1")
    public ModelAndView addModal(Integer orderId) throws NotFoundException {
        ModelAndView mv = new ModelAndView();
        OrderEntity orderEntity = new OrderEntity();
        if(orderId!=null){
            orderEntity=orderService.load(Long.valueOf(orderId));
            mv.addObject("orderEntity", orderEntity);
        }
        mv.setViewName("sale/modal/add_pay_back_model");
        return mv;
    }

    @GetMapping("/pbddetail1")
    public ModelAndView planPayDetail() {
        return new ModelAndView("sale/payBackDetail");
    }

    /**
     * 根据订单id查找所有回款
     *
     * @param orderId
     * @param
     * @return
     * @throws ServiceException
     */
    @PostMapping("/pbd_findAllByOrderId")
    @ResponseBody
    public List<PayBackDetailEntity> findAllByOrderId(Integer orderId) throws ServiceException {
       PayBackDetailVO payBackDetailVO=new PayBackDetailVO();
       payBackDetailVO.setOrderId(orderId);
        return payBackDetailService.findAllByManyConditions(payBackDetailVO).getRows();
    }
    /**
     * 添加回款
     *
     * @param payBackDetailEntity
     * @return
     * @throws ServiceException
     */
    @PostMapping("/pbdinsert1")
    @ResponseBody
    public Code pbd_insert(PayBackDetailEntity payBackDetailEntity) throws ServiceException {
        payBackDetailEntity.setMagicDelete(0l);
        if (payBackDetailService.pbd_insert(payBackDetailEntity))
            return Code.success("操作成功");
        return Code.fail();
    }

    /**
     * 删除
     *
     * @param pbdId
     * @return
     * @throws ServiceException
     */
    @PostMapping("/pbddelete1")
    @ResponseBody
    public Code pbd_delete(int pbdId) throws ServiceException, NotFoundException {
        payBackDetailService.pbd_delete(pbdId);
                return Code.success("删除回款成功");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     * @throws InputException
     * @throws NotFoundException
     * @throws ServiceException
     */
    @RequestMapping("/pbddeleteall1")
    @ResponseBody
    public Code deleteAll(@RequestParam(value = "ids[]") Long[] ids) throws InputException, NotFoundException, ServiceException {
        for (int i = 0; i < ids.length; i++)
            if (ids[i] == null || ids[i].toString().length() <= 0)
                throw new InputException("回款id错误");
        payBackDetailService.pbd_logicDeleteAll(ids);
        return Code.success("回款批量删除成功");
    }

    /**
     * 查找所有回款
     *
     * @param page
     * @return
     * @throws ServiceException
     */
    @PostMapping("/pbdfindAll1")
    @ResponseBody
    public PageEntity<PayBackDetailVO> ppb_findAll(@RequestBody top.went.vo.Page page) throws ServiceException {
        return payBackDetailService.pbd_findAll(page.getSize(), page.getPage());
    }

    /**
     * 根据日期查找所有回款
     *
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    @PostMapping("/pbd_findAllByDate1")
    @ResponseBody
    public PageEntity<PayBackDetailVO> findAllByDate(Integer pageSize, Integer pageNumber, String pbd_date) throws ServiceException {
        System.out.println("pageSize" + pageSize + " " + "pageNumber" + pageNumber);
        if (null == pageSize || null == pageNumber) {
            pageSize = 10;
            pageNumber = 1;
        }
        return payBackDetailService.pbd_findAllByDate(pbd_date, pageSize, pageNumber);
    }

    /**
     * 加载
     *
     * @param pbdId
     * @return
     */
    @GetMapping("pbdload1")
    @ResponseBody
    public Code load(Integer pbdId) {
        PayBackDetailVO payBackDetailVO = null;
        try {
            payBackDetailVO = payBackDetailService.load1(pbdId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return Code.success(payBackDetailVO);
    }

    /**
     * 加载所有
     *
     * @return
     */
    @RequestMapping("/pbdall")
    public PageEntity<PayBackDetailEntity> loadAll(@RequestBody PayBackDetailVO payBackDetailVO) throws ServiceException {
        System.out.println(payBackDetailVO);
        return payBackDetailService.findAllByManyConditions(payBackDetailVO);
    }

    /**
     * 查看详情
     *
     * @param pbdId
     * @return
     */
    @RequestMapping("/pbddetail1/{pbdId}/")
    public ModelAndView detail(@PathVariable("pbdId") int pbdId) {
        if (new Integer(pbdId) == null) {
            return new ModelAndView("404");
        }
        ModelAndView mv = new ModelAndView("sale/payBackDetail");
        PayBackDetailEntity payBackDetailEntity = null;
        try {
            payBackDetailEntity = payBackDetailService.load(pbdId);
            if (payBackDetailEntity == null)
                return new ModelAndView("404");
        } catch (ServiceException e) {
            e.printStackTrace();
            return new ModelAndView("404");
        }
        mv.addObject("info", payBackDetailEntity);
        return mv;
    }

    /**
     * 通过用户id查询订单
     * @param id
     * @return
     */
    @GetMapping("/queryOrderByCusId")
    public Code queryOrderByCusId(Integer id){
        return Code.success(orderService.loadOrderByus(id.longValue()));
    }
}
