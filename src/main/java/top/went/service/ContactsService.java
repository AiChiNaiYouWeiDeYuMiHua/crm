package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.ContactsDao;
import top.went.db.dao.CustomerDao;
import top.went.db.mapper.ContactMapper;
import top.went.db.mapper.CustomerMapper;
import top.went.exception.ServiceException;
import top.went.pojo.ContactsEntity;
import top.went.pojo.CustomerEntity;
import top.went.utils.Combines;
import top.went.vo.Code;
import top.went.vo.ContactVo;
import top.went.vo.PageEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class ContactsService {
    @Autowired
    private ContactsDao contactsDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private ContactMapper contactMapper;

    public ContactsEntity getOneCus(Integer id ) throws ServiceException {
        try {
            ContactsEntity contactsEntity = contactsDao.findByCusId(id);
            System.out.println("夏洛特烦恼");
            System.out.println(contactsEntity);
            return contactsEntity;
        } catch (Exception e) {
            throw new ServiceException("获取客户异常");
        }
    }


    /**
     * 添加联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code addCots(ContactsEntity contactsEntity) throws ServiceException {
        try {
            if (contactsEntity != null) {
                contactsEntity.setCotsIsDelete(1);
                System.out.println(contactsEntity);
                boolean result = contactsDao.save(contactsEntity) != null;
                if (result == true) {
                    return Code.success("添加联系人成功");
                }
                return Code.fail("添加联系人失败");
            }
            return Code.fail("添加联系人参数错误");
        } catch (Exception e) {
            throw new ServiceException("添加联系人异常");
        }
    }


    /**
     * 逻辑删除联系人
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Code deleteCots(Integer id) throws ServiceException {
        try {
            if (id != null) {
                ContactsEntity customerEntity = contactsDao.findOne(id);
                customerEntity.setCotsIsDelete(0);
                contactsDao.save(customerEntity);
                return Code.success("删除联系人成功");
            }
            return Code.fail("参数错误");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除联系人异常");
        }
    }

    public PageEntity<ContactVo> getCotsList(ContactVo contactVo) throws ServiceException {
        try {
            System.out.println(contactVo.getAll()+"          我市闸炸毁");
            PageHelper.startPage(contactVo.getOffset(), contactVo.getLimit());
            List<ContactsEntity> contactsEntityList = contactMapper.findByParam(contactVo);
            PageEntity pageEntity = new PageEntity(((com.github.pagehelper.Page) contactsEntityList).getTotal(), contactsEntityList);
            return pageEntity;
        } catch (Exception e) {
            throw new ServiceException("获取联系人列表异常");
        }
    }

    /**
     * 修改联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code updateCots(ContactsEntity contactsEntity) throws ServiceException {
        try{
            contactsEntity.setCotsIsDelete(1);
            contactsDao.save(contactsEntity);
            return Code.success("修改联系人成功");
        } catch (Exception e) {
            throw new ServiceException("更新联系人异常");
        }
    }


    public void deleteProductAll(Integer[] ids) throws ServiceException {
        try{
            for (int i=0;i<ids.length;i++){
                ContactsEntity contactsEntity = contactsDao.findOne(ids[i].intValue());
                contactsEntity.setCotsIsDelete(0);
                contactsDao.save(contactsEntity);
            }
        }catch (Exception e){
            throw new ServiceException("删除异常");
        }

    }

    public Map<String,String> getAddrInfo(Integer id){
        ContactsEntity contactsEntity = contactsDao.findByCusId(id);
        String addr = contactsEntity.getTbCustomerByCusId().getCusAddr();
        System.out.println(addr);
        String addrs[] = new String[3];
        Map<String,String> map = new HashMap<>();
        if(addr != null && addr != ""){
            addrs = addr.split("/");
            map.put("addressProvince",addrs[0]);
            map.put("addressCity",addrs[1]);
            map.put("addressCounty",addrs[2]);
        }
        map.put("addressContent",contactsEntity.getTbCustomerByCusId().getCusArea());
        map.put("addressTel",contactsEntity.getCotsPersonalPhone().toString());
        map.put("addressCode",contactsEntity.getCotsZipcode());
        return map;
    }


    public List<ContactsEntity> getCotsListByCusId(Integer id){
        List<ContactsEntity> contactsEntityList = contactsDao.getAllByCusId(id);
        System.out.println(contactsEntityList.size());
        return contactsEntityList;
    }

}
