package top.went.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.DeptDao;
import top.went.db.dao.UserDao;
import top.went.exception.InputException;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.DeptEntity;
import top.went.pojo.UserEntity;
import top.went.utils.Md5;
import top.went.utils.RegularUtils;
import top.went.vo.PageEntity;
import top.went.vo.UserPassword;

import java.util.Date;
import java.util.List;

/**
 * 用户Service
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class UserService {

    private final UserDao userDao;
    private final DeptDao deptDao;

    @Autowired
    public UserService(UserDao userDao, DeptDao deptDao) {
        this.userDao = userDao;
        this.deptDao = deptDao;
    }

    /**
     * 新增用户
     *
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    public boolean addUser(UserEntity userEntity) throws ServiceException {
        try {
            System.out.println("service：" + userEntity);
            userDao.save(userEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("新增用户错误");
        }
    }

    /**
     * 加载用户
     *
     * @param userId
     * @return
     * @throws ServiceException
     */
    public UserEntity load(Long userId) throws ServiceException {
        try {
            UserEntity userEntity1 = userDao.findUserByOne(0L, userId);
            return userEntity1;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载用户错误");
        }
    }

    /**
     * 修改用户
     *
     * @param userEntity
     * @return
     * @throws ServiceException
     * @throws NotFoundException
     */
    public boolean modifyUser(UserEntity userEntity)
            throws ServiceException {
        try {
            userDao.save(userEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("修改用户错误");
        }
    }

    /**
     * 用户离职
     *
     * @param userId
     * @return
     * @throws ServiceException
     * @throws NotFoundException
     */
    public boolean dimissionUser(Long userId) throws ServiceException {
        try {
            UserEntity userEntity1 = userDao.findUserByOne(0L, userId);
            if (userEntity1 == null) {
                return false;
            }
            userEntity1.setUserIsDimission(1L);
            userDao.save(userEntity1);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("用户离职错误");
        }
    }

    /**
     * 删除用户
     *
     * @param userEntity
     * @return
     * @throws ServiceException
     * @throws NotFoundException
     */
    public boolean deleteUser(UserEntity userEntity) throws ServiceException, NotFoundException {
        try {
            UserEntity userEntity1 = userDao.findOne(userEntity.getUserId());
            if (userEntity1 == null) {
                throw new NotFoundException("该用户不存在或已删除");
            }
            userEntity1.setUserIsDimission(2L);
            userDao.save(userEntity1);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除用户错误");
        }
    }

    /**
     * 通过用户名和密码登录
     *
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    public UserEntity loginByPassword(UserEntity userEntity) throws ServiceException {
        try {
            return userDao.loginByPassword(0L, userEntity.getUserName(), userEntity.getUserPassword());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("登录错误");
        }
    }

    /**
     * 修改用户密码
     *
     * @param userPassword
     * @return
     * @throws ServiceException
     * @throws NotFoundException
     */
    public boolean modifyUserPassword(UserPassword userPassword)
            throws ServiceException {
        try {
            UserEntity userEntity = userDao.findUserByOne(0L, userPassword.getUserId());
            userEntity.setUserPassword(Md5.encode(userPassword.getUserPassNew1()));
            userDao.save(userEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("修改密码错误");
        }
    }

    /**
     * 重置用户密码
     *
     * @param userId
     * @return
     * @throws ServiceException
     */
    public boolean resetUserPassword(Long userId) throws ServiceException {
        try {
            UserEntity userEntity1 = userDao.findUserByOne(0L, userId);
            userEntity1.setUserPassword(Md5.encode("aaa123"));
            userDao.save(userEntity1);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("重置密码错误");
        }
    }

    /**
     * 查询所有在职用户
     *
     * @return
     */
    public PageEntity<UserEntity> queryAllUser(Integer pageNumber, Integer pageSize) throws ServiceException {
        try {
            PageRequest pageRequest = new PageRequest(pageNumber, pageSize);

            List<UserEntity> list = userDao.findAllByUserIsDimissionOrderByUserId(0L, pageRequest);
            PageEntity pageEntity = new PageEntity(userDao.countAllByUserIsDimission(0L), list);
            return pageEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询所有在职用户失败");
        }
    }

    /**
     * 根据id查询用户
     *
     * @param userId
     * @return
     * @throws ServiceException
     */
    public UserEntity findUser(Long userId) throws ServiceException {
        try {
            UserEntity userEntity1 = userDao.findUserByOne(0L, userId);
            if (userEntity1 == null) {
                return null;
            }
            return userEntity1;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查找用户错误");
        }
    }

    /**
     * 根据用户姓名查找用户
     *
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    public UserEntity findUserByName(UserEntity userEntity) throws ServiceException {
        try {
            return userDao.queryUserByName(userEntity.getUserName(), 0L);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查找用户错误");
        }
    }

    /**
     * 根据用户姓名模糊查询
     *
     * @param userName
     * @return
     * @throws ServiceException
     */
    public PageEntity<UserEntity> findUserLikeName(String userName, Integer pageNumber, Integer pageSize) throws ServiceException {
        try {
            PageRequest pageRequest = new PageRequest(pageNumber, pageSize);
            List<UserEntity> list = userDao.findUserEntitiesByUserNameLikeAndUserIsDimission("%" + userName + "%", 0L, pageRequest);
            PageEntity pageEntity = new PageEntity(userDao.countAllByUserNameLikeAndUserIsDimission("%" + userName + "%", 0L), list);
            return pageEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("用户模糊查询失败");
        }
    }

    /**
     * 查找指定部门的所有用户
     *
     * @param deptId
     * @return
     * @throws ServiceException
     */
    public PageEntity<UserEntity> findUsersByDept(Long deptId, Integer pageNumber, Integer pageSize) throws ServiceException {
        try {
            PageRequest pageRequest = new PageRequest(pageNumber, pageSize);
            DeptEntity dept = deptDao.findOneById(deptId);
            List<UserEntity> list = userDao.findAllByTbDeptByDeptIdAndUserIsDimission(dept, 0L, pageRequest);
            PageEntity pageEntity = new PageEntity(userDao.countAllByTbDeptByDeptIdAndUserIsDimission(dept, 0L), list);
            return pageEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查找用户失败");
        }
    }
}
