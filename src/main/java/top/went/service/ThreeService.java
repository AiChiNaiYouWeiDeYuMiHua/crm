package top.went.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.ThreeDao;
import top.went.exception.ServiceException;
import top.went.pojo.ThreeEntity;
import top.went.vo.Code;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class ThreeService {
    @Autowired
    private ThreeDao threeDao;

    public Code addThree(ThreeEntity threeEntity) throws ServiceException {
        try {
            if (threeEntity != null) {
                if (threeEntity.getTbCustomerByCusId().getCusId() == null) {
                    threeEntity.setTbCustomerByCusId(null);
                }
                if (threeEntity.getTbUserByUserId().getUserId() == null) {
                    threeEntity.setTbUserByUserId(null);
                }
                java.util.Date udate = new java.util.Date();
                java.sql.Date date = new java.sql.Date(udate.getTime());
                threeEntity.setThreeUpdatatime(date);
                boolean result = threeDao.save(threeEntity) != null;
                if (result == true) {
                    return Code.success();
                }
                return Code.fail();
            }
            return Code.fail();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException();
        }
    }

    public List<ThreeEntity> getThreeListAll(Integer id) throws ServiceException{
        try {
            List<ThreeEntity> threeEntityList = threeDao.findByCusId(id);
            return threeEntityList;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException();
        }
    }


    public ThreeEntity getByCusIdSin(Integer id) throws ServiceException{
        try {
            List<ThreeEntity> threeEntityList = threeDao.findByCusId(id);
            if(threeEntityList.size() != 0){
                ThreeEntity threeEntity = threeEntityList.get(0);
                return threeEntity;
            }else{
                return null;
            }

        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException();
        }

    }



    public List findAllForStatistic(String state) throws ServiceException{
        try{
//            List<Map> type = customerMapper.findForStatistic(state);
//            return type;
            return null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("获取客户数据异常");
        }

    }
}
