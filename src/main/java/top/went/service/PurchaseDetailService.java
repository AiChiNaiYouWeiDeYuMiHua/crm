package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.PurDetailDao;
import top.went.db.mapper.PurchaseDetailMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.MaintainOrderEntity;
import top.went.pojo.PurDetailEntity;
import top.went.pojo.ReturnGoodsDetailEntity;
import top.went.vo.MaintainVO;
import top.went.vo.PageEntity;
import top.went.vo.PurchaseDetailVO;
import top.went.vo.ReturnGoodsDetailVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager",rollbackFor = ServiceException.class)
public class PurchaseDetailService {
    @Autowired
    private PurDetailDao purDetailDao;
    @Autowired
    private PurchaseDetailMapper purchaseDetailMapper;

    private static final Map<String,String> map=new HashMap<>();
    static {
        map.put("pdNum", "pd_num");
        map.put("pdMoney", "pd_money");
        map.put("pdPrice", "pd_price");
    }
    /**
     * 添加采购明细
     * @param purDetailEntity
     * @throws ServiceException
     */
    public boolean pd_insert(PurDetailEntity purDetailEntity)throws ServiceException{
        try {
            if (purDetailEntity.getTbPurchaseByPurId() != null && purDetailEntity.getTbPurchaseByPurId().getPurId()!=null){}
            else
                purDetailEntity.setTbPurchaseByPurId(null);
            if (purDetailEntity.getTbProductFormatByPfId()!= null && purDetailEntity.getTbProductFormatByPfId().getPfId()!=null){}
            else
                purDetailEntity.setTbProductFormatByPfId(null);
            purDetailEntity.setPdWpNum(0);
            purDetailEntity.setMagicDelete(0l);
            return purDetailDao.save(purDetailEntity)!= null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("新建采购明细失败");
        }
    }
    /**
     * 添加采购明细
     * @param purDetailEntity1
     * @throws ServiceException
     */
    public boolean pd_insert1(List<PurDetailEntity> purDetailEntity1)throws ServiceException{
        try {
            for(PurDetailEntity purDetailEntity:purDetailEntity1) {
                if (purDetailEntity.getTbPurchaseByPurId() != null && purDetailEntity.getTbPurchaseByPurId().getPurId() != null) {
                } else
                    purDetailEntity.setTbPurchaseByPurId(null);
                if (purDetailEntity.getTbProductFormatByPfId() != null && purDetailEntity.getTbProductFormatByPfId().getPfId() != null) {
                } else
                    purDetailEntity.setTbProductFormatByPfId(null);
                purDetailEntity.setPdWpNum(0);
                purDetailEntity.setMagicDelete(0l);
            }
            return purDetailDao.save(purDetailEntity1)!= null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("新建采购明细失败");
        }
    }

    /**
     * 删除采购明细
     * @param pdId
     * @throws ServiceException
     */
    public boolean pd_logicDelete(int pdId)throws ServiceException,NotFoundException {
        PurDetailEntity purDetailEntity=load(pdId);
        if(purDetailEntity==null)
            throw new NotFoundException("不存在该采购明细");
        try {
            purDetailEntity.setMagicDelete(1L);
            return purDetailDao.save(purDetailEntity)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw  new ServiceException("删除该采购明细");
        }
    }
    /**
     * 批量删除采购明细
     * @param ids
     * @throws ServiceException
     */
    public boolean pd_logicDeleteAll(Long[] ids)throws ServiceException,NotFoundException {
        for (int i = 0; i < ids.length; i++) {
            pd_logicDelete(ids[i].intValue());
        }
        return true;
    }

    /**
     * 查询采购明细
     * @return
     */
    public PageEntity<PurchaseDetailVO> pd_findAll(Integer pageSize, Integer pageNumber) throws ServiceException{
        System.out.println(pageSize+","+pageNumber);
        try {
            List<PurDetailEntity> rows =purDetailDao.findAllByMagicDeleteOrderByPdId(0l, new PageRequest(pageNumber,pageSize));
            int total=purDetailDao.pd_findAllCount();
            return new PageEntity<PurchaseDetailVO>((long) total, getPurchaseDetailVO(rows));
        }catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询所有采购明细失败");
        }
    }


    /**
     * 高级查询
     * @param purchaseDetailVO
     * @return
     * @throws ServiceException
     */
    public PageEntity<PurDetailEntity> findAllByManyConditions(PurchaseDetailVO purchaseDetailVO) {
        if(purchaseDetailVO==null)
            return new PageEntity<>(0l, null);
        String order =purchaseDetailVO.getPage().getSort(map);
        if(order.length()<=0)
            order = "pd_id desc";
        com.github.pagehelper.Page<PurDetailEntity> page1 = PageHelper.startPage(purchaseDetailVO.getPage().getPage(), purchaseDetailVO.getPage().getSize(), order+" nulls last");
        purchaseDetailMapper.findByManyCondition(purchaseDetailVO);
        return new PageEntity<>(page1.getTotal(), page1.getResult());
    }
    /**
     * 加载采购明细
     * @param pdId
     * @return
     * @throws ServiceException
     */
    public PurchaseDetailVO load1(Integer pdId) throws ServiceException{
        try {
            return getPurchaseDetailVO1(purDetailDao.findAllByMagicDeleteAndPdId(0l, pdId));
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载采购明细失败");
        }
    }
    /**
     * 加载采购明细
     * @param pdId
     * @return
     * @throws ServiceException
     */
    public PurDetailEntity load(Integer pdId) throws ServiceException{
        try {
            return purDetailDao.findAllByMagicDeleteAndPdId(0l, pdId);
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载采购明细");
        }
    }

    private List<PurchaseDetailVO> getPurchaseDetailVO(List<PurDetailEntity> purDetailEntities){
        List<PurchaseDetailVO> purchaseDetailVOS=new ArrayList<>();
        for(PurDetailEntity purDetailEntity:purDetailEntities){
            purchaseDetailVOS.add(new PurchaseDetailVO(purDetailEntity));
        }
        return purchaseDetailVOS;
    }
    private PurchaseDetailVO getPurchaseDetailVO1(PurDetailEntity purDetailEntity){
        PurchaseDetailVO purchaseDetailVO=new PurchaseDetailVO(purDetailEntity);
        return purchaseDetailVO;
    }
}
