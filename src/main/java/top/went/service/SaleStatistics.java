package top.went.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.went.db.dao.*;
import top.went.vo.PaymentRecordVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SaleStatistics {
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private PayBackDetailDao payBackDetailDao;
    @Autowired
    private PlanPayBackDao planPayBackDao;
    @Autowired
    private PlanPayDetailDao planPayDetailDao;
    @Autowired
    private PayRecordsDao payRecordsDao;

    /**
     * 统计合同/订单分布
     * @return
     */
    public List<Map<String,Object>> statisticsOrderType(){
        return arrayToMap(orderDao.statisticsOrderType());
    }
    /**
     * 统计所有者合同/订单分布
     * @return
     */
    public List<Map<String,Object>> statisticsUser(){
        return arrayToMap(orderDao.statisticsUser());
    }
    /**
     * 统计合同/订单状态
     * @return
     */
    public List<Map<String,Object>> statisticsOrderStatus(){
        return arrayToMap(orderDao.statisticsOrderStatus());
    }

    /**
     * 统计合同/订单所有者金额分布
     * @return
     */
    public List<Map<String,Object>> statisticsOrderUserMOney(){
        return arrayToMap(orderDao.statisticsOrderUserMOney());
    }
    /**
     * 统计合同/订单所有者预计毛利分布
     * @return
     */
    public List<Map<String,Object>> statisticsOrderUserfmori(){
        return arrayToMap(orderDao.statisticsOrderUserfmori());
    }
    /**
     * 统计合同/订单所有者毛利分布
     * @return
     */
    public List<Map<String,Object>> statisticsOrderUsermori(){
        return arrayToMap(orderDao.statisticsOrderUsermori());
    }
    /**
     * 统计合同/订单所有者回款分布
     * @return
     */
    public List<Map<String,Object>> statisticsOrderUserreturn(){
        return arrayToMap(orderDao.statisticsOrderUserreturn());
    }
    /**
     * 统计合同/订单类型分布
     * @return
     */
    public List<Map<String,Object>> statisticsOrderCategory(){
        return arrayToMap(orderDao.statisticsOrderCategory());
    }
    /**
     * 统计合同/订单类型金额分布
     * @return
     */
    public List<Map<String,Object>> statisticsOrderCategoryMoney(){
        return arrayToMap(orderDao.statisticsOrderCategoryMoney());
    }
//------------------------------------------------------------------------------
    /**
     * 统计计划回款金额随订单合同分布wsj
     * @return
     */
    public List<Map<String,Object>> ppbdstatisticsOrderCategoryMoney(){
        return arrayToMap(planPayBackDao.pbdstatisticsOrderCategoryMoney());
    }
    /**
     * 根据年统计计划回款金额分布
     * @return
     */
    public List<Map<String,Object>> ppbdstatisticsByYear(){
        return arrayToMap(planPayBackDao.pbdstatisticsByYear());
    }
    /**
     * 根据月统计计划回款金额分布
     * @return
     */
    public List<Map<String,Object>> ppbdstatisticsByMonth(){
        return arrayToMap(planPayBackDao.pbdstatisticsByMonth());
    }
    /**
     * 根据日期统计计划回款金额分布
     * @return
     */
    public List<Map<String,Object>> ppbdstatisticsByDate(){
        return arrayToMap(planPayBackDao.pbdstatisticsByDate());
    }
    /**
     * 根据客户统计计划回款金额分布
     * @return
     */
    public List<Map<String,Object>> ppbdstatisticsByCus(){
        return arrayToMap(planPayBackDao.pbdstatisticsByCus());
    }

    //-----------------------------------------------------------
    /**
     * 统计计划付款金额随采购分布wsj
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsOrderCategoryMoney(){
        return arrayToMap(planPayDetailDao.ppdstatisticsByPur());
    }
    /**
     * 根据年统计计划付款金额分布
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsByYear(){
        return arrayToMap(planPayDetailDao.ppdstatisticsByYear());
    }
    /**
     * 根据月统计计划付款金额分布
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsByMonth(){
        return arrayToMap(planPayDetailDao.ppdstatisticsByMonth());
    }
    /**
     * 根据日期统计计划付款金额分布
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsByDate(){
        return arrayToMap(planPayDetailDao.ppdstatisticsByDate());
    }
    /**
     * 根据客户统计计划付款金额分布
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsByCus(){
        return arrayToMap(planPayDetailDao.ppdstatisticsByCus());
    }

//-------------------------------------------------------------
    /**
     * 统计回款金额随订单合同分布wsj
     * @return
     */
    public List<Map<String,Object>> pbdstatisticsOrderCategoryMoney(){
        return arrayToMap(payBackDetailDao.pbdstatisticsOrderCategoryMoney());
    }
    /**
     * 根据年统计回款金额分布
     * @return
     */
    public List<Map<String,Object>> pbdstatisticsByYear(){
        return arrayToMap(payBackDetailDao.pbdstatisticsByYear());
    }
    /**
     * 根据月统计回款金额分布
     * @return
     */
    public List<Map<String,Object>> pbdstatisticsByMonth(){
        return arrayToMap(payBackDetailDao.pbdstatisticsByMonth());
    }
    /**
     * 根据日期统计回款金额分布
     * @return
     */
    public List<Map<String,Object>> pbdstatisticsByDate(){
        return arrayToMap(payBackDetailDao.pbdstatisticsByDate());
    }
    /**
     * 根据客户统计回款付款金额分布
     * @return
     */
    public List<Map<String,Object>> pbdstatisticsByCus(){
        return arrayToMap(payBackDetailDao.pbdstatisticsByCus());
    }

    /**
     * 统计合同/订单时间分布(最大20个)
     * @return
     */
    public List<Map<String,Object>> statisticsOrderTime(){
        return arrayToMap(orderDao.statisticsOrderTime());
    }
    /**
     * 统计合同/订单金额时间分布(最大20个)
     * @return
     */
    public List<Map<String,Object>> statisticsOrderTimeMoney(){
        return arrayToMap(orderDao.statisticsOrderTimeMoney());
    }
//------------------------------------------------------------
    /**
     * 统计计划付款金额随采购分布wsj
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsOrderCategoryMoney1(){
        return arrayToMap(payRecordsDao.prstatisticsByPur());
    }
    /**
     * 根据年统计计划付款金额分布
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsByYear1(){
        return arrayToMap(payRecordsDao.prstatisticsByYear());
    }
    /**
     * 根据月统计计划付款金额分布
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsByMonth1(){
        return arrayToMap(payRecordsDao.prstatisticsByMonth());
    }
    /**
     * 根据日期统计计划付款金额分布
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsByDate1(){
        return arrayToMap(payRecordsDao.prstatisticsByDate());
    }
    /**
     * 根据客户统计计划付款金额分布
     * @return
     */
    public List<Map<String,Object>> ppdstatisticsByCus1(){
        return arrayToMap(payRecordsDao.prstatisticsByDate());
    }


    private List<Map<String,Object>> arrayToMap(List<Object[]> objects){
        List list = new ArrayList();
        for (Object[] object : objects) {
            Map<String,Object> map = new HashMap<>();
            if (object != null && object.length >=2){
                map.put("TYPE",object[0]);
                map.put("COUNT",object[1]);
            }
            list.add(map);
        }
        if (list.size()<=0){
            Map<String,Object> map = new HashMap<>();
                map.put("TYPE","其他");
                map.put("COUNT",1);
        }
        return list;
    }
}
