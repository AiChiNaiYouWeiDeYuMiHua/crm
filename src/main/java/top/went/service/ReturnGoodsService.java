package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.ReturnGoodsDao;
import top.went.db.mapper.ReturnGoodsMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.*;
import top.went.vo.*;

import java.util.HashMap;
import java.util.Map;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class ReturnGoodsService {
    @Autowired
    private ReturnGoodsDao returnGoodsDao;
    @Autowired
    private ReturnGoodsMapper returnGoodsMapper;
    @Autowired
    private PurchaseService purchaseService;
    /**
     * 表格排序
     */
    private static Map<String, String> map = new HashMap<>();

    static {
        map.put("rgReturnTime", "rg_return_time");
        map.put("planMoney", "plan_money");
        map.put("toMoney", "to_money");
    }

    /**
     * 添加采购退货，或者订单退货
     *
     * @param returnGoodsEntity
     * @return
     * @throws ServiceException
     */
    public Integer rg_insert(ReturnGoodsEntity returnGoodsEntity, String type) throws ServiceException {
        try {
            if (returnGoodsEntity.getTbUserByUserId() == null)
                returnGoodsEntity.setTbUserByUserId(null);
            if (returnGoodsEntity.getTbOrderByOrderId() == null)
                returnGoodsEntity.setTbOrderByOrderId(null);
            if (returnGoodsEntity.getTbPurchaseByPurId() == null || returnGoodsEntity.getTbPurchaseByPurId().getPurId() == null)
                returnGoodsEntity.setTbPurchaseByPurId(null);
            else
                returnGoodsEntity.setWarehouseEntity(purchaseService.load(returnGoodsEntity.getTbPurchaseByPurId().getPurId()).getTbWarehouseByWhId());
            returnGoodsEntity.setMagicDelete(0l);
            returnGoodsEntity.setRgState("待处理");
            returnGoodsEntity.setPutState("未出库");
            returnGoodsEntity.setToMoney((double) 0);
            if(returnGoodsEntity.getPlanMoney()==null)
                returnGoodsEntity.setPlanMoney(0.0);
            returnGoodsEntity.setRgType(Long.valueOf(type));//0表示采购退货，1表示订单退货
            PayBackDetailVO payBackDetailVO = new PayBackDetailVO();
            payBackDetailVO.setRgId(returnGoodsEntity.getRgId());
            returnGoodsEntity.setPayBackState("未退款");
            ReturnGoodsEntity returnGoodsEntity1 = returnGoodsDao.save(returnGoodsEntity);
            System.out.println("添加成功"+returnGoodsEntity1.getRgId());
            return returnGoodsEntity1.getRgId();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("新增退货记录失败！");
        }
    }
    /**
     * 删除退货
     * @param id
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    public boolean rgDelete(int id) throws NotFoundException, ServiceException {
        ReturnGoodsEntity returnGoodsEntity = load(id);
        if(returnGoodsEntity==null)
            throw new NotFoundException("不存在该退货");
        if (!returnGoodsEntity.getRgState().equals("待处理"))
            throw new ServiceException("该退货单已经开始处理");
        try {
            returnGoodsEntity.setMagicDelete(1l);
            return returnGoodsDao.save(returnGoodsEntity)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("删除退货失败");
        }

    }
    /**
     * 加载退货
     * @param id
     * @return
     * @throws NotFoundException
     */
    public ReturnGoodsEntity load(Integer id) throws NotFoundException {
        ReturnGoodsEntity returnGoodsEntity = returnGoodsDao.findOne(0L,id.intValue());
        if (returnGoodsEntity == null)
            throw new NotFoundException("该退货不存在");
        return returnGoodsEntity;
    }
//    /**
//     * 加载退货
//     * @param id
//     * @return
//     * @throws NotFoundException
//     */
//    public ReturnGoodsVO loadBy(Integer id) throws NotFoundException {
//        ReturnGoodsVO returnGoodsVO = returnGoodsMapper.load(Long.valueOf(id));
//        if (returnGoodsVO == null)
//            throw new NotFoundException("该退货不存在");
//        return returnGoodsVO;
//    }
    /**
     * 加载退货
     * @param Id
     * @return
     * @throws ServiceException
     */
    public ReturnGoodsVO loadBy(Integer Id) throws ServiceException {
        try {
            return getReturnGoodsVO1(returnGoodsDao.findAllByMagicDeleteAndRgId(0l, Id));
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载退货单失败");
        }
    }
    /**
     * 修改数据
     * @param returnGoodsEntity
     * @param
     */
    private boolean rg_update(ReturnGoodsEntity returnGoodsEntity) throws NotFoundException, ServiceException {
        ReturnGoodsEntity returnGoodsEntity1 = load(returnGoodsEntity.getRgId());
        if(returnGoodsEntity1==null)
            throw new NotFoundException("该退货不存在");
        returnGoodsEntity1.setRgAccessories(returnGoodsEntity.getRgAccessories());
       returnGoodsEntity1.setRgOddNumbers(returnGoodsEntity.getRgOddNumbers());
       returnGoodsEntity1.setRgReturnTime(returnGoodsEntity.getRgReturnTime());
       returnGoodsEntity1.setRgState(returnGoodsEntity.getRgState());
       returnGoodsEntity1.setRgType(returnGoodsEntity.getRgType());
        try{
            return returnGoodsDao.save(returnGoodsEntity1)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("修改退货失败");
        }
    }
    /**
     * 高级查询
     * @param returnGoodsVO
     * @return
     */
    public PageEntity<ReturnGoodsVO> findAll(ReturnGoodsVO returnGoodsVO) {
        if (returnGoodsVO == null)
            return new PageEntity<>(0l, null);
        String order = returnGoodsVO.getPage().getSort(map);
        if (order.length() <= 0)
            order = "rg_id desc";
        com.github.pagehelper.Page<PlanPayDetailEntity> page1 = PageHelper.startPage(returnGoodsVO.getPage().getPage(), returnGoodsVO.getPage().getSize(), order + " nulls last");
        returnGoodsMapper.findAll(returnGoodsVO);
        return new PageEntity(page1.getTotal(), page1.getResult());
    }


    private ReturnGoodsVO getReturnGoodsVO1(ReturnGoodsEntity returnGoodsEntity) {
        ReturnGoodsVO returnGoodsVO = new ReturnGoodsVO(returnGoodsEntity);
        return null;
    }

    /**
     * 根据订单id查询退货数量
     *
     * @param orderId
     * @return
     */
    public int rgNum(Integer orderId) {
        ReturnGoodsVO returnGoodsVO = new ReturnGoodsVO();
        returnGoodsVO.setOrderId(orderId);
        java.util.List<ReturnGoodsVO> list = findAll(returnGoodsVO).getRows();
        if (list == null) {
            return 0;
        } else
            return list.size();
    }
    /**
     * 根据采购单id查询退货数量
     *
     * @param purId
     * @return
     */
    public int rgNum1(Integer purId) {
        ReturnGoodsVO returnGoodsVO = new ReturnGoodsVO();
        returnGoodsVO.setPurId(purId);
        java.util.List<ReturnGoodsVO> list = findAll(returnGoodsVO).getRows();
        if (list == null) {
            return 0;
        }
        return list.size();
    }

}
