package top.went.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.DimissionDao;
import top.went.db.dao.DimissionTypeDao;
import top.went.exception.ServiceException;
import top.went.pojo.DimissionEntity;
import top.went.pojo.DimissionTypeEntity;
import top.went.vo.PageEntity;

import java.util.List;

/**
 * 系统参数分类service
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class DimissionService {

    @Autowired
    private DimissionTypeDao dimissionTypeDao;

    @Autowired
    private DimissionDao dimissionDao;

    /**
     * 查找指定id参数类型
     *
     * @param typeId
     * @return
     * @throws ServiceException
     */
    public DimissionTypeEntity findTypeByTypeId(Long typeId) throws ServiceException {
        try {
            return dimissionTypeDao.findOne(typeId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询系统参数分类失败");
        }
    }

    /**
     * 查询所有系统参数分类
     *
     * @return
     * @throws ServiceException
     */
    public List<DimissionTypeEntity> findAll() throws ServiceException {
        try {
            return dimissionTypeDao.queryAll();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询系统参数分类失败");
        }
    }

    /**
     * 查找指定id类型的所有系统参数
     *
     * @param dimiTypeId
     * @return
     * @throws ServiceException
     */
    public List<DimissionEntity> queryByType(Long dimiTypeId) throws ServiceException {
        try {
            return dimissionDao.queryByType(dimiTypeId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询系统参数失败");
        }
    }

    public PageEntity<DimissionEntity> findToList(Long dimiTypeId, Integer pageNumber, Integer pageSize)
            throws ServiceException {
        try {
            if (dimiTypeId == null)
                return null;
            DimissionTypeEntity type = dimissionTypeDao.findOne(dimiTypeId);
            PageRequest pageRequest = new PageRequest(pageNumber, pageSize);
            List<DimissionEntity> list = dimissionDao.findAllByTbDimissionTypeByDimiTypeIdAndDimissionIsDel(type, 0L, pageRequest);
            PageEntity pageEntity = new PageEntity(dimissionDao.countAllByTbDimissionTypeByDimiTypeIdAndDimissionIsDel(type, 0L), list);
            return pageEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载错误");
        }
    }

    /**
     * 添加系统参数
     *
     * @param dimissionEntity
     * @return
     * @throws ServiceException
     */
    public boolean insertDimission(DimissionEntity dimissionEntity) throws ServiceException {
        try {
            return dimissionDao.save(dimissionEntity) != null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("添加系统参数失败");
        }
    }

    /**
     * 修改系统参数
     *
     * @param dimissionEntity
     * @return
     * @throws ServiceException
     */
    public boolean modifyDimission(DimissionEntity dimissionEntity) throws ServiceException {
        try {
            return dimissionDao.save(dimissionEntity) != null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("修改系统参数失败");
        }
    }

    /**
     * 删除系统参数
     *
     * @param dimissionId
     * @return
     * @throws ServiceException
     */
    public boolean deleteDimission(Long dimissionId) throws ServiceException {
        try {
            DimissionEntity dimissionEntity = dimissionDao.findOne(dimissionId);
            dimissionEntity.setDimissionIsDel(1L);
            return dimissionDao.save(dimissionEntity) != null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除系统参数失败");
        }
    }

}
