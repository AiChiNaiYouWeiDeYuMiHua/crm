package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.PurchaseReceiptDao;
import top.went.db.dao.ReceiptRecordsDao;
import top.went.db.mapper.PurchaseReceiptMapper;
import top.went.db.mapper.ReceiptRecordsMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.PlanPayDetailEntity;
import top.went.pojo.PurchaseReceiptEntity;
import top.went.pojo.ReceiptRecordsEntity;
import top.went.vo.PageEntity;
import top.went.vo.PurchaseReceiptVO;
import top.went.vo.ReceiptRecordsVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class ReceiptRecordsService {
    @Autowired
    private ReceiptRecordsDao receiptRecordsDao;
    @Autowired
    private ReceiptRecordsMapper receiptRecordsMapper;
    private static final Map<String,String> map = new HashMap<>();

    static {
        map.put("rrMoney","rr_money");
        map.put("rrDate","rr_date");
    }
    /**
     * 添加开票失败
     *
     * @param receiptRecordsEntity
     * @return
     * @throws ServiceException
     */
    public boolean rr_insert(ReceiptRecordsEntity receiptRecordsEntity) throws ServiceException {
        try {
            if(receiptRecordsEntity.getTbPayBackDetailsByRrId()!=null&&receiptRecordsEntity.getTbPayBackDetailsByRrId().getPbdId()!=null){}
            else receiptRecordsEntity.setTbPayBackDetailsByRrId(null);
            if(receiptRecordsEntity.getTbCustomerByCusId()!=null&&receiptRecordsEntity.getTbCustomerByCusId().getCusId()!=null){}
            else receiptRecordsEntity.setTbCustomerByCusId(null);
            if(receiptRecordsEntity.getTbUserByUserId()!=null&&receiptRecordsEntity.getTbUserByUserId().getUserId()!=null){}
            else receiptRecordsEntity.setTbUserByUserId(null);
            if(receiptRecordsEntity.getTbOrderByOrderId()!=null&&receiptRecordsEntity.getTbOrderByOrderId().getOrderId()!=null){}
            else receiptRecordsEntity.setTbOrderByOrderId(null);
            return receiptRecordsDao.save(receiptRecordsEntity)!=null;
        }catch (Exception e){
            throw new ServiceException("新增开票失败");
        }


    }

    /**
     * 删除开票
     * @param rrId
     * @throws ServiceException
     */
    public boolean rr_logicDelete(int rrId)throws ServiceException,NotFoundException {
        ReceiptRecordsEntity receiptRecordsEntity=load(rrId);
        if(receiptRecordsEntity==null)
            throw new NotFoundException("不存在该开票");
        try{
           receiptRecordsEntity.setRrDelete(1l);
            return receiptRecordsDao.save(receiptRecordsEntity)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("删除该开票");
        }
    }
    /**
     * 批量删除开票
     * @param ids
     * @throws ServiceException
     */
    public boolean rr_logicDeleteAll(Long[] ids)throws ServiceException,NotFoundException {
        for (int i = 0; i < ids.length; i++) {
            rr_logicDelete(ids[i].intValue());
        }
        return true;
    }

    /**
     * 查询开票
     * @return
     */
    public PageEntity<ReceiptRecordsVO> rr_findAll(Integer pageSize, Integer pageNumber) throws ServiceException{
        System.out.println(pageSize+","+pageNumber);
        try {
            List<ReceiptRecordsEntity> rows =receiptRecordsDao.findAllByRrDeleteOrderByRrIdDesc(0l, new PageRequest(pageNumber,pageSize));
            int total=receiptRecordsDao.rr_findAllCount();
            return new PageEntity<ReceiptRecordsVO>((long) total, getReceiptRecordsVO(rows));
        }catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询所有开票失败");
        }
    }

    /**
     * 根据日期查询开票
     * @param rr_date
     * @return
     * @throws ServiceException
     */
    public PageEntity<ReceiptRecordsVO> rr_findByPpdDate(Integer pageSize, Integer pageNumber,String rr_date) throws  ServiceException{
        try {
            String rr_date1="%"+rr_date+"%";
            Page<ReceiptRecordsEntity> p=receiptRecordsDao.findAllByDate(rr_date1, new PageRequest(pageNumber-1,pageSize));
            return new PageEntity<ReceiptRecordsVO>(p.getTotalElements(),getReceiptRecordsVO(p.getContent()));
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("根据日期查询开票失败");
        }
    }

    /**
     * 高级查询
     * @param receiptRecordsVO
     * @return
     * @throws ServiceException
     */
    public PageEntity<ReceiptRecordsEntity> findAllByManyConditions(ReceiptRecordsVO receiptRecordsVO) {
        if(receiptRecordsVO==null)
            return new PageEntity<>(0l, null);
        String order=receiptRecordsVO.getPage().getSort(map);
        if(order.length()<=0)
            order="rr_id desc";
        com.github.pagehelper.Page<ReceiptRecordsEntity> page1 = PageHelper.startPage(receiptRecordsVO.getPage().getPage(), receiptRecordsVO.getPage().getSize(), order+" nulls last");
        receiptRecordsMapper.findByManyCondition(receiptRecordsVO);
        return new PageEntity(page1.getTotal(), page1.getResult());
    }
    /**
     * 加载开票
     * @param rrId
     * @return
     * @throws ServiceException
     */
    public ReceiptRecordsVO load1(Integer rrId) throws ServiceException{
        try {
            return getReceiptRecordsVO1(receiptRecordsDao.findAllByRrDeleteAndRrId(0l, rrId)) ;
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载开票失败");
        }
    }
    /**
     * 加载开票
     *
     * @param rrId
     * @return
     * @throws ServiceException
     */
    public ReceiptRecordsEntity load(Integer rrId) throws ServiceException {
        try {
            return receiptRecordsDao.findAllByRrDeleteAndRrId(0l, rrId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载开票失败");
        }
    }
    private List<ReceiptRecordsVO> getReceiptRecordsVO(List<ReceiptRecordsEntity> receiptRecordsEntities){
        List<ReceiptRecordsVO> receiptRecordsVOList=new ArrayList<>();
        for(ReceiptRecordsEntity receiptRecordsEntity:receiptRecordsEntities){
            receiptRecordsVOList.add(new ReceiptRecordsVO(receiptRecordsEntity));
        }
        return receiptRecordsVOList;
    }
    private ReceiptRecordsVO getReceiptRecordsVO1(ReceiptRecordsEntity receiptRecordsEntity){
        ReceiptRecordsVO receiptRecordsVO=new ReceiptRecordsVO(receiptRecordsEntity);
        return receiptRecordsVO;
    }
}
