package top.went.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.CompetitorDao;
import top.went.db.mapper.CompetitorMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.CompetitorEntity;
import top.went.pojo.SaleOppEntity;
import top.went.utils.TrackLog;
import top.went.vo.CompetitorVo;
import top.went.vo.PageEntity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional(rollbackFor = Exception.class)
public class CompetitorService {
    @Autowired
    private CompetitorDao competitorDao;
    @Autowired
    private CompetitorMapper competitorMapper;
    @Autowired
    private SaleOppService saleOppService;

    private static final Map<String,String> map = new HashMap<>();

    static {
        map.put("competitiveAbility","competitive_ability");
        map.put("tbSaleOppByOppId.tbCustomerByCusId.cusName","cus_name");
        map.put("tbSaleOppByOppId.oppTheme","opp_theme");
    }
    /**
     * 新增竞争对手
     * @param competitorEntity
     * @return
     * @throws ServiceException
     */
    public boolean add(CompetitorEntity competitorEntity) throws ServiceException {
        try {
            if(competitorEntity.getTbSaleOppByOppId() == null || competitorEntity.getTbSaleOppByOppId().getOppId() == null){
                competitorEntity.setTbSaleOppByOppId(null);
            } else {
                Long oppId = competitorEntity.getTbSaleOppByOppId().getOppId();
                SaleOppEntity saleOppEntity = saleOppService.load(oppId);
                String trackLog = TrackLog.getTrack(saleOppEntity,"招投标/竞争").toString();
                saleOppEntity.setStage("招投标/竞争");
                saleOppEntity.setStageStartTime(new java.sql.Date(new Date().getTime()));
                saleOppEntity.setTrackLog(trackLog);
                saleOppService.modifySaleOpp(saleOppEntity);
            }
            return competitorDao.save(competitorEntity) != null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("新增失败！");
        }
    }

    /**
     * 修改竞争对手
     * @param competitorEntity
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    public boolean modify(CompetitorEntity competitorEntity) throws NotFoundException, ServiceException {
        CompetitorEntity competitorEntity1 = load(competitorEntity.getCompetitorId());
        if(competitorEntity1 == null){
            throw new NotFoundException("不存在");
        }
        competitorEntity1.setCompany(competitorEntity.getCompany());
        competitorEntity1.setPrice(competitorEntity.getPrice());
        competitorEntity1.setCompetitiveAbility(competitorEntity.getCompetitiveAbility());
        competitorEntity1.setCompetitiveProduct(competitorEntity.getCompetitiveProduct());
        competitorEntity1.setDisadvantage(competitorEntity.getDisadvantage());
        competitorEntity1.setStrategy(competitorEntity.getStrategy());
        competitorEntity1.setRemarks(competitorEntity.getRemarks());
        if(competitorEntity.getTbSaleOppByOppId() != null && competitorEntity.getTbSaleOppByOppId().getOppId() != null){
            competitorEntity1.setTbSaleOppByOppId(competitorEntity.getTbSaleOppByOppId());
        } else {
            competitorEntity1.setTbSaleOppByOppId(null);
        }
        try {
            competitorDao.save(competitorEntity1);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("修改失败！");
        }
    }

    /**
     * 删除机会
     * @param id
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    public boolean delete(Long id) throws NotFoundException, ServiceException {
        CompetitorEntity competitorEntity1 = load(id);
        if(competitorEntity1 == null){
            throw new NotFoundException("不存在");
        }
        competitorEntity1.setIsDelete(1L);
        try {
            competitorDao.save(competitorEntity1);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除失败！");
        }
    }

    /**
     * 编辑删除
     * @param ids
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    public boolean deleteSelected(Long[] ids) throws NotFoundException, ServiceException {
        for(int i=0; i< ids.length; i++){
            delete(ids[i]);
        }
        return true;
    }

    /**
     * 加载竞争对手
     * @param id
     * @return
     * @throws NotFoundException
     */
    public CompetitorEntity load(Long id) throws NotFoundException {
        CompetitorEntity competitorEntity = competitorDao.findAllByCompetitorIdAndIsDelete(id, 0L);
        if (competitorEntity == null)
            throw new NotFoundException("不存在");
        return competitorEntity;
    }

    /**
     * 竞争对手高级查询
     * @param competitorVo
     * @return
     */
    public PageEntity<CompetitorEntity> findAllSeniorSearch(CompetitorVo competitorVo){
        String order = competitorVo.getPage().getSort(map);
        if(order.length() <= 0){
            order = "competitor_id desc";
        }
        Page<CompetitorEntity> page = PageHelper.startPage(competitorVo.getPage().getPage(), competitorVo.getPage().getSize(),order+" nulls last");
        competitorMapper.findAllSeniorSearch(competitorVo);
        return new PageEntity<>(page.getTotal(),page.getResult());
    }

}
