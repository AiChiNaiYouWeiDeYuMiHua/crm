package top.went.service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.PlanPayDetailDao;
import top.went.db.dao.PurchaseDao;
import top.went.db.mapper.PlanPaydetailMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.PlanPayDetailEntity;
import top.went.pojo.PurchaseEntity;
import top.went.vo.PageEntity;
import top.went.vo.PlanPayDetailVO;
import top.went.vo.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager",rollbackFor = ServiceException.class)
public class PlanPayDetailService {
    @Autowired
    private PlanPayDetailDao planPayDetailDao;
    @Autowired
    private PlanPaydetailMapper planPaydetailMapper;
    @Autowired
    private PurchaseService purchaseService;

    private static final Map<String,String> map = new HashMap<>();

    static {
        map.put("ppdMoney","ppd_money");
        map.put("ppdDate","ppd_date");
        map.put("ppdTerms","ppd_terms");
    }

    /**
     * 添加计划付款
     * @param planPayDetailEntity
     * @throws ServiceException
     */
    public boolean ppd_insert(PlanPayDetailEntity planPayDetailEntity)throws ServiceException{
        try {
            if (planPayDetailEntity.getTbPurchaseByPurId() != null && planPayDetailEntity.getTbPurchaseByPurId().getPurId()!=null){
                System.out.println("================================"+planPayDetailEntity.getTbPurchaseByPurId().getPurId());
                PurchaseEntity purchaseEntity=purchaseService.load(planPayDetailEntity.getTbPurchaseByPurId().getPurId());
                System.out.println("+++++++++++++++++++++++++++++++"+purchaseEntity.getPurId());
                planPayDetailEntity.setTbPurchaseByPurId(purchaseEntity);
            }
            else{
                planPayDetailEntity.setTbPurchaseByPurId(null);
                System.out.println("***********************");
            }
            if (planPayDetailEntity.getTbCustomerByCusId() != null && planPayDetailEntity.getTbCustomerByCusId().getCusId()!=null){}
            else
                planPayDetailEntity.setTbCustomerByCusId(null);
            if (planPayDetailEntity.getTbUserByUserId() != null && planPayDetailEntity.getTbUserByUserId().getUserId()!=null){}
            else
                planPayDetailEntity.setTbUserByUserId(null);
            return planPayDetailDao.save(planPayDetailEntity)!= null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("新建计划付款失败");
        }
    }

    /**
     * 删除计划付款
     * @param ppdId
     * @throws ServiceException
     */
    public boolean ppd_logicDelete(int ppdId)throws ServiceException,NotFoundException{
       PlanPayDetailEntity planPayDetailEntity=load(ppdId);
       if(planPayDetailEntity==null)
           throw new NotFoundException("不存在该计划付款");
       try {
           planPayDetailEntity.setMagicDelete(1l);
           return planPayDetailDao.save(planPayDetailEntity)!=null;
       }catch (Exception e){
           e.printStackTrace();
           throw  new ServiceException("删除计划回款");
       }
    }
    /**
     * 批量删除计划付款
     * @param ids
     * @throws ServiceException
     */
    public boolean ppd_logicDeleteAll(Long[] ids)throws ServiceException,NotFoundException {
        for (int i = 0; i < ids.length; i++) {
            ppd_logicDelete(ids[i].intValue());
        }
        return true;
    }
    /**
     /**
     * 修改计划付款
     * @param planPayDetailEntity
     * @throws ServiceException
     */
    public boolean ppd_update(PlanPayDetailEntity planPayDetailEntity)throws ServiceException{
        PlanPayDetailEntity planPayDetailEntity1=load(planPayDetailEntity.getPpdId());
        planPayDetailEntity1.setPpdMoney(planPayDetailEntity.getPpdMoney());
        planPayDetailEntity1.setPpdDate(planPayDetailEntity.getPpdDate());
        planPayDetailEntity1.setPpdTerms(planPayDetailEntity.getPpdTerms());
        planPayDetailEntity1.setPpdState(planPayDetailEntity.getPpdState());
        try {
            return  planPayDetailDao.save(planPayDetailEntity1)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("修改计划付款失败");
        }

    }

    /**
     * 查询所有计划付款
     * @return
     */
    public PageEntity<PlanPayDetailVO>  ppd_findAll( Integer pageSize, Integer pageNumber) throws ServiceException{
        System.out.println(pageSize+","+pageNumber);
        try {
            List<PlanPayDetailEntity> rows =planPayDetailDao.findAllByMagicDeleteOrderByPpdIdDesc(0l, new PageRequest(pageNumber,pageSize));
            int total=planPayDetailDao.ppd_findAllCount();
            return new PageEntity<PlanPayDetailVO>((long) total, getPlanPayDetailVO(rows));

        }catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询所有计划付款失败");
        }
    }
    /**
     * 根据计划付款日期查询计划付款
     * @param ppd_date
     * @return
     * @throws ServiceException
     */
    public PageEntity<PlanPayDetailVO> ppd_findByPpdDate(Integer pageSize, Integer pageNumber,String ppd_date) throws  ServiceException{
        try {
            String ppd_date1="%"+ppd_date+"%";
            Page<PlanPayDetailEntity> p=planPayDetailDao.findAllByDate(ppd_date1, new PageRequest(pageNumber-1,pageSize));
            return new PageEntity<PlanPayDetailVO>(p.getTotalElements(),getPlanPayDetailVO(p.getContent()));
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("根据计划付款日期查询计划付款失败");
        }
    }

    /**
     * 根据id查询计划付款实体
     * @param ppdId
     * @return
     * @throws ServiceException
     * @throws NotFoundException
     */
    public PlanPayDetailEntity findOne(int ppdId) throws ServiceException,NotFoundException{
        PlanPayDetailEntity planPayDetailEntity=planPayDetailDao.findOne(ppdId);
        if(planPayDetailEntity==null)
            throw new NotFoundException("计划付款不存在");
        return planPayDetailEntity;
    }

//    /**
//     * 高级查询
//     * @param planPayDetailVO
//     * @return
//     * @throws ServiceException
//     */
//    public List<PlanPayDetailEntity> findAllByManyConditions(PlanPayDetailVO planPayDetailVO) throws  ServiceException{
//        try {
//            List<PlanPayDetailEntity> list = planPaydetailMapper.findByManyCondition(planPayDetailVO);
////           int total=payRecordsMapper.findByManyConditionintCount(paymentRecordVO);
//            return list;
//        }catch (Exception e){
//            e.printStackTrace();
//            throw new ServiceException("查询失败");
//        }
//    }
    /**
     * 高级查询
     * @param planPayDetailVO
     * @return
     * @throws ServiceException
     */
    public PageEntity<PlanPayDetailEntity> findAllByManyConditions(PlanPayDetailVO planPayDetailVO) {
            if(planPayDetailVO==null)
                return new PageEntity<>(0l, null);
                String order =planPayDetailVO.getPage().getSort(map);
                if(order.length()<=0)
                    order = "ppd_id desc";
                    com.github.pagehelper.Page<PlanPayDetailEntity> page1 = PageHelper.startPage(planPayDetailVO.getPage().getPage(), planPayDetailVO.getPage().getSize(), order+" nulls last");
                    planPaydetailMapper.findByManyCondition(planPayDetailVO);
                    return new PageEntity<>(page1.getTotal(), page1.getResult());
    }
    /**
     * 加载计划付款
     * @param ppdId
     * @return
     * @throws ServiceException
     */
    public PlanPayDetailVO load1(Integer ppdId) throws ServiceException{
        try {
            return getPlanPayDetailVO1(planPayDetailDao.findAllByMagicDeleteAndPpdId(0l, ppdId)) ;
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载计划付款失败");
        }
    }
    /**
     * 加载计划付款
     * @param ppdId
     * @return
     * @throws ServiceException
     */
    public PlanPayDetailEntity load(Integer ppdId) throws ServiceException{
        try {
            return planPayDetailDao.findAllByMagicDeleteAndPpdId(0l, ppdId);
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载计划付款失败");
        }
    }

    private List<PlanPayDetailVO> getPlanPayDetailVO(List<PlanPayDetailEntity> planPayDetailEntities){
        List<PlanPayDetailVO> planPayDetailVOS=new ArrayList<>();
        for(PlanPayDetailEntity planPayDetailEntity:planPayDetailEntities){
            planPayDetailVOS.add(new PlanPayDetailVO(planPayDetailEntity));
        }
        return planPayDetailVOS;
    }
    private PlanPayDetailVO getPlanPayDetailVO1(PlanPayDetailEntity planPayDetailEntity){
        PlanPayDetailVO planPayDetailVO=new PlanPayDetailVO(planPayDetailEntity);
        return planPayDetailVO;
    }
}
