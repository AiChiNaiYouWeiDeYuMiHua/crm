package top.went.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.QuoteDetailDao;
import top.went.db.mapper.QuoteDetailMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.QuoteDetailEntity;
import top.went.pojo.QuoteEntity;
import top.went.vo.PageEntity;
import top.went.vo.QuoteDetail;
import top.went.vo.QuoteDetailVo;

import javax.sql.rowset.serial.SerialException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 报价明细
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class QuoteDetailService {
    @Autowired
    private QuoteDetailDao quoteDetailDao;
    @Autowired
    private QuoteDetailMapper quoteDetailMapper;

    private static final Map<String,String> map = new HashMap<>();

    static {
        map.put("qdId","qd_id");
        map.put("amount","amount");
        map.put("unitPrice","unit_price");
        map.put("money","money");
        map.put("quoteId","quote_id");
    }

    /**
     * 新增报价明细
     * @param quoteDetailEntity
     * @return
     */
    public boolean add(QuoteDetailEntity quoteDetailEntity) throws ServiceException {
        if(quoteDetailEntity.getTbQuoteByQuoteId() == null || quoteDetailEntity.getTbQuoteByQuoteId().getQuoteId() == null){
            quoteDetailEntity.setTbQuoteByQuoteId(null);
        }
        if(quoteDetailEntity.getTbProductFormatByPfId() == null || quoteDetailEntity.getTbProductFormatByPfId().getPfId() == null){
            quoteDetailEntity.setTbQuoteByQuoteId(null);
        }
        quoteDetailEntity.setIsDelete(0L);
        try {
            return quoteDetailDao.save(quoteDetailEntity) != null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("新增失败！");
        }
    }

    /**
     * 删除报价明细
     * @param id
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    public boolean delete(Long id) throws NotFoundException, ServiceException {
        QuoteDetailEntity quoteDetailEntity = load(id);
        quoteDetailEntity.setIsDelete(1L);
        try {
            quoteDetailDao.save(quoteDetailEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除失败！");
        }
    }

    /**
     * 根据报价单id删除报价明细（物理删除）
     * @param id
     * @return
     * @throws ServiceException
     */
    public boolean deleteTrue(Long id) throws ServiceException {
        try {
            List<QuoteDetailEntity> quoteDetailEntities = quoteDetailDao.findAll(id);
            quoteDetailDao.delete(quoteDetailEntities);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除失败！");
        }
    }


    /**
     * 加载报价明细
     * @param id
     * @return
     * @throws NotFoundException
     */
    public QuoteDetailEntity load(Long id) throws NotFoundException {
        QuoteDetailEntity quoteDetailEntity = quoteDetailDao.findAllByQdIdAndIsDelete(id, 0L);
        if(quoteDetailEntity == null){
            throw new NotFoundException("id不存在！");
        }
        return quoteDetailEntity;
    }

    /**
     * 报价明细高级查询
     * @param quoteDetailVo
     * @return
     */
    public PageEntity<QuoteDetailEntity> findAllSeniorSearch(QuoteDetailVo quoteDetailVo){
        String order = quoteDetailVo.getPage().getSort(map);
        if(order.length() <= 0){
            order = "qd_id desc";
        }
        Page<QuoteDetailEntity> page = PageHelper.startPage(quoteDetailVo.getPage().getPage(), quoteDetailVo.getPage().getSize(), order+" nulls last");
        quoteDetailMapper.findAllSeniorSearch(quoteDetailVo);
        return new PageEntity<>(page.getTotal(),page.getResult());
    }

}
