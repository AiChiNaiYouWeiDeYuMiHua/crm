package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.PayBackDetailDao;
import top.went.db.dao.PlanPayBackDao;
import top.went.db.mapper.PayBackDetailMapper;
import top.went.db.mapper.PlanPayBackMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.CustomerEntity;
import top.went.pojo.PayBackDetailEntity;
import top.went.pojo.PlanPayBackEntity;
import top.went.pojo.PlanPayDetailEntity;
import top.went.vo.PageEntity;
import top.went.vo.PayBackDetailVO;
import top.went.vo.PlanPayBackVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager",rollbackFor = ServiceException.class)
public class PayBackDetailService {
    @Autowired
    private PayBackDetailDao payBackDetailDao;
    @Autowired
    private PayBackDetailMapper payBackDetailMapper;
    private static final Map<String,String> map = new HashMap<>();

    static {
        map.put("pbdMoney","pbd_money");
        map.put("pbdDate","pbd_date");
        map.put("pbdTerms","pbd_terms");
    }
    /**
     * 添加
     * @param payBackDetailEntity
     * @return
     * @throws ServiceException
     */
    public boolean pbd_insert(PayBackDetailEntity payBackDetailEntity) throws ServiceException{
        try{
            if(payBackDetailEntity.getTbUserByUserId()!=null&&payBackDetailEntity.getTbUserByUserId().getUserId()!=null){ }
            else
                payBackDetailEntity.setTbUserByUserId(null);
            if(payBackDetailEntity.getTbMaintainOrderByMtId()!=null&&payBackDetailEntity.getTbMaintainOrderByMtId().getMtId()!=null){}
            else
                payBackDetailEntity.setTbMaintainOrderByMtId(null);
            if(payBackDetailEntity.getTbOrderByOrderId()!=null&&payBackDetailEntity.getTbOrderByOrderId().getOrderId()!=null){}
            else
                payBackDetailEntity.setTbOrderByOrderId(null);
            if(payBackDetailEntity.getTbPlanPayBackByPpbId()!=null&&payBackDetailEntity.getTbPlanPayBackByPpbId().getPpbId()!=null){}
            else
                payBackDetailEntity.setTbPlanPayBackByPpbId(null);
            if(payBackDetailEntity.getTbCustomerByCusId()!=null&&payBackDetailEntity.getTbCustomerByCusId().getCusId()!=null){}
            else payBackDetailEntity.setTbCustomerByCusId(null);
            return payBackDetailDao.save(payBackDetailEntity)!=null;
        }catch (Exception e){
            throw new ServiceException("新建回款失败！");
        }
    }

    /**
     * 删除计划回款
     * @param pbd_id
     * @return
     * @throws ServiceException
     */
    public boolean pbd_delete(int pbd_id) throws ServiceException,NotFoundException {
        PayBackDetailEntity payBackDetailEntity=load(pbd_id);
        if(payBackDetailEntity==null)
            throw new NotFoundException("不存在该回款");
        try {
            payBackDetailEntity.setMagicDelete(1l);
            return payBackDetailDao.save(payBackDetailEntity)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("删除回款失败");
        }

    }
    /**
     * 批量删除回款
     * @param ids
     * @throws ServiceException
     */
    public boolean pbd_logicDeleteAll(Long[] ids)throws ServiceException,NotFoundException {
        for (int i = 0; i < ids.length; i++) {
            pbd_delete(ids[i].intValue());
        }
        return true;
    }
    /**
     * 修改回款
     * @param payBackDetailEntity
     * @return
     * @throws ServiceException
     */
    public boolean pbd_update(PayBackDetailEntity payBackDetailEntity)throws ServiceException,NotFoundException{
        PayBackDetailEntity payBackDetailEntity1= load(((PayBackDetailEntity) payBackDetailEntity).getPbdId());
        if(payBackDetailEntity1==null)
            throw new NotFoundException("回款不存在");
        payBackDetailEntity1.setPbdMoney(payBackDetailEntity.getPbdMoney());
        payBackDetailEntity1.setPbdDate(payBackDetailEntity.getPbdDate());
         payBackDetailEntity1.setPbdTerms(payBackDetailEntity.getPbdTerms());
        payBackDetailEntity1.setPbdTerms(payBackDetailEntity.getPbdTerms());
        try{
            return payBackDetailDao.save(payBackDetailEntity1)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("修改回款失败");
        }
    }

    /**
     * 查询所有回款
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    public PageEntity<PayBackDetailVO> pbd_findAll(Integer pageSize, Integer pageNumber) throws ServiceException{
        try {
            List<PayBackDetailEntity> rows =payBackDetailDao.findAllByMagicDeleteOrderByPbdId(0l, new PageRequest(pageNumber-1,pageSize));
            int total=payBackDetailDao.pbd_findAllCount();
            return new PageEntity<PayBackDetailVO>((long) total, getPayBackDetailVos(rows));
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("查询所有回款失败");
        }
    }

    /**
     * 根据回款日期查询
     * @param date
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    public PageEntity<PayBackDetailVO> pbd_findAllByDate (String date,Integer pageSize, Integer pageNumber) throws ServiceException{
        try {
            String date1= "%"+date+"%";
            Page<PayBackDetailEntity> p = payBackDetailDao.findAllByDate(date1,new PageRequest(pageNumber-1,pageSize));
            return new PageEntity<>(p.getTotalElements(),getPayBackDetailVos(p.getContent()));
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("根据回款日期查询失败");
        }

    }


    /**
     * 高级查询
     * @param payBackDetailVO
     * @return
     * @throws ServiceException
     */
    public PageEntity<PayBackDetailEntity> findAllByManyConditions(PayBackDetailVO payBackDetailVO) throws  ServiceException{
        if(payBackDetailVO==null)
            return new PageEntity<>(0l, null);
        String order =payBackDetailVO.getPage().getSort(map);
        if(order.length()<=0)
            order = "pbd_id desc";
        com.github.pagehelper.Page<PayBackDetailEntity> page1 = PageHelper.startPage(payBackDetailVO.getPage().getPage(), payBackDetailVO.getPage().getSize(), order+" nulls last");
        payBackDetailMapper.findByManyCondition(payBackDetailVO);
        return new PageEntity(page1.getTotal(), page1.getResult());

    }
    /**
     * 加载回款
     * @param pbdId
     * @return
     * @throws ServiceException
     */
    public PayBackDetailEntity load(Integer pbdId) throws  ServiceException{
        try {
            return payBackDetailDao.findAllByMagicDeleteAndPbdId(0l, pbdId);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("加载回款记录失败");
        }
    }
    /**
     * 加载回款
     * @param pbdId
     * @return
     * @throws ServiceException
     */
    public PayBackDetailVO load1(Integer pbdId) throws ServiceException {
        try {
            return getPayBackDetailVos1(payBackDetailDao.findAllByMagicDeleteAndPbdId(0l, pbdId)) ;
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载回款失败");
        }
    }
    private List<PayBackDetailVO> getPayBackDetailVos(List<PayBackDetailEntity> payBackDetailEntities) {
        List<PayBackDetailVO> payBackDetailVOS = new ArrayList<>();
        for (PayBackDetailEntity payBackDetailEntity : payBackDetailEntities){
            payBackDetailVOS.add(new PayBackDetailVO(payBackDetailEntity));
        }
        return payBackDetailVOS;
    }
    private PayBackDetailVO getPayBackDetailVos1(PayBackDetailEntity payBackDetailEntity){
        PayBackDetailVO payBackDetailVO=new PayBackDetailVO(payBackDetailEntity);
        return payBackDetailVO;
    }
}