package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.CustomerDao;
import top.went.db.dao.MemorialDao;
import top.went.db.mapper.MemorialMapper;
import top.went.exception.ServiceException;
import top.went.pojo.ContactsEntity;
import top.went.pojo.CustomerEntity;
import top.went.pojo.MemorialDayEntity;
import top.went.vo.Code;
import top.went.vo.MemorialVo;
import top.went.vo.PageEntity;
import top.went.vo.ProjectVo;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class MemorialService {
    @Autowired
    private MemorialDao memorialDao;
    @Autowired
    private MemorialMapper memorialMapper;
    @Autowired
    private CustomerDao customerDao;

    public MemorialDayEntity getOneCus(Integer id) throws ServiceException {
        try {
            MemorialDayEntity memorialDayEntity = memorialDao.findByMedId(id);
            System.out.println(memorialDayEntity);
            return memorialDayEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("获取客户异常");
        }
    }

    public List<MemorialVo> getCotsByCusId(Integer id) throws ServiceException{
        try{
            List<MemorialVo> memorialVos = memorialMapper.find(id);
            return memorialVos;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("获取联系人错误");
        }
    }

    public Code addCots(MemorialDayEntity memorialDayEntity) throws ServiceException {
        try {
            if(memorialDayEntity.getTbContactsByCotsId() == null){
                memorialDayEntity.setTbContactsByCotsId(null);
            }
            if(memorialDayEntity.getTbCustomerByCusId() == null){
                memorialDayEntity.setTbCustomerByCusId(null);
            }
            if (memorialDayEntity != null) {
                memorialDayEntity.setMedIsDelete(1);
                java.util.Date utilDate=new Date();
                java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());
                memorialDayEntity.setMedCreatetimr(sqlDate);
                System.out.println("当前时间"+utilDate);
                java.sql.Date date=memorialDayEntity.getMedMemorialDate();
                java.util.Date d=new java.util.Date (date.getTime());
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                c.add(Calendar.YEAR, 1);
                utilDate = c.getTime();
                System.out.println("下次时间"+utilDate);
                java.sql.Date sqlDateNext=new java.sql.Date(utilDate.getTime());
                memorialDayEntity.setMedNextPoint(sqlDateNext);
                System.out.println(memorialDayEntity.getMedCreatetimr());
                boolean result = memorialDao.save(memorialDayEntity) != null;
                if (result == true) {
                    return Code.success("添加纪念日成功");
                }
                return Code.fail("添加纪念日失败");
            }
            return Code.fail("添加纪念日参数错误");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("添加纪念日异常");
        }
    }


    /**
     * 逻辑删除联系人
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Code deleteCots(Integer id) throws ServiceException {
        try {
            if (id != null) {
                MemorialDayEntity memorialDayEntity = memorialDao.findOne(id);
                memorialDayEntity.setMedIsDelete(0);
                memorialDao.save(memorialDayEntity);
                return Code.success("删除联系人成功");
            }
            return Code.fail("参数错误");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除联系人异常");
        }
    }

    public PageEntity<MemorialVo> getCotsList(MemorialVo memorialVo) throws ServiceException {
        try {
            System.out.println(memorialVo.getAll() + "          我市闸炸毁");
            PageHelper.startPage(memorialVo.getOffset(), memorialVo.getLimit());
            List<MemorialDayEntity> memorialDayEntityList = memorialMapper.findByParam(memorialVo);
            PageEntity pageEntity = new PageEntity(((com.github.pagehelper.Page) memorialDayEntityList).getTotal(), memorialDayEntityList);
            return pageEntity;
        } catch (Exception e) {
            throw new ServiceException("获取联系人列表异常");
        }
    }

    /**
     * 修改联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code updateCots(MemorialDayEntity memorialDayEntity) throws ServiceException {
        try {
            memorialDayEntity.setMedIsDelete(1);
            java.util.Date utilDate=new Date();
            java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());
            memorialDayEntity.setMedCreatetimr(sqlDate);
            System.out.println("当前时间"+utilDate);
            java.sql.Date date=memorialDayEntity.getMedMemorialDate();
            java.util.Date d=new java.util.Date (date.getTime());
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            c.add(Calendar.YEAR, 1);
            utilDate = c.getTime();
            System.out.println("下次时间"+utilDate);
            java.sql.Date sqlDateNext=new java.sql.Date(utilDate.getTime());
            memorialDayEntity.setMedNextPoint(sqlDateNext);
            System.out.println(memorialDayEntity.getMedCreatetimr());
            boolean result = memorialDao.save(memorialDayEntity) != null;
            memorialDao.save(memorialDayEntity);
            return Code.success("修改纪念日成功");
        } catch (Exception e) {
            throw new ServiceException("更新纪念日异常");
        }
    }


    public void deleteProductAll(Integer[] ids) throws ServiceException {
        try {
            for (int i = 0; i < ids.length; i++) {
                MemorialDayEntity memorialDayEntity = memorialDao.findOne(ids[i].intValue());
                memorialDayEntity.setMedIsDelete(0);
                memorialDao.save(memorialDayEntity);
            }
        } catch (Exception e) {
            throw new ServiceException("删除异常");
        }

    }

    public List<MemorialDayEntity> getMedListAll(Integer id) throws ServiceException{
        try {
            List<MemorialDayEntity> threeEntityList = memorialDao.findAllByCusId(id);
            return threeEntityList;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException();
        }
    }

}
