package top.went.service;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import top.went.db.dao.CustomerDao;
import top.went.exception.ServiceException;
import top.went.pojo.CustomerEntity;
import top.went.vo.Code;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.List;
import java.util.UUID;

@Service
public class FileService {

//    private Logger logger = LoggerFactory.getLogger(FileService.class);
    @Autowired
    private CustomerExcelInputService customerExcelInputService;
    @Autowired
    private CustomerDao customerDao;
    /**
     * 把上传时候的文件名返回
     *
     * @return
     */
    public String upload(MultipartFile file, String path) throws ServiceException {
        String fileName = file.getOriginalFilename();//拿到文件名
        String fileExtensionName = fileName.substring(fileName.lastIndexOf(".") + 1);//获取扩展名,不要点
        String updateFileName = UUID.randomUUID().toString() + "." + fileExtensionName;//上传文件名字,为了保证每个人发送同名文件不被覆盖
//            logger.info("开始上传文件，上传文件的文件名:{},上传的路径:{},新文件名:{}", fileName, path, updateFileName);

        File fileDir = new File(path);//创建目录file
        if (!fileDir.exists()) {
            fileDir.setWritable(true);//赋予文件权限,可写
            fileDir.mkdirs();//获取能在webapp文件下创建文件加的权限,因为发布完后可以改，mkdir是当前级别的，mkdirs是/a/b这样的文件夹创建
        }
        File targetFile = new File(path, updateFileName);//完整的file，包括路径文件名和扩展名
        try {
            file.transferTo(targetFile);
            File file1 = (File) file;
            FileOutputStream fos = new FileOutputStream(targetFile);
            InputStream input = new FileInputStream(targetFile);
            // 一次30kb
            byte[] readBuff = new byte[1024 * 30];
            int count = -1;
            while ((count = input.read(readBuff, 0, readBuff.length)) != -1) {
                fos.write(readBuff, 0, count);
            }
            fos.flush();
            fos.close();
            input.close();

            return targetFile.getName();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return targetFile.getName();
        }
    }


    public Code excelupload(MultipartFile file, String path) throws ServiceException {
        String fileName = file.getOriginalFilename();//拿到文件名
        String fileExtensionName = fileName.substring(fileName.lastIndexOf(".") + 1);//获取扩展名,不要点
        String updateFileName = UUID.randomUUID().toString() + "." + fileExtensionName;//上传文件名字,为了保证每个人发送同名文件不被覆盖
//            logger.info("开始上传文件，上传文件的文件名:{},上传的路径:{},新文件名:{}", fileName, path, updateFileName);

        File fileDir = new File(path);//创建目录file
        if (!fileDir.exists()) {
            fileDir.setWritable(true);//赋予文件权限,可写
            fileDir.mkdirs();//获取能在webapp文件下创建文件加的权限,因为发布完后可以改，mkdir是当前级别的，mkdirs是/a/b这样的文件夹创建
        }
        File targetFile = new File(path, updateFileName);//完整的file，包括路径文件名和扩展名
        try {
            file.transferTo(targetFile);
            File file1 = (File) file;
            FileOutputStream fos = new FileOutputStream(targetFile);
            InputStream input = new FileInputStream(targetFile);
            // 一次30kb
            byte[] readBuff = new byte[1024 * 30];
            int count = -1;
            while ((count = input.read(readBuff, 0, readBuff.length)) != -1) {
                fos.write(readBuff, 0, count);
            }
            fos.flush();
            fos.close();
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Code code = customerExcelInputService.readExcelData(path+"\\"+targetFile.getName());
            System.out.println(fileExtensionName+"\\"+targetFile.getName());
            if (targetFile.exists()) {
                targetFile.delete();
            }
            return code;
        }
    }



    public String uploadfiles(MultipartFile file, String path) throws ServiceException {
        String fileName = file.getOriginalFilename();//拿到文件名

        File fileDir = new File(path);//创建目录file
        if (!fileDir.exists()) {
            fileDir.setWritable(true);//赋予文件权限,可写
            fileDir.mkdirs();//获取能在webapp文件下创建文件加的权限,因为发布完后可以改，mkdir是当前级别的，mkdirs是/a/b这样的文件夹创建
        }
        File targetFile = new File(path, fileName);//完整的file，包括路径文件名和扩展名
        try {
            file.transferTo(targetFile);
            File file1 = (File) file;
            FileOutputStream fos = new FileOutputStream(targetFile);
            InputStream input = new FileInputStream(targetFile);
            // 一次30kb
            byte[] readBuff = new byte[1024 * 30];
            int count = -1;
            while ((count = input.read(readBuff, 0, readBuff.length)) != -1) {
                fos.write(readBuff, 0, count);
            }
            fos.flush();
            fos.close();
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           return fileName;
        }
    }



}
