package top.went.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.mapper.CustomerMapper;
import top.went.exception.ServiceException;
import top.went.pojo.CustomerEntity;
import top.went.vo.CustomerVo;

import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager",rollbackFor = ServiceException.class)
public class CustomerStatisticService {
    @Autowired
    private CustomerMapper customerMapper;

    public List findAllForStatistic(String state) throws ServiceException{
        try{
            CustomerVo customerVo = new CustomerVo();
            List<Map> type = customerMapper.findForStatistic(state);
            return type;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("获取客户数据异常");
        }

    }
}
