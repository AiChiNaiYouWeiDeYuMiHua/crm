package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.MaintainDao;
import top.went.db.mapper.MaintainMapper;
import top.went.db.mapper.PlanPaydetailMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.MaintainOrderEntity;
import top.went.pojo.PlanPayDetailEntity;
import top.went.vo.MaintainVO;
import top.went.vo.PageEntity;
import top.went.vo.PlanPayDetailVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager",rollbackFor = ServiceException.class)
public class MaintainService {
    @Autowired
    private MaintainDao maintainDao;
    @Autowired
    private MaintainMapper maintainMapper;

    private static final Map<String,String> map = new HashMap<>();

    static {
        map.put("mtPay","mt_pay");
        map.put("mtDate", "mt_date");
    }

    /**
     * 添加维修工单
     * @param maintainOrderEntity
     * @throws ServiceException
     */
    public boolean mt_insert(MaintainOrderEntity maintainOrderEntity)throws ServiceException{
        try {
            if (maintainOrderEntity.getTbCustomerByCusId() != null && maintainOrderEntity.getTbCustomerByCusId().getCusId()!=null){}
            else
                maintainOrderEntity.setTbCustomerByCusId(null);
            if (maintainOrderEntity.getTbUserByUserId() != null && maintainOrderEntity.getTbUserByUserId().getUserId()!=null){}
            else
                maintainOrderEntity.setTbUserByUserId(null);
            if (maintainOrderEntity.getTbProductByProductId() != null && maintainOrderEntity.getTbProductByProductId().getPfId()!=null){}
            else
                maintainOrderEntity.setTbProductByProductId(null);
            if (maintainOrderEntity.getOrderEntity() != null && maintainOrderEntity.getOrderEntity().getOrderId()!=null){
            }
            else
                maintainOrderEntity.setOrderEntity(null);


            return maintainDao.save(maintainOrderEntity)!= null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("新建维修工单失败");
        }
    }

    /**
     * 删除维修工单
     * @param mtId
     * @throws ServiceException
     */
    public boolean mt_logicDelete(int mtId)throws ServiceException,NotFoundException {
        MaintainOrderEntity maintainOrderEntity=load(mtId);
        System.out.println("============================="+maintainOrderEntity);
        if(maintainOrderEntity==null)
            throw new NotFoundException("不存在该维修工单");
        try {
            maintainOrderEntity.setMagicDelete(1l);
            return maintainDao.save(maintainOrderEntity)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw  new ServiceException("删除维修工单");
        }
    }
    /**
     * 批量删除维修工单
     * @param ids
     * @throws ServiceException
     */
    public boolean mt_logicDeleteAll(Long[] ids)throws ServiceException,NotFoundException {
        for (int i = 0; i < ids.length; i++) {
            mt_logicDelete(ids[i].intValue());
        }
        return true;
    }

    /**
     * 查询维修工单
     * @return
     */
    public PageEntity<MaintainVO> mt_findAll(Integer pageSize, Integer pageNumber) throws ServiceException{
        System.out.println(pageSize+","+pageNumber);
        try {
            List<MaintainOrderEntity> rows =maintainDao.findAllByMagicDeleteOrderByMtId(0l, new PageRequest(pageNumber,pageSize));
            int total=maintainDao.mt_findAllCount();
            return new PageEntity<MaintainVO>((long) total, getMaintainVO(rows));

        }catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询所有维修工单失败");
        }
    }

    public PageEntity<MaintainVO> mt_findByStateAndTheme(MaintainVO maintainVO ) throws  ServiceException{
        if(maintainVO==null)
            return new PageEntity<>(0l, null);
        String order =maintainVO.getPage().getSort(map);
        if(order.length()<=0)
            order = "mt_id desc";
        com.github.pagehelper.Page<MaintainOrderEntity> page1 = PageHelper.startPage(maintainVO.getPage().getPage(), maintainVO.getPage().getSize(), order+" nulls last");
        maintainMapper.findByStateAndTheme(maintainVO);
        return new PageEntity(page1.getTotal(), page1.getResult());
    }

    /**
     * 高级查询
     * @param maintainVO
     * @return
     * @throws ServiceException
     */
    public PageEntity<MaintainOrderEntity> findAllByManyConditions(MaintainVO maintainVO) {
        if(maintainVO==null)
            return new PageEntity<>(0l, null);
        String order =maintainVO.getPage().getSort(map);
        if(order.length()<=0)
            order = "mt_id desc";
        com.github.pagehelper.Page<MaintainOrderEntity> page1 = PageHelper.startPage(maintainVO.getPage().getPage(), maintainVO.getPage().getSize(), order+" nulls last");
        maintainMapper.findByManyCondition(maintainVO);
        return new PageEntity<>(page1.getTotal(), page1.getResult());
    }
    /**
     * 加载维修工单
     * @param mtId
     * @return
     * @throws ServiceException
     */
    public MaintainVO load1(Integer mtId) throws ServiceException{
        try {
            return getMaintainVO1(maintainDao.findAllByMagicDeleteAndMtId(0l, mtId));
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载维修工单失败");
        }
    }
    /**
     * 加载维修工单
     * @param mtId
     * @return
     * @throws ServiceException
     */
    public MaintainOrderEntity load(Integer mtId) throws ServiceException{
        try {
            return maintainDao.findAllByMagicDeleteAndMtId(0l, mtId);
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载维修工单失败");
        }
    }

    private List<MaintainVO> getMaintainVO(List<MaintainOrderEntity> maintainOrderEntities){
        List<MaintainVO> maintainVOS=new ArrayList<>();
        for(MaintainOrderEntity maintainOrderEntity:maintainOrderEntities){
            maintainVOS.add(new MaintainVO(maintainOrderEntity));
        }
        return maintainVOS;
    }
    private MaintainVO getMaintainVO1(MaintainOrderEntity maintainOrderEntity){
        MaintainVO maintainVO=new MaintainVO(maintainOrderEntity);
        return maintainVO;
    }
}
