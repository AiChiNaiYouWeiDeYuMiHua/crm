package top.went.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.LogDao;
import top.went.exception.ServiceException;
import top.went.pojo.LogEntity;
import top.went.pojo.UserEntity;
import top.went.vo.PageEntity;

import java.sql.Date;
import java.util.List;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class LogService {

    @Autowired
    private LogDao logDao;
    @Autowired
    private UserService userService;

    /**
     * 新增操作日志
     *
     * @param log
     * @return
     * @throws ServiceException
     */
    public boolean addLog(LogEntity log) throws ServiceException {
        try {
            if (log.getLogId() != null)
                return false;
            logDao.save(log);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("生成日志失败");
        }
    }

    /**
     * 查找所有操作日志
     *
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws ServiceException
     */
    public PageEntity<LogEntity> findAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        try {
            PageRequest pageRequest = new PageRequest(pageNumber, pageSize);
            List<LogEntity> list = logDao.findAll(pageRequest);
            PageEntity pageEntity = new PageEntity(logDao.count(), list);
            return pageEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载操作日志失败");
        }
    }

    /**
     * 根据操作者和操作时间查找所有操作日志
     *
     * @param userId
     * @param time
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws ServiceException
     */
    public PageEntity<LogEntity> findAllByUserAndTime(Long userId, Date time, Integer pageNumber, Integer pageSize) throws ServiceException {
        try {
            PageRequest pageRequest = new PageRequest(pageNumber, pageSize);
            List<LogEntity> list = logDao.findAllByTbUserByUserIdAndLogOptionTime(userId, time, pageRequest);
            PageEntity pageEntity = new PageEntity(logDao.countAllByTbUserByUserIdAndLogOptionTime(userId, time), list);
            return pageEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载操作日志失败");
        }
    }

    /**
     * 根据操作者查找所有操作日志
     *
     * @param userId
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws ServiceException
     */
    public PageEntity<LogEntity> findAllByUser(Long userId, Integer pageNumber, Integer pageSize) throws ServiceException {
        try {
            PageRequest pageRequest = new PageRequest(pageNumber, pageSize);
            List<LogEntity> list = logDao.findAllByTbUserByUserId(userId, pageRequest);
            PageEntity pageEntity = new PageEntity(logDao.countAllByTbUserByUserId(userId), list);
            return pageEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载操作日志失败");
        }
    }

    /**
     * 根据操作时间查找所有操作日志
     *
     * @param time
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws ServiceException
     */
    public PageEntity<LogEntity> findAllByTime(Date time, Integer pageNumber, Integer pageSize) throws ServiceException {
        try {
            PageRequest pageRequest = new PageRequest(pageNumber, pageSize);
            List<LogEntity> list = logDao.findAllByLogOptionTime(time, pageRequest);
            PageEntity pageEntity = new PageEntity(logDao.countAllByLogOptionTime(time), list);
            return pageEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载操作日志失败");
        }
    }
}
