package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.CustomerDao;
import top.went.db.dao.ProjectDao;
import top.went.db.mapper.ProjectMapper;
import top.went.exception.ServiceException;
import top.went.pojo.ProjectEntity;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.ProjectVo;

import java.util.List;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class ProjectService {
    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private ProjectMapper projectMapper;

    public ProjectEntity getOneCus(Integer id ) throws ServiceException {
        try {
            ProjectEntity projectEntity = projectDao.findByCusId(id);
            System.out.println("夏洛特烦恼");
            System.out.println(projectEntity);
            return projectEntity;
        } catch (Exception e) {
            throw new ServiceException("获取客户异常");
        }
    }


    /**
     * 添加联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code addCots(ProjectEntity projectEntity) throws ServiceException {
        System.out.println("我吸扎炸毁");
        try {
            if (projectEntity != null) {
                projectEntity.setProjIsDelete(1);
                System.out.println(projectEntity);
                boolean result = projectDao.save(projectEntity) != null;
                if (result == true) {
                    return Code.success("添加联系人成功");
                }
                return Code.fail("添加联系人失败");
            }
            return Code.fail("添加联系人参数错误");
        } catch (Exception e) {
            throw new ServiceException("添加联系人异常");
        }
    }


    /**
     * 逻辑删除联系人
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Code deleteCots(Integer id) throws ServiceException {
        try {
            if (id != null) {
                ProjectEntity projectEntity = projectDao.findOne(id);
                projectEntity.setProjIsDelete(0);
                projectDao.save(projectEntity);
                return Code.success("删除联系人成功");
            }
            return Code.fail("参数错误");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除联系人异常");
        }
    }

    public PageEntity<ProjectVo> getCotsList(ProjectVo projectVo) throws ServiceException {
        try {
            System.out.println(projectVo.getAll()+"          我市闸炸毁");
            PageHelper.startPage(projectVo.getOffset(), projectVo.getLimit());
            List<ProjectEntity> projectEntities = projectMapper.findByParam(projectVo);
            PageEntity pageEntity = new PageEntity(((com.github.pagehelper.Page) projectEntities).getTotal(), projectEntities);
            for(ProjectEntity projectEntity:projectEntities){
                System.out.println(projectEntity);
            }
            return pageEntity;
        } catch (Exception e) {
            throw new ServiceException("获取联系人列表异常");
        }
    }

    public List<ProjectVo> getProjByCusId(Integer id) throws ServiceException{
        try{
            List<ProjectVo> projectVos = projectMapper.findProj(id);
            return projectVos;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("获取联系人错误");
        }
    }

    /**
     * 修改联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code updateCots(ProjectEntity projectEntity) throws ServiceException {
        try{
            projectEntity.setProjIsDelete(1);
            projectDao.save(projectEntity);
            return Code.success("修改联系人成功");
        } catch (Exception e) {
            throw new ServiceException("更新联系人异常");
        }
    }


    public void deleteProductAll(Integer[] ids) throws ServiceException {
        try{
            for (int i=0;i<ids.length;i++){
                ProjectEntity projectEntity = projectDao.findOne(ids[i].intValue());
                projectEntity.setProjIsDelete(0);
                projectDao.save(projectEntity);
            }
        }catch (Exception e){
            throw new ServiceException("删除异常");
        }

    }
}
