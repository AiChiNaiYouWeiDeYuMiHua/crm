package top.went.service;

import javafx.scene.control.Alert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.exception.ServiceException;
import top.went.pojo.*;
import top.went.vo.OrderVo;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class CustomerDetailService {
    //订单
    @Autowired
    private OrderService orderService;
    //销售机会
    @Autowired
    private SaleOppService saleOppService;
    //发货单
//    @Autowired
//    private DeliverGoo
    //回款
    @Autowired
    private PayBackDetailService payBackDetailService;
    //计划回款
    @Autowired
    private PlanPayBackService planPayBackService;
    //维修工单
    @Autowired
    private MaintainService maintainService;
    //开票
    @Autowired
    private ReceiptRecordsService receiptRecordsService;
    //详细需求
    @Autowired
    private DemandService demandService;
    //解决方案
    @Autowired
    private SolutionService solutionService;
    //报价
    @Autowired
    private QuoteService quoteService;
    //项目协作
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ThreeService threeService;
    @Autowired
    private MemorialService memorialService;



    public Map<Date,List<Object>> showAll(Integer id) throws ServiceException{
        Map<Date,List<Object>> mapDetail = new HashMap();
        //销售机会list
        List<SaleOppEntity> saleOppEntities = saleOppService.loadAllByCusId((long) id);
        //三一客list
        List<ThreeEntity> threeEntityList = threeService.getThreeListAll(id);
        //纪念日
        List<MemorialDayEntity> memorialDayEntityList = memorialService.getMedListAll(id);
        //订单
        List<OrderVo> orderEntityList = orderService.loadOrderByus((long)id);
        List<Date> datelist = new ArrayList<>();
        /**
         * 获取日期
         */
        //销售机会
        for(SaleOppEntity saleOppEntity:saleOppEntities){
            datelist.add(saleOppEntity.getUpdateTime());
            System.out.println(datelist.size());
        }
        //三一客
        for(ThreeEntity threeEntity:threeEntityList){
            datelist.add(threeEntity.getThreeUpdatatime());
        }
        //纪念日
        for(MemorialDayEntity memorialDayEntity:memorialDayEntityList){
            datelist.add(memorialDayEntity.getMedCreatetimr());
        }
        //订单
        for(OrderEntity orderEntity:orderEntityList){
            datelist.add(orderEntity.getOrderNewDate());
        }
        HashSet<Date> h = new HashSet<>(datelist);
        datelist.clear();
        datelist.addAll(h);
//        List<Date> dateSortList = DataSort(datelist);
        for(Date date:datelist){
            List<Object> listS = new ArrayList<>();
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
            String date1 = f.format(date);
            //销售机会
            for(SaleOppEntity saleOppEntity:saleOppEntities){
                String date2 = f.format(saleOppEntity.getUpdateTime());
                if(date1.equals(date2)){
                    List<SolutionEntity> solutionEntities = (List<SolutionEntity>) saleOppEntity.getTbSolutionsByOppId();
                    List<DemandEntity> demandEntities = (List<DemandEntity>) saleOppEntity.getTbDemandsByOppId();
                    List<QuoteEntity> quoteEntities = (List<QuoteEntity>) saleOppEntity.getTbQuotesByOppId();
                    String sale = "销售机会ｂ主题："+saleOppEntity.getOppTheme()+" "+saleOppEntity.getExpectedTime()+"￥"+saleOppEntity.getExpectedAmount()+"ｉｄ"+saleOppEntity.getOppId()+"ｃ";
                    for (SolutionEntity solutionEntity:solutionEntities){
                        sale = sale +"解决方案ｂ主题："+solutionEntity.getSolutionTheme() +" "+solutionEntity.getSubmitTime()+"ｉｄ"+solutionEntity.getSolutionId()+"ｃ";
                    }
                    for (DemandEntity demandEntity:demandEntities){
                        sale = sale +"详细需求ｂ主题："+demandEntity.getDemandTheme() +" "+demandEntity.getRecordTime()+"ｉｄ"+demandEntity.getDemandId()+"ｃ";
                    }
                    for (QuoteEntity quoteEntity:quoteEntities){
                        sale = sale +"报价ｂ主题："+quoteEntity.getQuoteTheme() +" ￥"+quoteEntity.getTotalQuote()+"ｉｄ"+quoteEntity.getQuoteId()+"ｃ";
                    }
                    System.out.println(saleOppEntity);
                    listS.add(sale);
                }
            }
            //订单
            for(OrderEntity orderEntity:orderEntityList){
                String date2 = f.format(orderEntity.getOrderNewDate());
                if(date1.equals(date2)){
                    String order = "订单ｂ"+orderEntity.getOrderTitle()+" "+orderEntity.getOrderPayWay()+"ｉｄ"+orderEntity.getOrderId()+"ｃ";
                    listS.add(order);
                }

            }
            //三一客
            for(ThreeEntity threeEntity:threeEntityList){
                String date2 = f.format(threeEntity.getThreeUpdatatime());
                if(date1.equals(date2)){
                    listS.add("三一客ｂ"+threeEntity.getThreeContent()+" "+threeEntity.getTbUserByUserId().getUserName()+"ａ");
                }
            }
            //纪念日
            for(MemorialDayEntity memorialDayEntity:memorialDayEntityList){
                String date2 = f.format(memorialDayEntity.getMedCreatetimr());
                if(date1.equals(date2)){
                    listS.add("纪念日ｂ"+memorialDayEntity.getTbContactsByCotsId().getCotsName()+" "+memorialDayEntity.getMedType()+memorialDayEntity.getMedMemorialDate()+"ａ");
                }
            }

            mapDetail.put(date,listS);
        }

        /**
         * 获取内容
         */

        Map<Date,List<Object>> mapSortDetail = sortMapByKey(mapDetail);

        return mapSortDetail;
    }


    public static Map<Date,List<Object>> sortMapByKey(Map<Date,List<Object>> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }

        Map<Date,List<Object>> sortMap = new TreeMap<Date,List<Object>>(
                new MapKeyComparator());

        sortMap.putAll(map);

        return sortMap;
    }

//    /**
//     * 日期排序list
//     * @param itemValue
//     * @return
//     */
//    public List<Date> DataSort(List<Date> itemValue){
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
//        for (int i = 0; i < itemValue.size() - 1; i++) {
//            for (int j = 1; j < itemValue.size() - i; j++) {
//                Date a;
//                java.util.Date data1 = null;
//                java.util.Date data2 = null;
//                try {
//                    data1 = sdf.parse(itemValue.get(j - 1).toString());
//                    data2 = sdf.parse(itemValue.get(j).toString());
//                } catch (ParseException e) {
//// TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//// 比较两个整数的大小
//                boolean flag = data1.before(data2);
//                if (flag) {
//                    a = itemValue.get(j - 1);
//                    itemValue.set((j - 1), itemValue.get(j));
//                    itemValue.set(j, a);
//                }
//            }
//        }
//        return itemValue;
//    }

}
class MapKeyComparator implements Comparator<Date>{

    @Override
    public int compare(Date str1, Date str2) {

        return str2.compareTo(str1);
    }
}
