package top.went.service;


import jxl.Sheet;
import jxl.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.CustomerDao;
import top.went.exception.ServiceException;
import top.went.pojo.CustomerEntity;
import top.went.vo.Code;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class CustomerExcelInputService {
    @Autowired
    private CustomerDao customerDao;


    public Code readExcelData(String filePath){
        List<CustomerEntity> list = new ArrayList<CustomerEntity>();
        try {
            String path = filePath;
            File xlsFile = new File(path);
            FileInputStream fs = new FileInputStream(xlsFile);
            Workbook book = Workbook.getWorkbook(fs);//获取工作簿对象
            Sheet sheet = book.getSheet(0);//获取工作表对象,第一个sheet
            int rows = sheet.getRows();//获取工作表中的数据行数

            for(int i=1;i<=rows-1;i++){//循环Excel工作表的行，并读取单元格数据
                CustomerEntity g = new CustomerEntity();
                String cusName = sheet.getCell(0, i).getContents();
                String cusAbb = sheet.getCell(1, i).getContents();
                String cusType = sheet.getCell(2, i).getContents();
                String cusAddr = sheet.getCell(3, i).getContents();
                String cusIndustry = sheet.getCell(4, i).getContents();
                String cusCome = sheet.getCell(5, i).getContents();
                String cusNet = sheet.getCell(6, i).getContents();
                Long cusPhone = Long.parseLong(sheet.getCell(7, i).getContents());
                String cusSynopsis = sheet.getCell(8, i).getContents();
                String cusArea = sheet.getCell(9, i).getContents();
                String cusKnot = sheet.getCell(10, i).getContents();
                String MsnQQ = sheet.getCell(11, i).getContents();
                String cusRemark = sheet.getCell(12, i).getContents();
                g.setCusName(cusName);
                g.setCusAbbreviation(cusAbb);
                g.setCusType(cusType);
                g.setCusAddr(cusAddr);
                g.setCusIndustry(cusIndustry);
                g.setCusCome(cusCome);
                g.setCusNet(cusNet);
                g.setCusPhone(cusPhone);
                g.setCusSynopsis(cusSynopsis);
                g.setCusArea(cusArea);
                g.setCusRemark(cusRemark);
                g.setCusMSNQQ(MsnQQ);
                g.setCusKnot(cusKnot);
                g.setCusIsDelete((long)1);
                System.out.println(g);
                List<CustomerEntity> customerEntityList = customerDao.findAll();
                for(CustomerEntity customerEntity:customerEntityList){
                    if(customerEntity.getCusName().equals(g.getCusName())){
                        return Code.fail("客户名称重复");
                    }else if(customerEntity.getCusAbbreviation().equals(customerEntity.getCusAbbreviation())){
                        return Code.fail("客户简称重复");
                    }
                    customerDao.save(g);
                }
            }
            System.out.println("------获取Excel中的数据【成功】------");
            return Code.success();
        } catch (Exception e) {
            System.out.println("获取Excel中的数据【异常】，异常信息："+e.getMessage());
            e.printStackTrace();
            return Code.fail("异常");
        }
    }
}
