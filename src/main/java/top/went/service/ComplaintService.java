package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.ComplaintDao;
import top.went.db.dao.CustomerDao;
import top.went.db.mapper.ComplaintMapper;
import top.went.exception.ServiceException;
import top.went.pojo.ComplaintEntity;
import top.went.vo.Code;
import top.went.vo.ComplaintVo;
import top.went.vo.PageEntity;

import java.util.List;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class ComplaintService {
    @Autowired
    private ComplaintDao complaintDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private ComplaintMapper complaintMapper;

    public ComplaintEntity getOneCus(Integer id ) throws ServiceException {
        try {
            ComplaintEntity complaintEntity = complaintDao.findByCusId(id);
            System.out.println("夏洛特烦恼");
            System.out.println(complaintEntity);
            return complaintEntity;
        } catch (Exception e) {
            throw new ServiceException("获取客户异常");
        }
    }


    /**
     * 添加联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code addCots(ComplaintEntity complaintEntity) throws ServiceException {
        System.out.println("我吸扎炸毁");
        try {
            if (complaintEntity != null) {
                complaintEntity.setComplainIsDelete(1);
                System.out.println(complaintEntity);
                boolean result = complaintDao.save(complaintEntity) != null;
                if (result == true) {
                    return Code.success("添加联系人成功");
                }
                return Code.fail("添加联系人失败");
            }
            return Code.fail("添加联系人参数错误");
        } catch (Exception e) {
            throw new ServiceException("添加联系人异常");
        }
    }


    /**
     * 逻辑删除联系人
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Code deleteCots(Integer id) throws ServiceException {
        try {
            if (id != null) {
                ComplaintEntity complaintEntity = complaintDao.findOne(id);
                complaintEntity.setComplainIsDelete(0);
                complaintDao.save(complaintEntity);
                return Code.success("删除联系人成功");
            }
            return Code.fail("参数错误");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除联系人异常");
        }
    }

    public PageEntity<ComplaintVo> getCotsList(ComplaintVo complaintVo) throws ServiceException {
        try {
            System.out.println(complaintVo.getAll()+"          我市闸炸毁");
            PageHelper.startPage(complaintVo.getOffset(), complaintVo.getLimit());
            List<ComplaintEntity> contactsEntityList = complaintMapper.findByParam(complaintVo);
            PageEntity pageEntity = new PageEntity(((com.github.pagehelper.Page) contactsEntityList).getTotal(), contactsEntityList);

            return pageEntity;
        } catch (Exception e) {
            throw new ServiceException("获取联系人列表异常");
        }
    }

    /**
     * 修改联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code updateCots(ComplaintEntity complaintEntity) throws ServiceException {
        try{
            complaintEntity.setComplainIsDelete(1);
            complaintDao.save(complaintEntity);
            return Code.success("修改联系人成功");
        } catch (Exception e) {
            throw new ServiceException("更新联系人异常");
        }
    }


    public void deleteProductAll(Integer[] ids) throws ServiceException {
        try{
            for (int i=0;i<ids.length;i++){
                ComplaintEntity complaintEntity = complaintDao.findOne(ids[i].intValue());
                complaintEntity.setComplainIsDelete(0);
                complaintDao.save(complaintEntity);
            }
        }catch (Exception e){
            throw new ServiceException("删除异常");
        }

    }
}
