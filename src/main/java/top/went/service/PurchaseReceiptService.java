package top.went.service;

import com.github.pagehelper.PageHelper;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.PurchaseReceiptDao;
import top.went.db.mapper.PurchaseReceiptMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PlanPayDetailEntity;
import top.went.pojo.PurchaseReceiptEntity;
import top.went.vo.PageEntity;
import top.went.vo.PaymentRecordVO;
import top.went.vo.PlanPayDetailVO;
import top.went.vo.PurchaseReceiptVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class PurchaseReceiptService {
    @Autowired
    private PurchaseReceiptDao purchaseReceiptDao;
    @Autowired
    private PurchaseReceiptMapper purchaseReceiptMapper;
    private static final Map<String,String> map = new HashMap<>();

    static {
        map.put("purrMoney","purr_money");
        map.put("purrDate","purr_date");
    }
    /**
     * 添加发票失败
     *
     * @param purchaseReceiptEntity
     * @return
     * @throws ServiceException
     */
    public boolean purr_insert(PurchaseReceiptEntity purchaseReceiptEntity) throws ServiceException {
        try {
            if(purchaseReceiptEntity.getTbUserByUserId()!=null&&purchaseReceiptEntity.getTbUserByUserId().getUserId()!=null){}
            else purchaseReceiptEntity.setTbUserByUserId(null);
            if(purchaseReceiptEntity.getTbCustomerByCusId()!=null&&purchaseReceiptEntity.getTbCustomerByCusId().getCusId()!=null){}
            else purchaseReceiptEntity.setTbCustomerByCusId(null);
            if(purchaseReceiptEntity.getTbPaymentRecordsByPrId()!=null&&purchaseReceiptEntity.getTbPaymentRecordsByPrId().getPrId()!=null){}
            else purchaseReceiptEntity.setTbPaymentRecordsByPrId(null);
            if(purchaseReceiptEntity.getTbPurchaseByPurId()!=null&&purchaseReceiptEntity.getTbPurchaseByPurId().getPurId()!=null){}
            else purchaseReceiptEntity.setTbPurchaseByPurId(null);
            return purchaseReceiptDao.save(purchaseReceiptEntity)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("新增发票失败");
        }


    }

    /**
     * 删除采购发票
     * @param purrId
     * @throws ServiceException
     */
    public boolean purr_logicDelete(int purrId)throws ServiceException,NotFoundException{
        PurchaseReceiptEntity purchaseReceiptEntity=load(purrId);
        if(purchaseReceiptEntity==null)
            throw new NotFoundException("不存在该采购发票");
        try{
            purchaseReceiptEntity.setMagicDelete(1l);
            return purchaseReceiptDao.save(purchaseReceiptEntity)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("删除该采购发票");
        }
    }
    /**
     * 批量删除计划付款
     * @param ids
     * @throws ServiceException
     */
    public boolean purr_logicDeleteAll(Long[] ids)throws ServiceException,NotFoundException {
        for (int i = 0; i < ids.length; i++) {
            purr_logicDelete(ids[i].intValue());
        }
        return true;
    }

    /**
     * 查询采购发票
     * @return
     */
    public PageEntity<PurchaseReceiptVO>  purr_findAll(Integer pageSize, Integer pageNumber) throws ServiceException{
        System.out.println(pageSize+","+pageNumber);
        try {
            List<PurchaseReceiptEntity> rows =purchaseReceiptDao.findAllByMagicDeleteOrderByPurrIdDesc(0l, new PageRequest(pageNumber,pageSize));
            int total=purchaseReceiptDao.purr_findAllCount();
            return new PageEntity<PurchaseReceiptVO>((long) total, getPurchaseReceiptVO(rows));
        }catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询所有采购发票失败");
        }
    }

    /**
     * 根据收票日期查询采购发票
     * @param purr_date
     * @return
     * @throws ServiceException
     */
    public PageEntity<PurchaseReceiptVO> purr_findByPpdDate(Integer pageSize, Integer pageNumber,String purr_date) throws  ServiceException{
        try {
            String purr_date1="%"+purr_date+"%";
            Page<PurchaseReceiptEntity> p=purchaseReceiptDao.findAllByDate(purr_date1, new PageRequest(pageNumber-1,pageSize));
            return new PageEntity<PurchaseReceiptVO>(p.getTotalElements(),getPurchaseReceiptVO(p.getContent()));
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("根据收票日期查询采购发票失败");
        }
    }

    /**
     * 根据id查询发票实体
     *
     * @param purrId
     * @return
     * @throws ServiceException
     * @throws NotFoundException
     */
    public PurchaseReceiptEntity findOne(int purrId) throws ServiceException, NotFoundException {
        try {
            PurchaseReceiptEntity purchaseReceiptEntity = purchaseReceiptDao.findOne(purrId);
            if (purchaseReceiptEntity == null)
                throw new NotFoundException("发票不存在");
            return purchaseReceiptEntity;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("根据id查询发票记录失败");
        }
    }

    /**
     * 高级查询
     * @param purchaseReceiptVO
     * @return
     * @throws ServiceException
     */
    public PageEntity<PurchaseReceiptEntity> findAllByManyConditions(PurchaseReceiptVO purchaseReceiptVO) {
        if(purchaseReceiptVO==null)
            return new PageEntity<>(0l, null);
        String order=purchaseReceiptVO.getPage().getSort(map);
        if(order.length()<=0)
            order="purr_id desc";
        com.github.pagehelper.Page<PlanPayDetailEntity> page1 = PageHelper.startPage(purchaseReceiptVO.getPage().getPage(), purchaseReceiptVO.getPage().getSize(), order+" nulls last");
        purchaseReceiptMapper.findByManyCondition(purchaseReceiptVO);
        return new PageEntity(page1.getTotal(), page1.getResult());
    }
    /**
     * 加载采购发票
     * @param purrId
     * @return
     * @throws ServiceException
     */
    public PurchaseReceiptVO load1(Integer purrId) throws ServiceException{
        try {
            System.out.println("======================================================="+getPurchaseReceiptVO1(purchaseReceiptDao.findAllByMagicDeleteAndPurrId(0l, purrId)));
            return getPurchaseReceiptVO1(purchaseReceiptDao.findAllByMagicDeleteAndPurrId(0l, purrId)) ;
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载计划付款失败");
        }
    }
    /**
     * 加载采购发票
     *
     * @param purId
     * @return
     * @throws ServiceException
     */
    public PurchaseReceiptEntity load(Integer purId) throws ServiceException {
        try {
            return purchaseReceiptDao.findAllByMagicDeleteAndPurrId(0l, purId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载采购发票失败");
        }
    }
    private List<PurchaseReceiptVO> getPurchaseReceiptVO(List<PurchaseReceiptEntity> purchaseReceiptEntities){
        List<PurchaseReceiptVO> purchaseReceiptVOS=new ArrayList<>();
        for(PurchaseReceiptEntity purchaseReceiptEntity:purchaseReceiptEntities){
            purchaseReceiptVOS.add(new PurchaseReceiptVO(purchaseReceiptEntity));
        }
        return purchaseReceiptVOS;
    }
    private PurchaseReceiptVO getPurchaseReceiptVO1(PurchaseReceiptEntity purchaseReceiptEntity){
        PurchaseReceiptVO purchaseReceiptVO=new PurchaseReceiptVO(purchaseReceiptEntity);
        return purchaseReceiptVO;
    }
}
