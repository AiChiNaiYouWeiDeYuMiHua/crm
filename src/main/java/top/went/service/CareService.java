package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.CareDao;
import top.went.db.dao.CustomerDao;
import top.went.db.mapper.CareMapper;
import top.went.exception.ServiceException;
import top.went.pojo.CareEntity;
import top.went.vo.CareVo;
import top.went.vo.Code;
import top.went.vo.PageEntity;

import java.util.List;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class CareService {
    @Autowired
    private CareDao careDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private CareMapper careMapper;

    public CareEntity getOneCus(Integer id ) throws ServiceException {
        try {
            CareEntity careEntity = careDao.findByCusId(id);
            System.out.println("夏洛特烦恼");
            System.out.println(careEntity);
            return careEntity;
        } catch (Exception e) {
            throw new ServiceException("获取客户异常");
        }
    }


    /**
     * 添加联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code addCots(CareEntity careEntity) throws ServiceException {
        System.out.println("我吸扎炸毁");
        try {
            if (careEntity != null) {
                careEntity.setCareIsDelete(1);
                System.out.println(careEntity);
                boolean result = careDao.save(careEntity) != null;
                if (result == true) {
                    return Code.success("添加联系人成功");
                }
                return Code.fail("添加联系人失败");
            }
            return Code.fail("添加联系人参数错误");
        } catch (Exception e) {
            throw new ServiceException("添加联系人异常");
        }
    }


    /**
     * 逻辑删除联系人
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Code deleteCots(Integer id) throws ServiceException {
        try {
            if (id != null) {
                CareEntity careEntity = careDao.findOne(id);
                careEntity.setCareIsDelete(0);
                careDao.save(careEntity);
                return Code.success("删除联系人成功");
            }
            return Code.fail("参数错误");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除联系人异常");
        }
    }

    public PageEntity<CareVo> getCotsList(CareVo careVo) throws ServiceException {
        try {
            System.out.println(careVo.getAll()+"          我市闸炸毁");
            PageHelper.startPage(careVo.getOffset(), careVo.getLimit());
            List<CareEntity> careEntities = careMapper.findByParam(careVo);
            PageEntity pageEntity = new PageEntity(((com.github.pagehelper.Page) careEntities).getTotal(), careEntities);
            return pageEntity;
        } catch (Exception e) {
            throw new ServiceException("获取联系人列表异常");
        }
    }

    /**
     * 修改联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code updateCots(CareEntity careEntity) throws ServiceException {
        try{
            careEntity.setCareIsDelete(1);
            careDao.save(careEntity);
            return Code.success("修改联系人成功");
        } catch (Exception e) {
            throw new ServiceException("更新联系人异常");
        }
    }


    public void deleteProductAll(Integer[] ids) throws ServiceException {
        try{
            for (int i=0;i<ids.length;i++){
                CareEntity careEntity = careDao.findOne(ids[i].intValue());
                careEntity.setCareIsDelete(0);
                careDao.save(careEntity);
            }
        }catch (Exception e){
            throw new ServiceException("删除异常");
        }

    }
}
