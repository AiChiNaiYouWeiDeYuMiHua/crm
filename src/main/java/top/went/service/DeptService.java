package top.went.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.DeptDao;
import top.went.db.dao.UserDao;
import top.went.exception.ServiceException;
import top.went.pojo.DeptEntity;
import top.went.pojo.UserEntity;
import top.went.vo.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 部门service
 */
@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class DeptService {
    private final DeptDao deptDao;

    private final UserDao userDao;

    @Autowired
    public DeptService(DeptDao deptDao, UserDao userDao) {
        this.deptDao = deptDao;
        this.userDao = userDao;
    }

    /**
     * 新增部门
     *
     * @param deptEntity
     * @return
     * @throws ServiceException
     */
    public boolean addDept(DeptEntity deptEntity) throws ServiceException {
        try {
            DeptEntity deptEntity1 = deptDao.findOneByDeptName(deptEntity.getDeptName());
            if (deptEntity1 != null)
                return false;
            deptDao.save(deptEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("新增部门错误");
        }
    }

    /**
     * 修改部门
     *
     * @param deptEntity
     * @return
     * @throws ServiceException
     */
    public boolean modifyDept(DeptEntity deptEntity) throws ServiceException {
        try {
            deptDao.save(deptEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("修改部门错误");
        }
    }

    /**
     * 删除部门
     *
     * @param deptId
     * @return
     * @throws ServiceException
     */
    public boolean deleteDept(Long deptId) throws ServiceException {
        try {
            DeptEntity deptEntity = deptDao.findOneById(deptId);
            deptEntity.setDeptIsDel(1L);
            deptDao.save(deptEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除部门错误");
        }
    }

    /**
     * 查找指定ID的部门
     *
     * @param deptId
     * @return
     * @throws ServiceException
     */
    public DeptEntity findOneById(Long deptId) throws ServiceException {
        try {
            return deptDao.findOneById(deptId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查找部门错误");
        }
    }

    /**
     * 查找所有部门
     *
     * @return
     * @throws ServiceException
     */
    public List<DeptEntity> findDepts() throws ServiceException {
        try {
            return deptDao.findAllDept();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查找所有部门失败");
        }
    }

    /**
     * 加载部门树
     *
     * @return
     * @throws ServiceException
     */
    /*public List<DeptVo> findUserOnDept() throws ServiceException {
        List<DeptVo> voList = new ArrayList<DeptVo>();
        //获取所有部门
        List<DeptEntity> deptEntities = deptDao.findAllDept();
        //找出每个部门的员工列表
        if (deptEntities != null) {
            for (DeptEntity dept : deptEntities) {
                List<UserVo> users = userDao.findUserByDept(dept.getDeptId());
                DeptVo deptVo = new DeptVo();
                deptVo.setDeptId(dept.getDeptId());
                deptVo.setDeptName(dept.getDeptName());
                deptVo.setList(users);
                voList.add(deptVo);
            }
        }
        return voList;
    }*/
    public List<DeptVo> getUserNodes() throws ServiceException {
        try {
            List<DeptVo> nodes = new ArrayList<DeptVo>();
            List<DeptEntity> deptList = deptDao.findAllDept();
            if (deptList != null) {
                for (DeptEntity dept : deptList) {
                    List<Object[]> objArrList = userDao.queryAllByDeptId(dept.getDeptId());
                    List<UserVo> userEntities = new ArrayList<UserVo>();
                    if (objArrList != null) {
                        for (Object[] objArr : objArrList) {
                            userEntities.add(objArr2User(objArr));
                        }
                    }
                    DeptVo node = new DeptVo();
                    node.setDeptId(dept.getDeptId());
                    node.setDeptName(dept.getDeptName());
                    node.setList(userEntities);
                    nodes.add(node);
                }
            }
            return nodes;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查找用户列表错误");
        }
    }

    /**
     * 将Object[]转换成User对象
     *
     * @param objArr
     * @return
     */
    private UserVo objArr2User(Object[] objArr) {
        UserPassword userVo = new UserPassword();
        userVo.setUserId(((BigDecimal) objArr[0]).longValue());
        userVo.setUserName((String) objArr[1]);
        return userVo;
    }

    /**
     * 将Object[]转换成Dept对象
     *
     * @param objArr
     * @return
     */
    private DeptEntity objArr2Dept(Object[] objArr) {
        DeptTable dept = new DeptTable();
        dept.setDeptId(((BigDecimal) objArr[0]).longValue());
        dept.setDeptName((String) objArr[1]);
        dept.setDeptCount((int) ((BigDecimal) objArr[2]).longValue());
        return dept;
    }

    /**
     * 给部门分配员工
     *
     * @param ids
     * @return
     * @throws ServiceException
     */
    public boolean grantUserToDept(DeptIdAndUserIds ids) throws ServiceException {
        try {
            DeptEntity dept = deptDao.findOneById(ids.getDeptId());
            List<UserEntity> users = userDao.findAllByUserIsDimissionAndTbDeptByDeptId(0L, dept);
            DeptEntity d = deptDao.findOneById(99L);
            for (UserEntity user : users) {
                user.setTbDeptByDeptId(d);
                userDao.save(user);
            }
            if (ids.getUserIds() != null) {
                for (Long userId : ids.getUserIds()) {
                    UserEntity u = userDao.findUserByOne(0L, userId);
                    u.setTbDeptByDeptId(dept);
                    userDao.save(u);
                }
                return true;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("分配员工失败");
        }
    }

    /**
     * 加载部门表格
     *
     * @return
     * @throws ServiceException
     */
    public List<DeptEntity> findToTable() throws ServiceException {
        try {
            List<DeptEntity> deptEntities = new ArrayList<DeptEntity>();
            List<Object[]> obj = deptDao.findAllToTable();
            if (obj != null) {
                for (Object[] o : obj) {
                    deptEntities.add(objArr2Dept(o));
                }
            }
            return deptEntities;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("失败");
        }
    }

    /**
     * 根据部门名模糊查询
     *
     * @param name
     * @return
     * @throws ServiceException
     */
    public List<DeptEntity> findAllDeptNameLike(String name) throws ServiceException {
        try {
            List<DeptEntity> deptEntities = new ArrayList<DeptEntity>();
            List<Object[]> obj = deptDao.findAllByDeptNameLike("%" + name + "%");
            if (obj != null) {
                for (Object[] o : obj) {
                    deptEntities.add(objArr2Dept(o));
                }
            }
            return deptEntities;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("失败");
        }
    }
}
