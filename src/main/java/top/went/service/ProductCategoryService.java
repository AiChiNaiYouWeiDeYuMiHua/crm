package top.went.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.ProductCategoryDao;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.ProductCategoryEntity;
import top.went.vo.ProductCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * 产品分类
 */
@Service
@Transactional(value = "transactionManager",rollbackFor = ServiceException.class)
public class ProductCategoryService {

    /** 产品类别dao*/
    private final ProductCategoryDao categoryDao;

    @Autowired
    public ProductCategoryService(ProductCategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }


    /**
     * 添加产品分类
     * @param categoryEntity
     * @return
     * @throws ServiceException
     * @throws NotFoundException
     */
    public boolean addCategory(ProductCategoryEntity categoryEntity) throws ServiceException, NotFoundException {
        checkParentCategory(categoryEntity);
        categoryEntity.setLogicDelete(false);
        try {
            categoryDao.save(categoryEntity);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("新增类别错误");
        }
        return true;
    }

    /**
     * 修改类别信息
     * @param categoryEntity
     * @return
     * @throws ServiceException
     * @throws NotFoundException
     */
    public boolean modifyCategory(ProductCategoryEntity categoryEntity)throws ServiceException,NotFoundException {
        ProductCategoryEntity category = load(categoryEntity.getPcId());
        if (category == null )
            throw new NotFoundException("类别不存在");
        category.setPcName(categoryEntity.getPcName());
        checkParentCategory(categoryEntity);
        category.setTbProductCategoryByTbPcId(categoryEntity.getTbProductCategoryByTbPcId());
        try {
            categoryDao.save(category);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("修改类别信息失败");
        }
        return true;
    }

    /**
     * 删除类别信息
     * @param id
     * @return
     * @throws NotFoundException
     * @throws ServiceException
     */
    public boolean deleteCategory(Long id) throws NotFoundException, ServiceException {
        ProductCategoryEntity categoryEntity = load(id);

       if (categoryEntity.getTbProductCategoriesByPcId().size() !=0 || categoryEntity.getTbProductsByPcId().size()!=0)
           throw new ServiceException("该类别下有子类别或者产品");
       categoryEntity.setLogicDelete(true);
        try {
            categoryDao.save(categoryEntity);
        }catch (Exception e){
            throw new ServiceException("删除类别失败");
        }
        return true;
    }

    /**
     * 加载类别
     * @param id
     * @return
     */
    public ProductCategoryEntity load(Long id) throws NotFoundException {
        ProductCategoryEntity categoryEntity =  categoryDao.findOne(id,false);
        if (categoryEntity == null)
            throw new NotFoundException("类别不存在");
        return categoryEntity;
    }

    /**
     * 加载类别面包屑
     * @param id
     * @return
     * @throws NotFoundException
     */
    public ProductCategory loadVo(Long id) throws NotFoundException {
        if (id == 0){
            return new ProductCategory("产品类别",0L);
        }
        ProductCategoryEntity categoryEntity = load(id);
        ProductCategoryEntity parent = categoryEntity;
        ProductCategory category = new ProductCategory();
        category.setId(categoryEntity.getPcId());
        StringBuffer sb = new StringBuffer();
        while ((parent = parent.getTbProductCategoryByTbPcId()) != null){
            sb.insert(0,parent.getPcName()+"->");
        }
        sb.insert(0,"产品类别->");
        sb.append(categoryEntity.getPcName());
        category.setText(sb.toString());
        return category;
    }

    /**
     * 加载所有类别信息(包括产品
     * @return
     */
    public ProductCategoryEntity loadListAndProduct() {
        List<ProductCategoryEntity> categoryEntities = categoryDao.findAll(false);
        ProductCategoryEntity root = new ProductCategoryEntity(0L,"产品类别");
        root.setTbProductCategoriesByPcId(categoryEntities);
        return root;
    }


    /**
     * 加载所有类别信息(不包括产品
     * @return
     */
    public ProductCategory loadList(){
        return new ProductCategory(loadListAndProduct());
    }

    /**
     * 加载所有产品类别
     * @return
     */
    public List<ProductCategory> loadAll() {
        List<ProductCategory> productCategories = new ArrayList<>();
        for (ProductCategoryEntity categoryEntity : categoryDao.findAllByLogicDelete(false)) {
            productCategories.add(new ProductCategory(categoryEntity,false));
        }
        if (productCategories.size() <=0)
            return null;
        return productCategories;
    }

    /**
     * 检查并且设置父类别
     * @param categoryEntity
     * @return
     * @throws NotFoundException
     */
    private ProductCategoryEntity checkParentCategory(ProductCategoryEntity categoryEntity) throws NotFoundException {
        ProductCategoryEntity parent = categoryEntity.getTbProductCategoryByTbPcId();
        Long parentId = null == parent?0:parent.getPcId();
        if (parentId == 0)
            parent = null;
        else {
            parent = load(parentId);
            if (parent == null)
                throw new NotFoundException("新增类别信息错误，其父类别不存在");
        }
        categoryEntity.setTbProductCategoryByTbPcId(parent);
        return categoryEntity;
    }

}
