package top.went.service;

import com.github.pagehelper.PageHelper;
import com.sun.xml.internal.ws.server.ServerRtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.PlanPayBackDao;
import top.went.db.dao.PurchaseReceiptDao;
import top.went.db.mapper.PlanPayBackMapper;
import top.went.db.mapper.PurchaseReceiptMapper;
import top.went.exception.NotFoundException;
import top.went.exception.ServiceException;
import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PlanPayBackEntity;
import top.went.pojo.PlanPayDetailEntity;
import top.went.pojo.PurchaseReceiptEntity;
import top.went.vo.PageEntity;
import top.went.vo.PaymentRecordVO;
import top.went.vo.PlanPayBackVO;
import top.went.vo.PurchaseReceiptVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value = "transactionManager",rollbackFor = ServiceException.class)
public class PlanPayBackService {
    @Autowired
    private PlanPayBackDao planPayBackDao;
    @Autowired
   private PlanPayBackMapper planPayBackMapper;
    private static final Map<String,String> map = new HashMap<>();

    static {
        map.put("ppbMoney","ppb_money");
        map.put("ppbDate","ppb_date");
        map.put("ppbTerms","ppb_terms");
    }
    /**
     * 添加
     * @param planPayBackEntity
     * @return
     * @throws ServiceException
     */
    public boolean ppb_insert(PlanPayBackEntity planPayBackEntity) throws ServiceException{
        try{
            if(planPayBackEntity.getTbCustomerByCusId()!=null&&planPayBackEntity.getTbCustomerByCusId().getCusId()!=null){}
            else
                planPayBackEntity.setTbCustomerByCusId(null);
            if(planPayBackEntity.getTbUserByUserId()!=null&&planPayBackEntity.getTbUserByUserId().getUserId()!=null){ }
            else
                planPayBackEntity.setTbUserByUserId(null);
            if(planPayBackEntity.getTbMaintainOrderByMtId()!=null&&planPayBackEntity.getTbMaintainOrderByMtId().getMtId()!=null){}
            else
                planPayBackEntity.setTbMaintainOrderByMtId(null);
            if(planPayBackEntity.getTbOrderByOrderId()!=null&&planPayBackEntity.getTbOrderByOrderId().getOrderId()!=null){}
            else
                planPayBackEntity.setTbOrderByOrderId(null);
            return planPayBackDao.save(planPayBackEntity)!=null;
        }catch (Exception e){
            throw new ServiceException("新建回款失败！");
        }
    }

    /**
     * 删除计划回款
     * @param ppb_id
     * @return
     * @throws ServiceException
     */
    public boolean ppb_delete(int ppb_id) throws ServiceException,NotFoundException{
        PlanPayBackEntity planPayBackEntity=load(ppb_id);
        if(planPayBackEntity==null)
            throw new NotFoundException("不存在该计划回款");
        try {
            planPayBackEntity.setMagicDelete(1l);
            return planPayBackDao.save(planPayBackEntity)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("删除计划回款失败");
        }

    }
    /**
     * 批量删除计划回款
     * @param ids
     * @throws ServiceException
     */
    public boolean ppb_logicDeleteAll(Long[] ids)throws ServiceException,NotFoundException {
        for (int i = 0; i < ids.length; i++) {
            ppb_delete(ids[i].intValue());
        }
        return true;
    }
    /**
     * 修改计划回款
     * @param planPayBackEntity
     * @return
     * @throws ServiceException
     */
    public boolean ppb_update(PlanPayBackEntity planPayBackEntity)throws ServiceException,NotFoundException{
        PlanPayBackEntity planPayBackEntity1 = load(planPayBackEntity.getPpbId());
        if(planPayBackEntity1==null)
            throw new NotFoundException("计划回款不存在");
        planPayBackEntity1.setPpbTerms(planPayBackEntity.getPpbTerms());
        planPayBackEntity1.setPpbType(planPayBackEntity.getPpbType());
        planPayBackEntity1.setPpbMoney(planPayBackEntity.getPpbMoney());
        planPayBackEntity1.setPpbDate(planPayBackEntity.getPpbDate());
        try{
            return planPayBackDao.save(planPayBackEntity1)!=null;
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("修改计划回款失败");
        }
    }

    /**
     * 查询所有计划回款
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    public PageEntity<PlanPayBackVO> ppb_findAll(Integer pageSize, Integer pageNumber) throws ServiceException{
        try {
            List<PlanPayBackEntity> rows =planPayBackDao.findAllByMagicDeleteOrderByPpbIdDesc(0l, new PageRequest(pageNumber-1,pageSize));
            int total=planPayBackDao.ppb_findAllCount();
            return new PageEntity<PlanPayBackVO>((long) total, getPlanPayBackVos(rows));
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("查询所有计划回款失败");
        }
    }

    /**
     * 根据计划回款日期查询
     * @param date
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws ServiceException
     */
    public PageEntity<PlanPayBackVO> ppb_findAllByDate (String date,Integer pageSize, Integer pageNumber) throws ServiceException{
        try {
            String date1= "%"+date+"%";
            Page<PlanPayBackEntity> p = planPayBackDao.findAllByDate(date1,new PageRequest(pageNumber-1,pageSize));
            return new PageEntity<>(p.getTotalElements(),getPlanPayBackVos(p.getContent()));
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("根据计划回款日期查询失败");
        }

    }


    /**
     * 高级查询
     * @param planPayBackVO
     * @return
     * @throws ServiceException
     */
    public PageEntity<PlanPayBackEntity> findAllByManyConditions(PlanPayBackVO planPayBackVO) throws  ServiceException{
        if(planPayBackVO==null)
            return new PageEntity<>(0l, null);
        String order =planPayBackVO.getPage().getSort(map);
        if(order.length()<=0)
            order = "ppb_id desc";
        com.github.pagehelper.Page<PlanPayDetailEntity> page1 = PageHelper.startPage(planPayBackVO.getPage().getPage(), planPayBackVO.getPage().getSize(), order+" nulls last");
        planPayBackMapper.findByManyCondition(planPayBackVO);
        return new PageEntity(page1.getTotal(), page1.getResult());

    }
    /**
     * 加载付款回款
     * @param ppbId
     * @return
     * @throws ServiceException
     */
    public PlanPayBackEntity load(Integer ppbId) throws  ServiceException{
        try {
            return planPayBackDao.findAllByMagicDeleteAndPpbId(0l, ppbId);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServiceException("加载付款记录失败");
        }
    }
    /**
     * 加载计划回款
     * @param ppbId
     * @return
     * @throws ServiceException
     */
    public PlanPayBackVO load1(Integer ppbId) throws ServiceException {
        try {
            return getPlanPayBackVos1(planPayBackDao.findAllByMagicDeleteAndPpbId(0l, ppbId)) ;
        }catch (Exception e){
            e.printStackTrace();;
            throw new ServiceException("加载计划回款失败");
        }
    }
    private List<PlanPayBackVO> getPlanPayBackVos(List<PlanPayBackEntity> planPayBackEntities) {
        List<PlanPayBackVO> planPayBackVOS = new ArrayList<>();
        for (PlanPayBackEntity payBackEntity : planPayBackEntities){
            planPayBackVOS.add(new PlanPayBackVO(payBackEntity));
        }
        return planPayBackVOS;
    }
    private PlanPayBackVO getPlanPayBackVos1(PlanPayBackEntity planPayBackEntity){
        PlanPayBackVO planPayBackVO=new PlanPayBackVO(planPayBackEntity);
        return planPayBackVO;
    }
}
