package top.went.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.CustomerDao;
import top.went.db.dao.OrderDao;
import top.went.db.dao.UserDao;
import top.went.exception.ServiceException;
import top.went.pojo.UserEntity;
import top.went.vo.MainCount;

@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class CrmMainService {

    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private SaleOppService saleOppService;
    @Autowired
    private QuoteService quoteService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private PurchaseService purchaseService;

    public MainCount countFF() throws ServiceException {
        try {
            MainCount mainCount = new MainCount();
            mainCount.setCus(customerDao.count());
            mainCount.setOrder(orderDao.count());
            return mainCount;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载失败");
        }
    }

    public MainCount countMy(Long userId) throws ServiceException {
        try {
//            UserEntity user = userDao.findUserByOne(0L, userId);
            MainCount mainCount = new MainCount();
            mainCount.setOrder(orderService.countByUser(userId));
            mainCount.setCus(customerService.countByUser(userId));
            mainCount.setOpp(saleOppService.countByUser(userId));
            mainCount.setQuote(quoteService.countByUser(userId));
            mainCount.setPur(purchaseService.countByUser(userId));
            return mainCount;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("加载失败");
        }
    }

    public MainCount countFirm() throws ServiceException {
        try {
            MainCount mainCount = new MainCount();
            mainCount.setPur(purchaseService.countByMonth());
            mainCount.setQuote(quoteService.countByMonth());
            mainCount.setOpp(saleOppService.countByMonth());
            mainCount.setCus(customerService.countByMonth());
            mainCount.setOrder(orderService.countByMonth());
            return mainCount;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("失败");
        }
    }
}
