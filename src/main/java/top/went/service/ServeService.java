package top.went.service;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.went.db.dao.CustomerDao;
import top.went.db.dao.ServeDao;
import top.went.db.mapper.ServeMapper;
import top.went.exception.ServiceException;
import top.went.pojo.ServeEntity;
import top.went.vo.Code;
import top.went.vo.PageEntity;
import top.went.vo.ServeVo;

import java.util.List;
@Service
@Transactional(value = "transactionManager", rollbackFor = ServiceException.class)
public class ServeService {
    @Autowired
    private ServeDao serveDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private ServeMapper serveMapper;

    public ServeEntity getOneCus(Integer id ) throws ServiceException {
        try {
            ServeEntity serveEntity = serveDao.findByCusId(id);
            System.out.println("夏洛特烦恼");
            System.out.println(serveEntity);
            return serveEntity;
        } catch (Exception e) {
            throw new ServiceException("获取客户异常");
        }
    }


    /**
     * 添加联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code addCots(ServeEntity serveEntity) throws ServiceException {
        System.out.println("我吸扎炸毁");
        try {
            if (serveEntity != null) {
                serveEntity.setServeIsDelete(1);
                System.out.println(serveEntity);
                boolean result = serveDao.save(serveEntity) != null;
                if (result == true) {
                    return Code.success("添加联系人成功");
                }
                return Code.fail("添加联系人失败");
            }
            return Code.fail("添加联系人参数错误");
        } catch (Exception e) {
            throw new ServiceException("添加联系人异常");
        }
    }


    /**
     * 逻辑删除联系人
     *
     * @param id
     * @return
     * @throws ServiceException
     */
    public Code deleteCots(Integer id) throws ServiceException {
        try {
            if (id != null) {
                ServeEntity serveEntity = serveDao.findOne(id);
                serveEntity.setServeIsDelete(0);
                serveDao.save(serveEntity);
                return Code.success("删除联系人成功");
            }
            return Code.fail("参数错误");
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("删除联系人异常");
        }
    }

    public PageEntity<ServeVo> getCotsList(ServeVo serveVo) throws ServiceException {
        try {
            System.out.println(serveVo.getAll()+"          我市闸炸毁");
            PageHelper.startPage(serveVo.getOffset(), serveVo.getLimit());
            List<ServeEntity> contactsEntityList = serveMapper.findByParam(serveVo);
            PageEntity pageEntity = new PageEntity(((com.github.pagehelper.Page) contactsEntityList).getTotal(), contactsEntityList);

            return pageEntity;
        } catch (Exception e) {
            throw new ServiceException("获取联系人列表异常");
        }
    }

    /**
     * 修改联系人
     *
     * @return
     * @throws ServiceException
     */
    public Code updateCots(ServeEntity serveEntity) throws ServiceException {
        try{
            serveEntity.setServeIsDelete(1);
            serveDao.save(serveEntity);
            return Code.success("修改联系人成功");
        } catch (Exception e) {
            throw new ServiceException("更新联系人异常");
        }
    }


    public void deleteProductAll(Integer[] ids) throws ServiceException {
        try{
            for (int i=0;i<ids.length;i++){
                ServeEntity serveEntity = serveDao.findOne(ids[i].intValue());
                serveEntity.setServeIsDelete(0);
                serveDao.save(serveEntity);
            }
        }catch (Exception e){
            throw new ServiceException("删除异常");
        }

    }

}
