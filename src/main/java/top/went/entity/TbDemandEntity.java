package top.went.entity;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "TB_DEMAND", schema = "USER_CRM", catalog = "")
public class TbDemandEntity {
    private long demandId;
    private String demandTheme;
    private Time recordTime;
    private Long importance;
    private String demandContent;
    private Long isDelete;
    private TbSaleOppEntity tbSaleOppByOppId;

    @Id
    @Column(name = "DEMAND_ID")
    public long getDemandId() {
        return demandId;
    }

    public void setDemandId(long demandId) {
        this.demandId = demandId;
    }

    @Basic
    @Column(name = "DEMAND_THEME")
    public String getDemandTheme() {
        return demandTheme;
    }

    public void setDemandTheme(String demandTheme) {
        this.demandTheme = demandTheme;
    }

    @Basic
    @Column(name = "RECORD_TIME")
    public Time getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Time recordTime) {
        this.recordTime = recordTime;
    }

    @Basic
    @Column(name = "IMPORTANCE")
    public Long getImportance() {
        return importance;
    }

    public void setImportance(Long importance) {
        this.importance = importance;
    }

    @Basic
    @Column(name = "DEMAND_CONTENT")
    public String getDemandContent() {
        return demandContent;
    }

    public void setDemandContent(String demandContent) {
        this.demandContent = demandContent;
    }

    @Basic
    @Column(name = "IS_DELETE")
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbDemandEntity that = (TbDemandEntity) o;
        return demandId == that.demandId &&
                Objects.equals(demandTheme, that.demandTheme) &&
                Objects.equals(recordTime, that.recordTime) &&
                Objects.equals(importance, that.importance) &&
                Objects.equals(demandContent, that.demandContent) &&
                Objects.equals(isDelete, that.isDelete);
    }

    @Override
    public int hashCode() {

        return Objects.hash(demandId, demandTheme, recordTime, importance, demandContent, isDelete);
    }

    @ManyToOne
    @JoinColumn(name = "OPP_ID", referencedColumnName = "OPP_ID")
    public TbSaleOppEntity getTbSaleOppByOppId() {
        return tbSaleOppByOppId;
    }

    public void setTbSaleOppByOppId(TbSaleOppEntity tbSaleOppByOppId) {
        this.tbSaleOppByOppId = tbSaleOppByOppId;
    }
}
