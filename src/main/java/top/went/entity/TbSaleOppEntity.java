package top.went.entity;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "TB_SALE_OPP", schema = "USER_CRM", catalog = "")
public class TbSaleOppEntity {
    private long oppId;
    private String oppTheme;
    private Time updateTime;
    private Long oppStatus;
    private String classification;
    private Time discoveryTime;
    private String oppSource;
    private String customerDemand;
    private Time expectedTime;
    private String possibility;
    private String intendProduct;
    private Long expectedAmount;
    private Integer priority;
    private String stage;
    private String stageRemarks;
    private String trackLog;
    private Long isDelete;
    private Time stageStartTime;
    private Time createTime;

    @Id
    @Column(name = "OPP_ID")
    public long getOppId() {
        return oppId;
    }

    public void setOppId(long oppId) {
        this.oppId = oppId;
    }

    @Basic
    @Column(name = "OPP_THEME")
    public String getOppTheme() {
        return oppTheme;
    }

    public void setOppTheme(String oppTheme) {
        this.oppTheme = oppTheme;
    }

    @Basic
    @Column(name = "UPDATE_TIME")
    public Time getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Time updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "OPP_STATUS")
    public Long getOppStatus() {
        return oppStatus;
    }

    public void setOppStatus(Long oppStatus) {
        this.oppStatus = oppStatus;
    }

    @Basic
    @Column(name = "CLASSIFICATION")
    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    @Basic
    @Column(name = "DISCOVERY_TIME")
    public Time getDiscoveryTime() {
        return discoveryTime;
    }

    public void setDiscoveryTime(Time discoveryTime) {
        this.discoveryTime = discoveryTime;
    }

    @Basic
    @Column(name = "OPP_SOURCE")
    public String getOppSource() {
        return oppSource;
    }

    public void setOppSource(String oppSource) {
        this.oppSource = oppSource;
    }

    @Basic
    @Column(name = "CUSTOMER_DEMAND")
    public String getCustomerDemand() {
        return customerDemand;
    }

    public void setCustomerDemand(String customerDemand) {
        this.customerDemand = customerDemand;
    }

    @Basic
    @Column(name = "EXPECTED_TIME")
    public Time getExpectedTime() {
        return expectedTime;
    }

    public void setExpectedTime(Time expectedTime) {
        this.expectedTime = expectedTime;
    }

    @Basic
    @Column(name = "POSSIBILITY")
    public String getPossibility() {
        return possibility;
    }

    public void setPossibility(String possibility) {
        this.possibility = possibility;
    }

    @Basic
    @Column(name = "INTEND_PRODUCT")
    public String getIntendProduct() {
        return intendProduct;
    }

    public void setIntendProduct(String intendProduct) {
        this.intendProduct = intendProduct;
    }

    @Basic
    @Column(name = "EXPECTED_AMOUNT")
    public Long getExpectedAmount() {
        return expectedAmount;
    }

    public void setExpectedAmount(Long expectedAmount) {
        this.expectedAmount = expectedAmount;
    }

    @Basic
    @Column(name = "PRIORITY")
    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Basic
    @Column(name = "STAGE")
    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    @Basic
    @Column(name = "STAGE_REMARKS")
    public String getStageRemarks() {
        return stageRemarks;
    }

    public void setStageRemarks(String stageRemarks) {
        this.stageRemarks = stageRemarks;
    }

    @Basic
    @Column(name = "TRACK_LOG")
    public String getTrackLog() {
        return trackLog;
    }

    public void setTrackLog(String trackLog) {
        this.trackLog = trackLog;
    }

    @Basic
    @Column(name = "IS_DELETE")
    public Long getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Long isDelete) {
        this.isDelete = isDelete;
    }

    @Basic
    @Column(name = "STAGE_START_TIME")
    public Time getStageStartTime() {
        return stageStartTime;
    }

    public void setStageStartTime(Time stageStartTime) {
        this.stageStartTime = stageStartTime;
    }

    @Basic
    @Column(name = "CREATE_TIME")
    public Time getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Time createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbSaleOppEntity that = (TbSaleOppEntity) o;
        return oppId == that.oppId &&
                Objects.equals(oppTheme, that.oppTheme) &&
                Objects.equals(updateTime, that.updateTime) &&
                Objects.equals(oppStatus, that.oppStatus) &&
                Objects.equals(classification, that.classification) &&
                Objects.equals(discoveryTime, that.discoveryTime) &&
                Objects.equals(oppSource, that.oppSource) &&
                Objects.equals(customerDemand, that.customerDemand) &&
                Objects.equals(expectedTime, that.expectedTime) &&
                Objects.equals(possibility, that.possibility) &&
                Objects.equals(intendProduct, that.intendProduct) &&
                Objects.equals(expectedAmount, that.expectedAmount) &&
                Objects.equals(priority, that.priority) &&
                Objects.equals(stage, that.stage) &&
                Objects.equals(stageRemarks, that.stageRemarks) &&
                Objects.equals(trackLog, that.trackLog) &&
                Objects.equals(isDelete, that.isDelete) &&
                Objects.equals(stageStartTime, that.stageStartTime) &&
                Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(oppId, oppTheme, updateTime, oppStatus, classification, discoveryTime, oppSource, customerDemand, expectedTime, possibility, intendProduct, expectedAmount, priority, stage, stageRemarks, trackLog, isDelete, stageStartTime, createTime);
    }
}
