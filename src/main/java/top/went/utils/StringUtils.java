package top.went.utils;

import top.went.pojo.ProductFormatEntity;

public final class StringUtils {
    /**
     * 获取title
     * @return
     */
    public static StringBuffer getStringBuffer(ProductFormatEntity formatEntity) {
        String formaterName = formatEntity.getPfName();
        String productName = formatEntity.getTbProductByProductId().getProductName();
        String productModalName = formatEntity.getTbProductByProductId().getProductModel();
        String util = formatEntity.getPfUnit();
        StringBuffer sb = new StringBuffer();
        if (productName != null && productName.length() >0)
            sb.append(productName +"/");
        if (productModalName != null && productModalName.length() >0)
            sb.append(productModalName +"/");
        if (formaterName != null && formaterName.length() >0)
            sb.append(formaterName +"/");
        int o = sb.lastIndexOf("/");
        if (o > 0)
            sb.deleteCharAt(o);
        if (util != null && util.length() >0)
            sb.append("["+util +"]");
        return sb;
    }

    public static StringBuffer getStringBuffer(String productName, String productModalName, String formaterName, String util) {
        System.out.println("产品"+productName);
        System.out.println("型号"+productModalName);
        System.out.println("规格"+formaterName);
        System.out.println("单位"+util);
        StringBuffer sb = new StringBuffer();
        if (productName != null && productName.length() >0)
            sb.append(productName +"/");
        if (productModalName != null && productModalName.length() >0)
            sb.append(productModalName +"/");
        if (formaterName != null && formaterName.length() >0)
            sb.append(formaterName +"/");
        int o = sb.lastIndexOf("/");
        if (o > 0)
            sb.deleteCharAt(o);
        if (util != null && util.length() >0)
            sb.append("["+util +"]");
        return sb;
    }
}
