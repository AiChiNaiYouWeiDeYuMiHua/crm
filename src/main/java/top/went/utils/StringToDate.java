package top.went.utils;

import org.springframework.util.StringUtils;
import top.went.exception.InputException;

import java.sql.Date;

/**
 * 日期格式转换工具
 */
public class StringToDate {

    public static Date convert(String source) throws InputException {
        if (StringUtils.isEmpty(source)) {
            return null;
        }
        if (RegularUtils.matchDate(source)) {
            return java.sql.Date.valueOf(source);
        }
        return null;
    }
}
