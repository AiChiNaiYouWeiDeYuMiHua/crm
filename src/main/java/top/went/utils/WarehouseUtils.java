package top.went.utils;

import top.went.exception.ServiceException;
import top.went.pojo.*;
import top.went.service.OrderService;
import top.went.service.PurchaseService;
import top.went.vo.OrderDetail;
import top.went.vo.PurchaseDetailVO;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 仓库工具类
 */
public class WarehouseUtils {
    /**
     * 采购单生成入库单
     * @param purchaseEntity
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    public static WhPutEntity purchaseIn(PurchaseEntity purchaseEntity,UserEntity userEntity)
            throws ServiceException {
        WarehouseEntity warehouseEntity = purchaseEntity.getTbWarehouseByWhId();
        if (warehouseEntity == null)
            throw new ServiceException("仓库信息错误");

        WhPutEntity whPutEntity = new WhPutEntity("采购单-"+purchaseEntity.getPurTheme(),
                Date.valueOf(LocalDate.now()),0,purchaseEntity.getPurRemarks(),false,
                warehouseEntity,userEntity,purchaseEntity
        );
        return whPutEntity;
    }
    /**
     * 采购明细生成入库明细
     * @param purchaseEntity
     * @param w
     */
    public static List<WpDetailEntity> buildWputDetail(PurchaseEntity purchaseEntity,
                                                 WhPutEntity w,PurchaseService purchaseService,UserEntity userEntity)
            throws ServiceException {
        List<PurchaseDetailVO> purchaseDetailVOS = purchaseService.loadDeatil(purchaseEntity.getPurId().longValue());
        purchaseDetailVOS.removeIf(purchaseDetailVO -> purchaseDetailVO.getPdNoIn()<=0);
        if (purchaseDetailVOS.size() <=0)
            throw new ServiceException("没有产品需要入库");
        List<WpDetailEntity> wpDetailEntities = new ArrayList<>();
        for (PurchaseDetailVO orderDetail : purchaseDetailVOS) {
            WpDetailEntity wpDetailEntity = new WpDetailEntity(
                    w.getWpId(),orderDetail.getPdNoIn(),orderDetail.getPdNoIn(),
                    0,1,orderDetail.getPdOther(),orderDetail.getPdPrice(),
                    orderDetail.getPdTotal(),orderDetail.getPdProfit(),orderDetail.getPfId(),
                    w.getTbWarehouseByWhId(),userEntity,purchaseEntity.getTbCustomerByCusId()
            );
            wpDetailEntities.add(wpDetailEntity);
        }
        return wpDetailEntities;
    }

    /**
     * 订单生成出库单
     * @param orderEntity
     * @param userEntity
     * @param warehouseEntity
     * @return
     */
    public static WhPullEntity orderOut(OrderEntity orderEntity,UserEntity userEntity,
                                       WarehouseEntity warehouseEntity) {
        WhPullEntity whPullEntity = new WhPullEntity("订单-"+orderEntity.getOrderTitle(),
                Date.valueOf(LocalDate.now()),0,orderEntity.getOrderOther(),
                false,orderEntity,null,userEntity,warehouseEntity);
        return whPullEntity;
    }

    /**
     * 订单明细生成出库明细
     * @param orderEntity
     * @param w
     * @param orderService
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    public static List<WpDetailEntity> buildWpDetail(OrderEntity orderEntity,
                                                       WhPullEntity w,OrderService orderService,UserEntity userEntity)
            throws ServiceException {
        List<OrderDetail> orderDetails = orderService.loadDeatil(orderEntity.getOrderId().longValue());
        orderDetails.removeIf(orderDetail -> orderDetail.getNoPay()<=0);
        if (orderDetails.size() <=0)
            throw new ServiceException("没有产品需要出库");
        List<WpDetailEntity> wpDetailEntities = new ArrayList<>();
        for (OrderDetail orderDetail : orderDetails) {
            WpDetailEntity wpDetailEntity = new WpDetailEntity(
                    w.getWpullId(),orderDetail.getNoPay(),orderDetail.getNoPay(),
                    0,0,orderDetail.getOther(),orderDetail.getPrice(),
                    orderDetail.getTotal(),orderDetail.getProfit(),orderDetail.getFormatEntity(),
                    w.getTbWarehouseByWhId(),userEntity,orderEntity.getTbCustomerByCusId()
            );
            wpDetailEntities.add(wpDetailEntity);
        }
        return wpDetailEntities;
    }

    /**
     * 采购退货单生成出库单
     * @param returnGoodsEntity
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    public static WhPullEntity returnOut(ReturnGoodsEntity returnGoodsEntity,UserEntity userEntity) throws ServiceException {
        WarehouseEntity warehouseEntity = returnGoodsEntity.getTbPurchaseByPurId().getTbWarehouseByWhId();
        if (warehouseEntity == null)
            throw new ServiceException("仓库信息不正确");
        WhPullEntity whPullEntity = new WhPullEntity("采购退货单-"+returnGoodsEntity.getRgTheme(),
                Date.valueOf(LocalDate.now()),0,"",
                false,null,returnGoodsEntity,userEntity,warehouseEntity);
        return whPullEntity;
    }

    /**
     * 采购退货明细生成出库明细
     * @param returnGoodsEntity
     * @param w
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    public static List<WpDetailEntity> buildWpDetail(ReturnGoodsEntity returnGoodsEntity, WhPullEntity w,
                                                     UserEntity userEntity)
            throws ServiceException {
        List<ReturnGoodsDetailEntity> returnGoodsDetailEntities = (List<ReturnGoodsDetailEntity>)
                returnGoodsEntity.getTbReturnGoodsDetailsByRgId();
        returnGoodsDetailEntities.removeIf(returnGoodsDetailEntity -> returnGoodsDetailEntity.getMagicDelete()>0);
        returnGoodsDetailEntities.removeIf(returnGoodsDetailEntity -> returnGoodsDetailEntity.getRgdNum()<=0);
        if (returnGoodsDetailEntities.size() <=0)
            throw new ServiceException("没有产品需要出库");
        List<WpDetailEntity> wpDetailEntities = new ArrayList<>();
        for (ReturnGoodsDetailEntity returnGoodsDetailEntity : returnGoodsDetailEntities) {
            WpDetailEntity wpDetailEntity = new WpDetailEntity(
                    w.getWpullId(),returnGoodsDetailEntity.getRgdNum(),
                    returnGoodsDetailEntity.getRgdNum(),
                    0,0,"", BigDecimal.ZERO,
                    BigDecimal.valueOf(returnGoodsDetailEntity.getRgdMoney()),BigDecimal.ZERO,
                    returnGoodsDetailEntity.getTbProductFormatByPfId(),
                    w.getTbWarehouseByWhId(),userEntity,returnGoodsEntity.getTbPurchaseByPurId().getTbCustomerByCusId()
            );
            wpDetailEntities.add(wpDetailEntity);
        }
        return wpDetailEntities;
    }
    /**
     * 订单退货单生成入库单
     * @param returnGoodsEntity
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    public static WhPutEntity returnIn(ReturnGoodsEntity returnGoodsEntity,UserEntity userEntity) throws ServiceException {
        WarehouseEntity warehouseEntity = returnGoodsEntity.getWarehouseEntity();
        if (warehouseEntity == null)
            throw new ServiceException("仓库信息不正确");
        WhPutEntity whPutEntity = new WhPutEntity("订单退货单-"+returnGoodsEntity.getRgTheme(),
                Date.valueOf(LocalDate.now()),0,"",
                false,warehouseEntity,userEntity,returnGoodsEntity);
        return whPutEntity;
    }

    /**
     * 订单退货明细生成入库明细
     * @param returnGoodsEntity
     * @param w
     * @param userEntity
     * @return
     * @throws ServiceException
     */
    public static List<WpDetailEntity> buildWpDetail(ReturnGoodsEntity returnGoodsEntity, WhPutEntity w,
                                                     UserEntity userEntity)
            throws ServiceException {
        List<ReturnGoodsDetailEntity> returnGoodsDetailEntities = (List<ReturnGoodsDetailEntity>)
                returnGoodsEntity.getTbReturnGoodsDetailsByRgId();
        returnGoodsDetailEntities.removeIf(returnGoodsDetailEntity -> returnGoodsDetailEntity.getRgdNum()<=0);
        if (returnGoodsDetailEntities.size() <=0)
            throw new ServiceException("没有产品需要入库");
        List<WpDetailEntity> wpDetailEntities = new ArrayList<>();
        for (ReturnGoodsDetailEntity returnGoodsDetailEntity : returnGoodsDetailEntities) {
            WpDetailEntity wpDetailEntity = new WpDetailEntity(
                    w.getWpId(),returnGoodsDetailEntity.getRgdNum().intValue(),
                    returnGoodsDetailEntity.getRgdNum().intValue(),
                    0,1,returnGoodsDetailEntity.getRgdRemarks(),BigDecimal.ZERO,
                    BigDecimal.valueOf(returnGoodsDetailEntity.getRgdMoney()),BigDecimal.ZERO,
                    returnGoodsDetailEntity.getTbProductFormatByPfId().getPfId(), w.getTbWarehouseByWhId(),
                    userEntity,returnGoodsEntity.getTbOrderByOrderId().getTbCustomerByCusId()
            );
            wpDetailEntities.add(wpDetailEntity);
        }
        return wpDetailEntities;
    }
}
