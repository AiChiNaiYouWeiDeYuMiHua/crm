package top.went.utils;


import top.went.exception.InputException;
import top.went.pojo.*;
import top.went.vo.WarehouseDetailVo;


import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 * 检查输入的表单数据
 */
public final class CheckForm {

    /**
     * 检查产品表单数据
     * @return
     */
    public static ProductFormatEntity checkProductForm(ProductFormatEntity formatEntity, boolean type) throws InputException {
        ProductEntity productEntity = formatEntity.getTbProductByProductId();
        if (productEntity == null)
            throw new InputException("表单数据错误");
        if (type && productEntity.getProductId() == null){
            throw new InputException("该规格没有发现产品信息");
        }
        if (type && formatEntity.getPfName() == null)
            throw new InputException("请输入规格名称");
        if (!type && productEntity.getProductName() == null)
            throw new InputException("请输入产品信息");
        if (formatEntity.getPfPrice() == null)
            formatEntity.setPfPrice(BigDecimal.ZERO);
        if (formatEntity.getPfCost() == null)
            formatEntity.setPfCost(BigDecimal.ZERO);
        if (formatEntity.getPfMin() == null)
            formatEntity.setPfMin(0L);
        if (formatEntity.getPfMax() == null)
            formatEntity.setPfMax(0L);
        return formatEntity;
    }

    /**
     * 检查用户表单数据
     *
     * @param userEntity
     * @return
     * @throws InputException
     */
    public static UserEntity checkUser(UserEntity userEntity) throws InputException {
        if (!RegularUtils.matchAccountName(userEntity.getUserName())) {
            throw new InputException("姓名错误");
        }
        if (!RegularUtils.matchIDCODE(userEntity.getUserCode())) {
            userEntity.setUserCode("430621199702060098");
        }
        if (userEntity.getTbDeptByDeptId() == null || userEntity.getTbDeptByDeptId().getDeptId() == null){
            userEntity.setTbDeptByDeptId(null);
        }
        userEntity.setUserPassword(Md5.encode("aaa123"));
        if (userEntity.getUserAddress() == null) {
            userEntity.setUserAddress("株洲");
        }
        if (userEntity.getUserTel() == null) {
            userEntity.setUserTel(null);
        }
        if (userEntity.getUserEmail() == null) {
            userEntity.setUserEmail(null);
        }
        if (userEntity.getHiredate() == null) {
            userEntity.setHiredate(new Date(System.currentTimeMillis()));
        }
        if (userEntity.getUserBirthday() == null) {
            userEntity.setUserBirthday(null);
        }
        if (userEntity.getUserIsDimission() == null) {
            userEntity.setUserIsDimission(0L);
        }
        if (userEntity.getUserCode() == null) {
            userEntity.setUserCode("未填写");
        }
//
//        userEntity.setUserTel(userTel);
//        userEntity.setUserEmail(userEmail == null ? "未填写" : userEmail);
//        userEntity.setHiredate(hiredate == null ? new Date(System.currentTimeMillis()) : hiredate);
//        userEntity.setUserBirthday(birthday == null ? null : birthday);
//        userEntity.setUserIsDimission(1L);
//        userEntity.setOpenid(null);
//        userEntity.setUserCode(userCode);
        return userEntity;
    }

    /**
     * 检查部门表单数据
     *
     * @param deptEntity
     * @return
     * @throws InputException
     */
    public static DeptEntity checkDept(DeptEntity deptEntity) throws InputException {
        if (deptEntity.getDeptName() == null)
            throw new InputException("部门名不能为空");
        if (deptEntity.getDeptIsDel() == null)
            deptEntity.setDeptIsDel(0L);
        return deptEntity;
    }

    /**
     * 检查合同表单数据
     * @param orderEntity
     */
    public static void checkContract(OrderEntity orderEntity) throws InputException {
        CustomerEntity customerEntity = orderEntity.getTbCustomerByCusId();
        if (customerEntity == null || nullId(customerEntity.getCusId())){
            throw new InputException("订单/合同必须有客户");
        }
        SaleOppEntity quoteEntity = orderEntity.getTbQuoteByQuoteId();
        if (quoteEntity != null && nullId(quoteEntity.getOppId()))
            orderEntity.setTbQuoteByQuoteId(null);
        ContactsEntity contactsEntity = orderEntity.getContactsEntity();
        if (contactsEntity != null && nullId(contactsEntity.getCotsId()))
            orderEntity.setContactsEntity(null);
        UserEntity userEntity = orderEntity.getTbUserByTbUserId();
        if (userEntity != null && nullId(userEntity.getUserId()))
            orderEntity.setTbUserByTbUserId(null);
        UserEntity userEntity1 = orderEntity.getTbUserByUserId();
        if (userEntity1 == null || nullId(userEntity1.getUserId()))
            orderEntity.setTbUserByUserId(null);
//            throw new InputException("订单/合同必须有所有者");
        ProjectEntity projectEntity = orderEntity.getProjectEntity();
        if (projectEntity == null || nullId(projectEntity.getProjId()))
            orderEntity.setProjectEntity(null);
        
    }

    /**
     * 检查订单明细
     * @param detailEntities
     * @throws InputException
     */
    public static void checkOrderDetail(List<OrderDetailEntity> detailEntities) throws InputException {
        if (detailEntities != null) {
            for (OrderDetailEntity detailEntity : detailEntities) {
                if (detailEntity.getFormatEntity() == null || detailEntity.getFormatEntity().getPfId() == null) {
                    throw new InputException("明细数据错误");
                }
                if (detailEntity.getOdNumber() == null || detailEntity.getOdNumber() < 0)
                    throw new InputException("产品数量错误");
                if (detailEntity.getOdTotal() == null || detailEntity.getOdTotal().compareTo(BigDecimal.ZERO) == -1)
                    throw new InputException("总价错误");
                if (detailEntity.getOdNumber() <0 || detailEntity.getOdNumber() > 100000000){
                    throw new InputException("数量信息正确");
                }
                if (detailEntity.getOdTotal().compareTo(BigDecimal.ZERO) < 0
                        || detailEntity.getOdTotal().compareTo(BigDecimal.valueOf(10000000000L)) > 0){
                    throw new InputException("总价格信息不正确");
                }
            }
        }
    }

    public static void checkOrder(OrderEntity orderEntity) {
    }

    /**
     * 判断ID是否为空
     * @param string
     * @return
     */
    public static boolean nullId(Long  string){
        return string == null || string.toString().length() <=0;
    }
    /**
     * 判断ID是否为空
     * @param string
     * @return
     */
    public static boolean nullId(Integer  string){
        return string == null || string.toString().length() <=0;
    }

    /**
     * 检查出库单
     * @param whPullEntity
     */
    public static void checkWareHouseOut(WhPullEntity whPullEntity) throws InputException {
        if (whPullEntity.getWpullName() == null  || whPullEntity.getWpullName().length()<=0){
            throw new InputException("请填写出库单标题");
        }
        if (whPullEntity.getWpullName().length() >= 20)
            throw new InputException("出库单标题过长");
        if (whPullEntity.getWpullStatus() != null && (whPullEntity.getWpullStatus()<0 ||
                whPullEntity.getWpullStatus() >2))
            throw new InputException("出库单状态不正确");
        OrderEntity orderEntity = whPullEntity.getTbOrderByOrderId();
        if (orderEntity != null && nullId(orderEntity.getOrderId()))
            whPullEntity.setTbOrderByOrderId(null);
        ReturnGoodsEntity returnGoodsEntity = whPullEntity.getTbReturnGoodsByRgId();
        if (returnGoodsEntity != null && nullId(returnGoodsEntity.getRgId()))
            whPullEntity.setTbReturnGoodsByRgId(null);
        WarehouseEntity warehouseEntity = whPullEntity.getTbWarehouseByWhId();
        if (warehouseEntity == null || nullId(warehouseEntity.getWhId()))
            throw new InputException("请选择出库仓库");
        UserEntity userEntity = whPullEntity.getTbUserByUserId();
        if (userEntity == null || nullId(userEntity.getUserId()))
            throw new InputException("请选择经手人");
        UserEntity userEntity1 = whPullEntity.getTbUserByTbUserId();
        if (userEntity1 != null && nullId(userEntity1.getUserId()))
            whPullEntity.setTbUserByTbUserId(null);
        if (whPullEntity.getTbOrderByOrderId()!=null&&whPullEntity.getTbReturnGoodsByRgId()!=null)
            throw new InputException("出库单只能对应一张订单或者一张采购单");

    }

    /**
     * 检查入库单
     * @param whPutEntity
     */
    public static void checkWareHouseIn(WhPutEntity whPutEntity) throws InputException {
        if (whPutEntity.getWpName() == null  || whPutEntity.getWpName().length()<=0){
            throw new InputException("请填写入库单标题");
        }
        if (whPutEntity.getWpName().length() >= 20)
            throw new InputException("出库单标题过长");
        if (whPutEntity.getWpStatus() != null && (whPutEntity.getWpStatus()<0 ||
                whPutEntity.getWpStatus() >2))
            throw new InputException("出库单状态不正确");
        PurchaseEntity purchaseEntity = whPutEntity.getTbPurchaseByPurId();
        if (purchaseEntity != null && nullId(purchaseEntity.getPurId()))
            whPutEntity.setTbPurchaseByPurId(null);
        ReturnGoodsEntity returnGoodsEntity = whPutEntity.getTbReturnGoodsByRgId();
        if (returnGoodsEntity != null && nullId(returnGoodsEntity.getRgId()))
            whPutEntity.setTbReturnGoodsByRgId(null);
        WarehouseEntity warehouseEntity = whPutEntity.getTbWarehouseByWhId();
        if (warehouseEntity == null || nullId(warehouseEntity.getWhId()))
            throw new InputException("请选择出库仓库");

        UserEntity userEntity1 = whPutEntity.getTbUserByTbUserId();
        if (userEntity1 != null && nullId(userEntity1.getUserId()))
            whPutEntity.setTbUserByTbUserId(null);
        if (whPutEntity.getTbPurchaseByPurId()!=null&&whPutEntity.getTbReturnGoodsByRgId()!=null)
            throw new InputException("入库单只能对应一张采购单或者一张退货单");
    }

    public static void checkWareHouseDetail(WarehouseDetailVo vos) throws InputException {
        if (vos.getId() == null || vos.getType() == null)
            throw new InputException("明细数据错误");
        if (vos.getDatas()!= null)
            for (WpDetailEntity wpDetailEntity : vos.getDatas()) {
                if (wpDetailEntity.getTbProductFormatByPfId() == null || nullId(wpDetailEntity.getTbProductFormatByPfId().getPfId()))
                    throw new InputException("明细数据错误");
            }
    }

    public static void checkDeliver(DeliverGoodsEntity goodsEntity) {
    }

    /**
     * 添加交付计划表单
     * @param detailEntity
     * @throws InputException
     */
    public static void checkOrderDetailOne(OrderDetailEntity detailEntity) throws InputException {
        if (detailEntity == null)
            throw new InputException("请输入数据");
        if (detailEntity.getFormatEntity() == null || nullId(detailEntity.getFormatEntity().getPfId()))
            throw new InputException("产品信息错误");
        if (detailEntity.getTbOrderByOrderId() == null || nullId(detailEntity.getTbOrderByOrderId().getOrderId()))
            throw new InputException("合同信息错误");
        if (detailEntity.getFormatEntity() == null || detailEntity.getFormatEntity().getPfId() == null) {
            throw new InputException("明细数据错误");
        }
        if (detailEntity.getOdNumber() == null || detailEntity.getOdNumber() < 0)
            throw new InputException("产品数量错误");
        if (detailEntity.getOdTotal() == null || detailEntity.getOdTotal().compareTo(BigDecimal.ZERO) == -1)
            throw new InputException("总价错误");
        if (detailEntity.getOdNumber() <0 || detailEntity.getOdNumber() > 100000000){
            throw new InputException("数量信息错误");
        }
        if (detailEntity.getOdTotal().compareTo(BigDecimal.ZERO) < 0
                || detailEntity.getOdTotal().compareTo(BigDecimal.valueOf(10000000000L)) > 0){
            throw new InputException("总价格信息不正确");
        }
    }

    /**
     * 添加交付记录表单
     * @param deliverDetailEntity
     */
    public static void checkSendDetailOne(DeliverDetailEntity deliverDetailEntity) throws InputException {
        if (deliverDetailEntity == null)
            throw new InputException("请输入数据");
        if (deliverDetailEntity.getFormatEntity() == null || nullId(deliverDetailEntity.getFormatEntity().getPfId()))
            throw new InputException("产品信息错误");
        if (deliverDetailEntity.getTbOrderByOrderId() == null || nullId(deliverDetailEntity.getTbOrderByOrderId().getOrderId()))
            throw new InputException("合同信息错误");
        if (deliverDetailEntity.getFormatEntity() == null || deliverDetailEntity.getFormatEntity().getPfId() == null) {
            throw new InputException("明细数据错误");
        }
        if (deliverDetailEntity.getDdNumber() == null || deliverDetailEntity.getDdNumber() < 0)
            throw new InputException("产品数量错误");
        if (deliverDetailEntity.getDdTotal() == null || deliverDetailEntity.getDdTotal().compareTo(BigDecimal.ZERO) == -1)
            throw new InputException("总价错误");
        if (deliverDetailEntity.getDdNumber() <0 || deliverDetailEntity.getDdNumber() > 100000000){
            throw new InputException("数量信息错误");
        }
        if (deliverDetailEntity.getDdTotal().compareTo(BigDecimal.ZERO) < 0
                || deliverDetailEntity.getDdTotal().compareTo(BigDecimal.valueOf(10000000000L)) > 0){
            throw new InputException("总价格信息不正确");
        }
    }

    public static void checkApprovalLog(ApprovalLogsEntity logsEntity) {
    }
}
