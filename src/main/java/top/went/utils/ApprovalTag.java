package top.went.utils;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.tags.RequestContextAwareTag;
import top.went.service.ApprovalService;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import java.math.BigDecimal;
import java.util.List;


/**
 * 审批
 */
public class ApprovalTag extends RequestContextAwareTag {
    private static ApprovalService approvalService;

    private String html = "<div id=\"modal-boss\"></div><div id=\"modal-approval\"></div>" +
            "<div id=\"modal-ok\"></div><div id=\"modal-no\"></div><div id=\"modal-unlock\"></div><div id=\"modal-cancel\"></div> <div class=\"text-center m-b-10\">\n" +
            "                <span class=\"text-gray\">审批过程:</span>\n" +
            "                <span class=\"m-l-10\">1. 待申请</span>\n" +
            "                <i class=\"glyphicon glyphicon-arrow-right  text-gray m-20 vertical-align\"></i>\n" +
            "                2. 待审批\n" +
            "                <i class=\"glyphicon glyphicon-arrow-right  text-gray m-20 vertical-align\"></i>\n" +
            "                3.否决\n" +
            "                <i class=\"glyphicon glyphicon-arrow-right  text-gray m-20 vertical-align\"></i>\n" +
            "                4.执行\n" +
            "            </div>" +
            "            <div class=\" text-center text-gray f-s-12 m-b-30\">" +
            "                <div class=\"icon-box\"><a class=\"text-primary f-s-12\" %s data-toggle=\"modal\"\n" +
            "                                         data-target=\"#boss\"><i class=\"fa fa-check fa-lg\"></i><br>直接同意</a></div>　\n" +
            "                <div class=\"icon-box\">\n" +
            "                    <a class=\"text-primary f-s-12\" %s data-toggle=\"modal\" data-target=\"#apply\"><i class=\"fa fa-hand-paper-o\"></i><br>申请审批</a>\n" +
            "                </div>" +
            "                <div class=\"icon-box border-r\"> <a class=\"text-primary f-s-12\" %s data-toggle=\"modal\" data-target=\"#cancel\"><i class=\"fa fa-reply-all\"></i><br>撤销审批</a></div>" +
            "                <div class=\"icon-box\"> <a class=\"text-primary f-s-12\" %s data-toggle=\"modal\" data-target=\"#enter\"><i class=\"fa fa-check-circle-o fa-lg\"></i><br>同意</a></div>" +
            "                <div class=\"icon-box border-r\"> <a class=\"text-primary f-s-12\" %s data-toggle=\"modal\" data-target=\"#veto\"><i class=\"fa fa-times-circle fa-lg\"></i><br>否决</a></div>" +
            "<div class=\"icon-box\"> <a class=\"text-primary f-s-12\" data-toggle=\"modal\" data-target=\"#unlock\"><i class=\"fa fa-unlock fa-lg\"></i><br>解锁</a></div>"+
            "            </div>";


    private Long entity;
    private Integer status;
    private String type;

    public Long getEntity() {
        return entity;
    }

    public void setEntity(Long entity) {
        this.entity = entity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    protected int doStartTagInternal() throws Exception {
        if (approvalService == null){
            WebApplicationContext applicationContext = this.getRequestContext().getWebApplicationContext();
            approvalService = applicationContext.getBean(ApprovalService.class);
        }
        List<BigDecimal> funIds = (List<BigDecimal>) this.pageContext.getSession().getAttribute("funIds");
        boolean approval = false;
        for (BigDecimal funId : funIds) {
            if (funId.compareTo(BigDecimal.valueOf(33))==0){
                approval = true;
                break;
            }
        }
        JspWriter out = pageContext.getOut();
        out.print(buildHtml(approval));
        out.flush();
        return 0;
    }

    private String buildHtml(boolean approval) throws JspException {
        String script = "<script>window.onload = function(){";
        String re = new String(html);
        String style = "style=\"color:#505458;\" onmouseover=\"#505458\"";
        switch (status){
            case 0:
                re = re.replace("1. 待申请","<b>1. 待申请</b>");
                re = String.format(re,approval?"href=\"#boss\"":style,"href=\"#apply\"",style,style,style,style);
                if (approval)
                    script += "$('#modal-boss').load('/approval/boss?id="+entity+"&type="+type+"');";
                script += "$('#modal-approval').load('/approval/approval?id="+entity+"&type="+type+"');";
                break;
            case 1:
                re = re.replace("2. 待审批","<b>2. 待审批</b>");
                re = String.format(re,approval?"href=\"#boss\"":style,style,"href=\"#cancel\"",
                        approval?"href=\"#enter\"":style,approval?"href=\"#veto\"":style,style);
                if (approval) {
                    script += "$('#modal-boss').load('/approval/boss?id=" + entity + "&type=" + type + "');";
                    script += "$('#modal-ok').load('/approval/ok?id=" + entity + "&type=" + type + "');";
                    script += "$('#modal-no').load('/approval/no?id=" + entity + "&type=" + type + "');";
                }
                script += "$('#modal-approval').load('/approval/cancel?id=" + entity + "&type=" + type + "');";
                break;
            case 3:
                re = re.replace("3.否决","<b>3.否决</b>");
                re = String.format(re,approval?"href=\"#boss\"":style,"href=\"#apply\"",style,style,style,style);
                if (approval)
                    script += "$('#modal-boss').load('/approval/boss?id="+entity+"&type="+type+"');";
                script += "$('#modal-approval').load('/approval/approval?id="+entity+"&type="+type+"');";
                break;
            case 2:
                re = re.replace("4.执行","<b>4.执行</b>");
                re = String.format(re,style,style,style,style,style,approval?"href=\"#unlock\"":style);
                if (approval)
                        script += "$('#modal-no').load('/approval/unlock?id="+entity+"&type="+type+"');";
                break;
            default:
                throw new JspException("请输入正确的状态");
        }
        script += "}</script>";
        script +="<script src=\"/js/sale/approval.js\"></script>";
        re += script;
        return re;
    }
}
