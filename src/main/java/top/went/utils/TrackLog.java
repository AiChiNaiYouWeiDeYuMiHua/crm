package top.went.utils;

import top.went.pojo.SaleOppEntity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TrackLog {

    public static String dateToString(java.sql.Date date){
        if(date == null){
            return "";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(date);
    }

    public static String getTrack(SaleOppEntity saleOppEntity, String stage){
        String dateString = dateToString(new java.sql.Date(new Date().getTime()));
        String possibility = saleOppEntity.getPossibility();
        String expectedTime = dateToString(saleOppEntity.getExpectedTime());
        Double expectedAmount = saleOppEntity.getExpectedAmount();
        String stageRemarks = saleOppEntity.getStageRemarks();
//        String userName1 = saleOppEntity.getTbUserByUserId().getUserName();
        String trackLog = saleOppEntity.getTrackLog();
        StringBuffer sb = new StringBuffer();
        if(trackLog == null){
            trackLog = "";
            sb.append("日期：");
        } else {
            sb.append("<br>日期:");
        }
        if(dateString != null &&dateString.length()>0){
            sb.append(dateString);
        }
        sb.append(" 阶段:");
        if(stage !=null && stage.length()>0){
            sb.append(stage);
        }
        if(possibility != null && possibility.length()>0){
            sb.append("(");
            sb.append(possibility);
            sb.append(") ");
        }
//        if(userName1 != null && userName1.length() >0){
//            sb.append(userName1);
//        }
        sb.append(" 预计:");
        if(expectedTime != null && expectedTime.length() >0){
            sb.append(expectedTime);
        }
        if(expectedAmount != null){
            sb.append("(￥ ");
            sb.append(expectedAmount);
            sb.append(")");
        }
        sb.append(" 备注:");
        if(stageRemarks != null && stageRemarks.length() >0){
            sb.append(stageRemarks);
        }
        return trackLog+sb.toString();
    };

}
