package top.went.utils;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 自定义权限标签处理类
 */
public class GrantTag extends SimpleTagSupport {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) this.getJspContext();
        HttpSession session = pageContext.getSession();
        List<BigDecimal> funIds = (List<BigDecimal>) session.getAttribute("funIds");
//        List<Long> a = new ArrayList<Long>();
//        for (BigDecimal b : funIds) {
//            a.add(b.longValue());
//        }
        System.out.println("自定义标签" + funIds /*+ "类型：" + a.get(1).getClass().getName()*/);
//        System.out.println("标签id：" + id + "类型" + id.getClass().getName());
        for (int i = 0; i < funIds.size(); i++) {
            if (funIds.get(i).longValue() == id) {
                System.out.println("拦截的id" + id);
                this.getJspBody().invoke(null);//原样输出标签体的内容
            }
        }
    }
}
