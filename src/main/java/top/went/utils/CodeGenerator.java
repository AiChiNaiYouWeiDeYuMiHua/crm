package top.went.utils;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

/**
 * 编号生成器
 */
public final class CodeGenerator {

    /**
     * 生成订单号
     * @param type
     * @return
     */
    public static final String orderGenerator(boolean type){
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMddHHmmss");
        return dateTime.format(formatter)+randStr(4)+(type?"01":"00");
    }

    /**
     * 随机数
     * @param number
     * @return
     */
    private static String randStr(int number){
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < number; i++) {
            sb.append(random.nextInt(10));
            System.out.println(sb);
        }
        return String.valueOf(sb);
    }
}
