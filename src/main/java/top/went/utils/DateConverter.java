package top.went.utils;

import org.springframework.core.convert.converter.Converter;

import java.util.Date;

public class DateConverter implements Converter<String, Date> {

    @Override
    public Date convert(String s) {
        return java.sql.Date.valueOf(s);
    }
}
