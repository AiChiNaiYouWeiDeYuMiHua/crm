package top.went.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

import java.sql.Date;


/**
 * String转成Date的转换器
 */
public class StringToDateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        System.out.println(1);
        if (StringUtils.isEmpty(source)) {
            return null;
        }
        if (!source.matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {
            return null;
        }
        return java.sql.Date.valueOf(source);
    }
}
