package top.went.db.mapper;

import top.went.pojo.MaintainOrderEntity;
import top.went.pojo.PurDetailEntity;
import top.went.vo.MaintainVO;
import top.went.vo.PurchaseDetailVO;

import java.util.List;

public interface PurchaseDetailMapper {
    /**
     * 高级查询
     * @param purchaseDetailVO
     * @return
     */
    public List<PurDetailEntity> findByManyCondition(PurchaseDetailVO purchaseDetailVO);

}
