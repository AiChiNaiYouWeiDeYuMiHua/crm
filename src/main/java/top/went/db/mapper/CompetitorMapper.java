package top.went.db.mapper;

import top.went.pojo.CompetitorEntity;
import top.went.vo.CompetitorVo;

import java.util.List;

public interface CompetitorMapper {
    /**
     * 竞争对手高级查询
     * @param competitorVo
     * @return
     */
    public List<CompetitorEntity> findAllSeniorSearch(CompetitorVo competitorVo);
}
