package top.went.db.mapper;

import top.went.pojo.OrderDetailEntity;
import top.went.vo.OrderDetail;
import top.went.vo.OrderDetailSearch;

import java.util.List;

/**
 * 交付计划
 */
public interface OrderDetailMapper {
    List<OrderDetail> findAll(OrderDetailSearch search);

    List<OrderDetailEntity> findAllByCus(Long id);
}
