package top.went.db.mapper;


import top.went.vo.Product;
import top.went.vo.ProductSearch;

import java.util.List;

/**
 * 产品mapper
 */
public interface ProductMapper {
    List<Product> findAllSeniorSearch(ProductSearch search);
    Product load(Long id);
    List<Product> loadFormatter(Long id);
}
