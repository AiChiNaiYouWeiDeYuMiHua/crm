package top.went.db.mapper;

import org.apache.ibatis.annotations.Param;
import top.went.vo.ReturnGoodsDetailVo;
import top.went.vo.ReturnGoodsVO;

import java.util.List;

public interface ReturnGoodsDetailMapper {
    /**
     * 查询所有
     * @param returnGoodsDetailVo
     * @return
     */
    List<ReturnGoodsDetailVo> findByManyCondition(ReturnGoodsDetailVo returnGoodsDetailVo);

    List<ReturnGoodsDetailVo> loadDetails(@Param("rgId") Integer rgId, @Param("type") Integer type);
}
