package top.went.db.mapper;

import top.went.pojo.MaintainOrderEntity;
import top.went.pojo.PurchaseEntity;
import top.went.vo.MaintainVO;
import top.went.vo.PurchaseVO;

import java.util.List;

public interface PurchaseMapper {
    /**
     * 高级查询
     * @param purchaseVO
     * @return
     */
    public List<PurchaseEntity> findByManyCondition(PurchaseVO purchaseVO);

}
