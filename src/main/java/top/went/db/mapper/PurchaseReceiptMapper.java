package top.went.db.mapper;

import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PurchaseReceiptEntity;
import top.went.vo.PaymentRecordVO;
import top.went.vo.PurchaseReceiptVO;

import java.util.List;

public interface PurchaseReceiptMapper {
    /**
     * 高级查询
     * @param purchaseReceiptVO
     * @return
     */
    public List<PurchaseReceiptEntity> findByManyCondition(PurchaseReceiptVO purchaseReceiptVO);

}
