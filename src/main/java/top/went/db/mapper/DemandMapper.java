package top.went.db.mapper;

import top.went.pojo.DemandEntity;
import top.went.vo.DemandVo;

import java.sql.Date;
import java.util.List;

/**
 * 需求
 */
public interface DemandMapper {
    /**
     * 需求高级查询
     * @param demandVo
     * @return
     */
    List<DemandEntity> findAllSeniorSearch(DemandVo demandVo);


}
