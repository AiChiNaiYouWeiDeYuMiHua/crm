package top.went.db.mapper;

import top.went.pojo.ProjectEntity;
import top.went.vo.ProjectVo;

import java.util.List;

public interface ProjectMapper {
    public List<ProjectEntity> findByParam(ProjectVo projectVo);

    public List<ProjectVo> findProj(Integer id);
}
