package top.went.db.mapper;

import top.went.pojo.SaleOppEntity;
import top.went.vo.SaleOppVo;

import java.util.List;
import java.util.Map;

/**
 * 销售机会
 */
public interface SaleOppMapper {
    /**
     * 高级查询
     * @param saleOppVo
     * @return
     */
    List<SaleOppEntity> findAllSeniorSearch(SaleOppVo saleOppVo);

    /**
     * 根据销售机会id查看视图
     * @param oppId
     * @return
     */
    public SaleOppEntity loadSaleOppDetailById(Long oppId);

    /**
     * 客户视图
     * @param cusId
     * @return
     */
    public List<SaleOppEntity> loadAllByCusId(Long cusId);


    /**
     * 销售机会饼状图统计
     * @param state
     * @return
     */
    public List<Map> findForStatistic(String state);

}
