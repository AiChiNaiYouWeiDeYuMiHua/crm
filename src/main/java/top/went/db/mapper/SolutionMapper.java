package top.went.db.mapper;

import top.went.pojo.SolutionEntity;
import top.went.vo.SolutionVo;

import java.util.List;

public interface SolutionMapper {
    /**
     * 解决方案高级查询
     * @return
     */
    public List<SolutionEntity> findAllSeniorSearch(SolutionVo solutionVo);
}
