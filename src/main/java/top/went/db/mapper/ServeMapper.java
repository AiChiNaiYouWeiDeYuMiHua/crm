package top.went.db.mapper;

import top.went.pojo.ServeEntity;
import top.went.vo.ServeVo;

import java.util.List;

public interface ServeMapper {
    public List<ServeEntity> findByParam(ServeVo serveVo);

    public List<ServeVo> find(Integer id);
}
