package top.went.db.mapper;

import top.went.pojo.QuoteDetailEntity;
import top.went.vo.QuoteDetailVo;

import java.util.List;

/**
 * 报价明细Mapper
 */
public interface QuoteDetailMapper {

    /**
     * 报价明细高级查询
     * @param quoteDetailVo
     * @return
     */
    public List<QuoteDetailEntity> findAllSeniorSearch(QuoteDetailVo quoteDetailVo);

}
