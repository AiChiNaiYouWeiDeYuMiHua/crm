package top.went.db.mapper;

import top.went.pojo.MaintainOrderEntity;
import top.went.pojo.PlanPayDetailEntity;
import top.went.vo.MaintainVO;
import top.went.vo.PlanPayDetailVO;

import java.util.List;

public interface MaintainMapper {
    /**
     * 高级查询
     * @param maintainVO
     * @return
     */
    public List<MaintainOrderEntity> findByManyCondition(MaintainVO maintainVO);

    /**
     * 联合查询
     * @param maintainVO
     * @return
     */
    public List<MaintainOrderEntity>  findByStateAndTheme(MaintainVO maintainVO);
}
