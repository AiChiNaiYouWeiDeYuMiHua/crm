package top.went.db.mapper;

import top.went.pojo.ComplaintEntity;
import top.went.vo.ComplaintVo;

import java.util.List;

public interface ComplaintMapper {
    public List<ComplaintEntity> findByParam(ComplaintVo complaintVo);

    public List<ComplaintVo> find(Integer id);
}
