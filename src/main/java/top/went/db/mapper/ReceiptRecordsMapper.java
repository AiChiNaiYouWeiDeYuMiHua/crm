package top.went.db.mapper;

import top.went.pojo.PurchaseReceiptEntity;
import top.went.pojo.ReceiptRecordsEntity;
import top.went.vo.PurchaseReceiptVO;
import top.went.vo.ReceiptRecordsVO;

import java.util.List;

public interface ReceiptRecordsMapper {
    /**
     * 高级查询
     * @param receiptRecordsVO
     * @return
     */
    public List<ReceiptRecordsEntity> findByManyCondition(ReceiptRecordsVO receiptRecordsVO);
}
