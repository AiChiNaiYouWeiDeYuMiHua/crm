package top.went.db.mapper;

import top.went.vo.InStorageSearch;
import top.went.vo.InStorageVo;

import java.util.List;

/**
 * 入库单
 */
public interface InStorageMapper {
    List<InStorageVo> findAll(InStorageSearch search);
    InStorageVo load(Integer id);
}
