package top.went.db.mapper;

import top.went.pojo.DeliverDetailEntity;
import top.went.vo.DeliverDetailVo;
import top.went.vo.SendGoodsDetail;

import java.util.List;

/**
 * 发货明细
 */
public interface DeliverDetailMapper {
    List<DeliverDetailEntity> findAll(SendGoodsDetail search);

    DeliverDetailEntity loadOne(Long id);

    List<DeliverDetailEntity> loadDetail(Long id);
    List<DeliverDetailVo> loadDetailByorder(Long id);

    List<DeliverDetailVo> loadDetailByCus(Long id);
}
