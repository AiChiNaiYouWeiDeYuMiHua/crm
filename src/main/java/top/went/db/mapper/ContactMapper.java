package top.went.db.mapper;

import top.went.pojo.ContactsEntity;
import top.went.vo.ContactVo;

import java.util.List;

public interface ContactMapper {
    public List<ContactsEntity> findByParam(ContactVo contactVo);

    public List<ContactsEntity> simpleFind(String data);
}
