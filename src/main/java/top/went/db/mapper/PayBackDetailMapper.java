package top.went.db.mapper;

import top.went.pojo.PayBackDetailEntity;
import top.went.pojo.PlanPayDetailEntity;
import top.went.vo.PayBackDetailVO;
import top.went.vo.PlanPayDetailVO;

import java.util.List;

public interface PayBackDetailMapper {
    /**
     * 高级查询
     * @param payBackDetailVO
     * @return
     */
    public List<PayBackDetailEntity> findByManyCondition(PayBackDetailVO payBackDetailVO);

}
