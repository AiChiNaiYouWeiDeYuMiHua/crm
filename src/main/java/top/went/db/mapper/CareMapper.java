package top.went.db.mapper;

import top.went.pojo.CareEntity;
import top.went.vo.CareVo;

import java.util.List;

public interface CareMapper {
    public List<CareEntity> findByParam(CareVo careVo);

    public List<CareVo> find(Integer id);
}
