package top.went.db.mapper;

import top.went.vo.SendGoodsSearch;
import top.went.vo.SendGoodsVo;

import java.util.List;

/**
 * 发货单
 */
public interface SendGoodsMapper {
    List<SendGoodsVo> findAll(SendGoodsSearch search);

    SendGoodsVo loadOne(Long id);

    List<SendGoodsVo> loadByOrder(Long id);

    List<SendGoodsVo> loadSendByCus(Long id);
}
