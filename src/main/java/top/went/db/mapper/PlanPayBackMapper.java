package top.went.db.mapper;

import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PlanPayBackEntity;
import top.went.vo.PaymentRecordVO;
import top.went.vo.PlanPayBackVO;

import java.util.List;

public interface PlanPayBackMapper {
    /**
     * 高级查询
     * @param planPayBackVO
     * @return
     */
    public List<PlanPayBackEntity> findByManyCondition(PlanPayBackVO planPayBackVO);
    /**
     * 高级查询数量
     * @param  planPayBackVO
     * @return
     */
    public int  findByManyConditionintCount(PlanPayBackVO planPayBackVO);
}
