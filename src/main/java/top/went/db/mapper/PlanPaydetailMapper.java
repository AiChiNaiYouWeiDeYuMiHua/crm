package top.went.db.mapper;

import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PlanPayDetailEntity;
import top.went.vo.PaymentRecordVO;
import top.went.vo.PlanPayDetailVO;

import java.util.List;

public interface PlanPaydetailMapper {
    /**
     * 高级查询
     * @param planPayDetailVO
     * @return
     */
    public List<PlanPayDetailEntity> findByManyCondition(PlanPayDetailVO planPayDetailVO);
    /**
     * 高级查询数量
     * @param planPayDetailVO
     * @return
     */
//    public int  findByManyConditionintCount(PlanPayDetailVO planPayDetailVO);
}
