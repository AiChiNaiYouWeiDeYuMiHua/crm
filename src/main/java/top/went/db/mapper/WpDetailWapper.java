package top.went.db.mapper;

import org.apache.ibatis.annotations.Param;
import top.went.pojo.WpDetailEntity;
import top.went.vo.DetailOutVo;
import top.went.vo.WpDeatilSearch;

import java.util.List;

/**
 * 库存流水
 */
public interface WpDetailWapper {
    List<WpDetailEntity> findAll(WpDeatilSearch search);
    List<DetailOutVo> findDetail(@Param("id") Long id, @Param("type") Integer type);
}
