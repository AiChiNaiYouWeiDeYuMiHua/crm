package top.went.db.mapper;

import top.went.pojo.WhNumberEntity;
import top.went.vo.WareHouse;

import java.util.List;

/**
 * 库存查询
 */
public interface WareHouseMapper {
    public List<WhNumberEntity> findAllSeniorSearch(WareHouse wareHouse);
    public List<WhNumberEntity> findAllSimpleSearch(String text);
}
