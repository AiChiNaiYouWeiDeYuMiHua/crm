package top.went.db.mapper;

import org.springframework.data.repository.CrudRepository;
import top.went.pojo.OrderEntity;
import top.went.vo.OrderSearch;
import top.went.vo.OrderVo;

import java.util.List;

/**
 * 订单
 */
public interface OrderMapper {
    /**
     * 查询所有
     * @param search
     * @return
     */
    List<OrderVo> findAll(OrderSearch search);

    OrderVo load(Long id);

    List<OrderVo> findByCus(Integer id);
}
