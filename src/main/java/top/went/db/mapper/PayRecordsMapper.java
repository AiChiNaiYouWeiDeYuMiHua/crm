package top.went.db.mapper;

import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PlanPayDetailEntity;
import top.went.vo.PaymentRecordVO;
import top.went.vo.PlanPayDetailVO;

import java.util.List;

public interface PayRecordsMapper {
    /**
     * 高级查询
     * @param paymentRecordVO
     * @return
     */
    public List<PaymentRecordsEntity> findByManyCondition(PaymentRecordVO paymentRecordVO);
    /**
     * 高级查询数量
     * @param  paymentRecordVO
     * @return
     */
    public int  findByManyConditionintCount(PaymentRecordVO paymentRecordVO);
    /**
     * 高级查询数量
     * @param  paymentRecordVO
     * @return
     */
    public int  findByManyConditionintCount1(PaymentRecordVO paymentRecordVO);
}
