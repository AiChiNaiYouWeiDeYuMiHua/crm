package top.went.db.mapper;

import top.went.vo.OrderSearch;
import top.went.vo.OrderVo;
import top.went.vo.ReturnGoodsVO;

import java.util.List;

public interface ReturnGoodsMapper {
    /**
     * 查询所有
     * @param returnGoodsVO
     * @return
     */
    List<ReturnGoodsVO> findAll(ReturnGoodsVO returnGoodsVO);

    ReturnGoodsVO load(Long id);
}
