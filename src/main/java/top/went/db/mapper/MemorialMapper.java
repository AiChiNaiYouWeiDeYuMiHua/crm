package top.went.db.mapper;

import top.went.pojo.ContactsEntity;
import top.went.pojo.MemorialDayEntity;
import top.went.vo.ContactVo;
import top.went.vo.MemorialVo;
import top.went.vo.ProjectVo;

import java.util.List;

public interface MemorialMapper {
    public List<MemorialDayEntity> findByParam(MemorialVo memorialVo);

    public List<MemorialVo> find(Integer id);

    public List<ProjectVo> findProj(Integer id);
}
