package top.went.db.mapper;

import top.went.pojo.ThreeEntity;

import java.util.List;
import java.util.Map;

public interface ThreeMapper {
    public List<ThreeEntity> findForStatistic(String state);
}
