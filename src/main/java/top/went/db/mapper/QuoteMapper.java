package top.went.db.mapper;

import top.went.pojo.QuoteEntity;
import top.went.vo.QuoteVo;

import java.util.List;

/**
 * 报价Mapper
 */
public interface QuoteMapper {
    /**
     * 报价高级查询
     * @param quoteVo
     * @return
     */
    public List<QuoteEntity> findAllSeniorSearch(QuoteVo quoteVo);

    /**
     * 根据报价id获取报价视图
     * @param quoteId
     * @return
     */
    public QuoteEntity loadDetailById(Long quoteId);

}
