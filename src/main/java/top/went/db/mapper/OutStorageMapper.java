package top.went.db.mapper;

import org.omg.CORBA.INTERNAL;
import top.went.vo.ChooseWarehouseVo;
import top.went.vo.OutStorageSearch;
import top.went.vo.OutStorageVo;

import java.util.List;

/**
 * 出库
 */
public interface OutStorageMapper {
    List<OutStorageVo> findAll(OutStorageSearch search);
    OutStorageVo load(Integer id);
    List<ChooseWarehouseVo> loadProductOut(Long id);
}
