package top.went.db.mapper;

import top.went.pojo.CustomerEntity;
import top.went.vo.CustomerVo;

import java.util.List;
import java.util.Map;

public interface CustomerMapper {

    public List<CustomerEntity> findByParam(CustomerEntity customerEntity);

    public List<CustomerEntity> findByKids(CustomerVo customerVo);

    public List<Map> findForStatistic(String state);

}
