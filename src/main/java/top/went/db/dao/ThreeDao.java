package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.ThreeEntity;

import java.util.List;

public interface ThreeDao extends CrudRepository<ThreeEntity,Integer> {
    @Query("from ThreeEntity c where c.tbCustomerByCusId.cusId = ?1 order by c.threeId desc")
    public List<ThreeEntity> findByCusId(Integer id);
}
