package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.MemorialDayEntity;
import top.went.vo.MemorialVo;

import java.util.List;

public interface MemorialDao extends CrudRepository<MemorialDayEntity,Integer> {
    @Query("from MemorialDayEntity m where m.medId = ?1 and m.medIsDelete = 1")
    public MemorialDayEntity findByMedId(Integer id);

    @Query(nativeQuery = true,value = "select COTS_NAME,COTS_ID from TB_CONTACTS where COTS_ID in (select COTS_ID from TB_CONTACTS where CUS_ID = ?1)")
    public List<MemorialVo> findByCusId(Integer id);

    @Query("from MemorialDayEntity c where c.tbCustomerByCusId.cusId = ?1 and c.medIsDelete=1 order by c.medId desc")
    public List<MemorialDayEntity> findAllByCusId(Integer id);
}
