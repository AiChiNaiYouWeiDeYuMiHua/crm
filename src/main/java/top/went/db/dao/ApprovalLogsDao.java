package top.went.db.dao;

import org.springframework.data.repository.CrudRepository;
import top.went.pojo.ApprovalLogsEntity;

public interface ApprovalLogsDao extends CrudRepository<ApprovalLogsEntity,Integer> {
}
