package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.OrderEntity;
import top.went.pojo.WhPullEntity;

/**
 * 出库
 */
public interface WhPullDao extends CrudRepository<WhPullEntity,Integer> {
    /**
     * 计算订单有多少未出库的出库单
     * @param b
     * @param id
     * @return
     */
    @Query("select count(w) from WhPullEntity w where w.logicDelete = ?1 and w.tbOrderByOrderId.orderId = ?2 and w.wpullStatus = 0")
    Integer sumNoPull(boolean b, Integer id);

    @Query("select count(w) from WhPullEntity w where w.logicDelete = ?1 and w.tbReturnGoodsByRgId.tbPurchaseByPurId.purId = ?2 and w.wpullStatus = 0")
    Integer sumNoPullReturn(boolean b, Integer id);

    @Query(nativeQuery = true,value = "select count(g) from TB_RETURN_GOODS g where g.MAGIC_DELETE = 0 and g.ORDER_ID = ?1")
    Integer haveReturn(OrderEntity tbOrderByOrderId);
}
