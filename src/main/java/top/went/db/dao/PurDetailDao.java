package top.went.db.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import top.went.pojo.MaintainOrderEntity;
import top.went.pojo.OrderDetailEntity;
import top.went.pojo.PurDetailEntity;

import java.util.List;

public interface PurDetailDao extends JpaRepository<PurDetailEntity,Integer> {
    /**
     * 查询所有采购明细
     * @return
     */
    public List<PurDetailEntity> findAllByMagicDeleteOrderByPdId(long delete, Pageable pageable);


    /**
     * 查询所有采购明细数量
     * @return
     */
    @Query(nativeQuery = true,value = "select count(*) from TB_PUR_DETAIL where magic_delete=0 order by PD_ID desc")
    public int pd_findAllCount();

    /**
     * 加载
     * @param delete
     * @param pdId
     * @return
     */
    public PurDetailEntity findAllByMagicDeleteAndPdId(long delete, int pdId);

    /**
     * 根据采购id查询采购明细
     * @param id
     * @return
     */
    @Query("from PurDetailEntity o where  o.tbPurchaseByPurId.purId = ?1 and o.magicDelete=0")
    List<PurDetailEntity> findAll(Integer id);

    /**
     * 根据采购单id删除所有采购明细
     * @param id
     * @return
     */
    @Query("delete from PurDetailEntity o where o.tbPurchaseByPurId.purId = ?1")
    @Modifying
    int deletePurDetail(Integer id);
}
