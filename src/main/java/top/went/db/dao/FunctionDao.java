package top.went.db.dao;

import org.springframework.data.repository.CrudRepository;
import top.went.pojo.FunctionsEntity;

public interface FunctionDao extends CrudRepository<FunctionsEntity, Long> {

}
