package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.ProductCategoryEntity;

import java.util.List;

/**
 * 产品类别dao
 */
public interface ProductCategoryDao extends CrudRepository<ProductCategoryEntity,Long> {
    /**
     * 查询分类
     * @param id
     * @param delete
     * @return
     */
    @Query("from ProductCategoryEntity c where c.logicDelete = ?2 and c.pcId = ?1")
    ProductCategoryEntity findOne(Long id, boolean delete);

    /**
     * 查询所有分类
     * @param delete
     * @return
     */
    @Query("from ProductCategoryEntity c where c.logicDelete = ?1 and c.tbProductCategoryByTbPcId is null")
    List<ProductCategoryEntity> findAll(boolean delete);

    /**
     * 加载分类
     * @param delete
     * @return
     */
    List<ProductCategoryEntity> findAllByLogicDelete(boolean delete);

}
