package top.went.db.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.CustomerEntity;
import top.went.pojo.UserEntity;

import java.util.List;

public interface CustomerDao extends JpaRepository<CustomerEntity,Integer> {

//    @Query( nativeQuery = true,value = "select * from TB_CUSTOMER c /*#pageable*/ where c.CUS_IS_DELETE = ?1 order by c.CUS_ID desc ")
    public List<CustomerEntity> findAllByCusIsDeleteOrderByCusIdDesc(long delete, Pageable pageable);

    @Query("from CustomerEntity c where c.cusId = ?1 and c.cusIsDelete = 1")
    public CustomerEntity findByCusId(Integer id);

    @Query("from CustomerEntity c where c.cusName = ?1 and c.cusIsDelete = 1")
    public CustomerEntity findCusId(String cusName);

    public long countAllByTbUserByUserId(UserEntity userEntity);

    @Query(nativeQuery = true, value = "select count(*) from tb_customer where to_number(to_char(CUS_DATE,'yyyy'))=?1 and to_number(to_char(CUS_DATE,'MM')) =?2 and CUS_IS_DELETE = 1")
    public long countAllByMonth(int year, int month);

}
