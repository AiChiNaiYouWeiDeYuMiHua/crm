package top.went.db.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import top.went.pojo.ReceiptRecordsEntity;

import java.util.List;

public interface ReceiptRecordsDao extends JpaRepository<ReceiptRecordsEntity,Integer> {
    /**
     * 查询所有开票
     * @param delete
     * @param pageable
     * @return
     */
    public List<ReceiptRecordsEntity> findAllByRrDeleteOrderByRrIdDesc(Long delete, Pageable pageable);

    /**
     * 根据开票日期模糊查询
     * @param rr_date
     * @return
     */
    @Query(nativeQuery = true,value = "SELECT * FROM TB_RECEIPT_RECORDS  WHERE  to_char(RR_DATE,'yyyy-MM-dd') like ?1 and RR_DELETE=0 order by ?#{#pageable}",
            countQuery = "SELECT count(*) FROM TB_RECEIPT_RECORDS WHERE to_char(RR_DATE,'yyyy-MM-dd') like ?1 and RR_DELETE=0")
    public Page<ReceiptRecordsEntity> findAllByDate(String rr_date, Pageable pageable);

    /**
     * 查询所有开票数量
     * @return
     */
    @Query(nativeQuery = true,value = "select count(*) from TB_RECEIPT_RECORDS where RR_DELETE=0 order by RR_ID desc")
    public int rr_findAllCount();

    /**
     * 根据日期查询所有开票记录
     * @param rrDate
     * @return
     */
    @Query(nativeQuery = true,value = "SELECT count(*) FROM TB_RECEIPT_RECORDS  WHERE  to_char(RR_DATE,'yyyy-MM-dd') like ?1 and RR_DELETE=0 order by RR_ID desc")
    public int rr_findByDateCount(String rrDate);
    /**
     * 加载
     * @param delete
     * @param rrId
     * @return
     */
    public ReceiptRecordsEntity findAllByRrDeleteAndRrId(long delete, Integer rrId);

}
