package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.QuoteEntity;
import top.went.pojo.UserEntity;

public interface QuoteDao extends CrudRepository<QuoteEntity, Long> {

    //加载
    public QuoteEntity findAllByQuoteIdAndIsDelete(Long quoteId, Long isDelete);

    public long countAllByTbUserByUserIdAndIsDelete(UserEntity userEntity,Long isDelete);

    @Query(nativeQuery = true, value = "select count(*) from TB_QUOTE where to_number(to_char(quote_time,'yyyy'))=?1 and to_number(to_char(quote_time,'MM')) =?2 and is_delete = 0")
    public long countAllByTime(int year,int month);
}
