package top.went.db.dao;

import org.springframework.data.repository.CrudRepository;
import top.went.pojo.ProductEntity;

/**
 * 产品dao
 */
public interface ProductDao extends CrudRepository<ProductEntity,Long> {
    public ProductEntity findByLogicDeleteAndProductId(boolean delete, Integer id);
}
