package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.FcTypeEntity;

import java.util.List;

public interface FcTypeDao extends CrudRepository<FcTypeEntity, Long> {

    /**
     * 查找所有功能分类
     * @return
     */
    @Query("from FcTypeEntity ft order by ft.fcTypeId asc ")
    public List<FcTypeEntity> queryAll();
}
