package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.ComplaintEntity;

public interface ComplaintDao extends CrudRepository<ComplaintEntity,Integer> {
    @Query("from ComplaintEntity c where c.complainId = ?1 and c.complainIsDelete = 1")
    public ComplaintEntity findByCusId(Integer id);
}
