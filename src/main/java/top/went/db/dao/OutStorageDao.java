package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.WhPullEntity;

/**
 * 出库
 */
public interface OutStorageDao extends CrudRepository<WhPullEntity,Integer> {
    @Query("from WhPullEntity p where p.logicDelete = ?1 and p.wpullId = ?2")
    WhPullEntity loadOne(boolean b, Integer wpullId);

    /**
     * 统计订单出库单个数
     * @param id
     * @return
     */
    @Query("select count(w) from WhPullEntity w where w.logicDelete = false and w.tbOrderByOrderId.orderId = ?1")
    int sumOrderPull(Integer id);
}
