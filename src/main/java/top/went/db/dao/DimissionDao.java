package top.went.db.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.DimissionEntity;
import top.went.pojo.DimissionTypeEntity;

import java.util.List;

/**
 * 系统参数dao
 */
public interface DimissionDao extends CrudRepository<DimissionEntity, Long> {

    /**
     * 查找指定id类型的所有系统参数
     *
     * @param dimiTypeId
     * @return
     */
    @Query(nativeQuery = true, value = "select * from TB_DIMISSION_TYPE left join TB_DIMISSION " +
            "on TB_DIMISSION.DIMI_TYPE_ID=TB_DIMISSION_TYPE.DIMI_TYPE_ID " +
            "where DIMISSION_IS_DEL=0 and TB_DIMISSION_TYPE.DIMI_TYPE_ID=?1")
    public List<DimissionEntity> queryByType(Long dimiTypeId);

    public List<DimissionEntity> findAllByTbDimissionTypeByDimiTypeIdAndDimissionIsDel(DimissionTypeEntity type,
                                                                                       Long del, Pageable pageable);

    public long countAllByTbDimissionTypeByDimiTypeIdAndDimissionIsDel(DimissionTypeEntity type, Long del);
}
