package top.went.db.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import top.went.pojo.MaintainOrderEntity;
import top.went.pojo.PurchaseEntity;
import top.went.pojo.UserEntity;

import java.util.Calendar;
import java.util.List;

public interface PurchaseDao extends JpaRepository<PurchaseEntity,Integer> {
    /**
     * 查询所有采购单
     * @return
     */
    public List<PurchaseEntity> findAllByMagicDeleteOrderByPurId(long delete, Pageable pageable);


    /**
     * 查询采购单数量
     * @return
     */
    @Query(nativeQuery = true,value = "select count(*) from TB_PURCHASE where magic_delete=0 order by PUR_ID desc")
    public int pur_findAllCount();

    /**
     * 加载
     * @param delete
     * @param purId
     * @return
     */
    public PurchaseEntity findAllByMagicDeleteAndPurId(long delete, int purId);
    /**
     * 查询入库数量
     * @return
     */
    @Query(nativeQuery = true,value = "select nvl(sum(IN_NUMBER),0) from TB_WH_PUT w left join TB_WP_DETAIL s on s.WP_ID=w.WP_ID  where LOGIC_DELETE=0 and PUR_ID=?1 and PF_ID=?2 and s.WD_TYPE=1 ")
    public int inCount(int purId,int pfId);

    //采购单，加一下逻辑删除
//dao
    public long countAllByTbUserByUserId(UserEntity userEntity);

    @Query(nativeQuery = true, value = "select count(*) from tb_purchase where to_number(to_char(pur_date,'yyyy'))=?1 and to_number(to_char(pur_date,'MM')) =?2 and MAGIC_DELETE=0")
    public long countAllByMonth(int year, int month);



}
