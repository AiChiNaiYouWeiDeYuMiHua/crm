package top.went.db.dao;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.CareEntity;

public interface CareDao extends CrudRepository<CareEntity,Integer> {
    @Query("from CareEntity c where c.careId = ?1 and c.careIsDelete = 1")
    public CareEntity findByCusId(Integer id);
}
