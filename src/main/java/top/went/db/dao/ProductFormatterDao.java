package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.ProductFormatEntity;

import java.util.List;


/**
 * 产品规格
 */
public interface ProductFormatterDao extends CrudRepository<ProductFormatEntity, Integer> {
    /**
     * 查找产品规格
     * @param delete
     * @param id
     * @return
     */
    public ProductFormatEntity findByLogicDeleteAndPfId(boolean delete, Integer id);

    /**
     * 计算产品下规格数量
     * @param id
     * @return
     */
    @Query(nativeQuery = true,value = "select count(*) from TB_PRODUCT_FORMAT where LOGIC_DELETE =0 and PRODUCT_ID = ?1")
    public Long countProductFormat(Integer id);

    @Query("select count(p) from ProductFormatEntity p where p.logicDelete = ?1 and p.pfName =?2")
    Long findFormatterName(boolean delete,String pfName);

    @Query("from ProductFormatEntity p where p.logicDelete = ?1 and p.tbProductByProductId.tbProductCategoryByPcId is null ")
    List<ProductFormatEntity> findCategoryNull(boolean delete);
}
