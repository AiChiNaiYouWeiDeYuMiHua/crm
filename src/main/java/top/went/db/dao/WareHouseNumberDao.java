package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.WhNumberEntity;

import java.util.List;

public interface WareHouseNumberDao extends CrudRepository<WhNumberEntity,Integer> {
    /**
     * 查询产品数量
     * @param id
     * @return
     */
    @Query(nativeQuery = true,value = "select sum(WN_NUMBER) from TB_WH_NUMBER where WN_ID=?1")
    public Long countAllProduct(Long id);

    /**
     * 查询某一仓库一产品记录
     * @param product
     * @param wareHouse
     * @return
     */
    @Query("from WhNumberEntity w where w.tbProductFormatByPfId.pfId = ?1 and w.tbWarehouseByWhId.whId = ?2")
    WhNumberEntity findOne(Integer product, Long wareHouse);

    /**
     * 查询某一产品的所有仓库记录
     * @param pfId
     * @return
     */
    @Query("from WhNumberEntity w where w.tbProductFormatByPfId.pfId =?1 and w.tbWarehouseByWhId.logicDelete =?2 order by w.wnNumber desc ")
    List<WhNumberEntity> loadAll(Integer pfId,boolean deete);
}
