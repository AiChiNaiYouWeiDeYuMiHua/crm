package top.went.db.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import top.went.pojo.ReturnGoodsDetailEntity;

import java.util.List;

public interface ReturnGoodsDetailDao extends JpaRepository<ReturnGoodsDetailEntity,Integer> {
        /**
         * 查询所有退货明细
         * @return
         */
        public List<ReturnGoodsDetailEntity> findAllByMagicDeleteOrderByRgdId(long delete, Pageable pageable);


        /**
         * 查询所有退货明细数量
         * @return
         */
        @Query(nativeQuery = true,value = "select count(*) from TB_RETURN_GOODS_DETAIL where magic_delete=0 order by RGD_ID desc")
        public int rgd_findAllCount();

        /**
         * 加载
         * @param delete
         * @param rgdId
         * @return
         */
        public ReturnGoodsDetailEntity findAllByMagicDeleteAndRgdId(long delete, int rgdId);

        /**
         * 根据退货id查询退货明细
         * @param id
         * @return
         */
        @Query("from ReturnGoodsDetailEntity o where o.tbReturnGoodsByRgId.rgId = ?1")
        List<ReturnGoodsDetailEntity> findAll(Integer id);
        //
        //    /**
        //     * 根据退货id删除所有退货明细
        //     * @param id
        //     * @return
        //     */
        //    @Query("delete from ReturnGoodsDetailEntity o where o.tbReturnGoodsByRgId.purId = ?1")
        //    @Modifying
        //    int deletePurDetail(Integer id);

}
