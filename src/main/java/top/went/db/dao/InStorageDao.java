package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.PurchaseEntity;
import top.went.pojo.WhPutEntity;

public interface InStorageDao extends CrudRepository<WhPutEntity,Integer> {
    @Query("from WhPutEntity p where p.logicDelete = ?1 and p.wpId = ?2")
    WhPutEntity loadOne(boolean b, Integer wpId);


    /**
     * 计算采购单有多少未入库的入库单
     * @param b
     * @param intValue
     * @return
     */
    @Query("select count(w) from WhPutEntity w where w.logicDelete = ?1 and w.tbPurchaseByPurId.purId = ?2 and w.wpStatus = 0")
    Integer sumNoPut(boolean b, int intValue);

    @Query("select count(w) from WhPutEntity w where w.logicDelete = ?1 and w.tbReturnGoodsByRgId.tbPurchaseByPurId = ?2 and w.wpStatus = 0")
    Integer sumNoPutReturn(boolean b, Integer id);

    @Query(nativeQuery = true,value = "select count(g) from TB_RETURN_GOODS g where g.MAGIC_DELETE = 0 and g.PUR_ID = ?1")
    Integer haveReturn(PurchaseEntity tbPurchaseByPurId);
}
