package top.went.db.dao;

import org.hibernate.annotations.SQLDelete;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import top.went.pojo.OrderDetailEntity;
import top.went.vo.OrderDetail;

import java.util.List;

/**
 * 订单详情
 */
public interface OrderDetailDao extends CrudRepository<OrderDetailEntity, Integer> {
    @Query("from OrderDetailEntity o where o.tbOrderByOrderId.orderId = ?1")
    List<OrderDetailEntity> findAll(Integer id);


    @Query("delete from OrderDetailEntity o where o.tbOrderByOrderId.orderId = ?1")
    @Modifying
    int deleteOrderDetail(Integer id);
}
