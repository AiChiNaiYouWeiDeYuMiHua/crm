package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.QuoteDetailEntity;
import top.went.pojo.QuoteEntity;

import java.util.List;

public interface QuoteDetailDao extends CrudRepository<QuoteDetailEntity, Long> {

    //加载
    public QuoteDetailEntity findAllByQdIdAndIsDelete(Long qdId, Long isDelete);

    //根据报价单id删除对应的所有明细
    @Query(nativeQuery = true,value = "delete from TB_QUOTE_DETAIL where QUOTE_ID = ?1 ")
    public void deleteAllByQuoteId(Long quoteId);

    @Query("from QuoteDetailEntity q where q.tbQuoteByQuoteId.quoteId =?1 and q.isDelete = 0")
    public List<QuoteDetailEntity> findAll(Long id);

}
