package top.went.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.WarehouseEntity;

import java.util.List;

/**
 * 仓库
 */
public interface WareHouseDao extends JpaRepository<WarehouseEntity,Long> {
    @Query("from WarehouseEntity w where w.logicDelete = ?1 and w.whId = ?2")
    public WarehouseEntity findOne(boolean delete,Long id);

    @Query("from WarehouseEntity w where w.logicDelete = ?1")
    List<WarehouseEntity> loadAll(boolean delete);
}
