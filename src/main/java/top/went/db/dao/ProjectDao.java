package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.ProjectEntity;

public interface ProjectDao extends CrudRepository<ProjectEntity, Integer> {
    @Query("from ProjectEntity c where c.projId = ?1 and c.projIsDelete = 1")
    public ProjectEntity findByCusId(Integer id);
}
