package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.MaintainOrderEntity;
import top.went.pojo.OrderEntity;
import top.went.pojo.ReturnGoodsEntity;

/**
 * 退货dao
 */
public interface ReturnGoodsDao extends CrudRepository<ReturnGoodsEntity,Integer> {
    /**
     * 退货加载
     * @param delete
     * @param id
     * @return
     */
    @Query("from ReturnGoodsEntity o where o.magicDelete = ?1 and o.rgId =?2")
    ReturnGoodsEntity findOne(Long delete, Integer id);
    /**
     * 加载
     * @param delete
     * @param mtId
     * @return
     */
    public ReturnGoodsEntity findAllByMagicDeleteAndRgId(long delete, int mtId);
}
