package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.UserRoleEntity;

import java.util.List;

public interface UserRoleDao extends CrudRepository<UserRoleEntity, Long> {

    /**
     * 清空指定角色的所有用户
     *
     * @param roleId
     * @return
     */
    @Query(nativeQuery = true, value = "delete tb_user_role where role_id = ?1")
    public boolean deleteAllByRoleId(Long roleId);

    /**
     * 清空指定用户的所有角色
     *
     * @param userId
     * @return
     */
    @Query(nativeQuery = true, value = "delete tb_user_role where user_id = ?1")
    public boolean deleteAllByUserId(Long userId);

    /**
     * 查找指定角色的所有分配用户记录
     *
     * @param roleId
     * @param del
     * @return
     */
    @Query(nativeQuery = true, value = "select * from TB_USER_ROLE where ROLE_ID=?1 and USER_ROLE_IS_DEL=?2")
    public List<UserRoleEntity> queryAllByRoleIdAndUserRoleIsDel(Long roleId, Long del);

    /**
     * 查找指定用户的所有分配角色记录
     *
     * @param userId
     * @param del
     * @return
     */
    @Query(nativeQuery = true, value = "select * from TB_USER_ROLE where USER_ID=?1 and USER_ROLE_IS_DEL=?2")
    public List<UserRoleEntity> queryAllByUserIdAndUserRoleIsDel(Long userId, Long del);
}
