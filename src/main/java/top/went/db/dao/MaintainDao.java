package top.went.db.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import top.went.pojo.MaintainOrderEntity;

import java.util.List;

public interface MaintainDao extends JpaRepository<MaintainOrderEntity,Integer>{
    /**
     * 查询所有维修工单
     * @return
     */
    public List<MaintainOrderEntity> findAllByMagicDeleteOrderByMtId(long delete, Pageable pageable);


    /**
     * 查询工单数量
     * @return
     */
    @Query(nativeQuery = true,value = "select count(*) from TB_MAINTAIN_ORDER where magic_delete=0 order by MT_ID desc")
    public int mt_findAllCount();

    /**
     * 加载
     * @param delete
     * @param mtId
     * @return
     */
    public MaintainOrderEntity findAllByMagicDeleteAndMtId(long delete, int mtId);

}
