package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.RoleFcEntity;

import java.util.List;

public interface RoleFcDao extends CrudRepository<RoleFcEntity, Long> {

    /**
     * 清空指定角色的所有功能权限
     *
     * @param roleId
     * @return
     */
    @Query(nativeQuery = true, value = "DELETE ROLE_FC WHERE ROLE_ID = ?1")
    public boolean deleteRoleFcEntitiesByRoleId(Long roleId);

    /**
     * 查找指定用户的功能分配记录
     * @param roleId
     * @param del
     * @return
     */
    @Query(nativeQuery = true, value = "select * from ROLE_FC where ROLE_ID=?1 and role_fc_is_del=?2")
    public List<RoleFcEntity> queryAllByRoleIdAndRoleFcIsDel(Long roleId, Long del);
//    @Query(nativeQuery = true, value = "")
//    public List<RoleFcEntity> queryAllByRoleIdAndRoleFcIsDel(Long roleId, Long del);
}
