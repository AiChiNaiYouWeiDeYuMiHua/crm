package top.went.db.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.DeliverGoodsEntity;

/**
 * 发货单
 */
public interface DeliverGoodsDao extends CrudRepository<DeliverGoodsEntity,Integer> {
    @Query("from DeliverGoodsEntity d where d.logicDelete = ?1 and d.dgId = ?2")
    DeliverGoodsEntity findOne(Boolean delete, Integer id);

    @Query("update DeliverDetailEntity d set d.status = 1 where d.tbDeliverGoodsByDgId.dgId = ?1")
    @Modifying
    void send(Integer id);

    @Query("update DeliverDetailEntity d set d.status = 0 where d.tbDeliverGoodsByDgId.dgId = ?1")
    @Modifying
    void cancelSend(Integer id);

    @Query("update DeliverDetailEntity d set d.status = -1 where d.tbDeliverGoodsByDgId.dgId = ?1")
    @Modifying
    void deleteSend(Integer id);

    @Query("from DeliverGoodsEntity d where d.logicDelete = ?1 and d.tbWhPullByWpullId.wpullId = ?2")
    DeliverGoodsEntity findOneByPull(boolean delete,Integer wpullId);

}
