package top.went.db.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.PlanPayBackEntity;
import top.went.pojo.PurchaseReceiptEntity;

import java.util.List;

/**
 * 付款发票Dao
 */
public interface PurchaseReceiptDao extends JpaRepository<PurchaseReceiptEntity,Integer> {
    /**
     * 查询所有付款发票
     * @param delete
     * @param pageable
     * @return
     */
    public List<PurchaseReceiptEntity> findAllByMagicDeleteOrderByPurrIdDesc(Long delete, Pageable pageable);

    /**
     * 根据收票日期模糊查询
     * @param purr_date
     * @return
     */
    @Query(nativeQuery = true,value = "SELECT * FROM TB_PURCHASE_RECEIPT  WHERE  to_char(PURR_DATE,'yyyy-MM-dd') like ?1 and MAGIC_DELETE=0 order by ?#{#pageable}",
            countQuery = "SELECT count(*) FROM TB_PURCHASE_RECEIPT WHERE to_char(PURR_DATE,'yyyy-MM-dd') like ?1 and magic_delete=0")
    public Page<PurchaseReceiptEntity> findAllByDate(String purr_date, Pageable pageable);

    /**
     * 查询所有发票数量
     * @return
     */
    @Query(nativeQuery = true,value = "select count(*) from TB_PURCHASE_RECEIPT where magic_delete=0 order by PURR_ID desc")
    public int purr_findAllCount();

    /**
     * 根据收票日期查询所有收票记录
     * @param purrDate
     * @return
     */
    @Query(nativeQuery = true,value = "SELECT count(*) FROM PurchaseReceiptEntity pr WHERE  to_char(PURR_DATE,'yyyy-MM-dd') like ?1 and magicDelete=0 order by prId desc")
    public int purr_findByDateCount(String purrDate);
    /**
     * 加载
     * @param delete
     * @param purrId
     * @return
     */
    public PurchaseReceiptEntity findAllByMagicDeleteAndPurrId(long delete, Integer purrId);

}
