package top.went.db.dao;

import org.springframework.data.repository.CrudRepository;
import top.went.pojo.DeptRoleEntity;

public interface DeptRoleDao extends CrudRepository<DeptRoleEntity, Long> {
}
