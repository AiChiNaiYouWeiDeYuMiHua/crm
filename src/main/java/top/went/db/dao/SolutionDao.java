package top.went.db.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.SolutionEntity;

import java.util.List;

/**
 * 解决方案
 */
public interface SolutionDao extends CrudRepository<SolutionEntity , Long> {
    //加载
    public SolutionEntity findBySolutionIdAndIsDelete(Long id, Long isdelete);
    //加载所有
    public List<SolutionEntity> findAllByIsDelete(Long isdelete);
    //分页加载所有
    public List<SolutionEntity> findAllByIsDeleteOrderBySolutionIdDesc(Long isdelete, Pageable pageable);
    //分页加载所有（方案主题）
    @Query(nativeQuery = true, value = "select s.* from TB_SOLUTION s where s.IS_DELETE = ?1 and s.SOLUTION_THEME like ?2  ORDER BY ?#{#pageable}"
    ,countQuery = "select count (s.*) from TB_SOLUTION s where s.IS_DELETE = ?1 and s.SOLUTION_THEME like ?2  ")
    public List<SolutionEntity> findAllByIsDeleteWhere(Long isDelete, String solutionTheme, Pageable pageable);

}
