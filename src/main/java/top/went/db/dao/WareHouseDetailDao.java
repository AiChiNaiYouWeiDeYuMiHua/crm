package top.went.db.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.WpDetailEntity;

import java.util.List;

public interface WareHouseDetailDao extends CrudRepository<WpDetailEntity, Long> {
    @Query("update WpDetailEntity d set d.wdState = ?3 where " +
            "d.wpId= ?1 and d.wdType = ?2")
    @Modifying
    int deleteByOut(Integer intValue, Integer type, Integer status);

    @Query("from WpDetailEntity d where d.wpId = ?1 and d.wdType = ?2")
    List<WpDetailEntity> loadAll(Integer wpullId, int i);

    @Query("delete from WpDetailEntity p where p.wpId = ?1 and p.wdType = ?2")
    @Modifying
    int deleteDetail(Integer id, int type);

    /**
     * 执行出库
     * @param id
     * @param i
     * @return
     */
    @Query(nativeQuery = true,value = "update TB_WP_DETAIL set WD_EXECUTE = sysdate,WD_STATE = 1 " +
            "where WP_ID = ?1 and WD_TYPE =?2")
    @Modifying
    int execute(Long id, int i);

    /**
     * 撤销出库
     * @param id
     * @param i
     * @return
     */
    @Query(nativeQuery = true,value = "update TB_WP_DETAIL set WD_EXECUTE = null ,WD_STATE = 0 " +
            "where WP_ID = ?1 and WD_TYPE =?2")
    @Modifying
    int cancel(Long id, int i);
    @Query("from WpDetailEntity w where w.wdType = ?1 and w.wpId = ?2 and w.tbProductFormatByPfId.pfId = ?3")
    WpDetailEntity loadOne(Integer type, int id, Integer pfId);
}
