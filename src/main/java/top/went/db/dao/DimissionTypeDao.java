package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.DimissionTypeEntity;

import java.util.List;

/**
 * 系统参数类型dao
 */
public interface DimissionTypeDao extends CrudRepository<DimissionTypeEntity, Long> {
    /**
     * 查找所有系统参数分类
     * @return
     */
    @Query(nativeQuery = true,value = "select * from USER_CRM.TB_DIMISSION_TYPE")
    public List<DimissionTypeEntity> queryAll();
}
