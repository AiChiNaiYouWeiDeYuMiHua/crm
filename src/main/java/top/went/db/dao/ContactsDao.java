package top.went.db.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.ContactsEntity;

import java.util.List;

public interface ContactsDao extends CrudRepository<ContactsEntity,Integer> {
    public List<ContactsEntity> findByCotsIsDeleteOrderByCotsId(Long delete, Pageable pageable);

    @Query("from ContactsEntity c where c.cotsId = ?1 and c.cotsIsDelete = 1")
    public ContactsEntity findByCusId(Integer id);

    @Query("from ContactsEntity c where c.cotsIsDelete = 1 and c.tbCustomerByCusId.cusId = ?1")
    public List<ContactsEntity> getAllByCusId(Integer id);
}
