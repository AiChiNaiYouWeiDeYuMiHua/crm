package top.went.db.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.LogEntity;
import top.went.pojo.UserEntity;

import java.sql.Date;
import java.util.List;

public interface LogDao extends CrudRepository<LogEntity, Long> {

    /**
     * @param pageable
     * @return
     */
    @Query(nativeQuery = true, value = "select * from TB_LOG order by ?#{#pageable}", countQuery = "SELECT count(*) from TB_LOG")
    public List<LogEntity> findAll(Pageable pageable);

    /**
     * 根据用户和时间查找操作日志
     *
     * @param userId
     * @param time
     * @param pageable
     * @return
     */
    @Query(nativeQuery = true, value = "select * from TB_LOG where USER_ID=?1 and LOG_OPTION_TIME=?2 order by ?#{#pageable}",
            countQuery = "select * from TB_LOG where USER_ID=?1 and LOG_OPTION_TIME=?2")
    public List<LogEntity> findAllByTbUserByUserIdAndLogOptionTime(Long userId, Date time, Pageable pageable);

    @Query(nativeQuery = true, value = "select count(*) from TB_LOG where USER_ID=?1 and LOG_OPTION_TIME=?2")
    public long countAllByTbUserByUserIdAndLogOptionTime(Long userId, Date time);

    /**
     * 根据时间查找日志
     *
     * @param time
     * @param pageable
     * @return
     */
    @Query(nativeQuery = true, value = "select * from TB_LOG where LOG_OPTION_TIME=?1 order by ?#{#pageable}",
            countQuery = "select * from TB_LOG where LOG_OPTION_TIME=?1")
    public List<LogEntity> findAllByLogOptionTime(Date time, Pageable pageable);

    /**
     * @param time
     * @return
     */
    @Query(nativeQuery = true, value = "select count(*) from TB_LOG where LOG_OPTION_TIME=?1")
    public long countAllByLogOptionTime(Date time);

    /**
     * 根据操作者查找日志
     *
     * @param userId
     * @param pageable
     * @return
     */
    @Query(nativeQuery = true, value = "select * from TB_LOG where USER_ID=?1 order by ?#{#pageable}",
            countQuery = "select * from TB_LOG where USER_ID=?1")
    public List<LogEntity> findAllByTbUserByUserId(Long userId, Pageable pageable);

    @Query(nativeQuery = true, value = "select count(*) from TB_LOG where USER_ID=?1")
    public long countAllByTbUserByUserId(Long userId);
}
