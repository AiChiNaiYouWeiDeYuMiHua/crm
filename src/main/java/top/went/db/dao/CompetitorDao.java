package top.went.db.dao;

import org.springframework.data.repository.CrudRepository;
import top.went.pojo.CompetitorEntity;

public interface CompetitorDao extends CrudRepository<CompetitorEntity, Long> {

    //加载
    public CompetitorEntity findAllByCompetitorIdAndIsDelete(Long competitorId, Long isDelete);

}
