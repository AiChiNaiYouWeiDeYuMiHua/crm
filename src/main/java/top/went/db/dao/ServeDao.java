package top.went.db.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import top.went.pojo.ContactsEntity;
import top.went.pojo.ServeEntity;

public interface ServeDao extends CrudRepository<ServeEntity, Integer> {
    @Query("from ServeEntity c where c.serveId = ?1 and c.serveIsDelete = 1")
    public ServeEntity findByCusId(Integer id);
}
