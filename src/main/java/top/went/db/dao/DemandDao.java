package top.went.db.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import top.went.pojo.DemandEntity;
import top.went.pojo.ReturnGoodsDetailEntity;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 详细需求
 */
public interface DemandDao extends JpaRepository<DemandEntity, Long> {
    //加载需求
    public DemandEntity findByDemandIdAndIsDelete(Long id, Long isDelete);
    //加载所有需求
    public List<DemandEntity> findAllByIsDelete(Long isDdelete);
    //分页加载所有需求
    public List<DemandEntity> findAllByIsDeleteOrderByDemandId(Long isDelete, Pageable pageable);

    //分页加载所有需求（重要程度、需求主题）
    @Query(nativeQuery = true, value = "select * from TB_DEMAND d where d.IS_DELETE = ?1 and d.IMPORTANCE like ?2 and d.DEMAND_THEME like ?3  ORDER BY ?#{#pageable}"
            ,countQuery = "select count(*) from TB_DEMAND d where d.IS_DELETE = ?1 and d.IMPORTANCE like ?2 and d.DEMAND_THEME like ?3 ")
    public Page<DemandEntity> findAllByIsDeleteWhere(Long isDelete, String importance, String demandTheme,  Pageable pageable);

}
