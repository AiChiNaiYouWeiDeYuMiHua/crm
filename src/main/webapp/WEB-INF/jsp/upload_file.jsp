<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/5
  Time: 13:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/css/fileinput.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<label class="btn btn-default ">
    <span class="fa fa-apple"></span>
    <span class="buttonText" data-toggle="modal" data-target="#import" onclick="InitExcelFile()">选择excel文件</span>
</label>

<div id="import" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">文件导入</h4>
            </div>
            <div class="modal-body">

                <form id="ffImport" method="post">
                    <div title="Excel导入操作" style="padding: 5px">
                        <input type="hidden" id="AttachGUID" name="AttachGUID" />
                        <input id="excelFile" type="file">
                    </div>
                </form>
                <!--数据显示表格-->
                <table id="gridImport" class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0" border="0">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="SaveImport()">保存</button>
            </div>
        </div>
    </div>
</div>





<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/zh.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/fa.min.js"></script>
<script>
    function InitExcelFile() {
        //记录GUID
        //          $("#AttachGUID").val(newGuid());
        $("#excelFile").fileinput({
            uploadUrl: "file_upload", //上传的地址
            uploadAsync: true, //异步上传
            language: "zh", //设置语言
            showCaption: true, //是否显示标题
            showUpload: true, //是否显示上传按钮
            showRemove: true, //是否显示移除按钮
            showPreview: true, //是否显示预览按钮
            browseClass: "btn btn-primary", //按钮样式
            dropZoneEnabled: false, //是否显示拖拽区域
            // allowedFileExtensions: ["xls", "xlsx"], //接收的文件后缀
            enctype: 'multipart/form-data',
            maxFileCount: 1, //最大上传文件数限制
            previewFileIcon: '<i class="fa fa-file"></i>',
            allowedPreviewTypes: null,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file"></i>',
                'xlsx': '<i class="fa fa-file"></i>',
                'pptx': '<i class="fa fa-file"></i>',
                'jpg': '<i class="glyphicon glyphicon-picture"></i>',
                'bmp': '<i class="glyphicon glyphicon-picture"></i>',
                'pdf': '<i class="glyphicon glyphicon-file"></i>',
                'zip': '<i class="glyphicon glyphicon-file"></i>'
            }
        });
        $('#excelFile').on("fileuploaded", function(event, data, previewId, index) {
            var result = data.response; //后台返回的json
            //console.log(result.status);
            //console.log(result.id);
            // $('#picid').val(result.id);//拿到后台传回来的id，给图片的id赋值序列化表单用
            //如果是上传多张图
            /*
            //计数标记，用于确保全部图片都上传成功了，再提交表单信息
            var fileCount = $('#file-pic').fileinput('getFilesCount');
            if(fileCount==1){
            $.ajax({//上传文件成功后再保存图片信息
                url:'BannerPicAction!savaForm.action',
                data:$('#form1').serialize(),//form表单的值
                success:function(data,status){
                    ...
                },
                cache:false,                    //不缓存
            });
            }
            */
            $.ajax({//上传文件成功后再保存图片信息
                url:'file_upload',
                type:'post',
                dataType:'json',
                data:$('#ffImport').serialize(),//form表单的值
                success:function(){
                    alert("success");
                },
                cache:false,                    //不缓存
            });
        });

        // $('#savePic').on('click',function (){// 提交图片信息 //
        //     //先上传文件，然后在回调函数提交表单
        //     $('#file-pic').fileinput('upload');
        //
        // });
    }
</script>
</body>
</html>
