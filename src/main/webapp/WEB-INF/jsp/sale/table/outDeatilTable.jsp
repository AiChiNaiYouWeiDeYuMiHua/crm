<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-14
  Time: 上午9:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>


<head>
    <style>
        html,
        body {
            color: #333333;
            background-color: #FFFFFF;
            font-size: 12px;
            font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        }
        .table-t thead{

        }
    </style>
</head>
<body>
<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-12" style="font-size: 12px;font-weight: normal;text-align: right;">
            <a class="f-s-12 text-black" id="edit" target="_blank" href="/sale/warehouse/detail/view?id=${id}&type=0"  style="margin-right: 10px;color: black;text-decoration: none;"><i class="fa fa-edit"></i>编辑出库单明细</a>
            <a style="FONT-WEIGHT: normal; COLOR: #c69; TEXT-DECORATION: none" id="enter" href="javascript:void(0);" onclick="enter();"style="text-decoration: none;"><i class="fa fa-arrow-circle-right"></i>确认出库</a>
        </label>
    </div>
</div>
<div class="col-md-12" style="padding-left: 80px;">
    <div class="form-group">
        <form id="save-form" action="/sale/warehouse/detail/enter" method="post">
            <input type="hidden" name="id" value="${id}">
            <input type="hidden" name="type" value="0">
            <table id="table1" class="table-t" style="font-size: 12px">
                <thead style="background-color: #EBEFF2;font-family: 楷体;">
                <tr>
                </tr>
                </thead>
            </table>
        </form>


    </div>
</div>
<script>
    $(function() {
        $("#table1").bootstrapTable({
            method: 'get',
            striped: true,

            cache: false,
            pagination: false,
            sortable: false,
            sortOrder: "desc",
            pageNumber: 1,
            pageSize: 10,
            url: "/sale/warehouse/out/detail?id=${id}",
            sidePagination: "server",
            queryParamsType: '',

            columns: [{
                field: 'productTitle',
                title: '产品名称[单位]',
                align: 'center',
                formatter:function (value, row, index) {
                    return [
                        value+'<input class="form-control" name="datas['+index+'].tbProductFormatByPfId.pfId" ' +
                        'type="hidden" value="'+row.productId+'">' +
                        '<input class="form-control"  name="datas['+index+'].outNumber" id="Number'+index+'" type="hidden" value="'+row.outNumber+'">'
                    ]
                }
            }, {
                field: 'planNumber',
                title: '订单数',
                align: 'center',
                formatter:function (value,row,index) {
                    if (value == null)
                        return 0;
                    else
                        return value;
                }
            }, {
                field: 'outNumber',
                title: '出库量',
                align: 'center',
                editable: {
                    type: 'text',
                    title: '出库量',
                    validate: function (v) {
                        if (isNaN(v)) return '出库量必须是数字';
                        if (v <= 0) return '出库量必须是正整数';
                        var data = $('#table1').bootstrapTable('getData'),
                         index = $(this).parents('tr').data('index');
                        if (v > data[index].whNumber)  return "当前仓库库存不足"
                        $('#Number'+index).val(v)
                        if (data[index].wdState && v!=data[index].outNumber)
                            return "该出库单已经被执行，不能在编辑"
                    }
                },
                formatter:function (value,row,index) {
                    if (value > row.whNumber) {
                        $('#enter').addClass("hidden")
                    }
                    return value
                }
            },{
                field: 'whNumber',
                title: '本库存',
                align: 'center',
                formatter:isnull
            }, {
                field: 'whAllNumber',
                title: '总库存',
                align: 'center',
                formatter:isnull
            },{
                field: 'wdOther',
                title: '备注',
                align: 'center',
                formatter:isnull
            }, {
                field: 'wdState',
                title: '状态',
                align: 'center',
                formatter:function (value,row,index) {
                    return value?"已执行":"未执行";
                }
//			footerFormatter: "合计"
            }],
            onEditableSave: function (field, row, oldValue, $el) {
                if (row.outNumber > row.whNumber) {
                    $('#enter').addClass("hidden")
                }else
                    $('#enter').removeClass("hidden")
            },
            onLoadSuccess: function(data){
                if (data.data.length > 0 && data.data[0].wdState){
                    $('#enter').addClass("hidden")
                    $('#edit').addClass("hidden")
                }
            },
            onLoadError: function(status) {
            }
        })


        //格式化

        //编辑时间

    })
    function addFotter(table, json) {
        table.bootstrapTable('append', json);
    }

    function formatterFotter(table, field, colspan, num) {
        table.bootstrapTable('mergeCells', {
            index: getLength(table) - num + 1,
            field: field,
            colspan: colspan
        });
    }

    function getLength(table) {
        return table.bootstrapTable('getData', true).length - 1;
    }
    function isnull(value,row,index) {
        if (value == null)
            return "";
        else
            return value;
    }
    function enter() {
        swal({
                title: "确定出库吗？",
                text: "该出库单即将被执行！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定出库！",
                closeOnConfirm: false
            },
            function(){
                $("#save-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            });

    }
</script>
</body>
</html>
