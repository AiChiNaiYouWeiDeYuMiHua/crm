<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-14
  Time: 上午9:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        html,
        body {
            color: #333333;
            background-color: #FFFFFF;
            font-size: 12px;
            font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        }
        .table-t thead{

        }
    </style>
</head>
<body>
<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-12" style="font-size: 12px;font-weight: normal;text-align: right;">
            <a style="FONT-WEIGHT: normal; COLOR: #c69; TEXT-DECORATION: none" href="javascript:void(0);" onclick="enter(${id});"style="text-decoration: none;"><i class="fa fa-arrow-circle-right"></i>确认发货</a>
        </label>
    </div>
</div>
<div class="col-md-12" style="padding-left: 80px;">
    <div class="form-group">
        <table id="table1" class="table-t" style="font-size: 12px">
            <thead style="background-color: #EBEFF2;font-family: 楷体;">
            <tr>
            </tr>
            </thead>
        </table>

    </div>
</div>
<script src="/js/sale/sendDetail.js"></script>
<script>
    $(function() {

    initSendTable(1,${id})
        //格式化

        //编辑时间
    })


    function enter(id) {
        swal({
                title: "确定发货吗？",
                text: "即将执行该发货单！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定发货！",
                closeOnConfirm: false
            },
            function () {
                ajax("/sale/warehouse/send/ok",function () {
                    swal("成功","发货成功","success");
                    window.history.go(0);
                },function () {},"post",{"id":id})
            });

    }
</script>
</body>
</html>
