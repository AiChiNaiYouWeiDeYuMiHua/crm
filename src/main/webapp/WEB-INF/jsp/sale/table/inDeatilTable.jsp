<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-14
  Time: 上午9:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>


<head>
    <style>
        html,
        body {
            color: #333333;
            background-color: #FFFFFF;
            font-size: 12px;
            font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        }
        .table-t thead{

        }
    </style>
</head>
<body>
<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-12" style="font-size: 12px;font-weight: normal;text-align: right;">
            <a class="f-s-12 text-black" id="edit" target="_blank" href="/sale/warehouse/detail/view?id=${id}&type=1"  style="margin-right: 10px;color: black;text-decoration: none;"><i class="fa fa-edit"></i>编辑入库单明细</a>
            <a style="FONT-WEIGHT: normal; COLOR: #c69; TEXT-DECORATION: none" id="enter" href="javascript:void(0);" onclick="enter();"style="text-decoration: none;"><i class="fa fa-arrow-circle-right"></i>确认入库</a>
        </label>
    </div>
</div>
<div class="col-md-12" style="padding-left: 80px;">
    <div class="form-group">
        <form id="save-form" action="/sale/warehouse/detail/enter" method="post">
            <input type="hidden" name="id" value="${id}">
            <input type="hidden" name="type" value="1">
            <table id="table1" class="table-t" style="font-size: 12px">
                <thead style="background-color: #EBEFF2;font-family: 楷体;">
                <tr>
                </tr>
                </thead>
            </table>
        </form>


    </div>
</div>
<script>
    $(function() {
        $("#table1").bootstrapTable({
            method: 'get',
            striped: true,

            cache: false,
            pagination: false,
            sortable: false,
            sortOrder: "desc",
            pageNumber: 1,
            pageSize: 10,
            url: "/sale/warehouse/in/detail?id=${id}",
            sidePagination: "server",
            queryParamsType: '',

            columns: [{
                field: 'productTitle',
                title: '产品名称[单位]',
                align: 'center',
                formatter:function (value, row, index) {
                    return [
                        value+'<input class="form-control" name="datas['+index+'].tbProductFormatByPfId.pfId" ' +
                        'type="hidden" value="'+row.productId+'">' +
                        '<input class="form-control"  name="datas['+index+'].inNumber" id="Number'+index+'" type="hidden" value="'+row.inNumber+'">'
                    ]
                }
            }, {
                field: 'inNumber',
                title: '入库量',
                align: 'center',
                editable: {
                    type: 'text',
                    title: '出库量',
                    validate: function (v) {
                        if (isNaN(v)) return '出库量必须是数字';
                        if (v < 0) return '出库量必须是非负整数';
                        if (v > 10000000) return '请输入正确的数据'
                        var data = $('#table1').bootstrapTable('getData'),
                            index = $(this).parents('tr').data('index');
                        $('#Number'+index).val(v)
                    }
                },
                formatter:isnull
            },{
                field: 'wdOther',
                title: '备注',
                align: 'center',
                formatter:isnull
            }, {
                field: 'wdState',
                title: '状态',
                align: 'center',
                formatter:function (value,row,index) {
                    return value?"已执行":"未执行";
                }
//			footerFormatter: "合计"
            }],
            onEditableSave: function (field, row, oldValue, $el) {

            },
            onLoadSuccess: function(data){
                if (data.data.length > 0 && data.data[0].wdState){
                    $('#enter').addClass("hidden")
                    $('#edit').addClass("hidden")
                }
            },
            onLoadError: function(status) {
            }
        })


        //格式化

        //编辑时间

    })
    function addFotter(table, json) {
        table.bootstrapTable('append', json);
    }

    function formatterFotter(table, field, colspan, num) {
        table.bootstrapTable('mergeCells', {
            index: getLength(table) - num + 1,
            field: field,
            colspan: colspan
        });
    }

    function getLength(table) {
        return table.bootstrapTable('getData', true).length - 1;
    }
    function isnull(value,row,index) {
        if (value == null)
            return "";
        else
            return value;
    }
    function enter() {
        swal({
                title: "确定入库吗？",
                text: "该入库单即将被执行！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定入库！",
                closeOnConfirm: false
            },
            function(){
                $("#save-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            });

    }
</script>
</body>
</html>
