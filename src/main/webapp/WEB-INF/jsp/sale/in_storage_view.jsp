<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<%--
  产品table
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>入库单管理</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css" />
    <style type="text/css">
        #order_top {
            height: 60px;
        }

        #order_top ul {
            border-bottom: solid gainsboro 1px;
        }

        .ul_i {
            border-bottom: solid black 2px;
        }


        thead tr th {
            border-bottom: 2px solid #a7b7c3;
            background-color: #ebeff2;
        }

        body {
            font-family: "microsoft yahei";
            padding: 20px;
            background-color: #f4f8fb;
        }

        strong {
            color: black;
        }

        #content {
            top: -150px;
        }

        .fixed-table-container {
            /*top: -10px;*/
        }

        .columns {
            top: -45px;
        }

        .fixed-table-pagination {
            position: absolute;
            width: 100%;
            text-align: center;
            /*margin-top: -10px;*/
        }

        .pagination {
            margin-right: 49%;
        }

        .pagination {}

        thead {
            background-color: #269ABC;
            color: #000000;
            font-size: 12px;
        }

        tbody {
            font-size: 13px;
            color: #000000;
        }

        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 180px;
        }
        #navbar1 li a{
            color: #505461;
            font-weight: 600;
        }
        #navbar1 .active a{
            color: #000;
        }
        textarea{
            resize:none;
        }
        a{
            color: #505458;
        }
    </style>
    <!-- [if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![end if]-->
</head>

<body>
<div class="container-fluid">
    <div class="row clearfix" id="order_top">
        <div class="col-md-12 column">
            <ul class="nav navbar-nav" style="width: 100%;" id="navbar1">
                <li class="active ul_i" data-value="0"  @click="addActive(this)">
                    <a href="javascript:void(0);" >全部</a>
                </li>
            </ul>
        </div>
    </div>
    <div style="margin-top: 20px;">
        <div class="form-group col-sm-2" style="padding: 0;">
            <div>
                <i class="fa fa-filter pull-left" style="margin-right: 10px;padding: 0;margin-top: 10px;"></i>
                <div class="col-sm-9" style="padding: 0;">
                    <select class="selectpicker" id="select-type" onchange="search()" name="type" data-live-search="true" style="width: 200px;">
                        <option selected value="0">全部数据</option>
                        <option value="1">未入库</option>
                        <option value="2">已入库</option>
                    </select>
                </div>
            </div>
        </div>
        <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：form搜索
-->
        <div class="form-group col-sm-4" style="padding: 0;">
            <i class="fa fa-search col-sm-1" style="margin-top: 10px;"></i>
            <div class="input-group col-sm-8">
                <input type="text" id="search-text" class="form-control">
                <div class="input-group-btn">
                    <button type="button" class="btn waves-effect" onclick="search()" style="color: black;">主题</button>
                    <button type="button" class="btn waves-effect" id="dropdownMenu3" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                        <li>
                            <a class="btn-default" id="search-btn"  href="javascript:void(0)">高级查询</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-2 pull-left text-center" style="padding: 0;margin-bottom: 15px;">

        </div>

        <div style="width: 100%;text-align: right;">
            <a class="btn btn-default" style="margin-right: 75px;opacity:.55" <g:g id="291"> id="add-btn" href="javascript:void(0);" </g:g><i class="fa fa-plus-circle"></i> 新建</a>
        </div>
        <!--
            作者：1810761389@qq.com
            时间：2018-07-23
            描述：搜索状态条数
        -->
    </div>
    <div class="col-sm-2" style="margin-top: 5px;">
        <span class="pull-left">入库单</span>
        <button type="button" onclick="cancelKey()" id="cancel-search" class="hidden btn btn-danger btn-xs pull-left" data-toggle="tooltip" data-placement="bottom" title="解除搜索,显示全部数据">
            <i class="fa fa-reply"></i>解除搜索
        </button>
    </div>
    <div class="col-sm-5">

    </div>

    <!--<div class="col-sm-9">

    </div>-->

    <!--
        作者：1810761389@qq.com
        时间：2018-07-31
        描述：table
    -->
    <div id="content">
        <table id="table">

        </table>

    </div>
    <div class="col-sm-3 pull-right" style="text-align: right; margin-top: 10px;">
        <button onclick="print()" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="打印">
            <i class="fa fa-print"></i>
        </button>
    </div>
</div>


<br>
<!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：页尾
-->
<div class="col-lg-12">
    <footer class="footer" style="text-align: center;padding-bottom: 20px;padding-top: 20px;">热线:<b>4008-8208-820 </b> &nbsp;&nbsp;网站:<b><a href="www.baidu.com" target="_blank">www.baidu.com</a></b> &nbsp;
        <a class="btn btn-danger btn-xs" href="#" onclick="">
            <i class="fa fa-whatsapp m-r-5"></i> 投诉&amp;问题
        </a>&nbsp;&nbsp;
        <a class="btn btn-default btn-xs" href="#" onclick="">
            <i class="fa fa-weixin m-r-5"></i>微客服
        </a>&nbsp;&nbsp;
        <a class="btn btn-primary btn-xs" href="#" onclick="">
            <i class="md md-speaker-notes m-r-5"></i>订阅号
        </a>
        <br>Copyright © 2004-2018 &nbsp;XXX技术有限公司&nbsp;&nbsp; </footer>
</div>


<!--
    作者：1810761389@qq.com
    时间：2018-07-31
    描述：高级查询
-->
<div class="modal fade" id="search-senitor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">
        </div>
    </div>
</div>


<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<script src="/js/tableExport.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="/js/sale/in-storage-table.js"></script>
<%--<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>--%>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script src="/js/resjs/printThis.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-validator/0.5.3/js/language/zh_CN.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<script>

    $(function() {
        $("[data-toggle='tooltip' ] ").tooltip();
        loadNav()
        add_modal();
        //消除模态框数据
        $("#add").on("hidden.bs.modal", function () {
            open_modal = null;
            $(this).removeData("bs.modal");
        });
        //回车时间
        $('#search-text').bind('keypress',function(event){


            if(event.keyCode == 13)

            {
                search()
            }

        });
    });

    function loadNav() {
        ajax("/sale/warehouse/wh",function (data) {
            for (var i =0;i<data.length;i++){
                var children =  '<li @click="addActive(this)" data-value="'+data[i].whId+'">'+
                    '<a href="javascript:void(0);" >'+data[i].whName+'</a>'+
                    '</li>';
                $('#navbar1').append(children);
            }
            switchNav();
        },function () {
            swal('','加载错误','error');
        },'get',{});
    }
    function print() {
        /* Act on the event */
        $("#table111").printThis({
            debug: false,
            importCSS: false,
            importStyle: false,
            printContainer: true, //打印容器
            loadCSS: "css/bootstrap-table.css",		//需要加载的css样式
            pageTitle: "sn",
            removeInline: false,
            printDelay: 333, //打印时延
            header: null,
            formValues: false
        });
    }

    //导航栏类别
    function switchNav() {
        $("#navbar1").find('li').each(function () {
            $(this).click(function () {
                $('li').removeClass();
                $(this).addClass('active ul_i');
                cusLifeCycle = $(this).val();
                search()
                // $("#table111").bootstrapTable('refresh');
            })
        })
    }
    //查询
    function search() {
        if ( $('#select-type').val()!=0 ||$('#search-text').val().length >0 ||!$.isEmptyObject(f($('#search-form')))) {
            $('#cancel-search').removeClass('hidden')
        }else {
            $('#cancel-search').addClass('hidden')
        }
        $('#table').bootstrapTable('refresh')
    }
    //取消搜索
    function cancelKey() {
        $('#select-type').selectpicker('val','0');
        $('#search-text').val("");
        if ($('#search-form')[0] != null)
            $('#search-form')[0].reset();
        search();

    }
    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                    error()
                }
            }
        })
    }
    //删除
    function deletePro(pID) {
        swal({
                title: "确定删除吗？",
                text: "你将无法恢复该入库单！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function(){
                ajax("/sale/warehouse/in/delete",function () {
                    swal("成功！", "入库单删除成功！","success")
                    $("#table").bootstrapTable('refresh')
                },function(){},'post',data={'id':pID});
            });
    }

    //批量删除
    function deleteAll() {
        var datas = $('#table').bootstrapTable("getSelections");
        if (datas.length >0) {
            var str=[];
            for (var i = 0; i < datas.length; i++) {
                var id = datas[i].wpId;
               str.push(id)
            }
            swal({
                    title: "确定删除吗？",
                    text: "将要删除选中的" + datas.length + "张入库单",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定删除！",
                    closeOnConfirm: false
                },
                function () {
                    ajax("/sale/warehouse/in/delete/all", function () {
                        swal("成功！", "合同/订单删除成功！", "success");
                        $("#table").bootstrapTable('refresh')
                    }, function () {
                    }, 'post', {ids:str});
                });
        }
    }
    //加载模态框
    function add_modal() {
        $("#add-btn").css("opacity",'1')
        $("#add-btn").click(function () {
            $('#add').modal({
                remote:'/sale/warehouse/in/add'
            })
        });

        $("#search-btn").click(function () {
            $('#search-senitor').modal({
                remote:'/sale/product/search'
            })
        });
    }
    function refresh() {
        $('#table').bootstrapTable()
    }

    var open_modal;
    //打开编辑
    var isedit = false;

    function edit(id) {
        open_modal = id;
        $('#add').modal({
            remote:'/sale/warehouse/in/add'
        });
    }
    function successForm() {
        $('#add').modal('hide')
        //表单重置
        $("#add-form")[0].reset();
        $("#table").bootstrapTable('refresh')
    }
    function add() {
        $("[data-toggle='tooltip' ] ").tooltip();
        <g:g id="292">
        $(".pagination-detail").before("<div><button onclick='deleteAll()' class='btn btn-default pull-left' style='margin-top: 10px;margin-right: 5px;color:black'><i class='fa fa-trash'></i></button></div>");
        </g:g>
    }
   function redio(dom,is) {
       dom.eq(0).removeProp('checked');
       dom.eq(1).removeProp('checked');
       if (is == 0 || !is ){
           dom.eq(0).prop('checked', 'checked');
       }else
           dom.eq(1).prop('checked', 'checked');
   }
    //撤销入库
    function cancelIn(id) {
        swal({
                title: "确定撤销吗？",
                text: "将要撤销该入库单，库存将会恢复",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定撤销！",
                closeOnConfirm: false
            },
            function () {
                ajax("/sale/warehouse/cancel/in/", function () {
                    swal("成功！", "入库单撤销成功！", "success");
                    $("#table").bootstrapTable('refresh')
                }, function () {
                }, 'post', {"id":id});
            });
    }
    //入库
    function inWarehouse(id) {
        window.open("/sale/warehouse/in/info/"+id+"/");
    }
    function operation(value,row) {
        var json = [];
        var cancel = '<g:g id="292"><a onclick="cancelIn('+row.wpId+')" style="cursor:pointer;margin-left:8px;" data-toggle="tooltip" data-placement="bottom"  title="撤销"><i class="fa fa-arrow-left"></i></a></g:g>'
        if (!value){
            json.push('<g:g id="293"><a onclick="inWarehouse('+row.wpId+')" style="cursor:pointer;margin-left:8px;" data-toggle="tooltip" data-placement="bottom"  title="入库"><i class="fa fa-sign-out"></i></a></g:g>')
            json.push('<g:g id="292"><a onclick="deletePro('+row.wpId+')" style="cursor:pointer;margin-left:8px;" data-toggle="tooltip" data-placement="bottom"  title="删除"><i class="fa fa-trash-o"></i></a></g:g>')
            json.push('<g:g id="293"><a onclick="edit('+row.wpId+')" style="cursor:pointer; margin-left:8px;" data-toggle="tooltip" data-placement="bottom" title="编辑"><i class="fa fa-pencil"></i></a></g:g>')
        }else if (value){
            json.push(cancel)
        }
        return json.join('');
    }
</script>
</body>

</html>