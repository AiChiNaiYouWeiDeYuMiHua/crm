<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/7
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Title</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css" />
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">
</head>
<style type="text/css">
    html,
    body {
        color: #333333;
        background-color: #FFFFFF;
        font-size: 12px;
        font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .table-t thead{
        background-color: #EBEFF2;font-family: 楷体;
    }
</style>
<body>
<div class="container-fluid" style="background-color: #F4F8FB;">
    <div style="margin: 10px;background-color: white;padding: 10px;">
        <div class="row">
            <div class="col-md-12">
                <h4 class="modal-title text-dark">
                    <span set-lan="html:采购单">采购单</span>
                </h4>
                <hr style="border-top: 1px solid #aaaaaa;">
            </div>
        </div>
        <div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">采购主题：</label>
                        <div class="col-md-10">1234</div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">供应商（客户）：</label>
                        <div class=" col-md-10"><span style="font-weight:normal;color:#9e9e9e">〖</span>风云
                            <a href="javascript:vopen('/xcrm/customer/customer/detail.xt?id=4','customer4',999,600);"><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">供应商联系人：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">杨威</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">采购单号：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">123456789</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">采购日期：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">今天</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">仓库：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">北京仓库</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">预计到货日期：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">2018-07-27</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">经手人：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">陈默</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">状态:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">2生成入库单</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">金额:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">RMB830.00</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">备注:</label>
                        <div class=" col-md-10">66666</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">采购物品明细:</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-12" style="font-size: 12px;font-weight: normal;text-align: right;">
                            <a class="f-s-12 text-black" href="javascript:vopen('/sys/editprice/puritemedit.xt?pu_id=49&amp;mdb=afb2ad','detailedit',960,530); " style="margin-right: 10px;color: black;text-decoration: none;"><i class="fa fa-edit"></i>编辑采购单明细</a>
                            <a class="f-s-12 text-black" href="javascript:vopen('/sys/editprice/puritemprint.xt?pu_id=51&amp;mdb=100dd1','print',960,530);"style="margin-right: 10px;color: black;text-decoration: none;"><i class="fa fa-print"></i>打印采购单</a>

                            <a class="f-s-12 text-black" href="javascript:void(0)" onclick="javascript:openbasepop('导出word打印', 'print_word', 'mdb=752fbc&amp;id=0&amp;type=purchase&amp;t_id=51&amp;t_mdb=100dd1&amp;vparse=amF2YXNjcmlwdDp2b3BlbignL3N5cy9lZGl0cHJpY2UvcHVyaXRlbXByaW50Lnh0P3B1X2lkPTUxJm1kYj0xMDBkZDEmc2lkPTEzMCZzc249OW4zdXN1ZnYwNTg4Mjh1bTRkaGc2am1tZzcmY2NuPWQwMDImY3I9ZTkxMDhiN2E5ODkzZTM3ZGMwZjU4YTFlZTlmOTEwOTcnLCdwcmludCcsMjAwLDIwMCk7', this, 300, 200);"style="margin-right: 10px;color: black;text-decoration: none;"><i class="fa fa-file-word-o m-r-5"></i>导出word打印</a>
                            <a style="FONT-WEIGHT: normal; COLOR: #c69; TEXT-DECORATION: none" href="" onclick="reportMove(this);"style="text-decoration: none;"><i class="fa fa-arrow-circle-right"></i>办理退货</a>
                        </label>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 80px;">
                    <div class="form-group">
                        <table id="table1" class="table-t">
                            <thead>
                            <tr>

                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">入库情况:</label>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 80px;">
                    <div class="form-group">
                        <table id="table2" class="table-t">
                            <thead>
                            <tr>

                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
    付款计划(应付)
    <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
        <div class="col-md-12">
            <div class="form-group">
                ￥退货单明细/退货单
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <span class="col-md-12" style="text-align: center;font-size: 12px;margin-bottom: 5px;">退 货 单</span>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;"><span>应退款:</span><span style="color: darkgray;">￥119,020.00</span> &nbsp;&nbsp;&nbsp;&nbsp;
                    <span>已退款:</span><span style="color: darkgray;">￥119,020.00(余额:￥0.00)</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;"><span>退货:</span><span style="color: darkgray;">全部出库</span> &nbsp;&nbsp;&nbsp;&nbsp;
                    <span>单据状态：</span><span style="color: darkgray;">结束</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;"><span>主题：</span><span style="color: darkgray;">上海仓库采购-退货</span> &nbsp;&nbsp;&nbsp;&nbsp;
                    <span>退货单号：</span><span style="color: darkgray;">123456789</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;"><span>经办人：</span><span style="color: darkgray;">陈默</span> &nbsp;&nbsp;&nbsp;&nbsp;
                    <span>仓库：</span><span style="color: darkgray;">上海仓库</span> &nbsp;&nbsp;&nbsp;&nbsp;
                    <span>日期：</span><span style="color: darkgray;">2017-03-17</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;"><span>操作：</span>
                    <button type="button" class="btn">撤销退货</button> &nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="f-s-12 text-black" href='javascript:purdetailitems_click_openreturn(1,"e2173e");' onclick=" reportMove(this) ;" style="color: #000000;text-decoration: none;margin-right: 10px;"><i class="fa fa-edit"></i>编辑/删除退货单</a>
                    <a class="f-s-12 text-black" href='javascript:purdetailitems_click_openlibout(45,"86e4ae");' onclick=" reportMove(this) ;" style="color: #000000;text-decoration: none;margin-right: 10px;"><i class="fa fa-list m-r-5"></i>对应出库单</a>
                    <a class="f-s-12 text-black" href='javascript:purdetailitems_click_openpay_note(24,"10eac8");' onclick=" reportMove(this) ;" style="color: #000000;text-decoration: none;margin-right: 10px;"><i class="fa fa-yen m-r-5"></i>对应退款</a>
                    <a class="f-s-12 text-black" href="javascript:vopen('/sys/editprice/purrtnitemprint.xt?rtn_id=1&amp;mdb=e2173e','print',960,530);" style="color: #000000;text-decoration: none;margin-right: 10px;"><i class="fa fa-print"></i>打印退货单</a>
                    <a class="f-s-12 text-black" href="javascript:vopen('/sys/editprice/purrtnitemprint.xt?rtn_id=1&amp;mdb=e2173e&amp;sid=3&amp;ssn=9n3usufv058828um4dhg6jmmg7&amp;ccn=d002&amp;cr=00c3bd411e15f87190810714fca6283d','print',200,200);" style="color: #000000;text-decoration: none;margin-right: 10px;"><i class="fa fa-file-word-o m-r-5"></i>导出word打印</a>
                    <a class="f-s-12 text-black" href="javascript:vopen('/sys/editprice/purrtnitemprint.xt?rtn_id=1&amp;mdb=e2173e&amp;sid=3&amp;ssn=9n3usufv058828um4dhg6jmmg7&amp;ccn=d002&amp;cr=00c3bd411e15f87190810714fca6283d&amp;te=1','print',200,200);" style="color: #000000;text-decoration: none;margin-right: 10px;"><i class="fa fa-file-excel-o m-r-5"></i>导出excel打印</a>
                </div><br /><br /><br />
            </div><br /><br /><br />
            <div class="form-group">
                <div class="col-md-12">
                    <table id="table3" class="table-t">

                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
        <div class="col-md-12">
            <div>
                ￥付款计划(应付)
                <button type="button" class="btn btn-link" style="font-size: 20px;text-decoration: none;margin-left:5px;color: black;" data-toggle="modal" data-target="#myModal">+</button>
            </div>
            <div id="model1">

            </div>

            <div>
                <div>
                    ￥</span>100.00</span>
                    </a>&nbsp;第7期&nbsp;2018-07-12&nbsp;已付&nbsp;<br>
                </div>
                <div>
                    ￥</span>100.00</span>
                    </a>&nbsp;第7期&nbsp;2018-07-12&nbsp;已付&nbsp;<br>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
        <div class="col-md-12">
            <div>
                ￥付款票据
                <!--<a href="" style="font-size: 20px;text-decoration: none;margin-left:5px;color: black;">+</a>-->
                <button type="button" class="btn btn-link" style="font-size: 20px;text-decoration: none;margin-left:5px;color: black;" data-toggle="modal" data-target="#myModal1">+</button>
            </div>
            <div id="model2"></div>
            <div>
                数码<span>￥</span>100.00</span>
                </a>&nbsp;第7期&nbsp;2018-07-12<br>
            </div>
        </div>
    </div>
    <!--</div>-->
    <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
        <div class="col-md-12">
            <div>
                ￥付款记录
                <button type="button" class="btn btn-link" style="font-size: 20px;text-decoration: none;margin-left:5px;color: black;" data-toggle="modal" data-target="#myModal2">+</button>
            </div>
            <div id="model3"></div>
        </div>
        <div>
            &nbsp;&nbsp;&nbsp;<span>￥</span>100.00</span>
            </a>&nbsp;第7期&nbsp;2018-07-12<br>
        </div>
    </div>
</div>
<div class="row" style="text-align: center;background-color: #F4F8FB;">
    <div class="col-md-12">
        <div class="form-group">
            <footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
                <a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
                <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
                <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
                <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>
        </div>
    </div>
</div>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="../js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="../js/table1.js"></script>
<script src="../js/table2.js"></script>
<script src="../js/table3.js"></script>
<script src="../js/table4.js"></script>
<script type="text/javascript">
    $("#model1").load("pur_model1.html");
    $("#model2").load("pur_model2.html");
    $("#model3").load("pur_model3.html");
    $(".form_datetime").datetimepicker({
        format: "yyyy年mm月dd日 ",
        minView: "month", //设置只显示到月份
        startView: 2,
        showMeridian: 1,
        forceParse: 0,
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });
</script>
</body>
</html>
