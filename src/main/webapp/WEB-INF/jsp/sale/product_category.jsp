<%--
    产品分类
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>产品分类维护</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/toastr.css">
    <!-- [if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![end if]-->
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <h3>
                <i class="fa fa-sitemap"></i>
                产品维护
            </h3>
            <small>
                <b>说明：</b>
                点击产品分类名字，可添加下级分类、改名 、删除、或剪切和粘贴分类
            </small>
            <div id="tree" style="margin-top: 10px"></div>
        </div>
    </div>

</div>
<div id="myModal" class="modal fade" >
    <div class="modal-dialog">
        <div class="modal-content" style="border: none">
        </div>
    </div>

</div>
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-treeview.js"></script>
<script src="/js/toastr.js"></script>
<script>
    var modal;
    $(function () {
        toastr.options = {
            "closeButton": true
        }


        $("#myModal").on("hidden.bs.modal", function () {
            $(this).removeData("bs.modal");
        });
        getTree();
    })
    //获取树数据
    function getTree() {
        var data = [];
        $.getJSON("/sale/product/category/tree",function (result) {
            if (result.code != 200)
                toastr.error(result.msg);
            else
                setTree(result.data)
        }).error(function () {
            toastr.error("加载数据失败")
        })
    }
    function setTree(data) {
        $('#tree').treeview({
            data: data,
            levels: 3,
            onNodeSelected: function (event, data) {
                modal = data.id;
                $('#myModal').modal({
                    remote: '/sale/product/category/modal'
                });
                $('#myModal').modal('show');
            }
        });
    }
    //刷新数据
    function refresh() {
        window.location.reload()
    }
</script>
</body>
</html>