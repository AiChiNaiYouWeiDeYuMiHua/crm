<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<%--
付款计划
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base target="_blank">
    <meta charset="UTF-8">
    <title>采购单</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <%--<link rel="stylesheet" href="/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.css" />--%>
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>

    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css" />
    <style type="text/css">
        #order_top {
            height: 60px;
        }

        #order_top ul {
            border-bottom: solid gainsboro 1px;
        }

        .ul_i {
            border-bottom: solid black 1px;
        }

        thead tr th {
            border-bottom: 2px solid #a7b7c3;
            background-color: #ebeff2;
        }

        body {
            font-family: "microsoft yahei";
            padding: 20px;
            background-color: #f4f8fb;
        }

        strong {
            color: black;
        }

        #content {
            top: -150px;
        }

        .fixed-table-container {
            /*top: -10px;*/
        }

        .columns {
            top: -45px;
        }



        .pagination {
            margin-right: 20%;
        }

        .pagination {}

        thead {
            background-color: #269ABC;
            color: #000000;
            font-size: 12px;
        }

        tbody {
            font-size: 13px;
            color: #000000;
        }

        textarea{
            resize:none;
        }
        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 100%;
        }
    </style>
</head>

<body>
<div class="container-fluid">
    <div class="form-group col-sm-2" style="padding: 0;">
        <div>
            <i class="fa fa-filter pull-left" style="margin-right: 10px;padding: 0;margin-top: 10px;"></i>
            <div class="col-sm-9" style="padding: 0;">
                <select class="selectpicker" id="purState"  name="purState" data-live-search="true" style="width: 200px;" onchange="selectOnchang(this)">
                    <option selected value="全部数据">全部数据</option>
                    <option value="新采购">1新采购</option>
                    <option value="生成入库单">2部分入库</option>
                    <option value="入库完成">3入库完成</option>
                </select>
            </div>
        </div>
    </div>

    <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：内容块
-->
    <div class="form-group col-sm-4" style="padding: 0;">
        <i class="fa fa-search col-sm-1" style="margin-top: 10px;"></i>
        <div class="input-group col-sm-8">
            <input type="text" class="form-control" id="search-text">
            <div class="input-group-btn">
                <button type="button" class="btn waves-effect" style="color: black;" id="queryByDateButton" onclick="search()">采购主题</button>
                <button type="button" class="btn waves-effect" id="dropdownMenu3" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                    <li class="divider"></li>
                    <li>
                        <a class="btn-default" id="search-btn" >高级查询</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-5"></div>
    <div class="col-sm-2 pull-left text-center" style="padding: 0;margin-bottom: 15px;">

    </div>
    <div style="width: 100%;text-align: right;">
        <a class="btn btn-default"  style="margin-right: 75px;opacity:.55" <g:g id="181">id="add-plan-btn" href="javascript:void(0);"</g:g> ><i class="fa fa-plus-circle"></i> 新建</a>
    </div>
    <!--
        作者：1810761389@qq.com
        时间：2018-07-23
        描述：搜索状态条数
    -->
</div>
</div>
<div class="col-sm-2" style="margin-top: 5px;">
    <span class="pull-left">采购单</span>&nbsp;&nbsp;&nbsp;
    <button type="button" onclick="cancelKey()" id="cancel-search" class="hidden btn btn-danger btn-xs pull-left" data-toggle="tooltip" data-placement="bottom" title="解除搜索,显示全部数据" style="margin-left: 15px">
        <i class="fa fa-reply"></i>解除搜索
    </button>
</div>
<br><br>

<!--<div class="col-sm-9">

</div>-->

<!--
    作者：1810761389@qq.com
    时间：2018-07-31
    描述：table
-->
<div id="content" >
    <table id="table888">

    </table>

</div>
</div>
<!--
作者：1810761389@qq.com
时间：2018-07-23
描述：统计
-->

<input type="hidden" value="${mark}" id="mark">
<br>
<!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：页尾
-->
<div class="col-lg-12">
    <footer class="footer" style="text-align: center;padding-bottom: 20px;padding-top: 20px;">热线:<b>4008-8208-820 </b> &nbsp;&nbsp;网站:<b><a href="www.baidu.com" target="_blank">www.baidu.com</a></b> &nbsp;
        <a class="btn btn-danger btn-xs" href="#" onclick="">
            <i class="fa fa-whatsapp m-r-5"></i> 投诉&amp;问题
        </a>&nbsp;&nbsp;
        <a class="btn btn-default btn-xs" href="#" onclick="">
            <i class="fa fa-weixin m-r-5"></i>微客服
        </a>&nbsp;&nbsp;
        <a class="btn btn-primary btn-xs" href="#" onclick="">
            <i class="md md-speaker-notes m-r-5"></i>订阅号
        </a>
        <br>Copyright © 2004-2018 &nbsp;XXX技术有限公司&nbsp;&nbsp; </footer>
</div>


<!--
    作者：1810761389@qq.com
    时间：2018-07-31
    描述：高级查询
-->
<div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade" id="wsj-search" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">
        </div>
    </div>
</div>
<div class="modal fade" id="wsj-add" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">
        </div>
    </div>
</div>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="/js/jquery-3.3.1.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>

<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="/js/wsjJS/purchase-table.js"></script>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<script src="/js/resjs/printThis.js"></script>



<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>

<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="/js/select.js"></script>
<script type="text/javascript " src="/js/wsjJS/bootstrap-datetimepicker.min.js "></script>
<%--<script type="text/javascript " src="/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>--%>
<%--<script type="text/javascript " src="/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>--%>
<script src="/js/sweetalert.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<script>
    $(function() {
        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
        //回车事件
        $('#search-text').bind('keypress',function(event){
            if(event.keyCode == 13)
            {
                search()
            }
        });
        //新建加载模态框
        $("#add-plan-btn").css("opacity",'1')
        $("#add-plan-btn").click(function () {
            $('#wsj-add').modal({
                remote:'/sale/purAddModal'
            })
        })
        //高级查询加载模态框
        $("#search-btn").click(function () {
            $('#wsj-search').modal({
                remote:'/sale/purQueryModal'
            })
        });
        //消除模态框数据
        $("#wsj-add").on("hidden.bs.modal", function () {
            $(this).removeData("bs.modal");
        });
        //消除模态框数据
        $("#wsj-search").on("hidden.bs.modal", function () {
            $(this).removeData("bs.modal");
        });
        $("#example").popover();
    });
    function print() {
        /* Act on the event */
        $("#table888").printThis({
            debug: false,
            importCSS: false,
            importStyle: false,
            printContainer: true, //打印容器
            loadCSS: "css/bootstrap-table.css",		//需要加载的css样式
            pageTitle: "sn",
            removeInline: false,
            printDelay: 333, //打印时延
            header: null,
            formValues: false
        });
    }
    //删除
    function deletePro1(purId) {
        swal({
                title: "确定删除吗？",
                text: "你将无法恢复该记录！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function(){
                ajax("/sale/purdelete",function () {
                    swal("成功！", "删除成功！","success")
                    $('#table888').bootstrapTable("refresh");
                },function(){},'post',{'purId':purId});
            });
    }

    function deleteAll() {
        var datas = $('#table888').bootstrapTable("getSelections");
        if (datas.length >0) {
            var str=[];
            for (var i = 0; i < datas.length; i++) {
                var id = datas[i].mtId;
                str.push(id)
            }
            swal({
                    title: "确定删除吗？",
                    text: "将要删除选中的" + datas.length + "记录",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定删除！",
                    closeOnConfirm: false
                },
                function () {
                    ajax("/saleAfter/mtdeleteall", function () {
                        swal("成功！", "记录删除成功！", "success")
                        $('#table888').bootstrapTable("refresh")
                    }, function () {
                    }, 'post', {ids:str});
                });
        }
    }
    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                    error()
                }
            }
        })
    }
    var open_modal;
    //打开编辑
    function edit(id) {
        open_modal = id;
        $('#wsj-add').modal({
            remote:'/sale/purAddModal'
        })
    }

    function add() {
        <g:g id="182">$(".pagination-detail").before("<div><button onclick='deleteAll()' class='btn btn-default pull-left' style='margin-top: 10px;margin-right: 5px;color:black'><i class='fa fa-trash'></i></button></div>");</g:g>
        $(".pagination-detail").before('<div class="col-sm-3 pull-right" style="text-align: right; margin-top: 10px;">'+
            ' <button onclick="print()" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="打印">'+
            '<i class="fa fa-print"></i>'+
            '</button>'+
            '</div>')
    }
    //普通查询
    function search() {
        var a=$('#purState').selectpicker('val');
        if ($('#search-text').val().length >0 ) {
            $('#cancel-search').removeClass('hidden')
        }else {
            $('#cancel-search').addClass('hidden')
        }
        var dateText = $("#search-text").val();
        if(dateText==""){
        }
        else {
            if(a=="全部数据"){
                $.ajax({
                    url: '/sale/purall',
                    type: 'post',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data:JSON.stringify({'purTheme':$("#search-text").val()}),
                    // $("#search-text").val()
                    success: function (json) {
                        $("#table888").bootstrapTable('load', json);//主要是要这种写法
                    }
                })
            }if(a=="新采购"||a=="生成入库单"||a=="部分入库"||a=="入库完成"){
                $.ajax({
                    url: '/sale/purall',
                    type: 'post',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data:JSON.stringify({'purState':a,'purTheme':$("#search-text").val()}),
                    success: function (json) {
                        $("#table888").bootstrapTable('load', json);//主要是要这种写法
                    }
                })
            }
        }
    }
    //高级查询
    function search1() {
        $('#cancel-search').removeClass('hidden')
        $('#table888').bootstrapTable('refresh');
        if ($('#search-form')[0] != null)
            $('#search-form')[0].reset();
    }
    //取消搜索
    function cancelKey() {
        $('#search-text').val("");
        if ($('#search-form')[0] != null)
            $('#search-form')[0].reset();
        search1();
        $('#cancel-search').addClass('hidden');
        $('#purState').selectpicker('val',"全部数据");
    }
    //Select下拉框选择触发事件
    function selectOnchang(obj){
        var a=$('#purState').selectpicker('val');
        if(a=="全部数据"){
            search1();
            $('#cancel-search').addClass('hidden');
        }  if(a=="新采购"||a=="生成入库单"||a=="部分入库"||a=="入库完成"){
            $('#cancel-search').removeClass('hidden');
            $.ajax({
                url: '/sale/purall',
                type: 'post',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data:JSON.stringify({'purState':a}),
                success: function (json) {
                    $("#table888").bootstrapTable('load', json);//主要是要这种写法
                }
            })
        }
    }
    function aa(name,id) {
        if($("#mark").val()=="1"){
            window.opener.setDANZI(id,name)
            window.close()
        }
        else{
            var a="/sale/purdetail1/"+id+"/";
            window.open(a)
        }
    }
    function operation(value,row) {
        if(row.purState=="新采购"){
            return [
                '<g:g id="184"><a href="/sale/purdetail1/'+row.purId+'/" style="cursor:pointer;" data-toggle="tooltip" data-placement="bottom"  title="明细"><i class="fa fa-list m-r-5""></i></a></g:g>',
                '<g:g id="183"><a onclick="edit('+row.purId+')" style="cursor:pointer; margin-left:8px;" data-toggle="tooltip" data-placement="bottom" title="编辑"><i class="fa fa-pencil"></i></a></g:g>',
                '<g:g id="182"><a onclick="deletePro1('+row.purId+')" style="cursor:pointer;margin-left:8px;" data-toggle="tooltip" data-placement="bottom"  title="删除"><i class="fa fa-trash-o"></i></a></g:g>'
            ].join('');
        }
        else
            return "";
    }
</script>
</body>
</html>
