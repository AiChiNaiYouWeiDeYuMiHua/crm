<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="modal-header" style="border-bottom: none;">
    <h4 class="modal-title" id="myModalLabel">
        高级查询--采购单
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="search-form" >
        <div class="mybody">
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            采购主题:
                        </label>
                        <div class="col-md-7">
                            <input name="purTheme" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-3">采购单号：</label>
                        <div class="col-md-7">
                                <input name="purOddNumbers" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            供应商(客户)：
                        </label>
                        <div class="col-md-7">
                            <input name="cusName" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            仓库:
                        </label>
                        <div class="col-md-7">
                            <input name="cusName" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            负责人：
                        </label>
                        <div class="col-md-7">
                            <input name="userName" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-3 ">
                            采购日期:
                        </label>
                        <div class="col-md-4" style="padding-right: 0">
                            <div id="datetime_1" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="开始日期" name="from1"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
                                            </span>
                            </div>

                        </div>

                        <div class="col-md-4" style="padding-left: 0">
                            <div id="datetime_2" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="结束日期" name="to1"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-3 ">
                          预计到货日期:
                        </label>
                        <div class="col-md-4" style="padding-right: 0">
                            <div id="datetime_3" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="开始日期" name="from2"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
                                            </span>
                            </div>

                        </div>

                        <div class="col-md-4" style="padding-left: 0">
                            <div id="datetime_4" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="结束日期" name="to2"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-3">状态:</label>
                        <div class="col-md-8">
                            <div id="asearchdt_pay_plan_serial"
                                 style="position: static; visibility: visible; top: 0px;">
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="新采购" name="stateList"><label>1新采购</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="生成入库单" name="stateList"><label>2生成入库单</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="部分入库" name="stateList"><label>3部分入库</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="入库完成" name="stateList"><label>4入库完成</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" id="reset-btns" style="margin-left: 5px">
                    <i class="fa fa-recycle"></i>
                    重置
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default"onclick="search_btn()" ><i
                        class="fa fa-check" ></i>
                    查询
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(function () {
        $('.selectpicker').selectpicker("refresh");
        $("#datetime_1").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_2").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });
        $("#datetime_3").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_4").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });
        $("#datetime_5").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_6").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });
        $('#reset-btns').click(function () {
            $('#search-form')[0].reset();
            $('.selectpicker1').selectpicker('val', '未选');
        })

        $(".selectpicker1").selectpicker({
            noneSelectedText: '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();

    });
    function search_btn(){
        search1();
        $('#wsj-search').modal("hide")
        $('.selectpicker1').selectpicker('val', '未选');
    }
</script>


</body>
</html>