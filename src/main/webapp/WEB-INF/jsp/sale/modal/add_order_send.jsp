<%--
  添加订单
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
</head>
<body >
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-default pull-right" id="add-btns">
                        <i class="fa fa-check"></i>
                        保存
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        交付记录/发货明细
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="add-form" action="/sale/order/send/addOne" method="post" class="form-horizontal">
                        <div class="mybody">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">
                                            <span style="color: #ff0000; font-size: 16px;"></span>客户：
                                        </label>
                                        <div class="col-md-10">
                                            <input  disabled class="form-control" value="${order.tbCustomerByCusId.cusName}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">
                                            <span style="color: #ff0000; font-size: 16px;"></span>合同/订单号：
                                        </label>
                                        <div class="col-md-10">
                                            <input  disabled class="form-control" value="${order.orderTitle}" />
                                            <input name="tbOrderByOrderId.orderId" type="hidden"   value="${order.orderId}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" >
                                            <span style="color: #ff0000; font-size: 16px;">*</span>产品名称：
                                        </label>
                                        <div class="col-md-10 input-group" style="padding-left: 15px;padding-right: 15px">
                                            <input class="form-control hidden" name="formatEntity.pfId" required/>
                                            <div class="form-control" id="product" disabled></div>
                                            <span class="input-group-btn">
                                                <button onclick="chooseProduct()" class="btn btn-default">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>单价：
                                        </label>
                                        <div class="col-md-8">
                                            <input id="price" value="￥0.00" disabled class="form-control"   />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>金额：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="ddTotal" class="form-control"   />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>订单数量：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="ddNumber"   class="form-control">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>交付时间：
                                        </label>
                                        <div class="col-md-8">
                                            <input  type="date" id="orderDate" name="ddDate" class="form-control"    />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">备注：</label>
                                        <div class="col-md-10">
                                            <textarea name="ddOther" class="form-control" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default" type="submit" id="add-submit"><i class="fa fa-check"></i>
                    保存</button>
            </div>

                <script type="text/javascript" src="/js/sale/distpicker.data.js"></script>
                <script type="text/javascript" src="/js/sale/distpicker.js"></script>
                <script type="text/javascript" src="/js/sale/main.js"></script>>
<script>
    $(function() {
        defaultTime();
        $("#add-form").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
        });

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();
        sumitForm();
    });
    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error(result.msg)
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                }
            }
        })
    }
    function sumitForm() {
        //提交表单数据
        $('#add-submit,#add-btns').click(function () {
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                $("#add-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            }
        })
    }
    //设置默认时间
    function defaultTime() {
        var time = new Date();
        var day = ("0" + time.getDate()).slice(-2);
        var month = ("0" + (time.getMonth() + 1)).slice(-2);
        var today = time.getFullYear() + "-" + (month) + "-" + (day);

        $('input[name="orderDate"],input[name="orderLatestDate"],input[name="orderNewDate"]').val(today)
    }
    function chooseProduct() {
        window.open("/sale/chooseProduct");
    }

    function selectProduct(title,id,price){
        $('input[name="formatEntity.pfId"]').val(id)
        $('#product').html(title)
        $('#price').val(price)
    }
</script>

</body>

</html>