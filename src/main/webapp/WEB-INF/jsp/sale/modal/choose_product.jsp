<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-14
  Time: 下午8:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>选择产品</title>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
</head>
<body>
<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">选择产品</h4>
</div>
<div class="modal-body" >
    <div class="container-fluid">
        <div class="row" >
            <div class="col-md-12" id="first-way" >
                <h4>方法1: <i class="fa fa-search"></i>查找选择产品：</h4>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="search-text" placeholder="品名，型号，编号，条码">
                            <span class="input-group-btn">
                                            <button class="btn btn-default " id="search">搜索</button>
                                        </span>

                        </div>
                    </div>
                <div id="result" style="margin-top: 10px;font-size: 12px"></div>
            </div>
            <div class="col-md-12">
                <h4>方法2: <i class="fa fa-check"></i>点击选择产品</h4>
                <div id="tree" style="height:400px;overflow-y: scroll"></div>
            </div>
        </div>
    </div>
</div>
<script>
    if (typeof jQuery == 'undefined'){
        var script=document.createElement("script");
        script.type="text/javascript";
        script.src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js";
        document.body.appendChild(script)
        var script1=document.createElement("script");
        script1.type="text/javascript";
        script1.src="/js/bootstrap-treeview.js";
        var script2=document.createElement("script");
        script2.type="text/javascript";
        script2.src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js";
        var script3=document.createElement("script");
        script3.type="text/javascript";
        script3.src="/js/sweetalert.min.js";
        var link = document.createElement("link");
        link.rel = "stylesheet";
        link.href = "https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css";
        var link1 = document.createElement("link");
        link1.rel = "stylesheet";
        link1.href = "https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css";
        var link2 = document.createElement("link");
        link2.rel = "stylesheet";
        link2.href = " /css/sweetalert.css";
        script.onload = function () {
            document.body.appendChild(script1);
            document.body.appendChild(script2);
            document.body.appendChild(script3);
            document.head.appendChild(link)
            document.head.appendChild(link1)
            document.head.appendChild(link2)
            $(function () {
                init();
            })
        }
    }else {
        $(function () {
            init()
        })
    }

</script>
<script>
    function init() {
        $('#search').click(function () {
            searchProduct();
        })
        getData();

    }
    function searchProduct() {
        var text = $('#search-text').val();
        if (text != null && text.length >0){
            ajax("/sale/product/all",function (data) {
                data = data.rows;
                if (data.length <=0)
                    $('#result').text("没有找到相应的产品");
                else {
                    var result = "";
                    for (var i =0; i<data.length;i++){
                        var click = 'returnProduct("'+formattertitle(data[i])+'",'+data[i].id+','+data[i].pfPrice+','+data[i].productEntity.productStock+')';
                        result += "<img src='/img/pdata1.gif'/><span>" +
                            "<a href='javascript:;' onclick="+click+" style='color: #000;margin-left: 5px'>"+formattertitle(data[i])+"</a>";
                        if (data[i].productEntity != null &&!data[i].productEntity.productStock)
                            result +="<i class='fa fa-shopping-cart' style='margin-left: 5px'></i>";
                        result += "<a href='/sale/product/info/"+data[i].id+"/' target='_blank' style='margin-left: 5px;color: #505458;'><i class='fa fa-comment-o'>详情</i></a><span><br>"
                    }
                    $('#result').html("搜索结果:<br>"+result)
                }
            },function () {},"post",{'searchText':text})
        }else {
            $('#result').text("查询内容不能为空");
        }
    }
    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                success(result);
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                }
            }
        })
    }
    function formattertitle(data) {
       var str = "";
        var productName = "";
        var productModalName = "";
       if (data.productEntity != null) {
           productName= data.productEntity.productName;
           productModalName = data.productEntity.productModel;
       }
       let formaterName = data.pfName;
       let util = data.pfUnit;
        if (productName != null && productName.length >0)
            str += productName +"/";
        if (productModalName != null && productModalName.length >0)
            str += productModalName +"/";
        if (formaterName != null && formaterName.length >0)
            str += formaterName;
        return str;
    }
    function setTree(data) {
        $('#tree').treeview({
            data:data
        })
    }
    function getData() {
        ajax("/sale/product/tree",function (data) {
            setTree(data.data)
        },function () {},"get",{})
    }
    function openProduct(id) {
        window.open("/sale/product/info/"+id+"/")
    }
    function returnProduct(title,id,price,stock) {
        if (window.hasOwnProperty("selectProduct"))
            selectProduct(title,id,price,stock)
        else {
            if (!stock) {
                swal("添加错误", "需要计算库存的产品，请选择订单", "error")
                return;
            } else {
                window.opener.selectProduct(title, id, price);
                window.close();
            }
        }
    }
</script>
</body>
</html>
