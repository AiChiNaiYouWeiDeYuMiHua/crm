<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal-header" style="border-bottom: none;height: 60px">
    <h4 class="modal-title" id="myModalLabel1">
        订单退货单
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="add-form" action="/sale/rg_insert" method="post" class="form-horizontal">
        <hr class="boder-t-a"/>
        <div class="mybody">
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-2">
                            <span style="color: red">*</span> 主题:
                        </label>
                        <div class="col-md-10" style="margin-left: 0px">
                            <input id="rgTheme" name="rgTheme" class="form-control" required
                                   style="background-color: white;width: 95%;" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-2">
                             退货单号:
                        </label>
                        <div class="col-md-10" style="margin-left: 0px">
                            <input id="rgOddNumbers" name="rgOddNumbers" class="form-control" required
                                   style="background-color: white;width: 95%;" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-2">
                            供应商：
                        </label>
                        <div class="col-md-10" style="margin-left: 0px">
                            <input disabled id="tbCustomerByCusId.cusId" class="form-control"
                                   value="〖${orderEntity.tbCustomerByCusId.cusName}〗" style="width: 95%;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-2">
                            对应订单：
                        </label>
                        <div class="col-md-10" style="margin-left: 0px">
                            <input disabled  class="form-control"
                                   value="〖${orderEntity.orderTitle}〗" style="width: 95%;"/>
                            <input type="hidden"  class="form-control"
                                   value="${orderEntity.orderId}" id="orderId" name="tbOrderByOrderId.orderId"/>
                            <input type="hidden" name="type" value="1">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input name="serveId" style="display: none">
                        <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                            <span style="color: #ff0000; font-size: 16px;">*</span>负责人：
                        </label>
                        <div class="input-group col-md-8" style="padding-left: 10px;">
                            <input type="text" id="user" class="form-control"
                                   style="display: none">
                            <input type="hidden" name="tbUserByUserId.userId" value="1">
                            <input type="text" disabled class="form-control" id="to_user" name="to_user">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default"
                                        style="color: black;height: 34px"><i
                                        class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <div id="user_warn"class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;"> 负责人不能为空
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-md-6">
                    <div class="form-group ">
                        <label class="control-label col-md-4">
                            <span style="color: red;">*</span>退货时间:
                        </label>
                        <div class="col-md-8">
                            <div id="datetime_4" class="input-append date form_datetime input-group">
                                <input name="rgReturnTime" id="rgReturnTime" class="form-control" required
                                       style="background-color: white"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-6">
                    <div class="form-group ">
                        <label class="control-label col-md-4"> <span style="color: red;">*</span>应退款：</label>
                        <div class="col-md-8" style="margin-left: 0px">
                            <input id="planMoney"
                                   name="planMoney" class="form-control"
                                   required
                                   style="background-color: white;width: 95%;" required/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group ">
                        <label class="control-label col-md-4"> <span style="color: red;">*</span>已退款：</label>
                        <div class="col-md-8" style="margin-left: 0px">
                            <input id="toMoney"
                                   name="toMoney" class="form-control"
                                   required
                                   style="background-color: white;width: 95%;" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-2">
                            <span style="color: red">*</span> 入库仓库:
                        </label>
                        <div class="col-md-8">
                            <select id="modal-wh" data-width="100%" required name="warehouseEntity.whId" class="selectpicker">
                                <option selected></option>
                            </select>
                        </div>
                        <div id="wh_warn"class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;"> 仓库不能为空
                        <%--<div class="col-md-10" style="margin-left: 0px">--%>
                            <%--<input id="tbWhPullsByRgId.tbWarehouseByWhId.whId" name="tbWhPullsByRgId.tbWarehouseByWhId.whId" class="form-control"--%>
                                   <%--style="background-color: white;width: 95%;" />--%>
                        <%--</div>--%>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-2">
                            <span style="color: red">*</span> 状态:
                        </label>
                        <div class="col-md-10" style="margin-left: 0px">
                            <input id="rgState" name="rgState" class="form-control" required
                                   style="background-color: white;width: 95%;" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label class="control-label col-md-4">
                            <span style="color: red">*</span> 出库状态:
                        </label>
                        <div class="col-md-8" style="margin-left: 0px">
                            <input id="putState" name="putState" class="form-control" required
                                   style="background-color: white;width: 95%;" required/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group ">
                        <label class="control-label col-md-4"><span style="color: red;">*</span>退款状态:
                        </label>
                        <div class="col-md-8" style="margin-left: 0px">
                            <input id="payBackState" name="payBackState" class="form-control"
                                   style="background-color: white;width: 95%;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-2">退货明细:</label>
                        <div class="col-md-10" style="margin-left: 0px">
                            <div id="content" >
                                <table id="table1">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-2">退货已出库:
                        </label>
                        <div class="col-md-10" style="margin-left: 0px">
                            <div id="content1" >
                                <table id="table2">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default" id="add-submit"><i
                        class="fa fa-check"></i>
                    保存
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    sumitForm();
    $(function () {
        getWareHouse();
        //消除模态框数据
        $("#wsj-add").on("hidden.bs.modal", function () {
            open_modal = null
            $(this).removeData("bs.modal");
        });
        setForm();
        $("#add-form").bootstrapValidator({
            // excluded : [':disabled'],
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "rgTheme": {
                    msg: '主题验证失败',
                    validators: {
                        notEmpty: {
                            message: '主题不能为空'
                        }
                    }
                },

                "rgReturnTime": {
                    msg: '日期无效',
                    validators: {
                        notEmpty: {
                            message: '日期不能为空'
                        }
                    }
                }, "planMoney": {
                    msg: '金额验证失败',
                    validators: {
                        notEmpty: {
                            message: '金额不能为空'
                        },
                        callback: {
                            message: '不能为负数',
                            callback: function (value) {
                                if (value < 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        }
                    }
                }, "toMoney": {
                    msg: '金额验证失败',
                    validators: {
                        notEmpty: {
                            message: '金额不能为空'
                        },
                        callback: {
                            message: '不能为负数',
                            callback: function (value) {
                                if (value < 0) {
                                    return false;
                                } else {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        });

        $(".selectpicker").selectpicker({
            noneSelectedText: '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();

        $('.selectpicker').selectpicker("refresh");

        $("#datetime_3").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_3"
        }).on('hide', function (e) {
            $('#add-form').data('bootstrapValidator')
                .updateStatus('purDate', 'NOT_VALIDATED', null)
                .validateField('purDate');
        });
        $("#datetime_4").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_4"
        }).on('hide', function (e) {
            $('#add-form').data('bootstrapValidator')
                .updateStatus('purProjectedDate', 'NOT_VALIDATED', null)
                .validateField('purProjectedDate');
        });
    })
    function getWareHouse() {
        return ajax("/sale/warehouse/wh",success = function (data) {
            var group = $('#modal-wh');
            for (var i =0; i<data.length;i++){
                group.append('<option value="'+data[i].whId+'">'+data[i].whName+'</option>')
            }
            setForm();
            $('#modal-wh').selectpicker('refresh');
            $('#modal-wh').selectpicker('render');
        });
    }
    function setForm() {
        if (open_modal != null) {
            ajax("/sale/rgload?rgId=" + open_modal, function (data) {
                // $('#add-form').append("<input class='hidden' name='purId' value='" + data.rgId + "'>")
                $('input[name="rgTheme"]').val(data.rgTheme);
                $('input[name="rgOddNumbers"]').val(data.rgOddNumbers);
                // $('input[name="tbPurchaseByPurId.tbCustomerByCusId.cusId"]').val(data.tbPurchaseByPurId.tbCustomerByCusId.cusName);
                // $('input[name="tbPurchaseByPurId.tbUserByUserId.userId"]').val(data.tbPurchaseByPurId.tbUserByUserId.userName);
                $('input[name="rgReturnTime"]').val(data.rgReturnTime);
                $('input[name="planMoney"]').val(data.planMoney);
                $('input[name="toMoney"]').val(data.toMoney);
             //   $('input[name="tbWhPutsByRgId.tbWarehouseByWhId.whId"]').val(data.tbWhPutsByRgId.tbWarehouseByWhId.whName);
                $('input[name="rgState"]').val(data.rgState);
                $('input[name="putState"]').val(data.putState);
                $('input[name="payBackState"]').val(data.payBackState);
            }, function (msg) {
                swal("加载失败", msg, "error");
                $('#wsj-add').modal('hide')
            }, "get", {})
        }
    }

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            type: type,
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error(result.msg)
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                }
            }
        })
    }

    function sumitForm() {
        //提交表单数据
        $('#add-submit').click(function () {
            var text = $('#user option:selected').text();
            if (text == "" || text == null) {
                $("#user1").show();
                // $("#cus1").show();
            }
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                // if(b=true){}
                $("#add-form").ajaxSubmit({
                    dataType: 'json',
                    success: function (result) {
                        var rgId = result.data;
                        $('#wsj-add').modal('hide');
                        //表单重置
                        $("#add-form")[0].reset();
                        if (result.code == 200) {
                            var a = "/sale/orderToRg?rgId=" + rgId + "&orderId=" + $("#orderId").val();
                            ajax(a, function () {
                                var b = "/sale/returnGoodsProDetail/" + rgId + "/"
                                window.open(b);
                            }, function (msg) {
                            }, "get", {})
                        }
                        else {
                            swal(result.msg, "", "error");
                        }
                    },
                    error: function () {
                        swal("连接服务器错误", "", "error");
                    }
                });
            }
        })
    }

    function redio(dom, is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        if (is == 0 || !is) {
            dom.eq(0).prop('checked', 'checked');
        } else
            dom.eq(1).prop('checked', 'checked');
    }

</script>
</body>
</body>
</html>