<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="modal-header" style="border-bottom: none;">
    <h4 class="modal-title" id="myModalLabel1">
        计划回款/应收表
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="add-form" action="/sale/ppbinsert" method="post" class="form-horizontal">
        <div class="mybody">
            <div class="row" >
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4">
                            <span style="color: red">*</span> 计划回款日期:
                        </label>
                        <div class="col-md-8">
                            <div id="datetime_3" class="input-append date form_datetime input-group">
                                <input name="ppbDate" id="prDate" class="form-control" />
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4"> <span style="color: red">*</span> 期次：</label>
                        <div class="col-md-8" style="position: relative;">
                            <select class="selectpicker" data-live-search="true"
                                    data-live-search="true" name="ppbTerms" id="terms">
                                <option selected></option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-6">
                    <div class="form-group overflow" style="margin-left:-20px">
                        <label class="control-label col-md-4">
                            <span style="color: red">*</span> 金额:
                        </label>
                        <div class="col-md-8" style="margin-left: 0px">
                            <input id="ppbMoney"  name="ppbMoney" class="form-control" required
                                   style="background-color: white;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input name="serveId" style="display: none">
                        <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                            <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                        </label>
                        <div id="cus3" class="input-group col-md-8" style="padding-left: 10px;">
                            <input type="text" name="tbCustomerByCusId.cusId" id="cus" class="form-control"
                                   style="display: none">
                            <input type="text" disabled class="form-control" name="to_cus" id="to_cus">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default"
                                        style="color: black;height: 34px" onclick="chooseCus()"><i
                                        class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <div id="cus_warn" class="input-group col-md-2"
                             style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">
                            客户不能为空
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group overflow" style="overflow: visible;">
                        <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                            <span style="color: #ff0000; font-size: 16px;">*</span>合同/订单:
                        </label>
                        <div id="ord1" class="input-group col-md-8" style="padding-left: 10px;">
                            <select class="selectpicker" data-live-search="true"
                                    data-live-search="true" name="tbOrderByOrderId.orderId" id="to_order">
                                <option selected></option>
                            </select>
                        </div>
                        <div id="order_warn" class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">合同/订单不能为空</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input name="serveId" style="display: none">
                        <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                            <span style="color: #ff0000; font-size: 16px;">*</span>负责人：
                        </label>
                        <div id="user1" class="input-group col-md-8" style="padding-left: 10px;">
                            <input type="text" id="user" class="form-control"
                                   style="display: none">
                            <input type="hidden" name="tbUserByUserId.userId">
                            <input type="text" disabled class="form-control" id="to_user" name="to_user">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default"
                                        style="color: black;height: 34px" onclick="chooseCus1()"><i
                                        class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <div id="user_warn" class="input-group col-md-2"
                             style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">
                            负责人不能为空
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="ord" value="${orderEntity.orderId}">
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default" id="add-submit"><i
                        class="fa fa-check"></i>
                    保存
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    sumitForm();
    $(function () {
        setForm();
        //消除模态框数据
        $("#wsj-add").on("hidden.bs.modal", function () {
            open_modal = null
            $(this).removeData("bs.modal");
        });
        var dou = /(^[1-9]\d{0,9}(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;
        $("#add-form").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                // valid: 'glyphicon glyphicon-ok',
                // invalid: 'glyphicon glyphicon-remove',
                // validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "ppbMoney": {
                    msg: '金额验证失败',
                    validators: {
                        notEmpty: {
                            message: '金额不能为空'
                        },
                        regexp:{
                            regexp: dou,
                            message: '请输入正确的价格'
                        }
                    }
                },
                "ppbDate": {
                    msg: '日期无效',
                    validators: {
                        notEmpty: {
                            message: '日期不能为空'
                        }
                    }
                },
            }
        });

        $(".selectpicker").selectpicker({
            noneSelectedText: '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();

        $('.selectpicker').selectpicker("refresh");

        $("#datetime_3").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_3"
        }).on('hide', function (e) {
            $('#add-form').data('bootstrapValidator')
                .updateStatus('ppbDate', 'NOT_VALIDATED', null)
                .validateField('ppbDate');
        });

    })
    var orderId;
    function setForm() {
        if (open_modal != null) {
            ajax("ppbload?ppbId=" + open_modal, function (data) {
                $('#add-form').append("<input class='hidden' name='ppbId' value='" + data.ppbId + "'>")
                $('input[name="ppbDate"]').val(data.ppbDate)
                $('#terms').selectpicker('val', data.ppbTerms);
                $('input[name="ppbMoney"]').val(data.ppbMoney);
                $('#to_cus').val(data.cusName);
                ajax("/sale/queryOrderByCusId?id=" + data.cusId, function (data) {
                    var group = $('#to_order');
                    for (var i = 0; i < data.length; i++) {
                        group.append('<option value="' + data[i].orderId + '">' + data[i].orderTitle + '</option>')
                    }
                    $('#to_order').selectpicker('refresh');
                    $('#to_order').selectpicker('render');
                    $('#to_order').selectpicker('val',orderId);
                });
                var orderId = data.orderId;
                $('#to_user').val(data.userName);
            }, function (msg) {
                swal("加载失败", msg, "error");
                $('#wsj-add').modal('hide')
            }, "get", {})
        }
    }

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            type: type,
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error(result.msg)
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                }
            }
        })
    }

    function sumitForm() {
        //提交表单数据
        $('#add-submit').click(function () {
            var a=true;
            var b=true;
            var c=true;
            if($("#to_cus").val()==null||$("#to_cus").val()==undefined||$("#to_cus").val()==""){
                a=false;
                $("#cus_warn").show();
            }else{
                $("#cus_warn").hide();
            }
            if($("#to_user").val()==null||$("#to_user").val()==undefined||$("#to_user").val()==""){
                b=false;
                $("#user_warn").show();
            }else{
                $("#user_warn").hide();
            }
            if($("#to_order").val()==null||$("#to_order").val()==undefined||$("#to_order").val()==""){
                c=false;
                $("#order_warn").show();
            }else{
                $("#order_warn").hide();
            }
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                if (a == true && b == true&& c == true ) {
                    $("#add-form").ajaxSubmit({
                        dataType: 'json',
                        success: function (result) {
                            if (result.code == 200) {
                                swal("成功", result.msg, "success");
                                $('#wsj-add').modal('hide');
                                //表单重置
                                $("#add-form")[0].reset();
                                $("#table444").bootstrapTable('refresh')
                            }
                            else {
                                swal(result.msg, "", "error");
                            }
                        },
                        error: function () {
                            swal("连接服务器错误", "", "error");
                        }
                    });
                }
            }
        })
    }

    function redio(dom, is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        if (is == 0 || !is) {
            dom.eq(0).prop('checked', 'checked');
        } else
            dom.eq(1).prop('checked', 'checked');
    }
    function chooseCus() {
        window.open("/customer_check");
    }

    function getCus(cusId, cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
        ajax("/sale/queryOrderByCusId?id=" + cusId, function (data) {
            var group = $('#to_order');
            for (var i = 0; i < data.length; i++) {
                group.append('<option value="' + data[i].orderId + '">' + data[i].orderTitle + '</option>')
            }
            $('#to_order').selectpicker('refresh');
            $('#to_order').selectpicker('render');
        });
    }
    function chooseCus1() {
        // window.open("/admin/to_user_check");
        var a = $("<a href='/admin/to_user_check' target='_blank'>Apple</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent('click', true, true);
        a.dispatchEvent(e);
    }

    function getUser(userId, userName) {
        $("input[name='tbUserByUserId.userId']").val(userId);
        $("#to_user").val(userName);
    }
</script>

</body>


</body>
</html>