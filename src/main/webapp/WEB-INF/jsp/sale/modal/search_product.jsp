<%--
  查找产品
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
</head>

<body>



                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-default pull-right" id="reset-btns">
                        <i class="fa fa-recycle"></i>
                        重置
                    </button>
                    <button type="button" onclick="search_btn()" class="btn btn-default pull-right" style="margin-right: 15px">
                        <i class="fa fa-check"></i>
                        查询
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        高级查询
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="search-form" class="form-horizontal">
                        <div class="mybody">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            ID：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="format.pfId" type="number" class="form-control"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            品名：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="format.tbProductByProductId.productName" class="form-control" required="required" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" >型号：</label>
                                        <div class="col-md-8">
                                            <input class="form-control" name="format.tbProductByProductId.productModel" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>编码/条码：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="format.pfCode" class="form-control"  style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            生成厂家：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="format.pfManufacturer" class="form-control" required="required" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" >批准文号：</label>
                                        <div class="col-md-8">
                                            <input class="form-control" name="format.pfApprovalNumber" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" >规格：</label>
                                        <div class="col-md-8">
                                            <input class="form-control" name="format.pfName" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" >批次：</label>
                                        <div class="col-md-8">
                                            <input class="form-control" max="10" name="format.pfBatch" />
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">计算库存：</label>
                                        <div class="col-md-8">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="0" name="format.tbProductByProductId.productStock" >
                                                <label> 是</label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="1" name="format.tbProductByProductId.productStock" >
                                                <label> 否 </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">状态：</label>
                                        <div class="col-md-8">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="0" name="format.pfStatus" >
                                                <label> 正常 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="1" name="format.pfStatus">
                                                <label> 停售 </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>成本大于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="costRt" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>成本小于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="costLt" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>库存上限：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="format.pfMax" class="form-control"  required style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>库存下限：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" class="form-control" name="format.pfMin"  style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>价格大于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="priceRt" class="form-control"  style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>价格小于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="priceLt" class="form-control"  style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>生产日期大于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="date" name="birthFrom" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>生产日期小于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="date" name="birthTo" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>失效日期大于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="date"  name="expireFrom" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>失效日期小于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="date" name="expireTo" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="height: 50px; "></div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" onclick="search_btn()" class="btn btn-default"><i class="fa fa-check"></i>
                    查询</button>
            </div>


<script>


    $(function() {
        $('#reset-btns').click(function () {
            $('#search-form')[0].reset();
        })

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();
    });

    function search_btn() {
        search();
        $('#search-senitor').modal("hide")
    }
</script>

</body>

</html>