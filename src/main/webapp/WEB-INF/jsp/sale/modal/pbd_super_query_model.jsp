<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="modal-header" style="border-bottom: none;height: 60px">
    <h4 class="modal-title" id="myModalLabel">
        高级查询--回款记录
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" >
    <form id="search-form" >
        <div class="mybody">
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">期次：</label>
                        <div class="col-md-10">
                            <div id="asearchdt_pay_plan_serial"
                                 style="position: static; visibility: visible; top: 0px;">
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="1" name="termsList"><label>1</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="2" name="termsList"><label>2</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="3" name="termsList"><label>3</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="4" name="termsList"><label>4</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="5" name="termsList"><label>5</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="6" name="termsList"><label>6</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="7" name="termsList"><label>7</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="8" name="termsList"><label>8</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="9" name="termsList"><label>9</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="10" name="termsList"><label>10</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="11" name="termsList"><label>11</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="12" name="termsList"><label>12</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 15px">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-2 ">
                            日期：
                        </label>
                        <div class="col-md-3" style="padding-right: 0">
                            <div id="datetime_1" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="开始日期" name="from"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
                                            </span>
                            </div>

                        </div>

                        <div class="col-md-3" style="padding-left: 0">
                            <div id="datetime_2" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="结束日期" name="to"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 15px">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-2">金额：</label>
                        <div class="col-md-6">
                            <div class="input-append date form_datetime input-group">
                                <span class="input-group-addon bg-d b-0">&nbsp;&nbsp;&nbsp;<=&nbsp;&nbsp;&nbsp;</span>
                                <input name="pbdMoney" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">
                            供应商(客户)：
                        </label>
                        <div class="col-md-6">
                            <input name="cusName" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">
                            负责人：
                        </label>
                        <div class="col-md-6">
                            <input name="userName" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" id="reset-btns" style="margin-left: 5px">
                    <i class="fa fa-recycle"></i>
                    重置
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default"onclick="search_btn()" ><i
                        class="fa fa-check" ></i>
                    查询
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(function () {
        $('.selectpicker').selectpicker("refresh");
        $("#datetime_1").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_2").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });

        $('#reset-btns').click(function () {
            $('#search-form')[0].reset();
            $('.selectpicker1').selectpicker('val', '未选');
        })

        $(".selectpicker1").selectpicker({
            noneSelectedText: '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();

    });
    function search_btn(){
        search1();
        $('#wsj-search').modal("hide")
        $('.selectpicker1').selectpicker('val', '未选');
    }
    function chooseCus() {
        window.open("/customer_check");
    }
    function getCus(cusId,cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
        $.ajax({
            url: 'memorial/get_contact?id=' + $("input[name='tbCustomerByCusId.cusId']").val(),
            method: 'get',
            async: true,
            success: function (data1) {
                $("#cotsName").empty();
                for (var i = 0; i < data1.length; i++) {
                    $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                    $('#cotsName').selectpicker('refresh');
                }
                $('#cotsName').selectpicker('refresh');
            }
        });
    }
</script>


</body>
</html>