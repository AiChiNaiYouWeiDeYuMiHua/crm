<%--
  发货单
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
</head>

<body >

                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-default pull-right" id="add-btns">
                        <i class="fa fa-check"></i>
                        保存
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        发货单/发货明细
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="add-form" action="/sale/warehouse/send/save" method="post" class="form-horizontal">
                        <div class="mybody">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" style="font-size: 12px;font-weight: normal;">客户：</label>
                                        <div class=" col-md-10">
                                            <div disabled class="form-control" id="cus">
                                                <span style="font-weight:normal;color:#9e9e9e">〖</span>dada
                                                <a target="_blank" href="/customer_detail?id=1" >
                                                    <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">订单：</label>
                                        <div class="col-md-8 input-group" style="padding-right: 15px;padding-left: 15px">
                                            <div disabled class="form-control" id="order">
                                                <span style="font-weight:normal;color:#9e9e9e">〖</span>三国杀
                                                <a target="_blank" href="/sale/order/info/1/" >
                                                    <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">发货人：</label>
                                        <div class="col-md-8 input-group" style="padding-left: 15px;padding-right: 15px">
                                            <input  class="form-control" name="tbUserByUserId.userName"/>
                                            <input  type="hidden" name="tbUserByUserId.userId"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">发货日期：</label>
                                        <div class="col-md-8" style="padding-right: 15px;padding-left: 15px">
                                            <input class="form-control " type="date" name="dgDate"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">发货单号：</label>
                                        <div class="col-md-8" style="padding-right: 15px;padding-left: 15px">
                                            <input class="form-control " disabled name="dgNumber"/>
                                        </div>
                                    </div>
                                </div>

                                <div class=" col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">收货人/地址:</label>
                                        <div class=" col-md-10">
                                            <div class="panel-group" id="caddress">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title" style="width: 100%">
                                                            <a data-toggle="collapse" style="display: block;" data-parent="#show_contract_address" href="#address" aria-expanded="true" class="">
                                                                详细信息
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="address" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body">
                                                            <p><b>收货人</b></p>
                                                            <div class="row">
                                                                <div class="col-md-12 form-group overflow">
                                                                    <label class="control-label col-md-3">从联系人读取:</label>
                                                                    <div class="col-md-9">
                                                                        <select class="selectpicker" data-width="100%" id="contact-read">
                                                                            <option selected></option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 form-group ">
                                                                    <label class="control-label col-md-3">姓名:</label>
                                                                    <div class="col-md-9">
                                                                        <input class="form-control" name="tbAddressByAddressId.addressName">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 form-group ">
                                                                    <label class="control-label col-md-3">联系方式:</label>
                                                                    <div class="col-md-9">
                                                                        <input class="form-control" name="tbAddressByAddressId.addressTel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <p>
                                                                <b>地址</b>
                                                            </p>

                                                            <div class="row" id="target">
                                                                <div class="col-md-12 form-group" id="id_state">
                                                                    <label class="control-label col-md-3" id="dt_sendgoods_country_3">省</label>
                                                                    <div class="col-md-9">
                                                                        <select class="form-control" name="tbAddressByAddressId.addressProvince"></select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12 form-group" id="dt_sendgoods_country_5">
                                                                    <label class="control-label col-md-3" id="dt_sendgoods_country_4">市</label>
                                                                    <div class="col-md-9">
                                                                        <select class="form-control" name="tbAddressByAddressId.addressCity"></select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 form-group" id="dt_sendgoods_country_6">
                                                                    <label class="control-label col-md-3" id="dt_sendgoods_country_7">县</label>
                                                                    <div class="col-md-9">
                                                                        <select class="form-control" name="tbAddressByAddressId.addressCounty"></select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-md-12 form-group ">
                                                                    <label class="control-label col-md-3">地址:</label>
                                                                    <div class="col-md-9">
                                                                        <input class="form-control" name="tbAddressByAddressId.addressContent">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 form-group ">
                                                                    <label class="control-label col-md-3">邮编:</label>
                                                                    <div class="col-md-9">
                                                                        <input class="form-control" name="tbAddressByAddressId.addressCode">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">发货方式：</label>
                                       <div class="col-md-8" style="padding-right: 15px;padding-left: 15px">
                                           <select id="send-way" data-width="100%" name="dgModel" class="selectpicker">
                                               <option selected></option>
                                           </select>
                                       </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">状态：</label>
                                       <div class="col-md-8 " style="padding-right: 15px;padding-left: 15px">
                                           <select id="send-status" data-width="100%" name="status" class="selectpicker">
                                               <option selected value="1"></option>
                                               <option value="2">已发货</option>
                                               <option value="3">已签收</option>
                                           </select>
                                       </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">打包件数：</label>
                                        <div class="col-md-8" style="padding-right: 15px;padding-left: 15px">
                                            <input class="form-control" name="dgPkgNumber"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">重量：</label>
                                        <div class="col-md-8" style="padding-right: 15px;padding-left: 15px">
                                            <input class="form-control" name="dgWeight"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">物流公司：</label>
                                        <div class="col-md-8" style="padding-right: 15px;padding-left: 15px">
                                            <input class="form-control" name="dgLogistice"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">物流单号：</label>
                                        <div class="col-md-8" style="padding-right: 15px;padding-left: 15px">
                                            <input class="form-control" name="dgLogisticId"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">运费：</label>
                                        <div class="col-md-8" style="padding-right: 15px;padding-left: 15px">
                                            <input class="form-control" name="dgFreight">
                                        </div>
                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="col-md-4 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">运费结算：</label>
                                        <div class="col-md-8">
                                            <select id="send-freight"  name="dgFreightModal" class="selectpicker">
                                                <option selected></option>
                                            </select>
                                        </div>
                                </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" style="font-size: 12px;font-weight: normal;text-align: right;">备注：</label>
                                        <div class="col-md-10">
                                            <input class="form-control" name="dgOther"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">发货明细:</label>
                                    </div>
                                </div>
                                <div class="col-md-12" id="detail" style="padding-right: 15px;padding-left: 15px">
                                    <table style="font-size: 10px" id="table-detail"></table>
                                </div>
                            </div>

                        </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default" type="submit" id="add-submit"><i class="fa fa-check"></i>
                    保存</button>
            </div>

                <script type="text/javascript" src="/js/sale/distpicker.data.js"></script>
                <script type="text/javascript" src="/js/sale/distpicker.js"></script>
                <script type="text/javascript" src="/js/sale/main.js"></script>
<script>

    $(function() {
        sumitForm()
        $("#table-detail").bootstrapTable({
            method: 'get',
            striped: true,

            cache: false,
            pagination: false,
            sortable: false,
            sortOrder: "desc",
            pageNumber: 1,
            pageSize: 10,
            url: "/sale/warehouse/send/detail?id=${id}",
            sidePagination: "server",
            queryParamsType: '',

            columns: [{
                field: 'formatEntity.tbProductByProductId.productName',
                title: '品名',
                align: 'center',
                formatter:isnull
//			footerFormatter: "合计"
            }, {
                field: 'formatEntity.tbProductByProductId.productModel',
                title: '型号',
                align: 'center',
                formatter:isnull
            }, {
                field: 'formatEntity.pfName',
                title: '规格',
                align: 'center',
                formatter:isnull
            }, {
                field: 'formatEntity.pfUnit',
                title: '单位',
                align: 'center',
                formatter:isnull
            }, {
                field: 'ddNumber',
                title: '数量',
                align: 'center',
                formatter:isnull
//			footerFormatter: "合计"
            }, {
                field: 'ddPrice',
                title: '单价',
                align: 'center',
                formatter:function (value,row,index) {
                    if (value != null)
                        return fmoney(value,2);
                    else
                        return "";
                }
            },{
                field: 'price',
                title: '折扣',
                align: 'center',
                formatter:function (value,row,index) {
                    if (value != null ) {

                        var add = (-1*(row.ddNumber*row.ddPrice-row.ddTotal));
                        if (row.ddNumber*row.ddPrice == 0 || add == 0)
                            return "";
                        if (add > 0)
                            return "<span style='color: red'>"+fmoney(add,2)+"</span>("+100*(row.ddTotal/(row.ddNumber*row.ddPrice))+"%)";
                        else
                            return "<span style='color: red'>"+fmoney(add,2)+"</span>("+100*(row.ddTotal/(row.ddNumber*row.ddPrice))+"%)";
                    }else
                        return "";
                }
//			footerFormatter: "合计"
            }, {
                field: 'ddTotal',
                title: '金额',
                align: 'center',
                formatter:function (value,row,index) {
                    if (value != null)
                        return fmoney(value,2);
                    else
                        return "";
                }
//			footerFormatter: "合计"
            },  {
                field: 'ddOther',
                title: '备注',
                align: 'center',
                formatter:isnull
            }],
            onLoadSuccess: function(data) {
                if (data.length > 0){
                    var total = 0;
                    var n = 0;
                    for (var i =0;i<data.length;i++) {
                        total += parseFloat(data[i].total);
                        n += parseFloat(data[i].number)
                    }
                    addFotter($('#table1'), {
                        productName: '合计',
                        number:n,
                        total: fmoney(total,2),
                    })

                    addFotter($('#table1'), {
                        productName: '合计(大写金额)',
                        other: digit_uppercase(total),
                    });
                    formatterFotter($('#table1'), 'number', 1, 2);
                    formatterFotter($('#table1'), 'other', 1, 1);
                }
            },
            onLoadError: function(status) {
            }
        })
        $("#add-form").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
        });
        setParameter();
        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });
        $("[data-toggle='tooltip' ] ").tooltip();
    });

    function setParameter() {
        getParameter($('#send-way'),7);
        getParameter($('#send-freight'),8);

    }
    //获取参数
    function getParameter(dom,id) {
        return ajax("/get_dimission",success = function (data) {
            for (var i =0; i<data.length;i++){
                dom.append('<option>'+data[i].dimissionName+'</option>')
            }
            if (id == 8)
                setForm();
            dom.selectpicker('refresh');
            dom.selectpicker('render');
        },function () {},"post",{typeId:id});
    }
    function setForm() {
            ajax("/sale/warehouse/send/load?id=${id}",function (data) {
                $('#add-form').append("<input class='hidden' name='dgId' value='" + data.dgId + "'>");
                $('#add-form input[name="tbUserByUserId.userId"]').val(data.userId)
                $('#add-form input[name="tbUserByUserId.userName"]').val(data.userName)
                $('#add-form input[name="dgDate"]').val(data.dgDate)
                $('#add-form input[name="dgNumber"]').val(data.dgNumber)
                $('#add-form input[name="dgPkgNumber"]').val(data.dgPkgNumber)
                $('#add-form input[name="dgOther"]').val(data.dgOther)
                $('#add-form input[name="dgWeight"]').val(data.dgWeight)
                $('#add-form input[name="dgLogistice"]').val(data.dgLogistice)
                $('#add-form input[name="dgLogisticId"]').val(data.dgLogisticId)
                $('#add-form input[name="dgFreight"]').val(fmoney(data.dgFreight,2))
                $('#add-form #send-way').selectpicker('val',data.dgModel)
                $('#add-form #send-status').selectpicker('val',data.status)
                $('#add-form #send-freight').selectpicker('val',data.dgFreightModal)

                if (data.cusName != null) {
                    var cus = '<span style="font-weight:normal;color:#9e9e9e">' +
                        '〖</span>' + data.cusName +
                        '<a target="_blank" href="/customer_detail?id=' + data.cusId +
                        '"> <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>'
                    $('#add-form #cus').html(cus);
                }
                if (data.orderId != null) {
                    var order = '<span style="font-weight:normal;color:#9e9e9e">' +
                        '〖</span>' + data.orderTitle +
                        '<a target="_blank" href="/sale/order/info/' + data.orderId +
                        '/"> <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>'
                    $('#add-form #order').html(order)
                    setAddress(data.tbAddressByAddressId)
                }
            },function (msg) {
                swal("加载失败",msg,"error");
                $('#add').modal('hide')
            },"get",{})
        }
    function setAddress(data) {
        if (data == null) {
            $('#add-form #target').distpicker({
                province: "湖南省",
                city: "株洲市",
                district: "天元区"
            });
            return
        }
        $('#add-form input[name="tbAddressByAddressId.addressName"]').val(data.addressName)
        $('#add-form input[name="tbAddressByAddressId.addressTel"]').val(data.addressTel)
        $('#add-form input[name="tbAddressByAddressId.addressContent"]').val(data.addressContent)
        $('#add-form input[name="tbAddressByAddressId.addressCode"]').val(data.addressCode)
        $('#add-form #target').distpicker({
            province: data.addressProvince,
            city: data.addressCity,
            district: data.addressCounty
        });
    }
    function sumitForm() {
        //提交表单数据
        $('#add-submit,#add-btns').click(function () {
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                $("#add-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            }
        })
    }

</script>

</body>

</html>