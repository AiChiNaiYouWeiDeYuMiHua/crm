<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="modal-header" style="border-bottom: none;">
    <h4 class="modal-title" id="myModalLabel">
        高级查询--开票记录
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="search-form1" >
        <div class="mybody">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">票据类型:</label>
                        <div class="col-md-10">
                            <div id="asearchdt_pay_plan_serial"
                                 style="position: static; visibility: visible; top: 0px;">
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="增值" name="typeList"><label>增值</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="普通国税" name="typeList"><label>普通国税</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="普通地税" name="typeList"><label>普通地税</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="收据" name="typeList"><label>收据</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-2 ">
                            开票日期：
                        </label>
                        <div class="col-md-3" style="padding-right: 0">
                            <div id="datetime_1" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="开始日期" name="from"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
                                            </span>
                            </div>

                        </div>

                        <div class="col-md-3" style="padding-left: 0">
                            <div id="datetime_2" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="结束日期" name="to"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-2">金额：</label>
                        <div class="col-md-6">
                            <div class="input-append date form_datetime input-group">
                                <span class="input-group-addon bg-d b-0">&nbsp;&nbsp;&nbsp;<=&nbsp;&nbsp;&nbsp;</span>
                                <input name="rrMoney" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-2">
                            供应商(客户)：
                        </label>
                        <div class="col-md-6">
                            <input  class="form-control" required="required"
                                    style="background-color: white;" name="cusId"/>
                            <div class="" style="margin-top: 10px; margin-bottom: 10px;">
                                <div class="form-group">
                                    <input class="form-control" placeholder="关键字"
                                           style="padding-right: 12px; width: 120px; float: left; margin-right: 5px;"/>

                                    <button class="btn btn-default" style="float: left; margin-right: 5px;">
                                        <i class="fa fa-search"></i></button>
                                    <a class="btn btn-default" data-toggle="tooltip" data-placement="bottom"
                                       title="浏览">
                                        <i class="fa fa-list"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4">负责人：</label>
                        <div class="col-md-8">
                            <select  class="selectpicker1" data-live-search="true"
                                     data-live-search="true" name="userId" id="user">
                                <option selected></option>
                                <option>option1</option>

                                <option>option2</option>

                                <option>option3</option>

                                <option>option4</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" id="reset-btns" style="margin-left: 5px">
                    <i class="fa fa-recycle"></i>
                    重置
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default"onclick="search_btn()" ><i
                        class="fa fa-check" ></i>
                    查询
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(function () {
        $('.selectpicker').selectpicker("refresh");
        $("#datetime_1").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_2").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });

        $('#reset-btns').click(function () {
            $('#search-form')[0].reset();
            $('.selectpicker1').selectpicker('val', '未选');
        })

        $(".selectpicker1").selectpicker({
            noneSelectedText: '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();

    });
    function search_btn(){
        search1();
        $('#wsj-search').modal("hide")
        $('.selectpicker1').selectpicker('val', '未选');
    }
</script>


</body>
</html>