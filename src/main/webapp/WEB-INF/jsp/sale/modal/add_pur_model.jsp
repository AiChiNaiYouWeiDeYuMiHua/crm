<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal-header" style="border-bottom: none;height: 60px">
    <h4 class="modal-title" id="myModalLabel1">
        采购单
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="add-form" action="/sale/pur_insert" method="post" class="form-horizontal">
        <hr class="boder-t-a"/>
        <div class="mybody">
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-2">
                            <span style="color: red">*</span> 采购主题:
                        </label>
                        <div class="col-md-10" style="margin-left: 0px">
                            <input id="purTheme" name="purTheme" class="form-control" required
                                   style="background-color: white;width: 95%;" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input name="serveId" style="display: none">
                        <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                            <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                        </label>
                        <div id="cus2" class="input-group col-md-8" style="padding-left: 10px;">
                            <input type="text" name="tbCustomerByCusId.cusId" id="cus" class="form-control"
                                   style="display: none">
                            <input type="text" disabled class="form-control" name="to_cus" id="to_cus">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default"
                                        style="color: black;height: 34px" onclick="chooseCus()"><i
                                        class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <div id="cus_warn" class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">客户不能为空</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input name="serveId" style="display: none">
                        <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                            <span style="color: #ff0000; font-size: 16px;">*</span>负责人：
                        </label>
                        <div id="user1" class="input-group col-md-8" style="padding-left: 10px;">
                            <input type="text" id="user" class="form-control"
                                   style="display: none">
                            <input type="hidden" name="tbUserByUserId.userId">
                            <input type="text" disabled class="form-control" id="to_user" name="to_user">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default"
                                        style="color: black;height: 34px" onclick="chooseCus1()"><i
                                        class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <div id="user_warn" class="input-group col-md-2"
                             style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">
                            负责人不能为空
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                        <div class="form-group overflow" style="overflow: visible;">
                            <label class="control-label col-md-4"><span style="color: #ff0000; font-size: 16px;">*</span>仓库：</label>
                            <div class="col-md-8">
                                <select id="modal-wh" data-width="100%" required name="tbWarehouseByWhId.whId" class="selectpicker">
                                    <option selected></option>
                                </select>
                            </div>
                            <div id="wh_warn"class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;"> 仓库不能为空
                            </div>
                        </div>
                    </div>
            </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4">采购单号:</label>
                        <div class="col-md-8" style="margin-left: 0px">
                            <input id="purOddNumbers" name="purOddNumbers" class="form-control"
                                   style="background-color: white;width: 95%;"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group overflow"style="overflow: visible">
                        <label class="control-label col-md-4">
                            <span style="color: red">*</span> 采购日期:
                        </label>
                        <div class="col-md-8" >
                            <div id="datetime_3" class="input-append date form_datetime input-group">
                                <input name="purDate" id="purDate" class="form-control" required
                                       style="background-color: white"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row"  >
                <div class="col-md-6">
                    <div class="form-group "style="overflow: visible">
                        <label class="control-label col-md-4">
                            <span style="color: red;">*</span>预计到货日期:
                        </label>
                        <div class="col-md-8">
                            <div id="datetime_4" class="input-append date form_datetime input-group">
                                <input name="purProjectedDate" id="purProjectedDate" class="form-control" required
                                       style="background-color: white"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-2">采购物品明细:</label>
                        <div class="col-md-10" style="margin-left: 0px" id="detail">
                            <div id="content">
                                    <div style="margin-top: 10px">
                                <table id="table22">
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default" id="add-submit"><i
                        class="fa fa-check"></i>
                    保存
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    sumitForm();
    $(function () {
        getWareHouse();
        if (open_modal == null)
            $("#detail").html(" <input id=\"tbPurDetailByPdId\" name=\"tbPurDetailByPdId\" class=\"form-control\" required\n" +
                "                                   style=\"width: 95%;\" placeholder=\"保存后方可编辑\" disabled/>")
        else{
            initTable();
        }

        //消除模态框数据
        $("#wsj-add").on("hidden.bs.modal", function () {
            open_modal = null
            $(this).removeData("bs.modal");
        });
        setForm();
        $("#add-form").bootstrapValidator({
            // excluded : [':disabled'],
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标

            },
            fields: {
                "purTheme": {
                    msg: '主题验证失败',
                    validators: {
                        notEmpty: {
                            message: '主题不能为空'
                        }
                    }
                },

                "purProjectedDate": {
                    msg: '日期无效',
                    validators: {
                        notEmpty: {
                            message: '日期不能为空'
                        }
                    }
                },
                "purDate": {
                    msg: '日期无效',
                    validators: {
                        notEmpty: {
                            message: '日期不能为空'
                        }
                    }
                },

            }
        });

        $(".selectpicker").selectpicker({
            noneSelectedText: '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();


        $("#datetime_3").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_3"
        }).on('hide', function (e) {
            $('#add-form').data('bootstrapValidator')
                .updateStatus('purDate', 'NOT_VALIDATED', null)
                .validateField('purDate');
        });
        $("#datetime_4").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_4"
        }).on('hide', function (e) {
            $('#add-form').data('bootstrapValidator')
                .updateStatus('purProjectedDate', 'NOT_VALIDATED', null)
                .validateField('purProjectedDate');
        });


    })
    function getWareHouse() {
        return ajax("/sale/warehouse/wh",success = function (data) {
            var group = $('#modal-wh');
            for (var i =0; i<data.length;i++){
                group.append('<option value="'+data[i].whId+'">'+data[i].whName+'</option>')
            }
            setForm();
            $('#modal-wh').selectpicker('refresh');
            $('#modal-wh').selectpicker('render');
        });
    }
    function setForm() {
        if (open_modal != null) {
            ajax("/sale/purload?purId=" + open_modal, function (data) {
                $('#add-form').append("<input class='hidden' name='purId' value='" + data.purId + "'>")
                $('input[name="purTheme"]').val(data.purTheme);
                $('input[name="purOddNumbers"]').val(data.purOddNumbers);
                $('input[name="purDate"]').val(data.purDate);
                $('input[name="purProjectedDate"]').val(data.purProjectedDate);
                $('#to_cus').val(data.cusName);
              //  $('#modal-wh').val(data.whName);
                $('#modal-wh').selectpicker('val',data.whName);
                $('#to_user').val(data.userName);
            }, function (msg) {
                swal("加载失败", msg, "error");
                $('#wsj-add').modal('hide')
            }, "get", {})
        }
    }

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            type: type,
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error(result.msg)
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                }
            }
        })
    }

    function sumitForm() {
        //提交表单数据
        $('#add-submit').click(function () {
            var a=true;
            var b=true;
            var c=true;
            if($("#to_cus").val()==null||$("#to_cus").val()==undefined||$("#to_cus").val()==""){
                $("#cus_warn").show();
                a=false;
            }else{
                $("#cus_warn").hide();
            }
            if($("#to_user").val()==null||$("#to_user").val()==undefined||$("#to_user").val()==""){
                $("#user_warn").show();
                b=false;
            }else{
                $("#user_warn").hide();
            }
            if($("#modal-wh").val()==null||$("#modal-wh").val()==undefined||$("#modal-wh").val()==""){
                $("#wh_warn").show();
                c=false;
            }else{
                $("#wh_warn").hide();
            }
            var text = $('#user option:selected').text();
            if (text == "" || text == null) {
                $("#user1").show();
            }
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                if (a == true && b == true && c == true) {
                    $("#add-form").ajaxSubmit({
                        dataType: 'json',
                        success: function (result) {
                            if (result.code == 200) {
                                var open_modal1 = result.msg;
                                // swal("成功", "操作成功", "success")
                                $('#wsj-add').modal('hide');
                                //表单重置
                                $("#add-form")[0].reset();
                                if (open_modal1 != null) {
                                    var b = "/sale/purdetail1/" + open_modal1 + "/";
                                    window.open(b);
                                }
                            }

                            else {
                                swal(result.msg, "", "error");
                            }
                        },
                        error: function () {
                            swal("连接服务器错误", "", "error");
                        }
                    });
                }
            }
        })
    }

    function redio(dom, is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        if (is == 0 || !is) {
            dom.eq(0).prop('checked', 'checked');
        } else
            dom.eq(1).prop('checked', 'checked');
    }
 function editDetail() {
     var b = "/sale/purdetail1/" + ${info.purId} + "/";
     alert(b)
     window.open(b);
 }
    function initTable() {
        $("#table22").bootstrapTable({
            method: 'post',
            url: "/sale/purProDetail1?id=" + open_modal,
            striped: true,
            showFooter: true,
            queryParams: function (params) {
                //表单转json(去掉孔项目，节省流量
                var d = f($('#search-form'));
                var page = {};
                page["size"] = params.limit;
                page["page"] = (params.offset / params.limit) + 1;
                var sort = params.sort;
                if (sort != null && sort.length > 0) {
                    page["sortName"] = sort;
                    page["sortOrder"] = params.order
                }
                d["page"] = page;
                return d
            },
            pageList: [10, 25, 50, 100],
            pageNumber: 1,
            pageSize: 5,
            contentType: "application/json",
            strictSearch: false,
            minimumCountColumns: 2,
            detailView: false,
            cache: false,
            sortable: true,
            sortOrder: "asc",
            sortName: 'pdId',
            uniqueId: "pdId",
            columns: [{
                // field: 'id', // 返回json数据中的name
                title: '序号', // 表格表头显示文字
                align: 'center', // 左右居中
                width: 80,
                valign: 'middle', // 上下居中
                formatter: function (value, row, index) {
                    return tableIndexNum(index);
                },
                footerFormatter: function (value) {
                    return "合计";
                }
            }, {
                field: 'purTheme',
                title: '采购单',
                align: 'center',
                valign: 'middle',
                formatter: f1
                // formatter:moneyFormatter,
                // footerFormatter:sumFormatter
            }, {
                field: 'title',
                title: '产品名称',
                align: 'center',
                valign: 'middle',
                formatter: isNullFormatter
            }, {
                field: 'pdPrice',
                title: '单价',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: moneyFormatter,
                footerFormatter: sumFormatter
            }, {
                field: 'pdNum',
                title: '数量',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: moneyFormatter,
                footerFormatter: sumFormatter
            }, {
                field: 'pdMoney',
                title: '金额',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: moneyFormatter,
                footerFormatter: sumFormatter
            }
            ],
            onLoadSuccess: function (data) { //加载成功时执行
                console.info("加载成功");
            },
            onLoadError: function () { //加载失败时执行
                console.info("加载数据失败");
            },

            //>>>>>>>>>>>>>>导出excel表格设置
            showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
            exportDataType: "basic", //basic', 'all', 'selected'.
            exportTypes: ['excel', 'xlsx'], //导出类型
            //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
            exportOptions: {
                ignoreColumn: [0, 0],            //忽略某一列的索引
                fileName: '数据导出', //文件名称设置
                worksheetName: 'Sheet1', //表格工作区名称
                tableName: '商品数据表',
                excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
                //onMsoNumberFormat: DoOnMsoNumberFormat
            }
            //导出excel表格设置<<<<<<<<<<<<<<<<
        });
    }

    function isNullFormatter(value, rows, index) {
        if (value == null)
            return "";
        else
            return value;
    }

    function moneyFormatter(value, rows, index) {
        if (value == null)
            return "";
        else {
            var money = transMoney(value);
            return "&nbsp;￥" + money;
        }
    }

    function f1(value, rows, index) {
        if (value == null)
            return "";
        else {
            return "<p align='center'><span style='font-weight:normal;color:#9e9e9e'>〖</span>" + value + "&nbsp;<a href='');><i class='fa fa-folder-open m-l-5'></i></a><span style='font-weight:normal;color:#9e9e9e'>〗</span></p>";
        }
    }

    function transMoney(money) {
        if (money && money != null) {
            money = String(money);
            var left = money.split('.')[0], right = money.split('.')[1];
            right = right ? (right.length >= 2 ? '.' + right.substr(0, 2) : '.' + right + '0') : '.00';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);
            return (Number(money) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
        } else if (money === 0) {   //注意===在这里的使用，如果传入的money为0,if中会将其判定为boolean类型，故而要另外做===判断
            return '0.00';
        } else {
            return "";
        }
    }

    function sumFormatter(data) {
        field = this.field;
        var sum1 = data.reduce(function (sum, row) {
            return sum + (+row[field]);
        }, 0);
        return "￥" + transMoney(sum1);
    }

    //表格自动序号
    function tableIndexNum(index) {
        var currentPage = $(".page-item.active").find('a').text();
        var size = $(".page-list .page-size").text();
        if (size == null || size.length < 0)
            size = 5;
        return Number(index + 1 + eval((currentPage - 1) * size));
    }

    //表单数据转json对象
    function f(form) {
        var d = {};
        var t = form.serializeArray();
        $.each(t, function () {
            if (this.value != null && this.value.length > 0)
                d[this.name] = this.value;
        });
        return d;
    }
    function chooseCus() {
        window.open("/customer_check");
    }

    function getCus(cusId, cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
    }
    function chooseCus1() {
        // window.open("/admin/to_user_check");
        var a = $("<a href='/admin/to_user_check' target='_blank'>Apple</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent('click', true, true);
        a.dispatchEvent(e);
    }

    function getUser(userId, userName) {
        $("input[name='tbUserByUserId.userId']").val(userId);
        $("#to_user").val(userName);
    }
</script>
</body>
</body>
</html>