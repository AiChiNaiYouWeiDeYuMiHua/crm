<%--
  添加合同
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
</head>

<body >

                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-default pull-right" id="add-btns">
                        <i class="fa fa-check"></i>
                        保存
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        出库单
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="add-form" action="/sale/warehouse/out/save" method="post" class="form-horizontal">
                        <div class="mybody">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>标题：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="wpullName" class="form-control" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" >
                                            <span style="color: #ff0000; font-size: 16px;"></span>状态：
                                        </label>
                                        <div class="col-md-8" >
                                            <input class="form-control" disabled id="wpullStatus"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" >
                                            <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                                        </label>
                                        <div class="col-md-10">
                                            <div class="form-control" id="cusName" disabled=""></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">对应单据：</label>
                                        <div class="col-md-8">
                                            <div id="orderName" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">采购退货单：</label>
                                        <div class="col-md-8">
                                            <div id="rdName" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4"><span style="color: #ff0000; font-size: 16px;">*</span>仓库：</label>
                                        <div class="col-md-8">
                                            <select id="modal-wh" data-width="100%" required name="tbWarehouseByWhId.whId" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>填单日期：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="wpullDate" type="date"  class="form-control"   />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>经手人：
                                        </label>
                                        <div class="col-md-8 input-group" style="padding-left: 15px">
                                            <input name="tbUserByUserId.userId" type="hidden" value="${user.userId}" class="form-control"  />
                                            <input name="tbUserByUserId.userName" disabled id="userName" value="${user.userName}" class="form-control"  />
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        style="color: black;height: 34px" onclick="chooseUser()"><i
                                                        class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>执行人：
                                        </label>
                                        <div class="col-md-8">
                                            <input id="execName"  disabled class="form-control"  />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-12">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-2">备注：</label>
                                    <div class="col-md-10">
                                            <textarea name="wpullOther" class="form-control" ></textarea>
                                    </div>
                                </div>
                            </div>
                </div>
                        </div>
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default" type="submit" id="add-submit"><i class="fa fa-check"></i>
                    保存</button>
            </div>


<script>
    sumitForm();
    $(function() {
        getWareHouse();
        $("#add-form").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
        });

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });
        $("[data-toggle='tooltip' ] ").tooltip();
    });
    function getWareHouse() {
        return ajax("/sale/warehouse/wh",success = function (data) {
            var group = $('#modal-wh');
            for (var i =0; i<data.length;i++){
                group.append('<option value="'+data[i].whId+'">'+data[i].whName+'</option>')
            }
            setForm();
            $('#modal-wh').selectpicker('refresh');
            $('#modal-wh').selectpicker('render');
        });
    }
    function setForm() {
        if (open_modal != null) {
            ajax("/sale/warehouse/out/load?id="+open_modal,function (data) {
                $('#add-form').append("<input class='hidden' name='wpullId' value='" + data.wpullId + "'>");
                $('#add-form input[name="wpullName"]').val(data.wpullName)
                if (data.wpullStatus == 1)
                    $('#add-form input[name="wpullStatus"]').val("已出库")
                else if (data.wpullStatus == 2)
                    $('#add-form input[name="wpullStatus"]').val("已发货")
                else{
                    $('#add-form input[name="wpullStatus"]').val("未出库")
                }
                if (data.cusName != null) {
                    var cus = '<span style="font-weight:normal;color:#9e9e9e">' +
                        '〖</span>' + data.cusName +
                        '<a target="_blank" href="/customer_detail?id=' + data.cusId +
                        '"> <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>'
                    $('#add-form #cusName').html(cus);
                }
                if (data.orderName != null) {
                    var order = '<span style="font-weight:normal;color:#9e9e9e">' +
                        '〖</span>' + data.orderName +
                        '<a target="_blank" href="/sale/order/info/' + data.orderId +
                        '/"> <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>'
                    $('#add-form #orderName').html(order)
                }
                $('#add-form #rdName').html(data.rdName)
                $('#add-form #modal-wh').selectpicker("val",data.wareHouseId)
                $('#add-form input[name="wpullDate"]').val(data.wpullDate)
                $('#add-form input[name="tbUserByUserId.userId"]').val(data.userId)
                $('#add-form input[name="tbUserByUserId.userName"]').val(data.userName)
                $('#add-form #execName').val(data.executName);
                $('#add-form input[name="wpullOther"]').val(data.wpullOther);
            },function (msg) {
                swal("加载失败",msg,"error");
                $('#add').modal('hide')
            },"get",{})
        }else {
            $('#file-btn').click(function () {
                swal('', '请保存该数据后，再上传附件 ', 'info')
            });
            defaultTime();
        }
    }

    function sumitForm() {
        //提交表单数据
        $('#add-submit,#add-btns').click(function () {
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                $("#add-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            }
        })
    }
    //设置默认时间
    function defaultTime() {
        var time = new Date();
        var day = ("0" + time.getDate()).slice(-2);
        var month = ("0" + (time.getMonth() + 1)).slice(-2);
        var today = time.getFullYear() + "-" + (month) + "-" + (day);

        $('input[name="wpullDate"]').val(today)
    }

    function chooseUser() {
        // window.open("/admin/to_user_check");
        var a = $("<a href='/admin/to_user_check' target='_blank'>Apple</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent( 'click', true, true );
        a.dispatchEvent(e);
    }

    function getUser(userId, userName) {
        $("input[name='tbUserByUserId.userId']").val(userId);
        $("#userName").val(userName);
    }
</script>

</body>

</html>