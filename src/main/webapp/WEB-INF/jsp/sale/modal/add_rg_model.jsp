<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/20
  Time: 11:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="modal-content" style="background-color: #EBEFF2"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> <h4 class="modal-title" id="basepop_popview_title">如何退货</h4> </div> <div class="modal-body" id="basepop_popview_body">
    <div id="basewinedit" style="position:absolute; visibility:hidden;left:0;	top:0; width:0;	height:0; z-index:100;"><iframe style="width: 0; height: 0; border: 0;" scrolling="no" name="basewineditfrm"></iframe></div>

    <form method="POST" action="/system/basepopok.xt" name="pageact" target="basewineditfrm">
        <input type="hidden" name="func" value="purreturn_help"><input type="hidden" name="mdb" value="3392eb">
        <input type="hidden" name="id" value="0">
        <p><b>采购单退货方法：</b></p>
        <ol>
            <li>在采购管理中，找到需要退货的采购单；</li>
            <li>
                在采购视图内点击“办理退货”即可。</li>
            <li>
                只有采购完成（无入库）、部分入库或者入库完成的采购单允许办理退货。
            </li>
        </ol>


    </form>

    <!--UTF8--></div> </div>
</body>
</html>
