<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="modal-header" style="border-bottom: none;height: 60px">
    <h4 class="modal-title" id="myModalLabel1">
        维修工单
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="add-form" action="/saleAfter/mt_insert" method="post" class="form-horizontal">
            <span class="modal-title">
                单号与接件
            </span>
        <div class="mybody">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group ">
                        <label class="control-label col-md-2">
                            <span style="color: red">*</span> 主题:
                        </label>
                        <div class="col-md-10" style="margin-left: 0px">
                            <input id="mtTheme" name="mtTheme" class="form-control" required
                                   style="background-color: white;" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 15px;">
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4">
                            <span style="color: red">*</span> 接件日期:
                        </label>
                        <div class="col-md-8">
                            <div id="datetime_3" class="input-append date form_datetime input-group">
                                <input name="mtDate" id="mtDate" class="form-control" required/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input name="serveId" style="display: none">
                            <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                                <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                            </label>
                            <div class="input-group col-md-9" style="padding-left: 10px;">
                                <input type="text" name="tbCustomerByCusId.cusId" id="cus" class="form-control"
                                       style="display: none">
                                <input type="text" disabled class="form-control" name="to_cus" id="to_cus">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick="chooseCus()"><i
                                            class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="cus_warn" class="input-group col-md-2"
                                 style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">
                                客户不能为空
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group overflow" style="overflow: visible;">
                            <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                                <span style="color: #ff0000; font-size: 16px;">*</span>合同/订单:
                            </label>
                            <div id="ord1" class="input-group col-md-9" style="padding-left: 10px;">
                                <select class="selectpicker" data-live-search="true"
                                        data-live-search="true" name="orderEntity.orderId" id="to_order" >
                                    <option selected></option>
                                </select>
                            </div>
                            <div id="order_warn" class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">合同/订单不能为空</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input name="serveId" style="display: none">
                            <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                                <span style="color: #ff0000; font-size: 16px;">*</span>负责人：
                            </label>
                            <div id="user1" class="input-group col-md-9" style="padding-left: 10px;">
                                <input type="text" id="user" class="form-control"
                                       style="display: none">
                                <input type="hidden" name="tbUserByUserId.userId" >
                                <input type="text" disabled class="form-control" id="to_user" name="to_user">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick="chooseCus1()"><i
                                            class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="user_warn"class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;"> 负责人不能为空
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4"><span style="color: red">*</span> 手机：</label>
                            <div class="col-md-8" style="margin-left: 0px">
                                <input id="mtTel" name="mtTel" class="form-control" required
                                       style="background-color: white;width: 95%;"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4"><span style="color: red">*</span> 姓名：</label>
                            <div class="col-md-8" style="margin-left: 0px">
                                <input id="mtName" name="mtName" class="form-control" required
                                       style="background-color: white;width: 95%;"/>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="modal-title" style="margin-left:15px">
接件详情
            </span>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group overflow" style="overflow: visible;">
                            <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                                <span style="color: #ff0000; font-size: 16px;">*</span>维修产品：
                            </label>
                            <div id="pro1" class="input-group col-md-9" style="padding-left: 10px;">
                                <select class="selectpicker" data-live-search="true"
                                        data-live-search="true" name="tbProductByProductId.pfId" id="to_pro">
                                    <option selected></option>
                                </select>
                            </div>
                            <div id="pro_warn" class="input-group col-md-2"
                                 style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">
                                维修产品不能为空
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                <span style="color: red">*</span> 产品生产日期:
                            </label>
                            <div class="col-md-8">
                                <div id="datetime_4" class="input-append date form_datetime input-group">
                                    <input name="mtProDate" id="mtProDate" class="form-control" required/>
                                    <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group " style="margin-left:-20px">
                            <label class="control-label col-md-4">
                                保修:
                            </label>
                            <div class="col-md-8" style="position: relative;">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="0" name="mtIsProtect" id="0" checked>
                                    <label> 在保 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="1" id="1" name="mtIsProtect">
                                    <label> 出保 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <div class="form-group no-margin">
                                <label class="control-label col-md-4">故障描述:</label>
                                <div class="col-md-8" style="position: relative;">
                                    <textarea name="mtDescript" class="form-control" id="mtDescript"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="modal-title" style="margin-left:15px">
费用与执行
            </span>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group " style="margin-left:-20px">
                            <label class="control-label col-md-4">
                                <span style="color: red">*</span>费用:
                            </label>
                            <div class="col-md-8" style="margin-left: 0px">
                                <input id="mtPay" name="mtPay" class="form-control" required
                                       style="background-color: white;width: 95%;"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                <span style="color: red">*</span> 约定交付日期:
                            </label>
                            <div class="col-md-8">
                                <div id="datetime_5" class="input-append date form_datetime input-group">
                                    <input name="mtCompleteDate" id="mtCompleteDate" class="form-control" required/>
                                    <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4"> 状态：</label>
                            <div class="col-md-8" style="position: relative;">
                                <select class="selectpicker" data-live-search="true"
                                        data-live-search="true" name="mtState" id="mtState">
                                    <option value="接件">接件</option>
                                    <option value="待维修">待维修</option>
                                    <option value="维修中">维修中</option>
                                    <option value="待交付">待交付</option>
                                    <option value="已交付">已交付</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow" style="margin-left:-20px">
                            <div class="form-group no-margin">
                                <label class="control-label col-md-4">备注:</label>
                                <div class="col-md-8" style="position: relative;">
                                    <textarea name="mtExtract" class="form-control" id="mtExtract"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-default" id="add-submit"><i
                            class="fa fa-check"></i>
                        保存
                    </button>
                </div>
            </div>
    </form>
</div>

<script>
    sumitForm();
    $(function () {
        var dou = /(^[1-9]\d{0,9}(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;
        //消除模态框数据
        $("#wsj-add").on("hidden.bs.modal", function () {
            open_modal = null
            $(this).removeData("bs.modal");
        });
        setForm();
        $("#add-form").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标

            },
            fields: {
                "mtPay": {
                    msg: '金额验证失败',
                    validators: {
                        notEmpty: {
                            message: '金额不能为空'
                        },
                        regexp: {
                            regexp: dou,
                            message: '请输入正确的价格'
                        }
                    }
                },

                "mtTel": {
                    msg: '手机号验证失败',
                    validators: {
                        notEmpty: {
                            message: '手机号不能为空'
                        },
                        callback: {
                            message: '格式不正确',
                            callback: function (value) {
                                var re = /^0?1[3|4|5|8][0-9]\d{8}$/;
                                if (re.test(value)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                },
                "mtTheme": {
                    msg: '主题验证失败',
                    validators: {
                        notEmpty: {
                            message: '主题不能为空'
                        }
                    }
                },
                "mtDate": {
                    msg: '日期无效',
                    validators: {
                        notEmpty: {
                            message: '日期不能为空'
                        }
                    }
                },
                "mtCompleteDate": {
                    msg: '日期无效',
                    validators: {
                        notEmpty: {
                            message: '日期不能为空'
                        }
                    }
                },
                "mtProDate": {
                    msg: '日期无效',
                    validators: {
                        notEmpty: {
                            message: '日期不能为空'
                        }
                    }
                },
                "mtName": {
                    msg: '姓名无效',
                    validators: {
                        notEmpty: {
                            message: '姓名不能为空'
                        }
                    }
                },
            }
        });

        $(".selectpicker").selectpicker({
            noneSelectedText: '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();

        $('.selectpicker').selectpicker("refresh");

        $("#datetime_3").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_3"
        }).on('hide', function (e) {
            $('#add-form').data('bootstrapValidator')
                .updateStatus('mtDate', 'NOT_VALIDATED', null)
                .validateField('mtDate');
        });
        $("#datetime_4").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_3"
        }).on('hide', function (e) {
            $('#add-form').data('bootstrapValidator')
                .updateStatus('mtProDate', 'NOT_VALIDATED', null)
                .validateField('mtProDate');
        });
        $("#datetime_5").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_3"
        }).on('hide', function (e) {
            $('#add-form').data('bootstrapValidator')
                .updateStatus('mtCompleteDate', 'NOT_VALIDATED', null)
                .validateField('mtCompleteDate');
        });
        $("#to_order").change(function(){
           var a=$(this).val();
        if(a!=null&&a!=""&&a!=undefined){
            ajax("/saleAfter/loadProByOrderId?id=" + a, function (data) {
                var group = $('#to_pro');
                for (var i = 0; i < data.length; i++) {
                    group.append('<option value="' + data[i].formatterId + '">' + data[i].title + '</option>')
                }
                $('#to_pro').selectpicker('refresh');
                $('#to_pro').selectpicker('render');
            });
        }
             });

    })
    var orderId;
    var pfId;
    function setForm() {
        if (open_modal != null) {
            ajax("mtload?mtId=" + open_modal, function (data) {
                $('#add-form').append("<input class='hidden' name='mtId' value='" + data.mtId + "'>")
                $('input[name="mtTheme"]').val(data.mtTheme);
                $('input[name="mtDate"]').val(data.mtDate);
                $('#mtState').selectpicker('val', data.mtState);
                $('input[name="mtTel"]').val(data.mtTel);
                $('input[name="mtName"]').val(data.mtName);
                $('input[name="mtProDate"]').val(data.mtProDate);
                $('input[name="mtCompleteDate"]').val(data.mtCompleteDate);
                $('input[name="mtPay"]').val(data.mtPay);
                $("#" + data.mtIsProtect).attr("checked", true);
                $('#mtDescript').val(data.mtDescript);
                $('#mtExtract').val(data.mtExtract);
                $("#to_cus").val(data.cusName);
                $('#to_user').val(data.userName);
                ajax("/saleAfter/loadOrderByus1?id=" + data.cusId, function (data) {
                    var group = $('#to_order');
                    for (var i = 0; i < data.length; i++) {
                        group.append('<option value="' + data[i].orderId + '">' + data[i].orderTitle + '</option>')
                    }
                    $('#to_order').selectpicker('refresh');
                    $('#to_order').selectpicker('render');
                    $('#to_order').selectpicker('val',orderId);
                });
                ajax("/saleAfter/loadProByOrderId?id=" + data.orderId, function (data) {
                    var group = $('#to_pro');
                    for (var i = 0; i < data.length; i++) {
                        group.append('<option value="' + data[i].formatterId + '">' + data[i].title + '</option>')
                    }
                    $('#to_pro').selectpicker('refresh');
                    $('#to_pro').selectpicker('render');
                    $('#to_pro').selectpicker('val',pfId);
                });
                var orderId = data.orderId;
                var pfId = data.pfId;
            }, function (msg) {
                swal("加载失败", msg, "error");
                $('#wsj-add').modal('hide')
            }, "get", {})
        }
    }

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            type: type,
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error(result.msg)
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                }
            }
        })
    }

    function sumitForm() {
        //提交表单数据
        $('#add-submit').click(function () {
            var a = true;
            var b = true;
            var c = true;
            if ($("#to_cus").val() == null || $("#to_cus").val() == undefined || $("#to_cus").val() == "") {
                a = false;
                $("#cus_warn").show();
            } else {
                $("#cus_warn").hide();
            }
            if ($("#to_user").val() == null || $("#to_user").val() == undefined || $("#to_user").val() == "") {
                b = false;
                $("#user_warn").show();
            } else {
                $("#user_warn").hide();
            }
            if ($("#to_pro").val() == null || $("#to_pro").val() == undefined || $("#to_pro").val() == "") {
                c = false;
                $("#pro_warn").show();
            } else {
                $("#pro_warn").hide();
            }
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                if (a == true&&b == true&&c == true) {
                    $("#add-form").ajaxSubmit({
                        dataType: 'json',
                        success: function (result) {
                            if (result.code == 200) {
                                swal("成功", result.msg, "success");
                                $('#wsj-add').modal('hide');
                                //表单重置
                                $("#add-form")[0].reset();
                                $("#table777").bootstrapTable('refresh')
                            }
                            else {
                                swal(result.msg, "", "error");
                            }
                        },
                        error: function () {
                            swal("连接服务器错误", "", "error");
                        }
                    });
                }
            }
        })
    }

    function redio(dom, is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        if (is == 0 || !is) {
            dom.eq(0).prop('checked', 'checked');
        } else
            dom.eq(1).prop('checked', 'checked');
    }

    function chooseCus() {
        window.open("/customer_check");
    }

    function getCus(cusId, cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
        ajax("/saleAfter/loadOrderByus1?id=" + cusId, function (data) {
            var group = $('#to_order');
            for (var i = 0; i < data.length; i++) {
                group.append('<option value="' + data[i].orderId + '">' + data[i].orderTitle + '</option>')
            }
            $('#to_order').selectpicker('refresh');
            $('#to_order').selectpicker('render');
        });
    }
    function chooseCus1() {
        var a = $("<a href='/admin/to_user_check' target='_blank'>Apple</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent( 'click', true, true );
        a.dispatchEvent(e);
    }

    function getUser(userId, userName) {
        $("input[name='tbUserByUserId.userId']").val(userId);
        $("#to_user").val(userName);
    }
</script>

</body>


</body>
</html>