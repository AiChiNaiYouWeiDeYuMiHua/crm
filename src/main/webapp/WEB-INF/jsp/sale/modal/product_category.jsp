<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<%--
产品分类模态框
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/css/loading.css">

</head>
<body>
<div id="loading" class="loading">加载中。。。</div>
<div class="hide" id="main">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">x</button>
        <h4 class="modal-title" id="myModalLabel">分类设置 </h4>
    </div>
    <div class="modal-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h6>当前分类:<span id="category-text">产品类别->电子设备</span></h6>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th><b>操作:</b></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>添加下级分类：</td>
                            <td >
                                <div class="input-group">
                                    <input type="text" class="form-control" name="add" id="add">
                                    <g:g id="311">
                                    <span class="input-group-btn" >
                                        <button class="btn " type="button" id="add-btn" style="padding: 6px 12px">添加</button>
                                    </span>
                                    </g:g>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>修改分类名称：</td>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="modify" id="modify">
                                    <g:g id="313">
                                    <span class="input-group-btn">
                                <input class="btn" type="submit" value="改名" style="padding: 6px 12px" id="modify-btn">
                            </span>
                                    </g:g>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>删除分类：</td>
                            <td>
                                <g:g id="312">
                                <button id="delete" class="btn" style="display: inline-block">删除</button></g:g>
                                删除前请处理该分类下的产品数据
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer"></div>
</div>
<script>
    $(function () {
        setDate();
    })
    //初始化数据
    function setDate() {
        ajax("/sale/product/category/load/"+modal+'/',loadSuccess,loadError);
        modal = "";
    }

    //加载成功
    function loadSuccess(data) {
        $('#loading').addClass('hide');
        $('#main').removeClass('hide');
        $('#category-text').html(data.text);
        $('#modify-btn').click(function () {
            var name = $('#modify').val();
            if (name == null || name.length <=0)
                toastr.warning("类别名称不能为空");
            else
                ajax("/sale/product/category/modify",opearteSuccess,function () {},"post",{'id':data.id,'name':name});
        });
        $('#add-btn').click(function () {
            var name = $('#add').val();
            if (name == null || name.length <=0)
                toastr.warning("类别名称不能为空");
            else
                ajax("/sale/product/category/add",opearteSuccess,function () {},"post",{'id':data.id,'name':name});
        });
        $('#delete').click(function () {
            if(confirm("确认删除该类别"))
                ajax("/sale/product/category/delete",opearteSuccess,function () {},"post",{'id':data.id});
        })
    }
    //操作成功
    function opearteSuccess(msg) {
        $('#myModal').modal('hide');
        refresh()
        toastr.success(msg)
    }
    // 加载失败
    function loadError() {
        $('#myModal').modal('hide');
    }
    //发送ajax请求
    function ajax(url,success,error,type="get",data={}) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    toastr.error(result.msg);
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    toastr.error("连接超时");
                    error()
                }
            }
        })
    }
</script>
</body>
