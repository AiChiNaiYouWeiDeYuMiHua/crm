<%--
  查找产品
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
</head>

<body>



                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-default pull-right" id="reset-btns">
                        <i class="fa fa-recycle"></i>
                        重置
                    </button>
                    <button type="button" onclick="search_btn()" class="btn btn-default pull-right" style="margin-right: 15px">
                        <i class="fa fa-check"></i>
                        查询
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        高级查询
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="search-form" class="form-horizontal">
                        <div class="mybody">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            ID：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="orderId" type="number" class="form-control"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            所有者：
                                        </label>
                                        <div class="col-md-8">
                                            <select name="users" class="form-control selectpicker"
                                                    data-max-options="5" ></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" >
                                            <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                                        </label>
                                        <div class="col-md-8 input-group" style="padding-left: 15px;">
                                            <input id="cus" class="hidden" name="cusId"/>
                                            <div class="form-control" id="cusName" disabled></div>
                                            <span class="input-group-btn">
                                                <a onclick="returnCus()" class="btn btn-default">
                                                    <i class="fa fa-search"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>合同/订单号：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="orderNumber" class="form-control"  style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            主题：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="orderTitle" class="form-control"  />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" >我方签约人：</label>
                                        <div class="col-md-8">
                                            <input class="form-control" name="userByName" />
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">付款方式：</label>
                                        <div class="col-md-8">
                                            <select id="search-payway"  multiple data-max-options="5" data-width="100%" name="payWays" class="selectpicker" data-live-search="true" data-live-search="true">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">结款方式：</label>
                                        <div class="col-md-8">
                                            <select id="search-knot" multiple data-max-options="5" data-width="100%" name="returnWays" class="selectpicker" data-live-search="true" >
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">分类：</label>
                                        <div class="col-md-8">
                                            <select id="search-category" multiple data-max-options="5" data-width="100%" name="categorys" class="selectpicker" data-live-search="true">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>总金额大于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="totalRt" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>总金额小于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="totalLt" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>预计毛利大于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="moriRt" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>预计毛利小于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" class="form-control" name="moriLt"  style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>回款金额大于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="returnMoneyRt" class="form-control"  style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>回款金额小于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="number" name="returnMoneyLt" class="form-control"  style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>开始时间大于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="date" name="startRt" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>开始时间小于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="date" name="startLt" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>结束时间大于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="date"  name="stopRt" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-6">
                                            <span style="color: #ff0000; font-size: 16px;"></span>结束时间小于：
                                        </label>
                                        <div class="col-md-6">
                                            <input  type="date" name="stopLt" class="form-control"   style="background-color: white;" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="height: 50px; "></div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" onclick="search_btn()" class="btn btn-default"><i class="fa fa-check"></i>
                    查询</button>
            </div>


<script>
    $(function() {
        initSystem($('#search-category'),3);
        initSystem($('#search-payway'),5);
        initSystem($('#search-knot'),6);
        $('#reset-btns').click(function () {
            $('#search-form')[0].reset();
        })

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();
    });

    function search_btn() {
        search();
        $('#search-senitor').modal("hide")
    }
    //获取参数
    function initSystem(dom,id) {
        return ajax("/get_dimission",success = function (data) {
            for (var i =0; i<data.length;i++){
                dom.append('<option>'+data[i].dimissionName+'</option>')
            }
            dom.selectpicker('refresh');
            dom.selectpicker('render');
        },function () {},"post",{typeId:id});
    }
    function returnCus() {
        window.open("/customer_check");
    }

    function getCus(cusId,cusName) {
        var cusText = '<span style="font-weight:normal;color:#9e9e9e">' +
            '〖</span>'+cusName+
            '<a href="/customer_detail?id='+cusId+
            '"> <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>'
        $("#cus").val(cusId);
        $("#cusName").html(cusText);
    }
</script>

</body>

</html>