<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="modal-header" style="border-bottom: none;height: 60px">
    <h4 class="modal-title" id="myModalLabel1">
        退款记录
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="add-form" action="/sale/pr_insert" method="post" class="form-horizontal">
        <div class="mybody">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4">
                            <span style="color: red">*</span>  退款日期:
                        </label>
                        <div class="col-md-8">
                            <div id="datetime_3" class="input-append date form_datetime input-group">
                                <input name="prDate" id="prDate" class="form-control" required/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden"  value="1" name="prType" >
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4">期次：</label>
                        <div class="col-md-8" style="position: relative;">
                            <select class="selectpicker " data-live-search="true"
                                    data-live-search="true" name="prTerms" id="terms">
                                <option selected></option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden"  value="1" name="prType" >
            <div class="col-md-6">
                <div class="form-group overflow" style="margin-left:-20px">
                    <label class="control-label col-md-4">
                        <span style="color: red">*</span> 金额:
                    </label>
                    <div class="col-md-7" style="margin-left: 0px">
                        <input id="prMoney" name="prMoney" class="form-control" required
                               style="background-color: white;"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input name="serveId" style="display: none">
                    <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                        <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                    </label>
                    <div class="input-group col-md-8" style="padding-left: 10px;">
                        <input   class="form-control" value="${returnGoodsEntity.tbPurchaseByPurId.tbCustomerByCusId.cusName}" disabled required />
                        </div>
                    </div>
                    <%--<div id="cus_warn" class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">客户不能为空</div>--%>
                </div>
            </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input name="tbReturnGoodsByRgId.rgId"  value="${returnGoodsEntity.rgId}"  id="rgId"  type="hidden">
                    <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                        <span style="color: #ff0000; font-size: 16px;">*</span>退货单:
                    </label>
                    <div id="pur1" class="input-group col-md-8" style="padding-left: 10px;">
                        <input   class="form-control" value="${returnGoodsEntity.rgTheme}" disabled required />
                    </div>
                    <div id="rg_warn" class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;">退货单不能为空</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input name="serveId" style="display: none">
                    <label class="control-label col-md-2" style="padding-right: 20px;margin-left: 5px">
                        <span style="color: #ff0000; font-size: 16px;">*</span>负责人：
                    </label>
                    <div class="input-group col-md-8" style="padding-left: 10px;">
                        <input type="text" id="user" class="form-control"
                               style="display: none">
                        <input type="hidden" name="tbUserByUserId.userId" value="1">
                        <input type="text" disabled class="form-control" id="to_user" name="to_user">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default"
                                    style="color: black;height: 34px"><i
                                    class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                    <div id="user_warn"class="input-group col-md-2" style="display:none;color: #a94442; font-size: 10px;margin-left: 120px; margin-top: 5px;"> 负责人不能为空
                    </div>
                </div>
            </div>
        </div>
        <input id="pur" value="${returnGoodsEntity.tbPurchaseByPurId.purId}" type="hidden">
        <%--<input id="state" value="${purchaseEntity.purId}" type="hidden">--%>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
            <button type="button" class="btn btn-default" id="add-submit"><i
                    class="fa fa-check"></i>
                保存
            </button>
        </div>
</div>
</form>
</div>

<script>
    sumitForm();
    $(function () {
        $(".selectpicker").selectpicker({
            noneSelectedText: '未选'
        });
        $('.selectpicker').selectpicker('refresh');
        $('.selectpicker').selectpicker('render');
        var dou = /(^[1-9]\d{0,9}(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;
        //消除模态框数据
        $("#wsj-add").on("hidden.bs.modal", function () {
            open_modal = null
            $(this).removeData("bs.modal");
        });

        $("#add-form").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
            },
            fields: {
                "prMoney": {
                    msg: '金额验证失败',
                    validators: {
                        notEmpty: {
                            message: '金额不能为空'
                        },
                        regexp:{
                            regexp: dou,
                            message: '请输入正确的价格'
                        }
                    }
                },
                "prDate": {
                    msg: '日期无效',
                    validators: {
                        notEmpty: {
                            message: '日期不能为空'
                        }
                    }
                },
            }
        });

        $("[data-toggle='tooltip' ] ").tooltip();

        $('.selectpicker').selectpicker("refresh");

        $("#datetime_3").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_3"
        }).on('hide', function (e) {
            $('#add-form').data('bootstrapValidator')
                .updateStatus('ppdDate', 'NOT_VALIDATED', null)
                .validateField('ppdDate');
        });
        setForm();

    })

    function setForm() {
        if (open_modal != null) {
            ajax("prload?prId=" + open_modal, function (data) {
                $('#add-form').append("<input class='hidden' name='prId' value='" + data.prId + "'>")
                $('input[name="prDate"]').val(data.prDate)
                $('#terms').selectpicker('val', data.prTerms);
                $('input[name="prMoney"]').val(data.prMoney);
                $('input[name="prType"]').val(data.prType);
                $('#to_cus').val(data.cusName);
                $('#to_user').val(data.userName);
                $("#to_rg").val(data.rgTheme);
            }, function (msg) {
                swal("加载失败", msg, "error");
                $('#wsj-add').modal('hide')
            }, "get", {})
        }
    }

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            type: type,
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error(result.msg)
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                }
            }
        })
    }

    function sumitForm() {
        //提交表单数据
        $('#add-submit').click(function () {
            var a=true;
            var b=true;
            var c=true;
            if($("#to_user").val()==null||$("#to_user").val()==undefined||$("#to_user").val()==""){
                b=false;
                $("#user_warn").show();
            }else{
                $("#user_warn").hide();
            }
            if($("#to_pur").val()==null||$("#to_pur").val()==undefined||$("#to_pur").val()==""){
                c=false;
                $("#pur_warn").show();
            }else{
                $("#pur_warn").hide();
            }
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                if (a == true) {
                    $("#add-form").ajaxSubmit({
                        dataType: 'json',
                        success: function (result) {
                            if (result.code == 200) {
                                swal("成功", result.msg, "success");
                                $('#wsj-add').modal('hide');
                                //表单重置
                                $("#add-form")[0].reset();
                                var a=$("#pur").val();
                                var b = "/sale/purdetail1/" + a + "/";
                                window.location.href = b;
                            }
                            else {
                                swal(result.msg, "", "error");
                            }
                        },
                        error: function () {
                            swal("连接服务器错误", "", "error");
                        }
                    });
                }
            }
        })
    }

    function redio(dom, is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        if (is == 0 || !is) {
            dom.eq(0).prop('checked', 'checked');
        } else
            dom.eq(1).prop('checked', 'checked');
    }
    function chooseCus() {
        window.open("/customer_check");
    }
    function getCus(cusId,cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
    }
</script>

</body>


</body>
</html>