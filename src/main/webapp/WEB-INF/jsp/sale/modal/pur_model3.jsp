<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/7
  Time: 8:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>付款记录模态框</title>
        <!-- Bootstrap -->
        <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <link rel="stylesheet" href="../css/bootstrap-datetimepicker.css" />
        <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.css" />
        <!--<link rel="" />-->
    </head>
    <!-- 模态框（Modal） -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;            </button>
                        <h4 class="modal-title" id="myModalLabel">
                            付款记录          </h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;">采购单:</label>
                                <div class="col-md-9">
                                    <div class="form-control" disabled="disabled"><span style="font-weight:normal;color:#9e9e9e">〖</span>月初采购
                                        <a href="javascript:vopen('/lib/purchase/detail.xt?id=48','purchase48',999,600);"><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
                                </div>
                            </div>
                        </div><br><br><br>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>期次:</label>
                                <div class="col-md-4">
                                    <select class="form-control ">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                        <option>11</option>
                                        <option>12</option>
                                    </select>
                                </div>
                            </div>
                        </div><br><br><br>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3 " style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>付款日期:</label>
                                <div class="input-append date form_datetime input-group col-md-5">
                                    <input id="data" class="form-control" />
                                    <span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
                                </div>
                            </div>
                        </div><br><br><br>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>付款金额:</label>
                                <div class="col-md-4">
                                    <input class="form-control" />
                                </div>
                                <!--<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>付款方式:</label>
                                <div class="col-md-4">
                                    <select class="form-control ">
                                        <option>支票</option>
                                        <option>现金</option>
                                        <option>网上银行</option>
                                        <option>其他</option>
                                    </select>
                                </div>-->
                            </div>
                        </div><br><br><br>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>经手人:</label>
                                <div class="col-md-5">
                                    <input class="form-control" />
                                </div>
                            </div>
                        </div><br><br><br>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;">供应商（客户）:</label>
                                    <div class="col-md-9">
                                        <div class="form-control" disabled="disabled"><span style="font-weight:normal;color:#9e9e9e">〖</span>宏远
                                            <a href="javascript:vopen('/lib/purchase/detail.xt?id=48','purchase48',999,600);"><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
                                    </div>
                                </div>
                            </div>
                        </div><br><br><br>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;">对应采购退货单:</label>
                                    <div class="col-md-9">
                                        <div class="form-control" disabled="disabled"><span style="font-weight:normal;color:#9e9e9e">〖</span>宏远
                                            <a href="javascript:vopen('/lib/purchase/detail.xt?id=48','purchase48',999,600);"><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
                                    </div>
                                </div>
                            </div>
                        </div><br><br><br><br>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;">备注:</label>
                                <div class="col-md-9">
                                    <input class="form-control" />
                                </div>
                            </div>
                        </div><br><br><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭            </button>
                        <button type="button" class="btn btn-primary">
                            保存         </button>

                    </div>
                    <!-- /.modal-content -->
                </div>
            </form>
        </div>
    </div>
    <!-- /.modal -->
    <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(".form_datetime").datetimepicker({
            format: "yyyy年mm月dd日 ",
            minView: "month", //设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            pickerPosition: "bottom-left"
        });
    </script>
    </body>

</html>