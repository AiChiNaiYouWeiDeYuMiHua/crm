<%--
  添加产品
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
</head>

<body>

                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-default pull-right" id="add-btns">
                        <i class="fa fa-check"></i>
                        保存
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        产品信息
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="add-form" action="/sale/product/save" method="post" class="form-horizontal">
                        <div class="mybody">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>品名：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="tbProductByProductId.productName" class="form-control"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" >型号：</label>
                                        <div class="col-md-8">
                                            <input class="form-control" name="tbProductByProductId.productModel"  />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>编码/条码：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="pfCode" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">分类：</label>
                                        <div class="col-md-8">
                                            <select id="modal-category" name="tbProductByProductId.tbProductCategoryByPcId.pcId" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">计算库存：</label>
                                        <div class="col-md-8">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="0" name="tbProductByProductId.productStock" >
                                                <label> 是</label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="1" name="tbProductByProductId.productStock" checked>
                                                <label> 否 </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">状态：</label>
                                        <div class="col-md-8">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="0" name="pfStatus" checked>
                                                <label> 正常 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="1" name="pfStatus">
                                                <label> 停售 </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>库存：
                                        </label>
                                        <div class="col-md-8">
                                            <input  type="number" id="number" class="form-control" value="0.00" disabled  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">单位：</label>
                                        <div class="col-md-8">
                                            <select name="pfUnit" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                                <option value="台">台</option>

                                                <option value="套">套</option>

                                                <option value="只">只</option>

                                                <option value="个">个</option>
                                            </select>
                                            <a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>价格：
                                        </label>
                                        <div class="col-md-8">
                                            <input  name="pfPrice" class="form-control" required  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>成本价格：
                                        </label>
                                        <div class="col-md-8">
                                            <input   name="pfCost" class="form-control" required   />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>重量：
                                        </label>
                                        <div class="col-md-8">
                                            <input   name="pfWeight" class="form-control" min="0"    />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">重量单位：</label>
                                        <div class="col-md-8">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="kg" name="pfWeigtUnit" >
                                                <label> kg </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="g" name="pfWeigtUnit" checked>
                                                <label> g </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>库存上限：
                                        </label>
                                        <div class="col-md-8">
                                            <input  type="number" name="pfMax" class="form-control"  required  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>库存下限：
                                        </label>
                                        <div class="col-md-8">
                                            <input  type="number" class="form-control" name="pfMin" required  />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-12">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-2">备注：</label>
                                    <div class="col-md-10">
                                            <textarea name="pfOther" class="form-control" style="resize: vertical"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">产品图片：</label>
                                        <div class="col-md-10">
                                            <img class="thumbnail"  id="p-img" width="50%" height="50%" >
                                            <input name="pfImf" class="form-control" type="hidden"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" style="padding-right: 20px;">
                                            <a class="btn btn-success" onclick="InitExcelImg(fuzhi)">上传图片</a>
                                        </label>
                                        <div class="col-md-10" id="upload" style="display: none">
                                            <form id="ffImport" method="post">
                                                <div style="padding: 5px">
                                                    <input id="excelFile" type="file">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 50px; "></div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default" id="add-submit"><i class="fa fa-check"></i>
                    保存</button>
            </div>


<script>
    sumitForm();
    $(function() {
        getCategory();
        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });
        validator()
        $("[data-toggle='tooltip' ] ").tooltip();
    });
    function getCategory() {
        return ajax("/sale/product/category/all",success = function (data) {
            var group = $('#modal-category');
            for (var i =0; i<data.length;i++){
                group.append('<option value="'+data[i].id+'">'+data[i].text+'</option>')
            }
            setForm();
            $('#modal-category').selectpicker('refresh');
            $('#modal-category').selectpicker('render');
        });
    }
    function setForm() {
        if (open_modal != null) {
            ajax("/sale/product/load?id="+open_modal,function (data) {
                $('#add-form').append("<input class='hidden' name='pfId' value='"+data.id+"'>")
                $('#add-form input[name="tbProductByProductId.productName"]').val(data.productEntity.productName)
                $('#add-form input[name="tbProductByProductId.productModel"]').val(data.productEntity.productModel)
                $('#add-form input[name="pfCode"]').val(data.pfCode);
                if (data.productEntity.tbProductCategoryByPcId != null)
                    $('#add-form #modal-category').selectpicker('val',data.productEntity.tbProductCategoryByPcId.pcId);
                redio($('#add-form input[name="tbProductByProductId.productStock"]'),data.productEntity.productStock)
                redio($('#add-form input[name="pfStatus"]'),data.pfStatus);
                $('#add-form #number').val(data.number);
                $('#add-form select[name="pfUnit"]').val(data.pfUnit);
                $('#add-form input[name="pfPrice"]').val(data.pfPrice);
                $('#add-form input[name="pfCost"]').val(data.pfCost);
                $('#add-form input[name="pfWeight"]').val(data.pfWeight);
                redio($('#add-form input[name="pfWeigtUnit"]'),data.pfWeigtUnit);
                $('#add-form input[name="pfMax"]').val(data.pfMax);
                $('#add-form input[name="pfMin"]').val(data.pfMin);
                $('#add-form textarea[name="pfOther"]').val(data.pfOther);
                $('#add-form input[name="pfImf"]').val(data.pfImf);
                $('#add-form input[name="pfOther"]').val(data.pfOther);
                $('#add-form #p-img').attr("src",data.pfImf);
            },function (msg) {
                swal("加载失败",msg,"error");
                $('#add').modal('hide')
            },"get",{})
        }
    }
    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error(result.msg)
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                }
            }
        })
    }
    function sumitForm() {
        //提交表单数据
        $('#add-submit,#add-btns').click(function () {
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                $("#add-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            }
        })
    }
    function fuzhi(result) {
        $('img[id="cotsPhoto"]').css("display","block");
        $('#p-img').prop("src","/img/"+result);
        $('input[name="pfImf"]').val("/img/"+result);
        $("#upload").css("display","none");
    }
</script>

</body>

</html>