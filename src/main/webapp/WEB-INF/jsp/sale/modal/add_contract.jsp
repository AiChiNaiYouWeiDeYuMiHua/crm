<%--
  添加合同
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
</head>

<body >

                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-default pull-right" id="add-btns">
                        <i class="fa fa-check"></i>
                        保存
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        合同
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="add-form" action="/sale/order/contract/save" method="post" class="form-horizontal">
                        <div class="mybody">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">
                                            <span style="color: #ff0000; font-size: 16px;"></span>主题：
                                        </label>
                                        <div class="col-md-10">
                                            <input name="orderTitle" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2" >
                                            <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                                        </label>
                                        <div class="col-md-10 input-group" style="padding-left: 15px;padding-right: 15px">
                                            <input class="form-control hidden" name="tbCustomerByCusId.cusId"  required/>
                                            <div class="form-control" id="cusName" disabled></div>
                                            <span class="input-group-btn">
                                                <button onclick="chooseCus()" class="btn btn-default">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">对应机会：</label>
                                        <div class="col-md-10">
                                            <select id="modal-quote" data-width="100%" name="tbQuoteByQuoteId.quoteId" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>合同/订单号：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="orderNumber" disabled class="form-control"   />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">分类：</label>
                                        <div class="col-md-8">
                                            <select id="modal-category" data-width="100%" name="orderCategory" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                            </select>
                                            <a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>总金额：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="orderTotal" class="form-control"   />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">付款方式：</label>
                                        <div class="col-md-8">
                                            <select id="modal-payway"  data-width="100%" name="orderPayWay" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                            </select>
                                            <a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>开始时间：
                                        </label>
                                        <div class="col-md-8">
                                            <input  type="date" id="orderDate" name="orderDate" class="form-control"    />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>结束时间：
                                        </label>
                                        <div class="col-md-8">
                                            <input  type="date" name="orderLatestDate" class="form-control"    />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>客户签约人：
                                        </label>
                                        <div class="col-md-8">
                                            <select   class="selectpicker" data-width="100%" id="cots" name="contactsEntity.cotsId"  >
                                                <option selected></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>我方签约人：
                                        </label>
                                        <div class="col-md-8 input-group" style="padding-left: 15px">
                                            <input   class="form-control hidden" name="tbUserByTbUserId.userId"/>
                                            <input class="form-control" id="userUserName">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        style="color: black;height: 34px" onclick="chooseUser()"><i
                                                        class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>所有者：
                                        </label>
                                        <div class="col-md-8">
                                            <input   name="tbUserByUserId.userId" class="form-control hidden" disabled value="${user.userId} "  />
                                            <input class="form-control" id="userName" required value="${user.userName}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>签约时间：
                                        </label>
                                        <div class="col-md-8">
                                            <input  type="date" name="orderNewDate" class="form-control" required   />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>回款金额：
                                        </label>
                                        <div class="col-md-8">
                                            <input  value="0.00" name="returnMoney" class="form-control disabled"   disabled  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;"></span>毛利：
                                        </label>
                                        <div class="col-md-8">
                                            <input  value="0.00" name="maori" class="form-control disabled"  disabled   />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>状态：
                                        </label>
                                        <div class="col-md-8">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="0" name="orderStatus" checked>
                                                <label> 执行中 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="1" name="orderStatus" >
                                                <label> 结束 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="2" name="orderStatus" >
                                                <label> 意外终止 </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">结款方式：</label>
                                        <div class="col-md-8">
                                            <select id="modal-knot" data-width="100%" name="orderKnotWay" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                            </select>
                                            <a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="row" style="margin-bottom: 10px;">
                            <div class="form-group overflow">
                                <label class="control-label col-md-2">项目：</label>
                                <div class="col-md-10">
                                    <select id="modal-project" data-width="100%" name="projectEntity.projId" class="selectpicker" data-live-search="true" data-live-search="true">
                                        <option selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-2">备注：</label>
                                    <div class="col-md-10">
                                            <textarea name="orderOther" class="form-control" ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-2"><b>审批状态：</b></label>
                                    <div class="col-md-10">
                                        <span  style="display: inline-block;padding: 6px 12px" id="orderOk">待审
                                            <i class="fa fa-coffee f-s-16 m-l-5" data-placement="bottom" data-toggle="tooltip" title="待审"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                       <label class="control-label col-md-2">
                                           <button class="btn" id="file-btn">上传附件</button>
                                       </label>
                                    <div class="col-md-10">
                                    </div>
                                </div>
                            </div>
                            <div style="height: 50px; "></div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default" type="submit" id="add-submit"><i class="fa fa-check"></i>
                    保存</button>
            </div>


<script>
    sumitForm();
    $(function() {
        setParameter();
        validator()

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();
    });
    //获取参数
    function setParameter() {
        getParameter($('#modal-category'),3);
        getParameter($('#modal-payway'),5);
        getParameter($('#modal-knot'),6);

    }
    //获取参数
    function getParameter(dom,id) {
        return ajax("/get_dimission",success = function (data) {
            for (var i =0; i<data.length;i++){
                dom.append('<option>'+data[i].dimissionName+'</option>')
            }
            dom.selectpicker('refresh');
            dom.selectpicker('render');
            if (id == 3)
                setForm();
        },function () {},"post",{typeId:id});
    }
    function setForm() {
        if (open_modal != null) {
            ajax("/sale/order/load?id="+open_modal,function (data) {
                $('#add-form').append("<input class='hidden' name='orderId' value='" + data.orderId + "'>");
                $('#add-form input[name="orderTitle"]').val(data.orderTitle);
                getCus(data.tbCustomerByCusId.cusId,data.tbCustomerByCusId.cusName)
                if (data.tbQuoteByQuoteId != null){
                    $('#add-form #modal-quote').selectpicker("val",data.tbQuoteByQuoteId.quoteId);
                }
                $('#add-form #modal-category').selectpicker("val",data.orderCategory);
                $('#add-form input[name="orderNumber"]').val(data.orderNumber);
                $('#add-form input[name="orderTotal"]').val(data.orderTotal);
                $('#add-form #modal-payway').selectpicker("val",data.orderPayWay);
                $('#add-form input[name="orderDate"]').val(data.orderDate);
                $('#add-form input[name="orderLatestDate"]').val(data.orderLatestDate);
                $('#add-form input[name="orderNewDate"]').val(data.orderNewDate);
                $('#add-form #modal-knot').selectpicker("val",data.orderKnotWay);
                if (data.contactsEntity != null) {
                    $('#add-form input[name="contactsEntity.cotsId"]').val(data.contactsEntity.cotsId);
                    $('#add-form #cotsName').val(data.contactsEntity.cotsName);
                }
                if (data.tbUserByTbUserId != null) {
                    $('#add-form #userUserName').val(data.tbUserByTbUserId.userName);
                    $('#add-form input[name="tbUserByTbUserId.userId"]').val(data.tbUserByTbUserId.userId);
                }
                $('#add-form #userName').val(data.tbUserByUserId.userName);
                $('#add-form input[name="tbUserByUserId.userId"]').val(data.tbUserByUserId.userId);
                $('#add-form input[name="returnMoney"]').val("￥"+data.returnMoney);
                $('#add-form input[name="maori"]').val("￥"+data.maori);
                $('#add-form input[name="orderStatus"]').eq(data.orderStatus).prop("checked","checked");
                if (data.productEntity != null)
                    $('#add-form #modal-category').selectpicker('val',data.productEntity.pcId);
                $('#add-form input[name="orderOther"]').val(data.orderOther);
                var ok;
                if (data.orderOk == 1){
                    ok = "待审<i data-toggle='tooltip' data-placement='bottom' title='待审' class='f-s-16 fa fa-coffee m-l-5'></i>"
                }else if (data.orderOk == 2){
                    ok = "同意<i data-toggle='tooltip' data-placement='bottom' title='同意' class='f-s-16 fa fa-check-circle-o m-l-5'></i>"
                }else if (data.orderOk == 3){
                    ok = "否决<i data-toggle='tooltip' data-placement='bottom' title='否决' class='f-s-16 fa fa-times-circle m-l-5'></i>"
                }else{
                    ok = "待申请<i data-toggle='tooltip' data-placement='bottom' title='待申请' class='f-s-16 fa fa-flickr m-l-5'></i>"
                }
                $('#add-form #orderOk').html(ok)
            },function (msg) {
                swal("加载失败",msg,"error");
                $('#add').modal('hide')
            },"get",{})
        }else {
            $('#file-btn').click(function () {
                swal('', '请保存该数据后，再上传附件 ', 'info')
            });
            defaultTime();
        }
    }

    function sumitForm() {
        //提交表单数据
        $('#add-submit,#add-btns').click(function () {
            $("#add-form").bootstrapValidator('validate');//提交验证
            if ($("#add-form").data('bootstrapValidator').isValid()) {//获取验证结果，如果成功，执行下面代码
                $("#add-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            }
        })
    }
    //设置默认时间
    function defaultTime() {
        var time = new Date();
        var day = ("0" + time.getDate()).slice(-2);
        var month = ("0" + (time.getMonth() + 1)).slice(-2);
        var today = time.getFullYear() + "-" + (month) + "-" + (day);

        $('input[name="orderDate"],input[name="orderLatestDate"],input[name="orderNewDate"]').val(today)
    }
    function chooseCus() {
        window.open("/customer_check");
    }

    function getCus(cusId,cusName) {
        var cus = '<span style="font-weight:normal;color:#9e9e9e">' +
            '〖</span>'+cusName+
            '<a href="/customer_detail?id='+cusId+
            '"> <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>'
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#cusName").html(cus);
        $.ajax({
            url: '/memorial/get_contact?id=' + $("input[name='tbCustomerByCusId.cusId']").val(),
            method: 'get',
            async: true,
            success: function (data1) {
                $("#cots,#contact-read").empty();
                $("#cots,#contact-read").append("<option selected></option>")
                for (var i = 0; i < data1.length; i++) {
                    $('#cots,#contact-read').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                    $('#cots,#contact-read').selectpicker('refresh');
                }
                $('#cots,#contact-read').selectpicker('refresh');
            }
        });

        $.ajax({
            type : 'post',
            url : "/getOppByCusId?cusId="+$("input[name='tbCustomerByCusId.cusId']").val(),
            dataType : 'json',
            success : function(datas) {//返回list数据并循环获取
                var select = $("#modal-quote");
                $("#modal-quote").empty();
                select.append("<option selected></option>");
                for (var i = 0; i < datas.length; i++) {
                    select.append("<option value='"+datas[i].oppId+"'>"
                        + datas[i].oppTheme + "</option>");
                }
                // $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });
    }
    function chooseUser() {
        // window.open("/admin/to_user_check");
        var a = $("<a href='/admin/to_user_check' target='_blank'>Apple</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent( 'click', true, true );
        a.dispatchEvent(e);
    }

    function getUser(userId, userName) {
        $("input[name='tbUserByTbUserId.userId']").val(userId);
        $("#userUserName").val(userName);
    }
</script>

</body>

</html>