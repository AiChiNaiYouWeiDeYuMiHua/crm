<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-30
  Time: 下午5:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="modal fade" id="cancel" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #ebeff2">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;            </button>
                <h4 class="modal-title">撤销审批</h4>
            </div>
            <div class="modal-body" style="font-size: 12px;height: auto">
                <form method="post" action="/approval/approval-ok" id="approval-form-cancel">
                <b>说明:</b>
                <p>等待审批时，允许撤销审批申请，撤消后：可修改合同订单数据，重新申请审批。</p>
                <input type="hidden" name="tbApprovalByApprovalId.approvalOrderId" value="${id}">
                <input type="hidden" name="tbApprovalByApprovalId.approvalType" value="${type}">
                </form>
                <div class="text-center form-group">
                    <button class="btn" style="margin-top: 20px" onclick="sumitApproval(-1,$('#approval-form-cancel'))">确定并且同意</button>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
