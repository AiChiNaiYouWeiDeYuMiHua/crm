<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-30
  Time: 下午5:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="modal fade" id="unlock" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #ebeff2">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;            </button>
                <h4 class="modal-title">审批解锁</h4>
            </div>
            <div class="modal-body" style="font-size: 12px;height: auto">
                <form method="post" action="/approval/approval-ok" id="approval-form-unlock">
                <b>说明:</b>
                <p>用于解决已经审批同意后，需修改原始合同订单数据。<br>
                    解锁后允许修改合同订单数据，并重新申请审批。<br>
                    Boss和当前级别审批人可解锁。</p>
                <input type="hidden" name="tbApprovalByApprovalId.approvalOrderId" value="${id}">
                <input type="hidden" name="tbApprovalByApprovalId.approvalType" value="${type}">
                </form>
                <div class="text-center form-group">
                    <button class="btn" style="margin-top: 20px" onclick="sumitApproval(4,$('#approval-form-unlock'))">确定解锁</button>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
