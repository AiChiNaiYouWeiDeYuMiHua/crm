<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-30
  Time: 下午2:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="modal fade" id="boss" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #ebeff2">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;            </button>
                <h4 class="modal-title" id="myModalLabel">Boss创建的订单，可直接同意</h4>
            </div>
            <div class="modal-body" style="font-size: 12px;height: auto">
                <form method="post" action="/approval/approval-ok" id="approval-form-boss">
                <b>说明:</b>
                <p>由Boss用户（必须有账户管理权限的Boss）创建的合同订单，可以由Boss直接审批同意。</p>
                <div class="form-group">
                    <label class="col-md-2 control-label"> 审批留言：</label>
                    <div class="col-md-10">
                        <textarea name="logsContent" style="resize: vertical" class="form-control"></textarea>
                        <input type="hidden" name="tbApprovalByApprovalId.approvalOrderId" value="${id}">
                        <input type="hidden" name="tbApprovalByApprovalId.approvalType" value="${type}">
                    </div>
                </div>
                </form>
                <div class="text-center form-group">
                    <button class="btn" style="margin-top: 20px" onclick="sumitApproval(2,$('#approval-form-boss'))">确定并且同意</button>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
