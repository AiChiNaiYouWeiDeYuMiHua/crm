<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-30
  Time: 下午5:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="modal fade" id="enter" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #ebeff2">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;            </button>
                <h4 class="modal-title">审批同意</h4>
            </div>
            <div class="modal-body" style="font-size: 12px;height: auto">
                <form method="post" action="/approval/approval-ok" id="approval-form-ok">
                <b>说明:</b>
                <p>审批同意后，该合同订单数据将被锁定（保护审批内容不被意外变更），申请人只能编辑合同订单状态字段。如此时必须修改该合同订单，需
                    先使用解锁操作。</p>
                <div class="form-group">
                    <label class="col-md-2 control-label"> 审批留言：</label>
                    <div class="col-md-10">
                        <textarea name="logsContent" style="resize: vertical" class="form-control"></textarea>
                        <input type="hidden" name="tbApprovalByApprovalId.approvalOrderId" value="${id}">
                        <input type="hidden" name="tbApprovalByApprovalId.approvalType" value="${type}">
                    </div>
                </div>
                </form>
                <div class="text-center form-group">
                    <button class="btn" style="margin-top: 20px" onclick="sumitApproval(2,$('#approval-form-ok'))">确定并且同意</button>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
