<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="modal-header" style="border-bottom: none;">
    <h4 class="modal-title" id="myModalLabel">
        高级查询
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="search-form" >
        <div class="mybody">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            主题:
                        </label>
                        <div class="col-md-7">
                            <input name="rgTheme" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            退货单号:
                        </label>
                        <div class="col-md-7">
                            <input name="rgOddNumbers" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-3 ">
                            退货时间:
                        </label>
                        <div class="col-md-4" style="padding-right: 0">
                            <div id="datetime_1" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="开始日期" name="from"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
                                            </span>
                            </div>

                        </div>

                        <div class="col-md-4" style="padding-left: 0">
                            <div id="datetime_2" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="结束日期" name="to"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-3">
                            供应商(客户)：
                        </label>
                        <div class="col-md-7">
                            <input  class="form-control" required="required"
                                    style="background-color: white;" name="cusId"/>
                            <div class="" style="margin-top: 10px; margin-bottom: 10px;">
                                <div class="form-group">
                                    <input class="form-control" placeholder="关键字"
                                           style="padding-right: 12px; width: 120px; float: left; margin-right: 5px;"/>

                                    <button class="btn btn-default" style="float: left; margin-right: 5px;">
                                        <i class="fa fa-search"></i></button>
                                    <a class="btn btn-default" data-toggle="tooltip" data-placement="bottom"
                                       title="浏览">
                                        <i class="fa fa-list"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-3">
                            负责人：
                        </label>
                        <div class="col-md-7">
                            <input  class="form-control" required="required"
                                    style="background-color: white;" name="userId"/>
                            <div class="" style="margin-top: 10px; margin-bottom: 10px;">
                                <div class="form-group">
                                    <input class="form-control" placeholder="关键字"
                                           style="padding-right: 12px; width: 120px; float: left; margin-right: 5px;"/>

                                    <button class="btn btn-default" style="float: left; margin-right: 5px;">
                                        <i class="fa fa-search"></i></button>
                                    <a class="btn btn-default" data-toggle="tooltip" data-placement="bottom"
                                       title="浏览">
                                        <i class="fa fa-list"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 30px;">
                <div class="col-md-12">
                    <div class="form-group overflow">
                        <label class="control-label col-md-3">采购单:</label>
                        <div class="col-md-7">
                            <div class="input-group" style="margin-right: 10px"><input type="hidden"
                                                                                       name="tbPurchaseByPurId.purId"
                                                                                       value="">
                                <input placeholder="点击右侧放大镜选择" class="form-control" type="text" readonly="true"
                                       name="dt_puritem_pu_id_a" id="pur" value="">
                                <input type="hidden" name="purId" id="pur1">
                                <span class="input-group-btn">
                      <button type="button" class="btn waves-effect waves-light btn-default" id="btn2"
                              onclick="change1(this)">
                          <i class="fa fa-search"></i>
                      </button>
                  </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-3">状态：</label>
                        <div class="col-md-7">
                            <div
                                 style="position: static; visibility: visible; top: 0px;">
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="待处理" name="stateList"><label>待处理</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="执行中" name="stateList"><label>执行中</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="结束" name="stateList"><label>结束</label>
                                </div>
                                <div class="checkbox checkbox-inline"><input type="checkbox"
                                                                             value="终止" name="stateList"><label>终止</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" id="reset-btns" style="margin-left: 5px">
                    <i class="fa fa-recycle"></i>
                    重置
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default"onclick="search_btn()" ><i
                        class="fa fa-check" ></i>
                    查询
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    $(function () {
        $('.selectpicker').selectpicker("refresh");
        $("#datetime_1").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_2").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });
        $("#datetime_3").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_4").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });
        $("#datetime_5").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_6").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });
        $('#reset-btns').click(function () {
            $('#search-form')[0].reset();
            $('.selectpicker1').selectpicker('val', '未选');
        })

        $(".selectpicker1").selectpicker({
            noneSelectedText: '未选'
        });

        $("[data-toggle='tooltip' ] ").tooltip();

    });
    function search_btn(){
        search1();
        $('#wsj-search').modal("hide")
        $('.selectpicker1').selectpicker('val', '未选');
    }
</script>


</body>
</html>