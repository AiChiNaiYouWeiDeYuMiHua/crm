<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-14
  Time: 下午3:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>选择仓库生成出库单</title>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <!-- [if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![end if]-->
    <style>
        html,
        body {
            color: #333333;
            background-color: #F4F8FB;
            font-size: 12px;
            font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        }
    </style>
</head>
<body>

<div class="container-fluid" style="margin-top: 20px">
      <h4>
          <b>选择仓库出库</b>
      </h4>
        <h5><b>1.核对库存数量</b>（<b class="text-success">绿</b>：库存<b>&gt;</b>出库需求；<b class="text-danger">红</b>：库存<b>&lt;</b>出库需求）</h5>
        <div>
            <table id="table"></table>
        </div>
        <h5><b>2.选择出库仓库</b></h5>
        <form method="POST" action="/sale/warehouse/order/out" id="save-form"  class="form-horizontal">
            <div class="form-group overflow">
                <label class="col-xs-1 control-label">选择仓库：</label>
                <div class="col-xs-2">
                    <select name="warehouseName" id="warehouseName" class="selectpicker">
                    </select>
                    <input type="hidden" name="id" value="${id}">
                </div>
            </div>

            <p class="text-center">
                <input  value="下一步：生成出库单" name="save" id="save" class="btn btn-primary"></p>
        </form>
</div>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<!-- Latest compiled and minified Locales -->
<script src="/js/sweetalert.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script>
    $(function () {
        getParameter($('#warehouseName'))
        sumitForm();
        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        $('#modal').load('/sale/chooseProduct')
    })
    function initTable(columnsArray) {
        $("#table").bootstrapTable({
            method: 'get',
            striped: true,
            showExport:true,
            cache: false,
            sortable: false,
            sortOrder: "desc",
            url: "/sale/warehouse/order/nopay?id=${id}",
            exportOptions:{
                fileName:$('#table-title').text(),
                ignoreColumn:[7]
            },
            columns:columnsArray,
            onLoadSuccess:function (data) {
            },
            onLoadError:function (status) {

            }
        })
    }
    function sumitForm() {
        //提交表单数据
        $('#save').click(function () {
                $("#save-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            window.location.href = "/sale/warehouse/out/info/"+result.data+"/"
                        }
                        else{
                            swal("生成出库单失败",result.msg,"error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            })
    }
    function successForm(){
        window.close();
        window.opener.history.go(0);
    }
    function getParameter(dom) {
        return ajax("/sale/warehouse/wh",success = function (data) {
            var columns =  [{
                field:'title',
                title:'产品',
                align:'center',
                valign: 'middle',
            },{
                field:'noPay',
                title:'需求数量',
                align:'center',
                valign: 'middle',
            }];
            for (var i =0; i<data.length;i++){
                dom.append('<option value="'+data[i].whId+'">'+data[i].whName+'</option>')
                columns.push({
                    title: data[i].whName,
                    align: 'center',
                    valign: 'middle',
                    formatter:function (value,row,index,col) {
                        var json = row.wareHouses[col-2];
                        var re = 0;
                        if (json != null)
                            re = json.number;
                        if (re >= row.noPay)
                            return '<span class="text-success">'+re+'</span>'
                        else
                            return '<span class="text-danger">'+re+'</span>'
                        return
                    }
                })
            }
            dom.selectpicker('refresh');
            dom.selectpicker('render');
            initTable(columns);
        },function () {},"post",{});
    }
    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                    error()
                }
            }
        })
    }
</script>
</body>
</html>
