<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="simple" uri="http://went.top/tags" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<!DOCTYPE html>
<html>

<head>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base target="_blank">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>采购单详情</title>
    <!-- Bootstrap -->
    <meta charset="UTF-8">
    <title>付款计划</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <%--<link rel="stylesheet" href="/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.css"/>--%>
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>

    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="/css/sale_start_modal.css"/>
</head>
<style type="text/css">
    html,
    body {
        color: #333333;
        background-color: #F4F8FB;
        font-size: 12px;
        font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }

    .table-t thead {
        background-color: #EBEFF2;
        font-family: 楷体;
    }
    .icon-box{
        display: inline-block;
        min-width: 100px;
    }
    .border-r{
        border-right: 1px solid #ccc;
    }
    a:hover{text-decoration: none}
    .panel-title{
        font-size: 12px;
    }
    .fixed-table-body{
        height: auto;
    }
    .m-l-5{
        margin-left: 5px;
    }
    .m-b-10{
        margin-bottom: 10px;
    }
    .m-r-5{
        margin-right: 5px;
    }
    .m-b-10{
        margin-bottom: 10px;
    }
    .m-b-30{
        margin-bottom: 30px;
    }
    a{
        color: #505458;
    }
    .text-gray{
        color: #999;
    }
    .m-20{
        margin: 20px;
    }
</style>

<body>
<div class="container-fluid" style="background-color: #F4F8FB;">
    <div class="row">
        <div class="col-md-12" style="font-size: 12px ">
            <simple:approval entity="${info.purId}" status="${info.purOk}" type="采购单"></simple:approval>
        </div>
    </div>
    <div style="margin: 10px;background-color: white;padding: 10px;">

        <div class="row">
            <div class="col-md-12">
                <h4 class="modal-title text-dark">
                    <div class="col-md-8">
                        <span set-lan="html:采购单">采购单</span>
                    </div>
                </h4>
                <div id="list" class="pull-right col-md-4" style="font-size: 12px;display: none">
                    <g:g id="182">
                    <button class="btn btn-default" style="font-size: 12px;" data-toggle="modal"
                            data-target="#delModal" onclick="deletePro1('${info.purId}')">
                        <i class="fa fa-trash-o m-r-5" style="margin-right: 5px;"></i>删除
                    </button>&nbsp;
                    </g:g>
                    <g:g id="183">
                    <button class="btn btn-default" style="font-size: 12px;" onclick="edit('${info.purId}')">
                        <i class="fa fa-pencil m-r-5" style="margin-right: 5px;"></i>编辑
                    </button>&nbsp;
                    </g:g>
                    <g:g id="234">
                    <button id="pd1" class="btn btn-default" style="font-size: 12px;display: none" onclick="edit1()">
                        <i class="fa fa-pencil m-r-5" style="margin-right: 5px;"></i>编辑采购明细
                    </button>&nbsp;
                    </g:g>
                    <g:g id="291">
                    <button id="rk1" class="btn btn-default" style="font-size: 12px;display: none" onclick="ruku()">
                        <i class="fa fa-level-down m-r-5" style="margin-right: 5px;"></i>生成入库单
                    </button>&nbsp;
                    </g:g>
                </div>
            </div>
        </div>
        <div id="img"
             style="position:absolute;right:0;top:147px;background-position:top right;display:none;background-repeat:no-repeat;width:300px;height:200px;">
            <img src="/img/returnGoods.png" alt=""/></div>
        <div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2"
                               style="font-size: 12px;font-weight: normal;text-align: right;">采购主题：</label>
                        <div class="col-md-10">${info.purTheme}&nbsp;</div>
                        <input type="hidden" id="state" value="${info.purState}">
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2"
                               style="font-size: 12px;font-weight: normal;text-align: right;">供应商（客户）：</label>
                        <div class=" col-md-10"><span
                                style="font-weight:normal;color:#9e9e9e">〖</span>${info.tbCustomerByCusId.cusName}&nbsp;
                            <a href="/customer_detail?id="+${info.tbCustomerByCusId.cusId} target="_blank"><i
                                    class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom"
                                    data-original-title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>
                        </div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">采购日期：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.purDate}&nbsp;</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">采购单号：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.purOddNumbers}&nbsp;</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">仓库：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.tbWarehouseByWhId.whName}&nbsp;</p>
                    </div>
                    <label class="col-md-2"
                           style="font-size: 12px;font-weight: normal;text-align: right;">预计到货日期：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.purProjectedDate}&nbsp;</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">经手人：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.tbUserByUserId.userName}&nbsp;</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">状态:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.purState}&nbsp;</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">审批状态:</label>
                        <div class=" col-md-10"><c:choose>
                            <c:when test="${info.purOk == 1}">
                                待审<i data-toggle='tooltip' data-placement='bottom' title='待审' class='f-s-16 fa fa-coffee m-l-5'></i>
                            </c:when>
                            <c:when test="${info.purOk == 2}">
                                同意<i data-toggle='tooltip' data-placement='bottom' title='同意' class='f-s-16 fa fa-check-circle-o m-l-5'></i>
                            </c:when>
                            <c:when test="${info.purOk == 3} ">
                                否决<i data-toggle='tooltip' data-placement='bottom' title='否决' class='f-s-16 fa fa-times-circle m-l-5'></i>
                            </c:when>
                            <c:otherwise>
                                待申请<i data-toggle='tooltip' data-placement='bottom' title='待申请' class='f-s-16 fa fa-flickr m-l-5'></i>
                            </c:otherwise>
                        </c:choose></div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">审批记录:</label>
                        <div class=" col-md-10">
                            <div class="row">
                                <c:forEach items="${approvals}" var="approval" varStatus="status">
                                    <div class="col-md-2">
                                        第${status.count}次:
                                    </div>
                                    <div class="col-md-10">
                                        <c:forEach items="${approval.tbApprovalLogsByApprovalId}" var="log">
                                            <c:if test="${log.tbUserByUserId !=null}">
                                                <span>${log.tbUserByUserId.userName}</span></c:if>
                                            &nbsp;&nbsp;
                                            <span>${log.category}</span>
                                            &nbsp;&nbsp;<span>${log.logsContent}</span>&nbsp;&nbsp<span>${log.logsTime}</span>
                                            <br>
                                        </c:forEach>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2"
                               style="font-size: 12px;font-weight: normal;text-align: right;">采购物品明细:</label>
                    </div>
                </div>
                <g:g id="141">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-12" style="font-size: 12px;font-weight: normal;text-align: right;">
                            <a id="returnGoods" onclick="returnGoods1()"
                               style="FONT-WEIGHT: normal; COLOR: #c69; TEXT-DECORATION: none;display: none"
                               style="text-decoration: none;"><i class="fa fa-arrow-circle-right"></i>办理退货</a>
                        </label>
                    </div>
                </div>
                </g:g>
                <div id="pdTable" class="col-md-12" style="padding-left: 80px;">
                    <div id="ta" class="form-group">
                        <table id="table33">
                        </table>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 80px;">
                    <div class="form-group">
                        <div class="col-md-5"></div>
                        <div class="col-md-1" style="margin-left: 65px">
                           <span class="text-left "
                                 style="cursor: pointer; padding: 8px 12px; border-radius:3px; background: #dddddd;margin:10px 0px; ">
					<i class="md  md-attach-file"></i><span set-lan="html:上传附件">上传附件</span>
				</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    付款计划(应付)
    <c:forEach items="${returnGoodsVOS}" var="returnGoodsVOS1">
        <div class="row" style="background-color: white;padding: 10px;margin: 10px;" id="return">
            <div class="col-md-12">
                <div class="form-group">
                    ￥退货单明细/退货单
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <span class="col-md-12" style="text-align: center;font-size: 12px;margin-bottom: 5px;">退 货 单</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                        <span>应退款:</span><span style="color: darkgray;">￥${returnGoodsVOS1.planMoney}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span>已退款:</span><span style="color: darkgray;">￥${returnGoodsVOS1.toMoney}</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                        <%--<span>退货:</span><span style="color: darkgray;">${returnGoodsVOS1.putState}</span> &nbsp;&nbsp;&nbsp;&nbsp;--%>
                        <span>单据状态:</span><span style="color: darkgray;"
                                                id="${returnGoodsVOS1.rgId}1">${returnGoodsVOS1.rgState}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                        <span>主题：</span><span style="color: darkgray;">${returnGoodsVOS1.rgTheme}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span>退货单号：</span><span style="color: darkgray;">${returnGoodsVOS1.rgOddNumbers}</span></label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                        <span>经办人：</span><span style="color: darkgray;">${returnGoodsVOS1.userName}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span>仓库：</span><span style="color: darkgray;">${returnGoodsVOS1.whName}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span>日期：</span><span style="color: darkgray;"><fmt:formatDate
                            value="${returnGoodsVOS1.rgReturnTime}" pattern="yyyy-MM-dd"/></span>
                    </div>
                </div>
                <g:g id="142">
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                        <span>操作：</span>
                        <button type="button" class="btn" id="${returnGoodsVOS1.rgId}2">撤销退货</button> &nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                    <br/><br/><br/>
                </div>
                </g:g>
                <br/><br/><br/>
                <div class="form-group">
                    <div class="col-md-12">
                        <table id="table3" class="table-t">

                        </table>
                    </div>
                </div>
            </div>
            <div id="${returnGoodsVOS1.rgId}"></div>
        </div>
    </c:forEach>
    <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
        <div class="col-md-12">
            <div>
                <a class="text-a btn btn-default" <g:g id="201">id="add_btn1"</g:g> style="margin-left: 10px;">
                    <i class="fa fa-plus" data-toggle="tooltip" data-placement="bottom" title=""
                       data-original-title="新建"></i>&nbsp;新建付款计划
                </a>
            </div>
            <div id="model1">
            </div>
            <div style="margin-top: 10px">
                <c:forEach items="${planPayDetailEntityPage}" var="planPayDetailEntityPage">
                    <div>
                        付款金额：&nbsp;￥</span>${planPayDetailEntityPage.ppdMoney}</span>&nbsp;&nbsp;
                        期次：&nbsp;第${planPayDetailEntityPage.ppdTerms}期&nbsp;&nbsp;
                        计划付款日期:&nbsp;${planPayDetailEntityPage.ppdDate}&nbsp;&nbsp;
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
        <div class="col-md-12" style="margin-bottom: 10px">
            <div>
                <a class="text-a btn btn-default" <g:g id="221">id="add_btn2"</g:g> style="margin-left: 10px;">
                    <i class="fa fa-plus" data-toggle="tooltip" data-placement="bottom" title=""
                       data-original-title="新建"></i>&nbsp;新建付款票据
                </a>
            </div>
            <div id="model2"></div>
            <div style="margin-top: 10px">
                <c:forEach items="${purchaseReceiptEntitylist}" var="purchaseReceiptEntity">
                    <div>
                        采购单主题：&nbsp;${info.purTheme}<span>&nbsp;&nbsp;
                        付款金额：&nbsp;￥</span>${purchaseReceiptEntity.purrMoney}</span>&nbsp;&nbsp;
                        期次：&nbsp;第${purchaseReceiptEntity.purrTerms}期&nbsp;&nbsp;
                        收票日期：&nbsp;<fmt:formatDate value="${purchaseReceiptEntity.purrDate}" type="date"/> &nbsp;<br>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <!--</div>-->
    <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
        <div class="col-md-12" style="margin-bottom: 10px">
            <div>
                <a class="text-a btn btn-default" <g:g id="161">id="add_btn3"</g:g> style="margin-left: 10px;">
                    <i class="fa fa-plus" data-toggle="tooltip" data-placement="bottom" title=""
                       data-original-title="新建"></i>&nbsp;新建付款记录
                </a>
            </div>
            <div id="model3"></div>
        </div>
        <div style="margin-top: 10px">
            <c:forEach items="${paymentRecordsEntities}" var="paymentRecords">
                <div>
                    &nbsp;&nbsp;&nbsp;&nbsp;采购单主题：&nbsp;${info.purTheme}<span>&nbsp;&nbsp;
                        付款金额：&nbsp;￥</span>${paymentRecords.prMoney}</span>&nbsp;&nbsp;
                    期次：&nbsp;第${paymentRecords.prTerms}期&nbsp;&nbsp;
                    付款日期：&nbsp;${paymentRecords.prDate}&nbsp;<br>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<input type="hidden" value=${info.purId} id="a">
<input type="hidden" value=${rgIds} id="rgIds">
<input type="hidden" id="suc">
<div class="modal fade" id="wsj-add" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">
        </div>
    </div>
</div>
<input type="hidden" id="purId" value="${info.purId}">
<input type="hidden" id="mark">
<div class="row" style="text-align: center;background-color: #F4F8FB;">
    <div class="col-md-12">
        <div class="form-group">
            <footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn"
                                                                                             target="_blank">www.xtools.cn</a></b>
                &nbsp;
                <a class="btn btn-danger btn-xs" href="#"
                   onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i
                        class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
                <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i
                        class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
                <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i
                        class="md md-speaker-notes m-r-5"></i>订阅号 </a>
                <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology
                Co.,ltd
            </footer>
        </div>
    </div>
</div>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="/js/jquery-3.3.1.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="text/javascript " src="/js/wsjJS/bootstrap-datetimepicker.min.js "></script>

<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="/js/wsjJS/MainPlanPayDetail.js"></script>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<script src="/js/resjs/printThis.js"></script>


<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>

<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="/js/select.js"></script>
<%--<script type="text/javascript " src="/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>--%>
<script type="text/javascript "
        src="/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
<script src="/js/sweetalert.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<script type="text/javascript">
    $(function () {
        if ($("#state").val() == "新采购"){
            $("#edit1").show();
            $("#ruku1").hide();
            $("#list").show();
        }else{
           $("#returnGoods").show();
        }
        var rgId = $("#rgIds").val();
        if (rgId == null || rgId == "" || rgId == undefined) {
            $("#return").hide();
        } else {
            var result = new Array();
            result = rgId.split(",");
            for (var i = 0; i < result.length; i++) {
                var a = "wsjTable" + i;
                var b = result[i];
                var c = "#" + result[i];
                var d = "#" + result[i] + "1";
                var e = "#" + result[i] + "2";
                $(c).append("<div class=\"form-group\">\n" +
                    "                <div class=\"col-md-12\">\n" +
                    "                    <table id=" + a + " class=\"table-t\">\n" +
                    "\n" +
                    "                    </table>\n" +
                    "\n" +
                    "                </div>\n" +
                    "            </div>");
                var f="chuku("+result[i]+")";
                var v="/sale/addPrModel1?rgId="+result[i];
                var w="returnMoney("+result[i]+")";
                if ($(d).html() == "待处理") {
                    $("#mark").val("待处理");
                    $(c).append("<g:g id='291'><div class=\"text-center m-b-10 \" id=\"outGoods\"><a style=\"font-size: 14px;font-weight: 400;margin-top: 20px;margin-bottom: 20px\" class=\"btn btn-primary\" onclick="+f+" >\n" +
                        "            <i class=\"fa fa-home m-r-5\" style=\"padding-bottom: 5px;padding-top: 5px\"></i>出库:生成退货出库单</a></g:g>" +
                        "<g:g id='161'><a style=\"font-size: 14px;font-weight: 400;margin-top: 20px;margin-bottom: 20px;margin-left:20px\" class=\"btn btn-primary\" onclick="+w+" >" +
                        "          <i class=\"fa fa-home m-r-5\" style=\"padding-bottom: 5px;padding-top: 5px\"></i>新建退款</a>\n" +
                        "            </div></g:g>")
                } else {
                }


                initTable1(a, result[i]);
                $(e).click(function () {
                    swal({
                            title: "撤销退货",
                            text: "确定要撤销退货吗？",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        },
                        function () {
                            var url = "/sale/rgdelete?id=" + b;
                            ajax(url, function (data) {
                                var b = " /sale/purdetail1/" + $("#purId").val() + "/";
                                window.location.href = b;

                            }, function (msg) {
                                $('#wsj-add').modal('hide')
                            }, "get", {})
                        });
                })
            }
        }
        ajax("/sale/purProDetail111?id=${info.purId}", function (data) {
            if (data == 1) {
                $("#returnGoods").show();
            }
            if (data == 1 || data == 2) {
                initTable();
                if ($("#state").val() == "新采购"){
                    $("#pdTable").append(" <div class=\"text-center m-b-10 \" id=\"outGoods\"><a style=\"font-size: 14px;font-weight: 400;margin-top: 20px;margin-bottom: 20px\" class=\"btn btn-primary\" onclick=\"ruku()\" >\n" +
                        "            <i class=\"fa fa-home m-r-5\" style=\"padding-bottom: 5px;padding-top: 5px\"></i>入库:点此生成入库单，由库管操作入库</a>\n" +
                        "            </div>");
                }
            } else {
                swal({
                        title: "编辑采购单明细",
                        text: "点击确认，新建采购单明细",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "确定",
                        closeOnConfirm: false
                    },
                    function () {
                        var b = "/sale/purProDetail/" + $("#purId").val() + "/";
                        window.open(b);
                    });
            }

        }, function (msg) {
            swal("加载失败", msg, "error");
            $('#wsj-add').modal('hide')
        }, "get", {})

        $("#add_btn1").click(function () {
            var a = $("#purId").val();
            var b = "/sale/add_modal?purId=" + a;
            $('#wsj-add').modal({
                remote: b
            })
        })

        $("#add_btn2").click(function () {
            var a = $("#purId").val();
            var b = "/sale/purrAddModal?purId=" + a;
            $('#wsj-add').modal({
                remote: b
            })
        })
        $("#add_btn3").click(function () {
            var a = $("#purId").val();
            var b = "/sale/addPrModel?purId=" + a;
            $('#wsj-add').modal({
                remote: b
            })
        })
        if ($("#return").html() == null || $("#return").html() == undefined) {
            $("#img").hide();
        } else {
            $("#img").show();
        }
    })
    $(".form_datetime").datetimepicker({
        format: "yyyy年mm月dd日 ",
        minView: "month", //设置只显示到月份
        startView: 2,
        showMeridian: 1,
        forceParse: 0,
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });

    //删除
    function deletePro1(purId) {
        swal({
                title: "确定删除吗？",
                text: "你将无法恢复该记录！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function () {
                ajax("/sale/purdelete", function () {
                    swal("成功！", "删除成功！", "success")
                    var b = "/sale/mainPurchase";
                    window.location.href = b;
                }, function () {
                }, 'post', {'purId': purId});
            });
    }

    var open_modal;
    var open_modal1;

    //打开编辑
    function edit(id) {
        open_modal = id;
        open_modal1 = id;
        $('#wsj-add').modal({
            remote: '/sale/purAddModal'
        })
    }
function returnMoney(rgId) {
    var a = $("#purId").val();
    var b = "/sale/addPrModel1?rgId=" + rgId;
    $('#wsj-add').modal({
        remote: b
    })
}
    //入库
    function ruku() {
        var a = $("#purId").val();
        var url = "/sale/warehouse/purse/in?id=" + a;
        ajax(url, function (data) {
            window.open("/sale/warehouse/in/info/" + data + "/");
        }, function (msg) {
        });
    }
    //出库
    function chuku(a) {
        var url = "/sale/warehouse/return/out?id=" + a;
        alert(url)
        ajax(url, function (data) {
            window.open("/sale/warehouse/out/info/" + data + "/");
        }, function (msg) {
        });
    }
    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            type: type,
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                    error()
                }
            }
        })
    }

    //办理退货
    function returnGoods1() {
        if ($("#mark").val() == "待处理") {
            swal(
                '请先处理待处理的退货单！'
            )
        } else {
            var b = "/sale/rgAddModal?purId=" +${info.purId};
            $('#wsj-add').modal({
                remote: b
            })
        }
    }

    function initTable() {
        var a = $("#a").val();
        $("#table33").bootstrapTable({
            method: 'post',
            url: "/sale/purProDetail1?id=" + a,
            // striped: true,
            // showFooter: true,
            queryParams: function (params) {
                //表单转json(去掉孔项目，节省流量
                var d = f($('#search-form'));
                var page = {};
                page["size"] = params.limit;
                page["page"] = (params.offset / params.limit) + 1;
                var sort = params.sort;
                if (sort != null && sort.length > 0) {
                    page["sortName"] = sort;
                    page["sortOrder"] = params.order
                }
                d["page"] = page;
                return d
            },
            pageList: [10, 25, 50, 100],
            pageNumber: 1,
            pageSize: 5,
            contentType: "application/json",
            strictSearch: false,
            minimumCountColumns: 2,
            detailView: false,
            cache: false,
            sortable: true,
            sortOrder: "asc",
            sortName: 'pdId',
            uniqueId: "pdId",
            columns: [{
                // field: 'id', // 返回json数据中的name
                title: '序号', // 表格表头显示文字
                align: 'center', // 左右居中
                width: 80,
                valign: 'middle', // 上下居中
                formatter: function (value, row, index) {
                    return tableIndexNum(index);
                },
                footerFormatter: function (value) {
                    return "合计";
                }
            }, {
                field: 'purTheme',
                title: '采购单',
                align: 'center',
                valign: 'middle',
                formatter: f1
                // formatter:moneyFormatter,
                // footerFormatter:sumFormatter
            }, {
                field: 'title',
                title: '产品名称',
                align: 'center',
                valign: 'middle',
                formatter: isNullFormatter
            }, {
                field: 'pdPrice',
                title: '单价',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: moneyFormatter,
                footerFormatter: sumFormatter
            }, {
                field: 'pdNum',
                title: '数量',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: moneyFormatter,
                footerFormatter: sumFormatter
            }, {
                field: 'pdWpNum',
                title: '入库数量',
                align: 'center',
                valign: 'middle',
                sortable: true,
                footerFormatter: sumFormatter
            }, {
                field: 'pdNoIn',
                title: '未入库数量',
                align: 'center',
                valign: 'middle',
                sortable: true,
                footerFormatter: sumFormatter
            }, {
                field: 'pdTotal',
                title: '金额',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: moneyFormatter,
                footerFormatter: sumFormatter
            }
            ],
            onLoadSuccess: function (data) { //加载成功时执行
                console.info("加载成功");
                $("#suc").val("123");
            },
            onLoadError: function () { //加载失败时执行
                console.info("加载数据失败");
            },

            //>>>>>>>>>>>>>>导出excel表格设置
            showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
            exportDataType: "basic", //basic', 'all', 'selected'.
            exportTypes: ['excel', 'xlsx'], //导出类型
            //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
            exportOptions: {
                ignoreColumn: [0, 0],            //忽略某一列的索引
                fileName: '数据导出', //文件名称设置
                worksheetName: 'Sheet1', //表格工作区名称
                tableName: '商品数据表',
                excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
                //onMsoNumberFormat: DoOnMsoNumberFormat
            }
            //导出excel表格设置<<<<<<<<<<<<<<<<
        });
        $(".table44").bootstrapTable({
            method: 'post',
            url: "/sale/purProDetail2?id=" + a,
            // striped: true,
            // showFooter: true,
            queryParams: function (params) {
                //表单转json(去掉孔项目，节省流量
                var d = f($('#search-form'));
                var page = {};
                page["size"] = params.limit;
                page["page"] = (params.offset / params.limit) + 1;
                var sort = params.sort;
                if (sort != null && sort.length > 0) {
                    page["sortName"] = sort;
                    page["sortOrder"] = params.order
                }
                d["page"] = page;
                return d
            },
            pageList: [10, 25, 50, 100],
            pageNumber: 1,
            pageSize: 5,
            contentType: "application/json",
            strictSearch: false,
            minimumCountColumns: 2,
            detailView: false,
            cache: false,
            sortable: true,
            sortOrder: "asc",
            sortName: 'pdId',
            uniqueId: "pdId",
            columns: [{
                // field: 'id', // 返回json数据中的name
                title: '序号', // 表格表头显示文字
                align: 'center', // 左右居中
                width: 80,
                valign: 'middle', // 上下居中
                formatter: function (value, row, index) {
                    return tableIndexNum(index);
                },
                footerFormatter: function (value) {
                    return "合计";
                }
            }, {
                field: 'purTheme',
                title: '采购单',
                align: 'center',
                valign: 'middle',
                formatter: f1
                // formatter:moneyFormatter,
                // footerFormatter:sumFormatter
            }, {
                field: 'title',
                title: '产品名称',
                align: 'center',
                valign: 'middle',
                formatter: isNullFormatter
            }, {
                field: 'pdPrice',
                title: '单价',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: moneyFormatter,
                footerFormatter: sumFormatter
            }, {
                field: 'pdNum',
                title: '数量',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: moneyFormatter,
                footerFormatter: sumFormatter
            }, {
                field: 'pdWpNum',
                title: '入库数量',
                align: 'center',
                valign: 'middle',
                sortable: true,
                footerFormatter: sumFormatter
            }, {
                field: 'pdNoIn',
                title: '未入库数量',
                align: 'center',
                valign: 'middle',
                sortable: true,
                footerFormatter: sumFormatter
            }, {
                field: 'pdTotal',
                title: '金额',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: moneyFormatter,
                footerFormatter: sumFormatter
            }
            ],
            onLoadSuccess: function (data) { //加载成功时执行
                console.info("加载成功");
            },
            onLoadError: function () { //加载失败时执行
                console.info("加载数据失败");
            },

            //>>>>>>>>>>>>>>导出excel表格设置
            showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
            exportDataType: "basic", //basic', 'all', 'selected'.
            exportTypes: ['excel', 'xlsx'], //导出类型
            //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
            exportOptions: {
                ignoreColumn: [0, 0],            //忽略某一列的索引
                fileName: '数据导出', //文件名称设置
                worksheetName: 'Sheet1', //表格工作区名称
                tableName: '商品数据表',
                excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
                //onMsoNumberFormat: DoOnMsoNumberFormat
            }
            //导出excel表格设置<<<<<<<<<<<<<<<<
        });
    }

    function initTable1(a, b) {
        var table = "#" + a;
        $(table).bootstrapTable({
            method: 'post',
            url: "/sale/rgdall1?type=0&rgId=" + b,
            pageList: [10, 25, 50, 100],
            pageNumber: 1,
            pageSize: 5,
            contentType: "application/json",
            strictSearch: false,
            minimumCountColumns: 2,
            detailView: false,
            cache: false,
            sortable: true,
            sortOrder: "asc",
            sortName: 'rgdId',
            uniqueId: "id",
            columns: [{
                // field: 'id', // 返回json数据中的name
                title: '序号', // 表格表头显示文字
                align: 'center', // 左右居中
                width: 80,
                valign: 'middle', // 上下居中
                formatter: function (value, row, index) {
                    return tableIndexNum(index);
                },
                footerFormatter: function (value) {
                    return "合计";
                }
            }, {
                field: 'rgdId', // 返回json数据中的name
                title: 'id', // 表格表头显示文字
                align: 'center', // 左右居中
                width: 80,
                valign: 'left', // 上下居中
            }, {
                field: 'rgTheme',
                title: '对应采购退货单',
                align: 'center',
                valign: 'middle',
                formatter: f2
            }, {
                field: 'title',
                title: '产品名称',
                align: 'center',
                valign: 'middle',
                formatter: f3
            }, {
                field: 'rgdReason',
                title: '退货原因',
                align: 'center',
                valign: 'middle',
                sortable: true,
                formatter: isNullFormatter
            }, {
                field: 'rgdNum',
                title: '退货数量',
                align: 'center',
                valign: 'middle',
                formatter: isNullFormatter
            }, {
                field: 'rgdMoney',
                title: '退货金额',
                align: 'center',
                valign: 'middle',
                formatter: isNullFormatter
            }, {
                field: 'rgdOutwhNum',
                title: '已出库数量',
                align: 'center',
                valign: 'middle',
                formatter: isNullFormatter
            }],
            onLoadSuccess: function (data) { //加载成功时执行
                console.info("加载成功");
            },
            onLoadError: function () { //加载失败时执行
                console.info("加载数据失败");
            },
        });
    }

    function isNullFormatter(value, rows, index) {
        if (value == null)
            return "";
        else
            return value;
    }

    function moneyFormatter(value, rows, index) {
        if (value == null)
            return "";
        else {
            var money = transMoney(value);
            return "&nbsp;￥" + money;
        }
    }

    function f1(value, rows, index) {
        if (value == null)
            return "";
        else {
            return "<p align='center'><span style='font-weight:normal;color:#9e9e9e'>〖</span>" + value + "&nbsp;<a href='');><i class='fa fa-folder-open m-l-5'></i></a><span style='font-weight:normal;color:#9e9e9e'>〗</span></p>";
        }
    }

    function transMoney(money) {
        if (money && money != null) {
            money = String(money);
            var left = money.split('.')[0], right = money.split('.')[1];
            right = right ? (right.length >= 2 ? '.' + right.substr(0, 2) : '.' + right + '0') : '.00';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);
            return (Number(money) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
        } else if (money === 0) {   //注意===在这里的使用，如果传入的money为0,if中会将其判定为boolean类型，故而要另外做===判断
            return '0.00';
        } else {
            return "";
        }
    }

    function sumFormatter(data) {
        field = this.field;
        var sum1 = data.reduce(function (sum, row) {
            return sum + (+row[field]);
        }, 0);
        return "￥" + transMoney(sum1);
    }

    //表格自动序号
    function tableIndexNum(index) {
        var currentPage = $(".page-item.active").find('a').text();
        var size = $(".page-list .page-size").text();
        if (size == null || size.length < 0)
            size = 5;
        return Number(index + 1 + eval((currentPage - 1) * size));
    }

    //表单数据转json对象
    function f(form) {
        var d = {};
        var t = form.serializeArray();
        $.each(t, function () {
            if (this.value != null && this.value.length > 0)
                d[this.name] = this.value;
        });
        return d;
    }

    function edit1() {
        var b = "/sale/purProDetail/" + $("#a").val() + "/";
        window.open(b);
    }

    function f2(value, row, index) {
        if (value == null)
            return "";
        else {
            return ['<a    href="/sale/rgdetail1/' + row.rgId + '/")"><i class="fa fa-arrow-circle-right text-blue m-r-5"></i>&nbsp;' + value + '</a>'].join('');
        }
    }

    function f3(value, row, index) {
        if (value == null)
            return "";
        else {
            return ['<a    href="/sale/product/info/' + row.pfId + '/")"><i class="fa fa-arrow-circle-right text-blue m-r-5"></i>&nbsp;' + value + '</a>'].join('');
        }
    }
</script>
</body>
</html>