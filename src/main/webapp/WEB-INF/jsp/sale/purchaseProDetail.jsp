<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-14
  Time: 下午3:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>编辑采购明细</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <!-- [if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![end if]-->
</head>
<body>
<style type="text/css">
    .mybtn {
        position: fixed;
        right: 50px;
        bottom: 50px;
        display: block;
        border-radius: 50%;
        padding: 20px;
        text-align: center;
        line-height: 40px;
    }

    .modal.left .modal-dialog,
    .modal.right .modal-dialog {
        position: fixed;
        margin: auto;
        width: 500px;
        height: 95%;
        -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
        -o-transform: translate3d(0%, 0, 0);
        transform: translate3d(0%, 0, 0);
    }

    .modal.left .modal-content,
    .modal.right .modal-content {
        height: 100%;
        overflow-y: auto;
    }

    .modal.left .modal-body,
    .modal.right .modal-body {
        padding: 15px 15px 80px;
    }

    /*Left*/
    .modal.left.fade .modal-dialog {
        left: -320px;
        -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
        -o-transition: opacity 0.3s linear, left 0.3s ease-out;
        transition: opacity 0.3s linear, left 0.3s ease-out;
    }

    .modal.left.fade.in .modal-dialog {
        left: 0;
    }

    /*Right*/
    .modal.right.fade .modal-dialog {
        right: -500px;
        top: 2.5%;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    .modal.right.fade.in .modal-dialog {
        right: 2%;
    }

    /* ----- MODAL STYLE ----- */
    .modal-content {
        border-radius: 5px;
        border: none;
    }

    .modal-header {
        border-bottom-color: #EEEEEE;
        background-color: #FAFAFA;
    }

</style>
<!--点击右侧弹出-->
<a href="javascript:;" class="btn btn-danger mybtn" data-toggle="modal" data-target="#myModal"><i
        class="fa fa-user"></i><span>产品</span></a>
<!-- 弹出层 modal -->
<div class="modal right fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="modal" style="overflow: hidden">
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <p>产品树图示说明&nbsp;&nbsp; <span class="fa fa-shopping-cart"></span> : 计算库存&nbsp;&nbsp;
                <span class="fa fa-comment-o"></span> : 查看产品详情</p>
        </div>
        <div class="col-md-12">
            采购详细 ： <span style="font-weight:normal;color:#9e9e9e">〖</span>${info.cusName}
            <a href="/customer_detail?id=" +${info.cusId} >
                <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span
                style="font-weight:normal;color:#9e9e9e">〗</span>
        </div>
        <div class="col-md-12" style="margin-top: 20px">
            <form id="save-form" action="/sale/purdetail/save" method="post">
                <input id="purId" type="hidden" name="id" value="${info.purId}">
                <table id="table"></table>
            </form>

        </div>
        <div class="col-md-12 text-center" style="margin-top: 20px">
            <button class="btn btn-lg " id="save">保存明细数据</button>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-treeview.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<!-- Latest compiled and minified Locales -->
<script src="/js/sweetalert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script>
    $(function () {
        initTable();
        sumitForm();
        $('#modal').load('/sale/chooseProduct')
    })

    function initTable() {
        $("#table").bootstrapTable({
            method: 'get',
            striped: true,
            showExport: true,
            cache: false,
            sortable: false,
            sortOrder: "desc",
            uniqueId: "pfId",
            url: "/sale/purProDetail1?id=${info.purId}",
            exportOptions: {
                fileName: $('#table-title').text(),
                ignoreColumn: [7]
            },
            showFooter: true,
            columns: [{
                field: 'title',
                title: '品名型号/规格[单位]',
                align: 'center',
                valign: 'middle',
                footerFormatter: "合计",
                formatter: function (value, row, index) {
                    var a = row.pfId;
                    var json = value + '<input  class="form-control" name="datas[' + index + '].tbProductFormatByPfId.pfId" type="hidden" value="' + a + '">' +
                        '<input class="form-control" name="datas[' + index + '].tbPurchaseByPurId.purId" type="hidden" value="${info.purId}">';
                    if (row.pdId != undefined) {
                        json = json + '<input class="form-control" name="datas[' + index + '].pdId" type="hidden" value="' + row.pdId + '">';
                    }
                    return json;
                }
            }, {
                field: 'pdPrice',
                title: '单价',
                align: 'center',
                valign: 'middle',
                formatter: function (value, row, index) {
                    return [
                        '<input class="form-control" disabled id="price' + index + '" type="number" value="' + value + '">'
                    ]
                }
            }, {
                field: 'pdNum',
                title: '数量',
                align: 'center',
                valign: 'middle',
                formatter: function (value, row, index) {
                    return [
                        '<input class="form-control" name="datas[' + index + '].pdNum" id="number' + index + '"type="number" value="' + value + '">'
                    ]
                },
                footerFormatter: sum
            }, {
                field: 'discount',
                title: '折扣',
                align: 'center',
                valign: 'middle',
                formatter: function (value, row, index) {
                    var d = 100;
                    if (row.pdNum * row.pdPrice != 0)
                        d = 100 * (row.pdTotal / (row.pdNum * row.pdPrice));
                    return [
                        '<div class="input-group">' +
                        '<input class="form-control" id="discount' + index + '" type="number" value="' + d + '">' +
                        '<span class="input-group-addon">%</span></div>'
                    ]
                }
            }, {
                field: 'pdTotal',
                title: '金额',
                align: 'center',
                valign: 'middle',
                formatter: function (value, row, index) {
                    return [
                        '<input class="form-control" name="datas[' + index + '].pdTotal" id="total' + index + '" type="number" value="' + value + '">'
                    ]
                },
                footerFormatter: sum
            }, {
                field: 'pdOther',
                title: '备注',
                align: 'center',
                valign: 'middle',
                formatter: function (value, row, index) {
                    if (value == null)
                        value = "";
                    return [
                        '<textarea class="form-control" name="datas[' + index + '].pdOther" id="other' + index + '" rows="1"  style="resize: vertical" >' + value + '</textarea  >'
                    ]
                }
            }, {
                field: 'pfId',
                title: '操作',
                align: 'center',
                formatter: function (value, row, index) {
                    return [
                        '<a class="btn" href="javascript:;" onclick="deleteDetail(' + row.pdId + ',+' + row.pfId + ')"><i class="fa fa-trash"></i></a>'
                    ]
                }
            }],
            onLoadSuccess: function (data) {
                change();
                // alert(JSON.stringify(data))
            },
            onLoadError: function (status) {
            }
        })
    }

    function sum(data) {
        field = this.field;
        return data.reduce(function (sum, row) {
            return sum + (+row[field]);
        }, 0);
    }

    function change() {
        for (var i = 0; i < $('#table').bootstrapTable('getData', 'useCurrentPage').length; i++) {
            $('#number' + i).blur((function blur(index) {
                return function () {
                    if ($('#number' + index).val().length <= 0 || parseFloat($('#number' + index).val()) <= 0) {
                        $('#number' + index).val(0)
                    }
                    $('#total' + index).val(parseFloat($('#number' + index).val()) * parseFloat($('#price' + index).val()) *
                        parseFloat($('#discount' + index).val()) / 100)
                    update(index, $('#total' + index).val(), $('#number' + index).val());
                }
            })(i))
            $('#price' + i).blur((function blur(index) {
                return function () {
                    if ($('#price' + index).val().length <= 0 || parseFloat($('#price' + index).val()) <= 0) {
                        $('#price' + index).val(0)
                    }
                    $('#total' + index).val(parseFloat($('#number' + index).val()) * parseFloat($('#price' + index).val()) *
                        parseFloat($('#discount' + index).val()) / 100)
                    update(index, $('#total' + index).val(), $('#number' + index).val());
                }
            })(i))
            $('#discount' + i).blur((function blur(index) {
                return function () {
                    if ($('#discount' + index).val().length <= 0 || parseFloat($('#discount' + index).val()) <= 0) {
                        $('#discount' + index).val(100)
                    }
                    $('#total' + index).val(parseFloat($('#number' + index).val()) * parseFloat($('#price' + index).val()) *
                        parseFloat($('#discount' + index).val()) / 100)
                    update(index, $('#total' + index).val(), $('#number' + index).val());
                }
            })(i))
            $('#total' + i).blur((function blur(index) {
                return function () {
                    if ($('#total' + index).val().length <= 0 || parseFloat($('#total' + index).val()) <= 0) {
                        $('#total' + index).val(0)
                    }
                    $('#discount' + index).val(parseFloat($('#total' + index).val()) / parseFloat($('#price' + index).val()) /
                        parseFloat($('#discount' + index).val()))
                    update(index, $('#total' + index).val(), $('#number' + index).val());
                }
            })(i))
        }
    }

    function sumitForm() {
        //提交表单数据
        $('#save').click(function () {
            $("#save-form").ajaxSubmit({
                dataType: 'json',
                success: function (result) {
                    if (result.code == 200) {
                        swal({
                                title: "成功！",
                                text: "2秒后自动关闭。",
                                timer: 2000,
                                showConfirmButton: false
                            },
                            function () {
                                successForm();
                            });
                    }
                    else {
                        swal(result.msg, "", "error");
                    }
                },
                error: function () {
                    swal("连接服务器错误", "", "error");
                }
            });
        })
    }

    function deleteDetail(id, pfId) {
        swal({
                title: "确定删除吗？",
                text: "你即将删除该采购明细！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function () {
                if (id != undefined) {
                    var url = "/sale/pddelete?pdId=" + id;
                    ajax(url, function (data) {
                        $('#table').bootstrapTable('removeByUniqueId', pfId);
                        swal("删除！", "一条采购详情已经被删除,请保存明细数据！", "success");
                    }, function (msg) {
                        swal("加载失败", msg, "error");
                        $('#wsj-add').modal('hide')
                    }, "get", {});
                }
                else {
                    $('#table').bootstrapTable('removeByUniqueId', pfId);
                    swal("删除！", "一条采购明细已经被删除,请保存明细数据", "success");
                }
            });
    }

    function successForm() {
        window.close();
        window.opener.history.go(0);
        // var url="/sale/purdetail1/"+$("#purId").val()+"/";
        // window.open(url)
    }

    function selectProduct(title, id, price, stock) {
        var datas = $('#table').bootstrapTable("getData", true);
        for (var i = 0; i < datas.length; i++) {
            if (datas[i].pfId == id) {
                swal("添加重复", "订单详情中已经有该产品", "error");
                return;
            }
        }
        if (stock) {
            swal("添加错误", "不需要计算库存的产品，请选择合同", "error")
            return;
        } else {
            $('#table').bootstrapTable('append', {
                    title: title,
                    "pfId": id,
                    "pdPrice": price,
                    "pdNum": 1,
                    "pdTotal": price
                }
            );
            change();
        }
    }

    function update(index, total, number) {
        $('#table').bootstrapTable("updateCell", {
            index: index,
            field: 'pdTotal',
            value: total
        })
        $('#table').bootstrapTable("updateCell", {
            index: index,
            field: 'pdNum',
            value: number
        })
        change()
    }
</script>
</body>
</html>
