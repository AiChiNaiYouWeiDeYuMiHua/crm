<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/7
  Time: 9:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>采购退货单</title>
    <!-- Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.css" />
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.css" />
    <!--<link rel="" />-->
</head>
<style type="text/css">
    html,
    body {
        color: #333333;
        background-color: #FFFFFF;
        font-size: 12px;
        font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .table-t thead{
        background-color: #EBEFF2;font-family: 楷体;
    }
</style>

<body>
<div class="container-fluid" style="background-color: #F4F8FB;">
    <div style="margin: 10px;background-color: white;padding: 10px;">
        <div class="row">
            <div class="col-md-12">
                <h4 class="modal-title text-dark">
                    <span>采购退货单</span>
                </h4>
                <hr style="border-top: 1px solid #aaaaaa;">
            </div>
        </div>
        <div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">主题：</label>
                        <div class="col-md-10">上海仓库采购-退货</div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">退货单号:</label>
                        <div class="col-md-10">123456789</div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">对应供应商:</label>
                        <div class=" col-md-10"><span style="font-weight:normal;color:#9e9e9e">〖</span>丰源
                            <a href="javascript:vopen('/xcrm/customer/customer/detail.xt?id=4','customer4',999,600);"><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">对应采购单:</label>
                        <div class=" col-md-10"><span style="font-weight:normal;color:#9e9e9e">〖</span>上海仓库采购
                            <a href="javascript:vopen('/xcrm/customer/customer/detail.xt?id=4','customer4',999,600);"><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">供应商联系人：</label>
                        <div class=" col-md-10">杨威</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">经办人:</label>
                        <div class=" col-md-10">陈默</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">退货时间:</label>
                        <div class=" col-md-10">2017-03-17</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">应退款:</label>
                        <div class=" col-md-10">￥119,020.00</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">已退款:</label>
                        <div class=" col-md-10">￥119,020.00</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">出库仓库:</label>
                        <div class=" col-md-10">上海仓库</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">状态:</label>
                        <div class=" col-md-10">结束</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">出库状态:</label>
                        <div class=" col-md-10">全部出库</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">退款状态:</label>
                        <div class=" col-md-10">全部退款</div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">退货明细:</label>
                    </div>
                </div>
                <!--<div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-12" style="font-size: 12px;font-weight: normal;text-align: right;">
                                                    <a class="f-s-12 text-black" href="javascript:vopen('/sys/editprice/puritemprint.xt?pu_id=51&amp;mdb=100dd1','print',960,530);"><i class="ion-printer m-r-5"></i>打印采购单</a>
<a class="f-s-12 text-black" href="javascript:void(0)" onclick="javascript:openbasepop('导出word打印', 'print_word', 'mdb=752fbc&amp;id=0&amp;type=purchase&amp;t_id=51&amp;t_mdb=100dd1&amp;vparse=amF2YXNjcmlwdDp2b3BlbignL3N5cy9lZGl0cHJpY2UvcHVyaXRlbXByaW50Lnh0P3B1X2lkPTUxJm1kYj0xMDBkZDEmc2lkPTEzMCZzc249OW4zdXN1ZnYwNTg4Mjh1bTRkaGc2am1tZzcmY2NuPWQwMDImY3I9ZTkxMDhiN2E5ODkzZTM3ZGMwZjU4YTFlZTlmOTEwOTcnLCdwcmludCcsMjAwLDIwMCk7', this, 300, 200);"><i class="fa fa-file-word-o m-r-5"></i>导出word打印</a>
                        <a class="f-s-12 text-black" href="/sys/editprice/puritemprint.xt?pu_id=51&amp;mdb=100dd1&amp;sid=130&amp;ssn=9n3usufv058828um4dhg6jmmg7&amp;ccn=d002&amp;cr=e9108b7a9893e37dc0f58a1ee9f91097&amp;te=1"><i class="fa fa-file-excel-o m-r-5"></i>导出excel打印</a>
                        </label>
                    </div>
                </div>-->
                <div class="col-md-12" style="padding-left: 80px;">
                    <div class="form-group">
                        <div class="col-md-12" style="padding-left: 80px;">
                            <div class="form-group">
                                <table id="returnGoodsTable" class="table-t">
                                    <thead>
                                    <tr>

                                    </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                        <!--<table class="table table-bordered" data-search="true" data-toggle="table" data-url="open_account">
                            <thead style="background-color: #EBEFF2;font-family: 楷体;">
                                <tr>
                                    <th>品名</th>
                                    <th>型号</th>
                                    <th>规格</th>
                                    <th>单位</th>
                                    <th>退货数量 </th>
                                    <th>已入库</th>
                                    <th>未入库</th>
                                    <th>单价</th>
                                    <th>金额</th>
                                    <th>退货原因</th>
                                    <th>备注</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <span>烟雾报警器 <a><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a></span>
                                    </td>
                                    <td>ASH-01</td>
                                    <td>ASH-01</td>
                                    <td>10台</td>
                                    <td>1</td>
                                    <td>0</td>
                                    <td>1</td>
                                    <td>RMB230.00</td>
                                    <td>RMB230.00</td>
                                    <td>RMB230.00</td>
                                    <td>66666</td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>烟雾报警器 <a><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a></sapn>
                                    </td>
                                    <td>ASH-01</td>
                                    <td>ASH-01</td>
                                    <td>10台</td>
                                    <td>1</td>
                                    <td>0</td>
                                    <td>1</td>
                                    <td>RMB230.00</td>
                                    <td>RMB230.00</td>
                                    <td>RMB230.00</td>
                                    <td>66666</td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="font-family: 楷体;">总计（大写金额）</td>
                                    <td colspan="7" style="text-align: right;font-family: 楷体;">捌佰叁拾元整</td>
                                </tr>
                            </tbody>
                        </table>-->
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">退货已出库:</label>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left: 80px;">
                    <div class="form-group">
                        <table id="returnGoodsTable1" class="table-t">
                            <thead>
                            <tr>

                            </tr>
                            </thead>
                        </table>
                        <!--<table class="table table-bordered" data-search="true" data-toggle="table" data-url="open_account">
                            <thead style="background-color: #EBEFF2;font-family: 楷体;">
                                <tr>
                                    <th>标题</th>
                                    <th>填单日期 </th>
                                    <th>出库明细</th>
                                    <th>经手人</th>
                                    <th>状态</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>上海仓库采购-退货</td>
                                    <td>2017-03-17</td>
                                    <td>
                                        <table class="table table-bordered" data-search="true" data-toggle="table" data-url="open_account">
                                            <thead style="background-color: #EBEFF2;font-family: 楷体;">
                                                <tr>
                                                    <th>序号</th>
                                                    <th>品名</th>
                                                    <th>型号</th>
                                                    <th>规格</th>
                                                    <th>单位</th>
                                                    <th>出库量</th>
                                                    <th>备注</th>
                                                    <th>参考库位</th>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>
                                                        <span>烟雾报警器 <a><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a></span>
                                                    </td>
                                                    <td>ASH-01</td>
                                                    <td>ASH-01</td>
                                                    <td>10台</td>
                                                    <td>1.00</td>
                                                    <td>66666</td>
                                                    <td>66666</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>
                                                        <span>烟雾报警器 <a><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i></a></span>
                                                    </td>
                                                    <td>ASH-01</td>
                                                    <td>ASH-01</td>
                                                    <td>10台</td>
                                                    <td>1.00</td>
                                                    <td>66666</td>
                                                    <td>66666</td>
                                                </tr>
                                                <tr>
                                                    <td>合计</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>2.00</td>
                                                    <td>66666</td>
                                                    <td>66666</td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </td>
                                    <td>陈默</td>
                                    <td>已出库</td>
                                </tr>
                            </tbody>
                        </table>-->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
        <div class="col-md-12">
            <div class="form-group">
                ￥退款记录
                <button type="button" class="btn btn-link" style="font-size: 20px;text-decoration: none;margin-left:5px;color: black;" data-toggle="modal" data-target="#myModal2">+</button>
            </div>
            <!--	 模态框（Modal） -->
            <!--<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          &times;            </button>
                            <h4 class="modal-title" id="myModalLabel">
   付款记录          </h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;">期次:</label>
                                    <div class="col-md-3">
                                        <input class="form-control " />
                                    </div>
                                    <label class="col-md-3 " style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>付款日期:</label>
                                    <div class="col-md-3">
                                        <input class="form-control" />
                                    </div>
                                </div>
                            </div><br><br><br>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>付款金额:</label>
                                    <div class="col-md-3">
                                        <input class="form-control" />
                                    </div>
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;">外币备注:</label>
                                    <div class="col-md-3">
                                        <input class="form-control" />
                                    </div>
                                </div>
                            </div><br><br><br>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;">*付款方式:</label>
                                    <div class="col-md-3">
                                        <input class="form-control" />
                                    </div>
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>期次:</label>
                                    <div class="col-md-3">
                                        <input class="form-control" />
                                    </div>
                                </div>
                            </div><br><br><br>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>经手人:</label>
                                    <div class="col-md-3">
                                        <input class="form-control" />
                                    </div>
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;">分类:</label>
                                    <div class="col-md-3">
                                        <input class="form-control" />
                                    </div>
                                </div>
                            </div><br><br><br>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3" style="font-size: 12px;font-weight: normal;text-align: right;">备注:</label>
                                    <div class="col-md-9">
                                        <input class="form-control" />
                                    </div>
                                </div>
                            </div><br><br><br>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭            </button>
                            <button type="button" class="btn btn-primary">
       保存         </button>

                        </div>
                        <!-- /.modal-content -->
            <!--</div>
        </div>
    </div>-->
            <!-- /.modal -->
            <!--</div>-->
            <div id="model1"></div>
            <div class="panel-body">
                <span>￥</span>100.00</span>
                </a><span>〖上海仓库采购 <a><i class="fa fa-folder-open m-l-5"></a></i>〗</span>2018-07-12<br>
            </div>
        </div>
    </div>
    <div class="row" style="text-align: center;">
        <div class="col-md-12">
            <div class="form-group">
                <footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
                    <a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
                    <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
                    <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
                    <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>
            </div>
        </div>
    </div>
</div>
</div>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
<script src="../js/bootstrap-datetimepicker.min.js"></script>
<script src="../js/returnGoodsTable.js"></script>
<script src="../js/returnGoodsTable1.js"></script>
<script src="../js/returnGoodsTable2.js"></script>

<script type="text/javascript">
    $("#model1").load("pur_model3.html");
    $(".form_datetime").datetimepicker({
        format: "dd MM yyyy - hh:ii",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });
</script>
</body>

</html>
