<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/7
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>发货单：〖${info.dgDate}〗</title>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css" />

</head>
<style type="text/css">
    html,
    body {
        color: #333333;
        background-color: #F4F8FB;
        font-size: 12px;
        font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .table-t thead{
        background-color: #EBEFF2;font-family: 楷体;
    }
    a{
        color: #505458;
    }
    .text-gray{
        color: #999;
    }
    .m-20{
        margin: 20px;
    }
    .m-l-10{
        margin-left: 10px;
    }
    .m-b-10{
        margin-bottom: 10px;
    }
    .m-b-30{
        margin-bottom: 30px;
    }
    .icon-box{
        display: inline-block;
        min-width: 100px;
    }
    .border-r{
        border-right: 1px solid #ccc;
    }
    a:hover{text-decoration: none}
    .panel-title{
        font-size: 12px;
    }
    .fixed-table-body{
        height: auto;
    }
</style>
<body>
<div class="container">
    <div class="row" style="background-color: white;padding: 10px;">
        <div id="pro-header">
            <div class="pull-right" style="font-size: 12px;">
                <c:if test="${info.status != 1}">
                    <g:g id="302">
                    <button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" onclick="cancelSend(${info.dgId})" data-target="#delModal">
                        <i class="fa fa-undo" style="margin-right: 5px;"></i>撤销
                    </button>&nbsp;
                    </g:g>
                </c:if>
                <g:g id="303">
                <button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.dgId})">
                    <i class="fa fa-pencil" style="margin-right: 5px;"></i>编辑
                </button>&nbsp;
                </g:g>
            </div>
            <h4>
                <span>发货单/发货明细</span>
                <a target="_blank" href="??" style="color: #999999;" data-toggle="tooltip" data-placement="bottom" title="数据日志">
                    <i class="fa fa-bars"></i>
                </a>
            </h4>
            <hr style="border-top: 1px solid #aaaaaa;" />
        </div>
        <div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">客户：</label>
                        <div class=" col-md-10">
                            <c:if test="${info.cusId != null}">
                                <span style="font-weight:normal;color:#9e9e9e">〖</span>${info.cusName}
                                <a target="_blank" href="/customer_details?id=${info.cusId}" >
                                    <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>
                            </c:if>
                        </div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">订单：</label>
                        <div class=" col-md-8">
                            <c:if test="${info.orderId != null}">
                                <span style="font-weight:normal;color:#9e9e9e">〖</span>${info.orderTitle}
                                <a target="_blank" href="/sale/order/info/${info.orderId}/" >
                                    <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>
                            </c:if></div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">发货人：</label>
                        <div class="col-md-8">${info.userName}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">发货日期：</label>
                        <div class="col-md-8">${info.dgDate}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">发货单号：</label>
                        <div class="col-md-8">${info.dgNumber}</div>
                    </div>
                    <hr class="border-t-a">
                </div>

                <div class=" col-md-12">
                   <div class="form-group">
                       <label class="control-label col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">收货人/地址:</label>
                       <div class=" col-md-10">
                           <div class="panel-group" id="show_contract_address">
                               <div class="panel panel-default">
                                   <div class="panel-heading">
                                       <h4 class="panel-title" style="width: 100%">
                                           <a data-toggle="collapse" style="display: block;" data-parent="#show_contract_address" href="#contract_address" aria-expanded="true" class="">
                                               详细信息
                                           </a>
                                       </h4>
                                   </div>
                                   <div id="contract_address" class="panel-collapse collapse in" aria-expanded="true" style="">
                                       <div class="panel-body">
                                           <p><b>收货人</b></p>
                                           <div class="row  form-horizonta">
                                               <div class="col-md-12 form-group ">
                                                   <label class="control-label col-md-3">姓名:</label>
                                                   <div class="col-md-9">${info.tbAddressByAddressId.addressName}</div>
                                               </div>
                                               <div class="col-md-12 form-group ">
                                                   <label class="control-label col-md-3">联系方式:</label>
                                                   <div class="col-md-9">${info.tbAddressByAddressId.addressTel}</div>
                                               </div>
                                           </div>
                                           <p>
                                               <b>地址</b>
                                           </p>

                                           <div class="row  form-horizonta">
                                               <div class="col-md-12 form-group" id="id_state">
                                                   <label class="control-label col-md-3" id="dt_sendgoods_country_3">省</label>
                                                   <div class="col-md-9">${info.tbAddressByAddressId.addressProvince}</div>
                                               </div>

                                               <div class="col-md-12 form-group" id="dt_sendgoods_country_5">
                                                   <label class="control-label col-md-3" id="dt_sendgoods_country_4">市</label>
                                                   <div class="col-md-9">${info.tbAddressByAddressId.addressCity}</div>
                                               </div>
                                               <div class="col-md-12 form-group" id="dt_sendgoods_country_7">
                                                   <label class="control-label col-md-3" id="dt_sendgoods_country_6">县</label>
                                                   <div class="col-md-9">${info.tbAddressByAddressId.addressCounty}</div>
                                               </div>
                                               <div class="col-md-12 form-group ">
                                                   <label class="control-label col-md-3">地址:</label>
                                                   <div class="col-md-9">${info.tbAddressByAddressId.addressContent}</div>
                                               </div>
                                               <div class="col-md-12 form-group ">
                                                   <label class="control-label col-md-3">邮编:</label>
                                                   <div class="col-md-9">${info.tbAddressByAddressId.addressCode}</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">发货方式：</label>
                        <div class="col-md-8">${info.dgModel}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">状态：</label>
                        <div class="col-md-8"><c:choose>
                            <c:when test="${info.status == 1}">
                                待发货
                            </c:when>
                            <c:when test="${info.status==2}">
                                已发货
                            </c:when>
                            <c:when test="${info.status==3}">
                                已签收
                            </c:when>
                        </c:choose></div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">打包件数：</label>
                        <div class="col-md-8">${info.status}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">重量：</label>
                        <div class="col-md-8">${info.dgWeight}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">物流公司：</label>
                        <div class="col-md-8">${info.dgLogistice}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">物流单号：</label>
                        <div class="col-md-8">${info.dgLogisticId}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">运费：</label>
                        <div class="col-md-8">${info.dgFreight}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4" style="font-size: 12px;font-weight: normal;text-align: right;">运费结算：</label>
                        <div class="col-md-8">${info.dgFreightModal}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">备注：</label>
                        <div class=" col-md-10"><span style="font-weight:normal;color:#9e9e9e">〖</span>${info.dgOther}
                            </div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <g:g id="304">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">发货明细:</label>
                    </div>
                </div>

                <div class="col-md-12" id="outdetail"></div>
                </g:g>
            </div>
    </div>
        <div id="pro-bottom" class="text-right">
            <g:g id="302">
            <button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" data-target="#delModal" onclick="deletePro()">
                <i class="fa fa-trash-o" style="margin-right: 5px;" ></i>删除
            </button>&nbsp;
            </g:g>
            <g:g id="303">
            <button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.dgId})">
                <i class="fa fa-pencil" style="margin-right: 5px;" ></i>编辑
            </button>&nbsp;
            </g:g>
        </div>
    </div>

<div class="row" style="text-align: center;background-color: #F4F8FB;">
    <div class="col-md-12">
        <div class="form-group">
            <footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
                <a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
                <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
                <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
                <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>
        </div>
    </div>
</div>
        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content mymodalcontent">
                </div>
            </div>
        </div>
    </div>
        <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
        <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
        <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
        <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<script src="/js/tableExport.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
        <!--excel-->
        <%--<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>--%>
        <script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
        <script src="/js/sweetalert.min.js"></script>
        <script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
        <script src="https://cdn.bootcss.com/bootstrap-validator/0.5.3/js/language/zh_CN.min.js"></script>
        <script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>

<script src="/js/sale/formatter.js"></script>
<script type="text/javascript">
    $(function () {
        $("[data-toggle='tooltip' ] ").tooltip();
        <%--$('#changeStatus').on('show.bs.modal',function () {--%>
            <%--$('#changeStatus input[nam  e="orderStatus"]').eq(${info.orderStatus}).prop("checked","checked");--%>
        <%--})--%>
        sumitStatus();
        //消除模态框数据
        $("#add").on("hidden.bs.modal", function () {
            open_modal = null;
            $(this).removeData("bs.modal");
        });
        $('#outdetail').load("/sale/sendDetail?id=${info.dgId}");

    });
    function edit(id) {
        $('#add').modal({
            remote:'/sale/send/add?id='+id
        });
    }
    function successForm() {
        $('#add').modal('hide')
        window.history.go(0)
    }
    function deletePro() {
        swal({
                title: "确定删除吗？",
                text: "你将无法恢复该入库单！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function(){
                ajax("/sale/warehouse/in/delete",function () {
                    swal("成功！", "入库单删除成功！","success");
                    window.close();
                },function(){},'post',data={'id':${info.dgId}});
            });


    }
    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                    error()
                }
            }
        })
    }
    function sumitStatus() {
        //提交表单数据
        $('#status-btn').click(function () {
                $("#status").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
        })
    }
    //撤销发货
    function cancelSend(id) {
        swal({
                title: "确定撤销吗？",
                text: "发货单状态即将恢复！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定撤销！",
                closeOnConfirm: false
            },
            function(){
                ajax("/sale/warehouse/send/cancel",function () {
                    swal("成功","撤销发货成功","success");
                    window.history.go(0);
                },function () {},"post",{"id":id})
            });

    }
</script>
</body>
</html>
