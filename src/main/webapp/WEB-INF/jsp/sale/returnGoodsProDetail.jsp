<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-14
  Time: 下午3:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>编辑退货明细</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <!-- [if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![end if]-->
</head>
<body>
<style type="text/css">
    .mybtn{position:fixed; right:50px; bottom:50px; display:block;
        border-radius:50%; padding:20px;
        text-align:center; line-height:40px;}
    .modal.left .modal-dialog,
    .modal.right .modal-dialog {
        position: fixed;
        margin: auto;
        width: 500px;
        height: 95%;
        -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
        -o-transform: translate3d(0%, 0, 0);
        transform: translate3d(0%, 0, 0);
    }

    .modal.left .modal-content,
    .modal.right .modal-content {
        height: 100%;
        overflow-y: auto;
    }

    .modal.left .modal-body,
    .modal.right .modal-body {
        padding: 15px 15px 80px;
    }

    /*Left*/
    .modal.left.fade .modal-dialog{
        left: -320px;
        -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
        -o-transition: opacity 0.3s linear, left 0.3s ease-out;
        transition: opacity 0.3s linear, left 0.3s ease-out;
    }

    .modal.left.fade.in .modal-dialog{
        left: 0;
    }

    /*Right*/
    .modal.right.fade .modal-dialog {
        right: -500px;
        top: 2.5%;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    .modal.right.fade.in .modal-dialog {
        right: 2%;
    }

    /* ----- MODAL STYLE ----- */
    .modal-content {
        border-radius: 5px;
        border: none;
    }

    .modal-header {
        border-bottom-color: #EEEEEE;
        background-color: #FAFAFA;
    }

</style>
<!--点击右侧弹出-->
<!-- 弹出层 modal -->
<div class="modal right fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="modal" style="overflow: hidden">
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6"  ></div>
            <label class="col-md-1"style="margin-top: 15px;margin-left: -90px">采购退货明细</label>
        </div>
        <div class="col-md-12">
            <div class="col-md-5"  ></div>
            <div class="col-md-2" style="margin-left: -50px" >供应商：<span style="font-weight:normal;color:#9e9e9e">〖</span>${info.cusName}
            <a href="/customer_detail?id="+${info.cusId} >
                <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
            <div class="col-md-2" style="margin-left: -70px" > 日期：${info.rgReturnTime}</div>
        </div>
        <div class="col-md-12" style="margin-top: 20px">
            <input type="hidden" id="purId" name="purId" value="${info.purId}">
            <input type="hidden" id="rgId" name="rgId" value="${id}">
            <form id="save-form" action="/sale/rgddetail/save" method="post">
                <table id="table"></table>
            </form>
        </div>
        <div class="col-md-12 text-center" style="margin-top: 20px">
            <button class="btn btn-lg " id="save">保存明细数据</button>
            <%--<input type="number" max="">--%>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-treeview.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<!-- Latest compiled and minified Locales -->
<script src="/js/sweetalert.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script>
    $(function () {
        initTable();
        sumitForm();
    })
    function initTable() {
        var rgId=$("#rgId").val();
        var purId=$("#purId").val();
        var a="/sale/rgdall1?type=0&rgId=${id}";
        $("#table").bootstrapTable({
            method: 'get',
            striped: true,
            showExport:true,
            cache: false,
            sortable: false,
            sortOrder: "desc",
            uniqueId:"rgdId",
            url:a,
            columns: [{
                field:'title',
                title:'品名型号/规格[单位]',
                align:'center',
                valign: 'middle',
                // footerFormatter:"合计",
                formatter:function (value, row, index) {
                    return [
                        value+'<input class="form-control"  type="hidden" value="'+row.title+'">'+
                        '<input class="form-control" name="datas['+index+'].rgdId"  type="hidden" value="'+row.rgdId+'">'
                    ]
                }
            },{
                field:'pdPrice',
                title:'退货单价',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                    return [
                        '<input class="form-control" disabled id="price'+index+'" type="number" value="'+row.pdPrice+'">'
                    ]
                }
            },{
                 field:'rgdNum',
                title:'退货数量',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                     var json  =  '<input  class="form-control" min="0" max="'+row.pdNum+'"  id="pdNum'+index+'"type="number" value="'+row.rgdNum+'">'+
                         '<input  class="form-control" type="hidden"  name="datas['+index+'].rgdNum" id="number'+index+'"type="number" value="'+value+'">'
                    return json;
                },
                footerFormatter:sum
            },{
                field:'pdTotal',
                title:'退货金额',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                    var d = row.rgdNum*row.pdPrice;
                    return [
                        '<input class="form-control" disabled id="pdTotal'+index+'" type="number" value="'+d+'">'+
                        '<input class="form-control" disabled  type="hidden"  name="datas['+index+'].rgdMoney" id="discount'+index+'" type="number" value="'+d+'">'
                    ]
                }
            },{
                title:'退货原因',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                    return [
                        '<input class="form-control" name="datas['+index+'].rgdReason" id="total'+index+'"value="">'
                    ]
                }
            },{
                title:'备注',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                    if (value == null)
                        value = "";
                    return [
                        '<textarea class="form-control" name="datas['+index+'].rgdRemarks" id="other'+index+'" rows="1"  style="resize: vertical" >'+value+'</textarea  >'
                    ]
                }
            },{
                title:'操作',
                align:'center',
                formatter:function (value, row, index) {
                    return [
                        '<a class="btn" href="javascript:;" onclick="deleteDetail('+row.rgdId+')"><i class="fa fa-trash"></i></a>'
                    ]
                }
            }],
            onLoadSuccess:function (data) {
                change();
            },
            onLoadError:function (status) {
            }
        })
    }
    function sum(data) {
        field = this.field;
        return data.reduce(function(sum, row) {
            var a=sum + (+row[field]);
            return a;
        }, 0);
    }
    function sumitForm() {
        //提交表单数据
        $('#save').click(function () {
            $("#save-form").ajaxSubmit({
                dataType:'json',
                success:function (result) {
                    if (result.code == 200) {
                        swal({
                                title: "退货明细保存成功！",
                                text: "2秒后自动关闭。",
                                timer: 2000,
                                showConfirmButton: false
                            },
                            function(){
                                window.close();
                                window.opener.history.go(0);
                            });
                    }
                    else{
                        swal(result.msg,"","error");
                    }
                },
                error:function () {
                    swal("连接服务器错误","","error");
                }
            });
        })
    }
    function deleteDetail(id) {
        swal({
                title: "确定删除吗？",
                text: "你即将删除该退货单明细！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function(){
                var url = "/sale/rgddelete?rgdId=" + id;
                ajax(url, function (data) {
                    $('#table').bootstrapTable('removeByUniqueId', id);
                    swal("删除！", "一条采购详情已经被删除,请保存明细数据！", "success");
                }, function (msg) {
                    $('#wsj-add').modal('hide')
                }, "get", {});
                // $('#table').bootstrapTable('removeByUniqueId', id);
                // swal("删除！", "一条退货详情已经被删除,请保存明细数据", "success");
                // $('#table').bootstrapTable("refresh");
            });
    }
    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                    error()
                }
            }
        })
    }
    function change() {
        for (var i =0 ;i<$('#table').bootstrapTable('getData','useCurrentPage').length;i++) {
            $('#pdNum'+i).blur((function blur(index) {
                return function () {
                    if ($('#pdNum'+index).val().length<=0||parseFloat($('#pdNum'+index).val())<=0) {
                        $('#pdNum' + index).val(0)
                    }
                    $('#number'+index).val( $('#pdNum' + index).val())
                    $('#pdTotal' + index).val(parseFloat($('#pdNum' + index).val()) * parseFloat($('#price' + index).val()));
                    // update(index,$('#pdTotal'+index).val(),$('#pdNum'+index).val());
                }
            })(i))
            $('#pdTotal'+i).blur((function blur(index) {
                return function () {
                    if ($('#pdTotal'+index).val().length<=0||parseFloat($('#pdTotal'+index).val())<=0) {
                        $('#pdTotal' + index).val(0)
                    }
                }
            })(i))
       }
    }

</script>
</body>
</html>
