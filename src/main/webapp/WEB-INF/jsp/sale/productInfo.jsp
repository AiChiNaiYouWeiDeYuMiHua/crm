<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<title>产品：${info.productEntity.productName}</title>
		<link rel="shortcut icon" href="/img/favicon.ico"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="/css/bootstrap-table.css">
		<link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
		<link rel="stylesheet" href="/css/sweetalert.css">
		<script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
		<link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
		<link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
		<link rel="stylesheet" href="/css/sale_start_modal.css" />
		<link href="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/css/fileinput.css" rel="stylesheet">
	</head>

	<body>
		<div id="content" style="margin-top: 20px;">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8">
						<div id="pro" class="form-horizontal" style="padding: 12px 20px;">
							<div id="pro-header">
								<div class="pull-right" style="font-size: 12px;">
									<g:g id="272">
									<button class="btn btn-default" style="font-size: 12px;" data-toggle="modal"  data-target="#delModal">
										<i class="fa fa-trash-o" style="margin-right: 5px;"></i>删除
									</button>&nbsp;
									</g:g>
									<g:g id="273">
									<button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.id},${info.base})">
										<i class="fa fa-pencil" style="margin-right: 5px;"></i>编辑
									</button>&nbsp;
									</g:g>
								</div>
								<h4>
									<span>产品信息</span>
									<a target="_blank" href="??" style="color: #999999;" data-toggle="tooltip" data-placement="bottom" title="数据日志">
										<i class="fa fa-bars"></i>
									</a>
								</h4>
								<hr style="border-top: 1px solid #aaaaaa;" />
							</div>
							<div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
								<div class="row">
									<div class="form-group col-md-12">
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">品名：</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.productEntity.productName}&nbsp;</p>
										</div>
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">型号：</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.productEntity.productModel}&nbsp;</p>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">规格：</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfName}&nbsp;</p>
										</div>
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">单位换算：</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfUnitCal}&nbsp;</p>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">编码/条码：</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfCode}&nbsp;</p>
										</div>
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">分类：</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.productEntity.tbProductCategoryByPcId.pcName}&nbsp;</p>
										</div>
									</div>
                                    <div class="form-group col-md-12">
                                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">批次：</label>
                                        <div class=" col-md-4">
                                            <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfBatch}&nbsp;</p>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">生产日期：</label>
                                        <div class=" col-md-4">
                                            <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatDate value="${info.pfDate}" pattern="yyyy-MM-dd" />&nbsp;</p>
                                        </div>
                                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">失效日期：</label>
                                        <div class=" col-md-4">
                                            <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatDate value="${info.pfExpiry}" pattern="yyyy-MM-dd" />&nbsp;</p>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">生产厂家：</label>
                                        <div class=" col-md-4">
                                            <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfManufacturer}&nbsp;</p>
                                        </div>
                                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">批准文号：</label>
                                        <div class=" col-md-4">
                                            <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfApprovalNumber}&nbsp;</p>
                                        </div>
                                    </div>
									<div class="form-group col-md-12">
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">计算库存：</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.productEntity.productStock?"否":"是"}&nbsp;</p>
										</div>
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">状态：</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.status==0?"正常":"停售"}&nbsp;</p>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">库存:</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.number}&nbsp;</p>
										</div>
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">单位:</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfUnit}&nbsp;</p>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">价格:</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfPrice}&nbsp;</p>
										</div>
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">成本价格:</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfCost}&nbsp;</p>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">重量:</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfWeight}&nbsp;</p>
										</div>
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">重量单位:</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfWeigtUnit}&nbsp;</p>
										</div>
									</div>
									<div class="form-group col-md-12">
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">库存上限:</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfMax}&nbsp;</p>
										</div>
										<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">库存下限:</label>
										<div class=" col-md-4">
											<p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.pfMin}&nbsp;</p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">备注：</label>
											<div class="col-md-10">${info.pfOther}&nbsp;</div>
										</div>
										<hr class="border-t-a">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">产品图片：</label>
											<div class="col-md-10">
												<img class="thumbnail" id="p-img" width="50%" height="50%" src="${info.pfImf}">
											</div>
										</div>
										<hr class="border-t-a">
									</div>
								</div>
							</div>
							<div id="pro-bottom" class="text-right">
								<g:g id="272">
									<button class="btn btn-default" style="font-size: 12px;" data-toggle="modal"  data-target="#delModal">
										<i class="fa fa-trash-o" style="margin-right: 5px;"></i>删除
									</button>&nbsp;
								</g:g>
								<g:g id="273">
									<button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.id},${info.base})">
										<i class="fa fa-pencil" style="margin-right: 5px;"></i>编辑
									</button>&nbsp;
								</g:g>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<table id="table-number" style="font-size: 13px;">
							<thead>

							</thead>
							<tbody style="background-color: white;">


							</tbody>
						</table>
					</div>
				</div>
				<div id="tool" style="margin-top: 20px;">
					<div class="panel panel-default">
						<div class="panel-heading" style="background-color: white;">
							<div class="panel-heading">
								<h5 class="panel-title">
									<i class="fa fa-pencil-square"></i>
									<b>全部规格</b>
									<%--<font style="cursor: pointer;" color="#808080">说明：产品“规格”的特殊作用</font>--%>
									<%--<span style="cursor: pointer;">--%>
										<%--<i class="fa fa-exclamation-triangle"></i>--%>
									<%--</span>--%>
									<g:g id="271">
									<a class="text-a btn btn-default" id="add_btn" style="margin-left: 10px;">
										<i class="fa fa-plus" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="新建"></i>新建子规格
									</a>
									</g:g>
									<%--<a class="text-a" style="margin-left: 10px;">--%>
										<%--<i class="fa fa-angle-double-down"></i>导入子规格--%>
									<%--</a>--%>
									<div class="pull-right">
									</div>
								</h5>
							</div>
							<div class="panel-body">
								<table id="formatter-table">
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<footer class="footer text-center" >热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
			<a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
			<a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
			<a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
			<br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>

		<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								&times;
							</button>
						<h4 class="modal-title" id="myModalLabel">
								删除
							</h4>
					</div>
					<div class="modal-body">
						<ol>
							<li>保留历史的成交数据，如订单、采购单等</li>
							<li>保留统计数据</li>
							<li>此产品将进入回收站，支持恢复</li>

						</ol>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭
							</button>
						<button type="button" class="btn btn-primary" onclick="deletePro()">
								确认删除
							</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal -->
		</div>
        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content mymodalcontent">
                </div>
            </div>
        </div>

		<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
		<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
		<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
		<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
		<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
		<script src="/js/tableExport.js"></script>
		<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
		<script src="/js/resjs/bootstrap-tooltip.js"></script>
		<script src="/js/resjs/bootstrap-popover.js"></script>
		<!--excel-->
		<script src="/js/sale/formatter.js"></script>
		<script src="/js/sale/product-table.js"></script>
		<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
		<script src="/js/sweetalert.min.js"></script>
		<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
		<script src="https://cdn.bootcss.com/bootstrap-validator/0.5.3/js/language/zh_CN.min.js"></script>
		<script src="/js/validator/product.js"></script>
		<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
		<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
		<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/zh.js"></script>
		<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/fa.min.js"></script>
		<script src="/js/sale/upload.js"></script>
		<script>
            var open_modal = ${info.id};
            var isedit = false;
			$(function() {
				$("[data-toggle='tooltip']").tooltip();
				addNumber();
				setProductFormatter();
				$('#add_btn').click(function () {
				    isedit = false;
                    $('#add').modal({
                        remote:'/sale/product/formatter/add'
                    });
                });

                $("#add").on("hidden.bs.modal", function () {
                    isedit = false;
                    $(this).removeData("bs.modal");
                });

			});
            function ajax(url,success,error,type,data) {
                $.ajax({
                    url:url,
                    timeout:3000,
                    type:type,
                    data:data,
                    dataType:'json',
                    success:function (result) {
                        if (result.code != 200) {
                            swal(result.msg,"","error");
                            error()
                        }
                        else {
                            success(result.data);
                        }
                    },
                    complete:function (XMLHttpRequest,status) {
                        if (status != "success") {
                            swal("连接服务器错误","","error");
                            error()
                        }
                    }
                })
            }
            //删除
            function deletePro() {
                        ajax("/sale/product/delete",function () {
                            swal("成功！", "产品删除成功！","success");
                            window.close();
                        },function(){},'post',data={'id':${info.id}});
            }
            function deleteFormatter(pfid) {
                swal({
                        title: "确定删除吗？",
                        text: "你将无法恢复该产品规格！",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "确定删除！",
                        closeOnConfirm: false
                    },
                    function(){
                        ajax("/sale/product/delete",function () {
                            swal("成功！", "产品删除成功！","success")
                            $('#formatter-table').bootstrapTable("refresh")
                        },function(){},'post',data={'id':pfid});
                    });

            }

            function edit(pfid,pfbase) {
                <g:g id="273">
                open_modal = pfid;
                if (!pfbase){
                    $('#add').modal({
                        remote:'/sale/product/add'
                    });
                } else {
                    isedit = true;
                    $('#add').modal({
                        remote:'/sale/product/formatter/add'
                    });
                }
                </g:g>
            }
            function successForm() {
				window.history.go(0);
            }
            function successFormatterForm() {
                $('#formatter-table').bootstrapTable("refresh")

            }
            function addNumber() {
				$('#table-number').bootstrapTable({
                    url: "/sale/warehouse/product?id=${info.id}",
                    striped: true,
                    method:'get',
					columns: [{
                        field: 'name', // 返回json数据中的name
                        title: '仓库', // 表格表头显示文字
                        align: 'center', // 左右居中
                        valign: 'middle', // 上下居中
                    },{
                        field: 'number', // 返回json数据中的name
                        title: '库存', // 表格表头显示文字
                        align: 'center', // 左右居中
                        valign: 'middle', // 上下居中
                        formatter:function(value,row,index){
                            if (value == null || value.length <=0)
                                return 0;
                            return value;
                        }
                    }]
                });
            }
		    function setProductFormatter() {
                $("#formatter-table").bootstrapTable({
                    url: "/sale/product/formatter/all?id=${info.productEntity.productId}",
                    striped: true,
                    contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
                    columns: [{
                        field: 'id', // 返回json数据中的name
                        title: '序号', // 表格表头显示文字
                        align: 'center', // 左右居中
                        width: 80,
                        valign: 'middle', // 上下居中
                        formatter:function(value,row,index){
                            return index+1;
                        }
                    },{
                        field: 'pfName',
                        title: '规格',
                        align: 'center',
                        valign: 'middle',
                        formatter:isnull
                    }, {
                        field: 'pfCode',
                        title: '编号/条码',
                        align: 'center',
                        valign: 'middle',
                        formatter:isnull
                    }, {
                        field: 'pfPrice',
                        title: '价格',
                        align: 'center',
                        valign: 'middle',
                        sortable: true,
                        formatter:function (value,row,index){
                            if (value != null)
                                return "￥"+value
                            else
                                return "￥0.00"
                        }
                    }, {
                        field: 'pfUnit',
                        title: '单位',
                        align: 'center',
                        valign: 'middle',
                        formatter:isnull
                    }, {
                        field: 'pfUnitCal',
                        title: '单位换算',
                        align: 'center',
                        valign: 'middle',
                        formatter:isnull
                    },{
                        field: 'status',
                        title: '状态',
                        align: 'center',
                        valign: 'middle',
                        formatter:function (value,row,index){
                            if (value == 0)
                                return "正常";
                            else
                                return "停售"
                        }
                    }, {
                        field: 'pfBatch',
                        title: '批次',
                        align: 'center',
                        valign: 'middle',
                        formatter:isnull
                    }, {
                        field: 'pfDate',
                        title: '生产日期',
                        align: 'center',
                        valign: 'middle',
                        formatter:isnull
                    },{
                        field: 'pfExpiry',
                        title: '失效日期',
                        align: 'center',
                        valign: 'middle',
                        formatter:isnull
                    }, {
                        title: '操作',
                        titlealign: 'left',
                        valign: 'middle',
                        align: 'center',
                        width: 80,
                        formatter: function(value, row, index) {
                            return [
								<g:g id="272">'<a onclick="deleteFormatter('+row.id+')" style="cursor:pointer;" data-toggle="tooltip" data-placement="bottom"  title="删除"><i class="fa fa-trash-o"></i></a>',</g:g>
								<g:g id="273">'<a onclick="edit('+row.id+','+row.base+')" style="cursor:pointer; margin-left:8px;" data-toggle="tooltip" data-placement="bottom"  title="编辑"><i class="fa fa-pencil"></i></a>'</g:g>
                            ].join('');
                        }
                    }],
                    onLoadSuccess: function() { //加载成功时执行
                        console.info("加载成功");
                    },
                    onLoadError: function() { //加载失败时执行
                        console.info("加载数据失败");
                    },

                });
            }
            function redio(dom,is) {
                dom.eq(0).removeProp('checked');
                dom.eq(1).removeProp('checked');
                if (is == 0 || !is ){
                    dom.eq(0).prop('checked', 'checked');
                }else
                    dom.eq(1).prop('checked', 'checked');
            }
        </script>

	</body>

</html>