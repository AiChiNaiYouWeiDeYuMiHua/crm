<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/7
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>维修工单详情页</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css" />
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">
</head>
<style type="text/css">
    html,
    body {
        color: #333333;
        background-color: #F4F8FB;
        font-size: 12px;
        font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .table-t thead{
        background-color: #EBEFF2;font-family: 楷体;
    }
</style>
<body>
<div class="container-fluid" style="background-color: #F4F8FB;">
    <div style="margin: 30px;background-color: white;padding: 10px;">
        <div class="row">
            <div class="col-md-12">
                <h4 class="modal-title text-dark">
                    <span set-lan="html:维修工单详情页">维修工单</span>
                </h4>
                <hr style="border-top: 1px solid #aaaaaa;">
            </div>
        </div>
        <div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
             <span class="modal-title">
                单号与接件
            </span>
            <div class="row" style="margin-top: 20px">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-1" style="font-size: 12px;font-weight: normal;text-align: right;">主题:</label>
                        <div class="col-md-10">${info.mtTheme}&nbsp;</div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">接件日期:</label>
                        <div class="col-md-10">${info.mtDate}&nbsp;</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">负责人:</label>
                        <div class="col-md-10">${info.tbUserByUserId.userName}&nbsp;</div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">客户:</label>
                    <div class="col-md-10">${info.tbCustomerByCusId.cusName}&nbsp;</div>
                </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">手机：</label>
                        <div class="col-md-10">${info.mtTel}&nbsp;</div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;"> 姓名：</label>
                    <div class="col-md-10">${info.mtName}&nbsp;</div>
                </div>
                <hr class="border-t-a">
            </div>
        </div>
            <span class="modal-title">
接件详情
            </span>
        <div class="row" style="margin-top: 20px">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">维修产品:</label>
                    <div class="col-md-10">${title}&nbsp;</div>
                </div>
                <hr class="border-t-a">
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">产品生产日期:</label>
                    <div class="col-md-10">${info.mtProDate}&nbsp;</div>
                </div>
                <hr class="border-t-a">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;"> 保修:</label>
                    <div class="col-md-10">${info.mtIsProtect==0?'在保':'出保'}&nbsp;</div>
                </div>
                <hr class="border-t-a">
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">故障描述:</label>
                    <div class="col-md-10">${info.mtDescript}&nbsp;</div>
                </div>
                <hr class="border-t-a">
            </div>
        </div>
            <span class="modal-title">
费用与执行
            </span>
        <div class="row" style="margin-top: 20px">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;"> 费用:</label>
                    <div class="col-md-10">${info.mtPay}&nbsp;</div>
                </div>
                <hr class="border-t-a">
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">约定交付日期:</label>
                    <div class="col-md-10">${info.mtCompleteDate}&nbsp;</div>
                </div>
                <hr class="border-t-a">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;"> 状态:</label>
                    <div class="col-md-10">${info.mtState}&nbsp;</div>
                </div>
                <hr class="border-t-a">
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">备注:</label>
                    <div class="col-md-10">${info.mtExtract}&nbsp;</div>
                </div>
                <hr class="border-t-a">
            </div>
        </div>
            </div>
        </div>
    </div>
    <div class="row" style="text-align: center;background-color: #F4F8FB;">
        <div class="col-md-12">
            <div class="form-group">
                <footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
                    <a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
                    <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
                    <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
                    <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>
            </div>
        </div>
    </div>

    <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript " src="/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>
    <script type="text/javascript " src="/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min.js"></script>

    <script type="text/javascript">
        <%--$(function () {--%>
            <%--ajax("/saleAfter/loadProByOrderId?id=" + ${info.orderEntity.orderId}, function (data) {--%>
                <%--var group = $('#to_pro');--%>
                <%--for (var i = 0; i < data.length; i++) {--%>
                    <%--group.append('<option value="' + data[i].formatterId + '">' + data[i].title + '</option>')--%>
                <%--}--%>
                <%--$('#to_pro').selectpicker('refresh');--%>
                <%--$('#to_pro').selectpicker('render');--%>
                <%--$('#to_pro').selectpicker('val',pfId);--%>
            <%--});--%>
        <%--}--%>
        $("#model1").load("pur_model1.html");
        $("#model2").load("pur_model2.html");
        $("#model3").load("pur_model3.html");
        $(".form_datetime").datetimepicker({
            format: "yyyy年mm月dd日 ",
            minView: "month", //设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            pickerPosition: "bottom-left"
        });
    </script>
</body>
</html>
