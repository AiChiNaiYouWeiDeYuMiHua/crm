<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-14
  Time: 下午3:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>编辑订单明细</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <!-- [if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![end if]-->
</head>
<body>
<style type="text/css">
    .mybtn{position:fixed; right:50px; bottom:50px; display:block;
        border-radius:50%; padding:20px;
        text-align:center; line-height:40px;}
    .modal.left .modal-dialog,
    .modal.right .modal-dialog {
        position: fixed;
        margin: auto;
        width: 500px;
        height: 95%;
        -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
        -o-transform: translate3d(0%, 0, 0);
        transform: translate3d(0%, 0, 0);
    }

    .modal.left .modal-content,
    .modal.right .modal-content {
        height: 100%;
        overflow-y: auto;
    }

    .modal.left .modal-body,
    .modal.right .modal-body {
        padding: 15px 15px 80px;
    }

    /*Left*/
    .modal.left.fade .modal-dialog{
        left: -320px;
        -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
        -o-transition: opacity 0.3s linear, left 0.3s ease-out;
        transition: opacity 0.3s linear, left 0.3s ease-out;
    }

    .modal.left.fade.in .modal-dialog{
        left: 0;
    }

    /*Right*/
    .modal.right.fade .modal-dialog {
        right: -500px;
        top: 2.5%;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    .modal.right.fade.in .modal-dialog {
        right: 2%;
    }

    /* ----- MODAL STYLE ----- */
    .modal-content {
        border-radius: 5px;
        border: none;
    }

    .modal-header {
        border-bottom-color: #EEEEEE;
        background-color: #FAFAFA;
    }

</style>
<!--点击右侧弹出-->
<a href="javascript:;" class="btn btn-danger mybtn" data-toggle="modal" data-target="#myModal"><i class="fa fa-user"></i><span>产品</span></a>
<!-- 弹出层 modal -->
<div class="modal right fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="modal" style="overflow: hidden">
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <p>产品树图示说明&nbsp;&nbsp; <span class="fa fa-shopping-cart"></span> : 计算库存&nbsp;&nbsp;
                <span class="fa fa-comment-o"></span> : 查看产品详情</p>
        </div>
        <div class="col-md-12">
            订单详细 致 ： <span style="font-weight:normal;color:#9e9e9e">〖</span>${info.tbCustomerByCusId.cusName}
            <a href="/customer_detail?id="+${info.tbCustomerByCusId.cusId} >
                <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>
        </div>
        <div class="col-md-12" style="margin-top: 20px">
            <form id="save-form" action="/sale/order/detail/save" method="post">
                <input type="hidden" name="id" value="${info.orderId}">
                <table id="table"></table>
            </form>

        </div>
        <div class="col-md-12 text-center" style="margin-top: 20px">
            <button class="btn btn-lg " id="save">保持明细数据</button>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-treeview.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<!-- Latest compiled and minified Locales -->
<script src="/js/sweetalert.min.js"></script>
<script>
    $(function () {
        initTable();
        sumitForm();
        $('#modal').load('/sale/chooseProduct')
    })
    function initTable() {
        $("#table").bootstrapTable({
            method: 'get',
            striped: true,
            showExport:true,
            cache: false,
            sortable: false,
            sortOrder: "desc",
            uniqueId:"formatterId",
            url: "/sale/order/detail?id=${info.orderId}",
            exportOptions:{
                fileName:$('#table-title').text(),
                ignoreColumn:[7]
            },
            showFooter:true,
            columns: [{
                field:'title',
                title:'品名型号/规格[单位]',
                align:'center',
                valign: 'middle',
                footerFormatter:"合计",
                formatter:function (value, row, index) {
                    return [
                        value+'<input class="form-control" name="datas['+index+'].formatEntity.pfId" type="hidden" value="'+row.formatterId+'">'
                        <%--'<input class="form-control" name="datas['+index+'].tbOrderByOrderId.orderId" type="hidden" value="${info.orderId}">'--%>
                    ]
                }

            },{
                field:'price',
                title:'单价',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                    return [
                        '<input class="form-control" disabled id="price'+index+'" type="number" value="'+value+'">'
                    ]
                }
            },{
                field:'number',
                title:'数量',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                    return [
                        '<input class="form-control" name="datas['+index+'].odNumber" id="number'+index+'"type="number" value="'+value+'">'
                    ]
                },
                footerFormatter:sum
            },{
                field:'discount',
                title:'折扣',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                    var d = 100;
                    if (row.number*row.price != 0)
                        d = 100*(row.total/(row.number*row.price));
                    return [
                        '<div class="input-group">' +
                        '<input class="form-control" id="discount'+index+'" type="number" value="'+d+'">' +
                        '<span class="input-group-addon">%</span></div>'
                    ]
                }
            },{
                field:'total',
                title:'金额',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                    return [
                        '<input class="form-control" name="datas['+index+'].odTotal" id="total'+index+'" type="number" value="'+value+'">'
                    ]
                },
                footerFormatter:sum
            },{
                field:'other',
                title:'备注',
                align:'center',
                valign: 'middle',
                formatter:function (value, row, index) {
                    if (value == null)
                        value = "";
                    return [
                        '<textarea class="form-control" name="datas['+index+'].odOther" id="other'+index+'" rows="1"  style="resize: vertical" >'+value+'</textarea  >'
                    ]
                }
            },{
                field:'formatterId',
                title:'操作',
                align:'center',
                formatter:function (value, row, index) {
                    return [
                        '<a class="btn" href="javascript:;" onclick="deleteDetail('+row.formatterId+')"><i class="fa fa-trash"></i></a>'
                    ]
                }
            }],
            onLoadSuccess:function (data) {
                change();
            },
            onLoadError:function (status) {

            }
        })
    }
    function sum(data) {
        field = this.field;
        return data.reduce(function(sum, row) {
            return sum + (+row[field]);
        }, 0);
    }
    function change() {
        for (var i =0 ;i<$('#table').bootstrapTable('getData','useCurrentPage').length;i++) {
            $('#number'+i).blur((function blur(index) {
                return function () {
                    if ($('#number'+index).val().length<=0||parseFloat($('#number'+index).val())<=0) {
                        $('#number' + index).val(0)
                    }
                    $('#total' + index).val(parseFloat($('#number' + index).val()) * parseFloat($('#price' + index).val()) *
                        parseFloat($('#discount' + index).val())/100)
                    update(index,$('#total'+index).val(),$('#number'+index).val());
                }
            })(i))
            $('#price'+i).blur((function blur(index) {
                return function () {
                    if ($('#price'+index).val().length<=0||parseFloat($('#price'+index).val())<=0) {
                        $('#price' + index).val(0)
                    }
                    $('#total' + index).val(parseFloat($('#number' + index).val()) * parseFloat($('#price' + index).val()) *
                        parseFloat($('#discount' + index).val())/100)
                    update(index,$('#total'+index).val(),$('#number'+index).val());
                }
            })(i))
            $('#discount'+i).blur((function blur(index) {
                return function () {
                    if ($('#discount'+index).val().length<=0||parseFloat($('#discount'+index).val())<=0) {
                        $('#discount' + index).val(100)
                    }
                    $('#total' + index).val(parseFloat($('#number' + index).val()) * parseFloat($('#price' + index).val()) *
                        parseFloat($('#discount' + index).val())/100)
                    update(index,$('#total'+index).val(),$('#number'+index).val());
                }
            })(i))
            $('#total'+i).blur((function blur(index) {
                return function () {
                    if ($('#total'+index).val().length<=0||parseFloat($('#total'+index).val())<=0) {
                        $('#total' + index).val(0)
                    }
                    $('#discount' + index).val(parseFloat($('#total' + index).val()) / parseFloat($('#price' + index).val()) /
                        parseFloat($('#discount' + index).val()))
                    update(index,$('#total'+index).val(),$('#number'+index).val());
                }
            })(i))
        }
    }
    function sumitForm() {
        //提交表单数据
        $('#save').click(function () {
                $("#save-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            })
    }
    function deleteDetail(id) {
            swal({
                    title: "确定删除吗？",
                    text: "你即将删除该订单明细！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定删除！",
                    closeOnConfirm: false
                },
                function(){
                $('#table').bootstrapTable('removeByUniqueId', id);
                    swal("删除！", "一条订单详情已经被删除,请保存明细数据", "success");
                });
    }
    function successForm(){
        window.close();
        window.opener.history.go(0);
    }
    function selectProduct(title,id,price,stock) {
        var datas = $('#table').bootstrapTable("getData",true);
        for (var i = 0; i < datas.length; i++) {
            if (datas[i].formatterId == id) {
                swal("添加重复", "订单详情中已经有该产品", "error");
                return;
            }
            if (stock){
                swal("添加错误", "不需要计算库存的产品，请选择合同", "error")
                return;
            }
        }
        $('#table').bootstrapTable('append', {
            title:title,
            "formatterId":id,
            "price":price,
            "number":1,
            "total":price
        }
        );
        change();
    }
    function update(index,total,number) {
        $('#table').bootstrapTable("updateCell",{
            index:index,
            field:'total',
            value:total
        })
        $('#table').bootstrapTable("updateCell",{
            index:index,
            field:'number',
            value:number
        })
        change()
    }
</script>
</body>
</html>
