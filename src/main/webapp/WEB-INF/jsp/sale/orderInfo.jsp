<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="simple" uri="http://went.top/tags" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/7
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>订单：${info.orderTitle}〖${info.tbCustomerByCusId.cusName}〗</title>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css" />
</head>
<style type="text/css">
    html,
    body {
        color: #333333;
        background-color: #F4F8FB;
        font-size: 12px;
        font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .table-t thead{
        background-color: #EBEFF2;font-family: 楷体;
    }
    a{
        color: #505458;
    }
    .text-gray{
        color: #999;
    }
    .m-20{
        margin: 20px;
    }
    .m-l-10{
        margin-left: 10px;
    }
    .m-b-10{
        margin-bottom: 10px;
    }
    .m-b-30{
        margin-bottom: 30px;
    }
    .icon-box{
        display: inline-block;
        min-width: 100px;
    }
    .border-r{
        border-right: 1px solid #ccc;
    }
    a:hover{text-decoration: none}
    .panel-title{
        font-size: 12px;
    }
    .fixed-table-body{
        height: auto;
    }
    .m-l-5{
        margin-left: 5px;
    }
    .m-b-10{
        margin-bottom: 10px;
    }
    .m-r-5{
        margin-right: 5px;
    }
</style>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="font-size: 12px ">
          <simple:approval entity="${info.orderId}" status="${info.orderOk}" type="订单"></simple:approval>
        </div>
    </div>
    <div class="row" style="background-color: white;padding: 10px;">
        <div id="pro-header">
            <div class="pull-right" style="font-size: 12px;">
                <c:if test="${info.orderOk == 0}">
                    <g:g id="242">
                <button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" onclick="deletePro()" data-target="#delModal">
                    <i class="fa fa-trash-o" style="margin-right: 5px;"></i>删除
                </button>&nbsp;
                    </g:g>
                    <g:g id="243">
                <button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.orderId},${info.orderType})">
                    <i class="fa fa-pencil" style="margin-right: 5px;"></i>编辑
                </button>&nbsp;
                    </g:g>
                </c:if>
            </div>
            <h4>
                <span>订单</span>
                <a target="_blank" href="??" style="color: #999999;" data-toggle="tooltip" data-placement="bottom" title="数据日志">
                    <i class="fa fa-bars"></i>
                </a>
            </h4>
            <hr style="border-top: 1px solid #aaaaaa;" />
        </div>
        <div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">订单：</label>
                        <div class="col-md-10">${info.orderTitle}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">对应客户：</label>
                        <div class=" col-md-10"><span style="font-weight:normal;color:#9e9e9e">〖</span>${info.tbCustomerByCusId.cusName}
                            <a href="/customer_detail?id="+${info.tbCustomerByCusId.cusId} target="_blank">
                                <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">对应机会：</label>
                        <div class=" col-md-10"><span style="font-weight:normal;color:#9e9e9e"></span>${info.tbQuoteByQuoteId.quoteTheme}

                            <span style="font-weight:normal;color:#9e9e9e"></span></div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">订单编号：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.orderNumber}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">分类</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.orderCategory}</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">总金额：</label>
                    <div class="col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.orderTotal}" type="currency"></fmt:formatNumber>
                            <c:if test="${info.orderTotal != info.total}">
                            <i class="fa fa-warning pull-right" data-toggle="toolip" data-placement="bottom"
                            title="总金额和明细金额不一致,明细金额<fmt:formatNumber value="${info.total}" type="currency"></fmt:formatNumber>" style="color: red"></i>
                            </c:if>
                        </p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">付款方式：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.orderPayWay}</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">发货金额：</label>
                    <div class="col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.sendMoney}" type="currency"></fmt:formatNumber>
                           </p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">回款金额：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.returnMoney}" type="currency"></fmt:formatNumber></p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">毛利：</label>
                    <div class="col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.maori}" type="currency"></fmt:formatNumber>
                            <i class="fa fa-info-circle pull-right" data-toggle="toolip" data-placement="bottom"
                               title="可以不输入，由订单明细自动合计"></i></p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">开发票金额：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.billMOney}" type="currency"></fmt:formatNumber></p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">预计毛利：</label>
                    <div class="col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.forecastMaori}" type="currency"></fmt:formatNumber>
                            <i class="fa fa-info-circle pull-right" data-toggle="toolip" data-placement="bottom"
                               title="可以不输入，由订单明细自动合计"></i></p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">开始时间：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.orderDate}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">结束时间：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.orderLatestDate}</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">客户签约人:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.contactsEntity.cotsName}&nbsp;</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">发货:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.sendGoods}&nbsp;</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">所有者:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.tbUserByUserId.userName}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">退货:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.returnGoods}</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">状态:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><c:choose>
                            <c:when test="${info.orderStatus == 0}">
                                执行中<i data-toggle="tooltip" data-placement="bottom"
                                title="执行中" class="f-s-16 fa fa-arrow-right text-primary"></i>
                            </c:when>
                            <c:when test="${info.orderStatus == 1}">
                                结束<i data-toggle="tooltip" data-placement="bottom"
                                title="结束" class="f-s-16 fa fa-check text-primary"></i>
                            </c:when>
                            <c:when test="${info.orderStatus == 2}">
                                意外终止<i data-toggle="tooltip" data-placement="bottom"
                                title="意外终止" class="f-s-16 fa fa-remove text-primary"></i>
                            </c:when>
                        </c:choose>
                            <g:g id="243">
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#changeStatus" style="margin-left: 30px">
                                <i data-toggle="tooltip"  title="编辑状态"
                                   class="fa fa-pencil" data-placement="bottom"></i>
                            </a>
                            </g:g>
                        </p>

                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">结款方式:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">${info.orderKnotWay}</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">项目:</label>
                        <div class=" col-md-10">${info.projectEntity.projTheme}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">订单明细:</label>
                    </div>
                </div>
                <div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-12" style="font-size: 12px;font-weight: normal;text-align: right;">
                                <c:if test="${info.orderOk == 0}">
                                    <g:g id="243">
                                <a class="f-s-12 text-black" target="_blank" href="/sale/order/detail/${id}/"  style="margin-right: 10px;color: black;text-decoration: none;"><i class="fa fa-edit"></i>编辑订单明细</a>
                                    </g:g>
                                </c:if>
                                <g:g id="141">
                                    <g:g id="211">
                                <a style="FONT-WEIGHT: normal; COLOR: #c69; TEXT-DECORATION: none" href="javascript:void(0);" onclick="returnGoods1()"style="text-decoration: none;"><i class="fa fa-arrow-circle-right"></i>办理退货</a>
                                    </g:g>
                                </g:g>
                            </label>
                        </div>
                    </div>
                    <div id="orderdetail"></div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">备注:</label>
                        <div class=" col-md-10">${info.orderOther}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">审批状态:</label>
                        <div class=" col-md-10"><c:choose>
                            <c:when test="${info.orderOk == 1}">
                                待审<i data-toggle='tooltip' data-placement='bottom' title='待审' class='f-s-16 fa fa-coffee m-l-5'></i>
                            </c:when>
                            <c:when test="${info.orderOk == 2}">
                                同意<i data-toggle='tooltip' data-placement='bottom' title='同意' class='f-s-16 fa fa-check-circle-o m-l-5'></i>
                            </c:when>
                            <c:when test="${info.orderOk == 3} ">
                                否决<i data-toggle='tooltip' data-placement='bottom' title='否决' class='f-s-16 fa fa-times-circle m-l-5'></i>
                            </c:when>
                            <c:otherwise>
                                待申请<i data-toggle='tooltip' data-placement='bottom' title='待申请' class='f-s-16 fa fa-flickr m-l-5'></i>
                            </c:otherwise>
                        </c:choose></div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">审批记录:</label>
                        <div class=" col-md-10">
                            <div class="row">
                            <c:forEach items="${approvals}" var="approval" varStatus="status">
                                <div class="col-md-2">
                                    第${status.count}次:
                                </div>
                                <div class="col-md-10">
                                <c:forEach items="${approval.tbApprovalLogsByApprovalId}" var="log">
                                    <c:if test="${log.tbUserByUserId !=null}">
                                        <span>${log.tbUserByUserId.userName}</span></c:if>
                                    &nbsp;&nbsp;
                                    <span>${log.category}</span>
                                    &nbsp;&nbsp;<span>${log.logsContent}</span>&nbsp;&nbsp<span>${log.logsTime}</span>
                                    <br>
                                </c:forEach>
                                </div>
                            </c:forEach>
                            </div>
                        </div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class=" col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">收货人/地址:</label>
                        <div class=" col-md-10">
                            <div class="panel-group" id="show_contract_address">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title" style="width: 100%">
                                            <a data-toggle="collapse" style="display: block;" data-parent="#show_contract_address" href="#contract_address" aria-expanded="true" class="">
                                                详细信息
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="contract_address" class="panel-collapse collapse in" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <p><b>收货人</b></p>
                                            <div class="row  form-horizonta">
                                                <div class="col-md-12 form-group ">
                                                    <label class="control-label col-md-3">姓名:</label>
                                                    <div class="col-md-9">${info.tbAddressByAddressId.addressName}</div>
                                                </div>
                                                <div class="col-md-12 form-group ">
                                                    <label class="control-label col-md-3">联系方式:</label>
                                                    <div class="col-md-9">${info.tbAddressByAddressId.addressTel}</div>
                                                </div>
                                            </div>
                                            <p>
                                                <b>地址</b>
                                            </p>

                                            <div class="row  form-horizonta">
                                                <div class="col-md-12 form-group" id="id_state">
                                                    <label class="control-label col-md-3" id="dt_sendgoods_country_3">省</label>
                                                    <div class="col-md-9">${info.tbAddressByAddressId.addressProvince}</div>
                                                </div>

                                                <div class="col-md-12 form-group" id="dt_sendgoods_country_5">
                                                    <label class="control-label col-md-3" id="dt_sendgoods_country_4">市</label>
                                                    <div class="col-md-9">${info.tbAddressByAddressId.addressCity}</div>
                                                </div>
                                                <div class="col-md-12 form-group" id="dt_sendgoods_country_7">
                                                    <label class="control-label col-md-3" id="dt_sendgoods_country_6">县</label>
                                                    <div class="col-md-9">${info.tbAddressByAddressId.addressCounty}</div>
                                                </div>
                                                <div class="col-md-12 form-group ">
                                                    <label class="control-label col-md-3">地址:</label>
                                                    <div class="col-md-9">${info.tbAddressByAddressId.addressContent}</div>
                                                </div>
                                                <div class="col-md-12 form-group ">
                                                    <label class="control-label col-md-3">邮编:</label>
                                                    <div class="col-md-9">${info.tbAddressByAddressId.addressCode}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 50px">
                    <div class="form-group text-center">
                        <span class="text-left " style="cursor: pointer; padding: 8px 12px; border-radius:3px; background: #dddddd;margin:10px 0px; ">
					<i class="fa  fa-file"></i><span>上传附件</span>
				</span>
                    </div>
                </div>
        </div>

    </div>
        <div id="pro-bottom" class="text-right">
<c:if test="${info.orderOk == 0}">
    <g:g id="242">
        <button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" onclick="deletePro()" data-target="#delModal">
            <i class="fa fa-trash-o" style="margin-right: 5px;"></i>删除
        </button>&nbsp;
    </g:g>
    <g:g id="243">
        <button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.orderId},${info.orderType})">
            <i class="fa fa-pencil" style="margin-right: 5px;"></i>编辑
        </button>&nbsp;
    </g:g>
</c:if>
        </div>

    </div>
    <c:if test="${info.orderOk == 2}">
        <g:g id="281">
    <div class="row" style="margin-top:20px;">
        <div class="text-center m-b-10 hidden" id="outGoods"><a style="font-size: 14px;font-weight: 400;margin-top: 20px;margin-bottom: 20px" class="btn btn-primary" href="/sale/warehouse/order?id=${info.orderId}" target="_blank" >
            <i class="fa fa-home m-r-5" style="padding-bottom: 5px;padding-top: 5px"></i>出库:点此生成出库单，由库管操作出库</a>
        </div>
    </div>
        </g:g>
    </c:if>
    <c:if test="${sends!=null&& fn:length(sends) >0}">
    <div class="row" style="background-color: white;" id="return">
        <div class="col-md-12">
            <div class="form-group" style="padding-top: 10px;padding-left: 5px">
                <b><i class="fa fa-mail-reply" style="padding-right: 5px"></i>订单已交付明细/发货单</b>
            </div>
        </div>
        <c:forEach var="send" items="${sends}" varStatus="status">
        <div class="col-md-12">
            <div class="form-group">
                <span class="col-md-12" style="text-align: center;font-size: 12px;margin-bottom: 5px;">发 货 单</span>
            </div>
        </div>
        <div class="col-md-12">
            <div class="">
                <div class="col-md-4" style="color: black;font-size: 12px;margin-bottom: 5px;">
                    <span>发货号:</span><span style="color: darkgray;">${send.dgNumber}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                </div>
            </div>
            <div class="">
                <div class="col-md-4" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                    <span>日期:</span><span style="color: darkgray;"><fmt:formatDate value="${send.dgDate}" pattern="yyyy-MM-dd"></fmt:formatDate> </span> &nbsp;&nbsp;&nbsp;&nbsp;
                </div>
            </div>
            <div class="">
            <div class="col-md-4" style="text-align: right;color: black;font-size: 12px;margin-bottom: 5px;">
                <span>状态:</span><span style="color: darkgray;">
                <c:choose>
                    <c:when test="${send.status==1}">
                        待发货
                    </c:when>
                    <c:when test="${send.status==2}">
                已发货
            </c:when>
            <c:otherwise>已签收</c:otherwise></c:choose></span> &nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </div>
            <div class="">
            <div class="col-md-4" style="color: black;font-size: 12px;margin-bottom: 5px;">
                <span>发货人:</span><span style="color: darkgray;">${send.userName}</span> &nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </div>
            <div class="">
            <div class="col-md-4" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                <span>收货人:</span><span style="color: darkgray;">${send.tbAddressByAddressId.addressName}</span> &nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </div>
            <div class="">
                <div class="col-md-4" style="text-align: right;color: black;font-size: 12px;margin-bottom: 5px;">
                    <span>发货方式:</span><span style="color: darkgray;">${send.dgModel}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                </div>
            </div>
            <div class="form-group" >
                <div class="col-md-12" style="margin-bottom: 10px;margin-top: 10px">
                    <table id="table${status.index}" class="table-t" style="font-size: 12px">
                            <thead style="background-color: #EBEFF2;font-family: 楷体;">
                            <tr>
                            </tr>
                            </thead>
                    </table>

                </div>
            </div>
        </div>
        </c:forEach>
    </div>
    </c:if>
    <c:forEach items="${returnGoodsVOS}" var="returnGoodsVOS1">
        <div class="row" style="background-color: white;" id="return">
            <div class="col-md-12">
                <div class="form-group">
                    ￥退货单明细/退货单
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <span class="col-md-12" style="text-align: center;font-size: 12px;margin-bottom: 5px;">退 货 单</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                        <span>应退款:</span><span style="color: darkgray;">￥${returnGoodsVOS1.planMoney}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span>已退款:</span><span style="color: darkgray;">￥${returnGoodsVOS1.toMoney}</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                            <%--<span>退货:</span><span style="color: darkgray;">${returnGoodsVOS1.putState}</span> &nbsp;&nbsp;&nbsp;&nbsp;--%>
                        <span>单据状态:</span><span style="color: darkgray;"
                                                id="${returnGoodsVOS1.rgId}1">${returnGoodsVOS1.rgState}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                        <span>主题：</span><span style="color: darkgray;">${returnGoodsVOS1.rgTheme}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span>退货单号：</span><span style="color: darkgray;">${returnGoodsVOS1.rgOddNumbers}</span></label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                        <span>经办人：</span><span style="color: darkgray;">${returnGoodsVOS1.userName}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span>仓库：</span><span style="color: darkgray;">${returnGoodsVOS1.whName}</span> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span>日期：</span><span style="color: darkgray;"><fmt:formatDate
                            value="${returnGoodsVOS1.rgReturnTime}" pattern="yyyy-MM-dd"/></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" style="text-align: center;color: black;font-size: 12px;margin-bottom: 5px;">
                        <g:g id="142">
                        <span>操作：</span>
                        <button type="button" class="btn" id="${returnGoodsVOS1.rgId}2">撤销退货</button> &nbsp;&nbsp;&nbsp;&nbsp;
                        </g:g>
                    </div>
                    <br/><br/><br/>
                </div>
                <br/><br/><br/>
            </div>
            <div id="${returnGoodsVOS1.rgId}"></div>
        </div>
    </c:forEach>
       <div class="row" style="margin-top:20px;display: flex">
           <div class="col-md-6">
               <div class="panel  panel-default" style="padding: 0;border: 0">
                   <div class="panel-heading" style="background-color: white;border: none">
                       <h5 class="panel-title">
                           <i class="fa fa-list-ol"></i>
                           回款计划
                           <g:g id="151">
                           <a href="javascript:void(0);" id="add-detail">
                               <i class="fa fa-plus"></i>
                           </a>
                           </g:g>
                       </h5>
                   </div>
                   <div class="panel-body">
                       <c:forEach items="${moneyPlan}" var="plan">
                       <p>
                           <small class="pull-right"><fmt:formatDate value="${plan.ppbDate}" pattern="yyyy-MM-dd"></fmt:formatDate></small>
                           <i class="fa fa-square text-gray m-r-5"></i>
                           <a class="text-black" target="_blank" href="/sale/ppbdetail1/${plan.ppbId}/">
                               <span style="white-space:nowrap"><span style="font-size:9pt;font-weight:normal;">￥</span><fmt:formatNumber value="${plan.ppbMoney}"></fmt:formatNumber> </span>
                           </a>
                           &nbsp;第${plan.ppbTerms}期&nbsp;
                       </p>
                       </c:forEach>
                   </div>
               </div>
               <div class="panel  panel-default" style="padding: 0;border: 0">
                   <div class="panel-heading" style="background-color: white;border: none">
                       <h5 class="panel-title">
                           <i class="fa fa-book"></i>
                           回款记录
                           <g:g id="161">
                           <a href="javascript:void(0);" id="add-send-modal">
                               <i class="fa fa-plus"></i>
                           </a>
                           </g:g>
                       </h5>
                   </div>
                   <div class="panel-body">
                       <c:forEach items="${moneyLogs}" var="log">
                           <p>
                               <small class="pull-right"><fmt:formatDate value="${log.pbdDate}" pattern="yyyy-MM-dd"></fmt:formatDate></small>
                               <i class="fa fa-square text-gray m-r-5"></i>
                               <a class="text-black" target="_blank" href="/sale/pbddetail1/${log.pbdId}/">
                                   <span style="white-space:nowrap"><span style="font-size:9pt;font-weight:normal;">￥</span><fmt:formatNumber value="${log.pbdMoney}"></fmt:formatNumber> </span>
                               </a>
                               &nbsp;第${log.pbdTerms}期&nbsp;
                           </p>
                       </c:forEach>
                   </div>
               </div>
           </div>
           <div class="col-md-6 panel  panel-default" style="padding: 0;border: 0">
               <div class="panel-heading" style="background-color: white;border: none">
                   <h5 class="panel-title">
                       <i class="fa fa-book"></i>
                       开票记录
                       <g:g id="171">
                       <a href="javascript:void(0);" id="add-money">
                           <i class="fa fa-plus"></i>
                       </a>
                       </g:g>
                   </h5>
               </div>
               <div class="panel-body">
                   <c:forEach items="${money}" var="m">
                       <p>
                           <small class="pull-right"><fmt:formatDate value="${m.rrDate}" pattern="yyyy-MM-dd"></fmt:formatDate></small>
                           <i class="fa fa-square text-gray m-r-5"></i>
                           <a class="text-black" target="_blank" href="/sale/rrdetail1/${m.rrId}/">
                               <span style="white-space:nowrap"><span style="font-size:9pt;font-weight:normal;">￥</span><fmt:formatNumber value="${m.rrMoney}"></fmt:formatNumber> </span>
                           </a>
                           &nbsp;票据类型:${m.rrType}&nbsp;
                           &nbsp;票据内容:${m.rrContent}&nbsp;
                       </p>
                   </c:forEach>
               </div>
           </div>
       </div>

<div class="row" style="text-align: center;background-color: #F4F8FB;">
    <div class="col-md-12">
        <div class="form-group">
            <footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
                <a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
                <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
                <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
                <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>
        </div>
    </div>
</div>
        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content mymodalcontent">
                </div>
            </div>
        </div>
    <div class="modal fade" id="changeStatus" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content mymodalcontent">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal" aria-hidden="true">
                        &times;            </button>
                    <h4 class="modal-title" id="myModalLabel">
                        编辑状态            </h4>
                </div>
                <div class="modal-body">
                    <p style="margin: 10px 5px">修改合同/订单状态:</p>
                    <form method="post" action="/sale/order/status" class="form-horizontal" id="status">
                        <input type="hidden" name="id" value="${info.orderId}">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>状态：
                            </label>
                            <div class="col-md-8">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="0" name="orderStatus" checked>
                                    <label> 执行中 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="1" name="orderStatus" >
                                    <label> 结束 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="2" name="orderStatus" >
                                    <label> 意外终止 </label>
                                </div>
                            </div>
                        </div>
                        <div class="text-center"><button class="btn" id="status-btn">保存</button></div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
        <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
        <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
        <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
        <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<script src="/js/tableExport.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
        <!--excel-->
        <%--<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>--%>
        <script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
        <script src="/js/sweetalert.min.js"></script>
        <script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
        <script src="https://cdn.bootcss.com/bootstrap-validator/0.5.3/js/language/zh_CN.min.js"></script>
        <script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="/js/sale/formatter.js"></script>
<script src="/js/sale/sendDetail.js"></script>
<script src="/js/validator/order.js"></script>
<script src="/js/sale/returngoods.js"></script>
<script type="text/javascript">
    $(function () {
        $("[data-toggle='tooltip' ] ").tooltip();
        $('#changeStatus').on('show.bs.modal',function () {
            $('#changeStatus input[name="orderStatus"]').eq(${info.orderStatus}).prop("checked","checked");
        })
        sumitStatus();
        $('#orderdetail').load("/sale/orderDetail?id=${info.orderId}");
        //消除模态框数据
        $("#add").on("hidden.bs.modal", function () {
            open_modal = null;
            $(this).removeData("bs.modal");
        });
        $('#add-detail').click(function () {
            $('#add').modal({
                remote:'/sale/ppbaddmodal?orderId=${info.orderId}'
            });
        })
        $('#add-send-modal').click(function () {
            $('#add').modal({
                remote:'/sale/pbdaddmodal1?orderId=${info.orderId}'
            });
        })
        $('#add-money').click(function () {
            $('#add').modal({
                remote:'/sale/rrAddModal?orderId=${info.orderId}'
            });
        })
        <%--$('#add-fapiao').click(function () {--%>
            <%--$('#add').modal({--%>
                <%--remote:'/sale/purrAddModal1?purId=${info.orderId}'--%>
            <%--});--%>
        <%--})--%>
        <c:forEach var="send" items="${sends}" varStatus="status">initSendTable(${status.index},${send.dgId});</c:forEach>
        <c:forEach var="send" items="${returnGoodsVOS}" varStatus="status">returnGoods(${send.rgId},${status.index});</c:forEach>
    });
    function edit(id,b) {
        open_modal = id;
        if (!b){
            $('#add').modal({
                remote:'/sale/contract/add'
            });
        } else {
            isedit = true;
            $('#add').modal({
                remote:'/sale/order/add'
            });
        }
    }
    //办理退货
    function returnGoods1() {
        if ($("#mark").val() == "待处理") {
            swal(
                '请先处理待处理的退货单！'
            )
        } else {
            var b = "/sale/rgAddModal1?orderId=" +${info.orderId};
            $('#add').modal({
                remote: b
            })
        }
    }
    function successForm() {
        $('#add').modal('hide')
        window.history.go(0)
    }
    function deletePro() {
        swal({
                title: "确定删除吗？",
                text: "你将无法恢复该合同！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function(){
                ajax("/sale/order/delete",function () {
                    swal("成功！", "合同删除成功！","success");
                    window.close();
                },function(){},'post',data={'id':${info.orderId}});
            });


    }
    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                    error()
                }
            }
        })
    }
    function sumitStatus() {
        //提交表单数据
        $('#status-btn').click(function () {
                $("#status").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
        })
    }
    function returnGoods(id,index) {
        var a = "wsjTable" + index;
        var b = id;
        var c = "#" + id;
        var d = "#" + id + "1";
        var e = "#" + id + "2";
        $(c).append("<div class=\"form-group\">\n" +
            "                <div class=\"col-md-12\">\n" +
            "                    <table id=" + a + " class=\"table-t\" style='font-size: 12px'>\n" +
            "\n<thead style=\"background-color: #EBEFF2;font-family: 楷体;\"></thead>" +
            "                    </table>\n" +
            "\n" +
            "                </div>\n" +
            "            </div>");
        var f="ruku("+id+")";
        var v="/sale/addPrModel1?rgId="+id;
        var w="returnMoney("+id+")";
        if ($(d).html() == "待处理") {
            $("#mark").val("待处理");
            $(c).append(" <div class=\"text-center m-b-10 \" id=\"outGoods\"><a style=\"font-size: 14px;font-weight: 400;margin-top: 20px;margin-bottom: 20px\" class=\"btn btn-primary\" onclick="+f+" >\n" +
                "            <i class=\"fa fa-home m-r-5\" style=\"padding-bottom: 5px;padding-top: 5px\"></i>入库:生成退货入库单</a>" +
                "<a style=\"font-size: 14px;font-weight: 400;margin-top: 20px;margin-bottom: 20px;margin-left:20px\" class=\"btn btn-primary\" onclick="+w+" >" +
                "          <i class=\"fa fa-home m-r-5\" style=\"padding-bottom: 5px;padding-top: 5px\"></i>新建退款</a>\n" +
                "            </div>")
        } else {
        }
        initTable1(a, id);
        $(e).click(function () {
            swal({
                    title: "撤销退货",
                    text: "确定要撤销退货吗？",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                },
                function () {
                    var url = "/sale/rgdelete?id=" + b;
                    ajax(url, function (data) {
                        window.history.go(0)
                    }, function (msg) {
                        $('#wsj-add').modal('hide')
                    }, "get", {})
                });
        })
    }
</script>
</body>
</html>
