<%--
  产品table
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>交付记录/发货明细</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css" />
    <style type="text/css">
        #order_top {
            height: 60px;
        }

        #order_top ul {
            border-bottom: solid gainsboro 1px;
        }

        .ul_i {
            border-bottom: solid black 2px;
        }


        thead tr th {
            border-bottom: 2px solid #a7b7c3;
            background-color: #ebeff2;
        }

        body {
            font-family: "microsoft yahei";
            padding: 20px;
            background-color: #f4f8fb;
        }

        strong {
            color: black;
        }

        #content {
            top: -150px;
        }

        .fixed-table-container {
            /*top: -10px;*/
        }

        .columns {
            top: -45px;
        }

        .fixed-table-pagination {
            position: absolute;
            width: 100%;
            text-align: center;
            /*margin-top: -10px;*/
        }

        .pagination {
            margin-right: 49%;
        }

        .pagination {}

        thead {
            background-color: #269ABC;
            color: #000000;
            font-size: 12px;
        }

        tbody {
            font-size: 13px;
            color: #000000;
        }

        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 180px;
        }
        #navbar1 li a{
            color: #505461;
            font-weight: 600;
        }
        #navbar1 .active a{
            color: #000;
        }
        textarea{
            resize:none;
        }
        a{
            color: #505458;
        }
    </style>
    <!-- [if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![end if]-->
</head>

<body>
<div class="container-fluid">
    <div style="margin-top: 20px;">
        <div class="form-group col-sm-2" style="padding: 0;">
            <div>
                <i class="fa fa-filter pull-left" style="margin-right: 10px;padding: 0;margin-top: 10px;"></i>
                <div class="col-sm-9" style="padding: 0;">
                    <select class="selectpicker" id="select-type" onchange="search()" name="type" data-live-search="true" style="width: 200px;">
                        <option selected value="0">全部数据</option>
                            <option value="1">合同交付</option>
                            <option value="2">发货明细</option>
                    </select>
                </div>
            </div>
        </div>
        <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：form搜索
-->
        <div class="form-group col-sm-4" style="padding: 0;">
            <i class="fa fa-search col-sm-1" style="margin-top: 10px;"></i>
            <div class="input-group col-sm-8">
                <input type="text" id="search-text" class="form-control">
                <div class="input-group-btn">
                    <button type="button" class="btn waves-effect" onclick="search()" style="color: black;">检索</button>
                    <button type="button" class="btn waves-effect" id="dropdownMenu3" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                        <li>
                            <a class="btn-default" id="search-btn"  href="javascript:void(0)">高级查询</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-8"></div>
        <div class="col-sm-2 pull-left text-center" style="padding: 0;margin-bottom: 15px;">

        </div>

        <div style="width: 100%;text-align: right;">
        </div>
        <!--
            作者：1810761389@qq.com
            时间：2018-07-23
            描述：搜索状态条数
        -->
    </div>
    <div class="col-sm-2" style="margin-top: 5px;">
        <span class="pull-left">交付记录/发货明细</span>
        <button type="button" onclick="cancelKey()" id="cancel-search" class="hidden btn btn-danger btn-xs pull-left" data-toggle="tooltip" data-placement="bottom" title="解除搜索,显示全部数据">
            <i class="fa fa-reply"></i>解除搜索
        </button>
    </div>
    <div class="col-sm-5">

    </div>

    <!--<div class="col-sm-9">

    </div>-->

    <!--
        作者：1810761389@qq.com
        时间：2018-07-31
        描述：table
    -->
    <div id="content">
        <table id="table">

        </table>

    </div>
    <div class="col-sm-3 pull-right" style="text-align: right; margin-top: 10px;">
        <button onclick="print()" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="打印">
            <i class="fa fa-print"></i>
        </button>
    </div>
</div>


<br>
<!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：页尾
-->
<div class="col-lg-12">
    <footer class="footer" style="text-align: center;padding-bottom: 20px;padding-top: 20px;">热线:<b>4008-8208-820 </b> &nbsp;&nbsp;网站:<b><a href="www.baidu.com" target="_blank">www.baidu.com</a></b> &nbsp;
        <a class="btn btn-danger btn-xs" href="#" onclick="">
            <i class="fa fa-whatsapp m-r-5"></i> 投诉&amp;问题
        </a>&nbsp;&nbsp;
        <a class="btn btn-default btn-xs" href="#" onclick="">
            <i class="fa fa-weixin m-r-5"></i>微客服
        </a>&nbsp;&nbsp;
        <a class="btn btn-primary btn-xs" href="#" onclick="">
            <i class="md md-speaker-notes m-r-5"></i>订阅号
        </a>
        <br>Copyright © 2004-2018 &nbsp;XXX技术有限公司&nbsp;&nbsp; </footer>
</div>


<!--
    作者：1810761389@qq.com
    时间：2018-07-31
    描述：高级查询
-->
<div class="modal fade" id="search-senitor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">
        </div>
    </div>
</div>


<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<script src="/js/tableExport.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="/js/sale/send-goods-detail-table.js"></script>
<%--<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>--%>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script src="/js/resjs/printThis.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-validator/0.5.3/js/language/zh_CN.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="/js/sale/formatter.js"></script>
<script>

    $(function() {
        $("[data-toggle='tooltip' ] ").tooltip();
        //回车时间
        $('#search-text').bind('keypress',function(event){


            if(event.keyCode == 13)

            {
                search()
            }

        });
    });

    function print() {
        /* Act on the event */
        $("#table111").printThis({
            debug: false,
            importCSS: false,
            importStyle: false,
            printContainer: true, //打印容器
            loadCSS: "css/bootstrap-table.css",		//需要加载的css样式
            pageTitle: "sn",
            removeInline: false,
            printDelay: 333, //打印时延
            header: null,
            formValues: false
        });
    }

    //查询
    function search() {
        if ( $('#select-type').val()!=0 ||$('#search-text').val().length >0 ||!$.isEmptyObject(f($('#search-form')))) {
            $('#cancel-search').removeClass('hidden')
        }else {
            $('#cancel-search').addClass('hidden')
        }
        $('#table').bootstrapTable('refresh')
    }
    //取消搜索
    function cancelKey() {
        $('#select-type').selectpicker('val','0');
        $('#search-text').val("");
        if ($('#search-form')[0] != null)
            $('#search-form')[0].reset();
        search();

    }
</script>
</body>

</html>