<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/7
  Time: 9:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>新建维修工单</title>
    <!-- Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.css" />
    <link rel="stylesheet" href="../fonts/font-awesome-4.7.0/css/font-awesome.css" />
    <!--<link rel="" />-->
</head>
<style type="text/css">
    html,
    body {
        color: #333333;
        background-color: #FFFFFF;
        font-size: 12px;
        font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
</style>

<body>
<div class="container-fluid" style="background-color: #F4F8FB;">
    <div style="margin: 10px;background-color: white;padding: 10px;">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-11">
                    <h4 class="modal-title text-dark ">
                        <span>维修工单</span>
                    </h4>
                </div>
                <div class="col-md-1">
                    <button class="btn btn-white waves-effect waves-light" name="B0" type="submit"><i class="fa fa-check m-r-5"></i><span set-lan="html:保存">保存</span></button>
                    </h4>
                </div>
            </div>
        </div>
        <hr style="border-top: 1px solid #aaaaaa;">
        <div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-12" style="font-size: 12px;font-weight: normal;">单号与接件</label>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <!--<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">工单流水号:</label>
                        <div class="col-md-4">
                            <input class="form-control" />
                        </div>-->
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">接单人:</label>
                        <div class="col-md-4">
                            <input class="form-control" />
                        </div>
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">接件日期:</label>
                        <div class="input-append date form_datetime input-group col-md-4">
                            <input id="data" class="form-control"/>
                            <span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>对应客户：</label>
                        <div class="col-md-4">
                            <input class="form-control" />
                        </div>
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">姓名:</label>
                        <div class="col-md-4">
                            <input class="form-control" />
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">电话:</label>
                        <div class="col-md-4">
                            <input class="form-control" />
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-12" style="font-size: 12px;font-weight: normal;">接件详情 <i class="fa fa-cog"></i></label>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;"><span style="color: red;">*</span>维修产品:</label>
                        <div class="col-md-4">
                            <input class="form-control" />
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">产品生产日期:</label>
                        <div class="input-append date form_datetime input-group col-md-4">
                            <input id="date" class="form-control"/>
                            <span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
                        </div>
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">保修:</label>
                        <div class="col-md-4">
                            <label class="radio-inline">
                                <input type="radio" name="optionsRadiosinline" id="optionsRadios3" value="option1" checked> 在保
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="optionsRadiosinline" id="optionsRadios4"  value="option2"> 出保
                            </label>
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">维修工单进度:</label>
                    <div class="col-md-4">
                        <select class="form-control ">
                            <option>接件</option>
                            <option>待检测</option>
                            <option>待维修</option>
                            <option>维修中</option>
                            <option>待交付</option>
                            <option>已交付</option>
                        </select>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">故障描述:</label>
                        <div class="col-md-10">
                            <input class="form-control" />
                        </div>
                    </div>
                </div>
            </div><br>
            <!--</div>
        </div><br>-->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-12" style="font-size: 12px;font-weight: normal;">费用与执行</label>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">费用:</label>
                        <div class="col-md-4">
                            <input class="form-control" />
                        </div>

                    </div>
                </div>
            </div><br>
            <!--<div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">已收款:</label>
                        <div class="col-md-4">
                            <input class="form-control" />
                        </div>

                    </div>
                </div>
            </div><br>-->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">约定交付日期:</label>
                        <div class="input-append date form_datetime input-group col-md-4" >
                            <input id="date1" class="form-control"/>
                            <span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">备注:</label>
                        <div class="col-md-10">
                            <input class="form-control" />
                        </div>
                    </div>
                </div>
            </div><br>
        </div>
    </div>
    <div class="row" style="background-color: white;padding: 10px;margin: 10px;">
        <div class="col-md-12">
            <div class="form-group">
                ￥付款记录
                <button type="button" class="btn btn-link" style="font-size: 20px;text-decoration: none;margin-left:5px;color: black;" data-toggle="modal" data-target="#myModal2">+</button>
            </div>
            <div id="model3"></div>
        </div>
        <!--</div>-->
        <!--</div>-->
        <!-- /.modal -->
        <!--	</div>-->
        <div class="panel-body">
            <span>￥</span>100.00</span>
            </a>&nbsp;第7期&nbsp;2018-07-12<br>
        </div>
    </div>
    <div class="row" style="text-align: center;">
        <div class="col-md-12">
            <div class="form-group">
                <footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
                    <a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
                    <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
                    <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
                    <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>
            </div>
        </div>
    </div>
</div>
<!--</div>-->

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="../js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $("#model3").load("payBack_model.html");
    $(".form_datetime").datetimepicker({
        format: "dd MM yyyy - hh:ii",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });
</script>
</body>

</html>