<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: liquanfang
  Date: 18-8-14
  Time: 下午3:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>编辑明细产品</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <!-- [if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![end if]-->
</head>
<body>
<style type="text/css">
    .mybtn{position:fixed; right:50px; bottom:50px; display:block;
        border-radius:50%; padding:20px;
        text-align:center; line-height:40px;}
    .modal.left .modal-dialog,
    .modal.right .modal-dialog {
        position: fixed;
        margin: auto;
        width: 500px;
        height: 95%;
        -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
        -o-transform: translate3d(0%, 0, 0);
        transform: translate3d(0%, 0, 0);
    }

    .modal.left .modal-content,
    .modal.right .modal-content {
        height: 100%;
        overflow-y: auto;
    }

    .modal.left .modal-body,
    .modal.right .modal-body {
        padding: 15px 15px 80px;
    }

    /*Left*/
    .modal.left.fade .modal-dialog{
        left: -320px;
        -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
        -o-transition: opacity 0.3s linear, left 0.3s ease-out;
        transition: opacity 0.3s linear, left 0.3s ease-out;
    }

    .modal.left.fade.in .modal-dialog{
        left: 0;
    }

    /*Right*/
    .modal.right.fade .modal-dialog {
        right: -500px;
        top: 2.5%;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    .modal.right.fade.in .modal-dialog {
        right: 2%;
    }

    /* ----- MODAL STYLE ----- */
    .modal-content {
        border-radius: 5px;
        border: none;
    }

    .modal-header {
        border-bottom-color: #EEEEEE;
        background-color: #FAFAFA;
    }

</style>
<!--点击右侧弹出-->
<a href="javascript:;" class="btn btn-danger mybtn" data-toggle="modal" data-target="#myModal"><i class="fa fa-user"></i><span>产品</span></a>
<!-- 弹出层 modal -->
<div class="modal right fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="modal" style="overflow: hidden">
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <p>产品树图示说明&nbsp;&nbsp; <span class="fa fa-shopping-cart"></span> : 计算库存&nbsp;&nbsp;
                <span class="fa fa-comment-o"></span> : 查看产品详情</p>
        </div>
        <div class="col-md-12">
            <h4><c:choose><c:when test="${type}">入库明细</c:when><c:otherwise>出库明细</c:otherwise></c:choose></h4>
        </div>
        <div class="col-md-12" style="margin-top: 20px">
            <form id="save-form" action="/sale/warehouse/detail/save" method="post">
                <input type="hidden" name="id" value="
<c:choose>
<c:when test="${type}">
                ${info.wpId}
</c:when>
<c:otherwise>${info.wpullId}</c:otherwise></c:choose>
">
                <input type="hidden" name="type" value="${type}">
                <table id="table"></table>
            </form>

        </div>
        <div class="col-md-12 text-center" style="margin-top: 20px">
            <button class="btn btn-lg " id="save">保持明细数据</button>
        </div>
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-treeview.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<!-- Latest compiled and minified Locales -->
<script src="/js/sweetalert.min.js"></script>
<script>
    $(function () {
        initTable();
        sumitForm();
        $('#modal').load('/sale/chooseProduct')
    })
    function initTable() {
        $("#table").bootstrapTable({
            method: 'get',
            striped: true,
            showExport:true,
            cache: false,
            sortable: false,
            sortOrder: "desc",
            uniqueId:"productId",
            url: '/sale/warehouse/<c:choose><c:when test="${type}">in/detail?id=${info.wpId}</c:when><c:otherwise>out/detail?id=${info.wpullId}</c:otherwise></c:choose>',
            exportOptions:{
                fileName:$('#table-title').text(),
                ignoreColumn:[7]
            },
            showFooter:true,
            columns: [{
                field:'productTitle',
                title:'品名型号/规格[单位]',
                valign: 'middle',
                width:50,
                footerFormatter:"合计",
                formatter:function (value, row, index) {
                    return [
                        value+'<input class="form-control" name="datas['+index+'].tbProductFormatByPfId.pfId" type="hidden" value="'+row.productId+'">'
                    ]
                }

            },{
                field:<c:choose><c:when test="${type}">'inNumber'</c:when><c:otherwise>'outNumber'</c:otherwise></c:choose>,
                title:'数量',
                align:'center',
                valign: 'middle',
                width:25,
                formatter:function (value, row, index) {
                    return [
                        '<input class="form-control"  name="datas['+index+'].<c:choose><c:when test="${type}">inNumber</c:when><c:otherwise>outNumber</c:otherwise></c:choose>" id="Number'+index+'" type="number" value="'+value+'">'
                    ]
                }
            },{
                field:'wdOther',
                title:'备注',
                align:'center',
                valign: 'middle',
                width:25,
                formatter:function (value, row, index) {
                    if (value == null)
                        value = "";
                    return [
                        '<textarea class="form-control" name="datas['+index+'].wdOther" id="other'+index+'" rows="1"  style="resize: vertical" >'+value+'</textarea  >'
                    ]
                }
            },{
                field:'formatterId',
                title:'操作',
                align:'center',
                width:1,
                formatter:function (value, row, index) {
                    return [
                        '<a class="btn" href="javascript:;" onclick="deleteDetail('+row.productId+')"><i class="fa fa-trash"></i></a>'
                    ]
                }
            }],
            onLoadSuccess:function (data) {
            },
            onLoadError:function (status) {

            }
        })
    }
    function sum(data) {
        field = this.field;
        return data.reduce(function(sum, row) {
            return sum + (+row[field]);
        }, 0);
    }
    function sumitForm() {
        //提交表单数据
        $('#save').click(function () {
                $("#save-form").ajaxSubmit({
                    dataType:'json',
                    success:function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            successForm();
                        }
                        else{
                            swal(result.msg,"","error");
                        }
                    },
                    error:function () {
                        swal("连接服务器错误","","error");
                    }
                });
            })
    }
    function deleteDetail(id) {
            swal({
                    title: "确定删除吗？",
                    text: "你即将删除该明细！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定删除！",
                    closeOnConfirm: false
                },
                function(){
                $('#table').bootstrapTable('removeByUniqueId', id);
                    swal("删除！", "一条订单详情已经被删除,请保存明细数据", "success");
                });
    }
    function successForm(){
        if (window.opener !=null) {
            window.close();
            window.opener.history.go(0);
        }
    }
    function selectProduct(title,id,price,stock) {
        var datas = $('#table').bootstrapTable("getData",true);
        for (var i = 0; i < datas.length; i++) {
            if (datas[i].productId == id) {
                swal("添加重复", "详情中已经有该产品", "error");
                return;
            }
        }
        if (stock){
            swal("添加错误", "不需要计算库存的产品，不需要经过库存", "error")
            return;
        }else {
            $('#table').bootstrapTable('append', {
                'productTitle': title,
                    "productId": id,
                <c:choose>
                <c:when test="${type}">'inNumber':1,</c:when><c:otherwise>'outNumber':1,</c:otherwise></c:choose>
                }
            );
        }
    }

</script>
</body>
</html>
