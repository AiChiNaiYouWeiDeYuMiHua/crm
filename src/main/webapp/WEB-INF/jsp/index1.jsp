<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/1
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="utf-8"/>
    <title>CRM</title>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet"/>
    <link href="/css/style.css" rel="stylesheet"/>
</head>

<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
<div id="wrapper">
    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="nav-close"><i class="fa fa-times-circle"></i>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
									<span class="clear">
                                    <span class="block m-t-xs" style="font-size:20px;">
                                        <i class="fa fa-area-chart"></i>
                                        <strong class="font-bold">CRM</strong>
                                    </span>
									</span>
                        </a>
                    </div>
                    <div class="logo-element">CRM
                    </div>
                </li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <span class="ng-scope">欢迎&nbsp;<i class="fa fa-trophy"></i></span>
                </li>
                <li>
                    <a class="J_menuItem" href="main">
                        <i class="fa fa-home"></i>
                        <span class="nav-label">主页</span>
                    </a>
                </li>

                <!--
                    作者：1037566091@qq.com
                    时间：2018-07-30
                    描述：市场
                -->
                <li class="line dk"></li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <i class="fa fa-jsfiddle"></i>&nbsp;<span class="ng-scope">市场</span>
                </li>
                <li>
                    <a href="#"><i class="fa fa-address-book-o"></i> <span class="nav-label">客户</span><span
                            class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem" href=""><i class="fa fa-user"></i>全部客户</a>
                        </li>
                        <li>
                            <a class="J_menuItem" href=""><i class="fa fa-mobile-phone"></i>联系人</a>
                        </li>
                        <li>
                            <a class="J_menuItem" href=""><i class="fa fa-meetup"></i>纪念日</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-edit"></i> <span class="nav-label">市场活动</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-desktop"></i> <span class="nav-label">广告发布</span></a>
                </li>

                <!--
                    作者：1037566091@qq.com
                    时间：2018-07-30
                    描述：销售
                -->
                <li class="line dk"></li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <i class="fa fa-modx"></i>&nbsp;<span class="ng-scope">销售</span>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-crosshairs"></i> <span class="nav-label">销售机会</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-money"></i> <span class="nav-label">报价</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">合同订单</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-clipboard"></i> <span class="nav-label">交付计划</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-shekel"></i> <span class="nav-label">退换货</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-paypal"></i> <span class="nav-label">回款</span></a>
                </li>

                <!--
                    作者：1037566091@qq.com
                    时间：2018-07-30
                    描述：产品
                -->
                <li class="line dk"></li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <i class="fa fa-apple"></i>&nbsp;<span class="ng-scope">产品</span>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-truck"></i> <span class="nav-label">发货</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href=""><i class="fa fa-shopping-cart"></i> <span
                            class="nav-label">采购</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-product-hunt"></i> <span class="nav-label">产品管理 </span></a>
                </li>

                <!--
                    作者：1037566091@qq.com
                    时间：2018-07-30
                    描述：仓储
                -->
                <li class="line dk"></li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <i class="fa fa-institution"></i>&nbsp;<span class="ng-scope">仓储</span>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-list-alt"></i> <span class="nav-label">库存</span></a>
                </li>

                <!--
                    作者：1037566091@qq.com
                    时间：2018-07-30
                    描述：财务
                -->
                <li class="line dk"></li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <i class="fa fa-dollar"></i>&nbsp;<span class="ng-scope">财务</span>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-list-ol"></i> <span class="nav-label">报账明细</span></a>
                </li>

                <!--
                    作者：1037566091@qq.com
                    时间：2018-07-30
                    描述：行政
                -->
                <li class="line dk"></li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <i class="fa fa-gg"></i>&nbsp;<span class="ng-scope">行政</span>
                </li>
                <li>
                    <a href="#"><i class="fa fa-smile-o"></i> <span class="nav-label">客服</span><span
                            class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem" href=""><i class="fa fa-user-plus"></i>售后服务</a>
                        </li>
                        <li>
                            <a class="J_menuItem" href=""><i class="fa fa-wrench"></i>维修工单</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-bullhorn"></i> <span class="nav-label">公告</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-book"></i> <span class="nav-label">知识库</span></a>
                </li>

                <!--
                    作者：1037566091@qq.com
                    时间：2018-07-30
                    描述：个人中心
                -->
                <li class="line dk"></li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <i class="fa fa-user"></i>&nbsp;<span class="ng-scope">个人center</span>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-address-card"></i> <span class="nav-label">个人信息</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-puzzle-piece"></i> <span class="nav-label">修改密码</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-newspaper-o"></i> <span class="nav-label">日月周报</span><span
                            class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a class="J_menuItem" href="typography.html"><i class="fa fa-star-o"></i>日报</a>
                        </li>
                        <li>
                            <a class="J_menuItem" href="typography.html"><i class="fa fa-star-half-o"></i>周报</a>
                        </li>
                        <li>
                            <a class="J_menuItem" href="typography.html"><i class="fa fa-star"></i>月报</a>
                        </li>
                    </ul>
                </li>

                <!--
                    作者：1037566091@qq.com
                    时间：2018-07-30
                    描述：系统中心
                -->
                <li class="line dk"></li>
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                    <i class="fa fa-gears"></i>&nbsp;<span class="ng-scope">系统center</span>
                </li>
                <li>
                    <a class="J_menuItem" href="to_dept"><i class="fa fa-group"></i> <span class="nav-label">部门管理</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="admin/to_user_list"><i class="fa fa-child"></i> <span class="nav-label">用户管理</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="to_role_list"><i class="fa fa-id-badge"></i> <span class="nav-label">角色管理</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-language"></i> <span class="nav-label">权限管理</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-map"></i> <span class="nav-label">系统操作日志</span></a>
                </li>
                <li>
                    <a class="J_menuItem" href="#"><i class="fa fa-stack-overflow"></i> <span class="nav-label">数据变更日志</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom be-gray">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-info " href="#"><i class="fa fa-bars"></i>
                    </a>
                    <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search"
                                   id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li class="m-t-xs">
                                <div class="dropdown-messages-box">
                                    <a href="" class="pull-left">
                                        <img alt="image" class="img-circle" src="/img/a7.jpg">
                                    </a>
                                    <div class="media-body">
                                        <small class="pull-right">46小时前</small>
                                        <strong>小四</strong> 是不是只有我死了,你们才不骂爵迹
                                        <br>
                                        <small class="text-muted">3天前 2014.11.8</small>
                                    </div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="dropdown-messages-box">
                                    <a href="" class="pull-left">
                                        <img alt="image" class="img-circle" src="/img/a4.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="pull-right text-navy">25小时前</small>
                                        <strong>二愣子</strong> 呵呵
                                        <br>
                                        <small class="text-muted">昨天</small>
                                    </div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a class="J_menuItem" href="">
                                        <i class="fa fa-envelope"></i> <strong> 查看所有消息</strong>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> 您有16条未读消息
                                        <span class="pull-right text-muted small">4分钟前</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="">
                                    <div>
                                        <i class="fa fa-qq fa-fw"></i> 3条新回复
                                        <span class="pull-right text-muted small">12分钟钱</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a class="J_menuItem" href="">
                                        <strong>查看所有 </strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle profile  waves-effect waves-light" data-toggle="dropdown"
                           aria-expanded="false" style="height: 50px;">
                            <img src="/img/a3.jpg" alt="user-img" class="img-circle" width="30px">陈默</a>
                        <ul class="dropdown-menu ">
                            <li>
                                <a href="/logout.xt "><i class="ti-power-off m-r-5 "></i>安全退出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row J_mainContent" id="content-main">
            <iframe id="J_iframe" width="100%" height="100%" src="main" frameborder="0"<%-- data-id="main.html"--%> seamless></iframe>
        </div>
    </div>
    <!--右侧部分结束-->
</div>

<!-- 全局js -->
<script src="/js/jquery.min.js?v=2.1.4"></script>
<script src="/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/js/plugins/layer/layer.min.js"></script>

<!-- 自定义js -->
<script src="/js/hAdmin.js?v=4.1.0"></script>
<script type="text/javascript" src="/js/index.js"></script>

<!-- 第三方插件 -->
<script src="/js/plugins/pace/pace.min.js"></script>
</body>

</html>
