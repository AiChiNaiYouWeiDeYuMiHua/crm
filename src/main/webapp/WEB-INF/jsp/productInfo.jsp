<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<title></title>
		<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
		<link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	</head>

	<body>
		<div id="content" style="margin-top: 20px;">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8">
						<div id="pro" class="form-horizontal" style="padding: 12px 20px;">
							<div id="pro-header">
								<div class="pull-right" style="font-size: 12px;">
									<button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" data-target="#delModal">
										<i class="fa fa-trash-o" style="margin-right: 5px;"></i>删除
									</button>&nbsp;
									<button class="btn btn-default" style="font-size: 12px;">
										<i class="fa fa-pencil" style="margin-right: 5px;"></i>编辑
									</button>&nbsp;
								</div>
								<h4>
									<span>产品信息</span>
									<a target="_blank" href="??" style="color: #999999;" data-toggle="tooltip" data-placement="bottom" title="数据日志">
										<i class="fa fa-bars"></i>
									</a>
								</h4>
								<hr style="border-top: 1px solid #aaaaaa;" />
							</div>
							<div id="pro-content" style="margin-top: 30px;">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">品名：</label>
											<div class="form-control1 col-md-8">拼接屏</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">编码/条码：</label>
											<div class="form-control1 col-md-8"></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">编码/条码：</label>
											<div class="form-control1 col-md-8">拼接屏</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">品名：</label>
											<div class="form-control1 col-md-8">拼接屏</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">品名：</label>
											<div class="form-control1 col-md-8">拼接屏</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">品名：</label>
											<div class="form-control1 col-md-8">拼接屏</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">品名：</label>
											<div class="form-control1 col-md-8">拼接屏</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">品名：</label>
											<div class="form-control1 col-md-8">拼接屏</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">品名：</label>
											<div class="form-control1 col-md-8">拼接屏</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">品名：</label>
											<div class="form-control1 col-md-8">拼接屏</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-md-2">价格策略:</label>
											<div class="form-control1  col-md-10">
												<table class="table table-hover table-bordered">
													<tbody style="font-size: 13px;">
														<tr>
															<th nowrap="" class="text-right" width="10%">零售价asdf sdf</th>
															<td width="90%"><b>5000</b>&nbsp;</td>
														</tr>
														<tr>
															<th nowrap="" class="text-right" width="10%">阿迪价格</th>
															<td width="90%"><b>5000</b>&nbsp;</td>
														</tr>
														<tr>
															<th nowrap="" class="text-right" width="10%">VIP客户价</th>
															<td width="90%"><b>5000</b>&nbsp;</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-md-2">外币:</label>
											<div class="form-control1  col-md-10">
												<table class="table table-hover table-bordered">
													<tbody style="font-size: 13px;">
														<tr>
															<th width="50%">价格</th>
															<td width="50%"><b>成本</b>&nbsp;</td>
														</tr>
														<tr>
															<td>
																<table>
																	<tbody>
																		<tr>
																			<td nowrap="" class="text-right">美元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">USD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">欧元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">EUR0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">港币:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">HKD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">英镑:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">GBP0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">日元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">JPY0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">加拿大元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">CAD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">澳大利亚元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">AUD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">新加坡元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">SGD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">瑞士法郎:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">CHF0.00</span></b>&nbsp;</td>
																		</tr>
																	</tbody>
																</table>
															</td>
															<td>
																<table>
																	<tbody>
																		<tr>
																			<td nowrap="" class="text-right">美元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">USD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">欧元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">EUR0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">港币:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">HKD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">英镑:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">GBP0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">日元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">JPY0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">加拿大元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">CAD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">澳大利亚元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">AUD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">新加坡元:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">SGD0.00</span></b>&nbsp;</td>
																		</tr>
																		<tr>
																			<td nowrap="" class="text-right">瑞士法郎:</td>
																			<td nowrap="" align="right"><b><span style="white-space:nowrap">CHF0.00</span></b>&nbsp;</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">重量：</label>
											<div class="form-control1 col-md-8">0.00</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">重量单位：</label>
											<div class="form-control1 col-md-8">kg</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">权限分组：</label>
											<div class="form-control1 col-md-8">0.00</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">序列号管理：</label>
											<div class="form-control1 col-md-8">否</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-md-2">产品图片：</label>
											<div class="form-control1 col-md-10">
												<a href="??" target="_blank">
													<img width="80" id="boder-d" src="img/cb913faa95a4d5be2c611493e421c48f.png" />
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">库存上限：</label>
											<div class="form-control1 col-md-8">50.00</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-md-4">库存下限：</label>
											<div class="form-control1 col-md-8">3.00</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-md-2">明细概要：</label>
											<div class="form-control1 col-md-10">

											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-md-2">明细概要：</label>
											<div class="form-control1 col-md-10">

											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-md-2">明细概要：</label>
											<div class="form-control1 col-md-10">

											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-md-2">明细概要：</label>
											<div class="form-control1 col-md-10">

											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label col-md-2">明细概要：</label>
											<div class="form-control1 col-md-10">

											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="pro-bottom" class="text-right">
								<button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" data-target="#delModal">
																		<i class="fa fa-trash-o" style="margin-right: 5px;"></i>删除
																	</button>&nbsp;
								<button class="btn btn-default" style="font-size: 12px;">
																		<i class="fa fa-pencil" style="margin-right: 5px;"></i>编辑
																	</button>&nbsp;
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<table class="table table-bordered table-hover" style="font-size: 13px;">
							<thead>
								<tr>
									<th>
										<div class="th-inner">仓库</div>
									</th>
									<th>
										<div class="th-inner ">库存</div>
									</th>
								</tr>
							</thead>
							<tbody style="background-color: white;">

								<tr>
									<td>全国仓库</td>
									<td>4.00</td>
								</tr>
								<tr>
									<td>北京仓库</td>
									<td>2.00</td>
								</tr>
								<tr>
									<td>上海仓库</td>
									<td>0.00</td>
								</tr>
							</tbody>
						</table>
						<a href="??" target="_blank" class="btn btn-link"> 查看本产品出入库历史<b>
						            &gt;&gt;</b></a>
					</div>
				</div>
				<div id="tool" style="margin-top: 20px;">
					<div class="panel panel-default">
						<div class="panel-heading" style="background-color: white;">
							<div class="panel-heading">
								<h5 class="panel-title">
									<i class="fa fa-pencil-square"></i>
									<b>全部规格</b>
									<font style="cursor: pointer;" color="#808080">说明：产品“规格”的特殊作用</font>
									<span style="cursor: pointer;">
										<i class="fa fa-exclamation-triangle"></i>
									</span>
									<a class="text-a" style="margin-left: 10px;">
										<i class="fa fa-plus" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="新建"></i>新建子规格
									</a>
									<a class="text-a" style="margin-left: 10px;">
										<i class="fa fa-angle-double-down"></i>导入子规格
									</a>
									<div class="pull-right">
										<a href="??" target="_blank" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="规格管理权限">
											<i class="fa fa-male"></i>规格管理权限
										</a>
									</div>
								</h5>
							</div>
							<div class="panel-body">
								<table class="table-bordered  table table-hover">

									<thead>
										<tr>
											<th class="th-inner ">ID</th>
											<th class="th-inner ">规格</th>
											<th class="th-inner ">单位</th>
											<th class="th-inner ">单位换算</th>
											<th class="th-inner ">价格</th>
											<th class="th-inner ">编号</th>
											<th class="th-inner ">批次</th>
											<th class="th-inner ">生产日期</th>
											<th class="th-inner ">失效日期</th>
											<th class="th-inner ">操作</th>
										</tr>
									</thead>
									<tbody>

										<tr>
											<td>
												<a href="??">103▲</a>
											</td>
											<td>-</td>
											<td></td>
											<td>1.00</td>
											<td><span style="white-space:nowrap"><span style="font-size:9pt;font-weight:normal;">￥</span>5,000.00</span>
											</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>
												<a href="??">视图</a>
												<a href="??">编辑</a>
												<a href="#">删除</a>
											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading bg-white" style="background-color: white;">
							<h5 class="panel-title">本产品全部规格库存：</h5>
						</div>
						<div class="panel-body">
							<table class="table-bordered  table table-hover">
								<thead>
									<tr>
										<th class="th-inner ">规格</th>
										<th class="th-inner ">单位</th>
										<th class="th-inner ">全国仓库</th>
										<th class="th-inner ">北京仓库</th>
										<th class="th-inner ">上海仓库</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>-</td>
										<td></td>
										<td align="right">4
											<a href="#"><img src="img/split.png" alt="本规格部分或全部库存，转换为本产品其它规格库存" title="本规格部分或全部库存，转换为本产品其它规格库存"></a>
										</td>
										<td align="right">2
											<a href="#"><img src="img/split.png" alt="本规格部分或全部库存，转换为本产品其它规格库存" title="本规格部分或全部库存，转换为本产品其它规格库存"></a>
										</td>
										<td> &nbsp;</td>
									</tr>
									<tr>
										<td>按基准合计</td>
										<td nowrap=""></td>
										<td align="right"><b>4</b></td>
										<td align="right"><b>2</b></td>
										<td align="right"><b>0</b></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
			<a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
			<a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
			<a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
			<br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>

		<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								&times;
							</button>
						<h4 class="modal-title" id="myModalLabel">
								删除
							</h4>
					</div>
					<div class="modal-body">
						<ol>
							<li>保留历史的成交数据，如订单、采购单等</li>
							<li>保留统计数据</li>
							<li>此产品将进入回收站，支持恢复</li>

						</ol>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭
							</button>
						<button type="button" class="btn btn-primary">
								确认删除
							</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal -->
		</div>

		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.js"></script>

		<script>
			$(function() {
				$("[data-toggle='tooltip']").tooltip();
			});
		</script>

	</body>

</html>