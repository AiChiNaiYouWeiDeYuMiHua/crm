<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/1
  Time: 17:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>登录</title>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/css/login.css"/>
    <link rel="stylesheet" type="text/css" href="/css/animate.css"/>
    <link rel="stylesheet" href="/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="/css/lcy/jigsaw.css">
</head>

<body>
<div class="container">
    <form action="" method="post" id="login_form">
        <div class="form row">
            <div class="form-horizontal col-md-offset-3">
                <h3 class="form-title">LOGIN</h3>
                <div class="col-md-9">
                    <div class="form-group">
                        <i class="fa fa-user fa-lg"></i>
                        <input class="form-control required" type="text" placeholder="Username" id="userName"
                               name="userName" autofocus="autofocus" maxlength="20"/>
                    </div>
                    <div class="form-group">
                        <i class="fa fa-lock fa-lg"></i>
                        <input class="form-control required" type="password" placeholder="Password" id="userPassword"
                               name="userPassword" maxlength="8"/>
                    </div>
                    <%--<div class="form-group">--%>
                    <%--<i class="fa fa-paypal"></i>--%>
                    <%--<input class="form-control required" width="120px" type="text" placeholder="Verification"--%>
                    <%--id="verification" name="verification" autofocus="autofocus" maxlength="20"/>--%>
                    <%--</div>--%>
                    <%--<div>${user}</div>--%>
                    <%--<div class="form-group">--%>
                    <%--<label class="checkbox">--%>
                    <%--<input type="checkbox" name="remember" value="1"/>记住我--%>
                    <%--</label>--%>
                    <%--</div>--%>
                    <div class="form-group">
                        <div id="captcha" style="position: relative"></div>
                        <div id="msg"></div>
                    </div>
                    <div class="form-group col-md-offset-9">
                        <button id="submit" type="submit" class="btn btn-success pull-right">登录</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script type="text/javascript" src="/js/plugins/validator/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="/js/login.js"></script>
<script type="text/javascript" color="175,209,231" opacity='0.7' zIndex="-2" count="200"
        src="/js/plugins/canvas/canvas-nest.js"></script>
<script type="text/javascript" src="/js/plugins/canvas/canvas-nest.umd.js"></script>
<script type="text/javascript" src="/js/lcy/jigsaw.js"></script>
<script>
    var aa = 0;
    jigsaw.init(document.getElementById('captcha'), function () {
        document.getElementById('msg').innerHTML = '验证成功！';
        aa = 1;
    })
</script>
</body>
</html>
