<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/1
  Time: 17:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta charset="utf-8"/>
    <title>首页</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/main.css"/>
    <link href="/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <style type="text/css">
        .col-xs-6 {
            padding: 5px 10px 0px 0px;
        }
    </style>
</head>

<body class="badge-gray">
<div class="row">
    <div class="col-sm-4">
        <div class="row row-sm text-center">
            <div class="col-xs-6">
                <div class="panel padder-v item">
                    <div class="h1 text-info font-thin h1">CRM</div>
                    <br>
                    <span class="text-muted text-xs">企业智慧云</span>
                    <div class="top text-right w-full">
                        <%--<i class="fa fa-caret-down text-warning m-r-sm"></i>--%>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="panel padder-v item bg-info">
                    <div class="h1 text-fff font-thin h1" style="padding-top: -15px">微笑以待</div>
                    <span class="text-muted text-xs">争取一个客户不容易</span><br>
                    <span class="text-muted text-xs">失去一个客户很简单</span>
                    <%--<div class="top text-right w-full">--%>
                    <%--<i class="fa fa-caret-down text-warning m-r-sm"></i>--%>
                    <%--</div>--%>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="panel padder-v item bg-primary">
                    <div class="h1 text-fff font-thin h1" id="cus_top">521</div>
                    <span class="text-muted text-xs">客户总数</span>
                    <div class="top text-right w-full">
                        <i class="fa fa-caret-up text-warning m-r-sm"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="panel padder-v item">
                    <div class="font-thin h1" id="sale_top">129</div>
                    <span class="text-muted text-xs">销售总数</span>
                    <div class="bottom text-left">
                        <i class="fa fa-caret-up text-warning m-l-sm"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title" style="border-bottom:none;background:#fff;">
                <h5>服务器状态</h5>
            </div>
            <div class="ibox-content" style="border-top:none;">
                <div id="flot-line-chart-moving" style="height:217px;">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-6 tabstyle widget-bg-color-icon">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#my_performance" data-toggle="tab">
                    我的历史业绩
                </a>
            </li>
            <li><a href="#firm_performance" data-toggle="tab">公司月度统计</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="my_performance">
                <div class="col-xs-5 col-md-offset-1">
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary">
                                    <i class="fa fa-user-o text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">新建客户</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom"
                                              data-original-title="过期和未来30天"><b class="ng-binding" id="cus_my">29</b><small>个</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary">
                                    <i class="fa fa-file-code-o text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">报价单</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="本月"><b
                                                class="ng-binding" id="quote_my">0</b><small>笔</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-xs-5 col-md-offset-1">
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary ">
                                    <i class="fa fa-star text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">销售机会</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom"
                                              data-original-title="过期和未来30天"><b class="ng-binding" id="opp_my">36</b><small>个</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary ">
                                    <i class="fa fa-file-powerpoint-o text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">合同订单</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="本月"><b
                                                class="ng-binding" id="order_my">3</b><small>笔</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-xs-5 col-md-offset-1">
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary ">
                                    <i class="fa fa-file-text text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">采购单</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom"
                                              data-original-title="过期和未来30天"><b class="ng-binding" id="pur_my">36</b><small>笔</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-xs-5 col-md-offset-1">
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary ">
                                    <i class="fa fa-truck text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">完成发货</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom"
                                              data-original-title="过期和未来30天"><b class="ng-binding" id="ship_my">36</b><small>笔</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade in active" id="firm_performance">
                <div class="col-xs-5 col-md-offset-1">
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary">
                                    <i class="fa fa-user-o text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">新建客户</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom"
                                              data-original-title="过期和未来30天"><b class="ng-binding" id="cus_firm">29</b><small>个</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary">
                                    <i class="fa fa-file-code-o text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">报价单</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="本月"><b
                                                class="ng-binding" id="quote_firm">0</b><small>笔</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-xs-5 col-md-offset-1">
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary ">
                                    <i class="fa fa-star text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">销售机会</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom"
                                              data-original-title="过期和未来30天"><b class="ng-binding" id="opp_firm">36</b><small>个</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary ">
                                    <i class="fa fa-file-powerpoint-o text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">合同订单</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom" data-original-title="本月"><b
                                                class="ng-binding" id="order_firm">3</b><small>笔</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-xs-5 col-md-offset-1">
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary ">
                                    <i class="fa fa-file-text text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">采购单</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom"
                                              data-original-title="过期和未来30天"><b class="ng-binding" id="pur_firm">36</b><small>笔</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-xs-5 col-md-offset-1">
                    <div class="m-t-20">
                        <div class="pull-left text-center">
                            <a style="cursor: pointer">
                                <div class="bg-icon bg-icon-primary ">
                                    <i class="fa fa-truck text-primary"></i>
                                </div>
                            </a>
                            <p class="text-muted">完成发货</p>
                        </div>
                        <h4 class="text-dark text-center p-t-10">
                            <a style="cursor: pointer" class="counter">
                                        <span data-toggle="tooltip" data-placement="bottom"
                                              data-original-title="过期和未来30天"><b class="ng-binding" id="ship_firm">36</b><small>笔</small></span>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6">
        <div class="card-box widget-box-1">
            <div class="card-box-head">
                <div class="pull-right ">
                    <a class="btn btn-link btn-xs ng-scope ng-hide" ng-click="tab.tabClick(k)"
                       ng-repeat="(k, v) in tab.status" ng-show="tab.isShowTab(k, v)"><span ng-if="!v.ishtml"
                                                                                            class="ng-binding ng-scope"></span>
                        <span class="badge badge-gray ng-binding" ng-show="v.num > 0 &amp;&amp; k != 'recent'"
                              style="">
                        12
                    </span>
                    </a>
                    <a class="btn btn-link btn-xs ng-scope" ng-click="tab.tabClick(k)"
                       ng-repeat="(k, v) in tab.status" ng-show="tab.isShowTab(k, v)"><span ng-if="!v.ishtml"
                                                                                            class="ng-binding ng-scope">最近沟通</span>
                        <span class="badge badge-gray ng-binding ng-hide"
                              ng-show="v.num > 0 &amp;&amp; k != 'recent'">
                        10
                    </span>
                    </a>
                    <a class="btn btn-link btn-xs ng-scope" ng-click="tab.tabClick(k)"
                       ng-repeat="(k, v) in tab.status" ng-show="tab.isShowTab(k, v)"><span ng-if="!v.ishtml"
                                                                                            class="ng-binding ng-scope">to我</span>
                        <span class="badge badge-gray ng-binding" ng-show="v.num > 0 &amp;&amp; k != 'recent'"
                              style="">
                        6
                    </span>
                    </a>
                    <a class="btn btn-link btn-xs ng-scope" ng-click="tab.tabClick(k)"
                       ng-repeat="(k, v) in tab.status" ng-show="tab.isShowTab(k, v)"><span
                            ng-bind-html="v.text|html" ng-if="v.ishtml" class="ng-binding ng-scope"><i
                            class="fa fa-star"></i></span>
                        <span class="badge badge-gray ng-binding" ng-show="v.num > 0 &amp;&amp; k != 'recent'"
                              style="">
                        1
                    </span>
                    </a>
                    <a class="btn btn-link btn-xs ng-scope" ng-click="tab.tabClick(k)"
                       ng-repeat="(k, v) in tab.status" ng-show="tab.isShowTab(k, v)"><span ng-if="!v.ishtml"
                                                                                            class="ng-binding ng-scope">超期</span>
                        <span class="badge badge-gray ng-binding" ng-show="v.num > 0 &amp;&amp; k != 'recent'"
                              style="">
                        11
                    </span>
                    </a>
                    <a class="btn btn-link btn-xs ng-scope" ng-click="tab.tabClick(k)"
                       ng-repeat="(k, v) in tab.status" ng-show="tab.isShowTab(k, v)"><span
                            ng-bind-html="v.text|html" ng-if="v.ishtml" class="ng-binding ng-scope"><i
                            class="fa fa-search fa-lg"></i></span>
                        <span class="badge badge-gray ng-binding ng-hide"
                              ng-show="v.num > 0 &amp;&amp; k != 'recent'">
                        0
                    </span>
                    </a>
                </div>
                <div ng-show="tab.status.icon_search.show" class="pull-right ng-hide">
                    <input type="text"
                           class="form-control ng-pristine ng-untouched ng-valid ng-isolate-scope ng-empty"
                           id="_action_input_search" ng-model="search_content" style="display: inline-block;"
                           set-focus="false">
                </div>
                <p class=" m-b-10 f-s-12" set-lan="html:待办任务">待办任务</p>
            </div>
            <div class="bar-widget">
                <div class="slimScrollDiv"
                     style="position: relative; overflow: hidden; width: 100%; height: 380px;">
                    <div style="overflow: hidden; width: 100%; height: 380px;" scrollend=""
                         ng-class="{true: 'inbox-widget slimescroll-km mx-box s p', false: 'inbox-widget slimescroll-km s p'}[rne]"
                         class="inbox-widget slimescroll-km s p mx-box">
                        <div class="text-center p-t-20 ng-hide" ng-show="!tab.status.all.show">
                            <a class="label label-danger" ng-click="tab.tabClick('all')"><span
                                    ng-if="!tab.status.all.ishtml" class="ng-binding ng-scope"></span>
                                <i class="fa fa-close m-l-5"></i>
                            </a>
                        </div>
                        <div ng-repeat="(index, info) in action | filter: tab.switchTab | orderBy: [tab.orderBy, tab.orderBy2] | limitTo: limitto track by $index"
                             ng-click="vopen(info.id)" on-finish-render-action="onFinishRenderAction"
                             style="cursor: pointer;" class="inbox-item animated_time animated ng-scope">
                            <div class="position-r">
                                <div class="inbox-item-img m-t-5 ">
                                        <span class="badge badge-xs badge-danger ng-hide"
                                              ng-show="info.redpoint"></span>
                                </div>
                                <div class="pull-left" style="width: calc(100% - 55px);">
                                    <p class="inbox-item-author">
                                        <span ng-bind-html="info.title|highlight:search_content"
                                              class="ng-binding"><span>有可能成为长期合作客户，多做关怀</span></span>
                                    </p>
                                    <p class="text-black ng-binding"><img
                                            ng-repeat="who_img in info.img_who track by $index"
                                            ng-src="/hc/?3b@1^5`@@1^5`@QNNxnMwADZ"
                                            class="img-circle img-xs ng-scope">
                                        <i class="fa fa-home m-l-5" ng-show="info.cu_name != ''"></i>
                                        <span ng-bind-html="info.cu_name|highlight:search_content"
                                              class="ng-binding"><span>华那商场</span></span> 刘经理
                                    </p>
                                    <p class="inbox-item-text ng-binding">
                                        陈默：收到
                                    </p>
                                    <p class="inbox-item-date ng-binding">2020-03-02</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="position-r">
                                <div class="inbox-item-img m-t-5 ">
                                        <span class="badge badge-xs badge-danger ng-hide"
                                              ng-show="info.redpoint"></span>
                                </div>
                                <div class="pull-left" style="width: calc(100% - 55px);">
                                    <p class="inbox-item-author">
                                        <span ng-bind-html="info.title|highlight:search_content"
                                              class="ng-binding"><span>有可能成为长期合作客户，多做关怀</span></span>
                                    </p>
                                    <p class="text-black ng-binding"><img
                                            ng-repeat="who_img in info.img_who track by $index"
                                            ng-src="/hc/?3b@1^5`@@1^5`@QNNxnMwADZ"
                                            class="img-circle img-xs ng-scope">
                                        <i class="fa fa-home m-l-5" ng-show="info.cu_name != ''"></i>
                                        <span ng-bind-html="info.cu_name|highlight:search_content"
                                              class="ng-binding"><span>华那商场</span></span> 刘经理
                                    </p>
                                    <p class="inbox-item-text ng-binding">
                                        陈默：收到
                                    </p>
                                    <p class="inbox-item-date ng-binding">2020-03-02</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="position-r">
                                <div class="inbox-item-img m-t-5 ">
                                        <span class="badge badge-xs badge-danger ng-hide"
                                              ng-show="info.redpoint"></span>
                                </div>
                                <div class="pull-left" style="width: calc(100% - 55px);">
                                    <p class="inbox-item-author">
                                        <span ng-bind-html="info.title|highlight:search_content"
                                              class="ng-binding"><span>有可能成为长期合作客户，多做关怀</span></span>
                                    </p>
                                    <p class="text-black ng-binding"><img
                                            ng-repeat="who_img in info.img_who track by $index"
                                            ng-src="/hc/?3b@1^5`@@1^5`@QNNxnMwADZ"
                                            class="img-circle img-xs ng-scope">
                                        <i class="fa fa-home m-l-5" ng-show="info.cu_name != ''"></i>
                                        <span ng-bind-html="info.cu_name|highlight:search_content"
                                              class="ng-binding"><span>华那商场</span></span> 刘经理
                                    </p>
                                    <p class="inbox-item-text ng-binding">
                                        陈默：收到
                                    </p>
                                    <p class="inbox-item-date ng-binding">2020-03-02</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="position-r">
                                <div class="inbox-item-img m-t-5 ">
                                        <span class="badge badge-xs badge-danger ng-hide"
                                              ng-show="info.redpoint"></span>
                                </div>
                                <div class="pull-left" style="width: calc(100% - 55px);">
                                    <p class="inbox-item-author">
                                        <span ng-bind-html="info.title|highlight:search_content"
                                              class="ng-binding"><span>有可能成为长期合作客户，多做关怀</span></span>
                                    </p>
                                    <p class="text-black ng-binding"><img
                                            ng-repeat="who_img in info.img_who track by $index"
                                            ng-src="/hc/?3b@1^5`@@1^5`@QNNxnMwADZ"
                                            class="img-circle img-xs ng-scope">
                                        <i class="fa fa-home m-l-5" ng-show="info.cu_name != ''"></i>
                                        <span ng-bind-html="info.cu_name|highlight:search_content"
                                              class="ng-binding"><span>华那商场</span></span> 刘经理
                                    </p>
                                    <p class="inbox-item-text ng-binding">
                                        陈默：收到
                                    </p>
                                    <p class="inbox-item-date ng-binding">2020-03-02</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="position-r">
                                <div class="inbox-item-img m-t-5 ">
                                        <span class="badge badge-xs badge-danger ng-hide"
                                              ng-show="info.redpoint"></span>
                                </div>
                                <div class="pull-left" style="width: calc(100% - 55px);">
                                    <p class="inbox-item-author">
                                        <span ng-bind-html="info.title|highlight:search_content"
                                              class="ng-binding"><span>有可能成为长期合作客户，多做关怀</span></span>
                                    </p>
                                    <p class="text-black ng-binding"><img
                                            ng-repeat="who_img in info.img_who track by $index"
                                            ng-src="/hc/?3b@1^5`@@1^5`@QNNxnMwADZ"
                                            class="img-circle img-xs ng-scope">
                                        <i class="fa fa-home m-l-5" ng-show="info.cu_name != ''"></i>
                                        <span ng-bind-html="info.cu_name|highlight:search_content"
                                              class="ng-binding"><span>华那商场</span></span> 刘经理
                                    </p>
                                    <p class="inbox-item-text ng-binding">
                                        陈默：收到
                                    </p>
                                    <p class="inbox-item-date ng-binding">2020-03-02</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="slimScrollBar"
                         style="background: rgb(0, 0, 0); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 380px;"></div>
                    <div class="slimScrollRail"
                         style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
    作者：1037566091@qq.com
    时间：2018-08-01
    描述：底部开始
-->
<div class="col-lg-12">
    <footer class="footer" style="text-align: center;padding-bottom: 20px;padding-top: 20px;">热线:<b>4008-8208-820 </b>
        &nbsp;&nbsp;网站:<b><a href="www.baidu.com" target="_blank">www.baidu.com</a></b> &nbsp;
        <a class="btn btn-danger btn-xs" href="#" onclick="">
            <i class="fa fa-whatsapp m-r-5"></i> 投诉&amp;问题
        </a>&nbsp;&nbsp;
        <a class="btn btn-default btn-xs" href="#" onclick="">
            <i class="fa fa-weixin m-r-5"></i>微客服
        </a>&nbsp;&nbsp;
        <a class="btn btn-primary btn-xs" href="#" onclick="">
            <i class="md md-speaker-notes m-r-5"></i>订阅号
        </a>
        <br>Copyright © 2004-2018 &nbsp;XXX技术有限公司&nbsp;&nbsp;
    </footer>
</div>

<!--
    作者：1037566091@qq.com
    时间：2018-07-30
    描述：右下角速建框
-->
<a id="checka" class="font-box position-plus badge-inverse f-s-12" href="javascript:void(0);" onclick="show();"
   style="z-index: 999;">
    <i class="fa fa-plus fa-3x m-t-15"></i>
    <br/>传送
</a>
<div class="position-plus new-box" id="pop" style="display: none;">
    <h5 class="text-muted" style="color: #ffffff;">
        <i onclick="hide()" class="fa fa-close text-muted pull-right inform" style="color: #ffffff;"></i>
        传送 </h5>
    <h4>
        <a href="/customer_view" onclick="hide()">
            <i class="fa fa-file-text"></i>客户</a>
        <a href="/contact_view" onclick="hide()">
            <i class="fa fa-file-text-o"></i>联系人</a>
    </h4>
    <h4>
        <a href="/to_ykm_quote_index" onclick="hide()">
            <i class="fa fa-file-text"></i>报价单</a>
        <a href="/to_ykm_sale_opp_index" onclick="hide()">
            <i class="fa fa-file-text-o"></i>销售机会</a>
    </h4>
    <h4>
        <a href="/sale/order" onclick="hide()">
            <i class="fa fa-file-text"></i>订单合同</a>
        <a href="/sale/order/to_detail" onclick="hide()">
            <i class="fa fa-file-text-o"></i>交付计划</a>
    </h4>
    <h4>
        <a href="/sale/mainPayBackDetail1" onclick="hide()">
            <i class="fa fa-yen"></i>回款</a>
        <a href="/sale/mainPlanPayBackDetail" onclick="hide()">
            <i class="fa fa-dollar"></i>计划回款</a>
    </h4>
</div>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/crm_main.js"></script>
<script src="/js/plugins/layer/layer.min.js"></script>
<script src="/js/plugins/flot/jquery.flot.js"></script>
<script src="/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="/js/lcy/content.js"></script>
<script src="/js/lcy/hmain.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script>
    $(function () {
        setCountMy();
        setCountFirm();
        setFF();
    });

    function setCountMy() {
        var userid = ${sessionScope.user.userId};
        ajax("/set_countmy?userId=" + userid, function (data) {
            $("#cus_my").text(data.cus);
            $("#opp_my").text(data.opp);
            // $("#order_my").text(data.opder);
            $("#pur_my").text(data.pur);
            $("#quote_my").text(data.quote);
            // $("#ship_my").text(data.ship);
        }, function () {
            swal("加载失败", result.msg, "error");
        }, "get", {})
    }

    function setCountFirm() {
        ajax("/set_countfirm", function (data) {
            $("#cus_firm").text(data.cus);
            $("#opp_firm").text(data.opp);
            // $("#order_firm").text(data.opder);
            $("#pur_firm").text(data.pur);
            $("#quote_firm").text(data.quote);
            // $("#ship_firm").text(data.ship);
        }, function () {
            swal("加载失败", result.msg, "error");
        }, "get", {})
    }
    
    function setFF() {
        ajax("/setFF", function (data) {
            $("#cus_top").html(data.cus);
            $("#sale_top").html(data.order);
        }, function () {
            swal("加载失败", result.msg, "error");
        }, "get", {})
    }

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            async: false,
            type: type,
            data: data,
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                    error()
                }
            }
        })
    }
</script>
</body>
</html>
