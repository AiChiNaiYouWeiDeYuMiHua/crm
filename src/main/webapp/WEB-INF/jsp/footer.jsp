<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/7
  Time: 9:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>底部</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/footer.css"/>
</head>
<body>
<hr/>
<footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn"
                                                                                 target="_blank">www.xtools.cn</a></b>
    &nbsp;
    <a class="btn btn-danger btn-xs" href="#"
       onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i
            class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
    <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i
            class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
    <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i
            class="md md-speaker-notes m-r-5"></i>订阅号 </a>
    <script>
        function showWX(n) {
            var url = '/ser_wx.html';
            var til = "微客服";
            var nm = "wkf";
            if (n == 1) {
                url = '/ser_dy.html';
                til = "订阅号";
                nm = "dyh";

            }
            var showWin = $.bwin({
                title: til,
                name: nm,
                modal: false,
                drag: true,
                autoSize: true,
                width: 275,
                height: 350,
                iframe: url
            });
        }
    </script>
    <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd
</footer>
</body>
</html>
