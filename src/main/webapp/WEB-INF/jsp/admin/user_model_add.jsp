<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/6
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 模态框（Modal）插件</title>

</head>

<body>
<div class="modal-header" style="border-bottom: none;">
    <button type="button" class="btn btn-default pull-right">
        <i class="fa fa-check"></i>
        保存
    </button>
    <h4 class="modal-title" id="myModalLabel">
        新增用户
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="xsjh" action="/admin/user_add" method="post">
        <div class="mybody">
            <div class="text-center-b">
                <span style="background-color: white; padding: 2px 10px;">用户信息</span>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            <span style="color: #ff0000; font-size: 16px;">*</span>用户姓名：
                        </label>
                        <div class="col-md-8">
                            <input id="userName" name="userName" class="form-control" required="required"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">用户密码：</label>
                        <div class="col-md-8">
                            <div class="form-control" disabled="disabled">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4"><span
                                style="color: #ff0000; font-size: 16px;">*</span>性别：</label>
                        <div class="col-md-8">
                            <div class="radio radio-info radio-inline">
                                <input id="userSex0" type="radio" value="男" name="userSex" checked>
                                <label> 男 </label>
                            </div>
                            <div class="radio radio-info radio-inline">
                                <input id="userSex1" type="radio" value="女" name="userSex">
                                <label> 女 </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            家庭住址：
                        </label>
                        <div class="col-md-8">
                            <input id="userAddress" name="userAddress" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            <span style="color: #ff0000; font-size: 16px;">*</span>联系方式：
                        </label>
                        <div class="col-md-8">
                            <input id="userTel" name="userTel" class="form-control" required="required"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            <span style="color: #ff0000; font-size: 16px;">*</span>邮箱：
                        </label>
                        <div class="col-md-8">
                            <input id="userEmail" name="userEmail" class="form-control" required="required"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4"><span
                                style="color: #ff0000; font-size: 16px;">*</span>任职时间：</label>
                        <div class="col-md-8">
                            <div class="input-append date form_datetime input-group">
                                <input id="hiredate" name="hiredate" class="form-control" type="date"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4">生日：</label>
                        <div class="col-md-8">
                            <div class="input-append date form_datetime input-group" id="userBirthday">
                                <input name="userBirthday" class="form-control" type="date"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group overflow">
                        <label class="control-label col-md-4">部门：</label>
                        <div class="col-md-8" style="position: relative;">
                            <select id="dept_id" name="tbDeptByDeptId.deptId" class="selectpicker"
                                    title="请选择部门">
                            </select>
                            <a href="#" target="_blank" data-placement="bottom" title="数据字典"
                               style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                <i class="fa fa-book"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            <span style="color: #ff0000; font-size: 16px;">*</span>身份证号码：
                        </label>
                        <div class="col-md-8">
                            <input id="userCode" name="userCode" class="form-control" required="required"/>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: 50px; "></div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
    <button class="btn btn-default" type="button" id="mysubmit"><i class="fa fa-check"></i>
        保存
    </button>
</div>


<script>

    $(function () {

        $(".selectpicker").selectpicker({
            noneSelectedText: '未选'
        });

        $('.selectpicker').selectpicker('refresh');

        $('.bootstrap-select').css('min-width', '213px');

        //下拉数据加载
        $.ajax({
            type: 'get',
            url: "/dept_list",
            async: false,
            dataType: 'json',
            success: function (datas) {//返回list数据并循环获取
                var select = $("#dept_id");
                for (var i = 0; i < datas.length; i++) {
                    select.append("<option value='" + datas[i].deptId + "'>"
                        + datas[i].text + "</option>");
                }
                $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });
        setForm();
        $("#xsjh").bootstrapValidator({

            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                userName: {
                    validators: {
                        notEmpty: {
                            message: '用户名不能为空'
                        },
                        stringLength: {
                            min: 2,
                            max: 5,
                            message: '用户名长度必须为2~5位之间'
                        },
                        regexp: {
                            regexp: /^[\u4e00-\u9fa5]{2,5}$/,
                            message: '用户名必须为汉字'
                        }
                    }
                },
                userTel: {
                    validators: {
                        notEmpty: {
                            message: '电话号码不能为空'
                        },
                        regexp: {
                            regexp: /^1[3|4|5|8][0-9]\d{8}$/,
                            message: '不是完整的11位手机号或者正确的手机号'
                        }
                    }
                },
                userEmail: {
                    validators: {
                        notEmpty: {
                            message: '邮箱地址不能为空'
                        },
                        regexp: {
                            regexp: /^([A-Za-z0-9_\-\.\u4e00-\u9fa5])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,8})$/,
                            message: '邮箱地址格式不正确'
                        }
                    }
                },
                userCode: {
                    validators: {
                        notEmpty: {
                            message: '身份证号码不能为空'
                        },
                        regexp: {
                            regexp: /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/,
                            message: '身份证号码格式不正确'
                        }
                    }
                }
            }
        });

        $("#model_add_user_lcy").on("hidden.bs.modal", function () {
            $(this).removeData("bs.modal");
        });

        $("#mysubmit").on("click", function () {
            //获取表单对象
            var bootstrapValidator = $("#xsjh").data('bootstrapValidator');
            //手动触发验证
            bootstrapValidator.validate();
            if (bootstrapValidator.isValid()) {
                /* $.post("/admin/user_add", {userName:'xxxx'}, function (v) {
                     alert(v);
                 });*/

                // alert("表单序列化结果："+$("#xsjh").serialize());
                $.ajax({
                    url: '/admin/user_add',
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    contentType: 'application/x-www-form-urlencoded',
                    data: $("#xsjh").serialize(),
                    success: function (result) {
                        if (result.code == 200) {
                            swal("成功", result.msg, "success");
                            $('#add_user').modal('hide');
                            //表单重置
                            $("#xsjh")[0].reset();
                            $("#table111").bootstrapTable('refresh');
                        }
                    },
                    error: function (data) {
                        swal("连接服务器错误", "", "error");
                    }
                });
            }
        });

        $("[data-toggle='tooltip' ] ").tooltip();

    });

    //表单初始化
    function setForm() {
        if (open_modal != null) {
            ajax("/admin/user_load?userId=" + open_modal, function (data) {
                $('#xsjh').append("<input class='hidden' name='userId' value='" + data.userId + "'>")
                $('input[name="userName"]').val(data.userName)
                // $('#updateTime').html(data.updateTime)
                $('input[name="userAddress"]').val(data.userAddress)
                redio($('input[name="userSex"]'), data.userSex);
                $('input[name="userTel"]').val(data.userTel)
                $('input[name="userEmail"]').val(data.userEmail)
                $('input[name="userCode"]').val(data.userCode)
                $('select[name="tbDeptByDeptId.deptId"]').selectpicker('val', data.tbDeptByDeptId.deptId)
                $('input[name="hiredate"]').val(data.hiredate)
                $('input[name="userBirthday"]').val(data.userBirthday)
            }, function () {
                swal("加载失败", result.msg, "error");
                $('#add_user').modal('hide')
            }, "get", {})
        }
    }

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            async: false,
            type: type,
            data: data,
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                    error()
                }
            }
        })
    }

    function redio(dom, is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        if (is == "男") {
            dom.eq(0).prop('checked', 'checked');
        } else {
            dom.eq(1).prop('checked', 'checked');
        }
    }
</script>

</body>

</html>