<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/6
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>审批中心</title>
    <link href="/css/old/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="/css/lcy/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css"/>
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <style type="text/css">
        #order_top {
            height: 60px;
        }

        #order_top ul {
            border-bottom: solid gainsboro 1px;
        }

        .ul_i {
            border-bottom: solid black 1px;
        }

        thead tr th {
            border-bottom: 2px solid #a7b7c3;
            background-color: #ebeff2;
        }

        body {
            font-family: "microsoft yahei";
            color: #777777;
            padding: 20px;
            background-color: #f4f8fb;
        }

        strong {
            color: black;
        }

        #content {
            top: -150px;
        }

        .fixed-table-container {
            /*top: -10px;*/
        }

        .columns {
            top: -45px;
        }

        .fixed-table-pagination {
            position: absolute;
            width: 98%;
            text-align: center;
            /*margin-top: -10px;*/
        }

        .pagination {
            margin-right: 49%;
        }

        .pagination {
        }

        thead {
            background-color: #269ABC;
            color: #000000;
            font-size: 12px;
        }

        tbody {
            font-size: 13px;
            color: #000000;
        }

        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 180px;
        }

        #dept_select {
            width: 200px !important;
        }
    </style>
</head>

<body>
<div class="container-fluid">
    <div class="row clearfix" id="order_top">
        <div class="col-md-12 column">
            <ul class="nav navbar-nav" style="width: 100%;">
                <li class="active ul_i" @click="addActive(this)">
                    <a href="#">审批中心</a>
                </li>
            </ul>
        </div>
    </div>

    <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：内容块
-->
    <div style="margin-top: 20px;">
        <div class="form-group col-sm-2" style="padding: 0;">
            <div>
                <i class="fa fa-filter pull-left" style="margin-right: 10px;padding: 0;margin-top: 10px;"></i>
                <div class="col-sm-9" style="padding: 0;">
                    <select class="selectpicker" id="select_type" onchange="search()">
                        <option selected value="0">全部数据</option>
                        <option value="订单">订单</option>
                        <option value="报价单">报价单</option>
                        <option value="采购单">采购单</option>
                    </select>
                </div>
            </div>
        </div>
        <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：form搜索
-->
        <div class="form-group col-sm-4" style="padding: 0;">
            <i class="fa fa-search col-sm-1" style="margin-top: 10px;"></i>
            <div class="input-group col-sm-8">
                <input type="text" name="userId" id="userId" class="form-control"
                       style="display: none">
                <input type="text" id="search-text" class="form-control" title="点击选择用户" onclick="chooseCus1()" readonly>
                <div class="input-group-btn">
                    <button type="button" class="btn waves-effect" onclick="search()" style="color: black;">申请人</button>
                </div>
            </div>
        </div>
        <div class="col-sm-3"></div>
        <div class="col-sm-2 pull-left text-center" style="padding: 0;margin-bottom: 15px;">

        </div>

        <div style="width: 100%;text-align: right;margin-top: 20px;">
            <a class="btn btn-default" disabled=""  href="javascript:void(0);"
               style="margin-right: 75px;"><i class="fa fa-plus-circle"></i></a>
        </div>
        <!--
            作者：1810761389@qq.com
            时间：2018-07-23
            描述：搜索状态条数
        -->
    </div>
    <div class="col-sm-2 pull-left" style="margin-top: 5px;">
        <span class="pull-left">审批</span>
        <button type="button" onclick="cancelKey()" id="cancel-search" class="hidden btn btn-danger btn-xs pull-left"
                data-toggle="tooltip" data-placement="bottom" title="解除搜索,显示全部数据">
            <i class="fa fa-reply"></i>解除搜索
        </button>
    </div>
    <!--
        作者：1810761389@qq.com
        时间：2018-07-31
        描述：table
    -->
    <div id="content">
        <table id="table111">

        </table>

    </div>
    <div class="col-sm-3 pull-right" style="text-align: right; margin-top: 10px;">
        <button onclick="print()" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom"
                title="打印">
            <i class="fa fa-print"></i>
        </button>
    </div>
</div>
<!--
作者：1810761389@qq.com
时间：2018-07-23
描述：统计
-->
<%--<div class="col-sm-12" style="margin-top: 50px;">--%>
    <%--<div class="col-sm-4" style="text-align: center;">--%>
        <%--<div class="col-sm-12" id="fenbu" style="width: 100%;height: 437px;"></div>--%>
        <%--<select class="selectpicker col-sm-9" data-live-search="true" style="text-align: center;width: 77%;"--%>
                <%--title="客户类型分布统计">--%>
            <%--<optgroup label="filter1">--%>
                <%--<option>option1</option>--%>
                <%--<option selected>客户类型分布统计</option>--%>
                <%--<option>option3</option>--%>
                <%--<option>option4</option>--%>
            <%--</optgroup>--%>
            <%--<optgroup label="filter2">--%>
                <%--<option>option1</option>--%>
                <%--<option>option2</option>--%>
                <%--<option>option3</option>--%>
                <%--<option>option4</option>--%>
            <%--</optgroup>--%>
        <%--</select>--%>
        <%--<!--<button type="button" style="text-align: center;width: 77%;" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="客户类型分布统计"><span class="filter-option pull-left">客户类型分布统计</span>&nbsp;<span class="bs-caret pull-right"><span class="caret"></span></span></button>-->--%>
    <%--</div>--%>
    <%--<div class="col-lg-4" style="text-align: center;">--%>

        <%--<div class="col-sm-4" id="top" style="width: 100%;height: 437px;"></div>--%>
        <%--<select class="selectpicker col-sm-9" data-live-search="true" style="text-align: center;width: 77%;"--%>
                <%--title="客户类型分布统计">--%>
            <%--<optgroup label="filter1">--%>
                <%--<option>option1</option>--%>
                <%--<option selected>大客户top20(合同额)</option>--%>
                <%--<option>option3</option>--%>
                <%--<option>option4</option>--%>
            <%--</optgroup>--%>
            <%--<optgroup label="filter2">--%>
                <%--<option>option1</option>--%>
                <%--<option>option2</option>--%>
                <%--<option>option3</option>--%>
                <%--<option>option4</option>--%>
            <%--</optgroup>--%>
        <%--</select>--%>
        <%--<!--<button type="button" style="text-align: center; width: 77%;" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="客户类型分布统计"><span class="filter-option pull-left">客户类型分布统计</span>&nbsp;<span class="bs-caret pull-right"><span class="caret"></span></span></button>-->--%>
    <%--</div>--%>
    <%--<div class="col-lg-4" style="text-align: center;">--%>
        <%--<div class="col-sm-4 pull-right" id="gongju" style="width: 100%;height: 437px;"></div>--%>
        <%--<select class="selectpicker col-sm-9" data-live-search="true" style="text-align: center;width: 77%;"--%>
                <%--title="客户类型分布统计">--%>
            <%--<optgroup label="filter1">--%>
                <%--<option>option1</option>--%>
                <%--<option selected>客户创建数量月度/类型统计</option>--%>
                <%--<option>option3</option>--%>
                <%--<option>option4</option>--%>
            <%--</optgroup>--%>
            <%--<optgroup label="filter2">--%>
                <%--<option>option1</option>--%>
                <%--<option>option2</option>--%>
                <%--<option>option3</option>--%>
                <%--<option>option4</option>--%>
            <%--</optgroup>--%>
        <%--</select>--%>
        <%--<!--<button type="button" style="text-align: center; width: 77%;" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="客户类型分布统计"><span class="filter-option pull-left">客户类型分布统计</span>&nbsp;<span class="bs-caret pull-right"><span class="caret"></span></span></button>-->--%>
    <%--</div>--%>
<%--</div>--%>

<br>
<!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：页尾
-->
<div class="col-lg-12">
    <footer class="footer" style="text-align: center;padding-bottom: 20px;padding-top: 20px;">热线:<b>4008-8208-820 </b>
        &nbsp;&nbsp;网站:<b><a href="www.baidu.com" target="_blank">www.baidu.com</a></b> &nbsp;
        <a class="btn btn-danger btn-xs" href="#" onclick="">
            <i class="fa fa-whatsapp m-r-5"></i> 投诉&amp;问题
        </a>&nbsp;&nbsp;
        <a class="btn btn-default btn-xs" href="#" onclick="">
            <i class="fa fa-weixin m-r-5"></i>微客服
        </a>&nbsp;&nbsp;
        <a class="btn btn-primary btn-xs" href="#" onclick="">
            <i class="md md-speaker-notes m-r-5"></i>订阅号
        </a>
        <br>Copyright © 2004-2018 &nbsp;XXX技术有限公司&nbsp;&nbsp;
    </footer>
</div>

<!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：新增审批
-->
<div class="modal fade" id="add_approval" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent" style="background-color: #ebeff2;">

        </div>
    </div>
</div>
<!-- 审批操作 -->
<div class="modal fade" id="approval_option" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent" style="background-color: #ebeff2;">

        </div>
    </div>
</div>
<!--
    作者：1810761389@qq.com
    时间：2018-07-31
    描述：高级查询
-->
<div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-labelledby="open"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>

<script type="text/javascript" src="/js/resjs/fenbu.js"></script>
<script type="text/javascript" src="/js/resjs/top.js"></script>
<script type="text/javascript" src="/js/resjs/util.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/moment.js/2.22.1/moment.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="/js/resjs/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<%--<script src="//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>--%>
<script src="/js/tableExport.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<!--excel-->
<script src="/js/lcy/table_approval.js"></script>
<script src="/js/ykm/money.js"></script>
<script src="/js/lcy/bootstrap-select.min.js"></script>
<%--<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>--%>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<%--<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>--%>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="/js/resjs/printThis.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script>
    $(function () {

        $("#example").popover();
        $("[data-toggle='tooltip']").tooltip();
        add_modal();
        //回车时间
        $('#search-text').bind('keypress', function (event) {

            if (event.keyCode == 13) {
                search()
            }

        });

    });

    var open_modal;
    var order;

    //清除模态框数据
    $("#add_approval").on('hidden.bs.modal', function () {
        open_modal = null;
        order = null;
        $(this).removeData('bs.modal');
    });
    $("#approval_option").on('hidden.bs.modal', function () {
        open_modal = null;
        order = null;
        $(this).removeData('bs.modal');
    });



    //打开审批模态框
    function approvalOption(approvalId) {
        open_modal = approvalId;
        // alert("你要想操作的id是：" + open_modal);
        $("#approval_option").modal({
            remote: '/to_approval_option_modal'
        });
    }

    function chooseCus1() {
        var a = $("<a href='/admin/to_user_check' target='_blank'>Apple</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent('click', true, true);
        a.dispatchEvent(e);
    }

    function getUser(userId, userName) {
        $("input[name='userId']").val(userId);
        $("#search-text").val(userName);
        $.ajax({
            url: '/admin/user_load?userId=' + $("input[name='userId']").val(),
            method: 'get',
            async: true,
            success: function (data1) {
                $("#cotsName").empty();
                for (var i = 0; i < data1.length; i++) {
                    $('#cotsName').append("<option value='" + data1[i].userId + "'>" + data1[i].userName + "</option>");
                    $('#cotsName').selectpicker('refresh');
                }
                $('#cotsName').selectpicker('refresh');
            }
        });
    }

    //查询
    function search() {
        if ($('#select_type').val() != 0 || $('#search-text').val().length > 0) {
            var userId = $('#userId').val();
            var type = $('#select_type').val();
            $('#cancel-search').removeClass('hidden');
            if ($('#select_type').val() != 0 && $('#search-text').val().length == 0) {
                $("#table111").bootstrapTable('refresh', {url: "/query_approval_all_type?type=" + type});
            } else if ($('#select_type').val() == 0 && $('#search-text').val().length > 0) {
                $("#table111").bootstrapTable('refresh', {url: "/query_approval_all_user?userId=" + userId});
            } else if ($('#select_type').val() != 0 && $('#search-text').val().length > 0) {
                $("#table111").bootstrapTable('refresh', {url: "/query_approval_all_user_type?userId=" + userId + "&type=" + type});
            }
        } else {
            $('#cancel-search').addClass('hidden');
            $("#table111").bootstrapTable('refresh', {url: "/query_approval_all"});
        }
    }

    //取消搜索
    function cancelKey() {
        $('#select_type').selectpicker('val', '0');
        $('#search-text').val("");
        search();

    }

    function print() {
        /* Act on the event */
        $("#table111").printThis({
            debug: false,
            importCSS: false,
            importStyle: false,
            printContainer: true, //打印容器
            loadCSS: "css/bootstrap-table.css",		//需要加载的css样式
            pageTitle: "sn",
            removeInline: false,
            printDelay: 333, //打印时延
            header: null,
            formValues: false
        });
    }
</script>
<script>
    $(function () {
        $(".pagination-detail").before("<div><button class='btn btn-default pull-left' style='margin-top: 10px;margin-right: 5px;color:black'><i class='fa fa-trash'></i></button></div>");
    })
</script>
</body>
</html>