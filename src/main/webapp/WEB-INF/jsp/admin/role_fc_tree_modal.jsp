<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/6
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 模态框（Modal）插件</title>

</head>

<body>
        <div class="modal-header" style="border-bottom: none;">
            <button type="button" class="btn btn-default pull-right">
                <i class="fa fa-check"></i>
                保存
            </button>
            <h4 class="modal-title" id="myModalLabel">
                角色授权
            </h4>
            <hr class="boder-t-a"/>
        </div>
        <div class="modal-body" style="padding-top: 0px;">
            <form id="xsjh" method="post">
                <div id="tree"></div>
                <input style="display: none" name="roleId" id="roleId">
                <input style="display: none" name="fcIds" id="fcIds">
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
            <button class="btn btn-default" type="button" id="mysubmit"><i class="fa fa-check"></i>
                保存
            </button>
        </div>

<script>
    $(function () {
        $("#role_grant_fun").on('hidden.bs.modal',function () {
            open_modal = null;
            $(this).removeData('bs.modal');
        });
        $.ajax({
            type: "Get",
            url: "/get_function_nodes?roleId=" + open_modal,
            dataType: "json",
            success: function (result) {
                // alert(result);
                $('#tree').treeview({
                    data: result, // 数据源
                    showCheckbox: true, //是否显示复选框
                    highlightSelected: false, //是否高亮选中
                    // nodeIcon: 'glyphicon glyphicon-globe',
                    // emptyIcon: '', //没有子节点的节点图标
                    // selectedIcon: 'glyphicon glyphicon-globe',
                    borderColor: 'default',
                    multiSelect: true, //多选
                    color: "#000000",
                    backColor: "default",
                    /*state: {
                        checked: function (result) {
                            for (var i; i < result.length; i++) {
                                if (result[i].nodes.length > 0) {
                                    for (var j; j < result[i].nodes.length; j++) {
                                        if (result[i].nodes[j].selected.toString.toLowerCase() === "true") {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    },*/
                    onNodeChecked: nodeChecked,   //级联选择
                    onNodeUnchecked: nodeUnchecked   //级联选择
                });
                $('#tree').treeview('collapseAll', {silent: true});
            },
            error: function () {
                alert("树形结构加载失败！")
            }
        });

        setTree();
        //初始化加载时的选中设置
        function setTree() {
            var idarr = [];
            $.ajax({
                type: "Get",
                url: "/get_function_nodes?roleId=" + open_modal,
                dataType: "json",
                success: function (result) {
                    // alert(result[0].nodes[0].selected.toString());
                    // alert("111：" + result.length);
                    for (var i = 0; i < result.length; i++) {
                        // alert("111：" + i);
                        if (result[i].nodes.length > 0) {
                            // alert("1112：" + i);
                            for (var j = 0; j < result[i].nodes.length; j++) {
                                // alert("111：" + i + "," + j);
                                if (result[i].nodes[j].selected.toString() == "true") {
                                    // alert("1112：" + i + "," + j);
                                    idarr.push(result[i].nodes[j].nodeid);
                                }
                            }
                        }
                    }
                    // alert("初始化：" + idarr);
                    var bb = $('#tree').treeview('getEnabled');
                    // alert(bb.length);
                    for (var m = 0; m < bb.length; m++) {
                        for (var n=0;n<idarr.length;n++){
                            if (bb[m].nodeid == idarr[n]){
                                nodeChecked(event, bb[m]);
                            }
                        }
                    }
                },
                error: function () {
                    alert("初始化数据加载失败！")
                }
            });
        }

        //角色授权
        $("#mysubmit").click(function () {
            var fcids = $("#tree").treeview('getChecked');
            // displayProp(fcids);
            var fcIds = [];
            for (var i = 0; i < fcids.length; i++) {
                if (fcids[i].nodeid != undefined) {
                    fcIds.push(fcids[i].nodeid);
                }
            }
            $("#roleId").val(open_modal);
            $("#fcIds").val(fcIds);
            // var ids = new Object();
            // ids.roleId = open_modal;
            // ids.fcIds = fcIds;
            // alert(typeof fcIds);
            $.ajax({
                url: 'grant_fc_to_role',
                type: "POST",
                async: false,
                dataType: 'json',
                contentType: 'application/x-www-form-urlencoded',
                data: $("#xsjh").serialize(),
                success: function (result) {
                    if (result.code == 200) {
                        swal("成功", result.msg, "success");
                        $('#role_grant_fun').modal('hide');
                        //表单重置
                        $("#table111").bootstrapTable('refresh');
                    }
                },
                error: function (data) {
                    swal("连接服务器错误", "", "error");
                }
            });
        });

        //查看对象的所有属性和方法
        function displayProp(obj) {
            var names = "";
            for (var name in obj) {
                names += name + ": " + obj[name] + ", ";
            }
            alert(names);
        }


        //级联选择
        var nodeCheckedSilent = false;

        function nodeChecked(event, node) {
            if (nodeCheckedSilent) {
                return;
            }
            nodeCheckedSilent = true;
            checkAllParent(node);
            checkAllSon(node);
            nodeCheckedSilent = false;
        }

        var nodeUncheckedSilent = false;

        function nodeUnchecked(event, node) {
            if (nodeUncheckedSilent)
                return;
            nodeUncheckedSilent = true;
            uncheckAllParent(node);
            uncheckAllSon(node);
            nodeUncheckedSilent = false;
        }

        //选中全部父节点
        function checkAllParent(node) {
            // $('#tree').treeview('checkNode', node.nodeId, { silent: true });
            var parentNode = $('#tree').treeview('getParent', node.nodeId);
            var siblings = $('#tree').treeview('getSiblings', node.nodeId);
            if (!("nodeId" in parentNode)) {
                return;
            }
            var isAllChecked = true;  //是否全部没选中
            if (node.nodes != null && node.nodes.length > 0) {
                for (var i in node.nodes) {
                    if (!node.nodes[i].state.checked) {
                        isAllChecked = false;
                        break;
                    }
                }
            }
            for (var j in siblings) {
                if (!siblings[j].state.checked) {
                    isAllChecked = false;
                    break;
                }
            }
            if (isAllChecked) {
                // checkAllParent(parentNode);
                $('#tree').treeview('checkNode', parentNode, {silent: true});
            }
            //else {
            //    checkAllParent(parentNode);
            //}
        }

        //取消全部父节点
        function uncheckAllParent(node) {
            $('#tree').treeview('uncheckNode', node.nodeId, {silent: true});
            var siblings = $('#tree').treeview('getSiblings', node.nodeId);
            var parentNode = $('#tree').treeview('getParent', node.nodeId);
            if (!("nodeId" in parentNode)) {
                return;
            }
            var isAllUnchecked = true;  //是否全部没选中
            //for (var i in siblings) {
            //    if (siblings[i].state.checked) {
            //        isAllUnchecked = false;
            //        break;
            //    }
            //}
            if (isAllUnchecked) {
                uncheckAllParent(parentNode);
            }

        }

        //级联选中所有子节点
        function checkAllSon(node) {
            $('#tree').treeview('checkNode', node.nodeId, {silent: true});
            if (node.nodes != null && node.nodes.length > 0) {
                for (var i in node.nodes) {
                    checkAllSon(node.nodes[i]);
                }
            }
        }

        //级联取消所有子节点
        function uncheckAllSon(node) {
            $('#tree').treeview('uncheckNode', node.nodeId, {silent: true});
            if (node.nodes != null && node.nodes.length > 0) {
                for (var i in node.nodes) {
                    uncheckAllSon(node.nodes[i]);
                }
            }
        }

        //获取选中的子节点数据
        function getChildChecked(node) {
            for (var i in node) {
                idList += node[i].Id + ",";
                //var _index=node[i].text.indexOf("(");
                //var text = node[i].text.substring(0,_index+1)
                //areaList += text + ",";
                var r = /\((\d+)\)/;
                var n = r.exec(node[i].text);
                num += parseInt(n[1]);
                if (node[i].nodes != null && node[i].nodes.length > 0) {
                    //有子节点，移除父节点
                    var nodeid = node[i].Id;
                    idList = idList.toString().replace(nodeid, '-1');
                    num -= parseInt(n[1]);
                }
            }
        }

    });

</script>

</body>

</html>