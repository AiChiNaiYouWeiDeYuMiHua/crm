<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/16
  Time: 21:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="modal-header" style="border-bottom: none;">
    <button type="button" class="btn btn-default pull-right">
        <i class="fa fa-check"></i>
        保存
    </button>
    <h4 class="modal-title" id="myModalLabel">
        选择用户
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <div>
        <input id="selected_user" type="text">
        <a class="btn btn-default" id="add_btn1" href="javascript:void(0);"
           style="margin-right: 75px;"><i class="fa fa-plus-circle"></i>选择</a>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
    <button class="btn btn-default" type="button" id="mysubmit"><i class="fa fa-check"></i>
        保存
    </button>
</div>
<script>
    $(function () {

        $("#example").popover();
        $("[data-toggle='tooltip']").tooltip();
        add_modal();
    });

    //清除模态框数据
    $("#select_user").on('hidden.bs.modal', function () {
        $(this).removeData();
    });

    function add_modal() {
        $("#add_btn1").click(function () {
            $('#select_user').modal({
                remote: '/admin/to_select_user'
            })
        });
    }
    $("#select_user").on('show.bs.modal', function () {
        $(this).css('z-index', '1600');
        // $(".modal-dialog").css('z-index', '3000');
    });
</script>
</body>
</html>
