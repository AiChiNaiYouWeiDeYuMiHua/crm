<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/9
  Time: 20:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>部门管理</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <!-- [if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![end if]-->
</head>
<body>
<div id="tree" class="col-sm-2"></div>

<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-treeview.js"></script>
<script src="/js/lcy/dept_tree.js"></script>
<script type="text/javascript">
    //选中/取消父节点时选中/取消所有子节点
    function getChildtextArr(node) {
        var ts = [];
        if (node.nodes) {
            for (x in node.nodes) {
                ts.push(node.nodes[x].text);
                if (node.nodes[x].nodes) {
                    var getNodeDieDai = getChildtextArr(node.nodes[x]);
                    for (j in getNodeDieDai) {
                        ts.push(getNodeDieDai[j]);
                    }
                }
            }
        } else {
            ts.push(node.text);
        }
        return ts;
    }

    //选中所有子节点时选中父节点
    function setParentNodeCheck(node) {
        var parentNode = $("#tree").treeview("getNode", node.parentId);
        if (parentNode.nodes) {
            var checkedCount = 0;
            for (x in parentNode.nodes) {
                if (parentNode.nodes[x].state.checked) {
                    checkedCount++;
                } else {
                    break;
                }
            }
            if (checkedCount === parentNode.nodes.length) {
                $("#tree").treeview("checkNode", parentNode.text);
                setParentNodeCheck(parentNode);
            }
        }
    }

    $(function () {
        $.ajax({
            type: "Get",
            url: '/dept_user_tree',
            dataType: "json",
            success: function (result) {
                $('#tree').treeview({
                    data: result.list,         // 数据源
                    showCheckbox: true,   //是否显示复选框
                    highlightSelected: true,    //是否高亮选中
                    multiSelect: true,    //多选
                    levels: 2,
                    enableLinks: true,//必须在节点属性给出href属性
                    color: "#010A0E",
                    onNodeChecked: function (event, node) {
                        var selectNodes = getChildtextArr(node); //获取所有子节点
                        if (selectNodes) { //子节点不为空，则选中所有子节点
                            $('#tree').treeview('checkNode', [selectNodes, {silent: true}]);
                        }
                    },
                    onNodeUnchecked: function (event, node) { //取消选中节点
                        var selectNodes = getChildtextArr(node); //获取所有子节点
                        if (selectNodes) { //子节点不为空，则取消选中所有子节点
                            $('#tree').treeview('uncheckNode', [selectNodes, {silent: true}]);
                        }
                    },

                    onNodeExpanded: function (event, data) {

                    },

                    onNodeSelected: function (event, data) {
                        //alert(data.text);
                    }

                });
            },
            error: function () {
                alert("菜单加载失败！")
            }
        });
    })
</script>
</body>
</html>
