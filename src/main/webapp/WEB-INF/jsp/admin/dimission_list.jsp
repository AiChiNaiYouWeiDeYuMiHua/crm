<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/10
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link href="/css/old/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="/css/lcy/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css"/>
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
    <link rel="stylesheet" href="/css/lcy/bootstrap.css"/>
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="/css/lcy/bootstrap.css"/>
    <link rel="stylesheet" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/x-editable/1.5.1/bootstrap-editable/css/bootstrap-editable.css"
          rel="stylesheet">
    <style type="text/css">
        #order_top {
            height: 60px;
        }

        #order_top ul {
            border-bottom: solid gainsboro 1px;
        }

        .ul_i {
            border-bottom: solid black 1px;
        }

        thead tr th {
            border-bottom: 2px solid #a7b7c3;
            background-color: #ebeff2;
        }

        body {
            font-family: "microsoft yahei";
            color: #777777;
            padding: 20px;
            background-color: #f4f8fb;
        }

        .fixed-table-body {
            height: auto;
        }

        strong {
            color: black;
        }

        #content {
            top: -150px;
        }

        .fixed-table-container {
            /*top: -10px;*/
        }

        .columns {
            top: 0px;
        }

        .fixed-table-pagination {
            position: absolute;
            width: 98%;
            text-align: center;
            /*margin-top: -10px;*/
        }

        .pagination {
            margin-right: 49%;
        }

        .pagination {
        }

        thead {
            background-color: #269ABC;
            color: #000000;
            font-size: 12px;
        }

        tbody {
            font-size: 13px;
            color: #000000;
        }

        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 180px;
        }

        #dept_select {
            width: 200px !important;
        }

        .list-group li {
            background-color: #ebeff2;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row clearfix" id="order_top">
        <div class="col-md-12 column">
            <ul class="nav navbar-nav" style="width: 100%;">
                <li class="active ul_i" @click="addActive(this)">
                    <a href="#">系统参数</a>
                </li>
            </ul>
        </div>
    </div>

    <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：内容块
-->
    <%--<div style="margin-top: 20px;">--%>
    <%--<div class="form-group col-sm-2" style="padding: 0;">--%>
    <%--<div>--%>
    <%--<i class="fa fa-filter pull-left" style="margin-right: 10px;padding: 0;margin-top: 10px;"></i>--%>
    <%--<div class="col-sm-9" style="padding: 0;">--%>
    <%--<select class="selectpicker" id="dept_select">--%>
    <%--</select>--%>
    <%--</div>--%>
    <%--</div>--%>
    <%--</div>--%>
    <!--
作者：1810761389@qq.com
时间：2018-07-23
描述：form搜索
-->

    <div class="col-sm-3"></div>
    <div class="col-sm-2 pull-left text-center" style="padding: 0;margin-bottom: 15px;">

    </div>

    <!--
        作者：1810761389@qq.com
        时间：2018-07-23
        描述：搜索状态条数
    -->
</div>

<!--
    作者：1810761389@qq.com
    时间：2018-07-31
    描述：table
-->

<div id="content">
    <table id="table111">

    </table>

</div>

<div class="col-sm-3 pull-right" style="text-align: right; margin-top: 10px;">
    <button onclick="print()" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom"
            title="打印">
        <i class="fa fa-print"></i>
    </button>
</div>
</div>
<!--
作者：1810761389@qq.com
时间：2018-07-23
描述：统计
-->
<div class="col-sm-12" style="margin-top: 50px;">
    <div class="col-sm-4" style="text-align: center;">
        <div class="col-sm-12" id="fenbu" style="width: 100%;height: 437px;"></div>
        <select class="selectpicker col-sm-9" data-live-search="true" style="text-align: center;width: 77%;"
                title="客户类型分布统计">
            <optgroup label="filter1">
                <option>option1</option>
                <option selected>客户类型分布统计</option>
                <option>option3</option>
                <option>option4</option>
            </optgroup>
            <optgroup label="filter2">
                <option>option1</option>
                <option>option2</option>
                <option>option3</option>
                <option>option4</option>
            </optgroup>
        </select>
        <!--<button type="button" style="text-align: center;width: 77%;" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="客户类型分布统计"><span class="filter-option pull-left">客户类型分布统计</span>&nbsp;<span class="bs-caret pull-right"><span class="caret"></span></span></button>-->
    </div>
    <div class="col-lg-4" style="text-align: center;">

        <div class="col-sm-12" id="top" style="width: 100%;height: 437px;"></div>
        <select class="selectpicker col-sm-9" data-live-search="true" style="text-align: center;width: 77%;"
                title="客户类型分布统计">
            <optgroup label="filter1">
                <option>option1</option>
                <option selected>大客户top20(合同额)</option>
                <option>option3</option>
                <option>option4</option>
            </optgroup>
            <optgroup label="filter2">
                <option>option1</option>
                <option>option2</option>
                <option>option3</option>
                <option>option4</option>
            </optgroup>
        </select>
        <!--<button type="button" style="text-align: center; width: 77%;" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="客户类型分布统计"><span class="filter-option pull-left">客户类型分布统计</span>&nbsp;<span class="bs-caret pull-right"><span class="caret"></span></span></button>-->
    </div>
    <div class="col-lg-4" style="text-align: center;">
        <div class="col-sm-4 pull-right" id="gongju" style="width: 100%;height: 437px;"></div>
        <select class="selectpicker col-sm-9" data-live-search="true" style="text-align: center;width: 77%;"
                title="客户类型分布统计">
            <optgroup label="filter1">
                <option>option1</option>
                <option selected>客户创建数量月度/类型统计</option>
                <option>option3</option>
                <option>option4</option>
            </optgroup>
            <optgroup label="filter2">
                <option>option1</option>
                <option>option2</option>
                <option>option3</option>
                <option>option4</option>
            </optgroup>
        </select>
        <!--<button type="button" style="text-align: center; width: 77%;" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="客户类型分布统计"><span class="filter-option pull-left">客户类型分布统计</span>&nbsp;<span class="bs-caret pull-right"><span class="caret"></span></span></button>-->
    </div>
</div>

<br>
<!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：页尾
-->

<div class="col-lg-12">
    <footer class="footer" style="text-align: center;padding-bottom: 20px;padding-top: 20px;">
        热线:<b>4008-8208-820 </b>
        &nbsp;&nbsp;网站:<b><a href="www.baidu.com" target="_blank">www.baidu.com</a></b> &nbsp;
        <a class="btn btn-danger btn-xs" href="#" onclick="">
            <i class="fa fa-whatsapp m-r-5"></i> 投诉&amp;问题
        </a>&nbsp;&nbsp;
        <a class="btn btn-default btn-xs" href="#" onclick="">
            <i class="fa fa-weixin m-r-5"></i>微客服
        </a>&nbsp;&nbsp;
        <a class="btn btn-primary btn-xs" href="#" onclick="">
            <i class="md md-speaker-notes m-r-5"></i>订阅号
        </a>
        <br>Copyright © 2004-2018 &nbsp;XXX技术有限公司&nbsp;&nbsp;
    </footer>
</div>


<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.bootcss.com/moment.js/2.22.1/moment.js"></script>
<script type="text/javascript" src="/js/resjs/fenbu.js"></script>
<script type="text/javascript" src="/js/resjs/top.js"></script>
<script type="text/javascript" src="/js/resjs/util.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/lcy/bootstrap-treeview.js"></script>
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="/js/resjs/bootstrap-table.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<%--<script src="//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>--%>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<!--excel-->
<%--<script src="/js/lcy/table_dimission.js"></script>--%>
<script src="/js/ykm/money.js"></script>
<script src="/js/lcy/bootstrap-select.min.js"></script>
<%--<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>--%>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<%--<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>--%>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="/js/resjs/printThis.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="https://cdn.bootcss.com/x-editable/1.5.1/bootstrap-editable/js/bootstrap-editable.js"></script>
<script>
    var open_modal;
    var typeid;
    $(function () {
        $("#example").popover();
        $("[data-toggle='tooltip']").tooltip();
        setUrl();
    });

    function setUrl() {
        typeid = ${sessionScope.typeId};
    }

    var curRow = {};
    $(function () {
        $("#table111").bootstrapTable({
            toolbar: "#toolbar",
            idField: "Id",
            pagination: true,
            showRefresh: true,
            search: true,
            clickToSelect: true,
            queryParams: function (param) {
                return {};
            },
            url: "/get_dimission?typeId=" + typeid,
            columns: [{
                checkbox: true
            }, {
                field: "dimissionName",
                title: "参数名称",
                formatter: function (value, row, index) {
                    return "<a href=\"#\" name=\"dimissionName\" data-type=\"text\" data-pk=\"" + row.Id + "\" data-title=\"参数名称\">" + value + "</a>";
                }
            }, {
                field: "tbDimissionTypeByDimiTypeId.dimiTypeName",
                title: "参数类型",
            }],
            onClickRow: function (row, $element) {
                curRow = row;
            },
            onLoadSuccess: function (aa, bb, cc) {
                $("#tb_user a").editable({
                    url: function (params) {
                        var sName = $(this).attr("name");
                        curRow[sName] = params.value;
                        $.ajax({
                            type: 'POST',
                            url: "/dimi_modify",
                            data: curRow,
                            dataType: 'JSON',
                            success: function (data, textStatus, jqXHR) {
                                alert('保存成功！');
                            },
                            error: function () {
                                alert("error");
                            }
                        });
                    },
                    type: 'text'
                });
            },
        });
    });
    $(function () {
        $('#table111').on('click-row.bs.table', function (e, row, element) {
            //$(element).css({"color":"blue","font-size":"16px;"});
            console.log(row);

        });
    })

    //单元格空处理
    function isnull(value, row, index) {
        if (value == null)
            return "";
        else
            return value;
    }

    //表格自动序号
    function tableIndexNum(index) {
        var currentPage = $(".page-item.active").find('a').text();
        return Number(index + 1 + eval((currentPage - 1) * 25));
    }

    //表单数据转json对象
    function f(form) {
        var d = {};
        var t = form.serializeArray();
        $.each(t, function () {
            if (this.value != null && this.value.length > 0)
                d[this.name] = this.value;
        });
        return d;
    }

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            type: type,
            data: data,
            contentType: 'application/x-www-form-urlencoded',
            dataType: 'json',
            success: function (result) {
                /*alert("发了十多年房价数据表空间");*/
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                    error()
                }
            }
        })
    }

    function sumFormatter(data) {
        field = this.field;
        var sum1 = data.reduce(function (sum, row) {
            return sum + (+row[field]);
        }, 0);
        return sum1;
    }

    function print() {
        /* Act on the event */
        $("#table111").printThis({
            debug: false,
            importCSS: false,
            importStyle: false,
            printContainer: true, //打印容器
            loadCSS: "css/bootstrap-table.css",		//需要加载的css样式
            pageTitle: "sn",
            removeInline: false,
            printDelay: 333, //打印时延
            header: null,
            formValues: false
        });
    }
</script>
<script>
    $(function () {
        $(".pagination-detail").before("<div><button class='btn btn-default pull-left' style='margin-top: 10px;margin-right: 5px;color:black'><i class='fa fa-trash'></i></button></div>");
    })
</script>
<%--<script type="text/javascript" color="187,225,234" opacity='0.7' zIndex="-2" count="400"--%>
<%--src="/js/plugins/canvas/canvas-nest.js"></script>--%>
<%--<script type="text/javascript" src="/js/plugins/canvas/canvas-nest.umd.js"></script>--%>
</body>
</html>
