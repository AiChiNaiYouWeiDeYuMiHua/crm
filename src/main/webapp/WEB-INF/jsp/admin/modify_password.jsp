<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/27
  Time: 21:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>修改密码</title>
    <link href="/css/old/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="/css/lcy/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css"/>
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
    <style type="text/css">
        #order_top {
            height: 60px;
        }

        #order_top ul {
            border-bottom: solid gainsboro 1px;
        }

        .ul_i {
            border-bottom: solid black 1px;
        }

        thead tr th {
            border-bottom: 2px solid #a7b7c3;
            background-color: #ebeff2;
        }

        body {
            font-family: "microsoft yahei";
            color: #777777;
            padding: 20px;
            background-color: #f4f8fb;
        }

        strong {
            color: black;
        }

        #content {
            top: -150px;
        }

        .fixed-table-container {
            /*top: -10px;*/
        }

        .columns {
            top: -45px;
        }

        .fixed-table-pagination {
            position: absolute;
            width: 98%;
            text-align: center;
            /*margin-top: -10px;*/
        }

        .pagination {
            margin-right: 49%;
        }

        .pagination {
        }

        thead {
            background-color: #269ABC;
            color: #000000;
            font-size: 12px;
        }

        tbody {
            font-size: 13px;
            color: #000000;
        }

        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 180px;
        }

        #dept_select {
            width: 200px !important;
        }

        .row {
            margin-top: 20px;
        }

        .control-label {
            text-align: center;
        }
    </style>
</head>
<body>
<div style="margin: 0 auto;font-size: 12px;">
    <form id="xsjh" action="/admin/user_add" method="post">
        <div class="mybody">
            <div style="height: 50px; "></div>
            <div class="row">
                <div class="col-md-7 col-md-offset-2">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <span style="color: #ff0000; font-size: 16px;">*</span>原密码：
                        </label>
                        <div class="col-md-9">
                            <input type="password" id="userPassOld" name="userPassOld" class="form-control"
                                   required="required"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 col-md-offset-2">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <span style="color: #ff0000; font-size: 16px;">*</span>新密码：
                        </label>
                        <div class="col-md-9">
                            <input type="password" id="userPassNew1" name="userPassNew1" class="form-control"
                                   required="required"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 col-md-offset-2">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <span style="color: #ff0000; font-size: 16px;">*</span>密码确认：
                        </label>
                        <div class="col-md-9">
                            <input type="password" id="userPassNew2" name="userPassNew2" class="form-control"
                                   required="required"/>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: 50px; "></div>
        </div>
    </form>
</div>
<div class="modal-footer" style="text-align: center">
    <button type="button" class="btn btn-primary" id="btn_cancer">取消</button>
    <g:g id="29">
        <button class="btn btn-default" type="button" id="mysubmit"><i class="fa fa-check"></i>
            保存
        </button>
    </g:g>
</div>
<div class="col-lg-12">
    <footer class="footer" style="text-align: center;padding-bottom: 20px;padding-top: 20px;">热线:<b>4008-8208-820 </b>
        &nbsp;&nbsp;网站:<b><a href="www.baidu.com" target="_blank">www.baidu.com</a></b> &nbsp;
        <a class="btn btn-danger btn-xs" href="#" onclick="">
            <i class="fa fa-whatsapp m-r-5"></i> 投诉&amp;问题
        </a>&nbsp;&nbsp;
        <a class="btn btn-default btn-xs" href="#" onclick="">
            <i class="fa fa-weixin m-r-5"></i>微客服
        </a>&nbsp;&nbsp;
        <a class="btn btn-primary btn-xs" href="#" onclick="">
            <i class="md md-speaker-notes m-r-5"></i>订阅号
        </a>
        <br>Copyright © 2004-2018 &nbsp;XXX技术有限公司&nbsp;&nbsp;
    </footer>
</div>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>

<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/moment.js/2.22.1/moment.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<%--<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>--%>
<%--<script src="//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>--%>
<%--<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>--%>
<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<script src="/js/lcy/bootstrap-select.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<!--excel-->
<%--<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>--%>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<%--<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>--%>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="/js/resjs/printThis.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script>
    $(function () {
        $("#btn_cancer").click(function () {
            swal({
                    title: "确定取消吗？",
                    text: "取消将无法保存已修改信息！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定取消！",
                    closeOnConfirm: false
                },
                function () {
                    swal("成功！", "取消操作成功！", "success");
                    window.location.href = '/admin/to_modify_password';
                }
            );
        });
        $("#xsjh").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                userPassOld: {
                    validators: {
                        notEmpty: {
                            message: '原密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 16,
                            message: '密码必须为6~18位之间'
                        },
                        regexp: {
                            regexp: /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/,
                            message: '密码必须是数字和字母的组合'
                        }

                    }
                },
                userPassNew1: {
                    validators: {
                        notEmpty: {
                            message: '新密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 16,
                            message: '密码必须为6~18位之间'
                        },
                        regexp: {
                            regexp: /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/,
                            message: '密码必须是数字和字母的组合'
                        }

                    }
                },
                userPassNew2: {
                    validators: {
                        notEmpty: {
                            message: '确认密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 16,
                            message: '密码必须为6~18位之间'
                        },
                        regexp: {
                            regexp: /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/,
                            message: '密码必须是数字和字母的组合'
                        }

                    }
                }
            }
        });
        $("#mysubmit").on("click", function () {
            var idd = ${sessionScope.user.userId};
            $('#xsjh').append("<input class='hidden' name='userId' value='" + idd + "'>");
            //获取表单对象
            var bootstrapValidator = $("#xsjh").data('bootstrapValidator');
            //手动触发验证
            bootstrapValidator.validate();
            if (bootstrapValidator.isValid()) {
                // alert("表单序列化结果："+$("#xsjh").serialize());
                $.ajax({
                    url: '/admin/user_pwd_modify',
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    contentType: 'application/x-www-form-urlencoded',
                    data: $("#xsjh").serialize(),
                    success: function (result) {
                        if (result.code == 200) {
                            if (result.msg == "原密码不能为空") {
                                swal("失败", result.msg, "error");
                                $("#xsjh")[0].reset();
                            } else if (result.msg == "新密码不能为空") {
                                swal("失败", result.msg, "error");
                                $("#xsjh")[0].reset();
                            } else if (result.msg == "原密码错误") {
                                swal("失败", result.msg, "error");
                                $("#xsjh")[0].reset();
                            } else if (result.msg == "两次密码不一致") {
                                swal("失败", result.msg, "error");
                                $("#xsjh")[0].reset();
                            } else if (result.msg == "修改用户密码成功") {
                                swal("成功", result.msg, "success");
                                window.location.href = '/admin/to_modify_password';
                            }
                        }
                    },
                    error: function (data) {
                        swal("连接服务器错误", "", "error");
                    }
                });
            }
        });
    });

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            async: false,
            type: type,
            data: data,
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                    error()
                }
            }
        })
    }
</script>
</body>
</html>
