<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/14
  Time: 9:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="shortcut icon" href="/img/favicon.ico"/>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/hgh/sale_start_modal.css"/>
    <link rel="stylesheet" href="/css/city-picker.css"/>
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="/css/hgh/cots_view.css">
</head>
<body>
<div class="col-sm-12" style="margin-top: 20px">
    <%--<div class="form-group col-sm-2" style="padding: 0;">
        <div>
            <i class="fa fa-filter pull-left" style="margin-right: 10px;padding: 0;margin-top: 10px;"></i>
            <div class="col-sm-9" style="padding: 0;">
                <select class="selectpicker" id="dept_select">
                </select>
            </div>
        </div>
    </div>--%>
    <div class="form-group col-sm-4 pull-left">
        <%--<i class="fa fa-search col-sm-1 pull-left" style="margin-top: 10px;padding: 0;"></i>--%>
        <div class="input-group col-sm-8" style="width: 295px;">
            <input id="name_to_likefind" type="text" class="form-control">
            <div class="input-group-btn">
                <button id="finduser_like_name" type="button" class="btn waves-effect" style="color: black;">用户名称
                </button>
            </div>
        </div>
    </div>
</div>
<%--<div class="col-sm-12" style="margin-top: 5px;margin-bottom: 10px">
    <div class="col-sm-6">
        <span class="pull-left">客户基本信息</span>
        <button type="button" class="btn btn-danger btn-xs pull-left" data-toggle="tooltip" data-placement="bottom"
                title="解除搜索,显示全部数据" style="display: none" id="reset1" onclick="init()">
            <i class="fa fa-reply"></i>解除搜索
        </button>
    </div>
    <div class="col-sm-6" style="padding: 0;">
    </div>
</div>--%>

<div class="col-sm-12">
    <table id="table111">

    </table>
</div>


<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript " src="/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>
<script type="text/javascript "
        src="/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
<!-- Latest compiled and minified JavaScript -->
<script src="/js/resjs/bootstrap-table.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<%--<script src="/js/resjs/tableExport.js"></script>--%>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/zh.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/fa.min.js"></script>

<script src="/js/resjs/bootstrap-table-export.js"></script>
<script src="/js/resjs/xlsx.core.min.js"></script>
<script src="/js/resjs/tableExport.min.js"></script>
<script src="/js/resjs/FileSaver.min.js"></script>

<script src="/js/lcy/user_check.js"></script>
<script src="/js/resjs/printThis.js"></script>
<script src="/js/resjs/bjx-table.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="/js/resjs/city-picker.data.js"></script>
<script src="/js/resjs/city-picker.js"></script>
<script src="/js/resjs/main.js"></script>
<script>
    $("#transforCus").click(function () {
        $("#tc").css('display', 'block');
    });

    $("#table111").on("dbl-click-cell.bs.table", function (field, value, row, element) {
        window.opener.getUser(element.userId, element.userName);
        window.close();
    });
    $(function () {
        $.ajaxSetup({
            scriptCharset: "utf-8",
            contentType: "text/html;charset=UTF-8"
        });
        /**
         * 根据用户名模糊查询
         */
        $("#finduser_like_name").click(function () {
            var name = encodeURI($("#name_to_likefind").val());
            $("#table111").bootstrapTable('refresh', {url: "/admin/user_find_like_name?userName=" + name});
        });
        /**
         * 下拉框选择部门快捷查询
         */
        $(".selectpicker").selectpicker({
            noneSelectedText: '请选择'
        });
        //解除搜索
        $("#remove_search").click(function () {
            $("#name_to_likefind").val("");
            $('#dept_select').selectpicker('val','0');
            $("#table111").bootstrapTable('refresh', {url: "/admin/user_table_all"});
        });

        $(window).on('load', function () {
            $('.selectpicker').selectpicker('val', '');
            $('.selectpicker').selectpicker('refresh');
        });

        //下拉数据加载
        $.ajax({
            type: 'get',
            url: "/dept_list",
            dataType: 'json',
            success: function (datas) {//返回list数据并循环获取
                var select = $("#dept_select");
                select.append("<option value='0' selected>所有部门</option>");
                for (var i = 0; i < datas.length; i++) {
                    select.append("<option value='" + datas[i].deptId + "'>"
                        + datas[i].text + "</option>");
                }
                $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });
        $('#dept_select').on('changed.bs.select', function (e) {
            var value = $("#dept_select").selectpicker('val');
            // alert("凡事都你说的"+value);
            if (value == 0) {
                $("#table111").bootstrapTable('refresh', {url: "/admin/user_table_all"});
            } else {
                $("#table111").bootstrapTable('refresh', {url: "/admin/user_find_by_dept?deptId=" + value});
            }
        });

    });
</script>
</body>
</html>
