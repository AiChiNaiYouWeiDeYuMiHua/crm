<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/6
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 模态框（Modal）插件</title>

</head>

<body>
<div class="modal-header" style="border-bottom: none;">
    <button type="button" class="btn btn-default pull-right">
        <i class="fa fa-check"></i>
        保存
    </button>
    <h4 class="modal-title" id="myModalLabel">
        新增部门
    </h4>
    <hr class="boder-t-a"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="xsjh" action="/dept_add" method="post">
        <div class="mybody">
            <div class="text-center-b">
                <span style="background-color: white; padding: 2px 10px;">部门信息</span>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            <span style="color: #ff0000; font-size: 16px;">*</span>部门名称：
                        </label>
                        <div class="col-md-8">
                            <input id="deptName" name="deptName" class="form-control" required="required"/>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: 50px; "></div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
    <button class="btn btn-default" type="button" id="mysubmit"><i class="fa fa-check"></i>
        保存
    </button>
</div>


<script>
    setForm();
    $(function () {
        $("#add_dept").on('hidden.bs.modal', function () {
            open_modal = null;
            $(this).removeData('bs.modal');
        });
        $("#xsjh").bootstrapValidator({

            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                roleName: {
                    validators: {
                        notEmpty: {
                            message: '部门名不能为空'
                        }
                    }
                }
            }
        });

        $("#mysubmit").on("click", function () {
            //获取表单对象
            var bootstrapValidator = $("#xsjh").data('bootstrapValidator');
            //手动触发验证
            bootstrapValidator.validate();
            if (bootstrapValidator.isValid()) {
                // alert("表单序列化结果："+$("#xsjh").serialize());
                $.ajax({
                    url: '/dept_add',
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    contentType: 'application/x-www-form-urlencoded',
                    data: $("#xsjh").serialize(),
                    success: function (result) {
                        if (result.code == 200) {
                            swal("成功", result.msg, "success");
                            $('#add_dept').modal('hide');
                            //表单重置
                            $("#xsjh")[0].reset();
                            $("#table111").bootstrapTable('refresh');
                        }
                    },
                    error: function (data) {
                        swal("连接服务器错误", "", "error");
                    }
                });
            }
        });

        $("[data-toggle='tooltip' ] ").tooltip();

    });

    //表单初始化
    function setForm() {
        if (open_modal != null) {
            ajax("/dept_load?deptId=" + open_modal, function (data) {
                // alert(data.oppTheme)
                // alert(JSON.stringify(data))
                $('#xsjh').append("<input class='hidden' name='deptId' value='" + data.deptId + "'>")
                $('input[name="deptName"]').val(data.text)
            }, function () {
                swal("加载失败", result.msg, "error");
                $('#add_dept').modal('hide')
            }, "get", {})
        }
    }

    function ajax(url, success, error, type, data) {
        $.ajax({
            url: url,
            timeout: 3000,
            type: type,
            data: data,
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded',
            success: function (result) {
                if (result.code != 200) {
                    swal(result.msg, "", "error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete: function (XMLHttpRequest, status) {
                if (status != "success") {
                    swal("连接服务器错误", "", "error");
                    error()
                }
            }
        })
    }
</script>

</body>

</html>