<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/13
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="">
    <div class="modal-header" style="border-bottom: none;">
        <div class="pull-right  f-s-12">
            <button class="btn btn-default"
                    onclick="deleteCus(${medInfo.medId})"><i
                    class="fa fa-trash-o m-r-5"></i>删除
            </button>&nbsp;<button class="btn btn-default"
                                   onclick="$('#med_detail').modal('hide');modify(${medInfo.medId})"><i
                class="fa fa-pencil m-r-5"></i>编辑
        </button>&nbsp;
        </div>
        <h4 class="modal-title text-dark">
            <span set-lan="html:联系人纪念日">联系人纪念日</span>
            <i class="fa fa-bars m-r-5"></i>
        </h4>
        <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
    </div>
    <div class="modal-body" style="padding-top: 0px;">
        <form id="addcotsbb" action="">
            <div class="mybody show-box">
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label col-md-2" style="padding-right: 20px;">
                            客户：
                        </label>
                        <div style="font-size: 16px;margin-top: -5px;">
                            <span style="font-weight:normal;color:#9e9e9e;margin-left: 4px">〖</span>${medInfo.tbCustomerByCusId.cusName}<a
                                href="javascript:window.open('customer_details?id=${medInfo.tbCustomerByCusId.cusId}')"><i
                                class="fa fa-folder-open" style="margin-left: 5px" data-toggle="tooltip"
                                data-placement="bottom"
                                data-original-title="打开详细页面"></i></a><span
                                style="font-weight:normal;color:#9e9e9e">〗</span>
                        </div>
                    </div>
                </div>
            </div>
            <%--<div class="row">--%>
            <%--<div class="col-md-12">--%>
            <%--<iframe id="choose_cus" style="display: none; margin-bottom: 10px;min-height: 600px;"--%>
            <%--src="customer_check" width="100%" scrolling="no">--%>

            <%--</iframe>--%>
            <%--</div>--%>
            <%--</div>--%>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-6">
                    <label class="control-label col-md-4" style="padding-right: 25px">
                        联系人：
                    </label>
                    <div class="col-md-8 pull-left" style="font-size: 16px;padding:0px;margin-top: -5px;">
                        <span style="font-weight:normal;color:#9e9e9e">〖</span>${medInfo.tbContactsByCotsId.cotsName}<a
                            href=""><i
                            class="fa fa-folder-open" style="margin-left: 5px" data-toggle="tooltip"
                            data-placement="bottom"
                            data-original-title="打开详细页面"></i></a><span
                            style="font-weight:normal;color:#9e9e9e">〗</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label col-md-4" style="padding-right: 25px;">
                        纪念日类型：
                    </label>
                    <div class="col-md-8 pull-left">
                        <span>${medInfo.medType}</span>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2" style="padding-right: 25px;">
                            纪念日：
                        </label>
                        <div>
                            <span style="margin-left: 4px">${medInfo.medMemorialDate}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2" style="padding-right: 25px;">
                            备注：
                        </label>
                        <div>
                            <span style="margin-left: 4px">${medInfo.medRemark}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 5px">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2" style="padding-right: 25px;">
                            下次提醒：
                        </label>
                        <div>
                            <span style="margin-left: 4px">${medInfo.medNextPoint}</span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-default"
            onclick="deleteCus(${medInfo.medId})"><i
            class="fa fa-trash-o m-r-5"></i>删除
    </button>&nbsp;<button class="btn btn-default"
                           onclick="$('#med_detail').modal('hide');modify(${medInfo.medId})"><i
        class="fa fa-pencil m-r-5"></i>编辑
</button>&nbsp;
</div>
<!-- /.modal-content -->
</div>
<script src="/js/resjs/cus-check.js"></script>
<script>
    function deleteCus(medId) {
        swal({
                title: "确定删除吗？",
                text: "你将无法恢复该纪念日！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: 'memorial/delete_memorial',
                    method: 'post',
                    async: true,
                    data: {'id': medId},
                    success: function (data) {
                        if (data.status == 400) {
                            swal("成功！", "纪念日删除失败！", "cancle");
                        } else {
                            swal("成功！", "纪念日删除成功！", "success");
                            $("#cots-table").bootstrapTable('refresh');
                        }
                    },
                    error: function (data) {
                        alert("操作失败");
                        swal("失败！", "纪念日删除失败！" + data.msg, "Cancel")
                    }
                });
            });
    }

</script>
</body>
</html>
