<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/11
  Time: 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>项目视图</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="css/city-picker.css"/>
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link href="/css/hgh/cus_view.css" rel="stylesheet">
    <link href="/css/hgh/sale_start_modal.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid">
    <div class="profile-info-name" style="height: 20px;">
        <div class="pull-right">
            <a id="other1" onclick="" style="text-decoration: none;" href="#">ID:<b
                    class="ng-binding">${projectInfo.projId}</b></a>
            <span id="div2">
					<a class="text-a" style="text-decoration: none; margin-left: 10px;" href="#" target="_BLANK"
                       data-toggle="tooltip" data-placement="bottom" title="" data-original-title="数据日志"><i
                            class="fa fa-bars m-l-5"></i></a>
					<a class="text-a" style="text-decoration: none;margin-left: 10px;cursor: pointer;"
                       onclick="detail_delete(${projectInfo.projId})"
                    ><i class="fa fa-trash m-r-5"></i>删除</a>
				</span>
            <a class="text-a" style="text-decoration: none;margin-left: 10px;cursor: pointer;"
               onclick="modify(${projectInfo.projId})"><i
                    class="fa fa-pencil m-r-5"></i>编辑</a>
        </div>
    </div>
    <h3 class="p-t-40  text-center" style="padding-top: 40px;">${projectInfo.projTheme}&nbsp;<sup
            class="text-gray btn-danger btn-xs" style="font-size: 5px;margin-top:-10px ;">${projectInfo.projStage}</sup>
        <sup class="text-gray btn-primary btn-xs" style="font-size: 5px;margin-top:-10px ;">${projectInfo.projState}</sup>
    </h3>

    <p class="text-muted  text-center ng-binding">${projectInfo.projEntryDate} 立项：${projectInfo.projEntryDate} ${projectInfo.tbUserByUserId.userName}  主客户：<a style="text-decoration: none;" href="customer_details?id=${projectInfo.tbCustomerByCusId.cusId}">〖${projectInfo.tbCustomerByCusId.cusName}&nbsp;<i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i>〗</a>

    </p>

    <div class="row">
        <div class="col-sm-8 pull-right" style="margin-top: 20px;">
            <div class="card-box2 " style="background-color: white;font-size: 12px;">
                <div class="card-box-head">
                    <i class="fa fa-rub" style="margin-right: 5px;"></i><span set-lan="html:跟单时间线">跟单时间线</span>
                </div>
                <div>
                    <div class="widget-inline">
                        <!-- ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope active" repeat-finish="" ng-repeat="da in d.tmc">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-tasks"></i> <b class="ng-binding">17</b></h5>
                                <p style="color: grey;">全部</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="" ng-repeat="da in d.tmc">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-odnoklassniki"></i> <b class="ng-binding">1</b></h5>
                                <p style="color: grey;">任务行动</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="" ng-repeat="da in d.tmc">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-file-text"></i> <b class="ng-binding">2</b></h5>
                                <p style="color: grey;">订单</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="" ng-repeat="da in d.tmc">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-cubes"></i> <b class="ng-binding">2</b></h5>
                                <p style="color: grey;">交付计划</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="" ng-repeat="da in d.tmc">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-clipboard"></i> <b class="ng-binding">2</b></h5>
                                <p style="color: grey;">交付记录</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-truck"></i> <b class="ng-binding">2</b></h5>
                                <p style="color: grey;">发货单</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-yen"></i> <b class="ng-binding">2</b></h5>
                                <p style="color: grey;">回款</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-money"></i> <b class="ng-binding">2</b></h5>
                                <p style="color: grey;">计划回款</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-wpforms"></i> <b class="ng-binding">2</b></h5>
                                <p style="color: grey;">开票</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a href="" style="text-decoration: none;">
                                <h5><i class="fa fa-gg"></i> <b class="ng-binding">2</b></h5>
                                <p style="color: grey;">项目协作</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->

                    </div>
                </div>
                <div>
                    <div style="padding: 20px;">
                        <div class="timeline-2" style="margin-left: 10px;">
                            <!-- ngRepeat: da in d.tm -->
                            <div class="time-item ng-scope" style="margin-top: 30px;">
                                <div class="item-info">
                                    <p><i class="fa fa-calendar-times-o" style="margin-right: 5px;"></i>2018-7-22</p>
                                    <!-- ngRepeat: fl in da.ds track by $index -->
                                    <div class="display-table " repeat-finish="">
                                        <div class="col-sm-3">
                                            <p>
                                                <i class="fa fa-star"></i>
                                                <span>三一客</span>
                                            </p>
                                        </div>
                                        <div>
                                            <p>
                                                <b>
                                                    <a href="" style="text-decoration: none;"><span>定性：有价值；定级：正常；定量：下周 陈默</span></a>
                                                    <!-- ngIf: fl.type == '电子面单' -->
                                                    <span></span>
                                                </b>
                                            </p>
                                            <!-- ngRepeat: sfl in fl._dd -->
                                        </div>
                                    </div>
                                    <div class="display-table " repeat-finish="">
                                        <div class="col-sm-3">
                                            <p>
                                                <i class="fa fa-star"></i>
                                                <span>三一客</span>
                                            </p>
                                        </div>
                                        <div>
                                            <p>
                                                <b>
                                                    <a href="" style="text-decoration: none;"><span>定性：有价值；定级：正常；定量：下周 陈默</span></a>
                                                    <!-- ngIf: fl.type == '电子面单' -->
                                                    <span></span>
                                                </b>
                                            </p>
                                            <!-- ngRepeat: sfl in fl._dd -->
                                        </div>
                                    </div>
                                    <!-- end ngRepeat: fl in da.ds track by $index -->
                                </div>
                            </div>
                            <!-- end ngRepeat: da in d.tm -->
                            <div class="time-item ng-scope" style="margin-top: 30px;">
                                <div class="item-info">
                                    <p><i class="fa fa-calendar-times-o" style="margin-right: 5px;"></i>2018-8-1</p>
                                    <!-- ngRepeat: fl in da.ds track by $index -->
                                    <div class="display-table " repeat-finish="">
                                        <div class="col-sm-3">
                                            <p>
                                                <i class="fa fa-gg"></i>
                                                <span>项目协作</span>
                                            </p>
                                        </div>
                                        <div>
                                            <p>
                                                <b>
                                                    <a href="" style="text-decoration: none;"><span>主题公园安防  有效 陈默 2017-09-20</span></a>
                                                </b>
                                            </p>
                                            <!-- ngRepeat: sfl in fl._dd -->
                                        </div>
                                    </div>
                                    <!-- end ngRepeat: fl in da.ds track by $index -->
                                </div>
                            </div>
                            <!-- end ngRepeat: da in d.tm -->
                            <div class="time-item ng-scope" style="margin-top: 30px;">
                                <div class="item-info">
                                    <p><i class="fa fa-calendar-times-o" style="margin-right: 5px;"></i>2018-7-3</p>
                                    <!-- ngRepeat: fl in da.ds track by $index -->
                                    <div class="display-table " repeat-finish="">
                                        <div class="col-sm-3">
                                            <p>
                                                <i class="fa fa-yen"></i>
                                                <span>回款</span>
                                            </p>
                                        </div>
                                        <div>
                                            <p>
                                                <b>
                                                    <a href="" style="text-decoration: none;"><span
                                                            ng-bind-html="fl.nr|html" class="ng-binding"><span
                                                            style="white-space:nowrap"><span
                                                            style="font-size:9pt;font-weight:normal;">￥</span>999.00</span> 2017-03-16 kindle 陈默</span></a>

                                                </b>
                                            </p>
                                            <!-- ngRepeat: sfl in fl._dd -->
                                        </div>
                                    </div>
                                    <!-- end ngRepeat: fl in da.ds track by $index -->
                                </div>
                            </div>

                        </div>
                        <a class=" btn-round2 waves-effect" data-toggle="tooltip" data-placement="top" title=""
                           data-original-title="无更早数据"></a>
                    </div>
                </div>
            </div>
        </div>


        <!--左边区块-->
        <div class="row col-sm-4" style="margin-top: 20px;padding: 0;">
            <div class="card-box2  col-sm-12" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <div class="pull-right">
                            停滞223天
                        </div>
                        <p style="margin-bottom: 10px;"><i class="fa fa-star" style="margin-right: 5px;"></i>三定一节点</p>
                    </div>
                    <div class="" id="31k_body">
                        <div class="table-box font-weg text-center">
                            <div class="table-detail a text-black  col-sm-4">
                                <div class="iconbox  bg-gray m-0" style="margin: 0;padding-top: 13px;">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <p class=" m-b-0 m-t-5"
                                   style="margin-bottom: 0;margin-top: 5px;">${cusInfo.cusDetermine}</p>
                            </div>
                            <div class="table-detail a text-black  col-sm-4">
                                <div class="iconbox  bg-gray" style="margin: 0;">
                                    <i class="fa fa-heart" style="margin: 0px;padding-top: 18px;"></i>
                                </div>
                                <p class="text-center" style="margin-bottom: 0;margin-top: 5px;">${cusInfo.cusClass}</p>
                            </div>
                            <div class="table-detail a text-black  col-sm-4">
                                <div class="iconbox bg-gray" style="margin: 0;">
                                    <i class="fa fa-hashtag" style="margin: 0px;padding-top: 18px;"></i>
                                </div>
                                <p class="text-center" style="margin-bottom: 0;margin-top: 5px;">
                                    3万 ${cusInfo.cusSignDate}</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <p class="" style="margin-bottom: 10px;"><i class="fa fa-xing m-r-5"></i>销售机会：售楼处监控+防火</p>
                    </div>
                    <div class="table-box text-center ">
                        <div class="col-sm-4">
                            <div class="iconbox bg-gray" style="margin: 0;">
                                <h5 class="" style="margin-top: 15px;">20%</h5>
                            </div>
                            <p style="margin-top: 5px;">可能性</p>
                        </div>
                        <div class="col-sm-4" style="margin-top: 10px;">
                            <h5>2-28</h5>
                            <p class="font-weg">预估日期</p>

                        </div>
                        <div class="col-sm-4" style="margin-top: 10px;">
                            <h5>3万</h5>
                            <p class="font-weg">预估金额</p>

                        </div>
                    </div>
                    <div class="pull-right" style="position: absolute;top: 125px;left: 190px;">
                        <p>阶段：${cusInfo.cusStage},停留251天</p>
                    </div>
                </div>
            </div>
            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <p class=" m-b-10" style="margin-bottom: 10px;"><i class="fa fa-hand-lizard-o m-r-5"></i>应收/预付
                        </p>
                    </div>
                    <div class="bar-widget">
                        <div class="table-box text-center">
                            <div class="text-center col-sm-4">
                                <h5>￥&nbsp;0.00</h5>
                                <p style="margin-left: 5px;">计划回款</p>
                            </div>

                            <div class="text-center col-sm-4">
                                <h5 class="ng-binding">￥&nbsp;6,999.00</h5>
                                <p style="margin-left: 5px;">智能应收</p>
                            </div>
                            <div class="text-center col-sm-4">
                                <h5>￥&nbsp;0.00</h5>
                                <p style="margin-left: 5px;">预付余额</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <p class=" m-b-10" style="margin-bottom: 10px;"><i class="fa fa-object-group m-r-5"
                                                                           style="margin-right: 5px;"></i>签约数据</p>
                    </div>
                    <div class="table-box text-center font-weg">
                        <div class="table-detail col-sm-3">
                            <h5><a href="" data-toggle="tooltip" data-placement="bottom" title=""
                                   data-original-title="9999.00" class="ng-binding">9千9</a></h5> 合约
                        </div>
                        <div class="table-detail col-sm-3">
                            <h5><a href="" data-toggle="tooltip" data-placement="bottom" title=""
                                   data-original-title="9999.00" class="ng-binding">9千9</a></h5> 发货
                        </div>
                        <div class="table-detail col-sm-3">
                            <h5><a href="" data-toggle="tooltip" data-placement="bottom" title=""
                                   data-original-title="9999.00">9千9</a></h5> 回款
                        </div>
                        <div class="table-detail col-sm-3">
                            <h5><a href="" target="_blank"><i
                                    class="fa fa-list-ol fa-x"></i></a></h5> 往来对账
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <p><i class="fa fa-info" style="margin-right: 5px;"></i><span>基本信息</span></p>
                    </div>
                    <div class="form-inline font-weg">
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>助记简称</span>
                            <label>${cusInfo.cusAbbreviation}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>种类</span>
                            <label>${cusInfo.cusType}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>编号</span>
                            <label>${cusInfo.cusId}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>行业</span>
                            <label>${cusInfo.cusIndustry}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>人员规模</span>
                            <label>${cusInfo.cusExistDate}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>来源</span>
                            <label>${cusInfo.cusCome}</label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group m-r-10 m-b-5" style="margin-top: 10px;">
                            <span>公司简介</span>
                            <span>${cusInfo.cusSynopsis}</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>首次签约</span>
                            <label>${cusInfo.cusDate}</label>
                        </div>
                        <div class="form-group m-r-10 " style="margin-top: 10px;">
                            <span>关系等级</span>
                            <label>${cusInfo.cusLevel}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>阶段</span>
                            <label>${cusInfo.cusStage}</label>
                        </div>
                        <div class="form-group m-r-10 " style="margin-top: 10px;">
                            <span>信用等级</span>
                            <label>${cusInfo.cusCredit}</label>
                        </div>
                        <br>
                        <div class="form-group m-r-10 " style="margin-top: 10px;">
                            <span>备注</span>
                            <label>${cusInfo.cusRemark}</label>
                        </div>
                    </div>
                    <div class="card-box-head">
                        <br>
                        <img src="http://cdn.xtcrm.com/cdn//v7/img/tyc2.png" width="12" class="m-r-5"
                             style="padding-bottom: 3px;">工商信息(由天眼查提供)
                        <span><br></span>
                    </div>
                </div>
            </div>
            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <div class="contact-action display-all pull-right">
                            <div class="radio radio-inline">
                                <input type="radio" id="inlineRadio1" value="option1" name="radioInline" checked=""
                                       onclick="$('#contact_show1').show(200);$('#contact_show2').hide();" ;="">
                                <label for="inlineRadio1"><i class="fa fa-users"></i></label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" id="inlineRadio2" value="option1" name="radioInline"
                                       onclick="$('#contact_show2').show(200);$('#contact_show1').hide();" ;="">
                                <label for="inlineRadio2"><i class="fa fa-sitemap"></i></label>
                            </div>
                        </div>
                        <p><i class="md md-quick-contacts-dialer m-r-5"></i><span set-lan="html:联系人">联系人</span></p>
                    </div>
                    <div class="contact-card" id="contact_show1" style="display: block;">
                        <a class="pull-left text-center" style="text-decoration: none"
                           href="">
                            <i class="fa fa-male fa-3x"></i><br> 白文治
                        </a>
                        <div class="member-info font-weg" style="padding-left: 100px;padding-bottom: 20px;">
                            <p>手机：<span><a title="发手机短信" style="text-decoration: none"
                                           href=''><b>13051640031</b><i
                                    class="glyphicon glyphicon-phonemobile m-l-5"></i></a></span></p>
                            <p>电话：<span>01062986755</span></p>
                            <p>邮箱：<span></span></p>
                            <p> Q Q：<span></span></p>
                            <p>微信：<span></span></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="font-weg">
                            <p></p>
                        </div>
                        <ul class="list-inline m-0 " style="padding-left: 0;margin-left: -5px;list-style: none;">
                            <li class=" text-center">
                                <a data-placement="top" data-toggle="tooltip" class="tooltips" href=""
                                   data-original-title="1231231" style="text-decoration: none">
                                    <i class="fa fa-male fa-2x"></i><br>vvvv<font color="gray">.qweqwr</font></a>
                            </li>
                        </ul>
                    </div>
                    <div class="contact-card" id="contact_show2" style="display: none;">
                        <ul id="treeDemo" class="tree" style="list-style-type: none;">
                            <li treenode=""><a href="" style="text-decoration: none" onclick="reportMove(this) ;"
                                               treenode_a=""><i
                                    class="fa fa-user" style="margin-right: 5px"></i><span>白文治</span></a>
                                <ul style="list-style-type: none;margin-top: 0">
                                    <li treenode=""><a href="" style="text-decoration: none"
                                                       onclick="reportMove(this) ;" treenode_a=""><i
                                            class="fa fa-user" style="margin-right: 5px"></i><span>vvvv<font
                                            color="gray">.qweqwr</font></span></a>
                                        <ul style="display: none;" class="line"></ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-box2" style="padding-top: 10px;padding-bottom: 10px">
                <b class="pull-right">￥&nbsp;0.00</b>
                <p style="margin: 0"><i class="fa fa-dollar" style="margin-right: 5px"></i><span>销售费用</span></p>
            </div>

            <div class="card-box2 " style="padding-top: 10px;font-size: 12px">
                <div class="card-box-head">
                    <p><i class="fa fa-map-marker m-r-5"></i><span set-lan="html:地址">地址</span></p>
                </div>
                <div class="form-inline font-weg">
                    <div class="form-group m-r-10" ng-class="{true: 'display-none ', false: ''}[c._v.country=='']">
                        <span set-lan="html:国家或地区">国家或地区</span>
                        <label class="ng-binding">CN China中国 亚洲</label>
                    </div>
                    <div class="form-group m-r-10" ng-class="{true: 'display-none ', false: ''}[c._v.tel=='']">
                        <span>电话</span>
                        <span>${cusInfo.cusPhone}</span>
                    </div>
                    <div class="form-group m-r-10 display-none"
                         ng-class="{true: 'display-none ', false: ''}[c._v.fax=='']">
                        <span>传真</span>
                        <label>${cusInfo.cusMSNQQ}</label>
                    </div>
                    <div class="form-group m-r-10 display-none"
                         ng-class="{true: 'display-none ', false: ''}[c._v.state=='']">
                        <span set-lan="html:省份">省/市/区</span>
                        <label id="prov">${cusInfo.cusAddr}</label>
                    </div>
                    <div class="form-group m-r-10" ng-class="{true: 'display-none ', false: ''}[c._d.address=='']">
                        <span>地址</span>
                        <label><span>${cusInfo.cusArea}</span></label>
                    </div>
                    <br>
                    <div class="form-group m-r-10" ng-class="{true: 'display-none ', false: ''}[c._d.web=='http://']">
                        <span set-lan="html:网址">网址</span>
                        <label>
                            <span ng-bind-html="c._v.web|html" class="ng-binding">
                                <a href="http://${cusInfo.cusNet}" target="_blank">
                                    <b>http://${cusInfo.cusNet}</b>
                                </a>
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="modal fade" id="addproj" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
         aria-labelledby="open"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content mymodalcontent">

            </div>
        </div>
    </div>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript " src="bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>
    <script type="text/javascript "
            src="bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="../../js/resjs/bootstrap-table.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
    <script src="js/resjs/tableExport.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
    <script src="../../js/resjs/bootstrap-tooltip.js"></script>
    <script src="../../js/resjs/bootstrap-popover.js"></script>
    <!--excel-->
    <script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>
    <script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
    <script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
    <script src="../../js/resjs/printThis.js"></script>
    <script src="../../js/resjs/table.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script>
        $(function () {
            $("#example").popover();
            $("[data-toggle='tooltip']").tooltip();
        })

        function detail_delete(cusId) {
            swal({
                    title: "确定删除吗？",
                    text: "你将无法恢复该项目！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定删除！",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        url: 'project/delete_project',
                        method: 'post',
                        async: true,
                        data: {'id': cusId},
                        success: function (data) {
                            alert('我成功了？' + data.msg);
                            swal("成功！", "项目删除成功！", "success");
                            window.location.href = "project_view";
                        },
                        error: function (data) {
                            alert("操作失败");
                            swal("失败！", "客户添加失败！" + data.msg, "Cancel")
                        }
                    });
                });
        }


        var open_modal;

        function modify(id) {
            $('#addproj').removeData("bs.modal");
            open_modal = id;
            $('#addproj').modal({
                remote: 'addproject_modal'
            });
        }

        $("#addproj").on("hidden.bs.modal", function () {
            window.location.reload();
        });

    </script>
</div>
</body>
</html>
