<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/20
  Time: 0:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>

</head>
<body>
<div class="modal-header" style="border-bottom: none;">
    <div class="pull-right  f-s-12">
        <button class="btn btn-default"
                onclick="deleteCus(${serveInfo.serveId})"><i
                class="fa fa-trash-o m-r-5"></i>删除
        </button>&nbsp;<button class="btn btn-default"
                               onclick="$('#serve_detail').modal('hide');modify(${serveInfo.serveId})"><i
            class="fa fa-pencil m-r-5"></i>编辑
    </button>&nbsp;
    </div>
    <h4 class="modal-title text-dark">
        <span>客户服务</span>
        <i class="fa fa-bars m-r-5"></i>
    </h4>
    <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="addcotsbb" action="">
        <div class="mybody">
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        主题：
                    </label>
                    <div style="font-size: 16px;margin-top: -5px;">
                                <span style="font-weight:normal;color:#9e9e9e;margin-left: 4px">
                                    ${serveInfo.serveTheme}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        对应客户：
                    </label>
                    <div style="font-size: 16px;margin-top: -5px;">
                        <span style="font-weight:normal;color:#9e9e9e;margin-left: 4px">〖</span>${serveInfo.tbCustomerByCusId.cusName}<a
                            href="javascript:window.open('customer_details?id=${serveInfo.tbCustomerByCusId.cusId}')"><i
                            class="fa fa-folder-open" style="margin-left: 5px" data-toggle="tooltip"
                            data-placement="bottom"
                            data-original-title="打开详细页面"></i></a><span
                            style="font-weight:normal;color:#9e9e9e">〗</span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        服务类型：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${serveInfo.serveType}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        服务方式：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${serveInfo.serveWay}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        开始日期：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${serveInfo.serveStartTime}
                                </span>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top: 15px">
                <div class="col-md-6">
                    <label class="control-label col-md-4" style="padding-right: 25px">
                        花费时间：
                    </label>
                    <div class="col-md-8 pull-left">
                        <span>${serveInfo.serveTimeSpent}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label col-md-4" style="padding-right: 25px;">
                        客户联系人：
                    </label>
                    <div class="col-md-8 pull-left">
                        <div style="font-size: 16px;margin-top: -5px;">
                            <span style="">〖</span>${serveInfo.serveContact}<span
                                style="font-weight:normal;color:#9e9e9e">〗</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        状态：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${serveInfo.serveState}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        执行人：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${serveInfo.tbUserByUserId.userName}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        服务内容：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${serveInfo.serveContent}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        客户反馈：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${serveInfo.serveBack}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        备注：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${serveInfo.serveRemarks}
                                </span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-default"
            onclick="deleteCus(${veInfo.serveId})"><i
            class="fa fa-trash-o m-r-5"></i>删除
    </button>&nbsp;<button class="btn btn-default"
                           onclick="$('#serve_detail').modal('hide');modify(${serveInfo.serveId})"><i
        class="fa fa-pencil m-r-5"></i>编辑
</button>&nbsp;
</div>
<!-- /.modal-content -->
<script>
    var viewer = new Viewer(document.getElementById('cotsPhoto'));
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    })
</script>
</body>
</html>
