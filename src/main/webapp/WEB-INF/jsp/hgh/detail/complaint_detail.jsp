<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/20
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="modal-header" style="border-bottom: none;">
    <div class="pull-right  f-s-12">
        <button class="btn btn-default"
                onclick="deleteCus(${complaintInfo.complainId})"><i
                class="fa fa-trash-o m-r-5"></i>删除
        </button>&nbsp;<button class="btn btn-default"
                               onclick="$('#complaint_detail').modal('hide');modify(${complaintInfo.complainId})"><i
            class="fa fa-pencil m-r-5"></i>编辑
    </button>&nbsp;
    </div>
    <h4 class="modal-title text-dark">
        <span>投诉管理</span>
        <i class="fa fa-bars m-r-5"></i>
    </h4>
    <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="addcotsbb" action="">
        <div class="mybody">
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        对应客户：
                    </label>
                    <div style="font-size: 16px;margin-top: -5px;">
                        <span style="font-weight:normal;color:#9e9e9e;margin-left: 4px">〖</span>${complaintInfo.tbCustomerByCusId.cusName}<a
                            href="javascript:window.open('customer_details?id=${complaintInfo.tbCustomerByCusId.cusId}')"><i
                            class="fa fa-folder-open" style="margin-left: 5px" data-toggle="tooltip"
                            data-placement="bottom"
                            data-original-title="打开详细页面"></i></a><span
                            style="font-weight:normal;color:#9e9e9e">〗</span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        投诉主题：
                    </label>
                    <div style="font-size: 16px;margin-top: -5px;">
                                <span style="font-weight:normal;color:#9e9e9e;margin-left: 4px">
                                    ${complaintInfo.complainTheme}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-6">
                    <label class="control-label col-md-4" style="padding-right: 25px">
                        首问接待人：
                    </label>
                    <div class="col-md-8 pull-left">
                        <span>${complaintInfo.tbUserByUserId.userName}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <label class="control-label col-md-4" style="padding-right: 25px;">
                        分类：
                    </label>
                    <div class="col-md-8 pull-left">
                        <span>${complaintInfo.complainType}</span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        描述：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${complaintInfo.complainBewrite}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        日期：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${complaintInfo.complainDate}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        投诉人：
                    </label>
                    <div style="font-size: 16px;margin-top: -5px;">
                        <span style="font-weight:normal;color:#9e9e9e;margin-left: 4px">〖</span>${complaintInfo.tbContactsByCotsId.cotsName}<a
                            href="contact_detail?id=${complaintInfo.tbContactsByCotsId.cotsId}"><i
                            class="fa fa-folder-open" style="margin-left: 5px" data-toggle="tooltip"
                            data-placement="bottom"
                            data-original-title="打开详细页面"></i></a><span
                            style="font-weight:normal;color:#9e9e9e">〗</span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        紧急程度：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${complaintInfo.complainEmergency}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        处理过程：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${complaintInfo.complainProcess}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        处理结果：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${complaintInfo.complainResults}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        花费时间：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${complaintInfo.complainTimeSpent}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        客户反馈：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${complaintInfo.complainBack}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        回访确认：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${complaintInfo.compainBackComfimed}
                                </span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-12">
                    <label class="control-label col-md-2" style="padding-right: 30px;">
                        备注：
                    </label>
                    <div>
                                <span style="margin-left: 4px">
                                    ${complaintInfo.compainRemark}
                                </span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-default"
            onclick="deleteCus(${complaintInfo.complainId})"><i
            class="fa fa-trash-o m-r-5"></i>删除
    </button>&nbsp;<button class="btn btn-default"
                           onclick="$('#complaint_detail').modal('hide');modify(${complaintInfo.complainId})"><i
        class="fa fa-pencil m-r-5"></i>编辑
</button>&nbsp;
</div>
<!-- /.modal-content -->
<script>
    var viewer = new Viewer(document.getElementById('cotsPhoto'));
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    })
</script>
</body>
</html>
