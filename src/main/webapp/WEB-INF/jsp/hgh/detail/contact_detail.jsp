<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/20
  Time: 0:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<html>
<head>
    <title>联系人：${cotsInfo.cotsName}</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="css/hgh/sale_start_modal.css"/>
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="/css/hgh/cots_view.css">
    <link rel="stylesheet" href="/css/hgh/viewer.min.css">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <link href="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/css/fileinput.css" rel="stylesheet">
</head>
<body style="padding-left:60px">

<div class="col-sm-3"></div>
<div class="col-sm-8">
    <div class="mymodalcontent" style="background-color: white">
        <div class="modal-header" style="border-bottom: none;">
            <div class="pull-right  f-s-12">
                <button class="btn btn-default" disabled
                        id="delete"><i
                        class="fa fa-trash-o m-r-5"></i>删除
                </button>&nbsp;<button class="btn btn-default"
                                     disabled  id="mod" ><i
                    class="fa fa-pencil m-r-5"></i>编辑
            </button>&nbsp;
                <g:g id="90">
                    <script>$('#delete').css("display","none")</script>
                <button class="btn btn-default"
                        onclick="detail_delete(${cotsInfo.cotsId})"><i
                        class="fa fa-trash-o m-r-5"></i>删除
                </button>&nbsp;</g:g>
                <g:g id="86">
                    <script>$('#mod').css("display","none")</script>
                <button class="btn btn-default"
                                       onclick="modify(${cotsInfo.cotsId})"><i
                    class="fa fa-pencil m-r-5"></i>编辑
            </button>&nbsp;</g:g>
            </div>
            <h4 class="modal-title text-dark">
                <span>联系人</span>
                <i class="fa fa-bars m-r-5"></i>
            </h4>
            <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
        </div>
        <div class="modal-body" style="padding-top: 0px;">
            <form id="addcotsbb" action="">
                <div class="mybody">
                    <div class="text-center-b">
                        <span style="background-color: white; padding: 2px 10px;">联系人</span>
                        <input name="cotsId" style="display: none">
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                姓名：
                            </label>
                            <div class="col-md-8 pull-left" style="font-size: 16px;margin-top: -5px;">
                                <span>${cotsInfo.cotsName}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                移动电话：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsPersonalPhone}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-12">
                            <label class="control-label col-md-2" style="padding-right: 30px;">
                                对应客户：
                            </label>
                            <div style="font-size: 16px;margin-top: -5px;">
                                <span style="font-weight:normal;color:#9e9e9e;margin-left: 4px">〖</span>${cotsInfo.tbCustomerByCusId.cusName}<a
                                    href="javascript:window.open('customer_details?id=${cotsInfo.tbCustomerByCusId.cusId}')"><i
                                    class="fa fa-folder-open" style="margin-left: 5px" data-toggle="tooltip"
                                    data-placement="bottom"
                                    data-original-title="打开详细页面"></i></a><span
                                    style="font-weight:normal;color:#9e9e9e">〗</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                联系人分类：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsClassification}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                负责业务：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsBusiness}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                性别：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsSex}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                微信：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsMsnQq}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-12">
                            <label class="control-label col-md-2" style="padding-right: 30px;">
                                项目：
                            </label>
                            <div>
                                <span></span>
                            </div>
                        </div>
                    </div>


                    <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">单位</span>
                    </div>

                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                称谓：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsTechnical}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                类型：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsType}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                部门：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsDepartment}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                职务：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsTechnical}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-12">
                            <label class="control-label col-md-2" style="padding-right: 30px;">
                                主联系人：
                            </label>
                            <div>
                                <span>${cotsInfo.cotsUpcots}</span>
                            </div>
                        </div>
                    </div>

                    <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">联系</span>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                工作电话：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsWorkphone}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                邮件地址：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsTechnical}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                MSN(QQ)：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsMsnQq}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                传真：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsFax}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                工作电话：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsWorkphone}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                邮件地址：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsTechnical}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-12">
                            <label class="control-label col-md-2" style="padding-right: 30px;">
                                家庭住址：
                            </label>
                            <div>
                                <span>${cotsInfo.cotsHome}</span>
                            </div>
                        </div>
                    </div>

                    <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">其他</span></div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-12">
                            <label class="control-label col-md-2" style="padding-right: 30px;">
                                爱好：
                            </label>
                            <div>
                                <span>${cotsInfo.cotsInterest}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                证件类型：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsCardtype}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                证件号码：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsIdnum}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-12">
                            <label class="control-label col-md-2" style="padding-right: 30px;">
                                备注：
                            </label>
                            <div>
                                <span>${cotsInfo.cotsRemark}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-12">
                            <label class="control-label col-md-2" style="padding-right: 30px;">
                                照片：
                            </label>
                            <div>
                                <img src="/img/${cotsInfo.cotsPhoto}" id="cotsPhoto" alt="联系人照片"
                                     style="width: 100px;height: 80px;">
                            </div>
                        </div>
                    </div>


                    <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">联系人画像</span>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                习惯：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsHabits}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                性格：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsCharacter}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                社交特点：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsSocialCharacteristics}</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px;">
                                年收入：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsAnnualIncome}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-6">
                            <label class="control-label col-md-4" style="padding-right: 25px">
                                消费习惯：
                            </label>
                            <div class="col-md-8 pull-left">
                                <span>${cotsInfo.cotsConsumptionHabits}</span>
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-2" style="padding-right: 20px;">
                                    <a class="btn btn-success" onclick="InitExcelFile()">上传附件</a>
                                </label>
                                <div class="col-md-10" id="upload" style="display: none">
                                    <form id="ffImport" method="post">
                                        <div style="padding: 5px">
                                            <input id="excelFile" type="file">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <g:g id="85">
            <button class="btn btn-default" type="button" id="mysubmit1"><i class="fa fa-check"></i>
                保存
            </button>
            </g:g>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<div class="modal fade" id="addcots" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-labelledby="open"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">

        </div>
    </div>
</div>
<div class="col-sm-1"></div>
<script src="/js/resjs/cus-check.js"></script>
<script>

</script>


<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript " src="bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>
<script type="text/javascript " src="bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
<!-- Latest compiled and minified JavaScript -->
<script src="/js/resjs/bootstrap-table.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/zh.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/fa.min.js"></script>
<script src="js/resjs/viewer.min.js"></script>
<script src="js/resjs/printThis.js"></script>
<script src="js/resjs/bjx-table.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="js/resjs/contact-table.js"></script>
<script src="js/resjs/upload-file.js"></script>
<script src="js/resjs/Scroll.js"></script>
<script src="js/resjs/Tween.js"></script>
<script>
    var viewer = new Viewer(document.getElementById('cotsPhoto'));
    $(function () {
        $("#example").popover();
        $("[data-toggle='tooltip']").tooltip();
    })

    function detail_delete(cusId) {
        swal({
                title: "确定删除吗？",
                text: "你将无法恢复该联系人！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: 'contact/delete_contact',
                    method: 'post',
                    async: true,
                    data: {'id': cusId},
                    success: function (data) {
                        alert('我成功了？' + data.msg);
                        swal("成功！", "联系人删除成功！", "success");
                        window.location.href = "contact_view";
                    },
                    error: function (data) {
                        alert("操作失败");
                        swal("失败！", "联系人添加失败！" + data.msg, "Cancel")
                    }
                });
            });
    }


    var open_modal;

    function modify(id) {
        $('#addcots').removeData("bs.modal");
        open_modal = id;
        $('#addcots').modal({
            remote: 'addcots_modal'
        });
    }

    $("#addcots").on("hidden.bs.modal", function () {
        window.location.reload();
    });

</script>
</body>
</html>
