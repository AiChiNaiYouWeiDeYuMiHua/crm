<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/13
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="">
    <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
            <span>&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">
            项目基本信息
        </h4>
        <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
    </div>
    <div class="modal-body" style="padding-top: 0px;">
        <form id="addcotsbb" action="">
            <div class="mybody">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;margin-top: 5px">
                                项目主题：
                            </label>
                            <div class="col-md-10" style="padding: 0;padding-left: 10px">
                                <input name="projTheme" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input name="projId" style="display: none">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                <span style="color: #ff0000; font-size: 16px;">*</span>主客户：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input type="text" name="tbCustomerByCusId.cusId" class="form-control"
                                       style="display: none">
                                <input type="text" disabled class="form-control" id="to_cus">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick="chooseCus()"><i
                                            class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px">状态：</label>
                            <div class="col-md-10">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="有效" name="projState" checked>
                                    <label> 有效 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="失败" name="projState">
                                    <label> 失败 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="搁置" name="projState">
                                    <label> 搁置 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="放弃" name="projState">
                                    <label> 放弃 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px">售前：</label>
                            <div class="col-md-10">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="初期沟通" name="projPresale" checked>
                                    <label> 初期沟通 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="立项评估" name="projPresale">
                                    <label> 立项评估 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="需求分析" name="projPresale">
                                    <label> 需求分析 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="方案制定" name="projPresale">
                                    <label> 方案制定 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="商务谈判" name="projPresale">
                                    <label> 商务谈判 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="合同签约" name="projPresale">
                                    <label> 合同签约 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px">阶段：</label>
                            <div class="col-md-10">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="售前跟踪" name="projStage" checked>
                                    <label> 售前跟踪 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="签约实施" name="projStage">
                                    <label> 签约实施 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="结项验收" name="projStage">
                                    <label> 结项验收 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4" style="padding-left:0">
                                <span style="color: #ff0000; font-size: 16px;">*</span>
                                负责人：
                            </label>
                            <div class="input-group col-md-8" style="padding-left: 15px;">
                                <input type="text" name="tbUserByUserId.userId" style="display: none">
                                <input type="text" disabled class="form-control" id="to_user1">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick="chooseUser()"><i
                                            class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>
                                立项日期：
                            </label>
                            <div class="col-md-8">
                                <div class="input-append date form_datetime input-group">
                                    <input name="projEntryDate" class="form-control" type="date"/>
                                    <span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px">类型：</label>
                            <div class="col-md-10">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="标准项目" name="projType" checked>
                                    <label> 标准项目 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="文本项目" name="projType">
                                    <label> 文本项目 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="数值项目" name="projType">
                                    <label> 数值项目 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="包装项目" name="projType">
                                    <label> 包装项目 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                <span style="color: #ff0000; font-size: 16px;">*</span>项目组成员：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input type="text" name="projCustomers" class="form-control"
                                       style="display: none">
                                <input type="text" disabled class="form-control" id="to_cus">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick=""><i
                                            class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                项目概要：
                            </label>
                            <div class="col-md-10" style="padding: 0;padding-left: 10px">
                                <textarea name="projOutline" style="resize: vertical;width: 100%" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center-b">
                    <span style="background-color: white; padding: 2px 10px;">项目预期(售前跟踪使用)</span>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>
                                预计签单日期：
                            </label>
                            <div class="col-md-8">
                                <div class="input-append date form_datetime input-group">
                                    <input name="projExpectDate" class="form-control" type="date"/>
                                    <span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="padding-left:0">
                                预期金额：
                            </label>
                            <div class="input-group col-md-8" style="padding-left: 15px;">
                                <input type="text" name="projExpectMoney" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group　overflow">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                可能性：
                            </label>
                            <div class="col-md-10" style="padding: 0;padding-left: 10px">
                                <select name="projExpectPossibility" class="selectpicker"
                                        data-live-search="true"
                                        data-live-search="true" id="projExpectPossibility">
                                    <option style="height: 26px"></option>
                                    <option>10%</option>
                                    <option>20%</option>
                                    <option>30%</option>
                                    <option>40%</option>
                                    <option>50%</option>
                                    <option>60%</option>
                                    <option>70%</option>
                                    <option>80%</option>
                                    <option>90%</option>
                                    <option>100%</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="addcus_close">关闭</button>
        <button class="btn btn-default" type="button" id="mysubmit1"><i class="fa fa-check"></i>
            保存
        </button>
    </div>
    <!-- /.modal-content -->
</div>
<script src="/js/resjs/cus-check.js"></script>
<script>

    $(function () {
        $("#addcotsbb").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "tbCustomerByCusId.cusId": {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '客户不能为空'
                        }
                    }
                }, "tbContactsByCotsId.cotsName": {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '联系人对应不能为空'
                        }
                    }
                }
            }
        });
        var result = open_modal;
        var i = setForm();
        if (i == 1) {
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcotsbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    $.ajax({
                        url: 'project/update_project',
                        method: 'post',
                        async: true,
                        data: $('#addcotsbb').serialize(),
                        success: function (data) {
                            if (data.code == 400) {
                                swal("失败！", "项目修改失败！", "error");
                                $('#addproject').modal('hide');
                                $('#addproject').removeData('bs.modal');
                            } else {
                                swal("成功！", "项目修改成功！", "success");
                                $('#addproject').modal('hide');
                                $('#addproject').removeData('bs.modal');
                                $('#project-table').bootstrapTable('refresh');
                            }
                        },
                        error: function (data) {
                            swal("失败！", "项目修改失败！" + data.msg, "error")
                        }
                    });
                }
            });
            $("#addproject").on("hidden.bs.modal", function () {
                $(this).removeData("bs.modal");
            });
            open_modal = null;
        } else {
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcotsbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    if (open_modal == null) {
                        $.ajax({
                            url: 'project/add_project',
                            method: 'post',
                            async: true,
                            data: $('#addcotsbb').serialize(),
                            success: function (data) {
                                if (data.code == 400) {
                                    swal("失败！", "项目添加失败！", "error");
                                    $('#addproject').modal('hide');
                                    $('#addproject').removeData('bs.modal');
                                } else {
                                    swal("成功！", "项目添加成功！", "success");
                                    $('#addproject').modal('hide');
                                    $('#addproject').removeData('bs.modal');
                                    $('#project-table').bootstrapTable('refresh');
                                }
                            },
                            error: function (data) {
                                swal("失败！", "项目添加失败！" + data.msg, "Cancel");
                            }
                        });
                    }
                }
            });
            $("#addcus_close").click(function () {
                $('#addproject').modal('hide');
                $('#addproject').removeData("bs.modal");
            });
            open_modal = null;
        }
        $("[data-toggle='tooltip' ] ").tooltip();
        $(".selectpicker").selectpicker({
            noneSelectedText: ''
        });
        $('.selectpicker').selectpicker('refresh');
        $('.bootstrap-select').css('min-width', '213px');
        $('#province').css('min-width', '80px');
    });


    function setForm() {
        if (open_modal != null) {
            $.ajax({
                url: 'project/load_project?id=' + open_modal,
                method: 'get',
                async: true,
                success: function (data) {
                    $('input[name="projTheme"]').val(data.projTheme);
                    $('input[name="projId"]').val(data.projId);
                    $('input[name="tbCustomerByCusId.cusId"]').val(data.tbCustomerByCusId.cusId);
                    $('input[id="to_cus"]').val(data.tbCustomerByCusId.cusName);
                    $.ajax({
                        url: 'memorial/get_contact?id=' + data.tbCustomerByCusId.cusId,
                        method: 'get',
                        async: true,
                        success: function (data1) {
                            for (var i = 0; i < data1.length; i++) {
                                $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                                $('#cotsName').selectpicker('refresh');
                            }
                            $('#cotsName').selectpicker('val', data.serveContact);
                            $('#cotsName').selectpicker('refrash');
                        }
                    });
                    // if(data.tbUserByUserId.userId != null){
                    //     $('input[name="tbUserByUserId.userId"]').val(data.tbUserByUserId.userId);
                    // }
                    // $('input[id="to_user1"]').val(data.tbUserByUserId.userName);
                    $('input[name="projEntryDate"]').val(data.projEntryDate);
                    $('textarea[name="projOutline"]').val(data.projOutline);
                    $("#projExpectPossibility").selectpicker("val", data.projExpectPossibility);
                    redio1($('input[name="projState"]'), data.projState);
                    redio1($('input[name="projStage"]'), data.projStage);
                    redio1($('input[name="projPresale"]'), data.projPresale);
                    redio1($('input[name="projType"]'), data.projType);
                    $('input[name="projExpectDate"]').val(data.projExpectDate);
                    $('input[name="projExpectMoney"]').val(data.projExpectMoney);
                    $('input[name="projCustomers"]').val(data.projCustomers);
                    $('input[name="tbUserByUserId.userId"]').val(data.tbUserByUserId.userId);
                    $('#to_user').val(data.tbUserByUserId.userName);
                },
                error: function (data) {
                    swal("加载失败", result.msg, "error");
                    $('#addcots').modal('hide');
                    $('#addcots').removeData('bs.modal');
                }
            });
            // ajax1("customer/load_cus?id=" + open_modal, function (data) {
            //     alert(JSON.stringify(data))
            //     $('input[name="cusName"]').val(data.cusName);
            //     $('input[name="cusAbbreviation"]').val(data.cusAbbreviation);
            //     $('#cusType').selectpicker('val',data.cusType);
            // }, function () {
            //     swal("加载失败",result.msg,"error");
            //     $('#add').modal('hide')
            // }, "get", {});
            return 1;
        }
    }

    var i = 0;

    function chooseCus() {
        window.open("customer_check");
    }

    function getCus(cusId, cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
        $.ajax({
            url: 'memorial/get_contact?id=' + $("input[name='tbCustomerByCusId.cusId']").val(),
            method: 'get',
            async: true,
            success: function (data1) {
                $("#cotsName").empty();
                for (var i = 0; i < data1.length; i++) {
                    $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                    $('#cotsName').selectpicker('refresh');
                }
                $('#cotsName').selectpicker('refresh');
            }
        });
    }

    function redio1(dom, is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        dom.eq(2).removeProp('checked');
        dom.eq(3).removeProp('checked');
        dom.eq(4).removeProp('checked');
        dom.eq(5).removeProp('checked');
        if (is == "有效" || !is || is == "初期沟通" || is == "售前沟通" || is == "标准项目") {
            dom.eq(0).prop('checked', 'checked');
        } else if (is == "失败" || !is || is == "立项评估" || is == "签约实施" || is == "文本项目") {
            dom.eq(1).prop('checked', 'checked');
        } else if (is == "搁置" || !is || is == "需求分析" || is == "结项验收" || is == "数值项目") {
            dom.eq(2).prop('checked', 'checked');
        } else if (is == "放弃" || !is || is == "方案制定" || is == "包装项目") {
            dom.eq(3).prop('checked', 'checked');
        } else if (is == "商务谈判" || !is ) {
            dom.eq(4).prop('checked', 'checked');
        } else if (is == "合同签约" || !is ) {
            dom.eq(5).prop('checked', 'checked');
        }
    }

    function chooseUser() {
        // window.open("/admin/to_user_check");
        var a = $("<a href='/admin/to_user_check' target='_blank'>Apple</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent( 'click', true, true );
        a.dispatchEvent(e);
    }

    function getUser(userId, userName) {
        $("input[name='tbUserByUserId.userId']").val(userId);
        $("#to_user1").val(userName);
    }


</script>
</body>
</html>
