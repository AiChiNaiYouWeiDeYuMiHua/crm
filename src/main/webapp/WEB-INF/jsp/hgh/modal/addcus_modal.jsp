<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/6
  Time: 19:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        .city-picker-span {
            width: 480px;
        }
    </style>
</head>
<body>
<div class="">
    <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="btn btn-default pull-right">
            <i class="fa fa-check"></i>
            保存
        </button>
        <h4 class="modal-title" id="myModalLabel">
            客户基本信息
        </h4>
        <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
    </div>
    <div class="modal-body" style="padding-top: 0px;">
        <form id="addcusbb" action="">
            <div class="mybody">
                <div class="text-center-b">
                    <span style="background-color: white; padding: 2px 10px;">基本信息</span>
                    <input name="cusId" style="display: none">
                    <input name="tbUserByUserId.userId" value="${sessionScope.user.userId}" style="display: none">
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                <span style="color: #ff0000; font-size: 16px;">*</span>客户名称：
                            </label>
                            <div class="col-md-10" style="padding-left: 10px">
                                <input name="cusName" class="form-control" required="required"/>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>简称：
                            </label>
                            <div class="col-md-8">
                                <input name="cusAbbreviation" class="form-control" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">种类：</label>
                            <div class="col-md-8" style="position: relative;">
                                <select name="cusType" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cusType">
                                    <option></option>
                                    <option>客户</option>
                                    <option>供应商</option>
                                    <option>合作伙伴</option>
                                    <option>媒体</option>
                                    <option>其他</option>
                                </select>
                                <!--<a href="??" target="_blank" data-placement="bottom" title="数据字典" class="input-group-btn">
                                    <i class="fa fa-database"></i>
                                </a>-->
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">生命周期</span>
                </div>

                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">状态：</label>
                            <div class="col-md-8">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="潜在客户" name="cusLifecycle" checked>
                                    <label> 潜在客户 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="签约客户" name="cusLifecycle">
                                    <label> 签约客户 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="重复购买" name="cusLifecycle">
                                    <label> 重复购买 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="失效客户" name="cusLifecycle">
                                    <label> 失效客户 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">首次签约：</label>
                            <div class="col-md-8">
                                <div class="input-append date form_datetime input-group">
                                    <input name="cusDate" class="form-control" type="date"/>
                                    <span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">商务特征</span>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">定性：</label>
                            <div class="col-md-8">
                                <select name="cusDetermine" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cusDetermine">
                                    <option></option>
                                    <option>有价值</option>
                                    <option>无价值</option>
                                    <option>不确定</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">定级：</label>
                            <div class="col-md-8">
                                <select name="cusClass" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cusClass">
                                    <option></option>
                                    <option>大单</option>
                                    <option>正常</option>
                                    <option>小单</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">定量：</label>
                            <div class="col-md-8">
                                <select name="cusSignDate" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cusSignDate">
                                    <option></option>
                                    <option>预计本周</option>
                                    <option>预计下周</option>
                                    <option>预计本月</option>
                                    <option>预计下月</option>
                                    <option>过期</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">阶段：</label>
                            <div class="col-md-8">
                                <select name="cusStage" class="selectpicker" data-live-search="true" id="cusStage">
                                    <option></option>
                                    <option>接通电话</option>
                                    <option>寄送资料</option>
                                    <option>实施沟通</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                来源：
                            </label>
                            <div class="col-md-8">
                                <select name="cusCome" class="selectpicker" data-live-search="true" id="cusCome">
                                    <option></option>
                                    <option>电话来访</option>
                                    <option>客户介绍</option>
                                    <option>独立开发</option>
                                    <option>老客户</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                关系等级：
                            </label>
                            <div class="col-md-8">
                                <select name="cusLevel" class="selectpicker" data-live-search="true" id="cusLevel">
                                    <option></option>
                                    <option>密切</option>
                                    <option>较好</option>
                                    <option>一般</option>
                                    <option>较差</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">信用等级：</label>
                            <div class="col-md-8">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="高" name="cusCredit" checked>
                                    <label> 高 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="中" name="cusCredit">
                                    <label> 中 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="低" name="cusCredit">
                                    <label> 低 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                结款方式：
                            </label>
                            <div class="col-md-8">
                                <select name="cusKnot" class="selectpicker" data-live-search="true" id="cusKnot">
                                    <option></option>
                                    <option>现货现款</option>
                                    <option>货到付款</option>
                                    <option>先款后货</option>
                                    <option>帐期收款</option>
                                    <option>月结</option>
                                    <option>季结</option>
                                    <option>年结</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">客观特征</span></div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                行业：
                            </label>
                            <div class="col-md-8">
                                <input name="cusIndustry" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                人员规模：
                            </label>
                            <div class="col-md-8">
                                <select name="cusExistDate" class="selectpicker" data-live-search="true"
                                        id="cusExistDate">
                                    <option></option>
                                    <option>10人以内</option>
                                    <option>10-20人</option>
                                    <option>20-50人</option>
                                    <option>50-200人</option>
                                    <option>200人以上</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group overflow">
                            <label class="control-label col-md-2">公司简介：</label>
                            <div class="col-md-10">
                                <textarea name="cusSynopsis" style="resize: vertical;" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">联系方式</span></div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                电话：
                            </label>
                            <div class="col-md-8">
                                <input name="cusPhone" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                传真(QQ)：
                            </label>
                            <div class="col-md-8">
                                <input name="cusMSNQQ" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                网址：
                            </label>
                            <div class="col-md-10" style="padding-left: 10px;">
                                <input name="cusNet" class="form-control" placeholder="http://"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label col-md-2">
                            地区：
                        </label>
                        <div class="docs-methods">
                            <form class="form-inline">
                                <div id="distpicker">
                                    <div class="form-group  overflow col-sm-8">
                                        <div style="position: relative;">
                                            <input name="cusAddr" id="city-picker3" class="form-control" readonly
                                                   type="text"
                                                   data-toggle="city-picker" style="min-width: 360px;">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <button class="btn btn-default" id="reset" type="button"><i
                                                class="fa fa-refresh"></i></button>
                                        <button class="btn btn-default" id="destroy" type="button"><i
                                                class="fa fa-check"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                地址：
                            </label>
                            <div class="col-md-10">
                                <input name="cusArea" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                备注：
                            </label>
                            <div class="col-md-10">
                                <textarea name="cusRemark" style="resize: vertical;" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                <a class="btn btn-success" onclick="InitExcelFile(
                    $('input[name=\'cusId\']').val())">上传附件</a>
                            </label>
                            <div class="col-md-10" id="upload" style="display: none">
                                <form id="ffImport" method="post">
                                    <div style="padding: 5px">
                                        <input id="excelFile" type="file" multiple class="file-loading">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;display: none">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                              附件一:
                            </label>
                            <div class="col-md-10" id="upload">
                                <input name="cusFiles" readonly disabled id="fj1" style="border: 0px;outline:none;">
                                <a href="javascript:void (0)" onclick="filedownload($('#fj1').val())" style="text-decoration: none">下载</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;display: none">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                附件二:
                            </label>
                            <div class="col-md-10" id="upload" >
                                <input  name="cusFiles" readonly disabled id="fj2"    style="border: 0px;outline:none;">
                                <a href="javascript:void (0)" onclick="filedownload($('#fj2').val())" style="text-decoration: none">下载</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;display: none">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                附件三:
                            </label>
                            <div class="col-md-10" id="upload" >
                                <input name="cusFiles" readonly disabled id="fj3"style="border: 0px;outline:none;">
                                <a href="javascript:void (0)" onclick="filedownload($('#fj3').val())" style="text-decoration: none">下载</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;display: none">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                附件四:
                            </label>
                            <div class="col-md-10" id="upload" >
                                <input  name="cusFiles" readonly disabled id="fj4" style="border: 0px;outline:none;">
                                <a href="javascript:void (0)" onclick="filedownload($('#fj4').val())" style="text-decoration: none">下载</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;display: none">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                附件五:
                            </label>
                            <div class="col-md-10" id="upload" >
                                <input name="cusFiles" readonly disabled id="fj5"  style="border: 0px;outline:none;">
                                <a href="javascript:void (0)" onclick="filedownload($('#fj5').val())" style="text-decoration: none">下载</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="height: 50px; "></div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="addcus_close">关闭</button>
        <button class="btn btn-default" type="button" id="mysubmit1"><i class="fa fa-check"></i>
            保存
        </button>
    </div>
    <!-- /.modal-content -->
</div>
<script src="js/resjs/city-picker.data.js"></script>
<script src="js/resjs/city-picker.js"></script>
<script src="js/resjs/main.js"></script>
<script>
    $(function () {
        $("#addcusbb").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                cusName: {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '客户不能为空'
                        }
                    }
                }, cusAbbreviation: {
                    msg: '客户简称验证失败',
                    validators: {
                        notEmpty: {
                            message: '客户简称不能为空'
                        }
                    }
                }
            }
        });
        var result = open_modal;
        var i = setForm();
        if (i == 1) {
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcusbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    $.ajax({
                        url: 'customer/update_customer',
                        method: 'post',
                        async: true,
                        data: $('#addcusbb').serialize(),
                        success: function (data) {
                            swal("成功！", "客户修改成功！", "success");
                            $('#addcus').modal('hide');
                            $('#addcus').removeData('bs.modal');
                            $('#table111').bootstrapTable('refresh');
                        },
                        error: function (data) {
                            swal("失败！", "客户添加失败！" + data.msg, "Cancel")
                        }
                    });
                }
            });
            $("#addcus").on("hidden.bs.modal", function () {
                $(this).removeData("bs.modal");
            });
            open_modal = null;
        } else {
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcusbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    if (open_modal == null) {
                        $.ajax({
                            url: 'customer/add_customer',
                            method: 'post',
                            async: true,
                            data: $('#addcusbb').serialize(),
                            success: function (data) {
                                if(data.code == 400){
                                    swal("失败！", "客户名称或简称重复！", "error");
                                }else{
                                    swal("成功！", "客户添加成功！", "success");
                                    $('#addcus').modal('hide');
                                    $('#addcus').removeData('bs.modal');
                                    $('#table111').bootstrapTable('refresh');
                                }
                            },
                            error: function (data) {
                                swal("失败！", "客户添加失败！" + data.msg, "Cancel")
                            }
                        });
                    }
                }
            });
            $("#addcus_close").click(function () {
                $('#addcus').modal('hide');
                $('#addcus').removeData("bs.modal");
            });
            open_modal = null;
        }
        $("[data-toggle='tooltip' ] ").tooltip();
        $(".selectpicker").selectpicker({
            noneSelectedText: ''
        });
        $('.selectpicker').selectpicker('refresh');
        $('.bootstrap-select').css('min-width', '213px');
        $('#province').css('min-width', '80px');
    });

    function setForm() {
        if (open_modal != null) {
            $.ajax({
                url: 'customer/load_cus?id=' + open_modal,
                method: 'get',
                async: true,
                success: function (data) {
                    $('input[name="cusId"]').val(data.cusId);
                    $('input[name="cusName"]').val(data.cusName);
                    $('input[name="cusAbbreviation"]').val(data.cusAbbreviation);
                    $('#cusType').selectpicker('val', data.cusType);
                    redio1($('input[name="cusLifecycle"]'), data.cusLifecycle);
                    $('input[name="cusDate"]').val(data.cusDate);
                    $('#cusDetermine').selectpicker('val', data.cusDetermine);
                    $('#cusClass').selectpicker('val', data.cusClass);
                    $('#cusSignDate').selectpicker('val', data.cusSignDate);
                    $('#cusStage').selectpicker('val', data.cusStage);
                    $('#cusCome').selectpicker('val', data.cusCome);
                    $('#cusLevel').selectpicker('val', data.cusLevel);
                    redio1($('input[name="cusCredit"]'), data.cusCredit);
                    $('#cusKnot').selectpicker('val', data.cusKnot);
                    $('input[name="cusIndustry"]').val(data.cusIndustry);
                    $('#cusExistDate').selectpicker('val', data.cusExistDate);
                    $('textarea[name="cusSynopsis"]').val(data.cusSynopsis);
                    $('input[name="cusPhone"]').val(data.cusPhone);
                    $('input[name="cusMSNQQ"]').val(data.cusMSNQQ);
                    $('input[name="cusNet"]').val(data.cusNet);
                    $('input[name="cusAddr"]').val(data.cusAddr);
                    $.ajax({
                        url: 'customer/get_files?id='+$('input[name="cusId"]').val(),
                        method: 'get',
                        async: true,
                        success: function (data) {
                            alert(data.length);
                            if(data.length == 5){
                                return;
                            }
                            for(var i=1;i<=data.length;i++){
                                $('#fj'+i).removeAttr("disabled");
                                $('#fj'+i).parent().parent().parent().parent().css("display","block");
                                $('#fj'+i).val(data[i-1]);
                            }
                        },
                        error: function (data) {
                            swal("失败！", "客户添加失败！" + data.msg, "Cancel")
                        }
                    });
                    $('#city-picker3').citypicker('destroy');
                    $('input[name="cusArea"]').val(data.cusArea);
                    $('textarea[name="cusRemark"]').val(data.cusRemark);
                },
                error: function (data) {
                    swal("加载失败", result.msg, "error");
                    $('#addcus').modal('hide');
                    $('#addcus').removeData('bs.modal');
                }
            });
            // ajax1("customer/load_cus?id=" + open_modal, function (data) {
            //     alert(JSON.stringify(data))
            //     $('input[name="cusName"]').val(data.cusName);
            //     $('input[name="cusAbbreviation"]').val(data.cusAbbreviation);
            //     $('#cusType').selectpicker('val',data.cusType);
            // }, function () {
            //     swal("加载失败",result.msg,"error");
            //     $('#add').modal('hide')
            // }, "get", {});
            return 1;
        }
    }

    function redio1(dom, is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        dom.eq(2).removeProp('checked');
        dom.eq(3).removeProp('checked');
        if (is == "潜在客户" || !is) {
            dom.eq(0).prop('checked', 'checked');
        } else if (is == "签约客户" || !is) {
            dom.eq(1).prop('checked', 'checked');
        } else if (is == "重复购买" || !is) {
            dom.eq(2).prop('checked', 'checked');
        } else if (is == "失效客户" || !is) {
            dom.eq(3).prop('checked', 'checked');
        } else if (is == "高" || !is) {
            dom.eq(0).prop('checked', 'checked');
        } else if (is == "中" || !is) {
            dom.eq(1).prop('checked', 'checked');
        } else if (is == "低" || !is) {
            dom.eq(2).prop('checked', 'checked');
        }
    }
    
    function filedownload(filename) {
        alert(filename)
        window.open("files_download?filename="+filename);
    }


</script>


</body>
</html>
