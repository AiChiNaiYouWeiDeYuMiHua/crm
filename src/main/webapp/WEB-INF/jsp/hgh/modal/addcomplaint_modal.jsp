<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/13
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="">
    <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
            <span>&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">
            投诉管理
        </h4>
        <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
    </div>
    <div class="modal-body" style="padding-top: 0px;">
        <form id="addcotsbb" action="">
            <div class="mybody">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input name="complainId" style="display: none">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input type="text" name="tbCustomerByCusId.cusId" class="form-control"
                                       style="display: none">
                                <input type="text" disabled class="form-control" id="to_cus">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick="chooseCus()"><i
                                            class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;margin-top: 5px">
                                投诉主题：
                            </label>
                            <div class="col-md-10" style="padding: 0;padding-left: 10px">
                                <input name="complainTheme" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4" style="padding-left:0">
                                <span style="color: #ff0000; font-size: 16px;">*</span>
                                首问接待人：
                            </label>
                            <div class="input-group col-md-8" style="padding-left: 15px;">
                                <input type="text" name="tbUserByUserId.userId" style="display: none">
                                <input type="text"  disabled class="form-control" id="to_user1">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick="chooseUser()"><i
                                            class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>
                                分类：
                            </label>
                            <div class="col-md-8">
                                <select name="complainType" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="complainType">
                                    <option style="height: 26px"></option>
                                    <option>产品投诉</option>
                                    <option>服务投诉</option>
                                    <option>客户意见</option>
                                    <option>其他</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                描述：
                            </label>
                            <div class="col-md-10" style="padding: 0;padding-left: 10px">
                                <textarea name="complainBewrite" style="resize: vertical;width: 100%" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="margin-top: 5px;padding-right: 20px;">
                                日期：
                            </label>
                            <div class="col-md-7">
                                <div class="input-append date form_datetime input-group">
                                    <input name="complainDate" class="form-control" type="date"/>
                                    <span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group overflow">
                            <label class="control-label col-md-2" style="margin-top: 5px;padding-right: 20px;">
                                投诉人：
                            </label>
                            <div class="col-md-10">
                                <select name="tbContactsByCotsId.cotsId" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsName">
                                    <option style="height: 26px"></option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px">紧急程度：</label>
                            <div class="col-md-10">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="非常紧急" name="complainEmergency" checked>
                                    <label> 非常紧急 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="急" name="complainEmergency">
                                    <label> 急 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="普通" name="complainEmergency">
                                    <label> 普通 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                处理过程：
                            </label>
                            <div class="col-md-10" style="padding: 0;padding-left: 10px">
                                <textarea name="complainProcess" style="resize: vertical;width: 100%" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px">处理结果：</label>
                            <div class="col-md-10">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="未处理" name="complainResults" checked>
                                    <label> 未处理 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="处理中" name="complainResults">
                                    <label> 处理中 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="处理完成" name="complainResults">
                                    <label> 处理完成 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group overflow">
                            <label class="control-label col-md-2" style="margin-top: 5px;padding-right: 20px;">
                                花费时间：
                            </label>
                            <div class="col-md-10">
                                <select name="complainTimeSpent" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="complainTimeSpent">
                                    <option style="height: 26px"></option>
                                    <option>1小时</option>
                                    <option>2小时</option>
                                    <option>3小时</option>
                                    <option>半个工作日</option>
                                    <option>1个工作日</option>
                                    <option>2个工作日</option>
                                    <option>2个工作日以上</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 10px">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                客户反馈：
                            </label>
                            <div class="col-md-10" style="padding: 0;padding-left: 10px">
                                <textarea name="complainBack" style="resize: vertical;width: 100%" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                回访确认：
                            </label>
                            <div class="col-md-10" style="padding: 0;padding-left: 10px">
                                <textarea name="compainBackComfimed" style="resize: vertical;width: 100%" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                备注：
                            </label>
                            <div class="col-md-10" style="padding: 0;padding-left: 10px">
                                <textarea name="compainRemark" style="resize: vertical;width: 100%" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="addcus_close">关闭</button>
        <button class="btn btn-default" type="button" id="mysubmit1"><i class="fa fa-check"></i>
            保存
        </button>
    </div>
    <!-- /.modal-content -->
</div>
<script src="/js/resjs/cus-check.js"></script>
<script>

    $(function () {
        $("#addcotsbb").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "tbCustomerByCusId.cusId": {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '客户不能为空'
                        }
                    }
                }, "tbContactsByCotsId.cotsName": {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '联系人对应不能为空'
                        }
                    }
                }, medType: {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '必填项'
                        }
                    }
                }
            }
        });
        var result = open_modal;
        var i = setForm();
        if (i == 1) {
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcotsbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    $.ajax({
                        url: 'complaint/update_complaint',
                        method: 'post',
                        async: true,
                        data: $('#addcotsbb').serialize(),
                        success: function (data) {
                            if (data.code == 400) {
                                swal("失败！", "投诉修改失败！", "error");
                                $('#addcomplaint').modal('hide');
                                $('#addcomplaint').removeData('bs.modal');
                            } else {
                                swal("成功！", "投诉修改成功！", "success");
                                $('#addcomplaint').modal('hide');
                                $('#addcomplaint').removeData('bs.modal');
                                $('#complaint-table').bootstrapTable('refresh');
                            }
                        },
                        error: function (data) {
                            swal("失败！", "投诉添加失败！" + data.msg, "error")
                        }
                    });
                }
            });
            $("#addcomplaint").on("hidden.bs.modal", function () {
                $(this).removeData("bs.modal");
            });
            open_modal = null;
        } else {
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcotsbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    if (open_modal == null) {
                        $.ajax({
                            url: 'complaint/add_complaint',
                            method: 'post',
                            async: true,
                            data: $('#addcotsbb').serialize(),
                            success: function (data) {
                                if (data.code == 400) {
                                    swal("失败！", "投诉添加失败！", "error");
                                    $('#addcomplaint').modal('hide');
                                    $('#addcomplaint').removeData('bs.modal');
                                } else {
                                    swal("成功！", "投诉添加成功！", "success");
                                    $('#addcomplaint').modal('hide');
                                    $('#addcomplaint').removeData('bs.modal');
                                    $('#complaint-table').bootstrapTable('refresh');
                                }
                            },
                            error: function (data) {
                                swal("失败！", "纪念日添加失败！" + data.msg, "error");
                            }
                        });
                    }
                }
            });
            $("#addcus_close").click(function () {
                $('#addcomplaint').modal('hide');
                $('#addcomplaint').removeData("bs.modal");
            });
            open_modal = null;
        }
        $("[data-toggle='tooltip' ] ").tooltip();
        $(".selectpicker").selectpicker({
            noneSelectedText: ''
        });
        $('.selectpicker').selectpicker('refresh');
        $('.bootstrap-select').css('min-width', '213px');
        $('#province').css('min-width', '80px');
    });


    function setForm() {
        if (open_modal != null) {
            $.ajax({
                url: 'complaint/load_complaint?id=' + open_modal,
                method: 'get',
                async: true,
                success: function (data) {
                    $('input[name="complainTheme"]').val(data.complainTheme);
                    $('input[name="complainId"]').val(data.complainId );
                    $('input[name="tbCustomerByCusId.cusId"]').val(data.tbCustomerByCusId.cusId);
                    $('input[id="to_cus"]').val(data.tbCustomerByCusId.cusName);
                    $.ajax({
                        url: 'memorial/get_contact?id=' + data.tbCustomerByCusId.cusId,
                        method: 'get',
                        async: true,
                        success: function (data1) {
                            for (var i = 0; i < data1.length; i++) {
                                $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                                $('#cotsName').selectpicker('refresh');
                            }
                            $('#cotsName').selectpicker('val',data.serveContact);
                            $('#cotsName').selectpicker('refrash');
                        }
                    });
                    // if(data.tbUserByUserId.userId != null){
                    //     $('input[name="tbUserByUserId.userId"]').val(data.tbUserByUserId.userId);
                    // }
                    // $('input[id="to_user1"]').val(data.tbUserByUserId.userName);
                    $("#complainType").selectpicker("val",data.complainType);
                    $('textarea[name="complainBewrite"]').val(data.complainBewrite);
                    $('input[name="complainDate"]').val(data.complainDate);
                    $('input[name="tbUserByUserId.userId"]').val(data.tbUserByUserId.userId);
                    $('#to_user').val(data.tbUserByUserId.userName);
                    $('textarea[name="complainProcess"]').val(data.complainProcess);
                    $("#complainTimeSpent").selectpicker("val",data.complainTimeSpent);
                    $('textarea[name="complainBack"]').val(data.complainBack);
                    $('textarea[name="compainBackComfimed"]').val(data.compainBackComfimed);
                    $('textarea[name="compainRemark"]').val(data.compainRemark);
                    redio1($('input[name="complainEmergency"]'), data.complainEmergency);
                    redio1($('input[name="complainResults"]'), data.complainResults);

                },
                error: function (data) {
                    swal("加载失败", result.msg, "error");
                    $('#addcots').modal('hide');
                    $('#addcots').removeData('bs.modal');
                }
            });
            // ajax1("customer/load_cus?id=" + open_modal, function (data) {
            //     alert(JSON.stringify(data))
            //     $('input[name="cusName"]').val(data.cusName);
            //     $('input[name="cusAbbreviation"]').val(data.cusAbbreviation);
            //     $('#cusType').selectpicker('val',data.cusType);
            // }, function () {
            //     swal("加载失败",result.msg,"error");
            //     $('#add').modal('hide')
            // }, "get", {});
            return 1;
        }
    }

    var i = 0;

    function chooseCus() {
        window.open("customer_check");
    }
    function getCus(cusId,cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
        $.ajax({
            url: 'memorial/get_contact?id=' + $("input[name='tbCustomerByCusId.cusId']").val(),
            method: 'get',
            async: true,
            success: function (data1) {
                $("#cotsName").empty();
                for (var i = 0; i < data1.length; i++) {
                    $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                    $('#cotsName').selectpicker('refresh');
                }
                $('#cotsName').selectpicker('refresh');
            }
        });
    }
    function redio1(dom, is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        dom.eq(2).removeProp('checked');
        if (is == "非常紧急" || !is ||is == "未处理") {
            dom.eq(0).prop('checked', 'checked');
        } else if (is == "急" || !is ||is == "处理中") {
            dom.eq(1).prop('checked', 'checked');
        } else if (is == "普通" || !is ||is == "处理完成") {
            dom.eq(2).prop('checked', 'checked');
        }
    }
    function chooseUser() {
        // window.open("/admin/to_user_check");
        var a = $("<a href='/admin/to_user_check' target='_blank'>Apple</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent( 'click', true, true );
        a.dispatchEvent(e);
    }

    function getUser(userId, userName) {
        $("input[name='tbUserByUserId.userId']").val(userId);
        $("#to_user1").val(userName);
    }

</script>
</body>
</html>
