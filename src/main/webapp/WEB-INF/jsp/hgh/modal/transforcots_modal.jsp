<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/13
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="">
    <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="btn btn-default pull-right" data-dismiss="">
            <i class="fa fa-check"></i>
            x
        </button>
        <h4 class="modal-title" id="myModalLabel">
            联系人高级查询
        </h4>
        <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
    </div>
    <div class="modal-body" style="padding-top: 0px;">
        <form id="addcotscc" action="">
            <div class="mybody">
                <div class="text-center-b">
                    <span style="background-color: white; padding: 2px 10px;">联系人</span>
                    <input name="cotsId" style="display: none">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>姓名：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsName" class="form-control" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                移动电话：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsPersonalPhone" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input type="text" name="tbCustomerByCusId.cusId" class="form-control" style="display: none">
                                <input type="text" disabled class="form-control" id="to_cus">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick="chooseCus()"><i class="fa fa-search"></i>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <iframe id="choose_cus" style="display: none; margin-bottom: 10px;min-height: 600px;" src="customer_check" width="100%"   scrolling="no">

                        </iframe>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                联系人分类：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsClassification" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsClassification">
                                    <option style="height: 26px"></option>
                                    <option>特别重要</option>
                                    <option>重要</option>
                                    <option>普通</option>
                                    <option>不重要</option>
                                    <option>失效</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                负责业务：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsBusiness" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                性别：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsSex" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsSex">
                                    <option style="height: 26px"></option>
                                    <option>男</option>
                                    <option>女</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                微信：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsWechart" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">单位</span>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                称谓：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsTechnical" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                类型：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsType" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsType">
                                    <option style="height: 26px"></option>
                                    <option>联系人</option>
                                    <option>主联系人</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                部门：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsDepartment" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                职务：
                            </label>
                            <div class="col-md-8">
                                <input class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group overflow">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                上级(主)联系人：
                            </label>
                            <!--上级联系人-->
                            <div class="col-md-10" style="padding: 0;">
                                <select name="cotsUpcots" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsUpcots" style="margin-right: 10px">
                                    <option style="height: 26px"></option>
                                    <option>有价值</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">联系</span>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">工作电话：</label>
                            <div class="col-md-8">
                                <input name="cotsWorkphone" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">邮件地址：</label>
                            <div class="col-md-8">
                                <input name="cotsPlace" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">MSN(QQ)：</label>
                            <div class="col-md-8">
                                <input name="cotsMsnQq" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">传真：</label>
                            <div class="col-md-8">
                                <input name="cotsFax" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">邮编：</label>
                            <div class="col-md-8">
                                <input name="cotsZipcode" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                家庭住址：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input name="cotsHome" 　type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">其他</span></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                爱好：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input name="cotsInterest" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                证件类型：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsCardtype" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsCardtype">
                                    <option style="height: 26px"></option>
                                    <option>身份证</option>
                                    <option>军官证</option>
                                    <option>护照</option>
                                    <option>其他</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                证件号码：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsIdnum" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                备注：
                            </label>
                            <div class="col-md-10">
                                <textarea name="cotsRemark" style="resize: vertical;width: 100%" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">联系人画像</span></div>
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                习惯：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsHabits" class="selectpicker" data-live-search="true"
                                        multiple data-live-search="true" id="cotsHabits">
                                    <option>抽烟</option>
                                    <option>红酒</option>
                                    <option>白酒</option>
                                    <option>雪茄</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                性格：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsCharacter" class="selectpicker" data-live-search="true"
                                        multiple data-live-search="true" id="cotsCharacter">
                                    <option>内向</option>
                                    <option>外向</option>
                                    <option>理智型</option>
                                    <option>急脾气</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                社交特点：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsSocialCharacteristics" class="selectpicker" data-live-search="true"
                                        multiple data-live-search="true" id="cotsSocialCharacteristics">
                                    <option>对新事物感兴趣</option>
                                    <option>接受新事物能力强</option>
                                    <option>理解力强</option>
                                    <option>理解力弱</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                年收入：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsAnnualIncome" class="selectpicker" data-live-search="true"
                                        multiple data-live-search="true" id="cotsAnnualIncome">
                                    <option>无</option>
                                    <option>小于50K</option>
                                    <option>小于100K</option>
                                    <option>小于150K</option>
                                    <option>小于200K</option>
                                    <option>大于200K</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                消费习惯：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsConsumptionHabits" class="selectpicker" data-live-search="true"
                                        multiple data-live-search="true" id="cotsConsumptionHabits">
                                    <option>稳健谨慎</option>
                                    <option>品牌高质</option>
                                    <option>偏好奢侈</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="transforcots_modal">关闭</button>
        <button class="btn btn-default" type="button" id="mysubmit2"><i class="fa fa-check"></i>
            搜索
        </button>
    </div>
    <!-- /.modal-content -->
</div>
<script src="/js/resjs/cus-check.js"></script>
<script>
    $(function () {
        $("[data-toggle='tooltip' ] ").tooltip();
        $(".selectpicker").selectpicker({noneSelectedText: ''});
        $('.selectpicker').selectpicker('refresh');
        $('.bootstrap-select').css('min-width', '213px');
        $('#province').css('min-width', '80px');
    });
    $("#mysubmit2").click(function () {
        $("#transfer").modal('hide');
        d = f($("#addcotscc"));
        $("#cots-table").bootstrapTable('refresh');
        $("#tc").css('display','block');
    })
    $("#transforcots_modal").click(function () {
        $("#transfer").removeData("bs.modal");
        d = {};
        $("#tc").css('display','none');
        $("#cots-table").bootstrapTable('refresh');
    });

</script>
</body>
</html>
