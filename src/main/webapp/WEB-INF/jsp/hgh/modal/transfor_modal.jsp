<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/6
  Time: 19:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        .city-picker-span {
            width: auto;
        }
    </style>
</head>
<body>
<div class="">
    <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="btn btn-default pull-right">
            <i class="fa fa-check"></i>
            保存
        </button>
        <h4 class="modal-title" id="myModalLabel">
            高级查询
        </h4>
        <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
    </div>
    <div class="modal-body" style="padding-top: 0px;">
        <form id="addcuscc" action="">
            <div class="mybody">
                <div class="text-center-b">
                    <span style="background-color: white; padding: 2px 10px;">基本信息</span>
                    <input name="cusId" style="display: none">
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                <span style="color: #ff0000; font-size: 16px;">*</span>客户名称：
                            </label>
                            <div class="col-md-10" style="padding: 0">
                                <input name="cusName" class="form-control" required="required"/>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>简称：
                            </label>
                            <div class="col-md-8">
                                <input name="cusAbbreviation" class="form-control" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">种类：</label>
                            <div class="col-md-8" style="position: relative;">
                                <select name="cusType" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cusType">
                                    <option></option>
                                    <option>客户</option>
                                    <option>供应商</option>
                                    <option>合作伙伴</option>
                                    <option>媒体</option>
                                    <option>其他</option>
                                </select>
                                <!--<a href="??" target="_blank" data-placement="bottom" title="数据字典" class="input-group-btn">
                                    <i class="fa fa-database"></i>
                                </a>-->
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">生命周期</span>
                </div>

                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">状态：</label>
                            <div class="col-md-8">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="潜在客户" name="cusLifecycle">
                                    <label> 潜在客户 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="签约客户" name="cusLifecycle">
                                    <label> 签约客户 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="重复购买" name="cusLifecycle">
                                    <label> 重复购买 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="失效客户" name="cusLifecycle">
                                    <label> 失效客户 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">首次签约：</label>
                            <div class="col-md-8">
                                <div class="input-append date form_datetime input-group">
                                    <input name="cusDate" class="form-control" type="date"/>
                                    <span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">商务特征</span>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">定性：</label>
                            <div class="col-md-8">
                                <select name="cusDetermine" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cusDetermine">
                                    <option></option>
                                    <option>有价值</option>
                                    <option>无价值</option>
                                    <option>不确定</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">定级：</label>
                            <div class="col-md-8">
                                <select name="cusClass" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cusClass">
                                    <option></option>
                                    <option>大单</option>
                                    <option>正常</option>
                                    <option>小单</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">定量：</label>
                            <div class="col-md-8">
                                <select name="cusSignDate" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cusSignDate">
                                    <option></option>
                                    <option>预计本周</option>
                                    <option>预计下周</option>
                                    <option>预计本月</option>
                                    <option>预计下月</option>
                                    <option>过期</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">阶段：</label>
                            <div class="col-md-8">
                                <select name="cusStage" class="selectpicker" data-live-search="true" id="cusStage">
                                    <option></option>
                                    <option>接通电话</option>
                                    <option>寄送资料</option>
                                    <option>实施沟通</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                来源：
                            </label>
                            <div class="col-md-8">
                                <select name="cusCome" class="selectpicker" data-live-search="true" id="cusCome">
                                    <option></option>
                                    <option>电话来访</option>
                                    <option>客户介绍</option>
                                    <option>独立开发</option>
                                    <option>老客户</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                关系等级：
                            </label>
                            <div class="col-md-8">
                                <select name="cusLevel" class="selectpicker" data-live-search="true" id="cusLevel">
                                    <option></option>
                                    <option>密切</option>
                                    <option>较好</option>
                                    <option>一般</option>
                                    <option>较差</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">信用等级：</label>
                            <div class="col-md-8">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="高" name="cusCredit">
                                    <label> 高 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="中" name="cusCredit">
                                    <label> 中 </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" value="低" name="cusCredit">
                                    <label> 低 </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                结款方式：
                            </label>
                            <div class="col-md-8">
                                <select name="cusKnot" class="selectpicker" data-live-search="true" id="cusKnot">
                                    <option></option>
                                    <option>现货现款</option>
                                    <option>货到付款</option>
                                    <option>先款后货</option>
                                    <option>帐期收款</option>
                                    <option>月结</option>
                                    <option>季结</option>
                                    <option>年结</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">客观特征</span></div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                行业：
                            </label>
                            <div class="col-md-8">
                                <input name="cusIndustry" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                人员规模：
                            </label>
                            <div class="col-md-8">
                                <select name="cusExistDate" class="selectpicker" data-live-search="true"
                                        id="cusExistDate">
                                    <option></option>
                                    <option>10人以内</option>
                                    <option>10-20人</option>
                                    <option>20-50人</option>
                                    <option>50-200人</option>
                                    <option>200人以上</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group overflow">
                            <label class="control-label col-md-2">公司简介：</label>
                            <div class="col-md-10">
                                <textarea name="cusSynopsis" style="resize: vertical;" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">联系方式</span></div>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                电话：
                            </label>
                            <div class="col-md-8">
                                <input name="cusPhone" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                传真(QQ)：
                            </label>
                            <div class="col-md-8">
                                <input name="cusMSNQQ" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                网址：
                            </label>
                            <div class="col-md-10" style="padding: 0;">
                                <input name="cusNet" class="form-control" placeholder="http://"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label col-md-2">
                            地区：
                        </label>
                        <div class="docs-methods">
                            <form class="form-inline">
                                <div id="distpicker">
                                    <div class="form-group  overflow col-sm-8">
                                        <div style="position: relative;">
                                            <input name="cusAddr" id="city-picker3" class="form-control" readonly
                                                   type="text"
                                                   data-toggle="city-picker" style="min-width: 360px;">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <button class="btn btn-default" id="reset" type="button"><i
                                                class="fa fa-refresh"></i></button>
                                        <button class="btn btn-default" id="destroy" type="button"><i
                                                class="fa fa-check"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                地址：
                            </label>
                            <div class="col-md-10">
                                <input name="cusArea" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                备注：
                            </label>
                            <div class="col-md-10">
                                <textarea name="cusRemark" style="resize: vertical;" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="height: 50px; "></div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="transcus_close">关闭</button>
        <button class="btn btn-default" type="button" id="mysubmit2"><i class="fa fa-check"></i>
            搜索
        </button>
    </div>
    <!-- /.modal-content -->
</div>
<script src="js/resjs/city-picker.data.js"></script>
<script src="js/resjs/city-picker.js"></script>
<script src="js/resjs/main.js"></script>

</body>
<script>
    $(function () {
        $("[data-toggle='tooltip' ] ").tooltip();
        $(".selectpicker").selectpicker({noneSelectedText: ''});
        $('.selectpicker').selectpicker('refresh');
        $('.bootstrap-select').css('min-width', '213px');
        $('#province').css('min-width', '80px');
    });
    $("#mysubmit2").click(function () {
        $("#transfer").modal('hide');
        d = f($("#addcuscc"));
        $("#table111").bootstrapTable('refresh');
        $("#tc").css('display','block');
    })
    $("#transcus_close").click(function () {
        $("#transfer").removeData("bs.modal");
        d = {};
        $("#tc").css('display','none');
        $("#table111").bootstrapTable('refresh');
    });
</script>
</html>
