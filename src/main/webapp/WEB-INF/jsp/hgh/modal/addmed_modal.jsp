<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/13
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="">
    <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
            <span>&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">
            联系人纪念日
        </h4>
        <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
    </div>
    <div class="modal-body" style="padding-top: 0px;">
        <form id="addcotsbb" action="">
            <div class="mybody">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input name="medId" style="display: none">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input type="text" name="tbCustomerByCusId.cusId" class="form-control"
                                       style="display: none">
                                <input type="text" disabled class="form-control" id="to_cus">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick="chooseCus()"><i
                                            class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="row">--%>
                <%--<div class="col-md-12">--%>
                <%--<iframe id="choose_cus" style="display: none; margin-bottom: 10px;min-height: 600px;"--%>
                <%--src="customer_check" width="100%" scrolling="no">--%>

                <%--</iframe>--%>
                <%--</div>--%>
                <%--</div>--%>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>
                                联系人：
                            </label>
                            <div class="col-md-8">
                                <select name="tbContactsByCotsId.cotsId" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsName">
                                    <option style="height: 26px"></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>
                                纪念日类型：
                            </label>
                            <div class="col-md-8">
                                <select name="medType" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="medType">
                                    <option style="height: 26px"></option>
                                    <option>生日</option>
                                    <option>公司成立纪念日</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                纪念日：
                            </label>
                            <div class="col-md-7">
                                <div class="input-append date form_datetime input-group">
                                    <input name="medMemorialDate" class="form-control" type="date"/>
                                    <span class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                备注：
                            </label>
                            <div class="col-md-10">
                                <textarea name="medRemark" style="resize: vertical;width: 100%" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                下次提醒：
                            </label>
                            <div class="col-md-10">
                                <input name="medNextPoint" disabled class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="addcus_close">关闭</button>
        <button class="btn btn-default" type="button" id="mysubmit1"><i class="fa fa-check"></i>
            保存
        </button>
    </div>
    <!-- /.modal-content -->
</div>
<script src="/js/resjs/cus-check.js"></script>
<script>

    $(function () {
        $("#addcotsbb").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "tbCustomerByCusId.cusId": {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '客户不能为空'
                        }
                    }
                }, "tbContactsByCotsId.cotsName": {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '联系人对应不能为空'
                        }
                    }
                }, medType: {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '必填项'
                        }
                    }
                }
            }
        });
        var result = open_modal;
        var i = setForm();
        if (i == 1) {
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcotsbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    $.ajax({
                        url: 'memorial/update_memorial',
                        method: 'post',
                        async: true,
                        data: $('#addcotsbb').serialize(),
                        success: function (data) {
                            if (data.status == 400) {
                                swal("失败！", "纪念日修改失败！", "cancle");
                                $('#addcots').modal('hide');
                                $('#addcots').removeData('bs.modal');
                            } else {
                                swal("成功！", "纪念日修改成功！", "success");
                                $('#addmed').modal('hide');
                                $('#addmed').removeData('bs.modal');
                                $('#memorial-table').bootstrapTable('refresh');
                            }
                        },
                        error: function (data) {
                            swal("失败！", "纪念日添加失败！" + data.msg, "error")
                        }
                    });
                }
            });
            $("#addmed").on("hidden.bs.modal", function () {
                $(this).removeData("bs.modal");
            });
            open_modal = null;
        } else {
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcotsbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    if (open_modal == null) {
                        $.ajax({
                            url: 'memorial/add_memorial',
                            method: 'post',
                            async: true,
                            data: $('#addcotsbb').serialize(),
                            success: function (data) {
                                if (data.code == 400) {
                                    swal("失败！", "纪念日添加失败！", "error");
                                    $('#addmed').modal('hide');
                                    $('#addmed').removeData('bs.modal');
                                } else {
                                    swal("成功！", "纪念日添加成功！", "success");
                                    $('#addmed').modal('hide');
                                    $('#addmed').removeData('bs.modal');
                                    $('#memorial-table').bootstrapTable('refresh');
                                }
                            },
                            error: function (data) {
                                swal("失败！", "纪念日添加失败！" + data.msg, "error");
                            }
                        });
                    }
                }
            });
            $("#addcus_close").click(function () {
                $('#addmed').modal('hide');
                $('#addmed').removeData("bs.modal");
            });
            open_modal = null;
        }
        $("[data-toggle='tooltip' ] ").tooltip();
        $(".selectpicker").selectpicker({
            noneSelectedText: ''
        });
        $('.selectpicker').selectpicker('refresh');
        $('.bootstrap-select').css('min-width', '213px');
        $('#province').css('min-width', '80px');
    });


    function setForm() {
        if (open_modal != null) {
            $.ajax({
                url: 'memorial/load_memorial?id=' + open_modal,
                method: 'get',
                async: true,
                success: function (data) {
                    $('input[name="medId"]').val(data.medId);
                    $('input[name="tbCustomerByCusId.cusId"]').val(data.tbCustomerByCusId.cusId);
                    $('input[id="to_cus"]').val(data.tbCustomerByCusId.cusName);

                    $.ajax({
                        url: 'memorial/get_contact?id=' + data.tbCustomerByCusId.cusId,
                        method: 'get',
                        async: true,
                        success: function (data1) {
                            for (var i = 0; i < data1.length; i++) {
                                $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                                $('#cotsName').selectpicker('refresh');
                            }

                        }
                    });
                    $('input[name="medMemorialDate"]').val(data.medMemorialDate);
                    $('input[name="medNextPoint"]').val(data.medNextPoint);
                    $('textarea[name="medRemark"]').val(data.medRemark);
                    $('#medType').selectpicker('val', data.medType);
                    $('#cotsName').selectpicker('val', data.tbContactsByCotsId.cotsName);
                    $('#cotsName').selectpicker('refresh');


                },
                error: function (data) {
                    swal("加载失败", result.msg, "error");
                    $('#addcots').modal('hide');
                    $('#addcots').removeData('bs.modal');
                }
            });
            // ajax1("customer/load_cus?id=" + open_modal, function (data) {
            //     alert(JSON.stringify(data))
            //     $('input[name="cusName"]').val(data.cusName);
            //     $('input[name="cusAbbreviation"]').val(data.cusAbbreviation);
            //     $('#cusType').selectpicker('val',data.cusType);
            // }, function () {
            //     swal("加载失败",result.msg,"error");
            //     $('#add').modal('hide')
            // }, "get", {});
            return 1;
        }
    }

    var i = 0;

    function chooseCus() {
        window.open("customer_check");
    }

    function getCus(cusId,cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
        $.ajax({
            url: 'memorial/get_contact?id=' + $("input[name='tbCustomerByCusId.cusId']").val(),
            method: 'get',
            async: true,
            success: function (data1) {
                $("#cotsName").empty();
                for (var i = 0; i < data1.length; i++) {
                    $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                    $('#cotsName').selectpicker('refresh');
                }
                $('#cotsName').selectpicker('refresh');
            }
        });

    }
</script>
</body>
</html>
