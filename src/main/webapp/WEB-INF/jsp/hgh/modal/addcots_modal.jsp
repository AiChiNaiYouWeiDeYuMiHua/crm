<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/13
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="">
    <div class="modal-header" style="border-bottom: none;">
        <button type="button" class="btn btn-default pull-right">
            <i class="fa fa-check"></i>
            保存
        </button>
        <h4 class="modal-title" id="myModalLabel">
            联系人
        </h4>
        <hr class="boder-t-a" style="border-top: 1px solid #aaaaaa;"/>
    </div>
    <div class="modal-body" style="padding-top: 0px;">
        <form id="addcotsbb" action="">
            <div class="mybody">
                <div class="text-center-b">
                    <span style="background-color: white; padding: 2px 10px;">联系人</span>
                    <input name="cotsId" style="display: none">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                <span style="color: #ff0000; font-size: 16px;">*</span>姓名：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsName" class="form-control" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                移动电话：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsPersonalPhone" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input type="text" name="tbCustomerByCusId.cusId" class="form-control" style="display: none">
                                <input type="text" disabled class="form-control" id="to_cus">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default"
                                            style="color: black;height: 34px" onclick="chooseCus()"><i class="fa fa-search"></i>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <iframe id="choose_cus" style="display: none; margin-bottom: 10px;min-height: 600px;" src="customer_check" width="100%"   scrolling="no">

                        </iframe>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                联系人分类：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsClassification" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsClassification">
                                    <option style="height: 26px"></option>
                                    <option>特别重要</option>
                                    <option>重要</option>
                                    <option>普通</option>
                                    <option>不重要</option>
                                    <option>失效</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                负责业务：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsBusiness" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                性别：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsSex" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsSex">
                                    <option style="height: 26px"></option>
                                    <option>男</option>
                                    <option>女</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                微信：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsWechart" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                项目：
                            </label>
                            <div class="col-md-10" style="padding-left: 10px;">
                                <input id="projName" style="display: none">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">单位</span>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                称谓：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsTechnical" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                类型：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsType" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsType">
                                    <option style="height: 26px"></option>
                                    <option>联系人</option>
                                    <option>主联系人</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                部门：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsDepartment" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                职务：
                            </label>
                            <div class="col-md-8">
                                <input class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">联系</span>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">工作电话：</label>
                            <div class="col-md-8">
                                <input name="cotsWorkphone" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">邮件地址：</label>
                            <div class="col-md-8">
                                <input name="cotsPlace" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">MSN(QQ)：</label>
                            <div class="col-md-8">
                                <input name="cotsMsnQq" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">传真：</label>
                            <div class="col-md-8">
                                <input name="cotsFax" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">邮编：</label>
                            <div class="col-md-8">
                                <input name="cotsZipcode" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                家庭住址：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input name="cotsHome" 　type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">其他</span></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                爱好：
                            </label>
                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                <input name="cotsInterest" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                证件类型：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsCardtype" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsCardtype">
                                    <option style="height: 26px"></option>
                                    <option>身份证</option>
                                    <option>军官证</option>
                                    <option>护照</option>
                                    <option>其他</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px">
                                证件号码：
                            </label>
                            <div class="col-md-8">
                                <input name="cotsIdnum" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                备注：
                            </label>
                            <div class="col-md-10">
                                <textarea name="cotsRemark" style="resize: vertical;width: 100%" class="form-control"
                                          onclick="$(this).css('min-height','130px')"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                照片：
                            </label>
                            <div class="col-md-10">
                                <input name="cotsPhoto" style="display: none">
                                <img src="" id="cotsPhoto" alt="联系人照片" style="width: 100px;height: 80px;display: none;">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">联系人画像</span></div>
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                习惯：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsHabits" class="selectpicker" data-live-search="true"
                                        multiple data-live-search="true" id="cotsHabits">
                                    <option>抽烟</option>
                                    <option>红酒</option>
                                    <option>白酒</option>
                                    <option>雪茄</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                性格：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsCharacter" class="selectpicker" data-live-search="true"
                                        multiple data-live-search="true" id="cotsCharacter">
                                    <option>内向</option>
                                    <option>外向</option>
                                    <option>理智型</option>
                                    <option>急脾气</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                社交特点：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsSocialCharacteristics" class="selectpicker" data-live-search="true"
                                        multiple data-live-search="true" id="cotsSocialCharacteristics">
                                    <option>对新事物感兴趣</option>
                                    <option>接受新事物能力强</option>
                                    <option>理解力强</option>
                                    <option>理解力弱</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                年收入：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsAnnualIncome" class="selectpicker" data-live-search="true"
                                        data-live-search="true" id="cotsAnnualIncome">
                                    <option style="height: 26px"></option>
                                    <option>小于50K</option>
                                    <option>小于100K</option>
                                    <option>小于150K</option>
                                    <option>小于200K</option>
                                    <option>大于200K</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-6">
                        <div class="form-group overflow">
                            <label class="control-label col-md-4">
                                消费习惯：
                            </label>
                            <div class="col-md-8">
                                <select name="cotsConsumptionHabits" class="selectpicker" data-live-search="true"
                                        multiple data-live-search="true" id="cotsConsumptionHabits">
                                    <option>稳健谨慎</option>
                                    <option>品牌高质</option>
                                    <option>偏好奢侈</option>
                                </select>
                                <a href="??" target="_blank" data-placement="bottom" title="数据字典"
                                   style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                    <i class="fa fa-book"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                <a class="btn btn-success" onclick="InitExcelFile()">上传附件</a>
                            </label>
                            <div class="col-md-10" id="upload" style="display: none">
                                <form id="ffImport" method="post">
                                    <div style="padding: 5px">
                                        <input id="excelFile" type="file" disabled>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="addcus_close">关闭</button>
        <button class="btn btn-default" type="button" id="mysubmit1"><i class="fa fa-check"></i>
            保存
        </button>
    </div>
    <!-- /.modal-content -->
</div>
<script src="/js/resjs/cus-check.js"></script>
<script>
    var viewer = new Viewer(document.getElementById('cotsPhoto'));
    $(function () {

        $("#addcotsbb").bootstrapValidator({
            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                cotsName: {
                    msg: '联系人验证失败',
                    validators: {
                        notEmpty: {
                            message: '联系人不能为空'
                        }
                    }
                },  cusName: {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '对应客户不能为空'
                        }
                    }
                }, cotsPersonalPhone: {
                    msg: '客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '对应客户不能为空'
                        },regexp: {
                            regexp: /\d{11}$/,
                            message: '请输入正确的号码'
                        }
                    }
                },cotsWechart: {
                    msg: '客户验证失败',
                    validators: {
                        regexp: {
                            regexp: /^[0-9a-zA-Z_]{1,}$/,
                            message: '请输入正确的微信号'
                        }
                    }
                },cotsWorkphone: {
                    msg: '客户验证失败',
                    validators: {
                        regexp: {
                            regexp: /\d{7}$/,
                            message: '请输入正确的工作电话'
                        }
                    }
                },cotsPlace: {
                    msg: '客户验证失败',
                    validators: {
                        regexp: {
                            regexp: /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/,
                            message: '请输入正确的邮箱地址'
                        }
                    }
                },cotsMsnQq: {
                    msg: '客户验证失败',
                    validators: {
                        regexp: {
                            regexp: /^[0-9a-zA-Z_]{1,}$/,
                            message: '请输入正确的MSN(QQ)'
                        }
                    }
                },cotsFax: {
                    msg: '客户验证失败',
                    validators: {
                        regexp: {
                            regexp: /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/,
                            message: '请输入正确的传真'
                        }
                    }
                },cotsZipcode: {
                    msg: '客户验证失败',
                    validators: {
                        regexp: {
                            regexp: /^[1-9]\d{5}(?!\d)$/,
                            message: '请输入正确的邮编'
                        }
                    }
                }
            }
        });
        var result = open_modal;
        var i = setForm();
        if (i == 1) {
            $('img[id="cotsPhoto"]').css("display","none");
            $("#excelFile").removeAttr("disabled");
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcotsbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    $.ajax({
                        url: 'contact/update_contact',
                        method: 'post',
                        async: true,
                        data: $('#addcotsbb').serialize(),
                        success: function (data) {
                            if(data.code == 400){
                                swal("失败！", "联系人修改失败！", "error");
                                $('#addcots').modal('hide');
                                $('#addcots').removeData('bs.modal');
                            }else{
                                swal("成功！", "联系人修改成功！", "success");
                                $('#addcots').modal('hide');
                                $('#addcots').removeData('bs.modal');
                                $('#cots-table').bootstrapTable('refresh');
                            }
                        },
                        error: function (data) {
                            swal("失败！", "客户添加失败！" + data.msg, "Cancel")
                        }
                    });
                }
            });
            $("#addcots").on("hidden.bs.modal", function () {
                $(this).removeData("bs.modal");
            });
            open_modal = null;
        } else {
            $("#mysubmit1").click(function () {
                //获取表单对象
                var bootstrapValidator = $("#addcotsbb").data('bootstrapValidator');
                //手动触发验证
                bootstrapValidator.validate();
                if (bootstrapValidator.isValid()) {
                    // var s = $("input[type='date']").valueOf();
                    // alert(s);
                    if (open_modal == null) {
                        $.ajax({
                            url: 'contact/add_contact',
                            method: 'post',
                            async: true,
                            data: $('#addcotsbb').serialize(),
                            success: function (data) {
                                if(data.code == 400){
                                    swal("失败！", "联系人添加失败！", "error");
                                    $('#addcots').modal('hide');
                                    $('#addcots').removeData('bs.modal');
                                }else{
                                    swal("成功！", "联系人添加成功！", "success");
                                    $('#addcots').modal('hide');
                                    $('#addcots').removeData('bs.modal');
                                    $('#cots-table').bootstrapTable('refresh');
                                }
                            },
                            error: function (data) {
                                swal("失败！", "联系人添加失败！" + data.msg, "error");
                            }
                        });
                    }
                }
            });
            $("#addcus_close").click(function () {
                $('#addcots').modal('hide');
                $('#addcots').removeData("bs.modal");
            });
            open_modal = null;
        }
        $("[data-toggle='tooltip' ] ").tooltip();
        $(".selectpicker").selectpicker({
            noneSelectedText: ''
        });
        $('.selectpicker').selectpicker('refresh');
        $('.bootstrap-select').css('min-width', '213px');
        $('#province').css('min-width', '80px');
    });


    function setForm() {
        if (open_modal != null) {
            $.ajax({
                url: 'contact/load_cots?id=' + open_modal,
                method: 'get',
                async: true,
                success: function (data) {
                    $('input[name="cotsId"]').val(data.cotsId);
                    $('input[name="cotsName"]').val(data.cotsName);
                    $('input[name="cotsPersonalPhone"]').val(data.cotsPersonalPhone);
                    $('input[name="tbCustomerByCusId.cusId"]').val(data.tbCustomerByCusId.cusId);
                    $('input[id="to_cus"]').val(data.tbCustomerByCusId.cusName);
                    $('#cotsClassification').selectpicker('val', data.cotsClassification);
                    $('input[name="cotsBusiness"]').val(data.cotsBusiness);
                    $('#cotsSex').selectpicker('val', data.cotsSex);
                    $('input[name="cotsWechart"]').val(data.cotsWechart);
                    $('input[name="cotsUpcots"]').val(data.cotsUpcots);
                    $('input[name="cotsTechnical"]').val(data.cotsTechnical);
                    $('#cotsType').selectpicker('val', data.cotsType);
                    $('input[name="cotsDepartment"]').val(data.cotsDepartment);
                    $('#cotsPoint').selectpicker('val', data.cotsPoint);
                    $('input[name="cotsWorkphone"]').val(data.cotsWorkphone);
                    $('input[name="cotsPlace"]').val(data.cotsPlace);
                    $('input[name="cotsMsnQq"]').val(data.cotsMsnQq);
                    $('input[name="cotsFax"]').val(data.cotsFax);
                    $('input[name="cotsZipcode"]').val(data.cotsZipcode);
                    $('input[name="cotsInterest"]').val(data.cotsInterest);
                    $('#cotsCardtype').selectpicker('val', data.cotsCardtype);
                    $('input[name="cotsIdnum"]').val(data.cotsIdnum);
                    $('input[name="cotsHome"]').val(data.cotsHome);
                    if(data.cotsPhoto != null&&data.cotsPhoto != ""){
                        $('img[id="cotsPhoto"]').css("display","block");
                        $('img[id="cotsPhoto"]').attr("src","/img/"+data.cotsPhoto);
                        $('input[name="cotsPhoto"]').val(data.cotsPhoto);
                    }else{
                        $('img[id="cotsPhoto"]').css("display","none");
                    }
                    $('textarea[name="cotsRemark"]').val(data.cotsRemark);
                    var cotsHabits = eval(data.cotsHabits);
                    $.each(cotsHabits, function (i) {
                        $("<option>"+data[i].text+"</option>")
                            .appendTo("#cotsHabits .selectpicker");
                    });
                    $('#cotsHabits').selectpicker('val', data.cotsHabits);
                    $('#cotsCharacter').selectpicker('val', data.cotsCharacter);
                    $('#cotsSocialCharacteristics').selectpicker('val', data.cotsSocialCharacteristics);
                    $('#cotsAnnualIncome').selectpicker('val', data.cotsAnnualIncome);
                    $('#cotsConsumptionHabits').selectpicker('val', data.cotsConsumptionHabits);
                },
                error: function (data) {
                    swal("加载失败", result.msg, "error");
                    $('#addcots').modal('hide');
                    $('#addcots').removeData('bs.modal');
                }
            });
            // ajax1("customer/load_cus?id=" + open_modal, function (data) {
            //     alert(JSON.stringify(data))
            //     $('input[name="cusName"]').val(data.cusName);
            //     $('input[name="cusAbbreviation"]').val(data.cusAbbreviation);
            //     $('#cusType').selectpicker('val',data.cusType);
            // }, function () {
            //     swal("加载失败",result.msg,"error");
            //     $('#add').modal('hide')
            // }, "get", {});
            return 1;
        }
    }

    var i = 0;
    function chooseCus() {
        if(i == 0){
            $("#choose_cus").css("display","block");i=1;
        }else{
            $("#choose_cus").css("display","none");i=0;
        }
    }
    function chooseCus() {
        window.open("customer_check");
    }

    function getCus(cusId,cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
    }

    function fuzhi(result) {
        $('img[id="cotsPhoto"]').css("display","block");
        $('img[id="cotsPhoto"]').attr("src","/img/"+result.uri);
        $('input[name="cotsPhoto"]').val(result.uri);
        $("#upload").css("display","none");
    }



</script>
</body>
</html>
