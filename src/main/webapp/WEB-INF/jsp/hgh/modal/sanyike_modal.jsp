<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>三一客弹框</title>
</head>
<style type="text/css">
.font-style {
	font-size: 16px;
	color: #7F8C8D;
}
 .input-group-addon{
 	padding:3px;
 	}
 .icon a{
 	color: #7F8C8D;
 }
</style>
<body class="">
	<div class="modal-header" style="border-bottom: none">
		 
		<div class="row">
			<div class="col-md-12 text-center">
					<span style="font-weight: 500; font-size: 20px;">
					<span class="glyphicon glyphicon-star"></span> 
					三定一节点</span>
			</div>
		</div>
	</div>

	<div class="modal-body">
		<form class="form-horizontal  font-style" id="three_one_form"role="form" id="threeOneForm">
			<input type="hidden" id="threeCusno" name="tbCustomerByCusId.cusId"value="${cusInfo.cusId}">
			<div class="row text-right" style="font-size: 14px;">
				<div class="col-md-12">1.定性</div>
			</div>
			<div class="row text-center icon" style="background-color: #EEF0F1;">
				<div class="col-md-4">
					<div class="form-group">
						<a id="three_val1">
							<div style="margin-top: 10%;">
							<i class="fa fa-thumbs-up fa-2x"></i>
						</div>
						<label
							style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
							有价值</label>
						</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<a id="three_val2">
						<div style="margin-top: 10%;">
							<i class="fa fa-thumbs-down fa-2x"></i>
						</div>
						<label
							style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
							无价值</label>
						</a>
					</div>
					
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<a id="three_val3">
						<div style="margin-top: 10%;">
							<i class="fa fa-question fa-2x"></i>
						</div>
						<label
							style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
							不确定</label>
					</a>
					</div>
				</div>
				<input value="" id="three_val" type="hidden"/>
			</div>

			<div class="row text-right" style="font-size: 14px;">
				<div class="col-md-12">2.定级</div>
			</div>
			<div class="row text-center icon" style="background-color: #EEF0F1;">
				<div class="col-md-4">
					<div class="form-group">
					<a id="three_level1">
						<div style="margin-top: 10%;">
							<i class="fa fa-smile-o fa-2x"></i>
						</div>
						<label
							style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
							大单</label>
					</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<a id="three_level2">
						<div style="margin-top: 10%;">
							<i class="fa fa-meh-o fa-2x"></i>
						</div>
						<label
							style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
							正常</label>
					</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<a id="three_level3">
						<div style="margin-top: 10%;">
							<i class="fa fa-frown-o fa-2x"></i>
						</div>
						<label
							style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
							小单</label>
					</a>
					</div>
				</div>
				<input value="" id="three_level" type="hidden"/>
			</div>

			<div class="row text-right" style="font-size: 14px;">
				<div class="col-md-12">3.定量</div>
			</div>
			<div class="row text-center" style="background-color: #EEF0F1;">
				<div class="row" style="padding-top: 10px; font-size: 14px;">预估签约日期</div>
				<div class="row icon">
					<div class="col-md-3">
						<div class="form-group">
						<a id="three_qua1">
							<div style="margin-top: 10%;">
								<i class="fa fa-cloud fa-2x"></i>
							</div>
							<label
								style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
								本周</label>
								</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						<a id="three_qua2">
							<div style="margin-top: 10%;">
								<i class="fa fa-cloud-download fa-2x"></i>
							</div>
							<label
								style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
								下周</label>
							</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						<a id="three_qua3">
							<div style="margin-top: 10%;">
								<i class="fa fa-star fa-2x"></i>
							</div>
							<label
								style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
								本月</label>
						</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
						<a id="three_qua4">
							<div style="margin-top: 10%;">
								<i class="fa fa-star-o fa-2x"></i>
							</div>
							<label
								style="font-weight: 500; font-family: '微软雅黑'; padding-right: 0px;">
								下月</label>
							</a>
						</div>
					</div>
				</div>
				<div class="row" style="padding-bottom: 10px; font-size: 14px;">预估签约金额</div>
				<div class="row" style="padding-bottom: 10px;">
					<div class="col-md-6 spanClick">
						<span>预估金额：</span>
						<a  id="btn_esAmount" data-toggle="modal" data-target="#esAmount" style="text-decoration: none;cursor: pointer">
							<span id="spa_esAmount">输入预估金额</span>
							 <span id="inp_esAmount"></span>
						</a>
						
					</div>
					<div class="col-md-6 spanClick">
						<span>预估销量：</span>
						<a  id="btn_esAmount" data-toggle="modal" data-target="#esSale" style="text-decoration: none;cursor: pointer">
							<span id="spa_esSale">输入预估销量</span>
							 <span id="inp_esSale"></span>
						</a>
					</div>
					<input name="threeContent" value="" id="three_qua" type="hidden"/>
                    <input name="tbUserByUserId.userId" value="${sessionScope.user.userId}" type="hidden"/>
				</div>
			</div>

		</form>
	</div>
	 
	<div class="modal-footer" style="border-top: none">
		<button id="close_thrOneModel" type="button" class="btn btn-default btn-sm"
			data-dismiss="modal">
			<span class="glyphicon glyphicon-remove"></span>关闭
		</button>
		<c:choose>
			<c:when test="${cusInfo.cusLifecycle == '签约客户'}">
		<button type="button" id="save_thrOneModel"  class="btn btn-default btn-sm" disabled >
			<span class="glyphicon glyphicon-ok"></span>保存
		</button><br>
				<div style="text-align: center"><span>已签约客户无法进行三一客编辑</span></div>
			</c:when>
			<c:otherwise>
				<button type="button" id="save_thrOneModel"  class="btn btn-default btn-sm">
					<span class="glyphicon glyphicon-ok"></span>保存
				</button>
			</c:otherwise>
		</c:choose>
	</div>
		 
        
		<!-- 预计金额模态框（Modal） -->
			<div class="modal fade" id="esAmount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog" style="width: 200px;border-color: #F5F5F5">
						<div class="modal-content">
							<div class="modal-header" style="border-bottom:none;text-align: center;">
								 预计金额
							</div>
							
							<div class="modal-body" >
						<form class="form-horizontal  font-style" role="form">
							 
								<div class="input-group spinner" data-trigger="spinner">
							<input type="text" id="money_wan" class="form-control text-center" value="0"
								data-min="0" data-max="9999" data-step="1" data-rule="quantity">
								<span class="input-group-addon" > 
								<a href="javascript:;"
								class="spin-up" data-spin="up">
									<span class="glyphicon glyphicon-triangle-top"></span>
								</a> 
								<a href="javascript:;" class="spin-down" data-spin="down">
									<span class="glyphicon glyphicon-triangle-bottom"></span>
								</a>
								
							</span>
							 <span class="pull-right" style="padding-bottom: 50%;padding-left: 15%;color: #000">万</span>
						 	</div>
							
							
								<div class="input-group spinner" data-trigger="spinner">
							<input type="text" id="money_qian" class="form-control text-center" value="0"
								data-min="0" data-max="9" data-step="1" data-rule="quantity">
								<span class="input-group-addon" > 
								<a href="javascript:;"
								class="spin-up" data-spin="up">
									<span class="glyphicon glyphicon-triangle-top"></span>
								</a> 
								<a href="javascript:;" class="spin-down" data-spin="down">
									<span class="glyphicon glyphicon-triangle-bottom"></span>
								</a>
								
							</span>
							 <span class="pull-right" style="padding-bottom: 50%;padding-left: 15%;color: #000">千</span>
						 	</div>
						</form>
				</div>

							<div class="modal-footer" style="border-top: none">
							<div class="row">
								<div class="col-md-6">
										<button type="button" id="close_esAmount" class="btn btn-default btn-sm" data-dismiss="">
									<span class="glyphicon glyphicon-remove"></span>取消
									</button>
								
								</div>
								<div class="col-md-6">
										<button type="button" id="sure_esAmount"style="background-color: #454545;color:#FFF" class="btn btn-default btn-sm">
									<span class="glyphicon glyphicon-ok"></span>确认
								</button>
								
								</div>
							</div>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal -->
				</div>
				<!-- 预计销量模态框（Modal） -->
			<div class="modal fade" id="esSale" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog" style="width: 200px;border-color: #F5F5F5">
						<div class="modal-content">
							<div class="modal-header" style="border-bottom:none;text-align: center;">
								 预计销量
							</div>
							
							<div class="modal-body" >
						<form class="form-horizontal  font-style" role="form">
							 
								<div class="input-group spinner" data-trigger="spinner ">
							<input type="text" id="esSize" class="form-control text-center" value="0"
								data-min="0" data-max="9999" data-step="1" data-rule="quantity">
								<span class="input-group-addon" > 
								<a href="javascript:;"
								class="spin-up" data-spin="up">
									<span class="glyphicon glyphicon-triangle-top"></span>
								</a> 
								<a href="javascript:;" class="spin-down" data-spin="down">
									<span class="glyphicon glyphicon-triangle-bottom"></span>
								</a>
								
							</span>
							
							 <span class="pull-right" style="font-size:12px;color: #000">平米</span>
						 	</div>
						</form>
					</div>

							<div class="modal-footer" style="border-top: none">
							<div class="row">
								<div class="col-md-6">
										<button type="button" id="close_esSale" class="btn btn-default btn-sm" data-dismiss="modal">
									<span class="glyphicon glyphicon-remove"></span>取消
									</button>

								</div>
								<div class="col-md-6">
										<button type="button" id="sure_esSale"style="background-color: #454545;color:#FFF" class="btn btn-default btn-sm">
									<span class="glyphicon glyphicon-ok"></span>确认
								</button>
								
								</div>
							</div>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal -->
				</div>
	
</body>

<script type="text/javascript">
var value="${cusInfo.cusId}";
	//初始化三一客状态
	$.ajax({
		url:'customer/load_cus',
	async : false,//同步，会阻塞操作
	type : 'POST',//PUT DELETE POST
	data : {
		id:value
	},
	contentType : 'application/x-www-form-urlencoded; charset=utf-8',
	dataType : 'json',
	success : function(s) {
        // $('#esAmount').on('show.bs.modal', centerModals);
        // $('#esSale').on('show.bs.modal', centerModals);
        //页面大小变化是仍然保证模态框水平垂直居中
        $(window).on('resize', centerModals);
        //定性
        switch (s.cusDetermine) {
            case "有价值":
                $("#three_val1").css('color', '#000');
                $("#three_val2").css('color', '#7F8C8D');
                $("#three_val3").css('color', '#7F8C8D');
                $("#three_val").val("有价值");
                break;
            case "无价值":
                $("#three_val2").css('color', '#000');
                $("#three_val1").css('color', '#7F8C8D');
                $("#three_val3").css('color', '#7F8C8D');
                $("#three_val").val("无价值");
                break;
            case "不确定":
                $("#three_val3").css('color', '#000');
                $("#three_val1").css('color', '#7F8C8D');
                $("#three_val2").css('color', '#7F8C8D');
                $("#three_val").val("不确定");
                break;
            default:
                break;
        }
        //定级
        switch (s.cusClass) {
            case "大单":
                $("#three_level1").css('color', '#000');
                $("#three_level2").css('color', '#7F8C8D');
                $("#three_level3").css('color', '#7F8C8D');
                $("#three_level").val("大单");
                break;
            case "正常":
                $("#three_level2").css('color', '#000');
                $("#three_level1").css('color', '#7F8C8D');
                $("#three_level3").css('color', '#7F8C8D');
                $("#three_level").val("正常");
                break;
            case "小单":
                $("#three_level3").css('color', '#000');
                $("#three_level2").css('color', '#7F8C8D');
                $("#three_level1").css('color', '#7F8C8D');
                $("#three_level").val("小单");
                break;
            default:
                break;
        }
            switch (s.cusSignDate) {
                case "预计本周":
                    $("#three_qua1").css('color', '#000');
                    $("#three_qua2").css('color', '#7F8C8D');
                    $("#three_qua3").css('color', '#7F8C8D');
                    $("#three_qua4").css('color', '#7F8C8D');
                    var qua = $("#three_qua").val();
                    $("#three_qua").val("本周" + qua);
                    break;
                case "预计下周":
                    $("#three_qua2").css('color', '#000');
                    $("#three_qua1").css('color', '#7F8C8D');
                    $("#three_qua3").css('color', '#7F8C8D');
                    $("#three_qua4").css('color', '#7F8C8D');
                    var qua = $("#three_qua").val();
                    $("#three_qua").val("下周" + qua);
                    break;
                case "预计本月":
                    $("#three_qua3").css('color', '#000');
                    $("#three_qua2").css('color', '#7F8C8D');
                    $("#three_qua1").css('color', '#7F8C8D');
                    $("#three_qua4").css('color', '#7F8C8D');
                    var qua = $("#three_qua").val();
                    $("#three_qua").val("本月" + qua);
                    break;
                case "预计下月":
                    $("#three_qua4").css('color', '#000');
                    $("#three_qua2").css('color', '#7F8C8D');
                    $("#three_qua3").css('color', '#7F8C8D');
                    $("#three_qua1").css('color', '#7F8C8D');
                    var qua = $("#three_qua").val();
                    $("#three_qua").val("下月" + qua);
                    break;
                default:
                    break;
            }
        }

	});


$.ajax({
    url:'three/get_by_cusId',
    async : false,//同步，会阻塞操作
    type : 'POST',//PUT DELETE POST
    data : {
        id:value
    },
    contentType : 'application/x-www-form-urlencoded; charset=utf-8',
    dataType : 'json',
    success : function(s) {
        var str1 = "万";
        var str2 = "千";
        var str3 = "平米";

		if(s.threeContent.indexOf(str2) >= 0){
		    var acount1 = s.threeContent.substring(21,s.threeContent.indexOf(str2)+1)
            $('#spa_esAmount').css("display","none");
		    $('#inp_esAmount').text(acount1);
		}else if(s.threeContent.indexOf(str1) >= 0){
            var acount2 = s.threeContent.substring(21,s.threeContent.indexOf(str1)+1)
            $('#spa_esAmount').css("display","none");
            $('#inp_esAmount').text(acount2);
		}
		if(s.threeContent.indexOf(str3) >= 0 && s.threeContent.indexOf(str2) >= 0){
		    var size = s.threeContent.substring(s.threeContent.indexOf(str2)+2,s.threeContent.indexOf(str3)+2)
            $('#spa_esSale').css("display","none");
		    $('#inp_esSale').text(size);
		}else if(s.threeContent.indexOf(str3) >= 0 && s.threeContent.indexOf(str1) >= 0){
            var size = s.threeContent.substring(s.threeContent.indexOf(str1)+2,s.threeContent.indexOf(str3)+2)
            $('#spa_esSale').css("display","none");
            $('#inp_esSale').text(size);
		}


        $('#esAmount').on('show.bs.modal', centerModals);
        $('#esSale').on('show.bs.modal', centerModals);


        }
});

	function centerModals() {
		　　$('#esAmount').each(function(i) {
		　　　　var $clone = $(this).clone().css('display','block').appendTo('body');
		　　var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
		　　top = top > 0 ? top : 0;
		　　$clone.remove();
		　　$(this).find('.modal-content').css("margin-top", top);
	});
		　　$('#esSale').each(function(i) {
		　　　　var $clone = $(this).clone().css('display','block').appendTo('body');
		　　var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
		　　top = top > 0 ? top : 0;
		　　$clone.remove();
		　　$(this).find('.modal-content').css("margin-top", top);
	});
		};
/*  //模态框上弹出模态框方法  解决模态框背景色越来越深的问题
	    $(document).on('show.bs.modal', '.modal', function(event) {
	        $(this).appendTo($('body'));
	    }).on('shown.bs.modal', '.modal.in', function(event) {
	        setModalsAndBackdropsOrder();
	    }).on('hidden.bs.modal', '.modal', function(event) {
	        setModalsAndBackdropsOrder();
	    });

	    function setModalsAndBackdropsOrder() {
	        var modalZIndex = 1040;
	        $('.modal.in').each(function(index) {
	            var $modal = $(this);
	            modalZIndex++;
	            $modal.css('zIndex', modalZIndex);
	            $modal.next('.modal-backdrop.in').addClass('hidden').css('zIndex', modalZIndex - 1);
	        });
	        $('.modal.in:visible:last').focus().next('.modal-backdrop.in').removeClass('hidden');
	    }

	    //覆盖Modal.prototype的hideModal方法
	    $.fn.modal.Constructor.prototype.hideModal = function () {
	        var that = this
	        this.$element.hide()
	        this.backdrop(function () {
	            //判断当前页面所有的模态框都已经隐藏了之后body移除.modal-open，即body出现滚动条。
	            $('.modal.fade.in').length === 0 && that.$body.removeClass('modal-open')
	            that.resetAdjustments()
	            that.resetScrollbar()
	            that.$element.trigger('hidden.bs.modal')
	        });
	    }*/
</script>
<script src="/js/resjs/threeOne.js"></script>



</html>