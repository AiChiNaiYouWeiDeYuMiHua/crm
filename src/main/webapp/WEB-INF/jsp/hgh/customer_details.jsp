<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/11
  Time: 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>客户视图</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="css/city-picker.css"/>
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="/css/bootstrap-spinner.min.css">
    <link href="/css/hgh/cus_view.css" rel="stylesheet">
    <link href="/css/hgh/sale_start_modal.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <style>
        .title{
            margin-top: -17px;
        }
    </style>
</head>

<body>
<div class="container-fluid">
    <div class="profile-info-name" style="height: 20px;">
        <ul class="pull-left list-inline b">
            <li class="tab active">
                <a href="#">
                    客户视图
                </a>
            </li>
            <li class="tab ">
                <a href="#">
                    供应商视图
                </a>
            </li>

        </ul>
        <a class="pull-left" href="#" target="_blank" data-placement="bottom" data-toggle="tooltip"
           data-original-title="供应商视图查看权限"><i class="fa fa-male"></i></a>
        <div class="pull-right">
            <a id="other1" onclick="" style="text-decoration: none;" href="#">ID:<b
                    class="ng-binding">${cusInfo.cusId}</b></a>
            <span id="div2">
					<a class="text-a" style="text-decoration: none; margin-left: 10px;" href="#" target="_BLANK"
                       data-toggle="tooltip" data-placement="bottom" title="" data-original-title="数据日志"><i
                            class="fa fa-bars m-l-5"></i></a>
					<a class="text-a" style="text-decoration: none;margin-left: 10px;" href="#" ng-click="chown()"><i
                            class="fa fa-random m-r-5"></i>转移</a>
					<a class="text-a" style="text-decoration: none;margin-left: 10px;cursor: pointer;"
                       onclick="detail_delete(${cusInfo.cusId})"
                    ><i class="fa fa-trash m-r-5"></i>删除</a>
				</span>
            <a class="text-a" style="text-decoration: none;margin-left: 10px;cursor: pointer;"
               onclick="modify(${cusInfo.cusId})"><i
                    class="fa fa-pencil m-r-5"></i>编辑</a>
        </div>
    </div>
    <h3 class="p-t-40  text-center" style="padding-top: 40px;">${cusInfo.cusName}&nbsp;<sup
            class="text-gray btn-danger btn-xs" style="font-size: 5px;margin-top:-10px ;">${cusInfo.cusLifecycle}</sup>
    </h3>

    <p class="text-muted  text-center ng-binding">更新：今天， 创建：${cusInfo.cusCreateDate} 孙杨
        <a style="text-decoration: none;margin-right: 5px;" href="https://www.baidu.com/s?wd=${cusInfo.cusName}"
           target="_blank"><img src="http://cdn.xtcrm.com/cdn//v7/img/baidu.png" width="12">百度查</a>
        <a style="text-decoration: none;"
           href="https://www.tianyancha.com/search?key=${cusInfo.cusName}&amp;from=xtools&amp;token=b784af7c5600bda82c6b8c8144dc3e92"
           target="_blank"><img src="http://cdn.xtcrm.com/cdn//v7/img/tyc.png" width="12"> 天眼查</a>
        <a href="" ng-click="opentyc()"><img src="http://cdn.xtcrm.com/cdn//v7/img/tyc.png" width="12"> 工商信息</a>
        <a href="" id="kmbopencus" ng-click="kmbopencus()"><img src="http://cdn.xtcrm.com/cdn//v7/img/kmb.png"
                                                                width="12"> 快目标同屏</a>
    </p>

    <div class="row">
        <div class="col-sm-8 pull-right" style="margin-top: 20px;">
            <div class="card-box2 " style="background-color: white;font-size: 12px;">
                <div class="card-box-head">
                    <i class="fa fa-rub" style="margin-right: 5px;"></i><span set-lan="html:跟单时间线">跟单时间线</span>
                </div>
                <div>
                    <div class="widget-inline" id="cus_detail">
                        <!-- ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope active" repeat-finish="" ng-repeat="da in d.tmc">
                            <a style="text-decoration: none; cursor: pointer">
                                <h5><i class="fa fa-tasks"></i> <b class="ng-binding" id="all"></b></h5>
                                <p style="color: grey;">全部</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="" ng-repeat="da in d.tmc">
                            <a style="text-decoration: none;cursor: pointer">
                                <h5><i class="fa fa-file-text"></i> <b class="ng-binding" id="dingdan"></b></h5>
                                <script>
                                    var a=0;
                                    <c:forEach items="${showAll}" var="i">
                                    <c:forEach items="${i.value}" var="j">
                                    <c:choose>
                                    <c:when test="${fn:substring(j,0,j.indexOf('ｂ')) == '订单'}">
                                    a= a+1;
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:forEach>
                                    if(a!=0){
                                        $('#dingdan').text(a);
                                    }else{
                                        $('#dingdan').text(a);
                                        $('#dingdan').parent().parent().parent().css("display","none");
                                    }
                                </script>
                                <p style="color: grey;">订单</p>
                            </a>
                        </div>
                        <%--<!-- end ngRepeat: da in d.tmc -->--%>
                        <%--<div class="widget-inline-box ng-scope" repeat-finish="" ng-repeat="da in d.tmc">--%>
                            <%--<a style="text-decoration: none;cursor: pointer">--%>
                                <%--<h5><i class="fa fa-cubes"></i> <b class="ng-binding">2</b></h5>--%>

                                <%--<p style="color: grey;">交付计划</p>--%>
                            <%--</a>--%>
                        <%--</div>--%>
                        <%--<!-- end ngRepeat: da in d.tmc -->--%>
                        <%--<div class="widget-inline-box ng-scope" repeat-finish="" ng-repeat="da in d.tmc">--%>
                            <%--<a style="text-decoration: none;cursor: pointer">--%>
                                <%--<h5><i class="fa fa-clipboard"></i> <b class="ng-binding">2</b></h5>--%>
                                <%--<p style="color: grey;">交付记录</p>--%>
                            <%--</a>--%>
                        <%--</div>--%>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="" ng-repeat="da in d.tmc">
                            <a style="text-decoration: none;cursor: pointer">
                                <h5><i class="fa fa-clipboard"></i> <b class="ng-binding" id="sale"></b></h5>
                                <script>
                                    var a=0;
                                    <c:forEach items="${showAll}" var="i">
                                    <c:forEach items="${i.value}" var="j">
                                    <c:choose>
                                    <c:when test="${fn:substring(j,0,j.indexOf('ｂ')) == '销售机会'}">
                                    a= a+1;
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:forEach>
                                    if(a!=0){
                                        $('#sale').text(a);
                                    }else{
                                        $('#sale').text(a);
                                        $('#sale').parent().parent().parent().css("display","none");
                                    }
                                </script>
                                <p style="color: grey;">销售机会</p>
                            </a>
                        </div>
                        <%--<!-- end ngRepeat: da in d.tmc -->--%>
                        <%--<div class="widget-inline-box ng-scope" repeat-finish="">--%>
                            <%--<a style="text-decoration: none;cursor: pointer">--%>
                                <%--<h5><i class="fa fa-truck"></i> <b class="ng-binding">2</b></h5>--%>
                                <%--<p style="color: grey;">发货单</p>--%>
                            <%--</a>--%>
                        <%--</div>--%>
                        <%--<!-- end ngRepeat: da in d.tmc -->--%>
                        <%--<div class="widget-inline-box ng-scope" repeat-finish="">--%>
                            <%--<a style="text-decoration: none;cursor: pointer">--%>
                                <%--<h5><i class="fa fa-yen"></i> <b class="ng-binding">2</b></h5>--%>
                                <%--<p style="color: grey;">回款</p>--%>
                            <%--</a>--%>
                        <%--</div>--%>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a style="text-decoration: none;cursor: pointer">
                                <h5><i class="fa fa-yen"></i> <b class="ng-binding" id="threeCount">2</b></h5>
                                <script>
                                    var a=0;
                                    <c:forEach items="${showAll}" var="i">
                                    <c:forEach items="${i.value}" var="j">
                                    <c:choose>
                                    <c:when test="${fn:substring(j,0,j.indexOf('ｂ')) == '三一客'}">
                                    a= a+1;
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:forEach>
                                    if(a!=0){
                                        $('#threeCount').text(a);
                                    }else{
                                        $('#threeCount').text(a);
                                        $('#threeCount').parent().parent().parent().css("display","none");
                                    }
                                </script>
                                <p style="color: grey;">三一客</p>
                            </a>
                        </div>
                        <%--<!-- end ngRepeat: da in d.tmc -->--%>
                        <%--<div class="widget-inline-box ng-scope" repeat-finish="">--%>
                            <%--<a style="text-decoration: none;cursor: pointer">--%>
                                <%--<h5><i class="fa fa-money"></i> <b class="ng-binding">2</b></h5>--%>
                                <%--<p style="color: grey;">计划回款</p>--%>
                            <%--</a>--%>
                        <%--</div>--%>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a style="text-decoration: none;cursor: pointer">
                                <h5><i class="fa fa-money"></i> <b class="ng-binding" id="need"></b></h5>
                                <script>
                                    var a=0;
                                    <c:forEach items="${showAll}" var="i">
                                    <c:forEach items="${i.value}" var="j">
                                    <c:choose>
                                    <c:when test="${fn:substring(j,0, j.indexOf('ｂ'))=='销售机会'}">
                                    <c:forEach items="${fn:split(j, 'ｃ')}" var="h">
                                    <c:choose>
                                    <c:when test="${fn:substring(h,0, h.indexOf('ｂ'))=='详细需求'}">
                                    a=a+1;
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:forEach>
                                    if(a!=0){
                                        $('#need').text(a);
                                    }else{
                                        $('#need').text(a);
                                        $('#need').parent().parent().parent().css("display","none");
                                    }
                                </script>
                                <p style="color: grey;">详细需求</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a style="text-decoration: none;cursor: pointer">
                                <h5><i class="fa fa-money"></i> <b class="ng-binding" id="solu"></b></h5>
                                <script>
                                    var a=0;
                                    <c:forEach items="${showAll}" var="i">
                                    <c:forEach items="${i.value}" var="j">
                                    <c:choose>
                                    <c:when test="${fn:substring(j,0, j.indexOf('ｂ'))=='销售机会'}">
                                    <c:forEach items="${fn:split(j, 'ｃ')}" var="h">
                                    <c:choose>
                                    <c:when test="${fn:substring(h,0, h.indexOf('ｂ'))=='解决方案'}">
                                    a=a+1;
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:forEach>
                                    if(a!=0){
                                        $('#solu').text(a);
                                    }else{
                                        $('#solu').text(a);
                                        $('#solu').parent().parent().parent().css("display","none");
                                    }
                                </script>
                                <p style="color: grey;">解决方案</p>
                            </a>
                        </div>
                        <!-- end ngRepeat: da in d.tmc -->
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a style="text-decoration: none;cursor: pointer">
                                <h5><i class="fa fa-money"></i> <b class="ng-binding" id="bj"></b></h5>
                                <script>
                                    var a=0;
                                    <c:forEach items="${showAll}" var="i">
                                    <c:forEach items="${i.value}" var="j">
                                    <c:choose>
                                    <c:when test="${fn:substring(j,0, j.indexOf('ｂ'))=='销售机会'}">
                                    <c:forEach items="${fn:split(j, 'ｃ')}" var="h">
                                    <c:choose>
                                    <c:when test="${fn:substring(h,0, h.indexOf('ｂ'))=='报价'}">
                                    a=a+1;
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:forEach>
                                    if(a!=0){
                                        $('#bj').text(a);
                                    }else{
                                        $('#bj').text(a);
                                        $('#bj').parent().parent().parent().css("display","none");
                                    }
                                </script>
                                <p style="color: grey;">报价</p>
                            </a>
                        </div>
                        <div class="widget-inline-box ng-scope" repeat-finish="">
                            <a style="text-decoration: none;cursor: pointer">
                                <h5><i class="fa fa-yen"></i> <b class="ng-binding" id="med">2</b></h5>
                                <script>
                                    var a=0;
                                    <c:forEach items="${showAll}" var="i">
                                    <c:forEach items="${i.value}" var="j">
                                    <c:choose>
                                    <c:when test="${fn:substring(j,0,j.indexOf('ｂ')) == '纪念日'}">
                                    a= a+1;
                                    </c:when>
                                    </c:choose>
                                    </c:forEach>
                                    </c:forEach>
                                    if(a!=0){
                                        $('#med').text(a);
                                    }else{
                                        $('#med').text(a);
                                        $('#med').parent().parent().parent().css("display","none");
                                    }
                                </script>
                                <p style="color: grey;">纪念日</p>
                            </a>
                        </div>
                        <%--<!-- end ngRepeat: da in d.tmc -->--%>
                        <%--<div class="widget-inline-box ng-scope" repeat-finish="" ng-repeat="da in d.tmc">--%>
                            <%--<a style="text-decoration: none;cursor: pointer">--%>
                                <%--<h5><i class="fa fa-clipboard"></i> <b class="ng-binding">2</b></h5>--%>
                                <%--<p style="color: grey;">维修工单</p>--%>
                            <%--</a>--%>
                        <%--</div>--%>
                        <%--<!-- end ngRepeat: da in d.tmc -->--%>
                        <%--<div class="widget-inline-box ng-scope" repeat-finish="">--%>
                            <%--<a style="text-decoration: none;cursor: pointer">--%>
                                <%--<h5><i class="fa fa-wpforms"></i> <b class="ng-binding">2</b></h5>--%>
                                <%--<p style="color: grey;">开票</p>--%>
                            <%--</a>--%>
                        <%--</div>--%>
                    </div>
                </div>
                <div>
                    <div style="padding: 20px;">
                        <div class="timeline-2" style="margin-left: 10px;" id="timeline">
                            <!-- ngRepeat: da in d.tm -->
                            <c:forEach items="${showAll}" var="i">
                            <div class="time-item ng-scope" style="margin-top: 30px;">
                                <div class="item-info">
                                    <p class="time"><i class="fa fa-calendar-times-o" style="margin-right: 5px;"></i>${i.key}</p>
                                <c:forEach items="${i.value}" var="j">
                                    <div class="display-table " repeat-finish="">
                                        <div class="col-sm-3">
                                            <p>
                                                <i class="fa fa-star"></i>
                                                <span class="title">${fn:substring(j,0,j.indexOf("ｂ"))}</span>
                                            </p>
                                        </div>
                                        <div class="col-sm-9 ingo1">
                                            <p class="ingo">
                                                        <c:choose>
                                                            <%--封装--%>
                                                        <c:when test="${fn:substring(j,0, j.indexOf('ｂ'))=='销售机会'}">
                                                            <script>
                                                                $("#timeline").find($(".title")).each(function () {
                                                                    if ($(this).text() == "销售机会") {
                                                                        $(this).parent().parent().parent().find($('.ingo1')).css("padding-top","10px");
                                                                        $(this).parent().parent().parent().find($('.ingo1')).css("padding-botton","20px");
                                                                        $(this).parent().parent().parent().find($('.ingo1')).css("margin-top","-10px");
                                                                        $(this).parent().parent().parent().find($('.ingo1')).css("margin-left","-10px");
                                                                        $(this).parent().parent().parent().find($('.ingo')).css("margin-left","10px");
                                                                        $(this).parent().parent().parent().find($('.ingo1')).css("background-color", "#f4f8fb")
                                                                    }
                                                                })
                                                            </script>
                                                            <c:forEach items="${fn:split(j, 'ｃ')}" var="h">
                                                                <c:choose>
                                                                    <c:when test="${fn:substring(h,0, h.indexOf('ｂ'))=='销售机会'}">
                                                                        <b><a href="/sale/opp/info/${fn:substring(h,h.indexOf("ｉｄ")+2,h.indexOf("ｃ"))}/" style="text-decoration: none;"><span class="content">${fn:substring(h,h.indexOf("ｂ")+1,h.indexOf("ｉｄ"))}</span></a></b><br>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <c:choose>
                                                                            <c:when test="${fn:substring(h,0,h.indexOf('ｂ')) == '报价'}">
                                                                                <span class="inner" style="color:gray;font-size: 14px"><i class="fa fa-star"></i>&nbsp;${fn:substring(h,0,h.indexOf("ｂ"))}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><a href="/quote/info/${fn:substring(h,h.indexOf("ｉｄ")+2,h.indexOf("ｃ"))}/" style="text-decoration: none;"><span class="contentinner">${fn:substring(h,h.indexOf("ｂ")+1,h.indexOf("ｉｄ"))}</span></a></b><br>
                                                                            </c:when>
                                                                            <c:when test="${fn:substring(h,0,h.indexOf('ｂ')) == '解决方案'}">
                                                                                <span class="inner" style="color:gray;font-size: 14px"><i class="fa fa-star"></i>&nbsp;${fn:substring(h,0,h.indexOf("ｂ"))}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><a href="#" style="text-decoration: none;"><span class="contentinner">${fn:substring(h,h.indexOf("ｂ")+1,h.indexOf("ｉｄ"))}</span></a></b><br>
                                                                            </c:when>
                                                                            <c:when test="${fn:substring(h,0,h.indexOf('ｂ')) == '详细需求'}">
                                                                                <span class="inner" style="color:gray;font-size: 14px"><i class="fa fa-star"></i>&nbsp;${fn:substring(h,0,h.indexOf("ｂ"))}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><a href="#" style="text-decoration: none;"><span class="contentinner">${fn:substring(h,h.indexOf("ｂ")+1,h.indexOf("ｉｄ"))}</span></a></b><br>
                                                                            </c:when>
                                                                        </c:choose>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </c:when>
                                                            <%--结束--%>
                                                            <c:when test="${fn:substring(j,0, j.indexOf('ｂ'))=='三一客'}">
                                                                <b><a data-toggle="modal" data-target="#threeOne" href='sanyike_modal?id=${cusInfo.cusId}' onclick="gaba()" style="text-decoration: none;"><span class="content">${fn:substring(j,j.indexOf("ｂ")+1,j.indexOf("ａ"))}</span></a></b>
                                                            </c:when>
                                                            <c:when test="${fn:substring(j,0, j.indexOf('ｂ'))=='纪念日'}">
                                                                <b><a data-toggle="modal" data-target="#threeOne" href='#' onclick="gaba()" style="text-decoration: none;"><span class="content">${fn:substring(j,j.indexOf("ｂ")+1,j.indexOf("ａ"))}</span></a></b>
                                                            </c:when>
                                                        </c:choose>
                                                </br>
                                            </p>
                                            <!-- ngRepeat: sfl in fl._dd -->
                                        </div>
                                    </div>
                                </c:forEach>
                                </div>
                            </div>
                            </c:forEach>

                            <!-- end ngRepeat: da in d.tm -->
                            <div class="time-item ng-scope" id="timestrip">
                                <div class="item-info"　style="margin-top:60px;">
                                    <p class="time1"><i class="fa fa-circle" style="margin-right: 5px;"></i></p>
                                    <!-- ngRepeat: fl in da.ds track by $index -->
                                    <div class="display-table " repeat-finish="">
                                        <div class="col-sm-3">
                                            <p>
                                                <span class="title1"></span>
                                            </p>
                                        </div>
                                        <div>
                                            <p>
                                                <b>
                                                </b>
                                            </p>
                                            <!-- ngRepeat: sfl in fl._dd -->
                                        </div>
                                    </div>
                                    <!-- end ngRepeat: fl in da.ds track by $index -->
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>


        <!--左边区块-->
        <div class="row col-sm-4" style="margin-top: 20px;padding: 0;">
            <div class="card-box2  col-sm-12" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <div class="pull-right">
                            ${cusInfo.cusLifecycle}
                        </div>
                        <p style="margin-bottom: 10px;"><i class="fa fa-star" style="margin-right: 5px;"></i>三定一节点</p>
                    </div>
                    <div class="" id="31k_body">
                        <div class="table-box font-weg text-center">
                            <div class="table-detail a text-black  col-sm-4" data-toggle="modal" data-target="#threeOne" href="sanyike_modal?id=${cusInfo.cusId}" onclick="gaba()">
                                <div class="iconbox  bg-gray m-0" style="margin: 0;padding-top: 13px;">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <c:choose>
                                    <c:when test="${cusInfo.cusDetermine != null}">
                                        <p class=" m-b-0 m-t-5"
                                           style="margin-bottom: 0;margin-top: 5px;">${cusInfo.cusDetermine}</p>
                                    </c:when>
                                    <c:otherwise>
                                        <p class=" m-b-0 m-t-5"
                                           style="margin-bottom: 0;margin-top: 5px;">定性</p>
                                    </c:otherwise>
                                </c:choose>

                            </div>
                            <div class="table-detail a text-black  col-sm-4" data-toggle="modal" data-target="#threeOne" href="sanyike_modal?id=${cusInfo.cusId}" onclick="gaba()">
                                <div class="iconbox  bg-gray" style="margin: 0;">
                                    <i class="fa fa-heart" style="margin: 0px;padding-top: 18px;"></i>
                                </div>
                                <c:choose>
                                    <c:when test="${cusInfo.cusClass != null}">
                                        <p class="text-center" style="margin-bottom: 0;margin-top: 5px;">${cusInfo.cusClass}</p>
                                    </c:when>
                                    <c:otherwise>
                                        <p class=" m-b-0 m-t-5"
                                           style="margin-bottom: 0;margin-top: 5px;">定级</p>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="table-detail a text-black  col-sm-4" data-toggle="modal" data-target="#threeOne" href="sanyike_modal?id=${cusInfo.cusId}" onclick="gaba()">
                                <div class="iconbox bg-gray" style="margin: 0;">
                                    <i class="fa fa-hashtag" style="margin: 0px;padding-top: 18px;"></i>
                                </div>
                                <c:choose>
                                    <c:when test="${cusInfo.cusSignDate != null && threeInfo.threeContent == null}">

                                        <p class="text-center" style="margin-bottom: 0;margin-top: 5px;">${cusInfo.cusSignDate}
                                        </p>
                                    </c:when>
                                    <c:when test="${threeInfo.threeContent != null}">
                                        <p class="text-center" id="cusSale" style="margin-bottom: 0;margin-top: 5px;">
                                        </p>
                                    </c:when>
                                    <c:otherwise>
                                        <p class=" m-b-0 m-t-5"
                                           style="margin-bottom: 0;margin-top: 5px;">定量</p>
                                    </c:otherwise>
                                </c:choose>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <p class="" style="margin-bottom: 10px;"><i class="fa fa-xing m-r-5"></i>销售机会：${saleInfo.oppTheme}</p>
                    </div>
                    <div class="table-box text-center ">
                        <div class="col-sm-4">
                            <div class="iconbox bg-gray" style="margin: 0;">
                            <c:choose>
                            <c:when test="${saleInfo.possibility != null}">
                                <h5 class="" style="margin-top: 15px;">${saleInfo.possibility}</h5>
                            </c:when>
                                <c:otherwise>
                                    <h5 class="" style="margin-top: 15px;">未定义</h5>
                                </c:otherwise>
                            </c:choose>
                            </div>
                            <p style="margin-top: 5px;">可能性</p>
                        </div>
                        <div class="col-sm-4" style="margin-top: 10px;">
                            <c:choose>
                                <c:when test="${saleInfo.expectedTime != null}">
                                    <h5>${fn:substring(saleInfo.expectedTime,5,10)}</h5>
                                </c:when>
                                <c:otherwise>
                                    <h5>未定义</h5>
                                </c:otherwise>
                            </c:choose>
                            <p class="font-weg">预估日期</p>

                        </div>
                        <div class="col-sm-4" style="margin-top: 10px;">
                            <c:choose>
                                <c:when test="${saleInfo.expectedAmount != null}">
                                    <h5>￥${saleInfo.expectedAmount}</h5>
                                </c:when>
                                <c:otherwise>
                                    <h5>未定义</h5>
                                </c:otherwise>
                            </c:choose>
                            <p class="font-weg">预估金额</p>

                        </div>
                    </div>
                    <div class="pull-right" style="position: absolute;top: 125px;left: 190px;">
                        <p>阶段：${cusInfo.cusStage},停留251天</p>
                    </div>
                </div>
            </div>
            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <p class=" m-b-10" style="margin-bottom: 10px;"><i class="fa fa-hand-lizard-o m-r-5"></i>应收/预付
                        </p>
                    </div>
                    <div class="bar-widget">
                        <div class="table-box text-center">
                            <div class="text-center col-sm-4">
                                <h5>￥&nbsp;0.00</h5>
                                <p style="margin-left: 5px;">计划回款</p>
                            </div>

                            <div class="text-center col-sm-4">
                                <h5 class="ng-binding">￥&nbsp;6,999.00</h5>
                                <p style="margin-left: 5px;">智能应收</p>
                            </div>
                            <div class="text-center col-sm-4">
                                <h5>￥&nbsp;0.00</h5>
                                <p style="margin-left: 5px;">预付余额</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <p class=" m-b-10" style="margin-bottom: 10px;"><i class="fa fa-object-group m-r-5"
                                                                           style="margin-right: 5px;"></i>签约数据</p>
                    </div>
                    <div class="table-box text-center font-weg">
                        <div class="table-detail col-sm-3">
                            <h5><a href="" data-toggle="tooltip" data-placement="bottom" title=""
                                   data-original-title="9999.00" class="ng-binding">9千9</a></h5> 合约
                        </div>
                        <div class="table-detail col-sm-3">
                            <h5><a href="" data-toggle="tooltip" data-placement="bottom" title=""
                                   data-original-title="9999.00" class="ng-binding">9千9</a></h5> 发货
                        </div>
                        <div class="table-detail col-sm-3">
                            <h5><a href="" data-toggle="tooltip" data-placement="bottom" title=""
                                   data-original-title="9999.00">9千9</a></h5> 回款
                        </div>
                        <div class="table-detail col-sm-3">
                            <h5><a href="" target="_blank"><i
                                    class="fa fa-list-ol fa-x"></i></a></h5> 往来对账
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <p><i class="fa fa-info" style="margin-right: 5px;"></i><span>基本信息</span></p>
                    </div>
                    <div class="form-inline font-weg">
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>助记简称</span>
                            <label>${cusInfo.cusAbbreviation}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>种类</span>
                            <label>${cusInfo.cusType}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>编号</span>
                            <label>${cusInfo.cusId}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>行业</span>
                            <label>${cusInfo.cusIndustry}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>人员规模</span>
                            <label>${cusInfo.cusExistDate}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>来源</span>
                            <label>${cusInfo.cusCome}</label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group m-r-10 m-b-5" style="margin-top: 10px;">
                            <span>公司简介</span>
                            <span>${cusInfo.cusSynopsis}</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>首次签约</span>
                            <label>${cusInfo.cusDate}</label>
                        </div>
                        <div class="form-group m-r-10 " style="margin-top: 10px;">
                            <span>关系等级</span>
                            <label>${cusInfo.cusLevel}</label>
                        </div>
                        <div class="form-group m-r-10" style="margin-top: 10px;">
                            <span>阶段</span>
                            <label>${cusInfo.cusStage}</label>
                        </div>
                        <div class="form-group m-r-10 " style="margin-top: 10px;">
                            <span>信用等级</span>
                            <label>${cusInfo.cusCredit}</label>
                        </div>
                        <br>
                        <div class="form-group m-r-10 " style="margin-top: 10px;">
                            <span>备注</span>
                            <label>${cusInfo.cusRemark}</label>
                        </div>
                    </div>
                    <div class="card-box-head">
                        <br>
                        <img src="http://cdn.xtcrm.com/cdn//v7/img/tyc2.png" width="12" class="m-r-5"
                             style="padding-bottom: 3px;">工商信息(由天眼查提供)
                        <span><br></span>
                    </div>
                </div>
            </div>
            <div class="card-box2 col-sm-12 pull-left" style="background-color: white;font-size: 12px;">
                <div class="">
                    <div class="card-box-head">
                        <div class="contact-action display-all pull-right">
                            <div class="radio radio-inline">
                                <input type="radio" id="inlineRadio1" value="option1" name="radioInline" checked=""
                                       onclick="$('#contact_show1').show(200);$('#contact_show2').hide();" ;="">
                                <label for="inlineRadio1"><i class="fa fa-users"></i></label>
                            </div>
                            <div class="radio radio-inline">
                                <input type="radio" id="inlineRadio2" value="option1" name="radioInline"
                                       onclick="$('#contact_show2').show(200);$('#contact_show1').hide();" ;="">
                                <label for="inlineRadio2"><i class="fa fa-sitemap"></i></label>
                            </div>
                        </div>
                        <p><i class="md md-quick-contacts-dialer m-r-5"></i><span set-lan="html:联系人">联系人</span></p>
                    </div>

                    <div class="contact-card" id="contact_show1" style="display: block;">
                    <c:if test="${fn:length(cotsInfo) != 0}">
                        <%--<script>alert(${fn:length(cotsInfo)})</script>--%>
                        <%--&lt;%&ndash;<c:forEach items="cotsInfo" var="i">&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<script>${i}</script>&ndash;%&gt;--%>
                        <%--&lt;%&ndash;</c:forEach>&ndash;%&gt;--%>

                    <%--</c:if>--%>
                        <c:forEach items="${cotsInfo}" var="i">
                            <c:choose>
                            <c:when test="${i.cotsType eq '主联系人'}">
                        <a class="pull-left text-center" style="text-decoration: none"
                           href="">
                                <c:choose>
                                    <c:when test="${i.cotsSex eq '男'}">
                                        <i class="fa fa-male fa-3x"></i><br> ${i.cotsName}
                                    </c:when>
                                 <c:when test="${i.cotsSex eq '女'}">
                                        <i class="fa fa-female fa-3x"></i><br> ${i.cotsName}
                                    </c:when>
                                </c:choose>
                        </a>
                        <div class="member-info font-weg" style="padding-left: 100px;padding-bottom: 20px;">
                            <p>手机：<span><a title="发手机短信" style="text-decoration: none"
                                           href=''><b>${i.cotsPersonalPhone}</b><i
                                    class="glyphicon glyphicon-phonemobile m-l-5"></i></a></span></p>
                            <p>电话：<span>${i.cotsWorkphone}</span></p>
                            <p>邮箱：<span>${i.cotsPlace}</span></p>
                            <p> Q Q：<span>${i.cotsPlace}</span></p>
                            <p>微信：<span>${i.cotsMsnQq}</span></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="font-weg">
                            <p></p>
                        </div>
                            </c:when>
                            <c:when test="${i.cotsType eq '联系人'}">
                        <ul class="list-inline m-0 " style="padding-left: 0;margin-left: -5px;list-style: none;float: left">
                            <li class=" text-center">
                                <a data-placement="top" data-toggle="tooltip" class="tooltips" href=""
                                   data-original-title="${i.cotsPersonalPhone}" style="text-decoration: none">
                                    <i class="fa fa-male fa-2x"></i><br><font color="gray">${i.cotsName}</font></a>
                            </li>
                        </ul>
                            </c:when>
                            </c:choose>
                        </c:forEach>
                        </c:if>
                    </div>

                    <div class="contact-card" id="contact_show2" style="display: none;">
                        <ul id="treeDemo" class="tree" style="list-style-type: none;">
                            <li treenode="">
                        <c:if test="${fn:length(cotsInfo) != 0}">
                            <c:forEach items="${cotsInfo}" var="i">
                                <c:choose>
                                    <c:when test="${i.cotsType eq '主联系人'}">
                                <a href="" style="text-decoration: none" onclick="reportMove(this) ;"
                                               treenode_a=""><i
                                    class="fa fa-user" style="margin-right: 5px"></i><span>${i.cotsName}</span></a>
                                    </c:when>
                                    <c:when test="${i.cotsType eq '联系人'}">
                                <ul style="list-style-type: none;margin-top: 0">
                                    <li treenode=""><a href="" style="text-decoration: none"
                                                       onclick="reportMove(this) ;" treenode_a=""><i
                                            class="fa fa-user" style="margin-right: 5px"></i><span><font
                                            color="gray">${i.cotsName}</font></span></a>
                                        <ul style="display: none;" class="line"></ul>
                                    </li>
                                </ul>
                                    </c:when>
                                    </c:choose>
                                </c:forEach>
                            </c:if>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-box2" style="padding-top: 10px;padding-bottom: 10px">
                <b class="pull-right">￥&nbsp;0.00</b>
                <p style="margin: 0"><i class="fa fa-dollar" style="margin-right: 5px"></i><span>销售费用</span></p>
            </div>

            <div class="card-box2 " style="padding-top: 10px;font-size: 12px">
                <div class="card-box-head">
                    <p><i class="fa fa-map-marker m-r-5"></i><span set-lan="html:地址">地址</span></p>
                </div>
                <div class="form-inline font-weg">
                    <div class="form-group m-r-10" ng-class="{true: 'display-none ', false: ''}[c._v.country=='']">
                        <span set-lan="html:国家或地区">国家或地区</span>
                        <label class="ng-binding">CN China中国 亚洲</label>
                    </div>
                    <div class="form-group m-r-10" ng-class="{true: 'display-none ', false: ''}[c._v.tel=='']">
                        <span>电话</span>
                        <span>${cusInfo.cusPhone}</span>
                    </div>
                    <div class="form-group m-r-10 display-none"
                         ng-class="{true: 'display-none ', false: ''}[c._v.fax=='']">
                        <span>传真</span>
                        <label>${cusInfo.cusMSNQQ}</label>
                    </div>
                    <div class="form-group m-r-10 display-none"
                         ng-class="{true: 'display-none ', false: ''}[c._v.state=='']">
                        <span set-lan="html:省份">省/市/区</span>
                        <label id="prov">${cusInfo.cusAddr}</label>
                    </div>
                    <div class="form-group m-r-10" ng-class="{true: 'display-none ', false: ''}[c._d.address=='']">
                        <span>地址</span>
                        <label><span>${cusInfo.cusArea}</span></label>
                    </div>
                    <br>
                    <div class="form-group m-r-10" ng-class="{true: 'display-none ', false: ''}[c._d.web=='http://']">
                        <span set-lan="html:网址">网址</span>
                        <label>
                            <span ng-bind-html="c._v.web|html" class="ng-binding">
                                <a href="http://${cusInfo.cusNet}" target="_blank">
                                    <b>http://${cusInfo.cusNet}</b>
                                </a>
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="modal fade" id="addcus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
         aria-labelledby="open"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content mymodalcontent">

            </div>
        </div>
    </div>
    <div class="modal fade" id="threeOne" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 460px;">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript " src="bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>
    <script type="text/javascript "
            src="bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="../../js/resjs/bootstrap-table.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
    <script src="js/resjs/tableExport.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
    <script src="../../js/resjs/bootstrap-tooltip.js"></script>
    <script src="../../js/resjs/bootstrap-popover.js"></script>
    <!--excel-->
    <script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>
    <script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
    <script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
    <script src="/js/resjs/customer_details.js"></script>
    <script src="/js/resjs/jquery.spinner.min.js"></script>
    <script src="js/resjs/printThis.js"></script>
    <script src="js/resjs/table.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script>
        $(function () {
            $("#example").popover();
            $("[data-toggle='tooltip']").tooltip();
            var three = "${threeInfo.threeContent}";
            var asle = three.substring(16,three.length);
            $('#cusSale').text(asle);
            $('#all').text(eval("("+$('#med').text()+")")+eval("("+$('#sale').text()+")")+eval("("+$('#threeCount').text()+")")+eval("("+$('#need').text()+")")+eval("("+$('#solu').text()+")")+eval("("+$('#bj').text()+")"));
        })

        function detail_delete(cusId) {
            swal({
                    title: "确定删除吗？",
                    text: "你将无法恢复该客户！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "确定删除！",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        url: 'customer/delete_customer',
                        method: 'post',
                        async: true,
                        data: {'id': cusId},
                        success: function (data) {
                            alert('我成功了？' + data.msg);
                            swal("成功！", "客户删除成功！", "success");
                            window.location.href = "customer_view";
                        },
                        error: function (data) {
                            alert("操作失败");
                            swal("失败！", "客户添加失败！" + data.msg, "Cancel")
                        }
                    });
                });
        }


        var open_modal;

        function modify(id) {
            $('#addcus').removeData("bs.modal");
            open_modal = id;
            $('#addcus').modal({
                remote: 'addcus_modal'
            });
        }

        $("#addcus").on("hidden.bs.modal", function () {
            window.location.reload();
        });

        function gaba() {
            $.ajax({
                url:'customer/load_cus',
                async : false,//同步，会阻塞操作
                type : 'POST',//PUT DELETE POST
                data : {
                    id:${cusInfo.cusId}
                },
                contentType : 'application/x-www-form-urlencoded; charset=utf-8',
                dataType : 'json',
                success : function(s) {
                    $('#esAmount').on('show.bs.modal', centerModals);
                    $('#esSale').on('show.bs.modal', centerModals);
                    //页面大小变化是仍然保证模态框水平垂直居中
                    $(window).on('resize', centerModals);
                    //定性
                    switch (s.cusDetermine) {
                        case "有价值":
                            $("#three_val1").css('color', '#000');
                            $("#three_val2").css('color', '#7F8C8D');
                            $("#three_val3").css('color', '#7F8C8D');
                            $("#three_val").val("有价值");
                            break;
                        case "无价值":
                            $("#three_val2").css('color', '#000');
                            $("#three_val1").css('color', '#7F8C8D');
                            $("#three_val3").css('color', '#7F8C8D');
                            $("#three_val").val("无价值");
                            break;
                        case "不确定":
                            $("#three_val3").css('color', '#000');
                            $("#three_val1").css('color', '#7F8C8D');
                            $("#three_val2").css('color', '#7F8C8D');
                            $("#three_val").val("不确定");
                            break;
                        default:
                            break;
                    }
                    //定级
                    switch (s.cusClass) {
                        case "大单":
                            $("#three_level1").css('color', '#000');
                            $("#three_level2").css('color', '#7F8C8D');
                            $("#three_level3").css('color', '#7F8C8D');
                            $("#three_level").val("大单");
                            break;
                        case "正常":
                            $("#three_level2").css('color', '#000');
                            $("#three_level1").css('color', '#7F8C8D');
                            $("#three_level3").css('color', '#7F8C8D');
                            $("#three_level").val("正常");
                            break;
                        case "小单":
                            $("#three_level3").css('color', '#000');
                            $("#three_level2").css('color', '#7F8C8D');
                            $("#three_level1").css('color', '#7F8C8D');
                            $("#three_level").val("小单");
                            break;
                        default:
                            break;
                    }
                    switch (s.cusSignDate) {
                        case "预计本周":
                            $("#three_qua1").css('color', '#000');
                            $("#three_qua2").css('color', '#7F8C8D');
                            $("#three_qua3").css('color', '#7F8C8D');
                            $("#three_qua4").css('color', '#7F8C8D');
                            var qua = $("#three_qua").val();
                            $("#three_qua").val("本周" + qua);
                            break;
                        case "预计下周":
                            $("#three_qua2").css('color', '#000');
                            $("#three_qua1").css('color', '#7F8C8D');
                            $("#three_qua3").css('color', '#7F8C8D');
                            $("#three_qua4").css('color', '#7F8C8D');
                            var qua = $("#three_qua").val();
                            $("#three_qua").val("下周" + qua);
                            break;
                        case "预计本月":
                            $("#three_qua3").css('color', '#000');
                            $("#three_qua2").css('color', '#7F8C8D');
                            $("#three_qua1").css('color', '#7F8C8D');
                            $("#three_qua4").css('color', '#7F8C8D');
                            var qua = $("#three_qua").val();
                            $("#three_qua").val("本月" + qua);
                            break;
                        case "预计下月":
                            $("#three_qua4").css('color', '#000');
                            $("#three_qua2").css('color', '#7F8C8D');
                            $("#three_qua3").css('color', '#7F8C8D');
                            $("#three_qua1").css('color', '#7F8C8D');
                            var qua = $("#three_qua").val();
                            $("#three_qua").val("下月" + qua);
                            break;
                        default:
                            break;
                    }
                }

            });
        }
    </script>
</div>
</body>
</html>
