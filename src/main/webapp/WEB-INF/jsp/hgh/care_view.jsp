<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/12
  Time: 16:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<html>
<head>
    <title>客户关怀</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="css/hgh/sale_start_modal.css"/>
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="/css/hgh/cots_view.css">
    <link rel="stylesheet" href="/css/hgh/viewer.min.css">
    <link href="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/css/fileinput.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <style>
        .f-s-16 {
            font-size: 16px;
        }
        .md {
            line-height: inherit;
            vertical-align: bottom;
        }
        [class^="md-"], [class*=" md-"] {
            display: inline-block;
            font: normal normal normal 14px/1 'Material Design Iconic Font';
            font-size: inherit;
            speak: none;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        .text-danger {
            color: #f05050;
        }
    </style>
</head>
<body>


<div class="container-fluid">
    <div class="row">

        <div class="col-sm-12" style="background-color: #ffffff;">
            <div id="memdayalert" style="z-index:0">
                <div class="col-sm-12">
                    <div class="col-sm-8" style="padding: 0">
                        <div class="form-group col-sm-4" style="padding: 0;">
                            <div>
                                <i class="fa fa-filter pull-left"
                                   style="padding: 0;margin-top: 10px;"></i>
                                <div class="col-sm-9" style="padding: 0;">
                                    <select class="selectpicker" onchange="getKid(this)"
                                            data-live-search="true"
                                            style="width: auto;" id="select_data">
                                        <option selected>全部数据</option>
                                        <option>旅游</option>
                                        <option>节日礼品</option>
                                        <option>贺卡</option>
                                        <option>聚餐</option>
                                        <option>其他</option>
                                        <option>本月客户关怀提醒</option>
                                        <option>下月客户关怀提醒</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-8" 　>
                            <i class="fa fa-search col-sm-1" style="margin-top: 10px;"></i>
                            <div class="input-group col-sm-8">
                                <input type="text" class="form-control" id="all">
                                <div class="input-group-btn">
                                    <button type="button" onclick="cotsclick()" class="btn waves-effect"
                                            style="color: black;">客户关怀
                                    </button>
                                    <button type="button" class="btn waves-effect" id="dropdownMenu3"
                                            data-toggle="dropdown"
                                            aria-expanded="false" style="height: 34px;">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                                        <li>
                                            <a class="btn-default" data-toggle="modal" data-target="#transfer"
                                               href="transforcots_modal" id="transforCus">高级查询</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-t-15 f-s-12">
                    <div class="col-sm-2">
                        <span>投诉管理</span>
                        <button type="button" class="btn btn-danger btn-xs pull-right" data-toggle="tooltip"
                                data-placement="bottom"
                                title="解除搜索,显示全部数据" style="display:none" id="reset2" onclick="init()">
                            <i class="fa fa-reply"></i>解除搜索
                        </button>
                    </div>
                    <div class="col-sm-10 text-right">
                        <a class="btn btn-default" disabled id="xj"><i
                                class="fa fa-plus-circle" style="margin-right: 5px"></i>新建
                        </a>
                        <g:g id="58">
                            <script>$('#xj').css("display","none")</script>
                        <a class="btn btn-default" data-toggle="modal" data-target="#addcare" href="addcare_modal"><i
                                class="fa fa-plus-circle" style="margin-right: 5px"></i>新建
                        </a>
                        </g:g>
                    </div>
                </div>
                <div class="tab-content p-0" style="padding: 0">
                    <div class="bootstrap-table m-t-10" style="margin-top: 10px">
                        <div class="fixed-table-container">
                            <table class="table" id="care-table">

                            </table>
                        </div>
                        <div class="col-sm-3 pull-right" style="text-align: right; margin-top: 10px;">
                            <button onclick="print()" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom"
                                    title="打印">
                                <i class="fa fa-print"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addcare" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-labelledby="open"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent" style="width: 800px">

        </div>
    </div>
</div>

<div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-labelledby="open"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">

        </div>
    </div>
</div>

<div class="modal fade" id="care_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-labelledby="open"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">

        </div>
    </div>
</div>

<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript "
        src="bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
<!-- Latest compiled and minified JavaScript -->
<script src="../../js/resjs/bootstrap-table.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="../../js/resjs/bootstrap-tooltip.js"></script>
<script src="../../js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/zh.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/fa.min.js"></script>
<!--资源文件-->
<script src="../../js/resjs/printThis.js"></script>
<script src="/js/sweetalert.min.js"></script>

<script src="js/resjs/care-table.js"></script>
<script src="js/resjs/upload-file.js"></script>
<script src="js/resjs/Scroll.js"></script>
<script src="js/resjs/Tween.js"></script>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    })

    var open_modal;

    function modify(id) {
        $('#addcare').removeData("bs.modal");
        open_modal = id;
        $('#addcare').modal({
            remote: 'addcare_modal'
        });
    }

    function print() {
        /* Act on the event */
        $("#memorial-table").printThis({
            debug: false,
            importCSS: false,
            importStyle: false,
            printContainer: true, //打印容器
            loadCSS: "css/bootstrap-table.css",		//需要加载的css样式
            pageTitle: "sn",
            removeInline: false,
            printDelay: 333, //打印时延
            header: null,
            formValues: false
        });
    }
    function add() {
        $(".pagination-detail").before("<div><button onclick='deleteAllCus()' class='btn btn-default pull-left' style='margin-top: 10px;margin-right: 5px;color:black'><i class='fa fa-trash'></i></button></div>");
    }

    $("#care_detail").on("hidden.bs.modal", function () {
        $(this).removeData("bs.modal");
    });

</script>
</body>
</html>
