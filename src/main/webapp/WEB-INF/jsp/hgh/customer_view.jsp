<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/6
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>客户管理</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="css/hgh/sale_start_modal.css"/>
    <link rel="stylesheet" href="css/city-picker.css"/>
    <link rel="stylesheet" href="/css/sweetalert.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
    <link href="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/css/fileinput.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <style type="text/css">
        #order_top {
            height: 60px;
        }

        #order_top ul {
            border-bottom: solid gainsboro 1px;
        }

        .ul_i {
            border-bottom: solid black 1px;
        }

        thead tr th {
            border-bottom: 2px solid #a7b7c3;
            background-color: #ebeff2;
        }

        body {
            font-family: "microsoft yahei";
            /*padding: 20px;*/
            background-color: #f4f8fb;
        }

        strong {
            color: black;
        }

        #content {
            top: -150px;
        }

        .fixed-table-container {
            /*top: -10px;*/
        }

        .columns {
            top: -45px;
        }

        .fixed-table-pagination {
            position: absolute;
            width: 98%;
            text-align: center;
            /*margin-top: -10px;*/
        }

        .pagination {
            margin-right: 45%;
        }

        .pagination {
        }

        thead {
            background-color: #269ABC;
            color: #000000;
            font-size: 12px;
        }

        tbody {
            font-size: 13px;
            color: #000000;
        }

    </style>
</head>

<body>
<div class="container-fluid">
    <div class="row clearfix col-sm-12" id="order_top">
        <div class="col-sm-12">
            <ul class="nav navbar-nav" style="width: 100%;" id="navbar1">
                <li class="active ul_i" id="first">
                    <g:g id="48">
                    <a href="#">全部客户</a>
                    </g:g>
                </li>
                <li>
                    <g:g id="50">
                    <a href="#">潜在客户</a>
                    </g:g>
                </li>
                <li>
                    <g:g id="52">
                    <a href="#">签约客户</a>
                    </g:g>
                </li>
                <li>
                    <g:g id="54">
                    <a href="#">重复购买</a>
                    </g:g>
                </li>
                <li>
                    <g:g id="54">
                    <a href="#">失效客户</a>
                    </g:g>
                </li>
            </ul>
        </div>
    </div>
    <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：内容块
-->
    <div style="margin-top: 20px;">
        <div class="col-sm-7">
            <div class="form-group col-sm-6" style="padding: 0;">
                <div>
                    <i class="fa fa-filter pull-left" style="margin-right: 10px;padding: 0;margin-top: 10px;"></i>
                    <div class="col-sm-9" style="padding: 0;">
                        <select class="selectpicker" onchange="getFamilyAndKid(this)" data-live-search="true"
                                style="width: auto;" id="select_data">
                            <option selected>全部数据</option>
                            <optgroup label="我的">
                                <option>我的客户</option>
                            </optgroup>
                            <optgroup label="种类">
                                <option>客户</option>
                                <option>供应商</option>
                                <option>合作伙伴</option>
                                <option>媒体</option>
                                <option>其他</option>
                            </optgroup>
                            <optgroup label="三一客定性">
                                <option>不确定</option>
                                <option>无价值</option>
                                <option>有价值</option>
                            </optgroup>
                            <optgroup label="三一客定级">
                                <option>小单</option>
                                <option>正常</option>
                                <option>大单</option>
                            </optgroup>
                            <optgroup label="三一客定量">
                                <option>预计本周</option>
                                <option>预计下周</option>
                                <option>预计本月</option>
                                <option>预计下月</option>
                                <option>过期</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-6 pull-left" style="padding-left: 0;padding-right: 0px;">
                <i class="fa fa-search col-sm-1" style="margin-top: 10px;"></i>
                <div class="input-group col-sm-10">
                    <input type="text" class="form-control" id="input_data">
                    <div class="input-group-btn">
                        <button type="button" onclick="cusclick()" class="btn waves-effect" style="color: black;">客户名称
                        </button>
                        <button type="button" class="btn waves-effect" id="dropdownMenu3" data-toggle="dropdown"
                                aria-expanded="false">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                            <li>
                                <a href="#">客户编号</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="btn-default" data-toggle="modal" data-target="#transfer" href="transfor_modal"
                                   id="transforCus">高级查询</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：form搜索
-->

        <div class="col-sm-3"></div>
        <div class="col-sm-2 pull-left text-center" style="padding: 0;margin-bottom: 15px;">

        </div>

        <div style="width: 100%;text-align: right;">
            <a class="btn btn-default" disabled id="xj"
               style="margin-right: 75px;"><i class="fa fa-plus-circle"></i> 新建客户</a>
            <g:g id="41">
                <script>$('#xj').css("display","none")</script>
            <a class="btn btn-default" data-toggle="modal" data-target="#addcus" href="addcus_modal"
               style="margin-right: 75px;"><i class="fa fa-plus-circle"></i> 新建客户</a>
            </g:g>
        </div>
        <!--
            作者：1810761389@qq.com
            时间：2018-07-23
            描述：搜索状态条数
        -->
    </div>
    <div class="col-sm-7">
        <div class="col-sm-6" style="margin-top: 5px;">
            <span class="pull-left">客户基本信息</span>
            <button type="button" class="btn btn-danger btn-xs pull-left" data-toggle="tooltip" data-placement="bottom"
                    title="解除搜索,显示全部数据" style="display: none" id="reset1" onclick="init()">
                <i class="fa fa-reply"></i>解除搜索
            </button>
            <b style="display: none;color: black" id="tc">&nbsp;高级查询中...</b>
        </div>
        <div class="col-sm-6" style="padding: 0;">
            <a data-toggle="tooltip" data-title="用户画像云图（判断获客渠道价值度）" data-placement="bottom" target="_blank"
               class="btn btn-link pull-left" style="text-decoration: none;">
                <i class="fa fa-cloud"></i> 用户画像云图（判断获客渠道价值度）
            </a>
        </div>
    </div>


    <!--<div class="col-sm-9">

    </div>-->

    <!--
        作者：1810761389@qq.com
        时间：2018-07-31
        描述：table
    -->
    <div id="content">
        <table id="table111">

        </table>
    </div>
    <div class="col-sm-3 pull-right" style="text-align: right; margin-top: 10px;">
        <button onclick="print()" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom"
                title="打印">
            <i class="fa fa-print"></i>
        </button>
    </div>
</div>
<!--
作者：1810761389@qq.com
时间：2018-07-23
描述：统计
-->
<div class="row">
    <div class="col-sm-12" style="margin-top: 50px;">
        <div class="col-sm-6" style="text-align: center;">
            <div class="col-sm-12" id="fenbu" style="width: 100%;height: 437px;"></div>
            <select class="selectpicker col-sm-9" data-live-search="true" style="text-align: center;width: 77%;"
                    title="客户类型分布统计" onchange="getStatistic(this)">
                <option selected>客户类型分布统计</option>
                <option>客户关系等级分布</option>
                <option>客户来源分布</option>
                <option>客户人员规模分布</option>
                <option>客户行业分布</option>
                <option>客户阶段分布</option>
            </select>
            <!--<button type="button" style="text-align: center;width: 77%;" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="客户类型分布统计"><span class="filter-option pull-left">客户类型分布统计</span>&nbsp;<span class="bs-caret pull-right"><span class="caret"></span></span></button>-->
        </div>
        <div class="col-sm-6" style="text-align: center;">

            <div class="col-sm-12" id="top" style="width: 100%;height: 437px;"></div>
            <select class="selectpicker col-sm-9" data-live-search="true" style="text-align: center;width: 77%;"
                    title="客户类型分布统计">
                    <option selected>客户行业分布</option>

            </select>
            <!--<button type="button" style="text-align: center; width: 77%;" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="客户类型分布统计"><span class="filter-option pull-left">客户类型分布统计</span>&nbsp;<span class="bs-caret pull-right"><span class="caret"></span></span></button>-->
        </div>

    </div>
</div>
<br>
<!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：页尾
-->
<div class="col-lg-12">
    <footer class="footer" style="text-align: center;padding-bottom: 20px;padding-top: 20px;">热线:<b>4008-8208-820 </b>
        &nbsp;&nbsp;网站:<b><a href="www.baidu.com" target="_blank">www.baidu.com</a></b> &nbsp;
        <a class="btn btn-danger btn-xs" href="#" onclick="">
            <i class="fa fa-whatsapp m-r-5"></i> 投诉&amp;问题
        </a>&nbsp;&nbsp;
        <a class="btn btn-default btn-xs" href="#" onclick="">
            <i class="fa fa-weixin m-r-5"></i>微客服
        </a>&nbsp;&nbsp;
        <a class="btn btn-primary btn-xs" href="#" onclick="">
            <i class="md md-speaker-notes m-r-5"></i>订阅号
        </a>
        <br>Copyright © 2004-2018 &nbsp;XXX技术有限公司&nbsp;&nbsp;
    </footer>
</div>

<!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：新增客户
-->
<div class="modal fade" id="addcus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-labelledby="open"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">

        </div>
    </div>
</div>
<!--
    作者：1810761389@qq.com
    时间：2018-07-31
    描述：高级查询
-->
<div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-labelledby="open"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">

        </div>
    </div>
</div>

<script type="text/javascript" src="../../js/resjs/fenbu.js"></script>
<script type="text/javascript" src="../../js/resjs/top.js"></script>
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script type="text/javascript " src="bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>
<script type="text/javascript "
        src="bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
<!-- Latest compiled and minified JavaScript -->
<script src="../../js/resjs/bootstrap-table.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="js/resjs/tableExport.js"></script>
<script src="../../js/resjs/bootstrap-tooltip.js"></script>
<script src="../../js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>

<script src="js/resjs/bootstrap-table-export.js"></script>
<script src="js/resjs/xlsx.core.min.js"></script>
<script src="js/resjs/tableExport.min.js"></script>
<script src="js/resjs/FileSaver.min.js"></script>
<script>

    function cusButton(value, row, index) {
        return [
            //
            '<a href="javascript:window.open(\'customer_details?id=' + row.cusId + '\')" style="cursor:pointer;" data-toggle="tooltip" data-placement="bottom"  title="视图"><i class="fa fa-file-text-o"></i></a>',
            '<g:g id="46"><a onclick="modify(' + row.cusId + ')" style="cursor:pointer; margin-left:8px;" data-toggle="tooltip" data-placement="bottom" title="修改"><i class="fa fa-pencil"></i></a></g:g>',
            '<g:g id="42"><a onclick="deleteCus(' + row.cusId + ')" style="cursor:pointer; margin-left:8px;" data-toggle="tooltip" data-placement="bottom" title="删除"><i class="fa fa-trash-o"></i></a></g:g>'
            //					'<a href="/account/delete?id=' + value + '" class=" btn btn-default  btn-sm" style="margin-right:15px;">销户</a>',
            //					'<a   href="/account/reset?id=' + value + '" class=" btn btn-default  btn-sm" style="margin-right:15px;">重置密码</a>',
            //					'<a  href="/transfer/ok?id=' + value + '" class=" btn btn-default  btn-sm" style="margin-right:15px;">对账</a>',
        ].join('');
    }

</script>
<script src="js/resjs/table.js"></script>


<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/zh.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/fa.min.js"></script>
<script src="js/resjs/customer_statistic.js"></script>
<script src="../../js/resjs/printThis.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script>
    $(function () {
        $("#example").popover();
        $("[data-toggle='tooltip']").tooltip();
        x("客户类型分布统计");
        y("客户行业分布");
        z("客户三一客日度统计");
    })

    function print() {
        /* Act on the event */
        $("#table111").printThis({
            debug: false,
            importCSS: false,
            importStyle: false,
            printContainer: true, //打印容器
            loadCSS: "css/bootstrap-table.css",		//需要加载的css样式
            pageTitle: "sn",
            removeInline: false,
            printDelay: 333, //打印时延
            header: null,
            formValues: false
        });
    }

    var open_modal;

    function modify(id) {
        $('#addcus').removeData("bs.modal");
        open_modal = id;
        $('#addcus').modal({
            remote: 'addcus_modal'
        });
    }

    $("#transforCus").click(function () {
        $("#tc").css('display', 'block');
    });


    function getStatistic(obj) {
        var kidData1 = obj.options[obj.selectedIndex].value;
        x(kidData1);
    }

    function x(kidData1) {
        $.ajax({
            url: "customer/customer_statistic?state=" + kidData1,
            method: 'get',
            success: function (data) {
                statisticfenbu(data);
            },
            error: function (data) {
                alert("操作失败");
                swal("失败！", "联系人删除失败！" + data.msg, "Cancel")
            }
        });
    }

    function y(kidData1) {
        $.ajax({
            url: "customer/customer_statistic?state=" + kidData1,
            method: 'get',
            success: function (data) {
                statistictop(data);
            },
            error: function (data) {
                alert("操作失败");
                swal("失败！", "联系人删除失败！" + data.msg, "Cancel")
            }
        });
    }
</script>
<script src="js/resjs/upload_files.js"></script>
<script>
    function add() {
        $(".pagination-detail").before("<div><button onclick='deleteAllCus()' class='btn btn-default pull-left' style='margin-top: 10px;margin-right: 5px;color:black'><i class='fa fa-trash'></i></button></div>");
    }



</script>
</body>

</html>
                                                    