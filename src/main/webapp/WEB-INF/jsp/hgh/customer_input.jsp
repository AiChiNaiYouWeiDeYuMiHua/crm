<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/26
  Time: 22:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
//    response.setContentType("application/vnd.ms-excel;charset=UTF-8");
//    response.setHeader("Content-disposition", "attachment;filename=excel.xls");
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>客户导入</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/css/fileinput.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <style type="text/css">
        thead tr th {
            border-bottom: 2px solid #a7b7c3;
            background: #ebeff2;
        }

        body {
            font-family: "microsoft yahei";
            color: #777777;
            font-size: 12px;
        }
    </style>
</head>

<body>
<div class="col-sm-12">
    <h4><b><i class="fa fa-download"></i> 客户・导入工具</b></h4>
    <h5 style="margin-top: 30px;">1.获取excel导入模板，并填写内容</h5>
    <div class="row" style="margin-left: 10px;">
        <div class="col-sm-4">
            <h4><i class="fa fa-bank m-r-5 fa-lg"></i>客户主体是<b>企业或单位</b>（2B的销售）</h4>
            <form target="excel_import_cus_f" id="frm_customer_full" action="/system/progressbar.xt" method="POST" novalidate="">
                <input type="hidden" value="生成企业客户完整模板" name="progressbar_title">
                <input type="hidden" value="/usetup/download_template.xt" name="progressbar_url">
                <input type="hidden" value="51@1^5`@@1^5`@AbsVnZfJXZt9GdzV3Y" name="excel_t">
                <a href="customer/download?filename=ceshi1.xls" class="btn btn-link m-l-10" onclick="" title="模板下载时间较长,请您耐心等待..." style="text-decoration: none;">
                    字段模版<i class="fa fa-save m-l-5"></i>
                </a>
            </form>
        </div>
        <div class="col-sm-4">
            <h4><i class="fa fa-user m-r-5 fa-lg"></i>客户主体是<b>个人</b>（2C的销售）</h4>
            <form target="excel_import_cuv_f" id="frm_cuview_full" action="/system/progressbar.xt" method="POST" novalidate="">
                <input type="hidden" value="生成个人客户完整模板" name="progressbar_title">
                <input type="hidden" value="/usetup/download_template.xt" name="progressbar_url">
                <input type="hidden" value="08@1^5`@wGb1Z2X3VWa2V3Y" name="excel_t">
                <a href="customer/download?filename=ceshi1.xls" class="btn btn-link m-l-10" onclick="" title="模板下载时间较长,请您耐心等待..." style="text-decoration: none;">
                    字段模版<i class="fa fa-save m-l-5"></i>
                </a>
            </form>
        </div>
    </div>
    <p style="margin-top: 10px;margin-left: 10px;">
        <b>注意：1.当数据字典和用户画像发生变化时，请重新下载模版 2.请使用微软Office，暂不支持WPS</b>
    </p>
</div>
<form method="POST" class="form-inline" style="margin-left: 10px;" enctype="multipart/form-data" action="/usetup/upload_excel.xt" id="subcusup" target="_blank" novalidate="">
    <h5 style="margin-top: 40px;">2. 选择导入方式</h5>
    <div style="margin-left: 20px;">
        <div class="radio">
            <input type="radio" value="0" name="insert_type" id="insert_type_0" checked="" data-parsley-multiple="insert_type">
            <label for="insert_type_0"> <b>新建式导入</b></label>
        </div>
        <br>
        <div class="radio" style="margin-top: 10px;">
            <input type="radio" value="1" name="insert_type" id="insert_type_1" data-parsley-multiple="insert_type">
            <label for="insert_type_1"> <b>覆盖式导入</b>（客户名称相同，excel表内有值的字段覆盖到数据库）</label>
        </div>
        <div class="text-danger" style="margin-left: 10px;">【重要提示：只用于刚导入客户的数据修正！】</div>
    </div>
    <h5 style="margin-top: 40px;">3. 上传xls文件，完成导入</h5>
    <div class="input-group" style="margin-top: 10px;margin-left: 20px;">

        <label class="btn btn-default " onclick="InitExcelFile()">
            <span class="fa fa-apple"></span>
            <span class="buttonText" data-toggle="modal" data-target="#import">选择excel文件</span>
        </label>

    </div>
    <p style="margin-top: 10px;margin-left: 20px;"><b>注意：1.上传填写好内容的excel文件 2.上传后请预览，无错误再导入</b></p>
    <p style="margin-top: 30px;margin-left: 20px;"><b>Tips：一个企业客户下如何导入多个联系人</b></p>
    <p style="margin-left: 20px;">客户“简称”相同的联系人都会导入到同一个企业客户下</p>
</form>

<div id="import" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">文件导入</h4>
            </div>
            <div class="modal-body">
                <form id="ffImport" method="post" accept="application/vnd.ms-excel,application/msexcel">
                    <div title="Excel导入操作" style="padding: 5px">
                        <input type="hidden" id="AttachGUID" name="AttachGUID" />
                        <input id="excelFile" type="file" >
                    </div>
                </form>
                <!--数据显示表格-->
                <table id="gridImport" class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0" border="0">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="SaveImport()">保存</button>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript " src="bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>
<script type="text/javascript "
        src="bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
<!-- Latest compiled and minified JavaScript -->
<script src="../../js/resjs/bootstrap-table.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<%--<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>--%>
<script src="../../js/resjs/bootstrap-tooltip.js"></script>
<script src="../../js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/zh.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-fileinput/4.4.8/js/locales/fa.min.js"></script>

<script src="js/resjs/bootstrap-table-export.js"></script>
<script src="js/resjs/xlsx.core.min.js"></script>
<script src="js//resjs/tableExport.min.js"></script>
<script src="js/resjs/FileSaver.min.js"></script>

<script src="js/resjs/viewer.min.js"></script>
<script src="/js/resjs/printThis.js"></script>
<script src="/js/resjs/bjx-table.js"></script>
<script src="/js/sweetalert.min.js"></script>
<%--<script src="js/resjs/upload-file.js"></script>--%>
<script src="js/resjs/Scroll.js"></script>
<script src="js/resjs/Tween.js"></script>
<script src="js/resjs/excel_xiazai.js"></script>
<script>
    //初始化Excel导入的文件
    function InitExcelFile() {
//				//记录GUID
//				$("#AttachGUID").val(newGuid());
        $("#excelFile").fileinput({
            uploadUrl: "/excel_upload", //上传的地址
            uploadAsync: true, //异步上传
            language: "zh", //设置语言
            showCaption: true, //是否显示标题
            showUpload: true, //是否显示上传按钮
            showRemove: true, //是否显示移除按钮
            showPreview: true, //是否显示预览按钮
            browseClass: "btn btn-primary", //按钮样式
            dropZoneEnabled: false, //是否显示拖拽区域
            allowedFileExtensions: ["xls", "xlsx"], //接收的文件后缀
            maxFileCount: 1, //最大上传文件数限制
            previewFileIcon: '<i class="glyphicon glyphicon-file"></i>',
            allowedPreviewTypes: null,
            previewFileIconSettings: {
                'docx': '<i class="glyphicon glyphicon-file"></i>',
                'xlsx': '<i class="glyphicon glyphicon-file"></i>',
                'pptx': '<i class="glyphicon glyphicon-file"></i>',
                'jpg': '<i class="glyphicon glyphicon-picture"></i>',
                'pdf': '<i class="glyphicon glyphicon-file"></i>',
                'zip': '<i class="glyphicon glyphicon-file"></i>',
            },
            uploadExtraData: { //上传的时候，增加的附加参数
                folder: '数据导入文件',
                guid: $("#AttachGUID").val()
            }
        }) //文件上传完成后的事件
            .on("fileuploaded", function(event, data, previewId, index) {
                var result = data.response; //后台返回的json
                //console.log(result.status);
                //console.log(result.id);
                // $('#picid').val(result.id);//拿到后台传回来的id，给图片的id赋值序列化表单用
                //如果是上传多张图
                /*
                //计数标记，用于确保全部图片都上传成功了，再提交表单信息
                var fileCount = $('#file-pic').fileinput('getFilesCount');
                if(fileCount==1){
                $.ajax({//上传文件成功后再保存图片信息
                    url:'BannerPicAction!savaForm.action',
                    data:$('#form1').serialize(),//form表单的值
                    success:function(data,status){
                        ...
                    },
                    cache:false,                    //不缓存
                });
                }
                */
                $.ajax({//上传文件成功后再保存图片信息
                    url:'excel_upload',
                    type:'post',
                    dataType:'json',
                    data:$('#ffImport').serialize(),//form表单的值
                    success:function(){

                    },
                    cache:false,                    //不缓存
                });
                alert(result.uri);
            });

        // $('#savePic').on('click',function (){// 提交图片信息 //
        //     //先上传文件，然后在回调函数提交表单
        //     $('#file-pic').fileinput('upload');
        //
        // });
    }

</script>
</body>

</html>
