<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/6
  Time: 9:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="/css/old/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="/css/lcy/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css"/>
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>

    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        html, body {
            height: 100%;
            width: 100%;
        }

        #area-render {
            position: fixed;
            width: 320px;
            height: 160px;
            right: 4px;
            bottom: 4px;
            border: dashed 1px #ccc;
        }
    </style>
</head>
<body>
<div id="area-render1">
    <h1>Hello Word</h1>
    <a class="btn btn-default" id="add_btn" href="javascript:void(0);"
       style="margin-right: 75px;"><i class="fa fa-plus-circle"></i>新建</a>
    <a class="btn btn-default" id="check_btn" href="javascript:void(0);"
       style="margin-right: 75px;"><i class="fa fa-plus-circle"></i>打开选择</a>
</div>
<div class="modal fade" id="to_select_user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade" id="select_user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- 选择用户 -->
<div class="modal fade" id="user_check" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.bootcss.com/moment.js/2.22.1/moment.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="/js/resjs/bootstrap-table.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<%--<script src="//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>--%>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="/js/resjs/bootstrap-popover.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>

<!--excel-->
<script src="/js/lcy/table-user.js"></script>
<script src="/js/ykm/money.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<%--<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>--%>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="/js/resjs/printThis.js"></script>
<script src="/js/lcy/bootstrap-select.min.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="/js/lcy/user_list.js"></script>
<script>
    $(function () {

        $("#example").popover();
        $("[data-toggle='tooltip']").tooltip();
        add_modal();
        open();
    });

    //清除模态框数据
    $("#select_user").on('hidden.bs.modal', function () {
        $(this).removeData();
    });

    //清除模态框数据
    $("#to_select_user").on('hidden.bs.modal', function () {
        $(this).removeData();
    });

    /**
     * 用户选择
     */
    $("#user_check").on('hidden.bs.modal', function () {
        $(this).removeData();
    });
    function open() {
        $("#check_btn").click(function () {
            $('#user_check').modal({
                remote:'/admin/to_select_user_modal'
            })
        })
    }

    function add_modal() {
        $("#add_btn").click(function () {
            $('#to_select_user').modal({
                remote: '/admin/go_to_select_user'
            })
        });
    }

    $("#select_user").on('show.bs.modal', function () {
        $(this).css('z-index', '1600');
        // $(".modal-dialog").css('z-index', '3000');
    });


    $("#to_select_user").on('show.bs.modal', function () {
        $(this).css('z-index', '1050');
        // $(".modal-backdrop").css('z-index', '1040');
    });

</script>
<script>
    $(function () {
        $(".pagination-detail").before("<div><button class='btn btn-default pull-left' style='margin-top: 10px;margin-right: 5px;color:black'><i class='fa fa-trash'></i></button></div>");
    })
</script>
<script type="text/javascript" color="187,225,234" opacity='0.7' zIndex="-2" count="400"
        src="/js/plugins/canvas/canvas-nest.js"></script>
<script type="text/javascript" src="/js/plugins/canvas/canvas-nest.umd.js"></script>
</body>
</html>
