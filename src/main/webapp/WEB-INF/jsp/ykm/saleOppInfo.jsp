<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/7
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<html>
<head>
    <title>机会：${info.oppTheme}</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css" />
</head>
<style type="text/css">
    html,
    body {
        color: #333333;
        background-color: #F4F8FB;
        font-size: 12px;
        font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .table-t thead{
        background-color: #EBEFF2;font-family: 楷体;
    }
    a{
        color: #505458;
    }
    .text-gray{
        color: #999;
    }
    .m-20{
        margin: 20px;
    }
    .m-l-10{
        margin-left: 10px;
    }
    .m-b-10{
        margin-bottom: 10px;
    }
    .m-b-30{
        margin-bottom: 30px;
    }
    .icon-box{
        display: inline-block;
        min-width: 100px;
    }
    .border-r{
        border-right: 1px solid #ccc;
    }
    a:hover{text-decoration: none}
    .panel-title{
        font-size: 12px;
    }
    .fixed-table-body{
        height: auto;
    }
    .m-r-5 {
        margin-right: 5px;
    }
    .m-l-5 {
        margin-left: 5px;
    }
</style>
<body>
<div class="container">
    <div class="row" style="background-color: white;padding: 10px; margin-top: 20px">
        <div id="pro-header">
            <div class="pull-right" style="font-size: 12px;">
                <g:g id="323">
                    <button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" onclick="deleteOpp(${info.oppId})" data-target="#delModal">
                        <i class="fa fa-trash-o" style="margin-right: 5px;"></i>删除
                    </button>&nbsp;
                </g:g>           
                <g:g id="322">
                    <button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.oppId})">
                        <i class="fa fa-pencil" style="margin-right: 5px;"></i>编辑
                    </button>&nbsp;
                </g:g>
            </div>
            <h4>
                <span>销售机会</span>
                <a target="_blank" href="??" style="color: #999999;" data-toggle="tooltip" data-placement="bottom" title="数据日志">
                    <i class="fa fa-bars"></i>
                </a>
            </h4>
            <hr style="border-top: 1px solid #aaaaaa;" />
        </div>
        <div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">机会主题：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.oppTheme}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">更新日期：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.updateTime}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">客户：</label>
                        <c:if test="${info.tbCustomerByCusId.cusId != null}">
                            <div class=" col-md-10"><span style="font-weight:normal;color:#9e9e9e">〖</span>${info.tbCustomerByCusId.cusName}
                                <a href="/customer_details?id=${info.tbCustomerByCusId.cusId}" target="_blank" >
                                    <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
                        </c:if>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">客户联系人：</label>
                        <div class=" col-md-10">${info.tbContactsBy联系人Id.cotsName}</div>
                        <%--<c:if test="${info.tbContactsBy联系人Id.cotsId != null}">
                            <div class=" col-md-10"><span style="font-weight:normal;color:#9e9e9e">〖</span>${info.tbContactsBy联系人Id.cotsName}
                                <a href="/customer_detail?id="+${info.tbContactsBy联系人Id.cotsId} >
                                    <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span></div>
                        </c:if>--%>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">状态：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;
                            <c:choose>
                            <c:when test="${info.oppStatus==1}">
                                跟踪
                            </c:when>
                            <c:when test="${info.oppStatus==2}">
                                成功
                            </c:when>
                            <c:when test="${info.oppStatus==3}">
                                失败
                            </c:when>
                            <c:when test="${info.oppStatus==4}">
                                搁置
                            </c:when>
                            <c:when test="${info.oppStatus==5}">
                                失效
                            </c:when>
                        </c:choose></p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">类型：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.classification}</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">发现时间：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.discoveryTime}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">来源：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.oppSource}</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">负责人：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.tbUserByUserId.userName}</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">客户需求:</label>
                        <div class=" col-md-10">${info.customerDemand}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">预计签单日期：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.expectedTime}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">可能性：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.possibility}</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">预期金额：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.expectedAmount}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">优先：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;
                            <c:choose>
                                <c:when test="${info.priority==1}">
                                    一级
                                </c:when>
                                <c:when test="${info.priority==2}">
                                    二级
                                </c:when>
                                <c:when test="${info.priority==3}">
                                    三级
                                </c:when>
                                <c:when test="${info.priority==4}">
                                    四级
                                </c:when>
                                <c:when test="${info.priority==5}">
                                    五级
                                </c:when>
                                <c:otherwise>

                                </c:otherwise>
                            </c:choose>
                            </p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">阶段：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.stage}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">阶段开启时间：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.stageStartTime}</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">阶段备注:</label>
                        <div class=" col-md-10">${info.stageRemarks}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">跟踪日志:</label>
                        <div class=" col-md-10" style="border-bottom: 1px solid #eee;">&nbsp;${info.trackLog}</div>
                    </div>
                    <%--<hr class="border-t-a">--%>
                </div>
            </div>
        </div>
        <div id="pro-bottom" class="text-right">
            <g:g id="323">
                <button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" data-target="#delModal" onclick="deleteOpp(${info.oppId})">
                    <i class="fa fa-trash-o" style="margin-right: 5px;" ></i>删除
                </button>&nbsp;
            </g:g>
            <g:g id="322">
                <button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.oppId})">
                    <i class="fa fa-pencil" style="margin-right: 5px;" ></i>编辑
                </button>&nbsp;
            </g:g>
        </div>

    </div>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-6 panel  panel-default" id="demand_left" style="padding: 0;border: 0;">
            <div class="panel-heading" style="background-color: white;border: none">
                <h5 class="panel-title">
                    <i class="fa fa-book"></i>
                    详细需求
                    <g:g id="331">
                        <a onclick="loadModal('/demand_add',null)" style="cursor: pointer;">
                            <i class="fa fa-plus"></i>
                        </a>
                    </g:g>
                </h5>
            </div>
            <div class="panel-body">
                <c:forEach items="${info.tbDemandsByOppId}" var="d">
                    <c:if test="${d.isDelete == 0}">
                        <p ><i class="fa fa-square text-gray" style="margin-right: 5px"></i>
                            <g:g id="332"><a onclick="loadModal('/demand_add',${d.demandId})" style="cursor: pointer;"></g:g>
                                        ${d.demandTheme}
                                <g:g id="332"></a></g:g>

                            <small style="margin-left: 5px">${d.recordTime}</small></p>
                    </c:if>
                </c:forEach>
            </div>
        </div>
        <div class="col-md-6" style="padding-right: 0" >
            <div class="panel  panel-default" style="padding: 0;border: 0" id="quote_right">
                <div class="panel-heading" style="background-color: white;border: none">
                    <h5 class="panel-title">
                        <i class="fa fa-calculator"></i>
                        报价单
                        <g:g id="361">
                            <a onclick="loadModal('/quote_add',null)" style="cursor: pointer;">
                                <i class="fa fa-plus"></i>
                            </a>
                        </g:g>
                    </h5>
                </div>
                <div class="panel-body">
                    <c:forEach items="${info.tbQuotesByOppId}" var="d">
                        <c:if test="${d.isDelete == 0}">
                            <p><i class="fa fa-square text-gray m-r-5"></i>
                                <g:g id="362"><a onclick="loadModal('/quote_add',${d.quoteId})" style="cursor: pointer;"></g:g>${d.quoteTheme}<g:g id="362"></a></g:g>
                                ${d.quoteTime}<font class="text-gray m-l-5">报价:</font><span style="white-space:nowrap"><span style="font-size:9pt;font-weight:normal;">￥</span>${d.totalQuote}</span><br><small>　
                                    <g:g id="365">
                                        <a class="btn btn-link" href="/quote/to_quoteDetail?id=${d.quoteId}" target="_blank">
                                            <i class="fa fa-arrow-circle-right m-r-5"></i>编辑明细
                                        </a>
                                    </g:g>
                                    <a class="btn btn-link" href="/quote/info/${d.quoteId}/" target="_blank">
                                        <i class="fa fa-arrow-circle-right m-r-5"></i>查看明细
                                    </a>
                                    <g:g id="362">
                                        <c:if test="${d.isOrder == 0}">
                                            <a class="btn btn-link" onclick="toOrder('${d.quoteId}')">
                                                <i class="fa fa-arrow-circle-right m-r-5"></i>转成订单
                                            </a>
                                        </c:if>
                                    </g:g>
                                </small></p>
                        </c:if>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 panel  panel-default" style="padding: 0;border: 0" id="solution_left">
            <div class="panel-heading" style="background-color: white;border: none">
                <h5 class="panel-title">
                    <i class="fa fa-book"></i>
                    解决方案
                    <g:g id="341">
                        <a onclick="loadModal('/solution_add',null)" style="cursor: pointer;">
                            <i class="fa fa-plus"></i>
                        </a>
                    </g:g>
                </h5>
            </div>
            <div class="panel-body">
                <c:forEach items="${info.tbSolutionsByOppId}" var="d">
                    <c:if test="${d.isDelete == 0}">
                        <p><small class="pull-right">${d.submitTime}</small><i class="fa fa-square text-gray m-r-5"></i>
                            <g:g id="342"><a onclick="loadModal('/solution_add',${d.solutionId})" style="cursor: pointer;"></g:g>${d.solutionTheme}<g:g id="342"></a></g:g></p>
                    </c:if>
                </c:forEach>
            </div>
        </div>
        <div class="col-md-6" style="padding-right: 0" >
            <div class="panel  panel-default" style="padding: 0;border: 0" id="competitor_right">
                <div class="panel-heading" style="background-color: white;border: none">
                    <h5 class="panel-title">
                        <i class="fa fa-list-ol"></i>
                        竞争对手
                        <g:g id="351">
                            <a onclick="loadModal('/competitor_add',null)" style="cursor: pointer;">
                                <i class="fa fa-plus"></i>
                            </a>
                        </g:g>
                    </h5>
                </div>
                <div class="panel-body">
                    <c:forEach items="${info.tbCompetitorsByOppId}" var="d">
                        <c:if test="${d.isDelete == 0}">
                            <p><small class="pull-right"><i data-toggle="tooltip" data-placement="bottom" data-title="" class="f-s-16 " data-original-title="" title=""></i></small><i class="fa fa-square text-gray m-r-5"></i>
                                <g:g id="352"><a onclick="loadModal('/competitor_add',${d.competitorId})" style="cursor: pointer;"></g:g>${d.company}<g:g id="352"></a></g:g><font class="text-gray m-l-5">价格:</font><span style="white-space:nowrap"><span style="font-size:9pt;font-weight:normal;">￥</span>${d.price}</span></p>
                        </c:if>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="text-align: center;background-color: #F4F8FB;">
        <div class="col-md-12">
            <div class="form-group">
                <footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
                    <a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
                    <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
                    <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
                    <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>
            </div>
        </div>
    </div>
    <!--
 作者：1810761389@qq.com
 时间：2018-08-09
 描述：新增/编辑销售机会
-->
    <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-labelledby="open" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content mymodalcontent">

            </div>
        </div>
    </div>

    <div class="modal fade" id="load_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-labelledby="open" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content mymodalcontent">

            </div>
        </div>
    </div>

</div>
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<%--<script src="//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>--%>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<!--excel-->
<%--<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>--%>
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-validator/0.5.3/js/language/zh_CN.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="/js/sale/formatter.js"></script>
<script type="text/javascript">
    var ykm_cusId;
    var ykm_cusName;
    var ykm_oppId;
    $(function () {
        $("[data-toggle='tooltip' ] ").tooltip();
        //清除模态框数据
        $("#add_modal,#load_modal").on("hidden.bs.modal", function () {
            open_modal = null;
            $(this).removeData('bs.modal');
            location.reload();
        });
        if($("#demand_left").height() > $("#quote_right").height()){
            $("#quote_right").css("height",$("#demand_left").height());
        }else{
            $("#demand_left").css("height",$("#quote_right").height());
        }
        if($("#solution_left").height() > $("#competitor_right").height()){
            $("#competitor_right").css("height",$("#solution_left").height());
        }else{
            $("#solution_left").css("height",$("#competitor_right").height());
        }
    });
    //编辑
    function edit(id) {
        open_modal = id;
        $("#add_modal").modal({
            remote:'/sale_opp_add'
        });
    }
    function successForm() {
        $('#add').modal('hide')
        window.history.go(0)
    }

    function toOrder(quoteId) {
        swal({
            title: "是否转成订单？",
            text: "是否转成订单吗？！",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定",
            closeOnConfirm: false
            },
            function(){
                ajax("/quote/to_order",function () {
                    swal("成功！", "转成订单成功！","success")
                    window.opener.location.reload();
                    self.close();
                },function(){},'get',data={'id':quoteId});
            });
    }
    
    //删除
    function deleteOpp(pID) {
        swal({
                title: "确定删除吗？",
                text: "确定删除本数据吗？！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定",
                closeOnConfirm: false
            },
            function(){
                ajax("/sale_opp_delete",function () {
                    swal("成功！", "删除成功！","success")
                    window.opener.location.reload();
                    self.close();
                },function(){},'get',data={'oppId':pID});
            });
    }

    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                    error()
                }
            }
        })
    }

    function loadModal(url,id) {
        if(id == null || id.length <= 0){
            open_modal = null;
        } else {
            open_modal = id;
        }
        ykm_cusId = "${info.tbCustomerByCusId.cusId}";
        ykm_cusName = "${info.tbCustomerByCusId.cusName}";
        ykm_oppId = "${info.oppId}";
        $('#load_modal').modal({
            remote: url
        })
    }


</script>
</body>
</html>
