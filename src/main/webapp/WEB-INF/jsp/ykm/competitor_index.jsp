<%--
  Created by IntelliJ IDEA.
  User: hgh
  Date: 2018/8/6
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>竞争对手管理</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/ykm/bootstrap-select.css" />
    <link rel="stylesheet" href="css/ykm/bootstrap-datetimepicker.css" />
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sale_start_modal.css" />
    <link rel="stylesheet" href="../css/bootstrap-table.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
    <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <style type="text/css">
        #order_top {
            height: 60px;
        }

        #order_top ul {
            border-bottom: solid gainsboro 1px;
        }

        .ul_i {
            border-bottom: solid black 1px;
        }

        thead tr th {
            border-bottom: 2px solid #a7b7c3;
            background-color: #ebeff2;
        }

        body {
            font-family: "microsoft yahei";
            color: #777777;
            padding: 20px;
            background-color: #f4f8fb;
        }

        strong {
            color: black;
        }

        #content {
            top: -150px;
        }

        .fixed-table-container {
            /*top: -10px;*/
        }

        .columns {
            top: -45px;
        }

        .fixed-table-pagination {
            position: absolute;
            width: 98%;
            text-align: center;
            /*margin-top: -10px;*/
        }

        .pagination {
            margin-right: 49%;
        }

        .pagination {}

        thead {
            background-color: #269ABC;
            color: #000000;
            font-size: 12px;
        }

        tbody {
            font-size: 13px;
            color: #000000;
        }

        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 180px;
        }
    </style>
</head>

<body>
<div class="container-fluid">
    <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：内容块
-->
    <div style="margin-top: 20px;">
        <div class="form-group col-sm-2" style="padding: 0;">
            <div>
                <i class="fa fa-filter pull-left" style="margin-right: 10px;padding: 0;margin-top: 10px;"></i>
                <div class="col-sm-9" style="padding: 0;">
                    <select class="selectpicker" id="select-type" onchange="getFamilyAndKid(this)" name="type" data-live-search="true" style="width: 200px;">
                        <option selected value="0">全部数据</option>
                        <optgroup label="竞争能力">
                            <option value="1">核心竞争</option>
                            <option value="2">有力竞争</option>
                            <option value="3">一般竞争</option>
                            <option value="4">忽略竞争</option>
                        </optgroup>
                    </select>
                </div>
            </div>
        </div>
        <!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：form搜索
-->
        <div class="form-group col-sm-4" style="padding: 0;">
            <i class="fa fa-search col-sm-1" style="margin-top: 10px;"></i>
            <div class="input-group col-sm-8">
                <input type="text" class="form-control" id="search-text">
                <div class="input-group-btn">
                    <button type="button" onclick="fastQuery(1)" class="btn waves-effect" style="color: black;">竞争对手公司名称</button>
                    <button type="button" class="btn waves-effect" id="dropdownMenu3" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                        <li class="divider"></li>
                        <li>
                            <a class="btn-default" id="search-btn"  href="javascript:void(0)">高级查询</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-3"></div>
        <div class="col-sm-2 pull-left text-center" style="padding: 0;margin-bottom: 15px;">

        </div>

        <div style="width: 100%;text-align: right;">
            <a class="btn btn-default" id="add-btn-false" href="javascript:void(0);" style="margin-right: 75px;" disabled="disabled">smile</a>
            <g:g id="361">
                <script type="text/javascript">
                    $("#add-btn-false").hide();
                </script>
                <a class="btn btn-default" id="add-btn" href="javascript:void(0);" style="margin-right: 75px;" ><i class="fa fa-plus-circle"></i>新建</a>
            </g:g>
        </div>
        <!--
            作者：1810761389@qq.com
            时间：2018-07-23
            描述：搜索状态条数
        -->
    </div>
    <div class="col-sm-2" style="margin-top: 5px;">
        <span class="pull-left">竞争对手</span>
        <button type="button" onclick="cancelKey()" id="cancel-search" class="hidden btn btn-danger btn-xs pull-left" data-toggle="tooltip" data-placement="bottom" title="解除搜索,显示全部数据">
            <i class="fa fa-reply"></i>解除搜索
        </button>
    </div>

    <!--
        作者：1810761389@qq.com
        时间：2018-07-31
        描述：table
    -->
    <div id="content">
        <table id="table111">

        </table>

    </div>
    <div class="col-sm-3 pull-right" style="text-align: right; margin-top: 10px;">
        <button onclick="print()" type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="打印">
            <i class="fa fa-print"></i>
        </button>
    </div>
</div>
<div style="height: 100px;"></div>
<!--
    作者：1810761389@qq.com
    时间：2018-07-23
    描述：页尾
-->
<div class="col-lg-12">
    <footer class="footer" style="text-align: center;padding-bottom: 20px;padding-top: 20px;">热线:<b>4008-8208-820 </b> &nbsp;&nbsp;网站:<b><a href="www.baidu.com" target="_blank">www.baidu.com</a></b> &nbsp;
        <a class="btn btn-danger btn-xs" href="#" onclick="">
            <i class="fa fa-whatsapp m-r-5"></i> 投诉&amp;问题
        </a>&nbsp;&nbsp;
        <a class="btn btn-default btn-xs" href="#" onclick="">
            <i class="fa fa-weixin m-r-5"></i>微客服
        </a>&nbsp;&nbsp;
        <a class="btn btn-primary btn-xs" href="#" onclick="">
            <i class="md md-speaker-notes m-r-5"></i>订阅号
        </a>
        <br>Copyright © 2004-2018 &nbsp;XXX技术有限公司&nbsp;&nbsp; </footer>
</div>

<!--
    作者：1810761389@qq.com
    时间：2018-08-09
    描述：新增/编辑销售机会
-->
<div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mymodalcontent">

        </div>
    </div>
</div>
<!--
    作者：1810761389@qq.com
    时间：2018-07-31
    描述：高级查询
-->
<div class="modal fade" id="search-senior" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>

<script type="text/javascript" src="../js/resjs/fenbu.js"></script>
<script type="text/javascript" src="../js/resjs/top.js"></script>
<script type="text/javascript" src="../js/resjs/util.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="../js/resjs/bootstrap-table.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<%--<script src="//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>--%>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="../js/resjs/bootstrap-tooltip.js"></script>
<script src="../js/resjs/bootstrap-popover.js"></script>
<!--excel-->
<script src="../js/ykm/public.js"></script>
<script>
    function dimiss(value, row, index) {
        return [
            '<g:g id="352"><a onclick="edit('+row.competitorId+')" style="cursor:pointer; margin-left:8px;" data-toggle="tooltip" data-placement="bottom" title="修改"><i class="fa fa-pencil"></i></a></g:g>',
            '<g:g id="363"><a onclick="deleteOpp('+row.competitorId+')" style="cursor:pointer; margin-left:8px;" data-toggle="tooltip" data-placement="bottom" title="删除"><i class="fa fa-trash-o"></i></a></g:g>'
        ].join('');
    }
</script>
<script src="../js/ykm/competitor_table.js"></script>
<script src="../js/ykm/money.js"></script>
<%--<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/i18n/defaults-zh_CN.min.js"></script>--%>
<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>
<script src="../js/ykm/bootstrap-select.js"></script>
<script src="/js/sweetalert.min.js"></script>
<%--<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>--%>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="../js/resjs/printThis.js"></script>
<script>
    $(function() {
        //回车事件
        $('#search-text').bind('keypress',function(event){
            if(event.keyCode == 13)
            {
                fastQuery(1)
            }
        });
        $("#example").popover();
        $("[data-toggle='tooltip']").tooltip();
        add_modal();
        //清除模态框数据
        $("#add_modal").on("hidden.bs.modal", function () {
            open_modal = null;
            $(this).removeData('bs.modal');
        });
    })

    var open_modal;

    //加载模态框
    function add_modal() {
        $("#add-btn").click(function () {
            $('#add_modal').modal({
                remote:'competitor_add'
            })
        });
        $("#search-btn").click(function () {
            $('#search-senior').modal({
                remote:'to_competitor_senior'
            })
        });
    }

    //编辑
    function edit(id) {
        open_modal = id;
        $("#add_modal").modal({
            remote:'competitor_add'
        });
    }

    //删除
    function deleteOpp(pID) {
        swal({
                title: "确定删除吗？",
                text: "确定删除本数据吗？！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定",
                closeOnConfirm: false
            },
            function(){
                ajax("competitor/delete",function () {
                    swal("成功！", "产品删除成功！","success")
                    $("#table111").bootstrapTable('refresh')
                },function(){},'get',data={'id':pID});
            });
    }

    //批量删除
    function deleteSelected() {
        var datas = $("#table111").bootstrapTable("getSelections");
        if(datas.length > 0){
            var idArray = [];
            for(var i= 0; i<datas.length; i++){
                idArray.push(datas[i].competitorId)
            }
            swal({
                title: "确定删除吗？",
                text: "将要删除选中的" + datas.length + "个数据",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定",
                closeOnConfirm: false
            },function () {
                ajax("competitor/deleteSelected", function () {
                    swal("成功！", "机会删除成功！", "success");
                    $("#table111").bootstrapTable('refresh')
                }, function () {
                }, 'get', {ids:idArray});
            });
        }
    }


    function print() {
        /* Act on the event */
        $("#table111").printThis({
            debug: false,
            importCSS: false,
            importStyle: false,
            printContainer: true, //打印容器
            loadCSS: "css/bootstrap-table.css",		//需要加载的css样式
            pageTitle: "sn",
            removeInline: false,
            printDelay: 333, //打印时延
            header: null,
            formValues: false
        });
    }
</script>
<script>

    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                    error()
                }
            }
        })
    }

    function deleteAll() {
        $(".pagination-detail").before("<div><button onclick='deleteSelected()' class='btn btn-default pull-left' style='margin-top: 10px;margin-right: 5px;color:black'><i class='fa fa-trash'></i></button></div>");
    }
</script>
</body>

</html>