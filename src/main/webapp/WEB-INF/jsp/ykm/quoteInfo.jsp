<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="simple" uri="http://went.top/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: wsj
  Date: 2018/8/7
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<html>
<head>
    <title>报价视图</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap-table.css">
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <script src="https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts.min.js"></script>
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sale_start_modal.css" />
</head>
<style type="text/css">
    html,
    body {
        color: #333333;
        background-color: #F4F8FB;
        font-size: 12px;
        font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }
    .table-t thead{
        background-color: #EBEFF2;font-family: 楷体;
    }
    a{
        color: #505458;
    }
    .text-gray{
        color: #999;
    }
    .m-20{
        margin: 20px;
    }
    .m-l-10{
        margin-left: 10px;
    }
    .m-b-10{
        margin-bottom: 10px;
    }
    .m-b-30{
        margin-bottom: 30px;
    }
    .icon-box{
        display: inline-block;
        min-width: 100px;
    }
    .border-r{
        border-right: 1px solid #ccc;
    }
    a:hover{text-decoration: none}
    .panel-title{
        font-size: 12px;
    }
    .fixed-table-body{
        height: auto;
    }
    a{
        color: #505458;
    }
    .text-gray{
        color: #999;
    }
    .m-20{
        margin: 20px;
    }
    .m-l-10{
        margin-left: 10px;
    }
    .m-b-10{
        margin-bottom: 10px;
    }
    .m-b-30{
        margin-bottom: 30px;
    }
    .icon-box{
        display: inline-block;
        min-width: 100px;
    }
    .border-r{
        border-right: 1px solid #ccc;
    }
    a:hover{text-decoration: none}
    .panel-title{
        font-size: 12px;
    }
    .fixed-table-body{
        height: auto;
    }
    .m-l-5{
        margin-left: 5px;
    }
    .m-b-10{
        margin-bottom: 10px;
    }
    .m-r-5{
        margin-right: 5px;
    }
</style>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="font-size: 12px ">
            <simple:approval entity="${info.quoteId}" status="${info.approvalStatus}" type="报价单"></simple:approval>
        </div>
    </div>
    <div class="row" style="background-color: white;padding: 10px;">
        <div id="pro-header">
            <div class="pull-right" style="font-size: 12px;">
                <g:g id="363">
                    <button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" onclick="deleteOpp(${info.quoteId})" data-target="#delModal">
                        <i class="fa fa-trash-o" style="margin-right: 5px;"></i>删除
                    </button>&nbsp;
                </g:g>
                <g:g id="362">
                    <button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.quoteId})">
                        <i class="fa fa-pencil" style="margin-right: 5px;"></i>编辑
                    </button>&nbsp;
                </g:g>
            </div>
            <h4>
                <span>报价记录</span>
                <a  href="javaScript:" style="color: #999999;" data-toggle="tooltip" data-placement="bottom" title="数据日志">
                    <i class="fa fa-bars"></i>
                </a>
            </h4>
            <hr style="border-top: 1px solid #aaaaaa;" />
        </div>
        <div class="gedit_scroll_content" style="padding-left: 30px;padding-right: 100px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">主题：</label>
                        <div class="col-md-10">${info.quoteTheme}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">客户：</label>
                        <div class=" col-md-10">
                            <c:if test="${info.tbCustomerByCusId.cusId != null}">
                                <span style="font-weight:normal;color:#9e9e9e">〖</span>${info.tbCustomerByCusId.cusName}
                                <a href="/customer_details?id=${info.tbCustomerByCusId.cusId}" target="_blank">
                                    <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a><span style="font-weight:normal;color:#9e9e9e">〗</span>
                            </c:if>
                        </div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">销售机会：</label>
                        <div class=" col-md-10">
                            <c:if test="${info.tbSaleOppByOppId.oppId != null}">
                                <span style="font-weight:normal;color:#9e9e9e">〖</span>${info.tbSaleOppByOppId.oppTheme}
                                <a href="/sale/opp/info/${info.tbSaleOppByOppId.oppId}" target="_blank">
                                    <i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a>
                                <span style="font-weight:normal;color:#9e9e9e">〗</span>
                            </c:if>
                        </div>
                    </div>
                    <hr class="border-t-a">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">报价单号：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.quoteNumber}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">日期：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.quoteTime}</p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <%--<label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">报价（总）：</label>
                    <div class="col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.orderTotal}" type="currency"></fmt:formatNumber>
                            <c:if test="${info.orderTotal != info.total}">
                            <i class="fa fa-warning pull-right" data-toggle="toolip" data-placement="bottom"
                            title="总金额和明细金额不一致,明细金额<fmt:formatNumber value="${info.total}" type="currency"></fmt:formatNumber>" style="color: red"></i>
                            </c:if>
                        </p>
                    </div>--%>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">报价（总）：</label>
                    <div class="col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp; <span id="totalQuote">${info.totalQuote}&nbsp;&nbsp;</span> </p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">预计毛利：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.expectedProfit}
                            &nbsp;&nbsp;<a title="提示：预估毛利是指报价单总金额减去报价单明细中的总成本计算而得" style="font-weight:bold;CURSOR:help"><img src="/img/infild.png" align="AbsMiddle"><font color="#FF99FF">提示</font></a>
                        </p>
                    </div>
                </div>
                <%--<div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">毛利：</label>
                    <div class="col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.maori}" type="currency"></fmt:formatNumber>
                            <i class="fa fa-info-circle pull-right" data-toggle="toolip" data-placement="bottom"
                               title="可以不输入，由订单明细自动合计"></i></p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">开发票金额：</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.billMOney}" type="currency"></fmt:formatNumber></p>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">预计毛利：</label>
                    <div class="col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;"><fmt:formatNumber value="${info.forecastMaori}" type="currency"></fmt:formatNumber>
                            <i class="fa fa-info-circle pull-right" data-toggle="toolip" data-placement="bottom"
                               title="可以不输入，由订单明细自动合计"></i></p>
                    </div>
                </div>--%>

                <div class="form-group col-md-12">
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">接收人:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.tbContactsBy接收人Id.cotsName}</p>
                    </div>
                    <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">报价人:</label>
                    <div class=" col-md-4">
                        <p style="border-bottom: 1px solid #eee;margin-bottom: 15px;">&nbsp;${info.tbUserByUserId.userName}</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">报价人联系方式:</label>
                        <div class=" col-md-10">${info.quotationContact}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">明细:</label>
                    </div>
                </div>
                <div id="detail">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">备注:</label>
                        <div class=" col-md-10">${info.remarks}</div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">审批状态:</label>
                        <div class=" col-md-10"><c:choose>
                            <c:when test="${info.approvalStatus == 1}">
                                待审<i data-toggle='tooltip' data-placement='bottom' title='待审' class='f-s-16 fa fa-coffee m-l-5'></i>
                            </c:when>
                            <c:when test="${info.approvalStatus == 2}">
                                同意<i data-toggle='tooltip' data-placement='bottom' title='同意' class='f-s-16 fa fa-check-circle-o m-l-5'></i>
                            </c:when>
                            <c:when test="${info.approvalStatus == 3}">
                                否决<i data-toggle='tooltip' data-placement='bottom' title='否决' class='f-s-16 fa fa-times-circle m-l-5'></i>
                            </c:when>
                            <c:otherwise>
                                待申请<i data-toggle='tooltip' data-placement='bottom' title='待申请' class='f-s-16 fa fa-flickr m-l-5'></i>
                            </c:otherwise>
                        </c:choose></div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2" style="font-size: 12px;font-weight: normal;text-align: right;">审批记录:</label>
                        <div class=" col-md-10">
                            <div class="row">
                                <c:forEach items="${approvals}" var="approval" varStatus="status">
                                    <div class="col-md-2">
                                        第${status.count}次:
                                    </div>
                                    <div class="col-md-10">
                                        <c:forEach items="${approval.tbApprovalLogsByApprovalId}" var="log">
                                            <c:if test="${log.tbUserByUserId !=null}">
                                                <span>${log.tbUserByUserId.userName}</span></c:if>
                                            &nbsp;&nbsp;
                                            <span>${log.category}</span>
                                            &nbsp;&nbsp;<span>${log.logsContent}</span>&nbsp;&nbsp<span>${log.logsTime}</span>
                                            <br>
                                        </c:forEach>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <hr class="border-t-a">
                </div>
                <div class="col-md-12" style="margin-top: 50px">
                    <div class="form-group text-center">
                        <span class="text-left " style="cursor: pointer; padding: 8px 12px; border-radius:3px; background: #dddddd;margin:10px 0px; ">
					<i class="fa  fa-file"></i><span>上传附件</span>
				</span>
                    </div>
                </div>
        </div>

    </div>
        <div id="pro-bottom" class="text-right">
            <g:g id="363">
                <button class="btn btn-default" style="font-size: 12px;" data-toggle="modal" data-target="#delModal" onclick="deleteOpp(${info.quoteId})">
                    <i class="fa fa-trash-o" style="margin-right: 5px;" ></i>删除
                </button>&nbsp;
            </g:g>
            <g:g id="362">
                <button class="btn btn-default" style="font-size: 12px;" onclick="edit(${info.quoteId})">
                    <i class="fa fa-pencil" style="margin-right: 5px;" ></i>编辑
                </button>&nbsp;
            </g:g>
        </div>

    </div>

<div class="row" style="text-align: center;background-color: #F4F8FB; margin-top: 100px">
    <div class="col-md-12">
        <div class="form-group">
            <footer class="footer">热线:<b>4000-80-4000 400-960-9606 </b> &nbsp;&nbsp;网站:<b><a href="http://www.xtools.cn" target="_blank">www.xtools.cn</a></b> &nbsp;
                <a class="btn btn-danger btn-xs" href="#" onclick="window.open ('http://www.xtools.cn/about/tousu.html', 'newwindow', 'height=410, width=540,top=100,left=200;toolbar=no, menubar=no, scrollbars=no, resizable=no,status=no');return false;"><i class="fa fa-whatsapp m-r-5"></i>投诉&amp;问题</a>&nbsp;&nbsp;
                <a class="btn btn-default btn-xs" href="#" onclick="showWX(0);return false;"><i class="fa fa-weixin m-r-5"></i>微客服</a>&nbsp;&nbsp;
                <a class="btn btn-primary btn-xs" href="#" onclick="showWX(1);return false;"><i class="md md-speaker-notes m-r-5"></i>订阅号 </a>
                <br>Copyright © 2004-2018 &nbsp;北京沃力森信息技术有限公司&nbsp;&nbsp; Beijing Volitation Information Technology Co.,ltd</footer>
        </div>
    </div>
</div>
    <!--
 作者：1810761389@qq.com
 时间：2018-08-09
 描述：新增/编辑销售机会
-->
    <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-labelledby="open" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content mymodalcontent">

            </div>
        </div>
    </div>
    </div>
        <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
        <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
        <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
        <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="/js/resjs/bootstrap-tooltip.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.js"></script>
<%--<script src="//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>--%>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
        <!--excel-->
        <%--<script src="https://cdn.bootcss.com/iCheck/1.0.2/icheck.min.js"></script>--%>
        <script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
        <script src="/js/sweetalert.min.js"></script>
        <script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
        <script src="https://cdn.bootcss.com/bootstrap-validator/0.5.3/js/language/zh_CN.min.js"></script>
        <script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="/js/sale/formatter.js"></script>
<script type="text/javascript">
    $(function () {
        $("[data-toggle='tooltip' ] ").tooltip();
        $('#detail').load("/quoteDetail?id=${info.quoteId}");
        //清除模态框数据
        $("#add_modal").on("hidden.bs.modal", function () {
            open_modal = null;
            $(this).removeData('bs.modal');
            location.reload();
        });
    });
    //编辑
    function edit(id) {
        open_modal = id;
        $("#add_modal").modal({
            remote:'/quote_add'
        });
    }
    function successForm() {
        $('#add').modal('hide')
        window.history.go(0)
    }

    //删除
    function deleteOpp(pID) {
        swal({
                title: "确定删除吗？",
                text: "确定删除本数据吗？！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定",
                closeOnConfirm: false
            },
            function(){
                ajax("/quote/delete",function () {
                    swal("成功！", "产品删除成功！","success")
                    // $("#table111").bootstrapTable('refresh')
                    window.opener.location.reload();
                    self.close();
                },function(){},'get',data={'id':pID});
            });
    }

    function ajax(url,success,error,type,data) {
        $.ajax({
            url:url,
            timeout:3000,
            type:type,
            data:data,
            dataType:'json',
            success:function (result) {
                if (result.code != 200) {
                    swal(result.msg,"","error");
                    error()
                }
                else {
                    success(result.data);
                }
            },
            complete:function (XMLHttpRequest,status) {
                if (status != "success") {
                    swal("连接服务器错误","","error");
                    error()
                }
            }
        })
    }

</script>
</body>
</html>
