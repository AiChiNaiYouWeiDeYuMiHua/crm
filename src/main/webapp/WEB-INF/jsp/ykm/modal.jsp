<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/6
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 模态框（Modal）插件</title>
    <!--<link rel="stylesheet" href="bootstrap/css/bootstrap.css">-->
    <!--<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.css" />-->
    <!--<link rel="stylesheet" href="awesome-bootstrap-checkbox/css/awesome-bootstrap-checkbox.css">-->
    <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
    <link rel="stylesheet" href="bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.css" />
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <!--<link href="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    <link href="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="css/sale_start_modal.css" />
</head>

<body>

<h2>创建模态框（Modal）</h2>
<!-- 按钮触发模态框 -->
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#opp">
    开始演示模态框
</button>

<div class="container-fluid">
    <!--
        作者：1102137042@qq.com
        时间：2018-07-31
        描述：销售机会模态框
    -->
    <div class="modal fade" id="opp" tabindex="-1" role="dialog" aria-labelledby="oppLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content mymodalcontent">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-default pull-right">
                        <i class="fa fa-check"></i>
                        保存
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        销售机会
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="xsjh" action="https://www.baidu.com">
                        <div class="mybody">
                            <div class="text-center-b">
                                <span style="background-color: white; padding: 2px 10px;">机会</span>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>机会主题：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="opp_theme" class="form-control" required="required" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">更新日期：</label>
                                        <div class="col-md-8">
                                            <div class="form-control" disabled="disabled">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>客户：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="customer" class="form-control" required="required" style="background-color: white;" />
                                            <div class="" style="margin-top: 10px; margin-bottom: 10px;">
                                                <div class="form-group">
                                                    <input class="form-control" placeholder="拼音头.编号.关键字" style="padding-right: 12px; width: 120px; float: left; margin-right: 5px;" />

                                                    <button class="btn btn-default" style="float: left; margin-right: 5px;"><i class="fa fa-search"></i></button>
                                                    <a class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="浏览">
                                                        <i class="fa fa-list"></i>
                                                    </a>
                                                </div>
                                                <div class="form-group">

                                                </div>
                                                <!--<div class="form-group">
                                                    <input class="form-control" style="width: 140px;" />
                                                    <button class="btn btn-default"></button>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">状态：</label>
                                        <div class="col-md-8">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="option1" name="opp_status" checked>
                                                <label> 跟踪 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="option1" name="opp_status">
                                                <label> 成功 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="option1" name="opp_status">
                                                <label> 失败 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="option1" name="opp_status">
                                                <label> 搁置 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="option1" name="opp_status">
                                                <label> 失效 </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">基本情况</span></div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">客户联系人：</label>
                                        <div class="col-md-8">
                                            <select name="contact" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                                <option>option1</option>

                                                <option>option2</option>

                                                <option>option3</option>

                                                <option>option4</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">类型：</label>
                                        <div class="col-md-8" style="position: relative;">
                                            <select name="classification" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                                <option>option1</option>

                                                <option>option2</option>

                                                <option>option3</option>

                                                <option>option4</option>
                                            </select>
                                            <!--<a href="??" target="_blank" data-placement="bottom" title="数据字典" class="input-group-btn">
                                                <i class="fa fa-database"></i>
                                            </a>-->
                                            <a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">发现时间：</label>
                                        <div class="col-md-8">
                                            <div id="datetime_1" class="input-append date form_datetime input-group">
                                                <input name="discovery_time"  class="form-control" />
                                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">来源：</label>
                                        <div class="col-md-8">
                                            <select name="opp_source" class="selectpicker" data-live-search="true">
                                                <option selected></option>
                                                <option>option1</option>
                                                <option>option2</option>
                                                <option>option3</option>
                                                <option>option4</option>
                                            </select>
                                            <a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">负责人：</label>
                                        <div class="col-md-8">
                                        <select name="principal" class="selectpicker" data-live-search="true" data-live-search="true">
                                            <option selected></option>
                                            <option>option1</option>

                                            <option>option2</option>

                                            <option>option3</option>

                                            <option>option4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-6">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-4">提供人：</label>
                                    <div class="col-md-8">
                                        <input class="form-control"/>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-2">客户需求：</label>
                                    <div class="col-md-10">
                                        <textarea name="customer_demand" style="resize: vertical;" class="form-control" onclick="$(this).css('min-height','130px')"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">预期</span></div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-4">预计签单时间：</label>
                                    <div class="col-md-8">
                                        <!--<div id="datetime_2" class="input-append date form_datetime">
                                            <input type="text" value="" readonly class="form-control">
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>-->
                                        <div id="datetime_2" class="input-append date form_datetime input-group">
                                            <input name="expected_time"  class="form-control" />
                                            <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-4">可能性：</label>
                                    <div class="col-md-8">
                                        <select name="possibility" class="selectpicker" data-live-search="true" data-live-search="true">
                                            <option selected></option>
                                            <option>option1</option>

                                            <option>option2</option>

                                            <option>option3</option>

                                            <option>option4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-4">预期金额：</label>
                                    <div class="col-md-8">
                                        <input name="expected_amount" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-4">意向产品：</label>
                                    <div class="col-md-8">
                                        <select name="intend_product" class="selectpicker" data-live-search="true" multiple data-live-search="true" title="可多选">

                                            <option>option1</option>

                                            <option>option2</option>

                                            <option>option3</option>

                                            <option>option4</option>
                                        </select>
                                        <a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                            <i class="fa fa-book"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">当前状态</span></div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-6">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-4">优先：</label>
                                    <div class="col-md-8">
                                        <select name="priority" class="selectpicker" data-live-search="true" data-live-search="true">
                                            <option selected></option>
                                            <option>option1</option>

                                            <option>option2</option>

                                            <option>option3</option>

                                            <option>option4</option>
                                        </select>
                                        <a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                            <i class="fa fa-book"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-4">
                                        <span style="color: #ff0000; font-size: 16px;">*</span>阶段：
                                    </label>
                                    <div class="col-md-8">
                                        <select name="stage" class="selectpicker"  data-live-search="true">
                                            <option selected></option>
                                            <option>option1</option>

                                            <option>option2</option>

                                            <option>option3</option>

                                            <option>option4</option>
                                        </select>
                                        <a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                            <i class="fa fa-book"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-12">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-2">阶段备注：</label>
                                    <div class="col-md-10">
                                        <input name="stage_remarks" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-12">
                                <div class="form-group overflow">
                                    <label class="control-label col-md-2">阶段停留：</label>
                                    <div class="col-md-10">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-12">
                                <div>
                                    <label class="control-label col-md-2">跟踪日志：</label>
                                    <div class="col-md-10">
                                        <div class="form-control" style="height: inherit; min-height: 36px;" disabled="disabled">
                                            日期:2017-10-31　阶段:初期沟通(10%)　Aima　预计:2019-12-31(￥0.00)　备注:
                                            <br />
                                            日期:2017-10-31　阶段:初期沟通(10%)　Aima　预计:2019-12-31(￥60000.00)　备注:
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="height: 50px; "></div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-default" type="submit" id="mysubmit"><i class="fa fa-check"></i>
                    保存</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal -->
</div>

</div>

<script src="js/jquery-3.3.1.min.js "></script>
<!--<script src="bootstrap/js/bootstrap.js "></script>-->
<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.js"></script>
<script type="text/javascript " src="bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js "></script>
<script type="text/javascript " src="bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.fr.js "></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.js"></script>
<!--<script src="https://cdn.bootcss.com/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>-->
<script src="https://cdn.bootcss.com/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<script src="js/select.js"></script>
<script>

    // getOption();
    $(function() {

        $("#xsjh").bootstrapValidator({

            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                opp_theme: {
                    msg:'机会主题验证失败',
                    validators: {
                        notEmpty: {
                            message: '机会主题不能为空'
                        },stringLength: {
                            min: 3,
                            message: '机会主题不能少于三个字'
                        }
                    }
                },customer: {
                    msg:'客户验证失败',
                    validators: {
                        notEmpty: {
                            message: '客户不能为空'
                        }
                    }
                },stage: {
                    msg:'阶段验证失败',
                    validators: {
                        notEmpty: {
                            message: '请选择阶段'
                        }
                    }
                }/*,contract: {
			                  msg:'用户名验证失败',
			                  validators: {
			                      notEmpty: {
			                          message: '用户名不能为空'
			                      },
			                      regexp: {
			                          regexp: /^[\u4e00-\u9fa5]{2,5}$/,
			                          message: '用户名2-5个汉字'
			                      }
			                  }
			              }*/
            }
        });

        $("#mysubmit").click(function(){
            $("#xsjh").bootstrapValidator('validate');
        });

        /*$(window).on('load', function() {

            $('.selectpicker').selectpicker({
                'selectedText': 'cat'
            });

            // $('.selectpicker').selectpicker('hide');
        });*/

        /*$(".selectpicker").selectpicker({
            noneSelectedText : '请选择'
        });

        $(window).on('load', function() {
            $('.selectpicker').selectpicker('val', '');
            $('.selectpicker').selectpicker('refresh');
        });

        //下拉数据加载
        $.ajax({
            type : 'get',
            url : "test_select",
            dataType : 'json',
            success : function(datas) {//返回list数据并循环获取
                alert(datas);
                var select = $(".selectpicker");
                for (var i = 0; i < datas.length; i++) {
                    select.append("<option value='"+datas[i]+"'>"
                         + "</option>");
                }
                $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });*/

        $("[data-toggle='tooltip' ] ").tooltip();
        $("#datetime_1").datetimepicker({
            format: "yyyy年mm月dd日",
            minView: "month",//设置只显示到月份
            startView:2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_2").datetimepicker({
            format: "yyyy年mm月dd日",
            minView: "month",//设置只显示到月份
            startView:2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });
    });
</script>

</body>

</html>