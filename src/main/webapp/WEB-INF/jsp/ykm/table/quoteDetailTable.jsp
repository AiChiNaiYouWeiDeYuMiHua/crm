<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/21
  Time: 14:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>
<html>
<head>
    <style>
        html,
        body {
            color: #333333;
            background-color: #FFFFFF;
            font-size: 12px;
            font-family: "Microsoft Yahei", 'Noto Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        }
        .table-t thead{

        }
    </style>
</head>
<body>
<div class="col-md-12">
    <div class="form-group">
        <label class="col-md-12" style="font-size: 12px;font-weight: normal;text-align: right;">
            <g:g id="365">
                <a class="f-s-12 text-black" target="_blank" href="/quote/to_quoteDetail?id=${id}"  style="margin-right: 10px;color: black;text-decoration: none;"><i class="fa fa-edit"></i>编辑明细</a>
            </g:g>
        </label>
    </div>
</div>
<div class="col-md-12" style="padding-left: 80px;">
    <div class="form-group">
        <table id="table1" class="table-t" style="font-size: 12px">
            <thead style="background-color: #EBEFF2;font-family: 楷体;">
            <tr>
            </tr>
            </thead>
        </table>

    </div>
</div>
<script>
    $(function() {
        $("#table1").bootstrapTable({
            method: 'get',
            striped: true,
            cache: false,
            pagination: false,
            sortable: false,
            sortOrder: "desc",
            pageNumber: 1,
            pageSize: 10,
            url: "/quote/detail?id=${id}",
            sidePagination: "server",
            queryParamsType: '',

            columns: [{
                field: 'productName',
                title: '品名',
                align: 'center',
                formatter:isnull
//			footerFormatter: "合计"
            }, {
                field: 'productModal',
                title: '型号',
                align: 'center',
                formatter:isnull
            }, {
                field: 'pfName',
                title: '规格',
                align: 'center',
                formatter:isnull
            }, {
                field: 'pfUnit',
                title: '单位',
                align: 'center',
                formatter:isnull
            }, {
                field: 'amount',
                title: '数量',
                align: 'center',
                formatter:isnull
//			footerFormatter: "合计"
            }, {
                field: 'unitPrice',
                title: '单价',
                align: 'center',
                formatter:function (value,row,index) {
                    if (value != null)
                        return fmoney(value,2);
                    else
                        return "";
                }
            }, {
                field: 'money',
                title: '金额',
                align: 'center',
                formatter:function (value,row,index) {
                    if (value != null)
                        return fmoney(value,2);
                    else
                        return "";
                }
            }, {
                field: 'remarks',
                title: '备注',
                align: 'center',
                formatter:isnull
            },{
                field: 'unitPrice',
                title: '折扣',
                align: 'center',
                formatter:function (value,row,index) {
                    if (value != null ) {

                        var add = (-1*(row.amount*row.unitPrice-row.money));
                        if (row.amount*row.unitPrice == 0 || add == 0)
                            return "";
                        if (add > 0)
                            return "<span style='color: red'>"+fmoney(add,2)+"</span>("+100*(row.money/(row.amount*row.unitPrice))+"%)";
                        else
                            return "<span style='color: red'>"+fmoney(add,2)+"</span>("+100*(row.money/(row.amount*row.unitPrice))+"%)";
                    }else
                        return "";
                }
//			footerFormatter: "合计"
            }],
            onLoadSuccess: function(data) {
                if (data.length > 0){
                    var total = 0;
                    var n = 0;
                    for (var i =0;i<data.length;i++) {
                        total += parseFloat(data[i].money == undefined ? 0 : data[i].money);
                        n += parseFloat(data[i].amount == undefined ? 0 : data[i].amount)
                    }
                    addFotterYkm($('#table1'), {
                        'productName': '合计',
                        amount:n,
                        money: fmoney(total,2),
                    },function () {
                        var totalMoney = fmoneyYkm(total,2);
                        if(parseFloat($("#totalQuote").text()) != parseFloat(fmoneyYkm(total,2))){
                            $("#totalQuote").append("<img src=\"/img/warn16.png\" alt=\"警告\" title=\"警告\">")
                                .append("<font color=\"#FF6666\">与明细合计(<b><span style=\"white-space:nowrap\"><span style=\"font-size:9pt;font-weight:normal;\">￥</span>"+totalMoney+"</span></b>)不符</font>")
                        }
                    })

                    addFotter($('#table1'), {
                        'productName': '合计(大写金额)',
                        remarks: digit_uppercase(total),
                    });
                    formatterFotter($('#table1'), 'amount', 1, 2);
                    formatterFotter($('#table1'), 'remarks', 1, 1);
                }
            },
            onLoadError: function(status) {
            }
        })


        //格式化

        //编辑时间

    })
    function addFotter(table, json) {
        table.bootstrapTable('append', json);
    }
    
    function addFotterYkm(table, json, check) {
        table.bootstrapTable('append', json);
        check();
    }

    function formatterFotter(table, field, colspan, num) {
        table.bootstrapTable('mergeCells', {
            index: getLength(table) - num + 1,
            field: field,
            colspan: colspan
        });
    }

    function getLength(table) {
        return table.bootstrapTable('getData', true).length - 1;
    }
    function isnull(value,row,index) {
        if (value == null)
            return "";
        else
            return value;
    }

    function fmoneyYkm(s, n)
    {
        if (s == null || s.toString().length <0)
            return "";
        n = n > 0 && n <= 20 ? n : 2;
        s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
        var l = s.split(".")[0].split("").reverse(),
            r = s.split(".")[1];
        t = "";
        for(i = 0; i < l.length; i ++ )
        {
            t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "" : "");
        }
        return t.split("").reverse().join("") + "." + r;
    }

</script>
</body>
</html>

