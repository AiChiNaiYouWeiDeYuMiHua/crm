<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/10
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>

<body>


<!--
    作者：1102137042@qq.com
    时间：2018-07-31
    描述：高级查询模态框
-->

<div class="modal-header" style="border-bottom: none;">
    <button type="button" class="btn btn-default pull-right" id="reset-btns">
        <i class="fa fa-check"></i>
        重置
    </button>
    <h4 class="modal-title" id="myModalLabel">
        高级查询
    </h4>
    <hr class="boder-t-a" />
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="search-form" class="form-horizontal">
        <div class="mybody">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            ID：
                        </label>
                        <div class="col-md-6">
                            <input name="qdId" class="form-control" type="number" onkeyup= "if(! /^-?[1-9]+[0-9]*$/.test(this.value)){alert('只能整数');this.value='';}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">数量：</label>
                        <div class="col-md-6">
                            <div class="input-append date form_datetime input-group">
                                <span class="input-group-addon bg-d b-0">&nbsp;&nbsp;&nbsp;>=&nbsp;&nbsp;&nbsp;</span>
                                <input name="amount" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">单价：</label>
                        <div class="col-md-6">
                            <div class="input-append date form_datetime input-group">
                                <span class="input-group-addon bg-d b-0">&nbsp;&nbsp;&nbsp;>=&nbsp;&nbsp;&nbsp;</span>
                                <input name="unitPrice" class="form-control" onKeyUp="amount(this)" onBlur="overFormat(this)"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">金额：</label>
                        <div class="col-md-6">
                            <div class="input-append date form_datetime input-group">
                                <span class="input-group-addon bg-d b-0">&nbsp;&nbsp;&nbsp;>=&nbsp;&nbsp;&nbsp;</span>
                                <input name="money" class="form-control" onKeyUp="amount(this)" onBlur="overFormat(this)"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4 ">
                            日期：
                        </label>
                        <div class="col-md-3" style="padding-right: 0">
                            <div id="datetime_1" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="开始日期" name="createTimeFrom"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
                                            </span>
                            </div>

                        </div>

                        <div class="col-md-3" style="padding-left: 0">
                            <div id="datetime_2" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="结束日期" name="createTimeTo"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: 50px; "></div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
    <button class="btn btn-default" onclick="search_btn()" type="submit" id="search-senior"><i class="fa fa-check"></i>
        搜索</button>
</div>
<script>

    $(function() {

        $("#datetime_1").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_1"
        });
        $("#datetime_2").datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_2"
        });


        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        $('.selectpicker').selectpicker('refresh');

        $('.bootstrap-select').css('min-width','213px');

        $('#reset-btns').click(function () {
            $('#search-form')[0].reset();
            $('.selectpicker1').selectpicker('val', '未选');
        })

        $("[data-toggle='tooltip' ] ").tooltip();

    });

    function search_btn() {
        search2();
        $('#search-senior').modal("hide")
        // $('#search-form')[0].reset();
    }


</script>

</body>

</html>

