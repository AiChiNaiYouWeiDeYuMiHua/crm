<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/7
  Time: 22:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 模态框（Modal）插件</title>
</head>

<body>


    <!--
        作者：1102137042@qq.com
        时间：2018-07-31
        描述：销售机会模态框
    -->



        <%--<div class="">--%>
            <%--<div class="modal-content mymodalcontent">--%>
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-default pull-right">
                        <i class="fa fa-check"></i>
                        保存
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        销售机会
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="xsjh">
                        <div class="mybody">
                            <div class="text-center-b">
                                <span style="background-color: white; padding: 2px 10px;">机会</span>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>机会主题：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="oppTheme" class="form-control" required="required" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">更新日期：</label>
                                        <div class="col-md-8">
                                            <div class="form-control" disabled="disabled" id="updateTime">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input name="serveId" style="display: none">
                                        <label class="control-label col-md-2" style="padding-right: 20px;">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                                        </label>
                                        <div class="input-group col-md-10" style="padding-left: 10px;">
                                            <input type="text" name="tbCustomerByCusId.cusId" class="form-control"
                                                   style="display: none">
                                            <input type="text" disabled class="form-control" id="to_cus" required="required">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default" id="btn_cus_choose"
                                                        style="color: black;height: 34px" onclick="chooseCus()"><i
                                                        class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">状态：</label>
                                        <div class="col-md-8">
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="1" name="oppStatus" checked>
                                                <label> 跟踪 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="2" name="oppStatus">
                                                <label> 成功 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="3" name="oppStatus">
                                                <label> 失败 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="4" name="oppStatus">
                                                <label> 搁置 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" value="5" name="oppStatus">
                                                <label> 失效 </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">基本情况</span></div>

                            <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-md-6">
                                        <div class="form-group overflow">
                                            <label class="control-label col-md-4" style="padding-left:0">
                                                客户联系人：
                                            </label>
                                            <div class="col-md-8">
                                                <select name="tbContactsBy联系人Id.cotsId" class="selectpicker" data-live-search="true"
                                                        data-live-search="true" id="cotsName">
                                                    <option selected style="height: 26px"></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">类型：</label>
                                        <div class="col-md-8" style="position: relative;">
                                            <select name="classification" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected style="height: 26px"></option>
                                            </select>
                                            <%--<a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">发现时间：</label>
                                        <div class="col-md-8">
                                            <div class="input-append date form_datetime input-group">
                                                <input name="discoveryTime"  class="form-control" type="date" />
                                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">来源：</label>
                                        <div class="col-md-8">
                                            <select name="oppSource" class="selectpicker" data-live-search="true">
                                                <option selected></option>
                                            </select>
                                            <%--<a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input name="serveId" style="display: none">
                                        <label class="control-label col-md-2" style="padding-right: 20px;">
                                            负责人：
                                        </label>
                                        <div class="input-group col-md-10" style="padding-left: 10px;">
                                            <input type="text" name="tbUserByUserId.userId" class="form-control"
                                                   style="display: none">
                                            <input type="text" class="form-control" id="to_user" disabled>
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default"
                                                        style="color: black;height: 34px" onclick="chooseCus1()"><i
                                                        class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">客户需求：</label>
                                        <div class="col-md-10">
                                            <textarea name="customerDemand" style="resize: vertical;" class="form-control" onclick="$(this).css('min-height','130px')"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">预期</span></div>
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">预计签单时间：</label>
                                        <div class="col-md-8">
                                            <div  class="input-append date form_datetime input-group">
                                                <input name="expectedTime"  class="form-control" type="date" />
                                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">可能性：</label>
                                        <div class="col-md-8">
                                            <select name="possibility" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                                <option>0%</option>
                                                <option>10%</option>
                                                <option>20%</option>
                                                <option>30%</option>
                                                <option>40%</option>
                                                <option>50%</option>
                                                <option>60%</option>
                                                <option>70%</option>
                                                <option>80%</option>
                                                <option>90%</option>
                                                <option>100%</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">预期金额：</label>
                                        <div class="col-md-8">
                                            <input name="expectedAmount" class="form-control" onKeyUp="amount(this)" onBlur="overFormat(this)" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">意向产品：</label>
                                        <div class="col-md-8">
                                            <select name="intendProduct" class="selectpicker" data-live-search="true" multiple data-live-search="true" title="可多选">

                                            </select>
                                            <%--<a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center-b"><span style="background-color: white; padding: 2px 10px;">当前状态</span></div>
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">优先：</label>
                                        <div class="col-md-8">
                                            <select name="priority" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                                <option value="1">一级</option>
                                                <option value="2">二级</option>
                                                <option value="3">三级</option>
                                                <option value="4">四级</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>阶段：
                                        </label>
                                        <div class="col-md-8">
                                            <select name="stage" class="selectpicker"  data-live-search="true" required>
                                                <option selected></option>
                                            </select>
                                            <%--<a href="??" target="_blank" data-placement="bottom" title="数据字典" style="font-size: 16px; position: absolute;left: 105%; top: 10%;">
                                                <i class="fa fa-book"></i>
                                            </a>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">阶段备注：</label>
                                        <div class="col-md-10">
                                            <input name="stageRemarks" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">阶段停留：</label>
                                        <div class="col-md-10">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div>
                                        <label class="control-label col-md-2">跟踪日志：</label>
                                        <div class="col-md-10">
                                            <div class="form-control" style="height: inherit; min-height: 36px;" disabled="disabled" id="trackLog">
                                                <%--日期:2017-10-31　阶段:初期沟通(10%)　Aima　预计:2019-12-31(￥0.00)　备注:
                                                <br />
                                                日期:2017-10-31　阶段:初期沟通(10%)　Aima　预计:2019-12-31(￥60000.00)　备注:--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 50px; "></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
                    <button class="btn btn-default" type="button" id="mysubmit"><i class="fa fa-check"></i>
                        保存</button>
                </div>
            <%--</div>--%>
            <%--<!-- /.modal-content -->--%>
        <%--</div>--%>

<script>

    $(function() {

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        $('.selectpicker').selectpicker('refresh');

        $('.bootstrap-select').css('min-width','213px');

        //下拉数据加载
        $.ajax({
            type: 'post',
            url: "/get_dimission?typeId=212",
            dataType: 'json',
            // async : false,
            success: function (datas) {//返回list数据并循环获取
                var select = $('select[name="oppSource"]');
                for (var i = 0; i < datas.data.length; i++) {
                    select.append("<option>"
                        + datas.data[i].dimissionName + "</option>");
                }
                // $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });
        $.ajax({
            type: 'post',
            url: "/get_dimission?typeId=213",
            dataType: 'json',
            // async : false,
            success: function (datas) {//返回list数据并循环获取
                var select = $('select[name="intendProduct"]');
                for (var i = 0; i < datas.data.length; i++) {
                    select.append("<option>"
                        + datas.data[i].dimissionName + "</option>");
                }
                // $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });
        $.ajax({
            type: 'post',
            url: "/get_dimission?typeId=214",
            dataType: 'json',
            // async : false,
            success: function (datas) {//返回list数据并循环获取
                var select = $('select[name="stage"]');
                // select.append("<option>")
                for (var i = 0; i < datas.data.length; i++) {
                    select.append("<option>"
                        + datas.data[i].dimissionName + "</option>");
                }
                // $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });
        $.ajax({
            type: 'post',
            url: "/get_dimission?typeId=211",
            dataType: 'json',
            // async : false,
            success: function (datas) {//返回list数据并循环获取
                var select = $('select[name="classification"]');
                // select.append("<option>")
                for (var i = 0; i < datas.data.length; i++) {
                    select.append("<option>"
                        + datas.data[i].dimissionName + "</option>");
                }
                // $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });
        setForm();

        $("#xsjh").bootstrapValidator({

            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                oppTheme: {
                    msg:'机会主题验证失败',
                    validators: {
                        notEmpty: {
                            message: '机会主题不能为空'
                        },stringLength: {
                            min: 3,
                            message: '机会主题不能少于三个字'
                        }
                    }
                }
            },fields: {
                'tbCustomerByCusId.cusId': {
                    msg:'阶段验证失败',
                    validators: {
                        notEmpty: {
                            message: '阶段不能为空'
                        }
                    }
                }
            },fields: {
                stage: {
                    msg:'阶段验证失败',
                    validators: {
                        notEmpty: {
                            message: '阶段不能为空'
                        }
                    }
                }
            }
        });
        //提交表单数据
        $("#mysubmit").on("click", function(){
            if($('input[name="tbCustomerByCusId.cusId"]').val() == ""){
                swal("客户很重要的亲！","","error");
                return;
            }
            //获取表单对象
            var bootstrapValidator = $("#xsjh").data('bootstrapValidator');
            //手动触发验证
            bootstrapValidator.validate();
            if(bootstrapValidator.isValid()){
                $.ajax({
                    url: '/sale_opp_addmethod',
                    type: 'post',
                    async: false,
                    data: $("#xsjh").serialize(),
                    success: function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            $('#add_sale_opp').modal('hide')
                            $('#add_modal').modal('hide')
                            $('#sale_modal').modal('hide')
                            //表单重置
                            $("#xsjh")[0].reset();
                            $("#table111").bootstrapTable('refresh')
                        }
                    },
                    error: function (data) {
                        swal("连接服务器错误","","error");
                    }
                });
            }
        });

        $("[data-toggle='tooltip' ] ").tooltip();

    });

    function chooseCus() {
        window.open("/customer_check");
    }

    function chooseCus1() {
        // window.open("/admin/to_user_check");
        var a = $("<a href='/admin/to_user_check' target='_blank'>Apple</a>").get(0);
        var e = document.createEvent('MouseEvents');
        e.initEvent( 'click', true, true );
        a.dispatchEvent(e);
    }

    function getUser(userId, userName) {
        $("input[name='tbUserByUserId.userId']").val(userId);
        $("#to_user").val(userName);
    }

    function getCus(cusId,cusName) {
        $("input[name='tbCustomerByCusId.cusId']").val(cusId);
        $("#to_cus").val(cusName);
        $.ajax({
            url: '/memorial/get_contact?id=' + $("input[name='tbCustomerByCusId.cusId']").val(),
            method: 'get',
            async: true,
            success: function (data1) {
                $("#cotsName").empty();
                $("#costName").append("<option selected></option>")
                for (var i = 0; i < data1.length; i++) {
                    $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                    $('#cotsName').selectpicker('refresh');
                }
                $('#cotsName').selectpicker('refresh');
            }
        });
    }

    //表单初始化
    function setForm() {
        $("input[name='tbUserByUserId.userId']").val('${sessionScope.user.userId}');
        $("#to_user").val('${sessionScope.user.userName}');
        if (open_modal != null) {
            $("#btn_cus_choose").attr("disabled","disabled");
            ajax("/sale_opp_load?oppId="+open_modal,function (data) {
                $('#xsjh').append("<input class='hidden' name='oppId' value='"+data.oppId+"'>")
                $('input[name="oppTheme"]').val(data.oppTheme)
                $('#updateTime').html(data.updateTime)
                if(data.tbCustomerByCusId != null){
                    $('input[name="tbCustomerByCusId.cusId"]').val(data.tbCustomerByCusId.cusId)
                    $("#to_cus").val(data.tbCustomerByCusId.cusName);
                    $.ajax({
                        url: '/memorial/get_contact?id=' + $("input[name='tbCustomerByCusId.cusId']").val(),
                        method: 'get',
                        async: true,
                        success: function (data1) {
                            $("#cotsName").empty();
                            $('#costName').append("<option selected></option>")
                            for (var i = 0; i < data1.length; i++) {
                                $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                                $('#cotsName').selectpicker('refresh');
                            }
                            $('#cotsName').selectpicker('refresh');
                        }
                    });
                }
                redio($('input[name="oppStatus"]'),data.oppStatus);
                if(data.tbContactsBy联系人Id != null){
                    $('#cotsName').selectpicker('val',data.tbContactsBy联系人Id.cotsId);
                }
                $('select[name="classification"]').selectpicker('val',data.classification)
                $('input[name="discoveryTime"]').val(data.discoveryTime)
                $('select[name="oppSource"]').selectpicker('val',data.oppSource)
                if(data.tbUserByUserId != null){
                    $('select[name="tbUserByUserId.userId"]').selectpicker('val',data.tbUserByUserId.userId);
                }
                $('textarea[name="customerDemand"]').val(data.customerDemand)
                $('select[name="possibility"]').selectpicker('val',data.possibility)
                $('input[name="expectedAmount"]').val(data.expectedAmount)
                $('input[name="expectedTime"]').val(data.expectedTime)
                if(data.intendProduct != null && data.intendProduct.search(',') != -1){
                    $('select[name="intendProduct"]').selectpicker('val',data.intendProduct.split(','))
                } else {
                    $('select[name="intendProduct"]').selectpicker('val',data.intendProduct)
                }
                $('select[name="priority"]').selectpicker('val',data.priority)
                $('select[name="stage"]').selectpicker('val',data.stage)
                $('input[name="stageRemarks"]').val(data.stageRemarks)
                $('#trackLog').html(data.trackLog)

            },function () {
                swal("加载失败",result.msg,"error");
                $('#add_sale_opp').modal('hide')
            },"get",{})
        }
    }

    function redio(dom,is) {
        dom.eq(0).removeProp('checked');
        dom.eq(1).removeProp('checked');
        dom.eq(2).removeProp('checked');
        dom.eq(3).removeProp('checked');
        dom.eq(4).removeProp('checked');
        if (is == "1"){
            dom.eq(0).prop('checked', 'checked');
        }else if(is == "2"){
            dom.eq(1).prop('checked', 'checked');
        }else if(is == "3"){
            dom.eq(2).prop('checked', 'checked');
        }else if(is == "4"){
            dom.eq(3).prop('checked', 'checked');
        }else
            dom.eq(4).prop('checked', 'checked');
    }

</script>

</body>

</html>
