<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/7
  Time: 22:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 模态框（Modal）插件</title>
</head>

<body>


    <!--
        作者：1102137042@qq.com
        时间：2018-07-31
        描述：详细需求模态框
    -->

                <div class="modal-header" style="border-bottom: none;">
                    <%--<button type="button" class="btn btn-default pull-right mysubmit" data-dismiss="modal">
                        <i class="fa fa-check"></i>
                        保存
                    </button>--%>
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">关闭</button>
                    <h4 class="modal-title" id="myModalLabel">
                        详细需求
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="xsjh">
                        <div class="mybody">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>需求主题：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="demandTheme" class="form-control" required="required" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">记录时间：</label>
                                        <div class="col-md-8">
                                            <div class="input-append date form_datetime input-group">
                                                <input name="recordTime"  class="form-control" type="date" />
                                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input name="serveId" style="display: none">
                                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                                <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                                            </label>
                                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                                <input type="text" id="cus" name="tbSaleOppByOppId.tbCustomerByCusId.cusId" class="form-control"
                                                       style="display: none">
                                                <input type="text" disabled class="form-control" id="to_cus">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-default" id="btn-cus"
                                                            style="color: black;height: 34px" onclick="chooseCus()"><i
                                                            class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4"><span style="color: #ff0000; font-size: 16px;">*</span>对应机会：</label>
                                        <div class="col-md-8">
                                            <select name="tbSaleOppByOppId.oppId" id="oppId" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">重要程度：</label>
                                        <div class="col-md-8">
                                            <select name="importance" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                                <option value="1">非常重要</option>
                                                <option value="2">重要</option>
                                                <option value="3">一般重要</option>
                                                <option value="4">不重要</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">客户需求：</label>
                                        <div class="col-md-10">
                                            <textarea name="demandContent" style="resize: vertical;" class="form-control" onclick="$(this).css('min-height','130px')"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="height: 50px; "></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <%--<button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>--%>
                    <button class="btn btn-default" type="button" id="mysubmit"><i class="fa fa-check"></i>
                        保存</button>
                </div>
            <%--</div>--%>
            <%--<!-- /.modal-content -->--%>
        <%--</div>--%>

<script>
    var ykm_cusId;
    $(function() {
        setForm();
        //客户对应的销售机会下拉数据加载
        $("#cus").blur(function () {
            var cusId = $(this).val();
            $.ajax({
                type : 'post',
                url : "/getOppByCusId?cusId="+cusId,
                dataType : 'json',
                success : function(datas) {//返回list数据并循环获取
                    var select = $("#oppId");
                    $("#oppId").empty();
                    select.append("<option selected></option>");
                    for (var i = 0; i < datas.length; i++) {
                        select.append("<option value='"+datas[i].oppId+"'>"
                            + datas[i].oppTheme + "</option>");
                    }
                    // $('.selectpicker').selectpicker('val', '');
                    $('.selectpicker').selectpicker('refresh');
                }
            });
        });

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        // $('.selectpicker').selectpicker('refresh');

        $('.bootstrap-select').css('min-width','213px');

        /*$("#oppId").selectpicker({
            width: 50
        });*/


        $("#xsjh").bootstrapValidator({

            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                demandTheme: {
                    msg:'需求主题验证失败',
                    validators: {
                        notEmpty: {
                            message: '需求主题不能为空'
                        },stringLength: {
                            min: 3,
                            message: '机会主题不能少于三个字'
                        }
                    }
                }
            },fields: {
                "tbSaleOppByOppId.oppId": {
                    msg:'机会验证失败',
                    validators: {
                        notEmpty: {
                            message: '机会不能为空'
                        }
                    }
                }
            }
        });
        //提交表单数据
        $("#mysubmit").on("click", function(){
            //获取表单对象
            var bootstrapValidator = $("#xsjh").data('bootstrapValidator');
            //手动触发验证
            bootstrapValidator.validate();
            if(bootstrapValidator.isValid()){
                $('select[name="tbSaleOppByOppId.oppId"]').removeAttr("disabled");
                $.ajax({
                    url: '/demand/demand_addMethod',
                    type: 'post',
                    async: false,
                    data: $("#xsjh").serialize(),
                    success: function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            $('#add_modal').modal('hide')
                            $("#load_modal").modal('hide')
                            //表单重置
                            $("#xsjh")[0].reset();
                            $("#table111").bootstrapTable('refresh')
                        } else {
                            swal(result.msg,"","error");
                        }
                    },
                    error: function (data) {
                        swal("连接服务器错误","","error");
                    }
                });
            }
        });

        $("[data-toggle='tooltip' ] ").tooltip();

    });

    function cusToOpp() {
        var cusId = $("input[name='tbSaleOppByOppId.tbCustomerByCusId.cusId']").val();
        $.ajax({
            type : 'post',
            url : "/getOppByCusId?cusId="+cusId,
            dataType : 'json',
            async: false,
            success : function(datas) {//返回list数据并循环获取
                var select = $("#oppId");
                $("#oppId").empty();
                select.append("<option selected></option>");
                for (var i = 0; i < datas.length; i++) {
                    if(datas[i].oppTheme == undefined){
                        select.append("<option value='"+datas[i].oppId+"'>"
                            + "无主题" + "</option>");
                    } else {
                        select.append("<option value='"+datas[i].oppId+"'>"
                            + datas[i].oppTheme + "</option>");
                    }
                }
                // $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });
    }

    function chooseCus() {
        window.open("/customer_check");
    }

    function getCus(cusId,cusName) {
        $("input[name='tbSaleOppByOppId.tbCustomerByCusId.cusId']").val(cusId);
        cusToOpp();
        // alert("id:"+cusId+"name:"+cusName)
        $("#to_cus").val(cusName);
        $.ajax({
            url: '/memorial/get_contact?id=' + $("input[name='tbSaleOppByOppId.tbCustomerByCusId.cusId']").val(),
            method: 'get',
            async: true,
            success: function (data1) {
                $("#cotsName").empty();
                for (var i = 0; i < data1.length; i++) {
                    $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                    $('#cotsName').selectpicker('refresh');
                }
                $('#cotsName').selectpicker('refresh');
            }
        });
    }

    //表单初始化
    function setForm() {
        if (open_modal != null) {
            $("#btn-cus").attr("disabled","disabled")
            ajax("/demand/demand_load?id="+open_modal,function (data) {
                $('#xsjh').append("<input class='hidden' name='demandId' value='"+data.demandId+"'>")
                $('input[name="demandTheme"]').val(data.demandTheme)
                $('input[name="recordTime"]').val(data.recordTime)
                if(data.tbSaleOppByOppId != null && data.tbSaleOppByOppId.tbCustomerByCusId != null){
                    $('input[name="tbSaleOppByOppId.tbCustomerByCusId.cusId"]').val(data.tbSaleOppByOppId.tbCustomerByCusId.cusId)
                    $('#to_cus').val(data.tbSaleOppByOppId.tbCustomerByCusId.cusName)
                }
                $('select[name="importance"]').selectpicker('val',data.importance)
                if(data.tbSaleOppByOppId != null){
                    //客户对应的销售机会下拉数据加载
                    cusToOpp();
                    $('select[name="tbSaleOppByOppId.oppId"]').selectpicker('val',data.tbSaleOppByOppId.oppId)

                }
                $('textarea[name="demandContent"]').val(data.demandContent)
                if(data.tbSaleOppByOppId != null){
                    $('select[name="tbSaleOppByOppId.oppId"]').selectpicker('val',data.tbSaleOppByOppId.oppId)

                }
            },function () {
                swal("加载失败",result.msg,"error");
                $('#add_sale_opp').modal('hide')
            },"get",{})
        }  if (ykm_cusId != null){
            $('input[name="tbSaleOppByOppId.tbCustomerByCusId.cusId"]').val(ykm_cusId)
            $("#btn-cus").attr("disabled","disabled")
            $('#to_cus').val(ykm_cusName);
            cusToOpp();
            $('select[name="tbSaleOppByOppId.oppId"]').selectpicker('val',ykm_oppId)
            $('select[name="tbSaleOppByOppId.oppId"]').attr("disabled","disabled")
        }

    }



</script>

</body>

</html>
