<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/10
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>

<body>


<!--
    作者：1102137042@qq.com
    时间：2018-07-31
    描述：高级查询模态框
-->

<div class="modal-header" style="border-bottom: none;">
    <button type="button" class="btn btn-default pull-right" id="reset-btns">
        <i class="fa fa-check"></i>
        重置
    </button>
    <h4 class="modal-title" id="myModalLabel">
        高级查询
    </h4>
    <hr class="boder-t-a" />
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="search-form" class="form-horizontal">
        <div class="mybody">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            ID：
                        </label>
                        <div class="col-md-6">
                            <input name="saleOppEntity.oppId" class="form-control" type="number" onkeyup= "if(! /^-?[1-9]+[0-9]*$/.test(this.value)){alert('只能整数');this.value='';}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">客户：</label>
                        <div class="col-md-6">
                            <input name="saleOppEntity.tbCustomerByCusId.cusName" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">客户联系人：</label>
                        <div class="col-md-6">
                            <input name="saleOppEntity.tbContactsBy联系人Id.cotsName" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">机会主题：</label>
                        <div class="col-md-6">
                            <input name="saleOppEntity.oppTheme" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <%--<div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">发现时间：</label>
                        <div class="col-md-8">
                            <input class="form-control" placeholder="开始日期" name="discoveryTimeFrom" type="date">
                            <span class="">&nbsp;到&nbsp;</span>
                            <input class="form-control" placeholder="结束日期" name="discoveryTimeTo" type="date">
                        </div>
                    </div>
                </div>
            </div>--%>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4 ">
                            发现时间：
                        </label>
                        <div class="col-md-3" style="padding-right: 0">
                            <div id="datetime_1" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="开始日期" name="discoveryTimeFrom"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
                                            </span>
                            </div>

                        </div>

                        <div class="col-md-3" style="padding-left: 0">
                            <div id="datetime_2" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="结束日期" name="discoveryTimeTo"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4 ">
                            预计签单时间：
                        </label>
                        <div class="col-md-3" style="padding-right: 0">
                            <div id="datetime_3" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="开始日期" name="expectedTimeFrom"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
                                            </span>
                            </div>

                        </div>

                        <div class="col-md-3" style="padding-left: 0">
                            <div id="datetime_4" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="结束日期" name="expectedTimeTo"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--<div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">预计签单日期：</label>
                        <div class="col-md-8">
                            <input class="form-control" placeholder="开始日期" name="expectedTimeFrom" type="date">
                            <span class="">&nbsp;到&nbsp;</span>
                            <input class="form-control" placeholder="结束日期" name="expectedTimeTo" type="date">
                        </div>
                    </div>
                </div>
            </div>--%>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">预期金额：</label>
                        <div class="col-md-6">
                            <input name="saleOppEntity.expected_amount" class="form-control" type="number">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">可能性：</label>
                        <div class="col-md-6">
                            <input name="saleOppEntity.possibility" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4 ">
                            更新日期：
                        </label>
                        <div class="col-md-3" style="padding-right: 0">
                            <div id="datetime_5" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="开始日期" name="updateTimeFrom"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
                                            </span>
                            </div>

                        </div>

                        <div class="col-md-3" style="padding-left: 0">
                            <div id="datetime_6" class="input-append date form_datetime input-group">
                                <input  class="form-control" placeholder="结束日期" name="updateTimeTo"/>
                                <span class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">阶段：</label>
                        <div class="col-md-8">
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled" id="inlineCheckbox1" value="初期沟通" name="stageList">
                                <label for="inlineCheckbox1"> 初期沟通 </label>
                            </div>
                            <div class="checkbox checkbox-inline checkbox-inline">
                                <input type="checkbox" class="styled" id="inlineCheckbox2" value="立项评估" name="stageList">
                                <label for="inlineCheckbox2"> 立项评估 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled" id="inlineCheckbox3" value="需求分析" name="stageList">
                                <label for="inlineCheckbox3"> 需求分析 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled" id="inlineCheckbox4" value="0" name="stageList">
                                <label for="inlineCheckbox3"> 【未选】 </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">状态：</label>
                        <div class="col-md-8">
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="1" name="oppStatusList">
                                <label for="inlineCheckbox1"> 跟踪 </label>
                            </div>
                            <div class="checkbox checkbox-inline checkbox-inline">
                                <input type="checkbox" class="styled"  value="2" name="oppStatusList">
                                <label for="inlineCheckbox2"> 成功 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="3" name="oppStatusList">
                                <label for="inlineCheckbox3"> 失败 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="4" name="oppStatusList">
                                <label for="inlineCheckbox3"> 搁置 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="5" name="oppStatusList">
                                <label for="inlineCheckbox3"> 失效 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="0" name="oppStatusList">
                                <label for="inlineCheckbox3"> 【未选】 </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">优先：</label>
                        <div class="col-md-8">
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="1" name="priorityList">
                                <label for="inlineCheckbox1"> 一级 </label>
                            </div>
                            <div class="checkbox checkbox-inline checkbox-inline">
                                <input type="checkbox" class="styled"  value="2" name="priorityList">
                                <label for="inlineCheckbox2"> 二级 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="3" name="priorityList">
                                <label for="inlineCheckbox3"> 三级 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="4" name="priorityList">
                                <label for="inlineCheckbox3"> 四级 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="0" name="priorityList">
                                <label for="inlineCheckbox3"> 【未选】 </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">类型：</label>
                        <div class="col-md-8">
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="办公建筑" name="classificationList">
                                <label for="inlineCheckbox1"> 办公建筑 </label>
                            </div>
                            <div class="checkbox checkbox-inline checkbox-inline">
                                <input type="checkbox" class="styled"  value="住宅建筑" name="classificationList">
                                <label for="inlineCheckbox2"> 住宅建筑 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="商业建筑" name="classificationList">
                                <label for="inlineCheckbox3"> 商业建筑 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="医疗教育" name="classificationList">
                                <label for="inlineCheckbox3"> 医疗教育 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="文化健身" name="classificationList">
                                <label for="inlineCheckbox3"> 文化健身 </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">意向产品：</label>
                        <div class="col-md-8">
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="电子设备" name="intendProductList">
                                <label for="inlineCheckbox1"> 电子设备 </label>
                            </div>
                            <div class="checkbox checkbox-inline checkbox-inline">
                                <input type="checkbox" class="styled"  value="监控设备" name="intendProductList">
                                <label for="inlineCheckbox2"> 监控设备 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="线路设施" name="intendProductList">
                                <label for="inlineCheckbox3"> 线路设施 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="产品更换" name="intendProductList">
                                <label for="inlineCheckbox3"> 产品更换 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled"  value="设备维护" name="intendProductList">
                                <label for="inlineCheckbox3"> 设备维护 </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">负责人：</label>
                        <div class="col-md-6">
                            <input name="saleOppEntity.tbUserByUserId.userName" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <div style="height: 50px; "></div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
    <button class="btn btn-default" onclick="search_btn()" type="submit" id="search-senior"><i class="fa fa-check"></i>
        搜索</button>
</div>
<script>

    $(function() {
        datePicker(6);

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        $('.selectpicker').selectpicker('refresh');

        $('.bootstrap-select').css('min-width','213px');

        /*function search_btn() {
            search2();
            $('#search-senitor').modal("hide")
        }*/

        $("[data-toggle='tooltip' ] ").tooltip();

        $('#reset-btns').click(function () {
            $('#search-form')[0].reset();
        })

    });

    function search_btn() {
        search2();
        $('#search-senior').modal("hide")
        // $('#search-form')[0].reset();
    }

    function datePicker(index){
        for(var i= 1;i<=index;i++){
            $("#datetime_"+i).datetimepicker({
                format: "yyyy-mm-dd",
                minView: "month",//设置只显示到月份
                startView: 2,
                showMeridian: 1,
                forceParse: 0,
                autoclose: true,
                todayBtn: true,
                container: "#datetime_"+i
            });
        }
    }


</script>

</body>

</html>

