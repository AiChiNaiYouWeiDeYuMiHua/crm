<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/7
  Time: 22:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 模态框（Modal）插件</title>
</head>

<body>


    <!--
        作者：1102137042@qq.com
        时间：2018-07-31
        描述：竞争对手模态框
    -->

                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">关闭</button>
                    <h4 class="modal-title" id="myModalLabel">
                        竞争对手
                    </h4>
                    <hr class="boder-t-a" />
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <form id="xsjh">
                        <div class="mybody">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">
                                            <span style="color: #ff0000; font-size: 16px;">*</span>公司名称：
                                        </label>
                                        <div class="col-md-8">
                                            <input name="company" class="form-control" required="required" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input name="serveId" style="display: none">
                                            <label class="control-label col-md-2" style="padding-right: 20px;">
                                                <span style="color: #ff0000; font-size: 16px;">*</span>对应客户：
                                            </label>
                                            <div class="input-group col-md-10" style="padding-left: 10px;">
                                                <input type="text" id="cus" name="tbSaleOppByOppId.tbCustomerByCusId.cusId" class="form-control"
                                                       style="display: none">
                                                <input type="text" disabled class="form-control" id="to_cus">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-default" id="btn-cus"
                                                            style="color: black;height: 34px" onclick="chooseCus()"><i
                                                            class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4"><span style="color: #ff0000; font-size: 16px;">*</span>对应机会：</label>
                                        <div class="col-md-8">
                                            <select name="tbSaleOppByOppId.oppId" id="oppId" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">价格：</label>
                                        <div class="col-md-8">
                                            <input name="price" class="form-control" type="number"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-4">竞争能力：</label>
                                        <div class="col-md-8">
                                            <select name="competitiveAbility" class="selectpicker" data-live-search="true" data-live-search="true">
                                                <option selected></option>
                                                <option value="1">核心竞争</option>
                                                <option value="2">有力竞争</option>
                                                <option value="3">一般竞争</option>
                                                <option value="4">忽略竞争</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">竞争产品/方案：</label>
                                        <div class="col-md-10">
                                            <textarea name="competitiveProduct" style="resize: vertical;" class="form-control" onclick="$(this).css('min-height','130px')"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">劣势：</label>
                                        <div class="col-md-10">
                                            <textarea name="disadvantage" style="resize: vertical;" class="form-control" onclick="$(this).css('min-height','130px')"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">应对策略：</label>
                                        <div class="col-md-10">
                                            <textarea name="strategy" style="resize: vertical;" class="form-control" onclick="$(this).css('min-height','130px')"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group overflow">
                                        <label class="control-label col-md-2">备注：</label>
                                        <div class="col-md-10">
                                            <textarea name="remarks" style="resize: vertical;" class="form-control" onclick="$(this).css('min-height','130px')"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="height: 50px; "></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <%--<button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>--%>
                    <button class="btn btn-default" type="button" id="mysubmit"><i class="fa fa-check"></i>
                        保存</button>
                </div>

<script>
    var ykm_cusId;
    $(function() {
        setForm();

        //客户对应的销售机会下拉数据加载
        $("#cus").blur(function () {
            var cusId = $(this).val();
            $.ajax({
                type : 'post',
                url : "/getOppByCusId?cusId="+cusId,
                dataType : 'json',
                success : function(datas) {//返回list数据并循环获取
                    var select = $("#oppId");
                    $("#oppId").empty();
                    select.append("<option selected></option>");
                    for (var i = 0; i < datas.length; i++) {
                        select.append("<option value='"+datas[i].oppId+"'>"
                            + datas[i].oppTheme + "</option>");
                    }
                    // $('.selectpicker').selectpicker('val', '');
                    $('.selectpicker').selectpicker('refresh');
                }
            });
        });

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        // $('.selectpicker').selectpicker('refresh');

        $('.bootstrap-select').css('min-width','213px');



        $("#xsjh").bootstrapValidator({

            message: '通用的验证失败消息',
            feedbackIcons: {//根据验证结果显示的各种图标
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                company: {
                    msg:'公司名称验证失败',
                    validators: {
                        notEmpty: {
                            message: '公司名称不能为空'
                        },stringLength: {
                            min: 3,
                            message: '公司名称不能少于三个字'
                        }
                    }
                }
            },fields: {
                "tbSaleOppByOppId.oppId": {
                    msg:'机会验证失败',
                    validators: {
                        notEmpty: {
                            message: '机会不能为空'
                        }
                    }
                }
            }
        });
        //提交表单数据
        $("#mysubmit").on("click", function(){
            //获取表单对象
            var bootstrapValidator = $("#xsjh").data('bootstrapValidator');
            //手动触发验证
            bootstrapValidator.validate();
            if(bootstrapValidator.isValid()){
                // alert("表单序列化结果："+$("#xsjh").serialize())
                $('select[name="tbSaleOppByOppId.oppId"]').removeAttr("disabled");
                $.ajax({
                    url: '/competitor/add_modify',
                    type: 'post',
                    async: false,
                    data: $("#xsjh").serialize(),
                    success: function (result) {
                        if (result.code == 200) {
                            swal("成功",result.msg,"success");
                            $('#add_modal').modal('hide')
                            $("#load_modal").modal('hide')
                            //表单重置
                            $("#xsjh")[0].reset();
                            $("#table111").bootstrapTable('refresh')
                        }
                    },
                    error: function (data) {
                        swal("连接服务器错误","","error");
                    }
                });
            }
        });

        $("[data-toggle='tooltip' ] ").tooltip();

    });

    function cusToOpp() {
        var cusId = $("#cus").val();
        $.ajax({
            type : 'post',
            url : "/getOppByCusId?cusId="+cusId,
            dataType : 'json',
            async: false,
            success : function(datas) {//返回list数据并循环获取
                var select = $("#oppId");
                $("#oppId").empty();
                select.append("<option selected></option>");
                for (var i = 0; i < datas.length; i++) {
                    if(datas[i].oppTheme == undefined){
                        select.append("<option value='"+datas[i].oppId+"'>"
                            + "无主题" + "</option>");
                    } else {
                        select.append("<option value='"+datas[i].oppId+"'>"
                            + datas[i].oppTheme + "</option>");
                    }
                }
                // $('.selectpicker').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');
            }
        });
    }

    function chooseCus() {
        window.open("/customer_check");
    }

    function getCus(cusId,cusName) {
        $("input[name='tbSaleOppByOppId.tbCustomerByCusId.cusId']").val(cusId);
        cusToOpp();
        $("#to_cus").val(cusName);
        $.ajax({
            url: '/memorial/get_contact?id=' + $("input[name='tbSaleOppByOppId.tbCustomerByCusId.cusId']").val(),
            method: 'get',
            async: true,
            success: function (data1) {
                $("#cotsName").empty();
                for (var i = 0; i < data1.length; i++) {
                    $('#cotsName').append("<option value='" + data1[i].tbContactsByCotsId.cotsId + "'>" + data1[i].tbContactsByCotsId.cotsName + "</option>");
                    $('#cotsName').selectpicker('refresh');
                }
                $('#cotsName').selectpicker('refresh');
            }
        });
    }

    //表单初始化
    function setForm() {
        if (open_modal != null) {
            $("#btn-cus").attr("disabled","disabled")
            ajax("/competitor/load?id="+open_modal,function (data) {
                $('#xsjh').append("<input class='hidden' name='competitorId' value='"+data.competitorId+"'>")
                $('input[name="company"]').val(data.company)
                $('input[name="price"]').val(data.price)
                if(data.tbSaleOppByOppId != null && data.tbSaleOppByOppId.tbCustomerByCusId != null){
                    // alert(data.tbSaleOppByOppId.tbCustomerByCusId.cusName)
                    $('input[name="tbSaleOppByOppId.tbCustomerByCusId.cusId"]').val(data.tbSaleOppByOppId.tbCustomerByCusId.cusId)
                    $('#to_cus').val(data.tbSaleOppByOppId.tbCustomerByCusId.cusName)
                }
                $('select[name="competitiveAbility"]').selectpicker('val',data.competitiveAbility)
                if(data.tbSaleOppByOppId != null){
                    //客户对应的销售机会下拉数据加载
                    cusToOpp();
                    $('select[name="tbSaleOppByOppId.oppId"]').selectpicker('val',data.tbSaleOppByOppId.oppId)

                }
                $('textarea[name="competitiveProduct"]').val(data.competitiveProduct)
                $('textarea[name="disadvantage"]').val(data.disadvantage)
                $('textarea[name="strategy"]').val(data.strategy)
                $('textarea[name="remarks"]').val(data.remarks)
                if(data.tbSaleOppByOppId != null){
                    $('select[name="tbSaleOppByOppId.oppId"]').selectpicker('val',data.tbSaleOppByOppId.oppId)

                }
            },function () {
                swal("加载失败",result.msg,"error");
                $('#add_sale_opp').modal('hide')
            },"get",{})
        } if (ykm_cusId != null){
            $('input[name="tbSaleOppByOppId.tbCustomerByCusId.cusId"]').val(ykm_cusId)
            $("#btn-cus").attr("disabled","disabled")
            $('#to_cus').val(ykm_cusName);
            cusToOpp();
            $('select[name="tbSaleOppByOppId.oppId"]').selectpicker('val',ykm_oppId)
            $('select[name="tbSaleOppByOppId.oppId"]').attr("disabled","disabled")
        }

    }



</script>

</body>

</html>
