<%--
  Created by IntelliJ IDEA.
  User: ykm
  Date: 2018/8/10
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>

<body>


<!--
    作者：1102137042@qq.com
    时间：2018-07-31
    描述：高级查询模态框
-->

<div class="modal-header" style="border-bottom: none;">
    <button type="button" class="btn btn-default pull-right" id="reset-btns">
        <i class="fa fa-check"></i>
        重置
    </button>
    <h4 class="modal-title" id="myModalLabel">
        高级查询
    </h4>
    <hr class="boder-t-a" />
</div>
<div class="modal-body" style="padding-top: 0px;">
    <form id="search-form" class="form-horizontal">
        <div class="mybody">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            ID：
                        </label>
                        <div class="col-md-6">
                            <input name="demandEntity.demandId" class="form-control" type="number" onkeyup= "if(! /^-?[1-9]+[0-9]*$/.test(this.value)){alert('只能整数');this.value='';}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">需求主题：</label>
                        <div class="col-md-6">
                            <input name="demandEntity.demandTheme" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">客户：</label>
                        <div class="col-md-6">
                            <input name="demandEntity.tbSaleOppByOppId.tbCustomerByCusId.cusName" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">记录时间：</label>
                        <div class="col-md-8">
                            <input class="form-control" placeholder="开始日期" name="recordTimeFrom" type="date">
                            <span class="">&nbsp;到&nbsp;</span>
                            <input class="form-control" placeholder="结束日期" name="recordTimeTo" type="date">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">重要程度：</label>
                        <div class="col-md-8">
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled" id="inlineCheckbox1" value="1" name="importanceList">
                                <label for="inlineCheckbox1"> 非常重要 </label>
                            </div>
                            <div class="checkbox checkbox-inline checkbox-inline">
                                <input type="checkbox" class="styled" id="inlineCheckbox2" value="2" name="importanceList">
                                <label for="inlineCheckbox2"> 重要 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled" id="inlineCheckbox3" value="3" name="importanceList">
                                <label for="inlineCheckbox3"> 一般 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled" id="inlineCheckbox4" value="4" name="importanceList">
                                <label for="inlineCheckbox3"> 不重要 </label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled" id="inlineCheckbox5" value="0" name="importanceList">
                                <label for="inlineCheckbox3"> 未选 </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: 50px; "></div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal">关闭</button>
    <button class="btn btn-default" onclick="search_btn()" type="submit" id="search-senior"><i class="fa fa-check"></i>
        搜索</button>
</div>
<script>

    $(function() {

        $(".selectpicker").selectpicker({
            noneSelectedText : '未选'
        });

        $('.selectpicker').selectpicker('refresh');

        $('.bootstrap-select').css('min-width','213px');


        $("[data-toggle='tooltip' ] ").tooltip();

        $('#reset-btns').click(function () {
            $('#search-form')[0].reset();
        })

    });

    function search_btn() {
        search2();
        $('#search-senior').modal("hide")
        // $('#search-form')[0].reset();
    }


</script>

</body>

</html>

