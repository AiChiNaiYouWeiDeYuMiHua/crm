<%--
  Created by IntelliJ IDEA.
  User: LiQuanfang
  Date: 2018/7/19
  Time: 20:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>招商银行账户列表</title>

    <!-- Bootstrap -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h2 class="" id="table-title">招商银行账户列表</h2>
            <hr/>
            <button class="btn btn-primary" data-toggle="modal"
                    id="new-btn">开户</button>

            <button class="btn btn-success" data-toggle="modal"
                    data-target="#transfer">转账</button>
            <table id="table" style="margin-top: 5px">
                <thead>
                <tr>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="new" tabindex="-1" role="dialog"
     aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade" id="msg" tabindex="-1" role="dialog"
     aria-labelledby="open" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<!-- Latest compiled and minified Locales -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.min.js"></script>
<script src="//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>
<script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script>
    var modal_data = -1;
    $(function () {
        alert("注意，只有编辑做了数据绑定，只有编辑中的姓名做了数据校验,只有jpa做了测试")
        toastr.options = {
            "closeButton": true,
        }
        $("#table").bootstrapTable({
            method: 'get',
            striped: true,
            showExport:true,
            cache: false,
            pagination: true,
            sortable: false,
            sortOrder: "desc",
            pageNumber:1,
            pageSize: 10,
            url: "/account/list",
            sidePagination: "server",
            queryParamsType:'',
            exportOptions:{
                fileName:$('#table-title').text(),
                ignoreColumn:[7]
            },
            showFooter:true,
            columns: [{
                field:'aid',
                title:'序号',
                align:'center',
                footerFormatter:totalTextFormatter
            },{
                field:'name',
                title:'姓名',
                align:'center'
            },{
                field:'sex',
                title:'性别',
                align:'center'
            },{
                field:'code',
                title:'身份证号',
                align:'center'
            },{
                field:'openMoney',
                title:'开户金额',
                align:'center',
                footerFormatter:sumFormatter
            },{
                field:'openTime',
                title:'开户时间',
                align:'center'
            },{
                field:'balance',
                title:'余额',
                align:'center',
                footerFormatter:sumFormatter
            },{
                field:'aid',
                title:'操作',
                align:'center',
                formatter:operateFormatter,
                width:"20%"
            }],
            onLoadSuccess:function (data) {

            },
            onLoadError:function (status) {
                alert("错误")
            }
        })
       //打开新建账号modal
        $("#new-btn").click(function () {
           $('#new').modal({
               remote:'/modal/accountNew.html'
           })
        });
        //消除模态框数据
        $("#new").on("hidden.bs.modal", function () {
            $(this).removeData("bs.modal");
        });

        $("#msg").on("hidden.bs.modal", function () {
            $(this).removeData("bs.modal");
        });

    })
    //格式化
    function operateFormatter(value, row, index) {
       return [
               '<a onclick="edit('+index+')" class=" btn btn-default  btn-sm" style="margin-right:15px;">编辑</a>',
               '<a href="/account/delete?id='+value+'" class=" btn btn-default  btn-sm" style="margin-right:15px;">销户</a>',
               '<a   href="/account/reset?id='+value+'" class=" btn btn-default  btn-sm" style="margin-right:15px;">重置密码</a>',
             '<a  href="/transfer/ok?id='+value+'" class=" btn btn-default  btn-sm" style="margin-right:15px;">对账</a>',
                         ].join('');
    }
    //编辑时间
    function edit(id) {
        $("#table").bootstrapTable('check',id);
        var data = $("#table").bootstrapTable('getSelections')[0]
        modal_data = data.aid;
        $("#table").bootstrapTable('uncheck',id);
        $('#msg').modal({
         remote:'/modal/accountModify.html'
        })
     }
    function totalTextFormatter(data) {
        return "合计"
    }
    function sumFormatter(data) {
        field = this.field;
        return data.reduce(function(sum, row) {
            return sum + (+row[field]);
        }, 0);
    }
</script>
</body>
</html>
