$(function () {
    $("#table").bootstrapTable({
        url: "/sale/warehouse/detail/all",
        striped: true,
        pagination: true,
        sidePagination: "server",
        paginationLoop: false,
        method:'post',
        queryParams: function(params) {
            //表单转json(去掉孔项目，节省流量
            var d = f($('#search-form'));
            var categorySimple = $('.nav .active').eq(0).attr("data-value");
            var type = $('#select-type').val();
            var searchText = $('#search-text').val();
            if (type != null && type.length >0)
                d["type"] = type;
            if (categorySimple != null && categorySimple.length >0)
                d["categorySimple"] = categorySimple;
            if (searchText != null && searchText.length >0)
                d["searchText"] = searchText;
            d["page.size"] = params.limit;
            d["page.page"] = (params.offset/params.limit)+1;
            var sort = params.sort;
            if (sort != null && sort.length >0){
                d["page.sortName"] = sort;
                d["page.sortOrder"] = params.order
            }
            return d
        },
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）

        contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据

        minimumCountColumns: 2, //最少允许的列数
        detailView: false, //是否显示父子表
        cache: false, // 设置为 false 禁用 AJAX 数据缓存， 默认为true
        sortable: true, //是否启用排序
        uniqueId: "id",
        columns: [{
            field: 'id', // 返回json数据中的name
            title: '序号', // 表格表头显示文字
            align: 'center', // 左右居中
            width: 80,
            valign: 'middle', // 上下居中
            formatter:function(value,row,index){
                return tableIndexNum(index);
            }
        }, {
            field: 'wdType',
            title: '类型',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:function (value,row,index){
                return [
                    '<i style="margin-right: 3px" class="fa fa-arrow-circle-right text-blue m-r-5"></i>'+typeStr(value)
                ].join('');
            }
        },  {
            field: 'wareHouseName',
            title: '仓库',
            align: 'center',
            valign: 'middle',
            formatter:isnull
        },{
            field: 'productTitle',
            title: '产品名称',
            align: 'center',
            valign: 'middle',
            formatter:function (value,row,index) {
                if (value != null)
                    return [
                        '<span style="font-weight:normal;color:#9e9e9e">〖</span>'+value+'' +
                        '<a target="_blank" href="/sale/product/info/'+row.productId+'/" style="margin-left: 5px" ><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a>' +
                        '<span style="font-weight:normal;color:#9e9e9e">〗</span>'
                    ].join('');
                else
                    return "";
            }
        }, {
            field: 'inNumber',
            title: '入库量 ',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:function (value,row,index) {
                if (value == null)
                    return 0;
                else
                    return value;
            }
        }, {
            field: 'outNumber',
            title: '出库量',
            align: 'center',
            valign: 'middle',
            formatter:function (value,row,index) {
                if (value == null)
                    return 0;
                else
                    return value;
            }
        },{
            field: 'wdExecute',
            title: '执行日期',
            align: 'center',
            valign: 'middle',
            formatter:isnull
        }, {
            field: 'userName',
            title: '执行人',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:isnull
        }, {
            field: 'customerName',
            title: '客户/供应商',
            align: 'center',
            valign: 'middle',
            formatter:function (value,row,index) {
                if (value != null)
                    return [
                        '<span style="font-weight:normal;color:#9e9e9e">〖</span>'+value+'' +
                        '<a target="_blank" href="/customer_details?id='+row.customerId+'" style="margin-left: 5px" ><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a>' +
                        '<span style="font-weight:normal;color:#9e9e9e">〗</span>'
                    ].join('');
                else
                    return "";
            }
        },  {
            field: 'wpId',
            title: '操作',
            titlealign: 'left',
            valign: 'middle',
            align: 'center',
            formatter: function(value, row, index) {
                var json = [];
                json.push('<a onclick="openBill('+value+','+row.wdType+')" style="cursor:pointer;margin-left:8px;" data-toggle="tooltip" data-placement="bottom"  title="对应单据"><i class="fa fa-file"></i></a>')
                return json.join('');
            }
        }],
        onLoadSuccess: function() { //加载成功时执行
            console.info("加载成功");
            $("[data-toggle='tooltip' ] ").tooltip();
        },
        onLoadError: function() { //加载失败时执行
            console.info("加载数据失败");
        },

        //>>>>>>>>>>>>>>导出excel表格设置
        showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
        exportDataType: "basic", //basic', 'all', 'selected'.
        exportTypes: ['excel', 'xlsx'], //导出类型
        //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
        exportOptions: {
            ignoreColumn: [0,0],            //忽略某一列的索引
            fileName: '产品信息', //文件名称设置
            worksheetName: 'Sheet1', //表格工作区名称
            tableName: '商品数据表',
            excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
            //onMsoNumberFormat: DoOnMsoNumberFormat
        }
        //导出excel表格设置<<<<<<<<<<<<<<<<

    });
})
//单元格空处理
function isnull(value,row,index) {
    if (value == null)
        return "";
    else
        return value;
}
//表格自动序号
function tableIndexNum(index){
    var currentPage=$(".page-item.active").find('a').text();
    return Number(index+1+eval((currentPage-1)*$(".page-list .page-size").val()));
}
//表单数据转json对象
function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function() {
        if (this.value != null && this.value.length >0)
            d[this.name] = this.value;
    });
    return d;
}
function typeStr(value) {
    switch (value) {
        case 1:
            return "入库";
        case 0:
            return "出库";
        // case 2:
        //     return "盘点差异";
        // case 3:
        //     return "调入";
        // case 4:
        //     return "调出";
        // case 5:
        //     return "退货";
        // case 6:
        //     return "采购退货";
    }
}