function initSendTable(id,sendId) {
    var $table = $("#table"+id);
    $table.bootstrapTable({
        method: 'get',
        striped: true,

        cache: false,
        pagination: false,
        sortable: false,
        sortOrder: "desc",
        pageNumber: 1,
        pageSize: 10,
        url: "/sale/warehouse/send/detail?id="+sendId,
        sidePagination: "server",
        queryParamsType: '',

        columns: [{
            field: 'formatEntity.tbProductByProductId.productName',
            title: '品名',
            align: 'center',
            formatter:isnull
//			footerFormatter: "合计"
        }, {
            field: 'formatEntity.tbProductByProductId.productModel',
            title: '型号',
            align: 'center',
            formatter:isnull
        }, {
            field: 'formatEntity.pfName',
            title: '规格',
            align: 'center',
            formatter:isnull
        }, {
            field: 'formatEntity.pfUnit',
            title: '单位',
            align: 'center',
            formatter:isnull
        }, {
            field: 'ddNumber',
            title: '数量',
            align: 'center',
            formatter:isnull
//			footerFormatter: "合计"
        }, {
            field: 'ddPrice',
            title: '单价',
            align: 'center',
            formatter:function (value,row,index) {
                if (value != null)
                    return fmoney(value,2);
                else
                    return "";
            }
        },{
            field: 'price',
            title: '折扣',
            align: 'center',
            formatter:function (value,row,index) {
                if (value != null ) {

                    var add = (-1*(row.ddNumber*row.ddPrice-row.ddTotal));
                    if (row.ddNumber*row.ddPrice == 0 || add == 0)
                        return "";
                    if (add > 0)
                        return "<span style='color: red'>"+fmoney(add,2)+"</span>("+100*(row.ddTotal/(row.ddNumber*row.ddPrice))+"%)";
                    else
                        return "<span style='color: red'>"+fmoney(add,2)+"</span>("+100*(row.ddTotal/(row.ddNumber*row.ddPrice))+"%)";
                }else
                    return "";
            }
//			footerFormatter: "合计"
        }, {
            field: 'ddTotal',
            title: '金额',
            align: 'center',
            formatter:function (value,row,index) {
                if (value != null)
                    return fmoney(value,2);
                else
                    return "";
            }
//			footerFormatter: "合计"
        },  {
            field: 'ddOther',
            title: '备注',
            align: 'center',
            formatter:isnull
        }],
        onLoadSuccess: function(data) {
            if (data.length > 0){
                var total = 0;
                var n = 0;
                for (var i =0;i<data.length;i++) {
                    total += parseFloat(data[i].total);
                    n += parseFloat(data[i].number)
                }
                addFotter($table, {
                    productName: '合计',
                    number:n,
                    total: fmoney(total,2),
                })

                addFotter($table, {
                    productName: '合计(大写金额)',
                    other: digit_uppercase(total),
                });
                formatterFotter($table, 'number', 1, 2);
                formatterFotter($table, 'other', 1, 1);
            }
        },
        onLoadError: function(status) {
        }
    })
}
function addFotter(table, json) {
    table.bootstrapTable('append', json);
}

function formatterFotter(table, field, colspan, num) {
    table.bootstrapTable('mergeCells', {
        index: getLength(table) - num + 1,
        field: field,
        colspan: colspan
    });
}

function getLength(table) {
    return table.bootstrapTable('getData', true).length - 1;
}
function isnull(value,row,index) {
    if (value == null)
        return "";
    else
        return value;
}