function InitExcelFile(obj) {
    $("#upload").css("display","block");
    //记录GUID
    //          $("#AttachGUID").val(newGuid());
    $("#excelFile").fileinput({
        uploadUrl: "/files_upload", //上传的地址
        uploadAsync: true, //异步上传
        language: "zh", //设置语言
        showCaption: true, //是否显示标题
        showUpload: true, //是否显示上传按钮
        showRemove: true, //是否显示移除按钮
        showPreview: true, //是否显示预览按钮
        browseClass: "btn btn-primary", //按钮样式
        dropZoneEnabled: false, //是否显示拖拽区域
        // allowedFileExtensions: ["jpg", "png",'gif'], //接收的文件后缀
        enctype: 'multipart/form-data',
        maxFileCount: 5, //最大上传文件数限制
        previewFileIcon: '<i class="fa fa-file"></i>',
        allowedPreviewTypes: null,
        dropZoneEnabled: true,//是否显示拖拽区域
        minImageWidth: 50, //图片的最小宽度
        minImageHeight: 50,//图片的最小高度
        maxImageWidth: 1000,//图片的最大宽度
        maxImageHeight: 1000,//图片的最大高度
        maxFileSize: 0,//单位为kb，如果为0表示不限制文件大小
        minFileCount: 0
    });

    $('#excelFile').on("fileuploaded", function(event, data, previewId, index) {
        var result = data.response; //后台返回的json
        $.ajax({//上传文件成功后再保存图片信息
            url:'files_upload',
            type:'post',
            dataType:'json',
            data:$('#ffImport').serialize(),//form表单的值
            success:function(){
            },
            cache:false,                    //不缓存
        });
        obj(result.uri)
    });

}


function InitExcelImg(obj) {
    $("#upload").css("display","block");
    //记录GUID
    //          $("#AttachGUID").val(newGuid());
    $("#excelFile,#excelFile-f").fileinput({
        uploadUrl: "/file_upload", //上传的地址
        uploadAsync: true, //异步上传
        language: "zh", //设置语言
        showCaption: true, //是否显示标题
        showUpload: true, //是否显示上传按钮
        showRemove: true, //是否显示移除按钮
        showPreview: true, //是否显示预览按钮
        browseClass: "btn btn-primary", //按钮样式
        dropZoneEnabled: false, //是否显示拖拽区域
        allowedFileExtensions: ["jpg", "png",'gif'], //接收的文件后缀
        enctype: 'multipart/form-data',
        maxFileCount: 1, //最大上传文件数限制
        previewFileIcon: '<i class="fa fa-file"></i>',
        allowedPreviewTypes: null,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file"></i>',
            'xlsx': '<i class="fa fa-file"></i>',
            'pptx': '<i class="fa fa-file"></i>',
            'jpg': '<i class="glyphicon glyphicon-picture"></i>',
            'bmp': '<i class="glyphicon glyphicon-picture"></i>',
            'pdf': '<i class="glyphicon glyphicon-file"></i>',
            'zip': '<i class="glyphicon glyphicon-file"></i>'
        }
    });

    $('#excelFile,#excelFile-f').on("fileuploaded", function(event, data, previewId, index) {
        var result = data.response; //后台返回的json
        //console.log(result.status);
        //console.log(result.id);
        // $('#picid').val(result.id);//拿到后台传回来的id，给图片的id赋值序列化表单用
        //如果是上传多张图
        /*
        //计数标记，用于确保全部图片都上传成功了，再提交表单信息
        var fileCount = $('#file-pic').fileinput('getFilesCount');
        if(fileCount==1){
        $.ajax({//上传文件成功后再保存图片信息
            url:'BannerPicAction!savaForm.action',
            data:$('#form1').serialize(),//form表单的值
            success:function(data,status){
                ...
            },
            cache:false,                    //不缓存
        });
        }
        */
        $.ajax({//上传文件成功后再保存图片信息
            url:'file_upload',
            type:'post',
            dataType:'json',
            data:$('#ffImport').serialize(),//form表单的值
            success:function(){

            },
            cache:false,                    //不缓存
        });
        obj(result.uri);
    });

    // $('#savePic').on('click',function (){// 提交图片信息 //
    //     //先上传文件，然后在回调函数提交表单
    //     $('#file-pic').fileinput('upload');
    //
    // });
}





function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}


function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}