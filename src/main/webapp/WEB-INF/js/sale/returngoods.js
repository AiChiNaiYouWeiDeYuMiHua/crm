function initTable1(a, b) {
    var table = "#" + a;
    $(table).bootstrapTable({
        method: 'post',
        url: "/sale/rgdall1?type=1&rgId=" + b,
        pageList: [10, 25, 50, 100],
        pageNumber: 1,
        pageSize: 5,
        contentType: "application/json",
        strictSearch: false,
        minimumCountColumns: 2,
        detailView: false,
        cache: false,
        sortable: true,
        sortOrder: "asc",
        sortName: 'rgdId',
        uniqueId: "id",
        columns: [{
            // field: 'id', // 返回json数据中的name
            title: '序号', // 表格表头显示文字
            align: 'center', // 左右居中
            width: 80,
            valign: 'middle', // 上下居中
            formatter: function (value, row, index) {
                return tableIndexNum(index);
            },
            footerFormatter: function (value) {
                return "合计";
            }
        },  {
            field: 'title',
            title: '产品名称',
            align: 'center',
            valign: 'middle',
            formatter: f3
        }, {
            field: 'rgdReason',
            title: '退货原因',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter: isNullFormatter
        }, {
            field: 'rgdNum',
            title: '退货数量',
            align: 'center',
            valign: 'middle',
            formatter: isNullFormatter
        }, {
            field: 'rgdMoney',
            title: '退货金额',
            align: 'center',
            valign: 'middle',
            formatter: function (value,row,index) {
                return fmoney(value,2)
            }
        }, {
            field: 'rgdOutwhNum',
            title: '已入库数量',
            align: 'center',
            valign: 'middle',
            formatter: isNullFormatter
        }],
        onLoadSuccess: function (data) { //加载成功时执行
            console.info("加载成功");
        },
        onLoadError: function () { //加载失败时执行
            console.info("加载数据失败");
        },
    });
}

function isNullFormatter(value, rows, index) {
    if (value == null)
        return "";
    else
        return value;
}

function moneyFormatter(value, rows, index) {
    if (value == null)
        return "";
    else {
        var money = transMoney(value);
        return "&nbsp;￥" + money;
    }
}

function f1(value, rows, index) {
    if (value == null)
        return "";
    else {
        return "<p align='center'><span style='font-weight:normal;color:#9e9e9e'>〖</span>" + value + "&nbsp;<a href='');><i class='fa fa-folder-open m-l-5'></i></a><span style='font-weight:normal;color:#9e9e9e'>〗</span></p>";
    }
}

function transMoney(money) {
    if (money && money != null) {
        money = String(money);
        var left = money.split('.')[0], right = money.split('.')[1];
        right = right ? (right.length >= 2 ? '.' + right.substr(0, 2) : '.' + right + '0') : '.00';
        var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);
        return (Number(money) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
    } else if (money === 0) {   //注意===在这里的使用，如果传入的money为0,if中会将其判定为boolean类型，故而要另外做===判断
        return '0.00';
    } else {
        return "";
    }
}

function sumFormatter(data) {
    field = this.field;
    var sum1 = data.reduce(function (sum, row) {
        return sum + (+row[field]);
    }, 0);
    return "￥" + transMoney(sum1);
}

//表格自动序号
function tableIndexNum(index) {
    var currentPage = $(".page-item.active").find('a').text();
    var size = $(".page-list .page-size").text();
    if (size == null || size.length < 0)
        size = 5;
    return Number(index + 1 + eval((currentPage - 1) * size));
}

//表单数据转json对象
function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}

function edit1() {
    var b = "/sale/purProDetail/" + $("#a").val() + "/";
    window.open(b);
}

function f2(value, row, index) {
    if (value == null)
        return "";
    else {
        return ['<a    href="/sale/rgdetail1/' + row.rgId + '/")"><i class="fa fa-arrow-circle-right text-blue m-r-5"></i>&nbsp;' + value + '</a>'].join('');
    }
}

function f3(value, row, index) {
    if (value == null)
        return "";
    else {
        return ['<a    href="/sale/product/info/' + row.pfId + '/")"><i class="fa fa-arrow-circle-right text-blue m-r-5"></i>&nbsp;' + value + '</a>'].join('');
    }
}
function returnMoney(rgId) {
    var a = $("#purId").val();
    var b = "/sale/addPrModel1?rgId=" + rgId;
    $('#wsj-add').modal({
        remote: b
    })
}

//入库
function ruku(id) {
    var url = "/sale/warehouse/return/in?id=" + id;
    ajax(url, function (data) {
        window.open("/sale/warehouse/in/info/" + data + "/");
    }, function (msg) {
    });
}