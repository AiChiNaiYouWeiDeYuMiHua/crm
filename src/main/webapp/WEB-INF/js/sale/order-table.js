$(function () {
    $("#table").bootstrapTable({
        url: "/sale/order/all",
        striped: true,
        pagination: true,
        sidePagination: "server",
        paginationLoop: false,
        method:'post',
        showFooter:true,
        pageList:[10, 25, 50, 100,'All'],
        queryParams: function(params) {
            //表单转json(去掉孔项目，节省流量
            var d = f($('#search-form'));
            var categorySimple = $('.nav .active').eq(0).attr("data-value");
            var type = $('#select-type').val();
            var searchText = $('#search-text').val();
            if (type != null && type.length >0)
                d["type"] = type;
            if (categorySimple != null && categorySimple.length >0)
                d["categorySimple"] = categorySimple;
            if (searchText != null && searchText.length >0)
                d["searchText"] = searchText;
            var page = {};
            page['size'] =  params.limit;
            page['page'] = (params.offset/params.limit)+1;
            var sort = params.sort;
            if (sort != null && sort.length >0){
                page['sortName'] = sort;
                page['sortOrder'] = params.order
            }
            d['page'] = page
            return d
        },
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）

        contentType: "application/json", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据

        minimumCountColumns: 2, //最少允许的列数
        detailView: false, //是否显示父子表
        cache: false, // 设置为 false 禁用 AJAX 数据缓存， 默认为true
        sortable: true, //是否启用排序
        uniqueId: "id",
        columns: [{
            checkbox: 'true',
            width: 60,
            align: 'center',

        }, {
            field: 'id', // 返回json数据中的name
            title: '序号', // 表格表头显示文字
            align: 'center', // 左右居中
            width: 80,
            valign: 'middle', // 上下居中
            formatter:function(value,row,index){
                return tableIndexNum(index);
            },
            footerFormatter:'合计'
        }, {
            field: 'orderNumber',
            title: '订单号',
            align: 'center',
            valign: 'middle',
            sortable: true,
        },  {
            field: 'orderTitle',
            title: '主题',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:function (value,row,index){
                var id = row.orderId;
                if (value == null || value.length <=0)
                    value = "无主题";
                return [
                    '<a  href="/sale/order/info/'+id+'/" target="_blank"><i style="margin-right: 3px" class="fa fa-arrow-circle-right text-blue m-r-5"></i>'+value+'</a>'
                ].join('');
            }
        },{
            field: 'tbCustomerByCusId.cusName',
            title: '对应客户',
            align: 'center',
            valign: 'middle',
            formatter:function (value,row,index) {
                if (value != null)
                    return [
                        '<span style="font-weight:normal;color:#9e9e9e">〖</span>'+value+'' +
                        '<a href="/customer_details?id='+row.orderId+'" style="margin-left: 5px" ><i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" title="打开详细页面"></i></a>' +
                        '<span style="font-weight:normal;color:#9e9e9e">〗</span>'
                    ].join('');
                else
                    return "";
            }
        }, {
            field: 'orderTotal',
            title: '总金额 ',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:function (value,row,index){
                return fmoney(value,2)
            },
            footerFormatter: sumFormatter
        }, {
            field: 'returnMoney',
            title: '回款金额',
            align: 'center',
            valign: 'middle',
            formatter:function (value,row,index){
              return fmoney(value,2)
            },
            footerFormatter: sumFormatter
        },{
            field: 'orderStatus',
            title: '状态',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:function (value,row,index){
                var title = "执行中";
                var icon = "fa-arrow-right";
                if (value == 1){
                    title = "结束";
                    icon = "fa-check";
                }else if (value == 2){
                    title = "意外终止";
                    icon = "fa-remove";
                }
                return '<i data-toggle="tooltip"  data-placement="bottom" class="f-s-16 fa '+icon+' ' +
                    'text-primary"  title="'+title+'"></i><span class="h-text">'+title+'</span>'
            }
        }, {
            field: 'tbUserByUserId.userName',
            title: '所有者',
            align: 'center',
            valign: 'middle',
            formatter:isnull
        },  {
            title: '操作',
            titlealign: 'left',
            valign: 'middle',
            align: 'center',
            formatter: function(value, row, index) {
                return operation(value,row)
            }
        }],
        onLoadSuccess: function(data) { //加载成功时执行
            console.info("加载成功");
            add()
        },
        onLoadError: function() { //加载失败时执行
            console.info("加载数据失败");
        },

        //>>>>>>>>>>>>>>导出excel表格设置
        showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
        exportDataType: "basic", //basic', 'all', 'selected'.
        exportTypes: ['excel', 'xlsx'], //导出类型
        //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
        exportOptions: {
            ignoreColumn: [0,10],            //忽略某一列的索引
            fileName: '合同/订单信息', //文件名称设置
            worksheetName: 'Sheet1', //表格工作区名称
            tableName: '合同/订单',
            excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
            //onMsoNumberFormat: DoOnMsoNumberFormat
        }
        //导出excel表格设置<<<<<<<<<<<<<<<<

    });
})
//单元格空处理
function isnull(value,row,index) {
    if (value == null)
        return "";
    else
        return value;
}
//表格自动序号
function tableIndexNum(index){
    var currentPage=$(".page-item.active").find('a').text();
    return Number(index+1+eval((currentPage-1)*$(".page-list .page-size").val()));
}
//表单数据转json对象
function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function() {
        if (this.value != null && this.value.length >0) {
            if (this.name == 'users' || this.name == 'payWays' || this.name == 'returnWays'
                || this.name == 'categorys') {
                if (!$.isArray(d[this.name]))
                    d[this.name] = []
                d[this.name].push(this.value);
            }else
                d[this.name] = this.value;
        }
    });
    return d;
}
function sumFormatter(data) {
    field = this.field;
    var sum1= data.reduce(function(sum, row) {
        return sum + (+row[field]);
    }, 0);
    return fmoney(sum1,2);
}
