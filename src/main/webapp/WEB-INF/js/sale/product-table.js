$(function () {
    $("#table").bootstrapTable({
        url: "/sale/product/all",
        striped: true,
        pagination: true,
        sidePagination: "server",
        paginationLoop: false,
        pageList:[10, 25, 50, 100,'All'],
        method:'post',
        queryParams: function(params) {
            //表单转json(去掉孔项目，节省流量
            var d = f($('#search-form'));
            var categorySimple = $('#select-category').val();
            var type = $('#select-type').val();
            var searchText = $('#search-text').val();
            if (type != null && type.length >0)
                d["type"] = type;
            if (categorySimple != null && categorySimple.length >0)
                d["categorySimple"] = categorySimple;
            if (searchText != null && searchText.length >0)
                d["searchText"] = searchText;
            d["page.size"] = params.limit;
            d["page.page"] = (params.offset/params.limit)+1;
            var sort = params.sort;
            if (sort != null && sort.length >0){
                d["page.sortName"] = sort;
                d["page.sortOrder"] = params.order
            }
            return d
        },
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）

        contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据

        cache: false, // 设置为 false 禁用 AJAX 数据缓存， 默认为true
        sortable: true, //是否启用排序
        uniqueId: "id",

        columns: [{
            checkbox: 'true',
            width: 60,
            align: 'center',
            footerFormatter: function(value) {
                return "合计";
            }
        }, {
            field: 'id', // 返回json数据中的name
            title: '序号', // 表格表头显示文字
            align: 'center', // 左右居中
            width: 80,
            valign: 'middle', // 上下居中
            formatter:function(value,row,index){
                return tableIndexNum(index);
            }
        }, {
            field: 'productEntity.productName',
            title: '品名',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:function (value,row,index){
                var id = row.id;
                return [
                    '<a href="/sale/product/info/'+id+'/" target="_blank"><i class="fa fa-arrow-circle-right text-blue m-r-5"></i>'+value+'</a>'
                ].join('');
            }
        }, {
            field: 'pfCode',
            title: '编号/条码',
            align: 'center',
            valign: 'middle',
            formatter:isnull
        }, {
            field: 'pfImf',
            title: '产品图片',
            align: 'center',
            valign: 'middle',
            formatter:function (value,row,index) {
                if (value != null)
                    return [
                        '<img class="thumbnail" width="80px" src="'+value+'"/>'
                    ].join('');
                else
                    return "";
            }
        }, {
            field: 'productEntity.productModel',
            title: '型号 ',
            align: 'center',
            valign: 'middle',
            formatter:isnull
        }, {
            field: 'pfName',
            title: '规格',
            align: 'center',
            valign: 'middle',
            formatter:isnull
        },{
            field: 'pfPrice',
            title: '价格',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:function (value,row,index){
                return fmoney(value,2)
            }
        }, {
            field: 'areaname',
            title: '单位',
            align: 'center',
            valign: 'middle',
            formatter:isnull
        }, {
            field: 'pfMax',
            title: '库存上限',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:isnull
        }, {
            field: 'pfMin',
            title: '库存下限',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:isnull
        }, {
            field: 'number',
            title: '当前库存',
            align: 'center',
            valign: 'middle',
            formatter:isnull
        },{
            field: 'status',
            title: '状态',
            align: 'center',
            valign: 'middle',
            formatter:function (value,row,index){
                if (value == 0)
                    return "正常";
                else
                    return "停售"
            }
        },{
            field: 'pfCost',
            title: '成本价格',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:function (value,row,index){
                return fmoney(value,2)
            }
        }, {
            title: '操作',
            titlealign: 'left',
            valign: 'middle',
            align: 'center',
            formatter: function(value, row, index) {
                return operation(value,row)
            }
        }],
        onLoadSuccess: function() { //加载成功时执行
            console.info("加载成功");
            add();
        },
        onLoadError: function() { //加载失败时执行
            console.info("加载数据失败");
        },

        //>>>>>>>>>>>>>>导出excel表格设置
        showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
        exportDataType: "basic", //basic', 'all', 'selected'.
        exportTypes: ['excel', 'xlsx'], //导出类型
        //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
        exportOptions: {
            ignoreColumn: [14],            //忽略某一列的索引
            fileName: '产品信息', //文件名称设置
            worksheetName: 'Sheet1', //表格工作区名称
            tableName: '商品数据表',
            excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
            //onMsoNumberFormat: DoOnMsoNumberFormat
        }
        //导出excel表格设置<<<<<<<<<<<<<<<<

    });
})
//单元格空处理
function isnull(value,row,index) {
    if (value == null)
        return "";
    else
        return value;
}
//表格自动序号
function tableIndexNum(index){
    var currentPage=$(".page-item.active").find('a').text();
    return Number(index+1+eval((currentPage-1)*$(".page-list .page-size").val()));
}
//表单数据转json对象
function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function() {
        if (this.value != null && this.value.length >0)
            d[this.name] = this.value;
    });
    return d;
}