function initDetail(id,dom) {
    dom.bootstrapTable({
        method: 'get',
        striped: true,

        cache: false,
        pagination: false,
        sortable: false,
        sortOrder: "desc",
        pageNumber: 1,
        pageSize: 10,
        url: "/sale/order/detail?id="+id,
        sidePagination: "server",
        queryParamsType: '',

        columns: [{
            field: 'productName',
            title: '品名',
            align: 'center',
            formatter:isnull
//			footerFormatter: "合计"
        }, {
            field: 'productModalName',
            title: '型号',
            align: 'center',
            formatter:isnull
        }, {
            field: 'formaterName',
            title: '规格',
            align: 'center',
            formatter:isnull
        }, {
            field: 'util',
            title: '单位',
            align: 'center',
            formatter:isnull
        }, {
            field: 'number',
            title: '数量',
            align: 'center',
            formatter:isnull
//			footerFormatter: "合计"
        }, {
            field: 'pay',
            title: '已交付',
            align: 'center',
            formatter:isnull
        }, {
            field: 'noPay',
            title: '未交付',
            align: 'center',
            formatter:isnull
        }, {
            field: 'price',
            title: '单价',
            align: 'center',
            formatter:function (value,row,index) {
                if (value != null)
                    return fmoney(value,2);
                else
                    return "";
            }
        },{
            field: 'price',
            title: '折扣',
            align: 'center',
            formatter:function (value,row,index) {
                if (value != null ) {

                    var add = (-1*(row.number*row.price-row.total));
                    if (row.number*row.price == 0 || add == 0)
                        return "";
                    if (add > 0)
                        return "<span style='color: red'>"+fmoney(add,2)+"</span>("+100*(row.total/(row.number*row.price))+"%)";
                    else
                        return "<span style='color: red'>"+fmoney(add,2)+"</span>("+100*(row.total/(row.number*row.price))+"%)";
                }else
                    return "";
            }
//			footerFormatter: "合计"
        }, {
            field: 'total',
            title: '金额',
            align: 'center',
            formatter:function (value,row,index) {
                if (value != null)
                    return fmoney(value,2);
                else
                    return "";
            }
//			footerFormatter: "合计"
        },  {
            field: 'other',
            title: '备注',
            align: 'center',
            formatter:isnull
        }],
        onLoadSuccess: function(data) {
            if (data.length > 0) {
                var total = 0;
                var n = 0;
                for (var i = 0; i < data.length; i++) {
                    total += parseFloat(data[i].total);
                    n += parseFloat(data[i].number)
                }
                addFotter(dom, {
                    productName: '合计',
                    number: n,
                    total: fmoney(total, 2),
                })

                addFotter(dom, {
                    productName: '合计(大写金额)',
                    other: digit_uppercase(total),
                });
                formatterFotter(dom, 'number', 1, 2);
                formatterFotter(dom, 'other', 1, 1);
                for (var i = 0; i < data.length; i++)
                    if (data[i].noPay > 0)
                        $('#outGoods').removeClass("hidden")
            }
        },
        onLoadError: function(status) {
        }
    })
}
function addFotter(table, json) {
    table.bootstrapTable('append', json);
}

function formatterFotter(table, field, colspan, num) {
    table.bootstrapTable('mergeCells', {
        index: getLength(table) - num + 1,
        field: field,
        colspan: colspan
    });
}

function getLength(table) {
    return table.bootstrapTable('getData', true).length - 1;
}
function isnull(value,row,index) {
    if (value == null)
        return "";
    else
        return value;
}