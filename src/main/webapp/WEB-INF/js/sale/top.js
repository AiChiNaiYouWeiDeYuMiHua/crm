function initTop(obj) {
    /**
     * top
     */
    var chartTop = echarts.init(document.getElementById('top'));
    var log = "";
    var content = "";
    for(var i=0;i<obj.length;i++){
        if(obj.length == 1){
            log = log+"[\'"+obj[i].TYPE+"\']";
        }else if(obj.length != 1 && i == 0){
            log = log+"[\'"+obj[i].TYPE+"\',"
        }else if(obj.length != 1 && i != obj.length-1){
            log = log + "\'" + obj[i].TYPE + "\',";
        }else if(obj.length != 1 && i == obj.length-1){
            log = log + "\'" + obj[i].TYPE + "\']";
        }
    }

    var max =  0;
    for(var i=0;i<obj.length;i++){
        if (obj[i] > max)
            max = obj[i]
        if(obj.length == 1){
            content = content+"[{value :"+obj[i].COUNT+",name:\'"+obj[i].TYPE+"\'}]";
        }else if(obj.length != 1 && i == 0){
            content = content+"[{value :"+obj[i].COUNT+",name:\'"+obj[i].TYPE+"\'},"
        }else if(obj.length != 1 && i != obj.length-1){
            content = content + "{value :"+obj[i].COUNT+",name:\'"+obj[i].TYPE+"\'},";
        }else if(obj.length != 1 && i == obj.length-1){
            content = content + "{value :"+obj[i].COUNT+",name:\'"+obj[i].TYPE+"\'}]";
        }
    }
    var log1 = eval('(' + log + ')');
    var content1 = eval('(' + content + ')')
    var yMax = max;
    var dataShadow = [];

    for(var i = 0; i < obj.length; i++) {
        dataShadow.push(yMax);
    }

    option = {
        title: {
            text: 'top'
        },
        xAxis: {
            data: log1,
            axisLabel: {
                inside: true,
                textStyle: {
                    color: '#fff'
                }
            },
            axisTick: {
                show: false
            },
            axisLine: {
                show: false
            },
            z: 10
        },
        yAxis: {
            axisLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                textStyle: {
                    color: '#999'
                }
            }
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        dataZoom: [{
            type: 'inside'
        }],
        series: [{ // For shadow
            type: 'bar',
            itemStyle: {
                normal: {
                    color: 'rgba(0,0,0,0.05)'
                }
            },
            barGap: '-100%',
            barCategoryGap: '40%',
            data: dataShadow,
            animation: false
        },
            {
                type: 'bar',
                itemStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(
                            0, 0, 0, 1, [{
                                offset: 0,
                                color: '#83bff6'
                            },
                                {
                                    offset: 0.5,
                                    color: '#188df0'
                                },
                                {
                                    offset: 1,
                                    color: '#188df0'
                                }
                            ]
                        )
                    },
                    emphasis: {
                        color: new echarts.graphic.LinearGradient(
                            0, 0, 0, 1, [{
                                offset: 0,
                                color: '#2378f7'
                            },
                                {
                                    offset: 0.7,
                                    color: '#2378f7'
                                },
                                {
                                    offset: 1,
                                    color: '#83bff6'
                                }
                            ]
                        )
                    }
                },
                data: content1
            }
        ]
    };
    chartTop.setOption(option);
    // Enable data zoom when user click bar.
    var zoomSize = 6;
    chartTop.on('click', function(params) {
        console.log(dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)]);
        chartTop.dispatchAction({
            type: 'dataZoom',
            startValue: dataAxis[Math.max(params.dataIndex - zoomSize / 2, 0)],
            endValue: dataAxis[Math.min(params.dataIndex + zoomSize / 2, data.length - 1)]
        });
    });

}