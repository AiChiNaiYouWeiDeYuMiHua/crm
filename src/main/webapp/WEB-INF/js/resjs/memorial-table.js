//本月纪念日table
var kid = "";
var all = "";

function init() {
    kid = "";
    all = "";
    $("#reset2").css("display", "none");
    $("#memorial-table").bootstrapTable('refresh');
    $('#select_data').selectpicker('val', "全部数据");
    $('input[id="all"]').val("");
}


function getKid(obj) {
    var kidData = obj.options[obj.selectedIndex].value;
    $("#reset2").css("display", "block");
    kid = kidData;
    $("#memorial-table").bootstrapTable('refresh');
}

function cotsclick() {
    var allDa = $("#all").val();
    all = allDa;
    $("#reset2").css("display", "block");
    $("#memorial-table").bootstrapTable('refresh');
}


//联系人table
var d = {};
$("#memorial-table").bootstrapTable({ // 对应table标签的id
    url: 'memorial/memorial_list',
    sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
    pagination: true, //是否显示分页（*）
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 5, //每页的记录行数（*）

    contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
    //search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
    strictSearch: false, //是否全局匹配,false模糊匹配
    minimumCountColumns: 2,
    queryParams: function (params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
        d["limit"] = params.limit;
        d["offset"] = ((params.offset) / params.limit) + 1;
        d["kid"] = kid;
        d["all"] = all;
        return d;
        //sort: params.sort, // 要排序的字段
        //sortOrder: params.order, // 排序规则
        //dataId: $("#dataId").val() // 额外添加的参数

    }, //传递参数（*）
    columns: [{
        checkbox: 'true',
        width: 30,
        align: 'center',
        valign: 'middle'
    }, {
        field: 'medId',
        title: '序号', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 50,
        valign: 'middle'
    }, {
        field: 'medType',
        title: '纪念日类型', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            return [
                '<a data-toggle="modal" data-target="#med_detail" href="memorial_detail?id='+row.medId+'" style="cursor:pointer;text-decoration: none" data-toggle="tooltip" data-placement="bottom"  title="视图"><i class="fa fa-arrow-circle-right" style="margin-right: 5px"></i>'+row.medType+'</a>'
            ].join('');
        }
    }, {
        field: 'tbCustomerByCusId.cusName', // 返回json数据中的name
        title: '客户', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle',// 上下居中
        formatter: function (value, row, index) {
            return [
                '<a href="javascript:window.open(\'customer_details?id=' + row.tbCustomerByCusId.cusId + '\')" style="cursor:pointer;text-decoration: none" data-toggle="tooltip" data-placement="bottom"  title="视图">〖'+row.tbCustomerByCusId.cusName+'&nbsp;<i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i>〗</a>'
            ].join('');
        }
    }, {
        field: 'tbContactsByCotsId.cotsName', // 返回json数据中的name
        title: '联系人', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            return [
                '<a href="javascript:window.open(\'contact_detail?id='+row.tbContactsByCotsId.cotsId+'\')" style="cursor:pointer;text-decoration: none" data-toggle="tooltip" data-placement="bottom"  title="视图">〖'+row.tbContactsByCotsId.cotsName+'&nbsp;<i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i>〗</a>'
            ].join('');
        }
    }, {
        field: 'medMemorialDate', // 返回json数据中的name
        title: '纪念日', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medNextPoint', // 返回json数据中的name
        title: '下次提醒', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medCreatetimr', // 返回json数据中的name
        title: '创建日期', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    },{
        title: '操作',
        titlealign: 'center',
        align: 'center',
        valign: 'middle', // 上下居中
        width: 60,
        formatter:medButton
    }],
    responseHandler: function (res) {
        if (res.total != 0) {
            var row = res.rows;
            length = res.total;
            page = parseInt((res.total / 5) + 1);
            return {
                "rows": row,
                "total": res.total
            };
        } else {
            return {
                "rows": [],
                "total": 0
            };
        }
    },
    onLoadSuccess: function () { //加载成功时执行
        $(".pagination-detail").before("<div><button onclick='deleteAllCus()' class='btn btn-default pull-left' style='margin-top: 10px;margin-right: 5px;color:black'><i class='fa fa-trash'></i></button></div>");
    },
    onLoadError: function () { //加载失败时执行
        console.info("加载数据失败");
    }
});


function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}

function deleteCus(medId) {
    swal({
            title: "确定删除吗？",
            text: "你将无法恢复该纪念日！",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定删除！",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: 'memorial/delete_memorial',
                method: 'post',
                async: true,
                data: {'id': medId},
                success: function (data) {
                    if (data.status == 400) {
                        swal("成功！", "纪念日删除失败！", "cancle");
                    } else {
                        swal("成功！", "纪念日删除成功！", "success");
                        $("#cots-table").bootstrapTable('refresh');
                    }
                },
                error: function (data) {
                    alert("操作失败");
                    swal("失败！", "纪念日删除失败！" + data.msg, "Cancel")
                }
            });
        });
}

function deleteAllCus() {
    var datas = $('#memorial-table').bootstrapTable("getSelections");
    if (datas.length > 0) {
        var str = [];
        for (var i = 0; i < datas.length; i++) {
            var id = datas[i].medId;
            str.push(id)
        }
        swal({
                title: "确定删除吗？",
                text: "将要删除选中的" + datas.length + "个联系人",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function () {
                alert(str);
                $.ajax({
                    url: "memorial/delete_all",
                    method: 'post',
                    data: {ids: str},
                    success: function (data) {
                        if (data.status == 400) {
                            swal("成功！", "纪念日删除失败！", "cancle");
                        } else {
                            swal("成功！", "纪念日删除成功！", "success");
                            $("#memorial-table").bootstrapTable('refresh');
                        }
                    },
                    error: function (data) {
                        alert("操作失败");
                        swal("失败！", "纪念日删除失败！" + data.msg, "Cancel")
                    }
                });
            });
    }
}