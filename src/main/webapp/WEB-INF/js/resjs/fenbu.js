
function statisticfenbu(obj) {
    var chart1 = echarts.init(document.getElementById('fenbu'));
    var log = "";
    var content = "";
    for(var i=0;i<obj.length;i++){
        if(obj.length == 1){
            log = log+"[\'"+obj[i].TYPE+"\']";
        }else if(obj.length != 1 && i == 0){
            log = log+"[\'"+obj[i].TYPE+"\',"
        }else if(obj.length != 1 && i != obj.length-1){
            log = log + "\'" + obj[i].TYPE + "\',";
        }else if(obj.length != 1 && i == obj.length-1){
            log = log + "\'" + obj[i].TYPE + "\']";
        }
    }

    for(var i=0;i<obj.length;i++){
        if(obj.length == 1){
            content = content+"[{value :"+obj[i].COUNT+",name:\'"+obj[i].TYPE+"\'}]";
        }else if(obj.length != 1 && i == 0){
            content = content+"[{value :"+obj[i].COUNT+",name:\'"+obj[i].TYPE+"\'},"
        }else if(obj.length != 1 && i != obj.length-1){
            content = content + "{value :"+obj[i].COUNT+",name:\'"+obj[i].TYPE+"\'},";
        }else if(obj.length != 1 && i == obj.length-1){
            content = content + "{value :"+obj[i].COUNT+",name:\'"+obj[i].TYPE+"\'}]";
        }
    }
    var log1 = eval('(' + log + ')');
    var content1 = eval('(' + content + ')')
    option = {
        title: {
            text: '分布',
            left: 'left'
        },
        /**
         * 保存为图片
         */
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            // orient: 'vertical',
            // top: 'middle',
            bottom: 10,
            left: 'center',
            data: log1
        },
        series: [{
            type: 'pie',
            radius: '65%',
            center: ['50%', '50%'],
            selectedMode: 'single',
            data: content1,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }]
    };
    chart1.setOption(option);
}
