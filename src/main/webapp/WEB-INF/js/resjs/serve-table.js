//本月纪念日table
var kid = "";
var all = "";

function init() {
    kid = "";
    all = "";
    $("#reset2").css("display", "none");
    $("#serve-table").bootstrapTable('refresh');
    $('#select_data').selectpicker('val', "全部数据");
    $('input[id="all"]').val("");
}

$("[data-toggle='tooltip']").tooltip();

function getKid(obj) {
    var kidData = obj.options[obj.selectedIndex].value;
    $("#reset2").css("display", "block");
    kid = kidData;
    $("#serve-table").bootstrapTable('refresh');
}

function cotsclick() {
    var allDa = $("#all").val();
    all = allDa;
    $("#reset2").css("display", "block");
    $("#serve-table").bootstrapTable('refresh');
}


//联系人table
var d = {};
$("#serve-table").bootstrapTable({ // 对应table标签的id
    url: 'serve/serve_list',
    sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
    pagination: true, //是否显示分页（*）
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 5, //每页的记录行数（*）

    contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
    //search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
    strictSearch: false, //是否全局匹配,false模糊匹配
    minimumCountColumns: 2,
    queryParams: function (params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
        d["limit"] = params.limit;
        d["offset"] = ((params.offset) / params.limit) + 1;
        d["kid"] = kid;
        d["all"] = all;
        return d;
        //sort: params.sort, // 要排序的字段
        //sortOrder: params.order, // 排序规则
        //dataId: $("#dataId").val() // 额外添加的参数

    }, //传递参数（*）
    columns: [{
        checkbox: 'true',
        width: 30,
        align: 'center',
        valign: 'middle'
    }, {
        field: 'serveId',
        title: '序号', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 50,
        valign: 'middle'
    }, {
        field: 'serveTheme',
        title: '主题', // 表格表头显示文字
        width: 330,
        align: 'center', // 左右居中
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            return '<a data-toggle="modal" data-target="#serve_detail" href="serve_detail?id='+row.serveId+'" style="text-decoration: none;"><i class="fa fa-arrow-circle-right" style="margin-right: 5px"></i>' + row.serveTheme + '</a>'
        }
    }, {
        field: 'tbCustomerByCusId.cusName', // 返回json数据中的name
        title: '对应客户', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle',// 上下居中
        formatter: function (value, row, index) {
            return '<a style="text-decoration: none;" href="javascript:window.open(\'customer_details?id=' + row.tbCustomerByCusId.cusId + '\')">〖' + row.tbCustomerByCusId.cusName + '&nbsp;<i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i>〗</a>'
        }
    }, {
        field: 'serveType', // 返回json数据中的name
        title: '服务类型', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'serveWay', // 返回json数据中的name
        title: '服务方式', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'serveStartTime', // 返回json数据中的name
        title: '开始日期', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'tbUserByUserId.userName', // 返回json数据中的name
        title: '执行人', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'serveTimeSpent', // 返回json数据中的name
        title: '花费时间', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'serveState', // 返回json数据中的name
        title: '状态', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            if (value == "未处理") {
                return '<i data-toggle="tooltip" data-placement="bottom" data-title="未处理" class="f-s-16 fa fa-exclamation text-warning" data-original-title="" title="" style="margin-right: 20px"></i>';
            } else if (value == "处理中") {
                return '<i data-toggle="tooltip" data-placement="bottom" data-title="处理中" class="f-s-16 fa fa-arrow-right text-primary" data-original-title="" title="" style="margin-right: 20px"></i>';
            } else if (value == "处理完成") {
                return '<i data-toggle="tooltip" data-placement="bottom" data-title="处理完成" class="f-s-16 fa fa-check text-custom" data-original-title="" title="" style="margin-right: 20px"></i>';
            } else if (value == "无需处理") {
            }

        }
    }, {
        title: '操作',
        titlealign: 'center',
        align: 'center',
        valign: 'middle', // 上下居中
        width: 60,
        formatter: serveButton
    }],
    responseHandler: function (res) {
        if (res.total != 0) {
            var row = res.rows;
            length = res.total;
            page = parseInt((res.total / 5) + 1);
            return {
                "rows": row,
                "total": res.total
            };
        } else {
            return {
                "rows": [],
                "total": 0
            };
        }
    },
    onLoadSuccess: function () { //加载成功时执行
        add();
    },
    onLoadError: function () { //加载失败时执行
        console.info("加载数据失败");
    }
});


function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}

function deleteCus(medId) {
    swal({
            title: "确定删除吗？",
            text: "你将无法恢复该客户服务！",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定删除！",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: 'serve/delete_serve',
                method: 'post',
                async: true,
                data: {'id': medId},
                success: function (data) {
                    if (data.status == 400) {
                        swal("成功！", "客户服务删除失败！", "cancle");
                    } else {
                        swal("成功！", "客户服务删除成功！", "success");
                        $("#serve-table").bootstrapTable('refresh');
                    }
                },
                error: function (data) {
                    alert("操作失败");
                    swal("失败！", "客户服务删除失败！" + data.msg, "Cancel")
                }
            });
        });
}

function deleteAllCus() {
    var datas = $('#serve-table').bootstrapTable("getSelections");
    if (datas.length > 0) {
        var str = [];
        for (var i = 0; i < datas.length; i++) {
            var id = datas[i].medId;
            str.push(id)
        }
        swal({
                title: "确定删除吗？",
                text: "将要删除选中的" + datas.length + "个客户服务",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: "serve/delete_all",
                    method: 'post',
                    data: {ids: str},
                    success: function (data) {
                        if (data.status == 400) {
                            swal("成功！", "客户服务删除失败！", "Cancle");
                        } else {
                            swal("成功！", "客户服务删除成功！", "success");
                            $("#serve-table").bootstrapTable('refresh');
                        }
                    },
                    error: function (data) {
                        alert("操作失败");
                        swal("失败！", "客户服务删除失败！" + data.msg, "Cancel")
                    }
                });
            });
    }
}

