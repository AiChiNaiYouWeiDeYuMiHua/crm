//本月纪念日table
var kid = "";
var bjxLetter = "";
var bjxFirstName = "";
var all = "";

function init() {
    kid = "";
    bjxLetter = "";
    bjxFirstName = "";
    all = "";
    $("#cots-table").bootstrapTable('refresh');
    $("#reset2").css("display", "none");
    $('#select_data').selectpicker('val', "全部数据");
    $('input[id="all"]').val("");
}

function bjxLetterfun(obj) {
    bjxLetter = obj;
    $("#reset2").css("display", "block");
    TweenMax.to(window, 1.5, {scrollTo: 0, ease: Expo.easeInOut});
    $("#cots-table").bootstrapTable('refresh');
}

function bjxFirstNamefun(obj) {
    bjxFirstName = obj;
    $("#reset2").css("display", "block");
    TweenMax.to(window, 1.5, {scrollTo: 0, ease: Expo.easeInOut});
    $("#cots-table").bootstrapTable('refresh');
}

function getKid(obj) {
    var kidData = obj.options[obj.selectedIndex].value;
    $("#reset2").css("display", "block");
    kid = kidData;
    $("#cots-table").bootstrapTable('refresh');
}

function cotsclick() {
    var allDa = $("#all").val();
    all = allDa;
    $("#reset2").css("display", "block");
    $("#cots-table").bootstrapTable('refresh');
}

$("#this_mouth").bootstrapTable({ // 对应table标签的id
    url: 'memorial/memorial_list',
    sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
    pagination: true, //是否显示分页（*）
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 3, //每页的记录行数（*）

    contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
    //search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
    strictSearch: false, //是否全局匹配,false模糊匹配
    minimumCountColumns: 2,
    queryParams: function (params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
        var b = {};
        b["limit"] = params.limit;
        b["offset"] = ((params.offset) / params.limit) + 1;
        b["kid"] = "本月纪念日提醒";
        return b;
        //sort: params.sort, // 要排序的字段
        //sortOrder: params.order, // 排序规则
        //dataId: $("#dataId").val() // 额外添加的参数

    }, //传递参数（*）
    columns: [{
        field: 'tbContactsByCotsId.cotsName',
        title: '姓名', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medMemorialDate', // 返回json数据中的name
        title: '纪念日', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medType', // 返回json数据中的name
        title: '类型', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medMemorialDate', // 返回json数据中的name
        title: '纪念日', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medRemark', // 返回json数据中的name
        title: '备注', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }],
    onLoadSuccess: function () { //加载成功时执行

    },
    onLoadError: function () { //加载失败时执行
        console.info("加载数据失败");
    }
});

//下月纪念日

$("#next_mouth").bootstrapTable({ // 对应table标签的id
    url: 'memorial/memorial_list',
    sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
    pagination: true, //是否显示分页（*）
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 3, //每页的记录行数（*）

    contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
    //search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
    strictSearch: false, //是否全局匹配,false模糊匹配
    minimumCountColumns: 2,
    queryParams: function (params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
        var c = {};
        c["limit"] = params.limit;
        c["offset"] = ((params.offset) / params.limit) + 1;
        c["kid"] = "下月纪念日提醒";
        return c;
        //sort: params.sort, // 要排序的字段
        //sortOrder: params.order, // 排序规则
        //dataId: $("#dataId").val() // 额外添加的参数

    }, //传递参数（*）
    columns: [{
        field: 'tbContactsByCotsId.cotsName',
        title: '姓名', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medMemorialDate', // 返回json数据中的name
        title: '纪念日', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medType', // 返回json数据中的name
        title: '类型', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medMemorialDate', // 返回json数据中的name
        title: '纪念日', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'medRemark', // 返回json数据中的name
        title: '备注', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }],
    onLoadSuccess: function () { //加载成功时执行
    },
    onLoadError: function () { //加载失败时执行
        console.info("加载数据失败");
    }
});


//联系人table
var d = {};
var length;//总数据
var page;//总页数
$("#cots-table").bootstrapTable({ // 对应table标签的id
    url: 'contact/contact_list',
    sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
    pagination: true, //是否显示分页（*）
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 5, //每页的记录行数（*）

    contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
    //search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
    strictSearch: false, //是否全局匹配,false模糊匹配
    minimumCountColumns: 2,
    queryParams: function (params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
        d["limit"] = params.limit;
        d["offset"] = ((params.offset) / params.limit) + 1;
        d["kid"] = kid;
        d["all"] = all;
        d["bjxLetter"] = bjxLetter;
        d["bjxFirstName"] = bjxFirstName;
        return d;
        //sort: params.sort, // 要排序的字段
        //sortOrder: params.order, // 排序规则
        //dataId: $("#dataId").val() // 额外添加的参数

    }, //传递参数（*）
    columns: [{
        checkbox: 'true',
        width: 30,
        align: 'center',
        valign: 'middle'
    }, {
        field: 'cotsId',
        title: '序号', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 50,
        valign: 'middle'
    }, {
        field: 'cotsName',
        title: '姓名', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            return '<a style="text-decoration: none" href="javascript:window.open(\'contact_detail?id='+row.cotsId+'\')"><i class="fa fa-arrow-circle-right text-blue" style="margin-right: 5px"></i>'+row.cotsName+'</a>'
        }
    }, {
        field: 'cotsPhoto', // 返回json数据中的name
        title: '照片', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            if (value != null && value != "") {
                return '<img id="cots_photo' + index + '" src="/img/' + value + ' " style="width: 60px;height: 35px;display: block"/>'
            } else {
                return '<img id="cots_photo' + index + '" src="/img/' + value + ' " style="width: 60px;height: 35px;display: none"/>'
            }

        }
    }, {
        field: 'cotsWorkphone', // 返回json数据中的name
        title: '工作电话', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'cotsPersonalPhone', // 返回json数据中的name
        title: '移动电话', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'cotsPlace', // 返回json数据中的name
        title: '邮件地址', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        field: 'tbCustomerByCusId.cusName', // 返回json数据中的name
        title: '对应客户', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            return [
                '<a href="javascript:window.open(\'customer_details?id=' + row.tbCustomerByCusId.cusId + '\')" style="cursor:pointer;text-decoration: none" data-toggle="tooltip" data-placement="bottom"  title="视图">〖'+value+'&nbsp;<i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i>〗</a>'
            ].join('');
        }
    }, {
        field: 'cotsClassification', // 返回json数据中的name
        title: '联系人分类', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle' // 上下居中
    }, {
        title: '操作',
        titlealign: 'center',
        align: 'center',
        valign: 'middle', // 上下居中
        width: 60,
        formatter: cotsButton
    }],
    responseHandler: function (res) {
        if (res.total != 0) {
            var row = res.rows;
            length = res.total;
            page = parseInt((res.total / 5) + 1);
            return {
                "rows": row,
                "total": res.total
            };
        } else {
            return {
                "rows": [],
                "total": 0
            };
        }
    },
    onLoadSuccess: function () { //加载成功时执行
        //动态对应图片
        if ($('#cots-table').bootstrapTable('getOptions').pageNumber == page && length != 0) {
            var rowbb = length - 5 * (page - 1);
            for (var i = 0; i < rowbb; i++) {
                var viewer = new Viewer(document.getElementById('cots_photo' + i));
            }
        } else {
            for (var i = 0; i < 5; i++) {
                var viewer = new Viewer(document.getElementById('cots_photo' + i + ''));
            }
        }
    },
    onLoadError: function () { //加载失败时执行
        console.info("加载数据失败");
    }
});


function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}

function deleteCus(cusId) {
    swal({
            title: "确定删除吗？",
            text: "你将无法恢复该联系人！",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定删除！",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: 'contact/delete_contact',
                method: 'post',
                async: true,
                data: {'id': cusId},
                success: function (data) {
                    if (data.status == 400) {
                        swal("成功！", "联系人删除失败！", "cancle");
                    } else {
                        swal("成功！", "联系人删除成功！", "success");
                        $("#cots-table").bootstrapTable('refresh');
                    }
                },
                error: function (data) {
                    alert("操作失败");
                    swal("失败！", "客户删除失败！" + data.msg, "Cancel")
                }
            });
        });
}

function deleteAllCus() {
    var datas = $('#cots-table').bootstrapTable("getSelections");
    if (datas.length > 0) {
        var str = [];
        for (var i = 0; i < datas.length; i++) {
            var id = datas[i].cotsId;
            str.push(id)
        }
        swal({
                title: "确定删除吗？",
                text: "将要删除选中的" + datas.length + "个联系人",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: "contact/delete_all",
                    method: 'post',
                    data: {ids: str},
                    success: function (data) {
                        if (data.status == 400) {
                            swal("成功！", "联系人删除失败！", "cancle");
                        } else {
                            swal("成功！", "联系人删除成功！", "success");
                            $("#cots-table").bootstrapTable('refresh');
                        }
                    },
                    error: function (data) {
                        alert("操作失败");
                        swal("失败！", "联系人删除失败！" + data.msg, "Cancel")
                    }
                });
            });
    }
}