function InitExcelFile(obj) {
    alert(obj);
    $("#upload").css("display","block");
    //记录GUID
    //          $("#AttachGUID").val(newGuid());
    $("#excelFile").fileinput({
        uploadUrl: "/files_upload", //上传的地址
        uploadAsync: true, //异步上传
        language: "zh", //设置语言
        showCaption: true, //是否显示标题
        showUpload: true, //是否显示上传按钮
        showRemove: true, //是否显示移除按钮
        showPreview: true, //是否显示预览按钮
        browseClass: "btn btn-primary", //按钮样式
        dropZoneEnabled: false, //是否显示拖拽区域
        // allowedFileExtensions: ["jpg", "png",'gif'], //接收的文件后缀
        enctype: 'multipart/form-data',
        maxFileCount: 5, //最大上传文件数限制
        previewFileIcon: '<i class="fa fa-file"></i>',
        allowedPreviewTypes: null,
        dropZoneEnabled: true,//是否显示拖拽区域
        minImageWidth: 50, //图片的最小宽度
        minImageHeight: 50,//图片的最小高度
        maxImageWidth: 1000,//图片的最大宽度
        maxImageHeight: 1000,//图片的最大高度
        maxFileSize: 0,//单位为kb，如果为0表示不限制文件大小
        minFileCount: 0
    });

    $('#excelFile').on("fileuploaded", function(event, data, previewId, index) {
        var result = data.response; //后台返回的json
        $.ajax({//上传文件成功后再保存图片信息
            url:'files_upload',
            type:'post',
            dataType:'json',
            data:$('#ffImport').serialize(),//form表单的值
            success:function(){
            },
            cache:false,                    //不缓存
        });
        alert(result);
        if($('#fj1').val() == null|| $('#fj1').val() == ""){
            $('#fj1').removeAttr("disabled");
            $('#fj1').parent().parent().parent().parent().css("display","block");
            $('#fj1').val(result);
        }else if($('#fj2').val() == null|| $('#fj2').val() == "") {
            $('#fj2').removeAttr("disabled");
            $('#fj2').parent().parent().parent().parent().css("display","block");
            $('#fj2').val(result);
        }else if($('#fj3').val() == null|| $('#fj3').val() == "") {
            $('#fj3').removeAttr("disabled");
            $('#fj3').parent().parent().parent().parent().css("display","block");
            $('#fj3').val(result);
        }else if($('#fj4').val() == null|| $('#fj4').val() == "") {
            $('#fj4').removeAttr("disabled");
            $('#fj4').parent().parent().parent().parent().css("display","block");
            $('#fj4').val(result);
        }else if($('#fj5').val() == null|| $('#fj5').val() == "") {
            $('#fj5').removeAttr("disabled");
            $('#fj5').parent().parent().parent().parent().css("display","block");
            $('#fj5').val(result);
        }
    });

}





function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}