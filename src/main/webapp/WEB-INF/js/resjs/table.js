var familyDa = "";
var kidDa = "全部数据";
var cusLifeCycle = "全部客户";
var all = "";

function init() {
    $("#navbar1").find('li').each(function () {
        $('li').removeClass();
        $('#first').addClass('active ul_i');
    })
    $("#input_data").val("");
    cusLifeCycle = "全部客户";
    all = "";
    familyDa = "";
    kidDa = "全部数据";
    $("#select_data").selectpicker('val', '全部数据');
    $('#select_data').selectpicker('render');
    $("#table111").bootstrapTable('refresh');
}


$("#navbar1").find('li').each(function () {
    cusLifeCycle = "全部客户";
    $(this).click(function () {
        $('li').removeClass();
        $(this).addClass('active ul_i');
        cusLifeCycle = $(this).text();
        $("#table111").bootstrapTable('refresh');
    })
})

function getFamilyAndKid(obj) {
    var kidData = obj.options[obj.selectedIndex].value;
    var familyData = obj.options[obj.selectedIndex].parentNode.attributes;

    kidDa = kidData;
    if (familyData["label"] != null) {
        familyDa = familyData["label"].value;
    } else {
        familyDa = "";
    }
    $("#table111").bootstrapTable('refresh');
}

function cusclick() {
    var allDa = $("#input_data").val();
    all = allDa;
    $("#table111").bootstrapTable('refresh');
}

function showReset(obj) {
    if (obj == 1) {
        $("#reset1").css('display', 'block');
    } else {
        $("#reset1").css('display', 'none');
    }
}

var d = {};
$("#table111").bootstrapTable({ // 对应table标签的id
    url: "customer/kid_of_find", //AJAX获取表格数据的url
    striped: true, //是否显示行间隔色(斑马线)
    pagination: true, //是否显示分页（*）
    sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
    paginationLoop: false, //当前页是边界时是否可以继续按
    showFooter: true,
    queryParams: function (params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
        d["limit"] = params.limit;
        d["offset"] = ((params.offset) / params.limit) + 1;
        d["lifeCycle"] = cusLifeCycle;
        d["family"] = familyDa;
        d["kid"] = kidDa;
        d["all"] = all;
        // sort: params.sort,      //排序列名
        //     sortOrder: params.order //排位命令（desc，asc）
        return d;
        //sort: params.sort, // 要排序的字段
        //sortOrder: params.order, // 排序规则
        //dataId: $("#dataId").val() // 额外添加的参数

    }, //传递参数（*）
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 10, //每页的记录行数（*）

    contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
    //search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
    strictSearch: false, //是否全局匹配,false模糊匹配
    //					showColumns: true, //是否显示所有的列
    //					showRefresh: true, //是否显示刷新按钮
    minimumCountColumns: 2, //最少允许的列数
    //					clickToSelect: true, //是否启用点击选中行
    //height: 500,                      //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
    //uniqueId: "id",                   //每一行的唯一标识，一般为主键列
    //					showToggle: true, //是否显示详细视图和列表视图的切换按钮
    //					cardView: false, //是否显示详细视图
    detailView: false, //是否显示父子表
    cache: false, // 设置为 false 禁用 AJAX 数据缓存， 默认为true
    sortable: true, //是否启用排序
    sortOrder: "asc", //排序方式
    sortName: 'cusId', // 要排序的字段
    columns: [{
        checkbox: 'true',
        width: 60,
        align: 'center',
        footerFormatter: function (value) {
            return "合计";
        }
    }, {
        field: 'cusId',
        title: '序号', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 80,
        valign: 'middle', // 上下居中
        sortable : true,
        formatter: function (value, row, index) {
            return tableIndexNum(index);
        }
    }, {
        field: 'cusName', // 返回json数据中的name
        title: '客户名称', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            return [
                '<a href="javascript:window.open(\'customer_details?id=' + row.cusId + '\')" style="cursor:pointer;text-decoration: none" data-toggle="tooltip" data-placement="bottom"  title="视图">〖'+value+'&nbsp;<i class="fa fa-folder-open m-l-5" data-toggle="tooltip" data-placement="bottom" data-original-title="打开详细页面"></i>〗</a>'
            ].join('');
        }

    }, {
        field: 'cusAbbreviation',
        title: '客户简称',
        align: 'center',
        valign: 'middle'
    }, {
        field: 'cusType',
        title: '客户种类',
        align: 'center',
        valign: 'middle',
        sortable: true
    }, {
        field: 'tbUserByUserId.userName',
        title: '录入员',
        align: 'center',
        valign: 'middle',
    }, {
        field: 'cusLifecycle',
        title: '生命周期 ',
        align: 'center',
        valign: 'middle',
    }, {
        field: 'cusDetermine',
        title: '定性',
        align: 'center',
        valign: 'middle',
    }, {
        field: 'cusClass',
        title: '定级',
        align: 'center',
        valign: 'middle',
    }, {
        field: 'cusSignDate',
        title: '定量',
        align: 'center',
        valign: 'middle',
    }, {
        title: '操作',
        titlealign: 'left',
        align: 'center',
        width: 80,
        formatter:cusButton
    }],

    onLoadSuccess: function () { //加载成功时执行
        var obj = 0;
        if (cusLifeCycle.trim() == "全部客户" && familyDa.trim() == "" && kidDa.trim() == "全部数据" && all.trim() == "") {
            obj = 0;
        } else {
            obj = 1;
        }
        showReset(obj);
        console.info("加载成功");
        add();
    },
    onLoadError: function () { //加载失败时执行
        console.info("加载数据失败");
    },

    //>>>>>>>>>>>>>>导出excel表格设置
    showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
    exportDataType: "basic", //basic', 'all', 'selected'.
    exportTypes: ['excel', 'xlsx'], //导出类型
    //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
    exportOptions: {
        // ignoreColumn: [0, 0],//忽略某一列的索引
        fileName: '数据导出', //文件名称设置
        worksheetName: 'Sheet1', //表格工作区名称
        tableName: '商品数据表',
        excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
        //onMsoNumberFormat: DoOnMsoNumberFormat
    }
    //导出excel表格设置<<<<<<<<<<<<<<<<

});

function tableIndexNum(index) {
    var currentPage = $(".page-item.active").find('a').text();
    return Number(index + 1 + eval((currentPage - 1) * 10));
}

function deleteCus(cusId) {
    swal({
            title: "确定删除吗？",
            text: "你将无法恢复该客户！",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定删除！",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                url: 'customer/delete_customer',
                method: 'post',
                async: true,
                data: {'id': cusId},
                success: function (data) {
                    alert('我成功了？' + data.msg);
                    swal("成功！", "客户删除成功！", "success");
                    $("#table111").bootstrapTable('refresh');
                },
                error: function (data) {
                    alert("操作失败");
                    swal("失败！", "客户添加失败！" + data.msg, "Cancel")
                }
            });
        });
}

function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}

function deleteAllCus() {
    var datas = $('#table111').bootstrapTable("getSelections");
    if (datas.length > 0) {
        var str = [];
        for (var i = 0; i < datas.length; i++) {
            var id = datas[i].cusId;
            str.push(id)
        }
        swal({
                title: "确定删除吗？",
                text: "将要删除选中的" + datas.length + "个客户",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定删除！",
                closeOnConfirm: false
            },
            function () {
            alert(str);
                $.ajax({
                    url: "customer/delete_all",
                    method: 'post',
                    data: {ids: str},
                    success: function (data) {
                        swal("成功！", "客户删除成功！", "success");
                        $("#table111").bootstrapTable('refresh')
                    },
                    error: function (data) {
                        alert("操作失败");
                        swal("失败！", "客户删除失败！" + data.msg, "Cancel")
                    }
                });
            });
    }
}