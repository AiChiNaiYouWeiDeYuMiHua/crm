var familyDa = "";
var kidDa = "全部数据";
var cusLifeCycle = "全部客户";
var all = "";

function init() {
    $("#input_data").val("");
    cusLifeCycle = "全部客户";
    all = "";
    familyDa = "";
    kidDa = "全部数据";
    $("#select_data").selectpicker('val', '全部数据');
    $('#select_data').selectpicker('render');
    $("#table111").bootstrapTable('refresh');
}


$("#navbar1").find('li').each(function () {
    cusLifeCycle = "全部客户";
    $(this).click(function () {
        $('li').removeClass();
        $(this).addClass('active ul_i');
        cusLifeCycle = $(this).text();
        $("#table111").bootstrapTable('refresh');
    })
})

function getFamilyAndKid(obj) {
    var kidData = obj.options[obj.selectedIndex].value;
    var familyData = obj.options[obj.selectedIndex].parentNode.attributes;

    kidDa = kidData;
    if (familyData["label"] != null) {
        familyDa = familyData["label"].value;
    } else {
        familyDa = "";
    }
    $("#table111").bootstrapTable('refresh');
}

function cusclick() {
    var allDa = $("#input_data").val();
    all = allDa;
    $("#table111").bootstrapTable('refresh');
}

function showReset(obj) {
    if (obj == 1) {
        $("#reset1").css('display', 'block');
    } else {
        $("#reset1").css('display', 'none');
    }
}

var d = {};
$("#table111").bootstrapTable({ // 对应table标签的id
    url: "customer/kid_of_find", //AJAX获取表格数据的url
    striped: true, //是否显示行间隔色(斑马线)
    pagination: true, //是否显示分页（*）
    sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
    paginationLoop: false, //当前页是边界时是否可以继续按
    queryParams: function (params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
        d["limit"] = params.limit;
        d["offset"] = ((params.offset) / params.limit) + 1;
        d["lifeCycle"] = cusLifeCycle;
        d["family"] = familyDa;
        d["kid"] = kidDa;
        d["all"] = all;
        return d;
        //sort: params.sort, // 要排序的字段
        //sortOrder: params.order, // 排序规则
        //dataId: $("#dataId").val() // 额外添加的参数

    }, //传递参数（*）
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 10, //每页的记录行数（*）

    contentType: "application/x-www-form-urlencoded",
    strictSearch: false,
    minimumCountColumns: 2,
    detailView: false, //是否显示父子表
    cache: false, // 设置为 false 禁用 AJAX 数据缓存， 默认为true
    sortable: true, //是否启用排序
    sortOrder: "asc", //排序方式
    sortName: 'sn', // 要排序的字段
    columns: [{
        field: 'cusId',
        title: '序号', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 80,
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            return tableIndexNum(index);
        }
    }, {
        field: 'cusName', // 返回json数据中的name
        title: '客户名称', // 表格表头显示文字
        align: 'center', // 左右居中
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            return [
                '<a href="#" style="cursor:pointer;text-decoration: none" data-toggle="tooltip" data-placement="bottom"  title="视图"><i class="fa fa-bookmark-o"></i>&nbsp;&nbsp;' + value + '</a>'
            ].join('');
        }
    }, {
        field: 'cusAbbreviation',
        title: '客户简称',
        align: 'center',
        valign: 'middle'
    }, {
        field: 'cusType',
        title: '客户种类',
        align: 'center',
        valign: 'middle',
        sortable: true
    }],

    onLoadSuccess: function () { //加载成功时执行
        var obj = 0;
        if (cusLifeCycle.trim() == "全部客户" && familyDa.trim() == "" && kidDa.trim() == "全部数据" && all.trim() == "") {
            obj = 0;
        } else {
            obj = 1;
        }
        showReset(obj);
        console.info("加载成功");
    },
    onLoadError: function () { //加载失败时执行
        console.info("加载数据失败");
    }

});

function tableIndexNum(index) {
    var currentPage = $(".page-item.active").find('a').text();
    return Number(index + 1 + eval((currentPage - 1) * 10));
}


function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}
