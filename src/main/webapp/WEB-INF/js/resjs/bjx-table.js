

$("#table-bjx").bootstrapTable({ // 对应table标签的id
    sidePagination: "client", //分页方式：client客户端分页，server服务端分页（*）
    paginationLoop: false, //当前页是边界时是否可以继续按
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 10, //每页的记录行数（*）

    contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
    //search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
    strictSearch: false, //是否全局匹配,false模糊匹配
    minimumCountColumns: 2,
    columns: [{
        field: 'letter',
        title: '字母', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 56,
        valign: 'middle' // 上下居中
    }, {
        field: 'firstName', // 返回json数据中的name
        title: '姓氏', // 表格表头显示文字
        align: 'left', // 左右居中
        width: 190,
        valign: 'middle' // 上下居中
    }],
    data: [{
        letter: '<a href="javascript:bjxLetterfun('+'\'a\''+')" style="text-decoration: none">A</a>',
        firstName: '<a href="javascript:bjxFirstNamefun('+'\'阿\''+')" style="text-decoration: none;margin-right: 10px">阿</a>' +
        '<a href="javascript:bjxFirstNamefun('+'\'安\''+')" style="text-decoration: none;margin-right: 10px">安</a>' +
        '<a href="javascript:bjxFirstNamefun('+'\'艾\''+')" style="text-decoration: none;margin-right: 10px">艾</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun('+'\'b\''+')" style="text-decoration: none">B</a>',
        firstName: '<a href="javascript:bjxFirstNamefun('+'\'白\''+')" style="text-decoration: none;margin-right: 10px">白</a>' +
        '<a href="javascript:bjxFirstNamefun('+'\'百\''+')" style="text-decoration: none;margin-right: 10px">百</a>' +
        '<a href="javascript:bjxFirstNamefun('+'\'搬\''+')" style="text-decoration: none;margin-right: 10px">搬</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun('+'\'c\''+')" style="text-decoration: none">C</a>',
        firstName: '<a href="javascript:bjxFirstNamefun('+'\'锤\''+')" style="text-decoration: none;margin-right: 10px">锤</a>' +
        '<a href="javascript:bjxFirstNamefun(\'陈\')" style="text-decoration: none;margin-right: 10px">陈</a>' +
        '<a href="javascript:bjxFirstNamefun(\'崔\')" style="text-decoration: none;margin-right: 10px">崔</a>'+
        '<a href="javascript:bjxFirstNamefun(\'楚\')" style="text-decoration: none;margin-right: 10px">楚</a>'+
        '<a href="javascript:bjxFirstNamefun(\'测\')" style="text-decoration: none;margin-right: 10px">测</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'d\')" style="text-decoration: none">D</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'杜\')" style="text-decoration: none;margin-right: 10px">杜</a>' +
        '<a href="javascript:bjxFirstNamefun(\'导\')" style="text-decoration: none;margin-right: 10px">导</a>' +
        '<a href="javascript:bjxFirstNamefun(\'电\')" style="text-decoration: none;margin-right: 10px">电</a>'+
        '<a href="javascript:bjxFirstNamefun(\'董\')" style="text-decoration: none;margin-right: 10px">董</a>'+
        '<a href="javascript:bjxFirstNamefun(\'邓\')" style="text-decoration: none;margin-right: 10px">邓</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'e\')" style="text-decoration: none">E</a>',
        firstName: '',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'f\')" style="text-decoration: none">F</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'冯\')" style="text-decoration: none;margin-right: 10px">冯</a>' +
        '<a href="javascript:bjxFirstNamefun(\'方\')" style="text-decoration: none;margin-right: 10px">方</a>',
    },{
        letter: '<a href="javascript:bjxLetterfun(\'g\')" style="text-decoration: none">G</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'高\')" style="text-decoration: none;margin-right: 10px">高</a>' +
        '<a href="javascript:bjxFirstNamefun(\'关\')" style="text-decoration: none;margin-right: 10px">关</a>' +
        '<a href="javascript:bjxFirstNamefun(\'光\')" style="text-decoration: none;margin-right: 10px">光</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'h\')" style="text-decoration: none">H</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'黄\')" style="text-decoration: none;margin-right: 10px">黄</a>' +
        '<a href="javascript:bjxFirstNamefun(\'韩\')" style="text-decoration: none;margin-right: 10px">韩</a>' +
        '<a href="javascript:bjxFirstNamefun(\'杭\')" style="text-decoration: none;margin-right: 10px">杭</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'i\')" style="text-decoration: none">I</a>',
        firstName: '',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'j\')" style="text-decoration: none">J</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'姜\')" style="text-decoration: none;margin-right: 10px">姜</a>' +
        '<a href="javascript:bjxFirstNamefun(\'贾\')" style="text-decoration: none;margin-right: 10px">贾</a>' +
        '<a href="javascript:bjxFirstNamefun(\'金\')" style="text-decoration: none;margin-right: 10px">金</a>' +
        '<a href="javascript:bjxFirstNamefun(\'季\')" style="text-decoration: none;margin-right: 10px">季</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'k\')" style="text-decoration: none">K</a>',
        firstName: ''
    },{
        letter: '<a href="javascript:bjxLetterfun(\'l\')" style="text-decoration: none">L</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'刘\')" style="text-decoration: none;margin-right: 10px">刘</a>' +
        '<a href="javascript:bjxFirstNamefun(\'罗\')" style="text-decoration: none;margin-right: 10px">罗</a>' +
        '<a href="javascript:bjxFirstNamefun(\'雷\')" style="text-decoration: none;margin-right: 10px">雷</a>'+
        '<a href="javascript:bjxFirstNamefun(\'李\')" style="text-decoration: none;margin-right: 10px">李</a>'+
        '<a href="javascript:bjxFirstNamefun(\'龙\')" style="text-decoration: none;margin-right: 10px">龙</a>'+
        '<a href="javascript:bjxFirstNamefun(\'林\')" style="text-decoration: none;margin-right: 10px">林</a>'+
        '<a href="javascript:bjxFirstNamefun(\'柳\')" style="text-decoration: none;margin-right: 10px">柳</a>'+
        '<a href="javascript:bjxFirstNamefun(\'卢\')" style="text-decoration: none;margin-right: 10px">卢</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'m\')" style="text-decoration: none">M</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'马\')" style="text-decoration: none;margin-right: 10px">马</a>' +
        '<a href="javascript:bjxFirstNamefun(\'喵\')" style="text-decoration: none;margin-right: 10px">喵</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'n\')" style="text-decoration: none">N</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'聂\')" style="text-decoration: none;margin-right: 10px">聂</a>' +
        '<a href="javascript:bjxFirstNamefun(\'年\')" style="text-decoration: none;margin-right: 10px">年</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'o\')" style="text-decoration: none">O</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'欧\')" style="text-decoration: none;margin-right: 10px">欧</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'p\')" style="text-decoration: none">P</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'朴\')" style="text-decoration: none;margin-right: 10px">朴</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'q\')" style="text-decoration: none">Q</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'钱\')" style="text-decoration: none;margin-right: 10px">钱</a>' +
        '<a href="javascript:bjxFirstNamefun(\'齐\')" style="text-decoration: none;margin-right: 10px">齐</a>' +
        '<a href="javascript:bjxFirstNamefun(\'邱\')" style="text-decoration: none;margin-right: 10px">邱</a>'+
        '<a href="javascript:bjxFirstNamefun(\'秦\')" style="text-decoration: none;margin-right: 10px">秦</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'r\')" style="text-decoration: none">R</a>',
        firstName: ''
    },{
        letter: '<a href="javascript:bjxLetterfun(\'s\')" style="text-decoration: none">S</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'孙\')" style="text-decoration: none;margin-right: 10px">孙</a>' +
        '<a href="javascript:bjxFirstNamefun(\'宋\')" style="text-decoration: none;margin-right: 10px">宋</a>' +
        '<a href="javascript:bjxFirstNamefun(\'苏\')" style="text-decoration: none;margin-right: 10px">苏</a>'+
        '<a href="javascript:bjxFirstNamefun(\'司\')" style="text-decoration: none;margin-right: 10px">司</a>'+
        '<a href="javascript:bjxFirstNamefun(\'上\')" style="text-decoration: none;margin-right: 10px">上</a>'+
        '<a href="javascript:bjxFirstNamefun(\'石\')" style="text-decoration: none;margin-right: 10px">石</a>'+
        '<a href="javascript:bjxFirstNamefun(\'尚\')" style="text-decoration: none;margin-right: 10px">尚</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'t\')" style="text-decoration: none">T</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'腾\')" style="text-decoration: none;margin-right: 10px">腾</a>' +
        '<a href="javascript:bjxFirstNamefun(\'陶\')" style="text-decoration: none;margin-right: 10px">陶</a>' +
        '<a href="javascript:bjxFirstNamefun(\'童\')" style="text-decoration: none;margin-right: 10px">童</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'u\')" style="text-decoration: none">U</a>',
        firstName: '',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'v\')" style="text-decoration: none">V</a>',
        firstName: '',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'w\')" style="text-decoration: none">W</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'王\')" style="text-decoration: none;margin-right: 10px">王</a>' +
        '<a href="javascript:bjxFirstNamefun(\'汪\')" style="text-decoration: none;margin-right: 10px">汪</a>' +
        '<a href="javascript:bjxFirstNamefun(\'万\')" style="text-decoration: none;margin-right: 10px">万</a>'+
        '<a href="javascript:bjxFirstNamefun(\'伍\')" style="text-decoration: none;margin-right: 10px">伍</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'x\')" style="text-decoration: none">X</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'谢\')" style="text-decoration: none;margin-right: 10px">谢</a>' +
        '<a href="javascript:bjxFirstNamefun(\'辛\')" style="text-decoration: none;margin-right: 10px">辛</a>' +
        '<a href="javascript:bjxFirstNamefun(\'鑫\')" style="text-decoration: none;margin-right: 10px">鑫</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'y\')" style="text-decoration: none">Y</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'杨\')" style="text-decoration: none;margin-right: 10px">杨</a>' +
        '<a href="javascript:bjxFirstNamefun(\'余\')" style="text-decoration: none;margin-right: 10px">余</a>' +
        '<a href="javascript:bjxFirstNamefun(\'于\')" style="text-decoration: none;margin-right: 10px">于</a>'+
        '<a href="javascript:bjxFirstNamefun(\'尹\')" style="text-decoration: none;margin-right: 10px">尹</a>',

    },{
        letter: '<a href="javascript:bjxLetterfun(\'z\')" style="text-decoration: none">Z</a>',
        firstName: '<a href="javascript:bjxFirstNamefun(\'赵\')" style="text-decoration: none;margin-right: 10px">赵</a>' +
        '<a href="javascript:bjxFirstNamefun(\'郑\')" style="text-decoration: none;margin-right: 10px">郑</a>' +
        '<a href="javascript:bjxFirstNamefun(\'张\')" style="text-decoration: none;margin-right: 10px">张</a>'+
        '<a href="javascript:bjxFirstNamefun(\'周\')" style="text-decoration: none;margin-right: 10px">周</a>'+
        '<a href="javascript:bjxFirstNamefun(\'章\')" style="text-decoration: none;margin-right: 10px">章</a>'+
        '<a href="javascript:bjxFirstNamefun(\'展\')" style="text-decoration: none;margin-right: 10px">展</a>'+
        '<a href="javascript:bjxFirstNamefun(\'钟\')" style="text-decoration: none;margin-right: 10px">钟</a>'
    },],
    onLoadSuccess: function () { //加载成功时执行

    },
    onLoadError: function () { //加载失败时执行
        console.info("加载数据失败");
    }
});
