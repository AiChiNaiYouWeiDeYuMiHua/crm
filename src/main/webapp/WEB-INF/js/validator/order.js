function validator() {
    var code = /^[0-9]{6}$/;
    var name = /^[\u4E00-\u9FA5\uf900-\ufa2d·s]{2,20}$/;
    var phone = /(^([0-9]{3,4}-)?[0-9]{7,8}$)|(^0?1[3|4|5|8][0-9]\d{8}$)/;
    var dou = /(^[1-9]\d{0,9}(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;
    $("#add-form").bootstrapValidator({
        message: '通用的验证失败消息',
        fields: {
            "tbCustomerByCusId.cusId": {
                validators: {
                    notEmpty: {
                        message: '请输入客户'
                    }
                }
            },
            "tbUserByUserId.userId":{
                validators: {
                    notEmpty: {
                        message: '请选择所有者'
                    }
                }
            },
            "orderOther":{
                validators: {
                    stringLength:{
                        max:100,
                        message:'备注不能超过100个字符'
                    }
                }
            },
            "orderDate":{
                validators: {
                    date:{
                        format: 'YYYY-MM-DD',
                        message: '日期格式不正确'
                    }
                }
            },
            "orderLatestDate":{
                validators: {
                    date:{
                        format: 'YYYY-MM-DD',
                        message: '日期格式不正确'
                    }
                }
            },
            "orderNewDate":{
                validators: {
                    date:{
                        format: 'YYYY-MM-DD',
                        message: '日期格式不正确'
                    }
                }
            },
            "tbAddressByAddressId.addressName":{
                validators: {
                    regexp:{
                        regexp: name,
                        message: '请输入正确的姓名'
                    }
                }
            },
            "tbAddressByAddressId.addressTel":{
                validators: {
                    regexp:{
                        regexp: phone,
                        message: '请输入正确的姓名'
                    }
                }
            },
            "tbAddressByAddressId.addressContent":{
                validators: {
                    stringLength: {
                        max: 100,
                        message: '地址长度过长'
                    }
                }
            },
            "tbAddressByAddressId.addressCode":{
                validators: {
                    regexp:{
                        regexp: code,
                        message: '请输入正确的邮编'
                    }
                }
            },
            "contactsEntity.cotsId":{
                notEmpty: {
                    message: '请选择客户签约人'
                }
            },
            "tbUserByTbUserId.userId":{
                notEmpty: {
                    message: '请选择我方签约人'
                }
            },
            "orderTotal":{
                validators: {
                    regexp:{
                        regexp: dou,
                        message: '请输入正确的价格'
                    }
                }
            },


        }
    });
}