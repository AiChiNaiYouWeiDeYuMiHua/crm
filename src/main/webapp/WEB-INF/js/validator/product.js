function validator() {
    //11位字母数字
    var code = /^[a-zA-Z0-9_\.]{11}$/;
    var dou = /(^[1-9]\d{0,9}(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;
    var ku = /^[0-9]{0,9}$/;
    $("#add-form,#addformatter-form").bootstrapValidator({
        message: '通用的验证失败消息',
        fields: {
            "tbProductByProductId.productName": {
                msg: '产品名称验证失败',
                validators: {
                    notEmpty: {
                        message: '产品名称不能为空'
                    }, stringLength: {
                        min: 2,
                        max: 20,
                        message: '产品名称不能少于二个字大于十个字'
                    }
                }
            },
            "tbProductByProductId.productModel":{
                validators: {
                    stringLength: {
                        min: 2,
                        max: 20,
                        message: '型号不能少于二个字'
                    }
                }
            },
            "pfCode":{
                validators: {
                    regexp:{
                        regexp: code,
                        message: '请输入11位字母数字的条码'
                    }
                }
            },
            "pfPrice":{
                validators: {
                    regexp:{
                        regexp: dou,
                        message: '请输入正确的价格'
                    }
                }
            },
            "pfCost":{
                validators: {
                    regexp:{
                        regexp: dou,
                        message: '请输入正确的价格'
                    }
                }
            },
            "pfWeight":{
                validators: {
                    regexp:{
                        regexp: dou,
                        message: '请正确重量'
                    }
                }
            },
            "pfMax":{
                validators: {
                    regexp:{
                        regexp: ku,
                        message: '请输入正确的库存'
                    }
                }
            },
            "pfMin":{
                validators: {
                    regexp:{
                        regexp: ku,
                        message: '请输入正确的库存'
                    }
                }
            },
            "pfOther":{
                validators: {
                    stringLength:{
                        max:100,
                        message:'备注不能超过100个字符'
                    }
                }
            },
            "pfName":{
                validators: {
                    stringLength: {
                        min: 2,
                        max: 20,
                        message: '规格不能少于三个字'
                    }
                }
            },
            "pfBatch":{
                validators: {
                    regexp:{
                        regexp: ku,
                        message: '请输入正确的批次信息'
                    }
                }
            },
            "pfDate":{
                validators: {
                    date:{
                        format: 'YYYY-MM-DD',
                        message: '日期格式不正确'
                    }
                }
            },
            "pfExpiry":{
                validators: {
                    date:{
                        format: 'YYYY-MM-DD',
                        message: '日期格式不正确'
                    }
                }
            },
            "pfManufacturer":{
                max:30,
                message:"输入过长"
            },
            "pfApprovalNumber":{
                regexp:{
                    regexp: /^[a-zA-Z0-9]{4,20}$/,
                    message: '请输入正确的批准文号'
                }
            }

        }
    });
}