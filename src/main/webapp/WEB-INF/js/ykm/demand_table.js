//表单数据转json对象
function formToJson(form) {
    var d = {};
    var t = form.serializeArray();
    var array = [];
    // alert("序列数组："+JSON.stringify(t));
    $.each(t, function() {
        if(this.name == 'importanceList'){
            array.push(this.value)
        }
        else if (this.value != null && this.value.length >0)
            d[this.name] = this.value;
    });
    d["importanceList"] = array;
    // alert(array.length+"formToJson结果："+JSON.stringify(d))
    return d;
}

$("#table111").bootstrapTable({ // 对应table标签的id
    url: "/demand/senior", //AJAX获取表格数据的url
    // method: 'post',
    striped: true, //是否显示行间隔色(斑马线)
    pagination: true, //是否显示分页（*）
    sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
    paginationLoop: false, //当前页是边界时是否可以继续按
    showFooter: true,
    queryParams: function(params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
        var json = formToJson($('#search-form'))
        json["page.size"] = params.limit;
        json["page.page"] = (params.offset/params.limit)+1;
        var sort = params.sort;
        if (sort != null && sort.length >0){
            json["page.sortName"] = sort;
            json["page.sortOrder"] = params.order
        }
        json["familyDa"] = familyDa;
        json["kidDa"] = kidDa;
        var searchText = $('#search-text').val();
        if (searchText != null && searchText.length >0)
            json["searchText"] = searchText;
        json["searchTextType"] = searchTextType;
        return json
    }, //传递参数（*）
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 5, //每页的记录行数（*）

    // contentType: "application/json", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
    //search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
    strictSearch: false, //是否全局匹配,false模糊匹配
    //					showColumns: true, //是否显示所有的列
    //					showRefresh: true, //是否显示刷新按钮
    minimumCountColumns: 2, //最少允许的列数
    //					clickToSelect: true, //是否启用点击选中行
    //height: 500,                      //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
    //uniqueId: "id",                   //每一行的唯一标识，一般为主键列
    //					showToggle: true, //是否显示详细视图和列表视图的切换按钮
    //					cardView: false, //是否显示详细视图
    detailView: false, //是否显示父子表
    cache: false, // 设置为 false 禁用 AJAX 数据缓存， 默认为true
    sortable: true, //是否启用排序
    sortOrder: "asc", //排序方式
    sortName: 'sn', // 要排序的字段
    columns: [{
        checkbox: 'true',
        width: 60,
        align: 'center',
        footerFormatter: function(value) {
            return "合计";
        }
    },{
        // field: 'id', // 返回json数据中的name
        title: '序号', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 80,
        valign: 'middle', // 上下居中
        formatter:function(value,row,index){
            return tableIndexNum(index);
        }
    }, {
        field: 'demandId', // 返回json数据中的name
        title: '详细需求ID', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 80,
        valign: 'middle', // 上下居中
        footerFormatter: function(value) {
            var count = 0;
            for(var i in value) {
                count += value[i].name;
            }
            return ""+count;
        }
    }, {
        field: 'demandTheme',
        title: '需求主题',
        align: 'center',
        width: '400px',
        valign: 'middle'
    }, {
        field: 'tbSaleOppByOppId.oppTheme',
        title: '对应机会',
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function (value,row,index){
            if (value == null || value.length <=0)
                return "无主题";
            var id = row.tbSaleOppByOppId.oppId;
            return [
                '<a  href="sale/opp/info/'+id+'" target="_blank"><i style="margin-right: 3px" class="fa fa-arrow-circle-right text-blue m-r-5"></i>'+value+'</a>'
            ].join('');
        }
    }, {
        field: 'tbSaleOppByOppId.tbCustomerByCusId.cusName',
        title: '客户',
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function (value,row,index){
            if (value == null || value.length <=0)
                return "";
            var id = row.tbSaleOppByOppId.tbCustomerByCusId.cusId;
            return [
                '<a  href="customer_details?id='+id+'" target="_blank"><i\n' +
                '                                    class="fa fa-folder-open" style="margin-left: 5px" data-toggle="tooltip"\n' +
                '                                    data-placement="bottom"\n' +
                '                                    data-original-title="打开详细页面"></i>'+value+'</a>'
            ].join('');
        }
    }, {
        field: 'importance',
        title: '重要程度',
        align: 'center',
        valign: 'middle',
        sortable:true,
        formatter:function (value,row,index){
            if (value == 1)
                return "非常重要"
            else if (value == 2)
                return "重要"
            else if (value == 3)
                return "一般"
            else if (value == 4)
                return "不重要"
            else
                return ""
        }
    }, {
        field: 'recordTime',
        title: '记录时间',
        align: 'center',
        valign: 'middle',
    }, {
        title: '操作',
        titlealign: 'left',
        align: 'center',
        width: 80,
        formatter: dimiss
    }],

    onLoadSuccess: function() { //加载成功时执行
        console.info("加载成功");
        deleteAll();    ////生成批量删除按钮
    },
    onLoadError: function() { //加载失败时执行
        console.info("加载数据失败");
    },

    //>>>>>>>>>>>>>>导出excel表格设置
    showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
    exportDataType: "basic", //basic', 'all', 'selected'.
    exportTypes: ['excel', 'xlsx'], //导出类型
    //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
    exportOptions: {
        ignoreColumn: [0,0],            //忽略某一列的索引
        fileName: '数据导出', //文件名称设置
        worksheetName: 'Sheet1', //表格工作区名称
        tableName: '商品数据表',
        excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
        //onMsoNumberFormat: DoOnMsoNumberFormat
    }
    //导出excel表格设置<<<<<<<<<<<<<<<<

});

//表格自动序号
function tableIndexNum(index){
    var currentPage=$(".page-item.active").find('a').text();
    var size = $(".page-list .page-size").text();
    if (size == null || size.length <0)
        size = 5;
    return Number(index+1+eval((currentPage-1)*size));
}

