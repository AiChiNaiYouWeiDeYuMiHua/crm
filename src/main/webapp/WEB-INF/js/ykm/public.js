var familyDa = "";
var kidDa = "";
var searchTextType = -1;
var open_modal;
var formJson;

//获取下拉列表值
function getFamilyAndKid(obj) {
    // $("#select-type").selectpicker('render')
    var kidData = obj.options[obj.selectedIndex].value;
    var familyData = obj.options[obj.selectedIndex].parentNode.attributes;

    kidDa = kidData;
    if (familyData["label"] != null) {
        familyDa = familyData["label"].value;
    } else {
        familyDa = "";
    }
    if($('#select-type').val()!= 0){
        $('#cancel-search').removeClass('hidden')
    }else {
        $('#cancel-search').addClass('hidden')
    }

    $("#table111").bootstrapTable('refresh');
}

//取消搜索的初始化
function init() {
    familyDa = "";
    kidDa = "";
    searchTextType = -1;
}

//取消搜索
function cancelKey() {
    $('#select-type').selectpicker('val','0');
    $('#search-text').val("");
    if ($('#search-form')[0] != null)
        $('#search-form')[0].reset();
    init();
    // search2();
    $('#cancel-search').addClass('hidden')
    $("#search-senior").on("hidden.bs.modal", function () {
        $(this).removeData('bs.modal');
    });
    $('#table111').bootstrapTable('refresh')
}

//快速查询
function fastQuery(searchType){
    searchTextType = searchType;
    if($('#search-text').val().length >0 || !$.isEmptyObject(formToJson($('#search-form')))){
        $('#cancel-search').removeClass('hidden')
    }else {
        $('#cancel-search').addClass('hidden')
    }

    $("#table111").bootstrapTable('refresh');
}

//查询
function search2() {
    if ( !$.isEmptyObject(formToJson($('#search-form')))) {
        $('#cancel-search').removeClass('hidden')
    }else {
        $('#cancel-search').addClass('hidden')
    }
    //清除模态框数据
    $("#search-senior").on("hidden.bs.modal", function () {
        $(this).removeData('bs.modal');
    });
    $('#table111').bootstrapTable('refresh')
}

/*
//表单数据转json对象
function formToJson(form) {
    var d = {};
    var t = form.serializeArray();
    var array = [];
    alert("序列数组："+JSON.stringify(t));
    $.each(t, function() {
        if(this.name == 'competitiveAbilityArray'){
            array.push(this.value)
        }
        else if (this.value != null && this.value.length >0)
            d[this.name] = this.value;
    });
    d["competitiveAbilityArray"] = array;
    alert(array.length+"formToJson结果："+JSON.stringify(d))
    return d;
}*/

function datePicker(index){
    for(var i= 1;i<=index;i++){
        $("#datetime_"+i).datetimepicker({
            format: "yyyy-mm-dd",
            minView: "month",//设置只显示到月份
            startView: 2,
            showMeridian: 1,
            forceParse: 0,
            autoclose: true,
            todayBtn: true,
            container: "#datetime_"+i
        });
    }
}