
var familyDa = "";
var kidDa = "";
var searchTextType = -1;

//获取下拉列表值
function getFamilyAndKid(obj) {
    $("#select-type").selectpicker('render')
    var kidData = obj.options[obj.selectedIndex].value;
    var familyData = obj.options[obj.selectedIndex].parentNode.attributes;

    kidDa = kidData;
    if (familyData["label"] != null) {
        familyDa = familyData["label"].value;
    } else {
        familyDa = "";
    }
    if($('#select-type').val()!= 0){
        $('#cancel-search').removeClass('hidden')
    }else {
        $('#cancel-search').addClass('hidden')
    }

    $("#table111").bootstrapTable('refresh');
}

//取消搜索的初始化
function init() {
    familyDa = "";
    kidDa = "";
    searchTextType = -1;
}

//快速查询
function fastQuery(searchType){
    searchTextType = searchType;
    if($('#search-text').val().length >0 || !$.isEmptyObject(formToJson($('#search-form')))){
        $('#cancel-search').removeClass('hidden')
    }else {
        $('#cancel-search').addClass('hidden')
    }

    $("#table111").bootstrapTable('refresh');
}




$("#table111").bootstrapTable({ // 对应table标签的id
	url: "sale_opp_senior", //AJAX获取表格数据的url
	striped: true, //是否显示行间隔色(斑马线)
	pagination: true, //是否显示分页（*）
	sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
	paginationLoop: false, //当前页是边界时是否可以继续按
	showFooter: true,
	queryParams: function(params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
		var json = formToJson($('#search-form'))
        json["page.size"] = params.limit;
        json["page.page"] = (params.offset/params.limit)+1;
        var sort = params.sort;
        if (sort != null && sort.length >0){
            json["page.sortName"] = sort;
            json["page.sortOrder"] = params.order
        }
        json["familyDa"] = familyDa;
        json["kidDa"] = kidDa;
        var searchText = $('#search-text').val();
        if (searchText != null && searchText.length >0)
            json["searchText"] = searchText;
        json["searchTextType"] = searchTextType;
		return json
		/*return {
            saleOppVo: json,
			pageSize: params.limit, // 每页要显示的数据条数
			pageNumber: (params.offset/params.limit)+1, // 每页显示数据的开始行号
			//sort: params.sort, // 要排序的字段
			//sortOrder: params.order, // 排序规则
			//dataId: $("#dataId").val() // 额外添加的参数
		}*/
	}, //传递参数（*）
	pageNumber: 1, //初始化加载第一页，默认第一页
	pageSize: 5, //每页的记录行数（*）

	contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
	//search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
	strictSearch: false, //是否全局匹配,false模糊匹配
	//					showColumns: true, //是否显示所有的列
	//					showRefresh: true, //是否显示刷新按钮
	minimumCountColumns: 2, //最少允许的列数
	//					clickToSelect: true, //是否启用点击选中行
	//height: 500,                      //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
	//uniqueId: "id",                   //每一行的唯一标识，一般为主键列
	//					showToggle: true, //是否显示详细视图和列表视图的切换按钮
	//					cardView: false, //是否显示详细视图
	detailView: false, //是否显示父子表
	cache: false, // 设置为 false 禁用 AJAX 数据缓存， 默认为true
	sortable: true, //是否启用排序
	sortOrder: "asc", //排序方式
	sortName: 'sn', // 要排序的字段
	columns: [{
		checkbox: 'true',
		width: 60,
		align: 'center',
		footerFormatter: function(value) {
			return "合计";
		}
	}, {
        // field: 'id', // 返回json数据中的name
        title: '序号', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 80,
        valign: 'middle', // 上下居中
        formatter:function(value,row,index){
            return tableIndexNum(index);
        }
    },{
		field: 'oppId', // 返回json数据中的name
		title: '机会ID', // 表格表头显示文字
		align: 'center', // 左右居中
		width: 80,
		valign: 'middle', // 上下居中
        sortable: true
	}, {
		field: 'oppTheme',
		title: '机会主题',
		align: 'center',
		valign: 'middle',
        sortable: true,
        width: '100px',
        /*cellStyle:{
		  css: {
		      "overflow" : "hidden",
              "text-overflow" : "ellipsis",
              "white-space" : "nowrap"
          }
        },*/
        formatter:function (value,row,index){
            var id = row.oppId;
            if (value == null || value.length <=0)
                value = "无主题";
            return [
                '<a  href="sale/opp/info/'+id+'" target="_blank"><i style="margin-right: 3px" class="fa fa-arrow-circle-right text-blue m-r-5"></i>'+value+'</a>'
            ].join('');
        }
	}, {
		field: 'tbCustomerByCusId.cusName',
		title: '客户',
		align: 'center',
		valign: 'middle',
		sortable: true,
        formatter:function (value,row,index){
            if(row.tbCustomerByCusId == null){
                return "";
            }
            var id = row.tbCustomerByCusId.cusId;
            if (value == null || value.length <=0)
                value = "无主题";
            return [
                '<a  href="customer_details?id='+id+'" target="_blank"><i\n' +
                '                                    class="fa fa-folder-open" style="margin-left: 5px" data-toggle="tooltip"\n' +
                '                                    data-placement="bottom"\n' +
                '                                    data-original-title="打开详细页面"></i>'+value+'</a>'
            ].join('');
        }
	}, {
        field: 'tbContactsBy联系人Id.cotsName',
        title: '联系人',
        align: 'center',
        valign: 'middle',
        sortable: true
    }, {
		field: 'priority',
		title: '优先',
		align: 'center',
		valign: 'middle',
        sortable: true,
        formatter:function (value,row,index){
            if (value == 1)
                return "一级"
            else if (value == 2)
                return "二级"
            else if (value == 3)
                return "三级"
            else if (value == 4)
                return "四级"
            else if (value == 5)
                return "五级"
            else
                return "-"
        }
	}, {
		field: 'classification',
		title: '类型',
		align: 'center',
		valign: 'middle',
        sortable: true
	}, {
		field: 'tbUserByUserId.userName',
		title: '负责人',
		align: 'center',
		valign: 'middle',
        sortable: true
	},{
        field: 'expectedTime',
        title: '预计签单日期',
        align: 'center',
        valign: 'middle',
        sortable: true
    },{
        field: 'expectedAmount',
        title: '预期金额',
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function (value,row,index){
            if (value != null)
                return "￥"+value
            else
                return "￥0.00"
        },
        footerFormatter:sumFormatter
    },{
        field: 'possibility',
        title: '可能性',
        align: 'center',
        valign: 'middle',
        sortable: true
    },{
        field: 'stage',
        title: '阶段',
        align: 'center',
        valign: 'middle',
        sortable: true
    },{
        field: 'oppStatus',
        title: '状态',
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function (value,row,index){
            if (value == 1)
                return "跟踪"
            else if (value == 2)
                return "成功"
            else if (value == 3)
                return "失败"
            else if (value == 4)
                return "搁置"
            else if (value == 5)
                return "失效"
            else
                return "-"
        }
    },{
        field: 'stageStartTime',
        title: '阶段停留',
        align: 'center',
        valign: 'middle',
        sortable: true,
		formatter: function (value) {
			if(value == null || value.length == 0){
				return value;
			}
        	var curDate = Date.now();
            var  startDate=Date.parse(value.replace('/-/g','/'));
            return parseInt((curDate - startDate)/(1*24*60*60*1000))+"天";
        }
    },{
        field: 'updateTime',
        title: '更新日期',
        align: 'center',
        valign: 'middle',
        sortable: true
    }, {
	    field: 'tbCustomerByCusId.cusId',
		title: '操作',
		titlealign: 'left',
		align: 'center',
		width: 80,
		formatter: dimiss
	}],

	onLoadSuccess: function() { //加载成功时执行
		console.info("加载成功");
        deleteAll();
	},
	onLoadError: function() { //加载失败时执行
		console.info("加载数据失败");
	},

	//>>>>>>>>>>>>>>导出excel表格设置
	showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
	exportDataType: "basic", //basic', 'all', 'selected'.
	exportTypes: ['excel', 'xlsx'], //导出类型
	//exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
	exportOptions: {
		ignoreColumn: [0,0],            //忽略某一列的索引  
		fileName: '数据导出', //文件名称设置  
		worksheetName: 'Sheet1', //表格工作区名称  
		tableName: '商品数据表',
		excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
		//onMsoNumberFormat: DoOnMsoNumberFormat  
	}
	//导出excel表格设置<<<<<<<<<<<<<<<<

});

//表单数据转json对象
function formToJson(form) {
    var d = {};
    var t = form.serializeArray();
    var oppSourceList = [];
    var stageList = [];
    var oppStatusList = [];
    var priorityList = [];
    var classificationList = [];
    var intendProductList = [];
    $.each(t, function() {
        if(this.name == 'oppSourceList'){
            oppSourceList.push(this.value)
        } else if (this.name == "stageList"){
            stageList.push(this.value)
        } else if (this.name == "oppStatusList"){
            oppStatusList.push(this.value)
        } else if (this.name == "priorityList"){
            priorityList.push(this.value)
        } else if (this.name == "classificationList"){
            classificationList.push(this.value)
        } else if (this.name == "intendProductList"){
            intendProductList.push(this.value)
        }
        else if (this.value != null && this.value.length >0)
            d[this.name] = this.value;
    });
    d["oppSourceList"] = oppSourceList;
    d["stageList"] = stageList;
    d["oppStatusList"] = oppStatusList;
    d["priorityList"] = priorityList;
    d["classificationList"] = classificationList;
    d["intendProductList"] = intendProductList;
    return d;
}

function transMoney(money) {
    if(money && money!=null){
        money = String(money);
        var left=money.split('.')[0],right=money.split('.')[1];
        right = right ? (right.length>=2 ? '.'+right.substr(0,2) : '.'+right+'0') : '.00';
        var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);
        return (Number(money)<0?"-":"") + temp.join(',').split('').reverse().join('')+right;
    }else if(money===0){   //注意===在这里的使用，如果传入的money为0,if中会将其判定为boolean类型，故而要另外做===判断
        return '0.00';
    }else{
        return "";
    }
}

function sumFormatter(data) {
    field = this.field;
    var sum1= data.reduce(function(sum, row) {
        if (row[field] != undefined)
            return sum + (+row[field]);
        else
            return sum;
    }, 0);
    return "￥"+transMoney(sum1);
}


//表格自动序号
function tableIndexNum(index){
    var currentPage=$(".page-item.active").find('a').text();
    var size = $(".page-list .page-size").text();
    if (size == null || size.length <0)
        size = 5;
    return Number(index+1+eval((currentPage-1)*size));
}
