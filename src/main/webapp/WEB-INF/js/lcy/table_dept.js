$("#table111").bootstrapTable({ // 对应table标签的id
    url: "dept_table", //AJAX获取表格数据的url
    //					striped: true, //是否显示行间隔色(斑马线)
    pagination: true, //是否显示分页（*）
    sidePagination: "client", //分页方式：client客户端分页，server服务端分页（*）
    paginationLoop: false, //当前页是边界时是否可以继续按
    showFooter: true,
    queryParams: function (params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
        return {
            pageSize: params.limit, // 每页要显示的数据条数
            pageNumber: (params.offset / params.limit) // 每页显示数据的开始行号
            //sort: params.sort, // 要排序的字段
            //sortOrder: params.order, // 排序规则
            //dataId: $("#dataId").val() // 额外添加的参数
        }
    }, //传递参数（*）
    pageNumber: 1, //初始化加载第一页，默认第一页
    pageSize: 5, //每页的记录行数（*）

    contentType: "application/x-www-form-urlencoded", //一种编码。在post请求的时候需要用到。这里用的get请求，注释掉这句话也能拿到数据
    //search: true,                     //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
    strictSearch: false, //是否全局匹配,false模糊匹配
    //					showColumns: true, //是否显示所有的列
    //					showRefresh: true, //是否显示刷新按钮
    minimumCountColumns: 2, //最少允许的列数
    //					clickToSelect: true, //是否启用点击选中行
    //height: 500,                      //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
    //uniqueId: "id",                   //每一行的唯一标识，一般为主键列
    //					showToggle: true, //是否显示详细视图和列表视图的切换按钮
    //					cardView: false, //是否显示详细视图
    detailView: false, //是否显示父子表
    cache: false, // 设置为 false 禁用 AJAX 数据缓存， 默认为true
    sortable: true, //是否启用排序
    sortOrder: "asc", //排序方式
    sortName: 'deptId', // 要排序的字段
    columns: [{
        checkbox: 'true',
        width: 60,
        align: 'center',
        footerFormatter: function (value) {
            return "合计";
        }
    }, {
        field: 'Number', // 返回json数据中的name
        title: '序号', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 80,
        valign: 'middle', // 上下居中
        formatter: function (value, row, index) {
            return tableIndexNum(index);
        }
        // footerFormatter: function(value) {
        //     var count = 0;
        //     for(var i in value) {
        //         count += value[i].name;
        //     }
        //     return ""+count;
        // }
    }, {
        field: 'text', // 返回json数据中的name
        title: '部门名称', // 表格表头显示文字
        align: 'center', // 左右居中
        width: 120,
        valign: 'middle', // 上下居中
        // footerFormatter: function(value) {
        //     var count = 0;
        //     for(var i in value) {
        //         count += value[i].name;
        //     }
        //     return ""+count;
        // }
    }, {
        field: 'deptId',
        title: '部门ID',
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: false
    }, {
        field: 'deptCount',
        title: '部门人数',
        align: 'center',
        width: 120,
        valign: 'middle',
        sortable: true
    }, {
        title: '操作',
        titlealign: 'left',
        align: 'center',
        valign: 'middle',
        width: 100,
        // events: operateEvents,
        formatter: function (value, row, index) {
            return [
                '<g:g id="23"><a onclick="modifyDept(' + row.deptId + ')" style="cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="修改"><i class="fa fa-pencil"></i></a>',
                '<g:g id="23"><a onclick="deleteDept(' + row.deptId + ')" style="cursor:pointer; margin-left:8px;" data-toggle="tooltip" data-placement="bottom" title="删除"><i class="fa fa-trash-o"></i></a>',
                '<g:g id="23"><a onclick="deptGrantUser(' + row.deptId + ')" style="cursor:pointer; margin-left:8px;" data-toggle="tooltip" data-placement="bottom"  title="用户"><i class="fa fa-file-text-o"></i></a>',
            ].join('');
        }
    }],
    onLoadSuccess: function () { //加载成功时执行
        console.info("加载成功");
    },
    onLoadError: function () { //加载失败时执行
        console.info("加载数据失败");
    },

    //>>>>>>>>>>>>>>导出excel表格设置
    showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
    exportDataType: "basic", //basic', 'all', 'selected'.
    exportTypes: ['excel', 'xlsx'], //导出类型
    //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
    exportOptions: {
        ignoreColumn: [0, 0],            //忽略某一列的索引
        fileName: '数据导出', //文件名称设置
        worksheetName: 'Sheet1', //表格工作区名称
        tableName: '角色数据表',
        excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
        //onMsoNumberFormat: DoOnMsoNumberFormat
    }
    //导出excel表格设置<<<<<<<<<<<<<<<<

});
$(function () {
    $('#table111').on('click-row.bs.table', function (e, row, element) {
        //$(element).css({"color":"blue","font-size":"16px;"});
        console.log(row);

    });
})

//单元格空处理
function isnull(value, row, index) {
    if (value == null)
        return "";
    else
        return value;
}

//表格自动序号
function tableIndexNum(index) {
    var currentPage = $(".page-item.active").find('a').text();
    return Number(index + 1 + eval((currentPage - 1) * 5));
}

//表单数据转json对象
function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}

//注销角色
function deleteDept(deptId) {
    swal({
            title: "确定删除吗？",
            text: "你将无法恢复该部门！",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "确定删除！",
            closeOnConfirm: false
        },
        function () {
            ajax("/dept_del", function () {
                swal("成功！", "部门删除成功！", "success");
                $("#table111").bootstrapTable('refresh');
            }, function () {
            }, 'get', data = {'deptId': deptId});
        }
    );
}

function ajax(url, success, error, type, data) {
    $.ajax({
        url: url,
        timeout: 3000,
        type: type,
        data: data,
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'json',
        success: function (result) {
            /*alert("发了十多年房价数据表空间");*/
            if (result.code != 200) {
                swal(result.msg, "", "error");
                error()
            }
            else {
                success(result.data);
            }
        },
        complete: function (XMLHttpRequest, status) {
            if (status != "success") {
                swal("连接服务器错误", "", "error");
                error()
            }
        }
    })
}