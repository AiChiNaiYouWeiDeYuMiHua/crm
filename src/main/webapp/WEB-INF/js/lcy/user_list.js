$(function () {
    $.ajaxSetup({
        scriptCharset: "utf-8",
        contentType: "text/html;charset=UTF-8"
    });
    /**
     * 根据用户名模糊查询
     */
    $("#finduser_like_name").click(function () {
        var name = encodeURI($("#name_to_likefind").val());
        // var top = {
        //     url: "/user_find_like_name/" + name + "/5/0",
        //     silent: true,
        //     query:{
        //         type:1,
        //         level:2
        //     }
        // };
        $("#table111").bootstrapTable('refresh', {url: "/admin/user_find_like_name?userName=" + name});
    });
    /**
     * 下拉框选择部门快捷查询
     */
    $(".selectpicker").selectpicker({
        noneSelectedText: '请选择'
    });
    //解除搜索
    $("#remove_search").click(function () {
        $("#name_to_likefind").val("");
        $('#dept_select').selectpicker('val','0');
        $("#table111").bootstrapTable('refresh', {url: "/admin/user_table_all"});
    });

    $(window).on('load', function () {
        $('.selectpicker').selectpicker('val', '');
        $('.selectpicker').selectpicker('refresh');
    });

    //下拉数据加载
    $.ajax({
        type: 'get',
        url: "/dept_list",
        dataType: 'json',
        success: function (datas) {//返回list数据并循环获取
            var select = $("#dept_select");
            select.append("<option value='0' selected>所有部门</option>");
            for (var i = 0; i < datas.length; i++) {
                select.append("<option value='" + datas[i].deptId + "'>"
                    + datas[i].text + "</option>");
            }
            $('.selectpicker').selectpicker('val', '');
            $('.selectpicker').selectpicker('refresh');
        }
    });
    $('.selectpicker').on('changed.bs.select', function (e) {
        var value = $("#dept_select").selectpicker('val');
        if (value == 0) {
            $("#table111").bootstrapTable('refresh', {url: "/admin/user_table_all"});
        } else {
            $("#table111").bootstrapTable('refresh', {url: "/admin/user_find_by_dept?deptId=" + value});
        }
    });

});