var curRow = {};
$(function () {
    $("#table111").bootstrapTable({
        toolbar: "#toolbar",
        idField: "Id",
        pagination: true,
        showRefresh: true,
        search: true,
        clickToSelect: true,
        queryParams: function (param) {
            return {};
        },
        url: "/find_dimi?typeId=" + typeid,
        columns: [{
            checkbox: true
        }, {
            field: "dimissionName",
            title: "参数名称",
            formatter: function (value, row, index) {
                return "<a href=\"#\" name=\"dimissionName\" data-type=\"text\" data-pk=\"" + row.Id + "\" data-title=\"参数名称\">" + value + "</a>";
            }
        }, {
            field: "tbDimissionTypeByDimiTypeId.dimiTypeName",
            title: "参数类型",
        }],
        onClickRow: function (row, $element) {
            curRow = row;
        },
        onLoadSuccess: function (aa, bb, cc) {
            $("#tb_user a").editable({
                url: function (params) {
                    var sName = $(this).attr("name");
                    curRow[sName] = params.value;
                    $.ajax({
                        type: 'POST',
                        url: "/dimi_modify",
                        data: curRow,
                        dataType: 'JSON',
                        success: function (data, textStatus, jqXHR) {
                            alert('保存成功！');
                        },
                        error: function () {
                            alert("error");
                        }
                    });
                },
                type: 'text'
            });
        },
    });
});
$(function () {
    $('#table111').on('click-row.bs.table', function (e, row, element) {
        //$(element).css({"color":"blue","font-size":"16px;"});
        console.log(row);

    });
})

//单元格空处理
function isnull(value, row, index) {
    if (value == null)
        return "";
    else
        return value;
}

//表格自动序号
function tableIndexNum(index) {
    var currentPage = $(".page-item.active").find('a').text();
    return Number(index + 1 + eval((currentPage - 1) * 25));
}

//表单数据转json对象
function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function () {
        if (this.value != null && this.value.length > 0)
            d[this.name] = this.value;
    });
    return d;
}

function ajax(url, success, error, type, data) {
    $.ajax({
        url: url,
        timeout: 3000,
        type: type,
        data: data,
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'json',
        success: function (result) {
            /*alert("发了十多年房价数据表空间");*/
            if (result.code != 200) {
                swal(result.msg, "", "error");
                error()
            }
            else {
                success(result.data);
            }
        },
        complete: function (XMLHttpRequest, status) {
            if (status != "success") {
                swal("连接服务器错误", "", "error");
                error()
            }
        }
    })
}

function sumFormatter(data) {
    field = this.field;
    var sum1 = data.reduce(function (sum, row) {
        return sum + (+row[field]);
    }, 0);
    return sum1;
}