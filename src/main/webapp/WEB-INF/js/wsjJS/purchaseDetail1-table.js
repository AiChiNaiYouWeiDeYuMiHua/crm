$(function () {
    $("#table33").bootstrapTable({
        method: 'post',
        url: "/sale/purProDetail1?id="+a,
        striped: true,
        showFooter: true,
        queryParams: function(params) {
            //表单转json(去掉孔项目，节省流量
            var d = f($('#search-form'));
            var page = {};
            page["size"] = params.limit;
            page["page"] = (params.offset/params.limit)+1;
            var sort = params.sort;
            if (sort != null && sort.length >0){
                page["sortName"] = sort;
                page["sortOrder"] = params.order
            }
            d["page"] = page;
            return d
        },
        pageList:	[10, 25, 50, 100],
        pageNumber: 1,
        pageSize: 5,
        contentType: "application/json",
        strictSearch: false,
        minimumCountColumns: 2,
        detailView: false,
        cache: false,
        sortable: true,
        sortOrder: "asc",
        sortName: 'pdId',
        uniqueId: "id",
        columns: [{
            checkbox: 'true',
            width: 60,
            align: 'center',
            footerFormatter: function(value) {
                return "合计";
            }
        },  {
            // field: 'id', // 返回json数据中的name
            title: '序号', // 表格表头显示文字
            align: 'center', // 左右居中
            width: 80,
            valign: 'middle', // 上下居中
            formatter:function(value,row,index){
                return tableIndexNum(index);
            }
        },{
            field: 'pdId', // 返回json数据中的name
            title: 'id', // 表格表头显示文字
            align: 'center', // 左右居中
            width: 80,
            valign: 'middle', // 上下居中
        }, {
            field: 'tbPurchaseByPurId.purTheme',
            title: '采购单',
            align: 'center',
            valign: 'middle',
            formatter:f1
            // formatter:moneyFormatter,
            // footerFormatter:sumFormatter
        },{
            field: 'tbProductFormatByPfId.pfName',
            title: '产品名称',
            align: 'center',
            valign: 'middle',
            formatter:isNullFormatter
        },{
            field: 'pdPrice',
            title: '单价',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:moneyFormatter,
            footerFormatter:sumFormatter
        },{
            field: 'pdNum',
            title: '数量',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:moneyFormatter,
            footerFormatter:sumFormatter
        },{
            field: 'pdMoney',
            title: '金额',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter:moneyFormatter,
            footerFormatter:sumFormatter
        },{
            field: 'pdClassify',
            title: '状态',
            align: 'center',
            valign: 'middle',
            formatter:isNullFormatter
        },{
            field: 'tbCustomerByCusId.cusName',
            title: '客户',
            align: 'center',
            valign: 'middle',
            formatter:f1
        },{
            field: 'tbUserByUserId.text',
            title: '负责人',
            align: 'center',
            valign: 'middle',
            formatter:f1
        }],
        onLoadSuccess: function() { //加载成功时执行
            console.info("加载成功");
            add();
        },
        onLoadError: function() { //加载失败时执行
            console.info("加载数据失败");
        },
        //>>>>>>>>>>>>>>导出excel表格设置
        showExport: true, //是否显示导出按钮(此方法是自己写的目的是判断终端是电脑还是手机,电脑则返回true,手机返回falsee,手机不显示按钮)
        exportDataType: "basic", //basic', 'all', 'selected'.
        exportTypes: ['excel', 'xlsx'], //导出类型
        //exportButton: $('#btn_export'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
        exportOptions: {
            ignoreColumn: [0,0],            //忽略某一列的索引
            fileName: '数据导出', //文件名称设置
            worksheetName: 'Sheet1', //表格工作区名称
            tableName: '商品数据表',
            excelstyles: ['background-color', 'color', 'font-size', 'font-weight'],
            //onMsoNumberFormat: DoOnMsoNumberFormat
        }
        //导出excel表格设置<<<<<<<<<<<<<<<<
    });

})

function isNullFormatter(value, rows, index){
    if (value == null)
        return "";
    else
        return value;
}
function moneyFormatter(value, rows, index){
    if (value == null)
        return "";
    else{
        var money=transMoney(value);
        return  "&nbsp;￥"+ money ;
    }
}
function f1(value, rows, index) {
    if (value == null)
        return "";
    else{
        return  "<p align='center'><span style='font-weight:normal;color:#9e9e9e'>〖</span>"+value+"&nbsp;<a href='');><i class='fa fa-folder-open m-l-5'></i></a><span style='font-weight:normal;color:#9e9e9e'>〗</span></p>" ;
    }
}
function transMoney(money) {
    if(money && money!=null){
        money = String(money);
        var left=money.split('.')[0],right=money.split('.')[1];
        right = right ? (right.length>=2 ? '.'+right.substr(0,2) : '.'+right+'0') : '.00';
        var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);
        return (Number(money)<0?"-":"") + temp.join(',').split('').reverse().join('')+right;
    }else if(money===0){   //注意===在这里的使用，如果传入的money为0,if中会将其判定为boolean类型，故而要另外做===判断
        return '0.00';
    }else{
        return "";
    }
}
function sumFormatter(data) {
    field = this.field;
    var sum1= data.reduce(function(sum, row) {
        row[field]==null?0:row[field]
        return sum + (+row[field]);
    }, 0);
    return "￥"+transMoney(sum1);
}
//表格自动序号
function tableIndexNum(index){
    var currentPage=$(".page-item.active").find('a').text();
    var size = $(".page-list .page-size").text();
    if (size == null || size.length <0)
        size = 5;
    return Number(index+1+eval((currentPage-1)*size));
}
//表单数据转json对象
function f(form) {
    var d = {};
    var t = form.serializeArray();
    $.each(t, function() {
        if (this.value != null && this.value.length >0)
            d[this.name] = this.value;
    });
    return d;
}
