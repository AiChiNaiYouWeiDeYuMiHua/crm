$(function() {
	$('#login_form').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			userName: {
				message: '用户名验证失败',
				validators: {
					notEmpty: {
						message: '用户名不能为空'
					},
					stringLength: {
						min: 2,
						max: 5,
						message: '用户名长度必须为2~6位之间'
					},
					regexp: {
						regexp: /^[\u4e00-\u9fa5]{2,5}$/,
						message: '用户名必须为汉字'
					}
				}
			},
            userPassword: {
				validators: {
					notEmpty: {
						message: '密码不能为空'
					},
					stringLength: {
						min: 6,
						max: 16,
						message: '密码必须为6~18位之间'
					},
					regexp: {
						regexp: /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/,
						message: '密码必须是数字和字母的组合'
					}

				}
			}
		}
	});

    $("#submit").on("click", function () {
        //获取表单对象
        var bootstrapValidator = $("#login_form").data('bootstrapValidator');
        //手动触发验证
        bootstrapValidator.validate();
        if (bootstrapValidator.isValid()) {
            // alert("表单序列化结果1："+$("#login_form").serialize());
            $.ajax({
                url: 'admin/login_to_index1',
                type: "POST",
                async: false,
                dataType: 'json',
                contentType: 'application/x-www-form-urlencoded',
                data: $("#login_form").serialize(),
                success: function (result) {
                    // alert("表单序列化结果3："+$("#login_form").serialize());
                    if (result.code == 200) {
                    	// alert(result.msg);
                        // alert(typeof result.msg);
                    	if (result.msg == "用户名错误") {
                            swal("失败", result.msg, "error");
                            $("#login_form")[0].reset();
						}else if (result.msg == "密码错误"){
                            swal("失败", result.msg, "error");
                            $("#login_form")[0].reset();
						}else if(result.msg == "登录成功"){
                            swal("成功", result.msg, "success");
                            window.location.href='index1';
                        }else {
                            alert("表单序列化结果45："+$("#login_form").serialize());
						}
                        // $('#add_role').modal('hide');
                        //表单重置
                        // $("#login_form")[0].reset();
                        // $("#table111").bootstrapTable('refresh');
                    }
                },
                error: function (data) {
                    swal("连接服务器错误", "", "error");
                }
            });
        }
    });
});