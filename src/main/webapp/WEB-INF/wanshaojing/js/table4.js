$(function() {
	$(".table4").bootstrapTable({
		method: 'get',
		striped: true,
		showExport: true,
		cache: false,
		pagination: false,
		sortable: false,
		sortOrder: "desc",
		pageNumber: 1,
		pageSize: 10,
		//          url: "/account/list",
		sidePagination: "server",
		queryParamsType: '',

		columns: [{
			field: 'aid',
			title: '品名',
			align: 'center',
			footerFormatter: "合计"
		}, {
			field: 'name',
			title: '型号',
			align: 'center',

		}, {
			field: 'sex',
			title: '规格',
			align: 'center'
		}, {
			field: 'code',
			title: '单位',
			align: 'center'
		}, {
			field: 'openMoney',
			title: '入库量',
			align: 'center',
			//              footerFormatter:sumFormatter
		}, {
			field: 'openTime',
			title: '备注',
			align: 'center'
		}],
		data: [{
			aid: '烟雾报警器',
			name: 'ASH-01',
			sex: 'ASH-01',
			code: '台',
			openMoney: '10台',
			openTime: '66666'
		}, {
			aid: '烟雾报警器',
			name: 'ASH-01',
			sex: 'ASH-01',
			code: '台',
			openMoney: '10台',
			openTime: '66666'
		}, {
			aid: '烟雾报警器',
			name: 'ASH-01',
			sex: 'ASH-01',
			code: '台',
			openMoney: '10台',
			openTime: '66666'
		}],
		onLoadSuccess: function(data) {

		},
		onLoadError: function(status) {}
	})
	addFotter($('.table4'), {
		aid: '合计',
		sex: '',
		code: '',
		openMoney: '10台',
		openTime: ''
	})

	addFotter($('.table4'), {
		aid: '合计(大写金额)',
		sex: '',
		code: '',
		openMoney: '玖仟壹佰玖拾捌元整',
		openTime: ''
	})

	formatterFotter($('.table4'), 'aid', 2, 2);
	formatterFotter($('.table4'), 'aid', 2, 1);

})
//  //编辑时间
//  function edit(id) {
//      $("#table").bootstrapTable('check',id);
//      var data = $("#table").bootstrapTable('getSelections')[0]
//      modal_data = data.aid;
//      $("#table").bootstrapTable('uncheck',id);
//      $('#msg').modal({
//       remote:'/modal/accountModify.html'
//      })
//   }
// 
//  function sumFormatter(data) {
//      field = this.field;
//      return data.reduce(function(sum, row) {
//          return sum + (+row[field]);
//      }, 0);
//  }