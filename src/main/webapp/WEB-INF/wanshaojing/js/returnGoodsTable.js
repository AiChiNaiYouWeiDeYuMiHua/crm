$(function() {
	$("#returnGoodsTable").bootstrapTable({
		method: 'get',
		striped: true,
		cache: false,
		pagination: false,
		sortable: false,
		sortOrder: "desc",
		pageNumber: 1,
		pageSize: 10,
		//          url: "/account/list",
		sidePagination: "server",
		queryParamsType: '',

		columns: [{
			field: 'aid',
			title: '品名',
			align: 'center',
//			footerFormatter: "合计"
		}, {
			field: 'name',
			title: '型号',
			align: 'center'
		}, {
			field: 'sex',
			title: '规格',
			align: 'center'
		}, {
			field: 'code',
			title: '单位',
			align: 'center'
		}, {
			field: 'openMoney',
			title: '退货数量',
			align: 'center',
//			footerFormatter: "合计"
		}, {
			field: 'openTime',
			title: '已出库',
			align: 'center'
		}, {
			field: 'balance',
			title: '未入库',
			align: 'center',
		}, {
			field: 'aid1',
			title: '单价',
			align: 'center',
		}, {
			field: 'openTime1',
			title: '金额',
			align: 'center',
//			footerFormatter: "合计"
		}, {
			field: 'balance1',
			title: '退货原因',
			align: 'center',

		}, {
			field: 'openTime2',
			title: '备注',
			align: 'center'
		}],
		data: [{
			aid: '烟雾报警器',
			name: 'ASH-01',
			sex: '台',
			code: '台',
			openMoney: '台',
			openTime: '10台',
			balance: '11.5',
			aid1: '10台',
			openTime1: '￥70,000.00',
			balance1: '七天无理由退货',
			openTime2: '66666'
		}, {
			aid: '烟雾报警器',
			name: 'ASH-01',
			sex: '台',
			code: '台',
			openMoney: '台',
			openTime: '10台',
			balance: '11.5',
			aid1: '10台',
			openTime1: '￥70,000.00',
			balance1: '七天无理由退货',
			openTime2: '66666'
		}, {
			aid: '烟雾报警器',
			name: 'ASH-01',
			sex: '台',
			code: '台',
			openMoney: '台',
			openTime: '10台',
			balance: '11.5',
			aid1: '10台',
			openTime1: '￥70,000.00',
			balance1: '七天无理由退货',
			openTime2: '66666'
		}],
		onLoadSuccess: function(data) {

		},
		onLoadError: function(status) {
			alert(1)
		}
	})

//	addFotter($('#table1'), {
//		aid: '合计',
//		openMoney: '',
//		openTime: '',
//		balance: '',
//		aid1: '',
//		openTime1: '￥70,000.00',
//		balance1: '',
//		openTime2: ''
//	})
//
//	addFotter($('#table1'), {
//		aid: '合计(大写金额)',
//		openMoney: '',
//		openTime: '',
//		balance: '',
//		aid1: '',
//		openTime1: '玖仟壹佰玖拾捌元整',
//		balance1: '',
//		openTime2: ''
//	})
//
//	formatterFotter($('#table1'), 'aid', 4, 2);
//	formatterFotter($('#table1'), 'aid', 4, 1);
	//格式化

	//编辑时间

})
//  function edit(id) {
//      $("#table").bootstrapTable('check',id);
//      var data = $("#table").bootstrapTable('getSelections')[0]
//      modal_data = data.aid;
//      $("#table").bootstrapTable('uncheck',id);
//      $('#msg').modal({
//       remote:'/modal/accountModify.html'
//      })
//   }
//  function totalTextFormatter(data) {
//      return ""
//  }
//  function sumFormatter(data) {
////      field = this.field;
////      return data.reduce(function(sum, row) {
////          return sum + (+row[field]);
////      }, 0);
//return ""
//  }
function addFotter(table, json) {
	table.bootstrapTable('append', json);
}

function formatterFotter(table, field, colspan, num) {
	table.bootstrapTable('mergeCells', {
		index: getLength(table) - num + 1,
		field: field,
		colspan: colspan
	});
}

function getLength(table) {
	return table.bootstrapTable('getData', true).length - 1;
}