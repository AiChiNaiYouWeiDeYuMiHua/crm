$("#table3").bootstrapTable({
	method: 'get',
	striped: true,
	showExport: true,
	cache: false,
	pagination: false,
	sortable: false,
	sortOrder: "desc",
	pageNumber: 1,
	pageSize: 10,
	//          url: "/account/list",
	sidePagination: "server",
	queryParamsType: '',
	exportOptions: {
		fileName: $('#table-title').text(),
		ignoreColumn: [10]
	},

	columns: [{
		field: 'aid',
		title: '品名',
		align: 'center',
		footerFormatter: "合计"
	}, {
		field: 'name',
		title: '型号',
		align: 'center'
	}, {
		field: 'sex',
		title: '规格',
		align: 'center'
	}, {
		field: 'code',
		title: '单位',
		align: 'center'
	}, {
		field: 'openMoney',
		title: '退货数量',
		align: 'center',
	}, {
		field: 'openTime',
		title: '已出库',
		align: 'center'
	}, {
		field: 'balance',
		title: '单价',
		align: 'center',
	}, {
		field: 'aid1',
		title: '退货',
		align: 'center',
		//              formatter:operateFormatter,
		width: "20%"
	}, {
		field: 'openTime1',
		title: '金额',
		align: 'center',
		footerFormatter: "合计"
	}, {
		field: 'balance1',
		title: '退货原因',
		align: 'center',
		footerFormatter: "合计"
	}, {
		field: 'openTime2',
		title: '备注',
		align: 'center'
	}],
	data: [{
		aid: '1',
		name: '烟雾报警器',
		sex: 'ASH-01',
		code: '台',
		openMoney: '10台',
		openTime: '10台',
		balance: '12.5',
		aid1: '1',
		openTime1: '￥70,000.00',
		balance1: '七天无理由退货',
		openTime2: '66666'
	}, {
		aid: '1',
		name: '烟雾报警器',
		sex: 'ASH-01',
		code: '台',
		openMoney: '10台',
		openTime: '10台',
		balance: '12.5',
		aid1: '1',
		openTime1: '￥70,000.00',
		balance1: '七天无理由退货',
		openTime2: '66666'
	}, {
		aid: '1',
		name: '烟雾报警器',
		sex: 'ASH-01',
		code: '台',
		openMoney: '10台',
		openTime: '10台',
		balance: '12.5',
		aid1: '1',
		openTime1: '￥70,000.00',
		balance1: '七天无理由退货',
		openTime2: '66666'
	}],
	onLoadSuccess: function(data) {

	},
	onLoadError: function(status) {
		alert(3)
	}
})
addFotter($('#table3'), {
	aid: '合计',
	sex: '',
	code: '',
	openMoney: '',
	openTime: '',
	balance: '',
	aid1: '',
	openTime1: '￥70,000.00',
	balance1: '',
	openTime2: ''
})

addFotter($('#table3'), {
	aid: '合计（大写金额）',
	sex: '',
	code: '',
	openMoney: '',
	openTime: '',
	balance: '',
	aid1: '',
	openTime1: '玖仟壹佰玖拾捌元整',
	balance1: '',
	openTime2: ''
})

formatterFotter($('#table3'), 'aid', 2, 2);
formatterFotter($('#table3'), 'aid', 2, 1);
//格式化
//  function operateFormatter(value, row, index) {
//     return [
//             '<a onclick="edit('+index+')" class=" btn btn-default  btn-sm" style="margin-right:15px;">编辑</a>',
//             '<a href="/account/delete?id='+value+'" class=" btn btn-default  btn-sm" style="margin-right:15px;">销户</a>',
//             '<a   href="/account/reset?id='+value+'" class=" btn btn-default  btn-sm" style="margin-right:15px;">重置密码</a>',
//           '<a  href="/transfer/ok?id='+value+'" class=" btn btn-default  btn-sm" style="margin-right:15px;">对账</a>',
//                       ].join('');
//  }
//  //编辑时间
//  function edit(id) {
//      $("#table").bootstrapTable('check',id);
//      var data = $("#table").bootstrapTable('getSelections')[0]
//      modal_data = data.aid;
//      $("#table").bootstrapTable('uncheck',id);
//      $('#msg').modal({
//       remote:'/modal/accountModify.html'
//      })
//   }
//
//  function sumFormatter(data) {
//      field = this.field;
//      return data.reduce(function(sum, row) {
//          return sum + (+row[field]);
//      }, 0);
//  }