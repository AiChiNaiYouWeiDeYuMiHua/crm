
			/**
			 * 工具
			 */
			var chartU = echarts.init(document.getElementById('gongju'))
			option = {
				title: {
					text: '工具'
				},
				tooltip: {
					trigger: 'axis'
				},
//				grid: {
//					left: '3%',
//					right: '4%',
//					bottom: '3%',
//					containLabel: true
//				},
				toolbox: {
					feature: {
						saveAsImage: {}
					}
				},
				xAxis: {
					type: 'category',
					boundaryGap: false,
					data: ['', '', '', '', '', '', '']
				},
				yAxis: {
					type: 'value'
				},
				series: [{
						name: '邮件营销',
						type: 'line',
						stack: '总量',
						data: [120, 132, 101, 134, 90, 230, 210]
					},
					{
						name: '联盟广告',
						type: 'line',
						stack: '总量',
						data: [220, 182, 191, 234, 290, 330, 310]
					}
				]
			};

			chartU.setOption(option);
		