create table ACCOUNT
(
  AID       NUMBER(6) not null
    constraint ACCOUNT_PK
    primary key,
  NAME      VARCHAR2(30),
  PASS      VARCHAR2(30),
  CODE      VARCHAR2(30),
  OPENMONEY NUMBER       default NULL,
  OPENTIME  TIMESTAMP(6) default NULL,
  BALANCE   NUMBER       default NULL,
  STATUS    NUMBER(1),
  SEX       VARCHAR2(6)
)
/

