/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     2018/8/2 17:19:14                            */
/*==============================================================*/


alter table dept_announce
   drop constraint FK_DEPT_ANN_DEPT_ANNO_TB_DEPT;

alter table dept_announce
   drop constraint FK_DEPT_ANN_DEPT_ANNO_TB_ANNOU;

alter table dept_role
   drop constraint FK_DEPT_ROL_DEPT_ROLE_TB_ROLE;

alter table dept_role
   drop constraint FK_DEPT_ROL_DEPT_ROLE_TB_DEPT;

alter table project_expect
   drop constraint FK_PROJECT__RELATIONS_TB_PROJE;

alter table role_fc
   drop constraint FK_ROLE_FC_ROLE_FC_TB_FUNCT;

alter table role_fc
   drop constraint FK_ROLE_FC_ROLE_FC2_TB_ROLE;

alter table tb_addr
   drop constraint FK_TB_ADDR_CUSTOMER__TB_CUSTO;

alter table tb_allocation
   drop constraint FK_TB_ALLOC_RELATIONS_TB_USER2;

alter table tb_allocation
   drop constraint FK_TB_ALLOC_RELATIONS_TB_USER;

alter table tb_allocation
   drop constraint FK_TB_ALLOC_RELATIONS_TB_WARE2;

alter table tb_allocation
   drop constraint FK_TB_ALLOC_RELATIONS_TB_WAREH;

alter table tb_approval
   drop constraint FK_TB_APPRO_REFERENCE_TB_USER;

alter table tb_care
   drop constraint FK_TB_CARE_RELATIONS_TB_USER;

alter table tb_care
   drop constraint FK_TB_CARE_RELATIONS_TB_CONTA;

alter table tb_care
   drop constraint FK_TB_CARE_RELATIONS_TB_CUSTO;

alter table tb_competitor
   drop constraint FK_TB_COMPE_RELATIONS_TB_SALE_;

alter table tb_complaint
   drop constraint FK_TB_COMPL_RELATIONS_TB_CONTA;

alter table tb_complaint
   drop constraint FK_TB_COMPL_RELATIONS_TB_CUSTO;

alter table tb_complaint
   drop constraint FK_TB_COMPL_RELATIONS_TB_USER;

alter table tb_contacts
   drop constraint FK_TB_CONTA_RELATIONS_TB_CUSTO;

alter table tb_contacts
   drop constraint FK_TB_CONTA_RELATIONS_TB_SERVE;

alter table tb_customer
   drop constraint FK_TB_CUSTO_RELATIONS_TB_USER;

alter table tb_customer
   drop constraint FK_TB_CUSTO_RELATIONS_TB_SERVE;

alter table tb_customer
   drop constraint FK_TB_CUSTO_CUSTOMER__TB_ADDR;

alter table tb_daily_recode
   drop constraint FK_TB_DAILY_USER_DAIL_TB_USER;

alter table tb_deliver_detail
   drop constraint FK_TB_DELIV_RELATIONS_TB_CUSTO;

alter table tb_deliver_detail
   drop constraint FK_TB_DELIV_RELATIONS_TB_DELIV;

alter table tb_deliver_detail
   drop constraint FK_TB_DELIV_RELATIONS_TB_ORDER;

alter table tb_deliver_detail
   drop constraint FK_TB_DELIV_RELATIONS_TB_PRODU;

alter table tb_deliver_goods
   drop constraint FK_TB_DELIV_RELATIONS_TB_ADDRE;

alter table tb_deliver_goods
   drop constraint FK_TB_DELIV_RELATIONS_TB_WH_PU;

alter table tb_deliver_goods
   drop constraint FK_TB_DELIV_RELATIONS_TB_USER;

alter table tb_demand
   drop constraint FK_TB_DEMAN_RELATIONS_TB_SALE_;

alter table tb_dimission
   drop constraint FK_TB_DIMIS_DIMI_TYPE_TB_DIMIS;

alter table tb_functions
   drop constraint FK_TB_FUNCT_FC_FC_TYP_TB_FC_TY;

alter table tb_log
   drop constraint FK_TB_LOG_USER_LOG_TB_USER;

alter table tb_log_data
   drop constraint FK_TB_LOG_D_USER_LOG__TB_USER;

alter table tb_maintain_order
   drop constraint FK_TB_MAINT_RELATIONS_TB_CUSTO;

alter table tb_maintain_order
   drop constraint FK_TB_MAINT_RELATIONS_TB_USER;

alter table tb_memorial_day
   drop constraint FK_TB_MEMOR_RELATIONS_TB_CONTA;

alter table tb_memorial_day
   drop constraint FK_TB_MEMOR_RELATIONS_TB_CUSTO;

alter table tb_message
   drop constraint FK_TB_MESSA_RELATIONS_TB_ROLE;

alter table tb_order
   drop constraint FK_TB_ORDER_RELATIONS_TB_CUSTO;

alter table tb_order
   drop constraint FK_TB_ORDER_RELATIONS_TB_ADDRE;

alter table tb_order
   drop constraint FK_TB_ORDER_RELATIONS_TB_QUOTE;

alter table tb_order
   drop constraint FK_TB_ORDER_RELATIONS_TB_USER2;

alter table tb_order
   drop constraint FK_TB_ORDER_RELATIONS_TB_USER;

alter table tb_order_detail
   drop constraint FK_TB_ORDER_RELATIONS_TB_ORDER;

alter table tb_pay_back_detail
   drop constraint FK_TB_PAY_B_RELATIONS_TB_PLAN_;

alter table tb_pay_back_detail
   drop constraint FK_TB_PAY_B_RELATIONS_TB_RECEI;

alter table tb_pay_back_detail
   drop constraint FK_TB_PAY_B_RELATIONS_TB_MAIN2;

alter table tb_pay_back_detail
   drop constraint FK_TB_PAY_B_RELATIONS_TB_ORDER;

alter table tb_pay_back_detail
   drop constraint FK_TB_PAY_B_RELATIONS_TB_USER;

alter table tb_payment_records
   drop constraint FK_TB_PAYME_RELATIONS_TB_CUSTO;

alter table tb_payment_records
   drop constraint FK_TB_PAYME_RELATIONS_TB_PLAN_;

alter table tb_payment_records
   drop constraint FK_TB_PAYME_RELATIONS_TB_USER;

alter table tb_plan_pay_back
   drop constraint FK_TB_PLAN__RELATIONS_TB_CUST2;

alter table tb_plan_pay_back
   drop constraint FK_TB_PLAN__RELATIONS_TB_MAINT;

alter table tb_plan_pay_back
   drop constraint FK_TB_PLAN__RELATIONS_TB_ORDER;

alter table tb_plan_pay_back
   drop constraint FK_TB_PLAN__RELATIONS_TB_USER2;

alter table tb_plan_pay_detail
   drop constraint FK_TB_PLAN__RELATIONS_TB_CUSTO;

alter table tb_plan_pay_detail
   drop constraint FK_TB_PLAN__RELATIONS_TB_USER;

alter table tb_product
   drop constraint FK_TB_PRODU_RELATIONS_TB_PROD2;

alter table tb_product_category
   drop constraint FK_TB_PRODU_RELATIONS_TB_PRODU;

alter table tb_product_format
   drop constraint FK_TB_PRODU_RELATIONS_TB_PROD3;

alter table tb_proj_cus
   drop constraint FK_TB_PROJ__RELATIONS_TB_PROJE;

alter table tb_proj_cus
   drop constraint FK_TB_PROJ__RELATIONS_TB_CUSTO;

alter table tb_proj_mid
   drop constraint FK_RELATION_RELATIONS_TB_PROJ2;

alter table tb_proj_mid
   drop constraint FK_TB_PROJ__RELATIONS_TB_CONTA;

alter table tb_project
   drop constraint FK_TB_PROJE_RELATIONS_PROJECT_;

alter table tb_project
   drop constraint FK_TB_PROJE_RELATIONS_TB_USER;

alter table tb_pur_detail
   drop constraint FK_TB_PUR_D_RELATIONS_TB_PURCH;

alter table tb_purchase
   drop constraint FK_TB_PURCH_RELATIONS_TB_CUST2;

alter table tb_purchase
   drop constraint FK_TB_PURCH_RELATIONS_TB_WAREH;

alter table tb_purchase
   drop constraint FK_TB_PURCH_RELATIONS_TB_USER2;

alter table tb_purchase_receipt
   drop constraint FK_TB_PURCH_RELATIONS_TB_CUSTO;

alter table tb_purchase_receipt
   drop constraint FK_TB_PURCH_RELATIONS_TB_PURCH;

alter table tb_purchase_receipt
   drop constraint FK_TB_PURCH_RELATIONS_TB_PAY_B;

alter table tb_purchase_receipt
   drop constraint FK_TB_PURCH_RELATIONS_TB_PLAN_;

alter table tb_purchase_receipt
   drop constraint FK_TB_PURCH_RELATIONS_TB_USER;

alter table tb_quote
   drop constraint FK_TB_QUOTE_RELATIONS_TB_CONTA;

alter table tb_quote
   drop constraint FK_TB_QUOTE_RELATIONS_TB_CUSTO;

alter table tb_quote
   drop constraint FK_TB_QUOTE_RELATIONS_TB_SALE_;

alter table tb_quote
   drop constraint FK_TB_QUOTE_RELATIONS_TB_USER;

alter table tb_quote_detail
   drop constraint FK_TB_QUOTE_RELATIONS_TB_QUOTE;

alter table tb_quote_detail
   drop constraint FK_TB_QUOTE_RELATIONS_TB_PRODU;

alter table tb_receipt_records
   drop constraint FK_TB_RECEI_RELATIONS_TB_CUSTO;

alter table tb_receipt_records
   drop constraint FK_TB_RECEI_RELATIONS_TB_ORDER;

alter table tb_receipt_records
   drop constraint FK_TB_RECEI_RELATIONS_TB_USER;

alter table tb_return_goods
   drop constraint FK_TB_RETUR_RELATIONS_TB_CUSTO;

alter table tb_return_goods
   drop constraint FK_TB_RETUR_RELATIONS_TB_PURCH;

alter table tb_return_goods
   drop constraint FK_TB_RETUR_RELATIONS_TB_ORDER;

alter table tb_return_goods_detail
   drop constraint FK_TB_RETUR_RELATIONS_TB_RETUR;

alter table tb_return_goods_detail
   drop constraint FK_TB_RETUR_RELATIONS_TB_PRODU;

alter table tb_sale_opp
   drop constraint FK_TB_SALE__RELATIONS_TB_CONTA;

alter table tb_sale_opp
   drop constraint FK_TB_SALE__RELATIONS_TB_CUSTO;

alter table tb_sale_opp
   drop constraint FK_TB_SALE__RELATIONS_TB_USER;

alter table tb_serve
   drop constraint FK_TB_SERVE_RELATIONS_TB_CUSTO;

alter table tb_serve
   drop constraint FK_TB_SERVE_RELATIONS_TB_USER;

alter table tb_solution
   drop constraint FK_TB_SOLUT_RELATIONS_TB_SALE_;

alter table tb_task_action
   drop constraint FK_TB_TASK__RELATIONS_TB_CONTA;

alter table tb_task_action
   drop constraint FK_TB_TASK__RELATIONS_TB_CUSTO;

alter table tb_task_action
   drop constraint FK_TB_TASK__RELATIONS_TB_SALE_;

alter table tb_task_action
   drop constraint FK_TB_TASK__RELATIONS_TB_USER;

alter table tb_travel
   drop constraint FK_TB_TRAVE_RELATIONS_TB_CUSTO;

alter table tb_travel
   drop constraint FK_TB_TRAVE_RELATIONS_TB_SALE_;

alter table tb_travel
   drop constraint FK_TB_TRAVE_RELATIONS_TB_USER;

alter table tb_user
   drop constraint FK_TB_USER_USER_DEPT_TB_DEPT;

alter table tb_wh_check
   drop constraint FK_TB_WH_CH_RELATIONS_TB_WAREH;

alter table tb_wh_check
   drop constraint FK_TB_WH_CH_RELATIONS_TB_USER;

alter table tb_wh_number
   drop constraint FK_TB_WH_NU_RELATIONS_TB_WAREH;

alter table tb_wh_number
   drop constraint FK_TB_WH_NU_RELATIONS_TB_PRODU;

alter table tb_wh_pull
   drop constraint FK_TB_WH_PU_REFERENCE_TB_RETUR;

alter table tb_wh_pull
   drop constraint FK_TB_WH_PU_RELATIONS_TB_ORDER;

alter table tb_wh_pull
   drop constraint FK_TB_WH_PU_RELATIONS_TB_USER4;

alter table tb_wh_pull
   drop constraint FK_TB_WH_PU_RELATIONS_TB_USER;

alter table tb_wh_pull
   drop constraint FK_TB_WH_PU_RELATIONS_TB_WAREH;

alter table tb_wh_put
   drop constraint FK_TB_WH_PU_RELATIONS_TB_WARE2;

alter table tb_wh_put
   drop constraint FK_TB_WH_PU_RELATIONS_TB_PURCH;

alter table tb_wh_put
   drop constraint FK_TB_WH_PU_RELATIONS_TB_RETUR;

alter table tb_wh_put
   drop constraint FK_TB_WH_PU_RELATIONS_TB_USER2;

alter table tb_wh_put
   drop constraint FK_TB_WH_PU_RELATIONS_TB_USER3;

alter table tb_wp_detail
   drop constraint FK_TB_WP_DE_REFERENCE_TB_CUSTO;

alter table tb_wp_detail
   drop constraint FK_TB_WP_DE_RELATIONS_TB_WAREH;

alter table tb_wp_detail
   drop constraint FK_TB_WP_DE_RELATIONS_TB_PRODU;

alter table tb_wp_detail
   drop constraint FK_TB_WP_DE_RELATIONS_TB_USER;

drop index dept_announce2_FK;

drop index dept_announce_FK;

drop table dept_announce cascade constraints;

drop index dept_role2_FK;

drop index dept_role_FK;

drop table dept_role cascade constraints;

drop index Relationship_90_FK;

drop table project_expect cascade constraints;

drop index role_fc2_FK;

drop index role_fc_FK;

drop table role_fc cascade constraints;

drop index customer_addr_r2_FK;

drop table tb_addr cascade constraints;

drop table tb_address cascade constraints;

drop index Relationship_68_FK;

drop index Relationship_67_FK;

drop index Relationship_8_FK;

drop index Relationship_7_FK;

drop table tb_allocation cascade constraints;

drop table tb_announcement cascade constraints;

drop table tb_approval cascade constraints;

drop index Relationship_100_FK;

drop index Relationship_89_FK;

drop index Relationship_88_FK;

drop table tb_care cascade constraints;

drop index Relationship_51_FK;

drop table tb_competitor cascade constraints;

drop index Relationship_98_FK;

drop index Relationship_87_FK;

drop index Relationship_86_FK;

drop table tb_complaint cascade constraints;

drop index Relationship_85_FK;

drop index Relationship_57_FK;

drop table tb_contacts cascade constraints;

drop index Relationship_101_FK;

drop index Relationship_84_FK;

drop index customer_addr_r_FK;

drop table tb_customer cascade constraints;

drop index user_daily_recode_FK;

drop table tb_daily_recode cascade constraints;

drop index Relationship_110_FK;

drop index Relationship_19_FK;

drop index Relationship_18_FK;

drop index Relationship_17_FK;

drop table tb_deliver_detail cascade constraints;

drop index Relationship_69_FK;

drop index Relationship_14_FK;

drop index Relationship_13_FK;

drop table tb_deliver_goods cascade constraints;

drop index Relationship_49_FK;

drop table tb_demand cascade constraints;

drop table tb_dept cascade constraints;

drop index dimi_type_FK;

drop table tb_dimission cascade constraints;

drop table tb_dimission_type cascade constraints;

drop table tb_fc_type cascade constraints;

drop index fc_fc_type_FK;

drop table tb_functions cascade constraints;

drop index user_log_FK;

drop table tb_log cascade constraints;

drop index user_log_data_FK;

drop table tb_log_data cascade constraints;

drop index Relationship_114_FK;

drop index Relationship_75_FK;

drop table tb_maintain_order cascade constraints;

drop index Relationship_59_FK;

drop index Relationship_58_FK;

drop table tb_memorial_day cascade constraints;

drop index Relationship_119_FK;

drop table tb_message cascade constraints;

drop index Relationship_109_FK;

drop index Relationship_71_FK;

drop index Relationship_70_FK;

drop index Relationship_54_FK;

drop index Relationship_15_FK;

drop table tb_order cascade constraints;

drop index Relationship_16_FK;

drop table tb_order_detail cascade constraints;

drop index Relationship_78_FK;

drop index Relationship_43_FK;

drop index Relationship_27_FK;

drop index Relationship_22_FK;

drop table tb_pay_back_detail cascade constraints;

drop index Relationship_116_FK;

drop index Relationship_76_FK;

drop index Relationship_23_FK;

drop table tb_payment_records cascade constraints;

drop index Relationship_117_FK;

drop index Relationship_77_FK;

drop index Relationship_46_FK;

drop index Relationship_45_FK;

drop table tb_plan_pay_back cascade constraints;

drop index Relationship_113_FK;

drop index Relationship_74_FK;

drop table tb_plan_pay_detail cascade constraints;

drop index Relationship_1_FK;

drop table tb_product cascade constraints;

drop index Relationship_2_FK;

drop table tb_product_category cascade constraints;

drop index Relationship_3_FK;

drop table tb_product_format cascade constraints;

drop index Relationship_95_FK;

drop index Relationship_94_FK;

drop table tb_proj_cus cascade constraints;

drop index Relationship_93_FK;

drop index Relationship_92_FK;

drop table tb_proj_mid cascade constraints;

drop index Relationship_97_FK;

drop index Relationship_91_FK;

drop table tb_project cascade constraints;

drop index Relationship_24_FK;

drop table tb_pur_detail cascade constraints;

drop index Relationship_112_FK;

drop index Relationship_73_FK;

drop index Relationship_41_FK;

drop table tb_purchase cascade constraints;

drop index Relationship_111_FK;

drop index Relationship_72_FK;

drop index Relationship_35_FK;

drop index Relationship_33_FK;

drop index Relationship_25_FK;

drop table tb_purchase_receipt cascade constraints;

drop index Relationship_108_FK;

drop index Relationship_107_FK;

drop index Relationship_83_FK;

drop index Relationship_52_FK;

drop table tb_quote cascade constraints;

drop index Relationship_55_FK;

drop index Relationship_53_FK;

drop table tb_quote_detail cascade constraints;

drop index Relationship_118_FK;

drop index Relationship_79_FK;

drop index Relationship_44_FK;

drop table tb_receipt_records cascade constraints;

drop index Relationship_115_FK;

drop index Relationship_40_FK;

drop index Relationship_31_FK;

drop table tb_return_goods cascade constraints;

drop index Relationship_42_FK;

drop index Relationship_21_FK;

drop table tb_return_goods_detail cascade constraints;

drop table tb_role cascade constraints;

drop index Relationship_106_FK;

drop index Relationship_105_FK;

drop index Relationship_82_FK;

drop table tb_sale_opp cascade constraints;

drop index Relationship_99_FK;

drop index Relationship_60_FK;

drop table tb_serve cascade constraints;

drop index Relationship_50_FK;

drop table tb_solution cascade constraints;

drop index Relationship_103_FK;

drop index Relationship_102_FK;

drop index Relationship_80_FK;

drop index Relationship_47_FK;

drop table tb_task_action cascade constraints;

drop index Relationship_104_FK;

drop index Relationship_81_FK;

drop index Relationship_48_FK;

drop table tb_travel cascade constraints;

drop index user_dept_FK;

drop table tb_user cascade constraints;

drop table tb_warehouse cascade constraints;

drop index Relationship_66_FK;

drop index Relationship_6_FK;

drop table tb_wh_check cascade constraints;

drop index Relationship_5_FK;

drop index Relationship_4_FK;

drop table tb_wh_number cascade constraints;

drop index Relationship_65_FK;

drop index Relationship_64_FK;

drop index Relationship_10_FK;

drop index Relationship_9_FK;

drop table tb_wh_pull cascade constraints;

drop index Relationship_62_FK;

drop index Relationship_61_FK;

drop index Relationship_39_FK;

drop index Relationship_36_FK;

drop index Relationship_11_FK;

drop table tb_wh_put cascade constraints;

drop index Relationship_63_FK;

drop index Relationship_20_FK;

drop index Relationship_12_FK;

drop table tb_wp_detail cascade constraints;

/*==============================================================*/
/* Table: dept_announce                                         */
/*==============================================================*/
create table dept_announce 
(
   dept_announce_id     NUMBER(10)           not null,
   dept_id              NUMBER(10)           not null,
   announce_id
   id      NUMBER(10)           not null,
   constraint PK_DEPT_ANNOUNCE primary key (dept_announce_id)
);

/*==============================================================*/
/* Index: dept_announce_FK                                      */
/*==============================================================*/
create index dept_announce_FK on dept_announce (
   dept_id ASC
);

/*==============================================================*/
/* Index: dept_announce2_FK                                     */
/*==============================================================*/
create index dept_announce2_FK on dept_announce (
   announce_id
   id ASC
);

/*==============================================================*/
/* Table: dept_role                                             */
/*==============================================================*/
create table dept_role 
(
   role_dept_id         NUMBER(10)           not null,
   role_id              NUMBER(10)           not null,
   dept_id              NUMBER(10)           not null,
   constraint PK_DEPT_ROLE primary key (role_dept_id)
);

/*==============================================================*/
/* Index: dept_role_FK                                          */
/*==============================================================*/
create index dept_role_FK on dept_role (
   role_id ASC
);

/*==============================================================*/
/* Index: dept_role2_FK                                         */
/*==============================================================*/
create index dept_role2_FK on dept_role (
   dept_id ASC
);

/*==============================================================*/
/* Table: project_expect                                        */
/*==============================================================*/
create table project_expect 
(
   pexp_id              NUMBER(10)           not null,
   proj_id              NUMBER(10),
   pexp_date            DATE,
   pexp_money           NUMBER(15),
   pexp_possibility     VARCHAR2(20),
   constraint PK_PROJECT_EXPECT primary key (pexp_id)
);

/*==============================================================*/
/* Index: Relationship_90_FK                                    */
/*==============================================================*/
create index Relationship_90_FK on project_expect (
   proj_id ASC
);

/*==============================================================*/
/* Table: role_fc                                               */
/*==============================================================*/
create table role_fc 
(
   角role_fc_id          NUMBER(10)           not null,
   fc_id                NUMBER(10)           not null,
   role_id              NUMBER(10)           not null,
   constraint PK_ROLE_FC primary key (角role_fc_id)
);

/*==============================================================*/
/* Index: role_fc_FK                                            */
/*==============================================================*/
create index role_fc_FK on role_fc (
   fc_id ASC
);

/*==============================================================*/
/* Index: role_fc2_FK                                           */
/*==============================================================*/
create index role_fc2_FK on role_fc (
   role_id ASC
);

/*==============================================================*/
/* Table: tb_addr                                               */
/*==============================================================*/
create table tb_addr 
(
   addr_id              NUMBER(10)           not null,
   cus_id               NUMBER(10),
   addr_country         VARCHAR2(20),
   addr_provence        VARCHAR2(20),
   addr_area            VARCHAR2(20),
   addr_zip_code        NUMBER(10),
   constraint PK_TB_ADDR primary key (addr_id)
);

/*==============================================================*/
/* Index: customer_addr_r2_FK                                   */
/*==============================================================*/
create index customer_addr_r2_FK on tb_addr (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_address                                            */
/*==============================================================*/
create table tb_address 
(
   address_id           NUMBER(10)           not null,
   address_name         VARCHAR2(10),
   address_tel          VARCHAR2(11),
   address_province     VARCHAR2(20),
   address_city         VARCHAR2(20),
   address_content      VARCHAR2(100),
   address_code         VARCHAR2(10),
   constraint PK_TB_ADDRESS primary key (address_id)
);

/*==============================================================*/
/* Table: tb_allocation                                         */
/*==============================================================*/
create table tb_allocation 
(
   allocation_id        NUMBER(10)           not null,
   user_id              INTEGER,
   tb__user_id          INTEGER,
   wh_id                NUMBER(4),
   tb__wh_id            NUMBER(4),
   allocation_name      VARCHAR2(30),
   allocation_date      DATE,
   address_phone        VARCHAR2(15),
   allocation_status    NUMBER(1),
   allocation_other     VARCHAR2(100),
   login_delete         NUMBER(1),
   constraint PK_TB_ALLOCATION primary key (allocation_id)
);

/*==============================================================*/
/* Index: Relationship_7_FK                                     */
/*==============================================================*/
create index Relationship_7_FK on tb_allocation (
   wh_id ASC
);

/*==============================================================*/
/* Index: Relationship_8_FK                                     */
/*==============================================================*/
create index Relationship_8_FK on tb_allocation (
   tb__wh_id ASC
);

/*==============================================================*/
/* Index: Relationship_67_FK                                    */
/*==============================================================*/
create index Relationship_67_FK on tb_allocation (
   tb__user_id ASC
);

/*==============================================================*/
/* Index: Relationship_68_FK                                    */
/*==============================================================*/
create index Relationship_68_FK on tb_allocation (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_announcement                                       */
/*==============================================================*/
create table tb_announcement 
(
   announce_id
   id      NUMBER(10)           not null,
   announce_title       VARCHAR2(50),
   announce_content     CLOB,
   announce_is_topping  INTEGER,
   announce_user        VARCHAR2(20),
   announce_time        DATE,
   announce_annex       VARCHAR2(2000),
   announce_type        VARCHAR2(20),
   announce_is_del      INTEGER,
   constraint PK_TB_ANNOUNCEMENT primary key (announce_id
   id)
);

/*==============================================================*/
/* Table: tb_approval                                           */
/*==============================================================*/
create table tb_approval 
(
   approval_id          NUMBER(10)           not null,
   approval_receiver_id INTEGER,
   approval_user_id     NUMBER(10),
   approval_type        VARCHAR2(20),
   approval_title       VARCHAR2(50),
   approval_status      VARCHAR2(4),
   approval_apply_time  DATE,
   approval_time        DATE,
   approval_content     VARCHAR2(200),
   constraint PK_TB_APPROVAL primary key (approval_id)
);

/*==============================================================*/
/* Table: tb_care                                               */
/*==============================================================*/
create table tb_care 
(
   care_id              NUMBER(10)           not null,
   cots_id              NUMBER(10),
   cus_id               NUMBER(10),
   user_id              INTEGER,
   care_theme           VARCHAR2(200),
   care_time            DATE,
   care_type            VARCHAR2(20),
   care_content         VARCHAR2(1000),
   care_remark          VARCHAR2(2000),
   constraint PK_TB_CARE primary key (care_id)
);

comment on table tb_care is
'执行人(用户id)，外键';

/*==============================================================*/
/* Index: Relationship_88_FK                                    */
/*==============================================================*/
create index Relationship_88_FK on tb_care (
   cots_id ASC
);

/*==============================================================*/
/* Index: Relationship_89_FK                                    */
/*==============================================================*/
create index Relationship_89_FK on tb_care (
   cus_id ASC
);

/*==============================================================*/
/* Index: Relationship_100_FK                                   */
/*==============================================================*/
create index Relationship_100_FK on tb_care (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_competitor                                         */
/*==============================================================*/
create table tb_competitor 
(
   competitor_id        INTEGER              not null,
   opp_id               INTEGER,
   company              VARCHAR2(30),
   price                NUMBER(12,2),
   competitive_ability  INTEGER,
   competitive_product  CLOB,
   disadvantage         CLOB,
   strategy             CLOB,
   remarks              VARCHAR2(100),
   is_delete            INTEGER,
   constraint PK_TB_COMPETITOR primary key (competitor_id)
);

/*==============================================================*/
/* Index: Relationship_51_FK                                    */
/*==============================================================*/
create index Relationship_51_FK on tb_competitor (
   opp_id ASC
);

/*==============================================================*/
/* Table: tb_complaint                                          */
/*==============================================================*/
create table tb_complaint 
(
   complain_id          NUMBER(10)           not null,
   cots_id              NUMBER(10),
   user_id              INTEGER,
   cus_id               NUMBER(10),
   complain_theme       VARCHAR2(20),
   complain_type        VARCHAR2(20),
   complain_bewrite     VARCHAR2(2000),
   complain_date        DATE,
   complain_emergency   VARCHAR2(10),
   complain_process     VARCHAR2(500),
   complain_results     VARCHAR2(20),
   complain_time_spent  DATE,
   compain_back_comfimed VARCHAR2(500),
   compain_remark       VARCHAR2(2000),
   constraint PK_TB_COMPLAINT primary key (complain_id)
);

comment on table tb_complaint is
'首问接待人（用户id），外键';

/*==============================================================*/
/* Index: Relationship_86_FK                                    */
/*==============================================================*/
create index Relationship_86_FK on tb_complaint (
   cots_id ASC
);

/*==============================================================*/
/* Index: Relationship_87_FK                                    */
/*==============================================================*/
create index Relationship_87_FK on tb_complaint (
   cus_id ASC
);

/*==============================================================*/
/* Index: Relationship_98_FK                                    */
/*==============================================================*/
create index Relationship_98_FK on tb_complaint (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_contacts                                           */
/*==============================================================*/
create table tb_contacts 
(
   cots_id              NUMBER(10)           not null,
   cus_id               NUMBER(10),
   serve_id             NUMBER(10),
   cots_name            VARCHAR2(10)         not null,
   cots_photo           VARCHAR2(100),
   cots_sex             VARCHAR2(2),
   cots_classification  VARCHAR2(10),
   cots_business        VARCHAR2(20),
   cots_project         VARCHAR2(20),
   cots_technical       VARCHAR2(20),
   cots_type            VARCHAR2(20),
   cots_department      VARCHAR2(20),
   cots_workphone       NUMBER(20),
   cots_place           VARCHAR2(50),
   cots_personal_phone  NUMBER(20),
   "cots_MSN(QQ)"       VARCHAR2(20),
   cots_fax             VARCHAR2(20),
   cots_zipcode         VARCHAR2(50),
   cots_wechart         VARCHAR2(25),
   cots_interest        VARCHAR2(100),
   cots_cardtype        VARCHAR2(20),
   cots_idnum           VARCHAR2(25),
   cots_create_date     DATE,
   cots_modify_date     DATE,
   cots_habits          VARCHAR2(100),
   cots_character       VARCHAR2(50),
   cots_social_characteristics VARCHAR2(100),
   cots_annual_income   NUMBER(20),
   cots_consumption_habits VARCHAR2(50),
   constraint PK_TB_CONTACTS primary key (cots_id)
);

/*==============================================================*/
/* Index: Relationship_57_FK                                    */
/*==============================================================*/
create index Relationship_57_FK on tb_contacts (
   cus_id ASC
);

/*==============================================================*/
/* Index: Relationship_85_FK                                    */
/*==============================================================*/
create index Relationship_85_FK on tb_contacts (
   serve_id ASC
);

/*==============================================================*/
/* Table: tb_customer                                           */
/*==============================================================*/
create table tb_customer 
(
   cus_id               NUMBER(10)           not null,
   serve_id             NUMBER(10),
   user_id              INTEGER,
   addr_id              NUMBER(10),
   cus_name             VARCHAR2(50)         not null,
   cus_abbreviation     VARCHAR2(50)         not null,
   cus_type             VARCHAR2(10),
   cus_proprietor       VARCHAR2(20),
   cus_lifecycle        VARCHAR2(10),
   cus_level            VARCHAR2(10),
   cus_come             VARCHAR2(100),
   cus_stage            VARCHAR2(20),
   cus_industry         VARCHAR2(50),
   cus_credit           VARCHAR2(10),
   cus_phone            NUMBER(20),
   cus_net              VARCHAR2(100),
   cus_advance          NUMBER(15),
   cus_addr             VARCHAR2(30),
   cus_date             DATE,
   cus_updata_date      DATE,
   cus_determine        VARCHAR2(10),
   cus_class            VARCHAR2(10),
   cus_advance_money    NUMBER(15),
   cus_advance_num      NUMBER(10),
   cus_exist_date       NUMBER(5),
   cus_dc_status        VARCHAR2(10),
   cus_knot             VARCHAR2(20),
   constraint PK_TB_CUSTOMER primary key (cus_id)
);

/*==============================================================*/
/* Index: customer_addr_r_FK                                    */
/*==============================================================*/
create index customer_addr_r_FK on tb_customer (
   addr_id ASC
);

/*==============================================================*/
/* Index: Relationship_84_FK                                    */
/*==============================================================*/
create index Relationship_84_FK on tb_customer (
   serve_id ASC
);

/*==============================================================*/
/* Index: Relationship_101_FK                                   */
/*==============================================================*/
create index Relationship_101_FK on tb_customer (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_daily_recode                                       */
/*==============================================================*/
create table tb_daily_recode 
(
   daily_recode_id      NUMBER(10)           not null,
   user_id              INTEGER,
   daily_recode_content CLOB,
   daily_recode_accessories VARCHAR2(2000),
   daily_plan_content   CLOB,
   daily_plan_accessories VARCHAR2(2000),
   daily_recode_time    DATE,
   daily_recode_type    INTEGER,
   daily_is_del         INTEGER,
   constraint PK_TB_DAILY_RECODE primary key (daily_recode_id)
);

/*==============================================================*/
/* Index: user_daily_recode_FK                                  */
/*==============================================================*/
create index user_daily_recode_FK on tb_daily_recode (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_deliver_detail                                     */
/*==============================================================*/
create table tb_deliver_detail 
(
   dd_id                NUMBER(10)           not null,
   product_id           NUMBER(10),
   cus_id               NUMBER(10),
   order_id             NUMBER(10),
   dg_id                NUMBER(10),
   dd_number            NUMBER(4),
   dd_total             NUMBER(8,2),
   dd_date              DATE,
   dd_cost              NUMBER(8,2),
   dd_other             VARCHAR2(100),
   constraint PK_TB_DELIVER_DETAIL primary key (dd_id)
);

/*==============================================================*/
/* Index: Relationship_17_FK                                    */
/*==============================================================*/
create index Relationship_17_FK on tb_deliver_detail (
   dg_id ASC
);

/*==============================================================*/
/* Index: Relationship_18_FK                                    */
/*==============================================================*/
create index Relationship_18_FK on tb_deliver_detail (
   order_id ASC
);

/*==============================================================*/
/* Index: Relationship_19_FK                                    */
/*==============================================================*/
create index Relationship_19_FK on tb_deliver_detail (
   product_id ASC
);

/*==============================================================*/
/* Index: Relationship_110_FK                                   */
/*==============================================================*/
create index Relationship_110_FK on tb_deliver_detail (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_deliver_goods                                      */
/*==============================================================*/
create table tb_deliver_goods 
(
   dg_id                NUMBER(10)           not null,
   address_id           NUMBER(10),
   user_id              INTEGER,
   wpull_id             NUMBER(10),
   dg_date              DATE,
   dg_pkg_number        NUMBER(2),
   dg_model             VARCHAR2(10),
   dg_weight            NUMBER(6,2),
   dg_logistice         VARCHAR2(50),
   dg_logistic_id       VARCHAR2(30),
   dg_freight           NUMBER(6,2),
   dg_freight_modal     VARCHAR2(10),
   dg_other             VARCHAR2(100),
   logic_delete         NUMBER(1),
   constraint PK_TB_DELIVER_GOODS primary key (dg_id)
);

/*==============================================================*/
/* Index: Relationship_13_FK                                    */
/*==============================================================*/
create index Relationship_13_FK on tb_deliver_goods (
   address_id ASC
);

/*==============================================================*/
/* Index: Relationship_14_FK                                    */
/*==============================================================*/
create index Relationship_14_FK on tb_deliver_goods (
   wpull_id ASC
);

/*==============================================================*/
/* Index: Relationship_69_FK                                    */
/*==============================================================*/
create index Relationship_69_FK on tb_deliver_goods (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_demand                                             */
/*==============================================================*/
create table tb_demand 
(
   demand_id            INTEGER              not null,
   opp_id               INTEGER,
   demand_theme         VARCHAR2(30),
   record_time          DATE,
   importance           INTEGER,
   demand_content       CLOB,
   is_delete            INTEGER,
   constraint PK_TB_DEMAND primary key (demand_id)
);

/*==============================================================*/
/* Index: Relationship_49_FK                                    */
/*==============================================================*/
create index Relationship_49_FK on tb_demand (
   opp_id ASC
);

/*==============================================================*/
/* Table: tb_dept                                               */
/*==============================================================*/
create table tb_dept 
(
   dept_id              NUMBER(10)           not null,
   dept_name            VARCHAR2(20),
   dept_is_del          INTEGER,
   constraint PK_TB_DEPT primary key (dept_id)
);

/*==============================================================*/
/* Table: tb_dimission                                          */
/*==============================================================*/
create table tb_dimission 
(
   dimission_id         NUMBER(10)           not null,
   dimi_type_id         NUMBER(10),
   dimission_name       VARCHAR2(20),
   dimission_is_del     INTEGER,
   constraint PK_TB_DIMISSION primary key (dimission_id)
);

/*==============================================================*/
/* Index: dimi_type_FK                                          */
/*==============================================================*/
create index dimi_type_FK on tb_dimission (
   dimi_type_id ASC
);

/*==============================================================*/
/* Table: tb_dimission_type                                     */
/*==============================================================*/
create table tb_dimission_type 
(
   dimi_type_id         NUMBER(10)           not null,
   dimi_type_name       VARCHAR2(50),
   constraint PK_TB_DIMISSION_TYPE primary key (dimi_type_id)
);

/*==============================================================*/
/* Table: tb_fc_type                                            */
/*==============================================================*/
create table tb_fc_type 
(
   fc_type_id           NUMBER(10)           not null,
   fc_type_name         VARCHAR2(20),
   constraint PK_TB_FC_TYPE primary key (fc_type_id)
);

/*==============================================================*/
/* Table: tb_functions                                          */
/*==============================================================*/
create table tb_functions 
(
   fc_id                NUMBER(10)           not null,
   fc_type_id           NUMBER(10),
   fc_name              VARCHAR2(50),
   constraint PK_TB_FUNCTIONS primary key (fc_id)
);

/*==============================================================*/
/* Index: fc_fc_type_FK                                         */
/*==============================================================*/
create index fc_fc_type_FK on tb_functions (
   fc_type_id ASC
);

/*==============================================================*/
/* Table: tb_log                                                */
/*==============================================================*/
create table tb_log 
(
   log_id               INTEGER              not null,
   user_id              INTEGER,
   log_login_ip         VARCHAR2(30),
   log_option_content   VARCHAR2(50),
   log_option_time      DATE,
   log_option_type      VARCHAR2(20),
   log_remark           VARCHAR2(50),
   constraint PK_TB_LOG primary key (log_id)
);

/*==============================================================*/
/* Index: user_log_FK                                           */
/*==============================================================*/
create index user_log_FK on tb_log (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_log_data                                           */
/*==============================================================*/
create table tb_log_data 
(
   log_data_id          NUMBER(10)           not null,
   user_id              INTEGER,
   log_data_table       VARCHAR2(30),
   log_data_old         VARCHAR2(500),
   log_data_new         VARCHAR2(500),
   log_data_time        DATE,
   constraint PK_TB_LOG_DATA primary key (log_data_id)
);

/*==============================================================*/
/* Index: user_log_data_FK                                      */
/*==============================================================*/
create index user_log_data_FK on tb_log_data (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_maintain_order                                     */
/*==============================================================*/
create table tb_maintain_order 
(
   mt_id                NUMBER(10)           not null,
   cus_id               NUMBER(10),
   user_id              INTEGER,
   mt__theme            VARCHAR2(20),
   mt_date              DATE,
   mt_complete_date     DATE,
   mt_schedule          VARCHAR2(20),
   mt_pay               NUMBER(10,2),
   mt_state             VARCHAR2(20),
   mt_accessories       VARCHAR2(100),
   magic_delete         INTEGER,
   mt_is_protect        INTEGER,
   mt_descript          VARCHAR2(100),
   mt_pro_date          DATE,
   mt_extract           VARCHAR2(100),
   mt_tel               VARCHAR2(20),
   mt_name              VARCHAR2(20),
   constraint PK_TB_MAINTAIN_ORDER primary key (mt_id)
);

/*==============================================================*/
/* Index: Relationship_75_FK                                    */
/*==============================================================*/
create index Relationship_75_FK on tb_maintain_order (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_114_FK                                   */
/*==============================================================*/
create index Relationship_114_FK on tb_maintain_order (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_memorial_day                                       */
/*==============================================================*/
create table tb_memorial_day 
(
   med_id               NUMBER(5)            not null,
   cus_id               NUMBER(10),
   cots_id              NUMBER(10),
   med_type             VARCHAR2(20),
   med_memorial_date    DATE,
   med_next_point       DATE,
   med_createtimr       DATE,
   constraint PK_TB_MEMORIAL_DAY primary key (med_id)
);

/*==============================================================*/
/* Index: Relationship_58_FK                                    */
/*==============================================================*/
create index Relationship_58_FK on tb_memorial_day (
   cots_id ASC
);

/*==============================================================*/
/* Index: Relationship_59_FK                                    */
/*==============================================================*/
create index Relationship_59_FK on tb_memorial_day (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_message                                            */
/*==============================================================*/
create table tb_message 
(
   message_id           NUMBER(10)           not null,
   role_id              NUMBER(10),
   message_type         VARCHAR2(20),
   message_content      VARCHAR2(100),
   message_time         DATE,
   constraint PK_TB_MESSAGE primary key (message_id)
);

/*==============================================================*/
/* Index: Relationship_119_FK                                   */
/*==============================================================*/
create index Relationship_119_FK on tb_message (
   role_id ASC
);

/*==============================================================*/
/* Table: tb_order                                              */
/*==============================================================*/
create table tb_order 
(
   order_id             NUMBER(10)           not null,
   user_id              INTEGER,
   address_id           NUMBER(10),
   cus_id               NUMBER(10),
   tb__user_id          INTEGER,
   quote_id             INTEGER,
   order_number         VARCHAR2(20),
   order_category       VARCHAR2(20),
   order_pay_way        VARCHAR2(20),
   order_total          NUMBER(8,2),
   order_date           DATE,
   order_latest_date    DATE,
   order_status         NUMBER(1),
   "order)type"         NUMBER(1),
   order_other          VARCHAR2(100),
   order_enclosure      VARCHAR2(200),
   login_delete         NUMBER(1),
   order_ok             NUMBER(1),
   constraint PK_TB_ORDER primary key (order_id)
);

/*==============================================================*/
/* Index: Relationship_15_FK                                    */
/*==============================================================*/
create index Relationship_15_FK on tb_order (
   address_id ASC
);

/*==============================================================*/
/* Index: Relationship_54_FK                                    */
/*==============================================================*/
create index Relationship_54_FK on tb_order (
   quote_id ASC
);

/*==============================================================*/
/* Index: Relationship_70_FK                                    */
/*==============================================================*/
create index Relationship_70_FK on tb_order (
   tb__user_id ASC
);

/*==============================================================*/
/* Index: Relationship_71_FK                                    */
/*==============================================================*/
create index Relationship_71_FK on tb_order (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_109_FK                                   */
/*==============================================================*/
create index Relationship_109_FK on tb_order (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_order_detail                                       */
/*==============================================================*/
create table tb_order_detail 
(
   od_id                NUMBER(10)           not null,
   order_id             NUMBER(10),
   od_number            NUMBER(6),
   od_price             NUMBER(6,2),
   od_discount          NUMBER(2,2),
   od_other             VARCHAR2(100),
   constraint PK_TB_ORDER_DETAIL primary key (od_id)
);

/*==============================================================*/
/* Index: Relationship_16_FK                                    */
/*==============================================================*/
create index Relationship_16_FK on tb_order_detail (
   order_id ASC
);

/*==============================================================*/
/* Table: tb_pay_back_detail                                    */
/*==============================================================*/
create table tb_pay_back_detail 
(
   pbd_id               NUMBER(10)           not null,
   order_id             NUMBER(10),
   mt_id                NUMBER(10),
   user_id              INTEGER,
   ppb_id               NUMBER(10),
   rr_id                NUMBER(10),
   pbd_money            NUMBER(10,2),
   pbd_terms            INTEGER,
   pbd_date             DATE,
   magic_delete         INTEGER,
   pbd_type             NUMBER(1),
   constraint PK_TB_PAY_BACK_DETAIL primary key (pbd_id)
);

/*==============================================================*/
/* Index: Relationship_22_FK                                    */
/*==============================================================*/
create index Relationship_22_FK on tb_pay_back_detail (
   ppb_id ASC
);

/*==============================================================*/
/* Index: Relationship_27_FK                                    */
/*==============================================================*/
create index Relationship_27_FK on tb_pay_back_detail (
   rr_id ASC
);

/*==============================================================*/
/* Index: Relationship_43_FK                                    */
/*==============================================================*/
create index Relationship_43_FK on tb_pay_back_detail (
   order_id ASC
);

/*==============================================================*/
/* Index: Relationship_78_FK                                    */
/*==============================================================*/
create index Relationship_78_FK on tb_pay_back_detail (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_payment_records                                    */
/*==============================================================*/
create table tb_payment_records 
(
   user_id              INTEGER,
   cus_id               NUMBER(10),
   ppd_id               NUMBER(10),
   pr_money             NUMBER(10,2),
   pr_terms             INTEGER,
   pr_date              DATE,
   pr_is_ticket         VARCHAR2(20),
   magic_delete         INTEGER,
   pr_id                NUMBER(10)           not null,
   constraint PK_TB_PAYMENT_RECORDS primary key (pr_id)
);

/*==============================================================*/
/* Index: Relationship_23_FK                                    */
/*==============================================================*/
create index Relationship_23_FK on tb_payment_records (
   ppd_id ASC
);

/*==============================================================*/
/* Index: Relationship_76_FK                                    */
/*==============================================================*/
create index Relationship_76_FK on tb_payment_records (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_116_FK                                   */
/*==============================================================*/
create index Relationship_116_FK on tb_payment_records (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_plan_pay_back                                      */
/*==============================================================*/
create table tb_plan_pay_back 
(
   ppb_id               NUMBER(10)           not null,
   user_id              INTEGER,
   cus_id               NUMBER(10),
   mt_id                NUMBER(10),
   order_id             NUMBER(10),
   ppb_money            NUMBER(10,2),
   ppb_date             DATE,
   ppb_terms            INTEGER,
   magic_delete         INTEGER,
   ppb_type             INTEGER,
   constraint PK_TB_PLAN_PAY_BACK primary key (ppb_id)
);

/*==============================================================*/
/* Index: Relationship_45_FK                                    */
/*==============================================================*/
create index Relationship_45_FK on tb_plan_pay_back (
   mt_id ASC
);

/*==============================================================*/
/* Index: Relationship_46_FK                                    */
/*==============================================================*/
create index Relationship_46_FK on tb_plan_pay_back (
   order_id ASC
);

/*==============================================================*/
/* Index: Relationship_77_FK                                    */
/*==============================================================*/
create index Relationship_77_FK on tb_plan_pay_back (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_117_FK                                   */
/*==============================================================*/
create index Relationship_117_FK on tb_plan_pay_back (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_plan_pay_detail                                    */
/*==============================================================*/
create table tb_plan_pay_detail 
(
   ppd_id               NUMBER(10)           not null,
   user_id              INTEGER,
   cus_id               NUMBER(10),
   ppd_money            NUMBER(10,2),
   ppd_date             DATE,
   ppd_terms            INTEGER,
   ppd_state            VARCHAR2(20),
   magic_delete         INTEGER,
   constraint PK_TB_PLAN_PAY_DETAIL primary key (ppd_id)
);

/*==============================================================*/
/* Index: Relationship_74_FK                                    */
/*==============================================================*/
create index Relationship_74_FK on tb_plan_pay_detail (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_113_FK                                   */
/*==============================================================*/
create index Relationship_113_FK on tb_plan_pay_detail (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_product                                            */
/*==============================================================*/
create table tb_product 
(
   product_id           NUMBER(10)           not null,
   pc_id                NUMBER(6),
   product_name         VARCHAR2(30),
   product_stock        NUMBER(1),
   product_model        VARCHAR2(30),
   logic_delete         NUMBER(1),
   constraint PK_TB_PRODUCT primary key (product_id)
);

/*==============================================================*/
/* Index: Relationship_1_FK                                     */
/*==============================================================*/
create index Relationship_1_FK on tb_product (
   pc_id ASC
);

/*==============================================================*/
/* Table: tb_product_category                                   */
/*==============================================================*/
create table tb_product_category 
(
   pc_id                NUMBER(6)            not null,
   tb__pc_id            NUMBER(6),
   pc_name              VARCHAR2(20),
   logic_delete         NUMBER(1),
   constraint PK_TB_PRODUCT_CATEGORY primary key (pc_id)
);

/*==============================================================*/
/* Index: Relationship_2_FK                                     */
/*==============================================================*/
create index Relationship_2_FK on tb_product_category (
   tb__pc_id ASC
);

/*==============================================================*/
/* Table: tb_product_format                                     */
/*==============================================================*/
create table tb_product_format 
(
   pf_id                NUMBER(10)           not null,
   product_id           NUMBER(10),
   pf_name              VARCHAR2(30),
   pf_unit_cal          NUMBER(4),
   pf_price             NUMBER(8,2),
   pf_cost              NUMBER(8,2),
   pf_status            NUMBER(1),
   pf_code              VARCHAR2(30),
   pf_batch             NUMBER(3),
   pf_date              DATE,
   pf_expiry            DATE,
   pf_manufacturer      VARCHAR2(40),
   pf_approval_number   VARCHAR2(30),
   pf_weight            NUMBER(5,2),
   pf_imf               VARCHAR2(40),
   pf_other             CLOB,
   logic_delete         NUMBER(1),
   pf_min               NUMBER(6),
   pf_max               NUMBER(6),
   pf_unit              VARCHAR2(10),
   pf_weigt_unit        VARCHAR2(6),
   constraint PK_TB_PRODUCT_FORMAT primary key (pf_id)
);

/*==============================================================*/
/* Index: Relationship_3_FK                                     */
/*==============================================================*/
create index Relationship_3_FK on tb_product_format (
   product_id ASC
);

/*==============================================================*/
/* Table: tb_proj_cus                                           */
/*==============================================================*/
create table tb_proj_cus 
(
   proj_id              NUMBER(10)           not null,
   cus_id               NUMBER(10)           not null,
   constraint PK_TB_PROJ_CUS primary key (proj_id, cus_id)
);

comment on table tb_proj_cus is
'项目对应多个联系人，客户，用户
一个联系人，客户，用户对应多个项目

为项目协作单位';

/*==============================================================*/
/* Index: Relationship_94_FK                                    */
/*==============================================================*/
create index Relationship_94_FK on tb_proj_cus (
   proj_id ASC
);

/*==============================================================*/
/* Index: Relationship_95_FK                                    */
/*==============================================================*/
create index Relationship_95_FK on tb_proj_cus (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_proj_mid                                           */
/*==============================================================*/
create table tb_proj_mid 
(
   proj_id              NUMBER(10)           not null,
   cots_id              NUMBER(10)           not null,
   constraint PK_TB_PROJ_MID primary key (proj_id, cots_id)
);

comment on table tb_proj_mid is
'项目对应多个联系人，客户，用户
一个联系人，客户，用户对应多个项目

为项目成员';

/*==============================================================*/
/* Index: Relationship_92_FK                                    */
/*==============================================================*/
create index Relationship_92_FK on tb_proj_mid (
   proj_id ASC
);

/*==============================================================*/
/* Index: Relationship_93_FK                                    */
/*==============================================================*/
create index Relationship_93_FK on tb_proj_mid (
   cots_id ASC
);

/*==============================================================*/
/* Table: tb_project                                            */
/*==============================================================*/
create table tb_project 
(
   proj_id              NUMBER(10)           not null,
   user_id              INTEGER,
   pexp_id              NUMBER(10),
   proj_theme           VARCHAR2(20),
   proj_state           VARCHAR2(20),
   proj_stage           VARCHAR2(10),
   proj_presale         VARCHAR2(20),
   proj_entry_date      DATE,
   proj_outline         VARCHAR2(300),
   proj_customers       VARCHAR2(100),
   constraint PK_TB_PROJECT primary key (proj_id)
);

comment on table tb_project is
'负责人（用户id），外键 ';

/*==============================================================*/
/* Index: Relationship_91_FK                                    */
/*==============================================================*/
create index Relationship_91_FK on tb_project (
   pexp_id ASC
);

/*==============================================================*/
/* Index: Relationship_97_FK                                    */
/*==============================================================*/
create index Relationship_97_FK on tb_project (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_pur_detail                                         */
/*==============================================================*/
create table tb_pur_detail 
(
   pd_id                NUMBER(10)           not null,
   pur_id               NUMBER(10),
   pd_num               INTEGER,
   pd_money             NUMBER(10,2),
   magic_delete         INTEGER,
   pd_classify          VARCHAR2(100),
   pd_price             NUMBER(10,2),
   constraint PK_TB_PUR_DETAIL primary key (pd_id)
);

/*==============================================================*/
/* Index: Relationship_24_FK                                    */
/*==============================================================*/
create index Relationship_24_FK on tb_pur_detail (
   pur_id ASC
);

/*==============================================================*/
/* Table: tb_purchase                                           */
/*==============================================================*/
create table tb_purchase 
(
   pur_id               NUMBER(10)           not null,
   user_id              INTEGER,
   cus_id               NUMBER(10),
   wh_id                NUMBER(4),
   pur_theme            VARCHAR2(20),
   pur_odd_numbers      VARCHAR2(20),
   pur_date             DATE,
   pur_state            VARCHAR2(20),
   pur_projected_date   DATE,
   pur_remarks          VARCHAR2(100),
   magic_delete         INTEGER,
   pur_accessories      VARCHAR2(100),
   pur_ok               NUMBER(1),
   constraint PK_TB_PURCHASE primary key (pur_id)
);

/*==============================================================*/
/* Index: Relationship_41_FK                                    */
/*==============================================================*/
create index Relationship_41_FK on tb_purchase (
   wh_id ASC
);

/*==============================================================*/
/* Index: Relationship_73_FK                                    */
/*==============================================================*/
create index Relationship_73_FK on tb_purchase (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_112_FK                                   */
/*==============================================================*/
create index Relationship_112_FK on tb_purchase (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_purchase_receipt                                   */
/*==============================================================*/
create table tb_purchase_receipt 
(
   purr_id              NUMBER(10)           not null,
   user_id              INTEGER,
   pr_id                NUMBER(10),
   ppb_id               NUMBER(10),
   cus_id               NUMBER(10),
   pur_id               NUMBER(10),
   purr_type            VARCHAR2(20),
   purr_is_pay          VARCHAR2(20),
   magic_delete         INTEGER,
   constraint PK_TB_PURCHASE_RECEIPT primary key (purr_id)
);

/*==============================================================*/
/* Index: Relationship_25_FK                                    */
/*==============================================================*/
create index Relationship_25_FK on tb_purchase_receipt (
   pur_id ASC
);

/*==============================================================*/
/* Index: Relationship_33_FK                                    */
/*==============================================================*/
create index Relationship_33_FK on tb_purchase_receipt (
   pr_id ASC
);

/*==============================================================*/
/* Index: Relationship_35_FK                                    */
/*==============================================================*/
create index Relationship_35_FK on tb_purchase_receipt (
   ppb_id ASC
);

/*==============================================================*/
/* Index: Relationship_72_FK                                    */
/*==============================================================*/
create index Relationship_72_FK on tb_purchase_receipt (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_111_FK                                   */
/*==============================================================*/
create index Relationship_111_FK on tb_purchase_receipt (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_quote                                              */
/*==============================================================*/
create table tb_quote 
(
   quote_id             INTEGER              not null,
   cus_id               NUMBER(10),
   opp_id               INTEGER,
   user_id              INTEGER,
   接收人id                NUMBER(10),
   quote_theme          VARCHAR2(30),
   quote_number         VARCHAR2(30),
   classification       VARCHAR2(30),
   quote_time           DATE,
   total_quote          NUMBER(12,2),
   quotation_contact    VARCHAR2(30),
   remarks              VARCHAR2(100),
   annex                VARCHAR2(50),
   is_delete            INTEGER,
   cus_ok               NUMBER(1),
   constraint PK_TB_QUOTE primary key (quote_id)
);

/*==============================================================*/
/* Index: Relationship_52_FK                                    */
/*==============================================================*/
create index Relationship_52_FK on tb_quote (
   opp_id ASC
);

/*==============================================================*/
/* Index: Relationship_83_FK                                    */
/*==============================================================*/
create index Relationship_83_FK on tb_quote (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_107_FK                                   */
/*==============================================================*/
create index Relationship_107_FK on tb_quote (
   接收人id ASC
);

/*==============================================================*/
/* Index: Relationship_108_FK                                   */
/*==============================================================*/
create index Relationship_108_FK on tb_quote (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_quote_detail                                       */
/*==============================================================*/
create table tb_quote_detail 
(
   qd_id                INTEGER              not null,
   pf_id                NUMBER(10),
   quote_id             INTEGER,
   unit_price           NUMBER(12,2),
   amount               NUMBER(12,2),
   money                NUMBER(12,2),
   remarks              VARCHAR2(100),
   is_delete            INTEGER,
   constraint PK_TB_QUOTE_DETAIL primary key (qd_id)
);

/*==============================================================*/
/* Index: Relationship_53_FK                                    */
/*==============================================================*/
create index Relationship_53_FK on tb_quote_detail (
   quote_id ASC
);

/*==============================================================*/
/* Index: Relationship_55_FK                                    */
/*==============================================================*/
create index Relationship_55_FK on tb_quote_detail (
   pf_id ASC
);

/*==============================================================*/
/* Table: tb_receipt_records                                    */
/*==============================================================*/
create table tb_receipt_records 
(
   rr_id                NUMBER(10)           not null,
   cus_id               NUMBER(10),
   user_id              INTEGER,
   order_id             NUMBER(10),
   rr_content           VARCHAR2(100),
   rr_type              VARCHAR2(20),
   rr_monry             NUMBER(10,2),
   rr_number            NUMBER(20),
   rr_date              DATE,
   rr_delete            INTEGER,
   constraint PK_TB_RECEIPT_RECORDS primary key (rr_id)
);

/*==============================================================*/
/* Index: Relationship_44_FK                                    */
/*==============================================================*/
create index Relationship_44_FK on tb_receipt_records (
   order_id ASC
);

/*==============================================================*/
/* Index: Relationship_79_FK                                    */
/*==============================================================*/
create index Relationship_79_FK on tb_receipt_records (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_118_FK                                   */
/*==============================================================*/
create index Relationship_118_FK on tb_receipt_records (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_return_goods                                       */
/*==============================================================*/
create table tb_return_goods 
(
   rg_id                NUMBER(10)           not null,
   cus_id               NUMBER(10),
   order_id             NUMBER(10),
   pur_id               NUMBER(10),
   rg_odd_numbers       VARCHAR2(20),
   rg_return_time       DATE,
   rg_state             VARCHAR2(20),
   rg_accessories       VARCHAR2(100),
   magic_delete         INTEGER,
   rg_type              INTEGER,
   constraint PK_TB_RETURN_GOODS primary key (rg_id)
);

/*==============================================================*/
/* Index: Relationship_31_FK                                    */
/*==============================================================*/
create index Relationship_31_FK on tb_return_goods (
   pur_id ASC
);

/*==============================================================*/
/* Index: Relationship_40_FK                                    */
/*==============================================================*/
create index Relationship_40_FK on tb_return_goods (
   order_id ASC
);

/*==============================================================*/
/* Index: Relationship_115_FK                                   */
/*==============================================================*/
create index Relationship_115_FK on tb_return_goods (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_return_goods_detail                                */
/*==============================================================*/
create table tb_return_goods_detail 
(
   rgd_id               NUMBER(10)           not null,
   rg_id                NUMBER(10),
   pf_id                NUMBER(10),
   rgd_reason           VARCHAR2(100),
   rgd_num              NUMBER,
   rgd_remarks          VARCHAR2(100),
   magic_delete         INTEGER,
   constraint PK_TB_RETURN_GOODS_DETAIL primary key (rgd_id)
);

/*==============================================================*/
/* Index: Relationship_21_FK                                    */
/*==============================================================*/
create index Relationship_21_FK on tb_return_goods_detail (
   rg_id ASC
);

/*==============================================================*/
/* Index: Relationship_42_FK                                    */
/*==============================================================*/
create index Relationship_42_FK on tb_return_goods_detail (
   pf_id ASC
);

/*==============================================================*/
/* Table: tb_role                                               */
/*==============================================================*/
create table tb_role 
(
   role_id              NUMBER(10)           not null,
   role_name            VARCHAR2(20),
   role_is_del          INTEGER,
   constraint PK_TB_ROLE primary key (role_id)
);

/*==============================================================*/
/* Table: tb_sale_opp                                           */
/*==============================================================*/
create table tb_sale_opp 
(
   opp_id               INTEGER              not null,
   cus_id               NUMBER(10),
   user_id              INTEGER,
   联系人id                NUMBER(10),
   opp_theme            VARCHAR2(30),
   update_time          DATE,
   opp_status           INTEGER,
   classification       VARCHAR2(30),
   discovery_time       DATE,
   opp_source           VARCHAR2(20),
   customer_demand      VARCHAR2(100),
   expected_time        DATE,
   possibility          VARCHAR2(10),
   intend_product       VARCHAR2(50),
   expected_amount      NUMBER(20,2),
   priority             NUMBER(1),
   stage                VARCHAR2(10),
   stage_remarks        CLOB,
   track_log            CLOB,
   is_delete            INTEGER,
   constraint PK_TB_SALE_OPP primary key (opp_id)
);

/*==============================================================*/
/* Index: Relationship_82_FK                                    */
/*==============================================================*/
create index Relationship_82_FK on tb_sale_opp (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_105_FK                                   */
/*==============================================================*/
create index Relationship_105_FK on tb_sale_opp (
   联系人id ASC
);

/*==============================================================*/
/* Index: Relationship_106_FK                                   */
/*==============================================================*/
create index Relationship_106_FK on tb_sale_opp (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_serve                                              */
/*==============================================================*/
create table tb_serve 
(
   serve_id             NUMBER(10)           not null,
   cus_id               NUMBER(10),
   user_id              INTEGER,
   serve_theme          VARCHAR2(100),
   serve_type           VARCHAR2(30),
   serve_way            VARCHAR2(30),
   serve_start_time     DATE,
   serve_time_spent     DATE,
   serve_state          VARCHAR2(10),
   serve_content        VARCHAR2(500),
   serve_back           VARCHAR2(500),
   serve_remarks        VARCHAR2(1000),
   constraint PK_TB_SERVE primary key (serve_id)
);

comment on table tb_serve is
'执行用户（用户id）外键';

/*==============================================================*/
/* Index: Relationship_60_FK                                    */
/*==============================================================*/
create index Relationship_60_FK on tb_serve (
   cus_id ASC
);

/*==============================================================*/
/* Index: Relationship_99_FK                                    */
/*==============================================================*/
create index Relationship_99_FK on tb_serve (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_solution                                           */
/*==============================================================*/
create table tb_solution 
(
   solution_id          INTEGER              not null,
   opp_id               INTEGER,
   solution_theme       VARCHAR2(30),
   submit_time          DATE,
   solution_content     CLOB,
   customer_feedback    CLOB,
   annex                VARCHAR2(50),
   is_delete            INTEGER,
   constraint PK_TB_SOLUTION primary key (solution_id)
);

/*==============================================================*/
/* Index: Relationship_50_FK                                    */
/*==============================================================*/
create index Relationship_50_FK on tb_solution (
   opp_id ASC
);

/*==============================================================*/
/* Table: tb_task_action                                        */
/*==============================================================*/
create table tb_task_action 
(
   task_id              INTEGER              not null,
   user_id              INTEGER,
   联系人id                NUMBER(10),
   cus_id               NUMBER(10),
   opp_id               INTEGER,
   deadline             DATE,
   priority             NUMBER(1),
   create_time          DATE,
   is_delete            INTEGER,
   action_description   CLOB,
   task_status          INTEGER,
   record_type          VARCHAR2(20),
   is_task              INTEGER,
   constraint PK_TB_TASK_ACTION primary key (task_id)
);

/*==============================================================*/
/* Index: Relationship_47_FK                                    */
/*==============================================================*/
create index Relationship_47_FK on tb_task_action (
   opp_id ASC
);

/*==============================================================*/
/* Index: Relationship_80_FK                                    */
/*==============================================================*/
create index Relationship_80_FK on tb_task_action (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_102_FK                                   */
/*==============================================================*/
create index Relationship_102_FK on tb_task_action (
   联系人id ASC
);

/*==============================================================*/
/* Index: Relationship_103_FK                                   */
/*==============================================================*/
create index Relationship_103_FK on tb_task_action (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_travel                                             */
/*==============================================================*/
create table tb_travel 
(
   travel_id            INTEGER              not null,
   cus_id               NUMBER(10),
   user_id              INTEGER,
   opp_id               INTEGER,
   travel_theme         VARCHAR2(30),
   start_time           DATE,
   end_time             DATE,
   destination          VARCHAR2(30),
   peer_personnel       VARCHAR2(30),
   transportation       VARCHAR2(30),
   status               INTEGER,
   remarks              VARCHAR2(100),
   is_delete            INTEGER,
   constraint PK_TB_TRAVEL primary key (travel_id)
);

/*==============================================================*/
/* Index: Relationship_48_FK                                    */
/*==============================================================*/
create index Relationship_48_FK on tb_travel (
   opp_id ASC
);

/*==============================================================*/
/* Index: Relationship_81_FK                                    */
/*==============================================================*/
create index Relationship_81_FK on tb_travel (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_104_FK                                   */
/*==============================================================*/
create index Relationship_104_FK on tb_travel (
   cus_id ASC
);

/*==============================================================*/
/* Table: tb_user                                               */
/*==============================================================*/
create table tb_user 
(
   user_id              INTEGER              not null,
   dept_id              NUMBER(10),
   user_name            VARCHAR2(20),
   user_password        VARCHAR2(20),
   user_sex             VARCHAR2(2),
   user_birthday        DATE,
   user_address         VARCHAR2(60),
   hiredate             DATE,
   user_tel             VARCHAR2(11),
   user_email           VARCHAR2(50),
   user_photo           VARCHAR2(100),
   user_is_dimission    INTEGER,
   openid               VARCHAR2(200),
   constraint PK_TB_USER primary key (user_id)
);

/*==============================================================*/
/* Index: user_dept_FK                                          */
/*==============================================================*/
create index user_dept_FK on tb_user (
   dept_id ASC
);

/*==============================================================*/
/* Table: tb_warehouse                                          */
/*==============================================================*/
create table tb_warehouse 
(
   wh_id                NUMBER(4)            not null,
   wh_name              VARCHAR2(20),
   logic_delete         NUMBER(1),
   constraint PK_TB_WAREHOUSE primary key (wh_id)
);

/*==============================================================*/
/* Table: tb_wh_check                                           */
/*==============================================================*/
create table tb_wh_check 
(
   wc_id                NUMBER(8)            not null,
   user_id              INTEGER,
   wh_id                NUMBER(4),
   wc_date              DATE,
   wc_status            NUMBER(1),
   wc_other             VARCHAR2(100),
   logic_delete         NUMBER(1),
   constraint PK_TB_WH_CHECK primary key (wc_id)
);

/*==============================================================*/
/* Index: Relationship_6_FK                                     */
/*==============================================================*/
create index Relationship_6_FK on tb_wh_check (
   wh_id ASC
);

/*==============================================================*/
/* Index: Relationship_66_FK                                    */
/*==============================================================*/
create index Relationship_66_FK on tb_wh_check (
   user_id ASC
);

/*==============================================================*/
/* Table: tb_wh_number                                          */
/*==============================================================*/
create table tb_wh_number 
(
   wn_id                NUMBER(10)           not null,
   pf_id                NUMBER(10),
   wh_id                NUMBER(4),
   wn_number            NUMBER(6),
   constraint PK_TB_WH_NUMBER primary key (wn_id)
);

/*==============================================================*/
/* Index: Relationship_4_FK                                     */
/*==============================================================*/
create index Relationship_4_FK on tb_wh_number (
   wh_id ASC
);

/*==============================================================*/
/* Index: Relationship_5_FK                                     */
/*==============================================================*/
create index Relationship_5_FK on tb_wh_number (
   pf_id ASC
);

/*==============================================================*/
/* Table: tb_wh_pull                                            */
/*==============================================================*/
create table tb_wh_pull 
(
   wpull_id             NUMBER(10)           not null,
   order_id             NUMBER(10),
   user_id              INTEGER,
   tb__user_id          INTEGER,
   wh_id                NUMBER(4),
   rg_id                NUMBER(10),
   wpull_name           VARCHAR2(30),
   wpull_date           DATE,
   wpull_status         NUMBER(1),
   wpull_execute        DATE,
   wpull_other          VARCHAR2(100),
   logic_delete         NUMBER(1),
   constraint PK_TB_WH_PULL primary key (wpull_id)
);

/*==============================================================*/
/* Index: Relationship_9_FK                                     */
/*==============================================================*/
create index Relationship_9_FK on tb_wh_pull (
   wh_id ASC
);

/*==============================================================*/
/* Index: Relationship_10_FK                                    */
/*==============================================================*/
create index Relationship_10_FK on tb_wh_pull (
   order_id ASC
);

/*==============================================================*/
/* Index: Relationship_64_FK                                    */
/*==============================================================*/
create index Relationship_64_FK on tb_wh_pull (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_65_FK                                    */
/*==============================================================*/
create index Relationship_65_FK on tb_wh_pull (
   tb__user_id ASC
);

/*==============================================================*/
/* Table: tb_wh_put                                             */
/*==============================================================*/
create table tb_wh_put 
(
   wp_id                NUMBER(10)           not null,
   wh_id                NUMBER(4),
   user_id              INTEGER,
   rg_id                NUMBER(10),
   tb__user_id          INTEGER,
   pur_id               NUMBER(10),
   wp_name              VARCHAR2(30),
   wp_date              DATE,
   wp_status            NUMBER(1),
   wp_execute           DATE,
   wp_other             VARCHAR2(100),
   logic_delete         NUMBER(1),
   constraint PK_TB_WH_PUT primary key (wp_id)
);

/*==============================================================*/
/* Index: Relationship_11_FK                                    */
/*==============================================================*/
create index Relationship_11_FK on tb_wh_put (
   wh_id ASC
);

/*==============================================================*/
/* Index: Relationship_36_FK                                    */
/*==============================================================*/
create index Relationship_36_FK on tb_wh_put (
   pur_id ASC
);

/*==============================================================*/
/* Index: Relationship_39_FK                                    */
/*==============================================================*/
create index Relationship_39_FK on tb_wh_put (
   rg_id ASC
);

/*==============================================================*/
/* Index: Relationship_61_FK                                    */
/*==============================================================*/
create index Relationship_61_FK on tb_wh_put (
   user_id ASC
);

/*==============================================================*/
/* Index: Relationship_62_FK                                    */
/*==============================================================*/
create index Relationship_62_FK on tb_wh_put (
   tb__user_id ASC
);

/*==============================================================*/
/* Table: tb_wp_detail                                          */
/*==============================================================*/
create table tb_wp_detail 
(
   wd_id                NUMBER(12)           not null,
   pf_id                NUMBER(10),
   wh_id                NUMBER(4),
   user_id              INTEGER,
   cus_id               NUMBER(10),
   wp_id                NUMBER(10),
   wd_number            NUMBER(6),
   wd_execute           DATE,
   wd_state             NUMBER(1),
   wd_type              NUMBER(1),
   wd_other             VARCHAR2(100),
   constraint PK_TB_WP_DETAIL primary key (wd_id)
);

/*==============================================================*/
/* Index: Relationship_12_FK                                    */
/*==============================================================*/
create index Relationship_12_FK on tb_wp_detail (
   wh_id ASC
);

/*==============================================================*/
/* Index: Relationship_20_FK                                    */
/*==============================================================*/
create index Relationship_20_FK on tb_wp_detail (
   pf_id ASC
);

/*==============================================================*/
/* Index: Relationship_63_FK                                    */
/*==============================================================*/
create index Relationship_63_FK on tb_wp_detail (
   user_id ASC
);

alter table dept_announce
   add constraint FK_DEPT_ANN_DEPT_ANNO_TB_DEPT foreign key (dept_id)
      references tb_dept (dept_id);

alter table dept_announce
   add constraint FK_DEPT_ANN_DEPT_ANNO_TB_ANNOU foreign key (announce_id
id)
      references tb_announcement (announce_id
 id);

alter table dept_role
   add constraint FK_DEPT_ROL_DEPT_ROLE_TB_ROLE foreign key (role_id)
      references tb_role (role_id);

alter table dept_role
   add constraint FK_DEPT_ROL_DEPT_ROLE_TB_DEPT foreign key (dept_id)
      references tb_dept (dept_id);

alter table project_expect
   add constraint FK_PROJECT__RELATIONS_TB_PROJE foreign key (proj_id)
      references tb_project (proj_id);

alter table role_fc
   add constraint FK_ROLE_FC_ROLE_FC_TB_FUNCT foreign key (fc_id)
      references tb_functions (fc_id);

alter table role_fc
   add constraint FK_ROLE_FC_ROLE_FC2_TB_ROLE foreign key (role_id)
      references tb_role (role_id);

alter table tb_addr
   add constraint FK_TB_ADDR_CUSTOMER__TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_allocation
   add constraint FK_TB_ALLOC_RELATIONS_TB_USER2 foreign key (tb__user_id)
      references tb_user (user_id);

alter table tb_allocation
   add constraint FK_TB_ALLOC_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_allocation
   add constraint FK_TB_ALLOC_RELATIONS_TB_WARE2 foreign key (wh_id)
      references tb_warehouse (wh_id);

alter table tb_allocation
   add constraint FK_TB_ALLOC_RELATIONS_TB_WAREH foreign key (tb__wh_id)
      references tb_warehouse (wh_id);

alter table tb_approval
   add constraint FK_TB_APPRO_REFERENCE_TB_USER foreign key (approval_receiver_id)
      references tb_user (user_id);

alter table tb_care
   add constraint FK_TB_CARE_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_care
   add constraint FK_TB_CARE_RELATIONS_TB_CONTA foreign key (cots_id)
      references tb_contacts (cots_id);

alter table tb_care
   add constraint FK_TB_CARE_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_competitor
   add constraint FK_TB_COMPE_RELATIONS_TB_SALE_ foreign key (opp_id)
      references tb_sale_opp (opp_id);

alter table tb_complaint
   add constraint FK_TB_COMPL_RELATIONS_TB_CONTA foreign key (cots_id)
      references tb_contacts (cots_id);

alter table tb_complaint
   add constraint FK_TB_COMPL_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_complaint
   add constraint FK_TB_COMPL_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_contacts
   add constraint FK_TB_CONTA_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_contacts
   add constraint FK_TB_CONTA_RELATIONS_TB_SERVE foreign key (serve_id)
      references tb_serve (serve_id);

alter table tb_customer
   add constraint FK_TB_CUSTO_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_customer
   add constraint FK_TB_CUSTO_RELATIONS_TB_SERVE foreign key (serve_id)
      references tb_serve (serve_id);

alter table tb_customer
   add constraint FK_TB_CUSTO_CUSTOMER__TB_ADDR foreign key (addr_id)
      references tb_addr (addr_id);

alter table tb_daily_recode
   add constraint FK_TB_DAILY_USER_DAIL_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_deliver_detail
   add constraint FK_TB_DELIV_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_deliver_detail
   add constraint FK_TB_DELIV_RELATIONS_TB_DELIV foreign key (dg_id)
      references tb_deliver_goods (dg_id);

alter table tb_deliver_detail
   add constraint FK_TB_DELIV_RELATIONS_TB_ORDER foreign key (order_id)
      references tb_order (order_id);

alter table tb_deliver_detail
   add constraint FK_TB_DELIV_RELATIONS_TB_PRODU foreign key (product_id)
      references tb_product (product_id);

alter table tb_deliver_goods
   add constraint FK_TB_DELIV_RELATIONS_TB_ADDRE foreign key (address_id)
      references tb_address (address_id);

alter table tb_deliver_goods
   add constraint FK_TB_DELIV_RELATIONS_TB_WH_PU foreign key (wpull_id)
      references tb_wh_pull (wpull_id);

alter table tb_deliver_goods
   add constraint FK_TB_DELIV_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_demand
   add constraint FK_TB_DEMAN_RELATIONS_TB_SALE_ foreign key (opp_id)
      references tb_sale_opp (opp_id);

alter table tb_dimission
   add constraint FK_TB_DIMIS_DIMI_TYPE_TB_DIMIS foreign key (dimi_type_id)
      references tb_dimission_type (dimi_type_id);

alter table tb_functions
   add constraint FK_TB_FUNCT_FC_FC_TYP_TB_FC_TY foreign key (fc_type_id)
      references tb_fc_type (fc_type_id);

alter table tb_log
   add constraint FK_TB_LOG_USER_LOG_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_log_data
   add constraint FK_TB_LOG_D_USER_LOG__TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_maintain_order
   add constraint FK_TB_MAINT_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_maintain_order
   add constraint FK_TB_MAINT_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_memorial_day
   add constraint FK_TB_MEMOR_RELATIONS_TB_CONTA foreign key (cots_id)
      references tb_contacts (cots_id);

alter table tb_memorial_day
   add constraint FK_TB_MEMOR_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_message
   add constraint FK_TB_MESSA_RELATIONS_TB_ROLE foreign key (role_id)
      references tb_role (role_id);

alter table tb_order
   add constraint FK_TB_ORDER_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_order
   add constraint FK_TB_ORDER_RELATIONS_TB_ADDRE foreign key (address_id)
      references tb_address (address_id);

alter table tb_order
   add constraint FK_TB_ORDER_RELATIONS_TB_QUOTE foreign key (quote_id)
      references tb_quote (quote_id);

alter table tb_order
   add constraint FK_TB_ORDER_RELATIONS_TB_USER2 foreign key (tb__user_id)
      references tb_user (user_id);

alter table tb_order
   add constraint FK_TB_ORDER_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_order_detail
   add constraint FK_TB_ORDER_RELATIONS_TB_ORDER foreign key (order_id)
      references tb_order (order_id);

alter table tb_pay_back_detail
   add constraint FK_TB_PAY_B_RELATIONS_TB_PLAN_ foreign key (ppb_id)
      references tb_plan_pay_back (ppb_id);

alter table tb_pay_back_detail
   add constraint FK_TB_PAY_B_RELATIONS_TB_RECEI foreign key (rr_id)
      references tb_receipt_records (rr_id);

alter table tb_pay_back_detail
   add constraint FK_TB_PAY_B_RELATIONS_TB_MAIN2 foreign key (mt_id)
      references tb_maintain_order (mt_id);

alter table tb_pay_back_detail
   add constraint FK_TB_PAY_B_RELATIONS_TB_ORDER foreign key (order_id)
      references tb_order (order_id);

alter table tb_pay_back_detail
   add constraint FK_TB_PAY_B_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_payment_records
   add constraint FK_TB_PAYME_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_payment_records
   add constraint FK_TB_PAYME_RELATIONS_TB_PLAN_ foreign key (ppd_id)
      references tb_plan_pay_detail (ppd_id);

alter table tb_payment_records
   add constraint FK_TB_PAYME_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_plan_pay_back
   add constraint FK_TB_PLAN__RELATIONS_TB_CUST2 foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_plan_pay_back
   add constraint FK_TB_PLAN__RELATIONS_TB_MAINT foreign key (mt_id)
      references tb_maintain_order (mt_id);

alter table tb_plan_pay_back
   add constraint FK_TB_PLAN__RELATIONS_TB_ORDER foreign key (order_id)
      references tb_order (order_id);

alter table tb_plan_pay_back
   add constraint FK_TB_PLAN__RELATIONS_TB_USER2 foreign key (user_id)
      references tb_user (user_id);

alter table tb_plan_pay_detail
   add constraint FK_TB_PLAN__RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_plan_pay_detail
   add constraint FK_TB_PLAN__RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_product
   add constraint FK_TB_PRODU_RELATIONS_TB_PROD2 foreign key (pc_id)
      references tb_product_category (pc_id);

alter table tb_product_category
   add constraint FK_TB_PRODU_RELATIONS_TB_PRODU foreign key (tb__pc_id)
      references tb_product_category (pc_id);

alter table tb_product_format
   add constraint FK_TB_PRODU_RELATIONS_TB_PROD3 foreign key (product_id)
      references tb_product (product_id);

alter table tb_proj_cus
   add constraint FK_TB_PROJ__RELATIONS_TB_PROJE foreign key (proj_id)
      references tb_project (proj_id);

alter table tb_proj_cus
   add constraint FK_TB_PROJ__RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_proj_mid
   add constraint FK_RELATION_RELATIONS_TB_PROJ2 foreign key (proj_id)
      references tb_project (proj_id);

alter table tb_proj_mid
   add constraint FK_TB_PROJ__RELATIONS_TB_CONTA foreign key (cots_id)
      references tb_contacts (cots_id);

alter table tb_project
   add constraint FK_TB_PROJE_RELATIONS_PROJECT_ foreign key (pexp_id)
      references project_expect (pexp_id);

alter table tb_project
   add constraint FK_TB_PROJE_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_pur_detail
   add constraint FK_TB_PUR_D_RELATIONS_TB_PURCH foreign key (pur_id)
      references tb_purchase (pur_id);

alter table tb_purchase
   add constraint FK_TB_PURCH_RELATIONS_TB_CUST2 foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_purchase
   add constraint FK_TB_PURCH_RELATIONS_TB_WAREH foreign key (wh_id)
      references tb_warehouse (wh_id);

alter table tb_purchase
   add constraint FK_TB_PURCH_RELATIONS_TB_USER2 foreign key (user_id)
      references tb_user (user_id);

alter table tb_purchase_receipt
   add constraint FK_TB_PURCH_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_purchase_receipt
   add constraint FK_TB_PURCH_RELATIONS_TB_PURCH foreign key (pur_id)
      references tb_purchase (pur_id);

alter table tb_purchase_receipt
   add constraint FK_TB_PURCH_RELATIONS_TB_PAY_B foreign key (pr_id)
      references tb_pay_back_detail (pbd_id);

alter table tb_purchase_receipt
   add constraint FK_TB_PURCH_RELATIONS_TB_PLAN_ foreign key (ppb_id)
      references tb_plan_pay_back (ppb_id);

alter table tb_purchase_receipt
   add constraint FK_TB_PURCH_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_quote
   add constraint FK_TB_QUOTE_RELATIONS_TB_CONTA foreign key (接收人id)
      references tb_contacts (cots_id);

alter table tb_quote
   add constraint FK_TB_QUOTE_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_quote
   add constraint FK_TB_QUOTE_RELATIONS_TB_SALE_ foreign key (opp_id)
      references tb_sale_opp (opp_id);

alter table tb_quote
   add constraint FK_TB_QUOTE_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_quote_detail
   add constraint FK_TB_QUOTE_RELATIONS_TB_QUOTE foreign key (quote_id)
      references tb_quote (quote_id);

alter table tb_quote_detail
   add constraint FK_TB_QUOTE_RELATIONS_TB_PRODU foreign key (pf_id)
      references tb_product_format (pf_id);

alter table tb_receipt_records
   add constraint FK_TB_RECEI_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_receipt_records
   add constraint FK_TB_RECEI_RELATIONS_TB_ORDER foreign key (order_id)
      references tb_order (order_id);

alter table tb_receipt_records
   add constraint FK_TB_RECEI_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_return_goods
   add constraint FK_TB_RETUR_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_return_goods
   add constraint FK_TB_RETUR_RELATIONS_TB_PURCH foreign key (pur_id)
      references tb_purchase (pur_id);

alter table tb_return_goods
   add constraint FK_TB_RETUR_RELATIONS_TB_ORDER foreign key (order_id)
      references tb_order (order_id);

alter table tb_return_goods_detail
   add constraint FK_TB_RETUR_RELATIONS_TB_RETUR foreign key (rg_id)
      references tb_return_goods (rg_id);

alter table tb_return_goods_detail
   add constraint FK_TB_RETUR_RELATIONS_TB_PRODU foreign key (pf_id)
      references tb_product_format (pf_id);

alter table tb_sale_opp
   add constraint FK_TB_SALE__RELATIONS_TB_CONTA foreign key (联系人id)
      references tb_contacts (cots_id);

alter table tb_sale_opp
   add constraint FK_TB_SALE__RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_sale_opp
   add constraint FK_TB_SALE__RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_serve
   add constraint FK_TB_SERVE_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_serve
   add constraint FK_TB_SERVE_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_solution
   add constraint FK_TB_SOLUT_RELATIONS_TB_SALE_ foreign key (opp_id)
      references tb_sale_opp (opp_id);

alter table tb_task_action
   add constraint FK_TB_TASK__RELATIONS_TB_CONTA foreign key (联系人id)
      references tb_contacts (cots_id);

alter table tb_task_action
   add constraint FK_TB_TASK__RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_task_action
   add constraint FK_TB_TASK__RELATIONS_TB_SALE_ foreign key (opp_id)
      references tb_sale_opp (opp_id);

alter table tb_task_action
   add constraint FK_TB_TASK__RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_travel
   add constraint FK_TB_TRAVE_RELATIONS_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_travel
   add constraint FK_TB_TRAVE_RELATIONS_TB_SALE_ foreign key (opp_id)
      references tb_sale_opp (opp_id);

alter table tb_travel
   add constraint FK_TB_TRAVE_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_user
   add constraint FK_TB_USER_USER_DEPT_TB_DEPT foreign key (dept_id)
      references tb_dept (dept_id);

alter table tb_wh_check
   add constraint FK_TB_WH_CH_RELATIONS_TB_WAREH foreign key (wh_id)
      references tb_warehouse (wh_id);

alter table tb_wh_check
   add constraint FK_TB_WH_CH_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

alter table tb_wh_number
   add constraint FK_TB_WH_NU_RELATIONS_TB_WAREH foreign key (wh_id)
      references tb_warehouse (wh_id);

alter table tb_wh_number
   add constraint FK_TB_WH_NU_RELATIONS_TB_PRODU foreign key (pf_id)
      references tb_product_format (pf_id);

alter table tb_wh_pull
   add constraint FK_TB_WH_PU_REFERENCE_TB_RETUR foreign key (rg_id)
      references tb_return_goods (rg_id);

alter table tb_wh_pull
   add constraint FK_TB_WH_PU_RELATIONS_TB_ORDER foreign key (order_id)
      references tb_order (order_id);

alter table tb_wh_pull
   add constraint FK_TB_WH_PU_RELATIONS_TB_USER4 foreign key (user_id)
      references tb_user (user_id);

alter table tb_wh_pull
   add constraint FK_TB_WH_PU_RELATIONS_TB_USER foreign key (tb__user_id)
      references tb_user (user_id);

alter table tb_wh_pull
   add constraint FK_TB_WH_PU_RELATIONS_TB_WAREH foreign key (wh_id)
      references tb_warehouse (wh_id);

alter table tb_wh_put
   add constraint FK_TB_WH_PU_RELATIONS_TB_WARE2 foreign key (wh_id)
      references tb_warehouse (wh_id);

alter table tb_wh_put
   add constraint FK_TB_WH_PU_RELATIONS_TB_PURCH foreign key (pur_id)
      references tb_purchase (pur_id);

alter table tb_wh_put
   add constraint FK_TB_WH_PU_RELATIONS_TB_RETUR foreign key (rg_id)
      references tb_return_goods (rg_id);

alter table tb_wh_put
   add constraint FK_TB_WH_PU_RELATIONS_TB_USER2 foreign key (user_id)
      references tb_user (user_id);

alter table tb_wh_put
   add constraint FK_TB_WH_PU_RELATIONS_TB_USER3 foreign key (tb__user_id)
      references tb_user (user_id);

alter table tb_wp_detail
   add constraint FK_TB_WP_DE_REFERENCE_TB_CUSTO foreign key (cus_id)
      references tb_customer (cus_id);

alter table tb_wp_detail
   add constraint FK_TB_WP_DE_RELATIONS_TB_WAREH foreign key (wh_id)
      references tb_warehouse (wh_id);

alter table tb_wp_detail
   add constraint FK_TB_WP_DE_RELATIONS_TB_PRODU foreign key (pf_id)
      references tb_product_format (pf_id);

alter table tb_wp_detail
   add constraint FK_TB_WP_DE_RELATIONS_TB_USER foreign key (user_id)
      references tb_user (user_id);

