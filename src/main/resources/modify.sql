/*==============================================================*/
/*8月2号*/
/*修改产品分类表，添加逻辑删除*/
alter table tb_product_category add (logic_delete number(1) default 0);

ALTER TABLE TB_PRODUCT_FORMAT MODIFY PF_PRICE number(12,2) DEFAULT 0;
ALTER TABLE TB_PRODUCT_FORMAT MODIFY PF_COST number(12,2) DEFAULT 0;


/*8月3号*/
/*更改用户性别字段大小 */
alter table USER_CRM.TB_USER modify (user_sex varchar2(4));
/*更改用户密码字段长度，加密*/
alter table USER_CRM.TB_USER modify (user_password varchar2(32));
/*==============================================================*/
-- 8月3号（杨科敏）
-- 修改销售机会表，添加字段：阶段开启时间（stage_start_time）
ALTER TABLE TB_SALE_OPP ADD stage_start_time date;

/*==============================================================*/



--8月5
ALTER TABLE TB_ORDER ADD contact_id number NULL;
ALTER TABLE TB_ORDER
ADD CONSTRAINT COTS_ID_fk
FOREIGN KEY (contact_id) REFERENCES TB_CONTACTS (COTS_ID);
ALTER TABLE TB_ORDER RENAME COLUMN "order)type" TO order_type;

ALTER TABLE TB_ORDER ADD product_id number(10) NULL;
ALTER TABLE TB_ORDER
ADD CONSTRAINT TB_ORDER_TB_PRODUCT_ID_fk
FOREIGN KEY (project_id) REFERENCES TB_PRODUCT (PRODUCT_ID);

--8月5号
ALTER TABLE TB_ORDER_DETAIL ADD product_id number(10) NULL;
ALTER TABLE TB_ORDER_DETAIL
ADD CONSTRAINT ORDER_DETAIL_PRODUCT_FORMAT_fk
FOREIGN KEY (product_id) REFERENCES TB_PRODUCT_FORMAT (PF_ID);

ALTER TABLE TB_ORDER ADD order_title varchar2(50) NULL;


----8月8
ALTER TABLE TB_PRODUCT_FORMAT MODIFY PF_IMF VARCHAR2(100) DEFAULT NULL;

ALTER TABLE TB_PRODUCT_FORMAT MODIFY PF_BASE DEFAULT 0;

----8.9hgh
ALTER TABLE TB_CONTACTS RENAME COLUMN COTS_MSN(QQ) TO "COTS_MSNQQ";
ALTER TABLE TB_CUSTOMER ADD cots_is_delete number(2) NULL;
ALTER TABLE TB_CUSTOMER ADD cots_is_delete number(2) NULL;
ALTER TABLE TB_CUSTOMER ADD cus_is_delete number(2) NULL;
ALTER TABLE TB_CUSTOMER ADD cus_synopsis varchar2(2000) NULL;
ALTER TABLE TB_CUSTOMER MODIFY CUS_EXIST_DATE varchar2(10) DEFAULT NULL;
ALTER TABLE TB_CUSTOMER ADD cus_area varchar2(100) NULL;
ALTER TABLE TB_CUSTOMER ADD cus_remark varchar2(1000) NULL;
ALTER TABLE TB_CUSTOMER ADD cus_MSNQQ varchar2(20) NULL;

--8.9hgh
ALTER TABLE TB_CUSTOMER ADD cus_sign_date varchar2(15) NULL;
ALTER TABLE TB_CUSTOMER ADD cus_create_date date NULL;


--8.10
ALTER TABLE TB_ORDER_DETAIL ADD od_profit number(12,2) DEFAULT 0 NULL;
ALTER TABLE TB_ORDER_DETAIL MODIFY OD_PRICE NUMBER(12,2) DEFAULT NULL;
ALTER TABLE TB_ORDER_DETAIL RENAME COLUMN OD_DISCOUNT TO OD_total;
ALTER TABLE TB_ORDER_DETAIL MODIFY OD_total NUMBER(12,2) DEFAULT NULL;

drop view order_view;
create  view  order_view as
select o.*,u.USER_NAME,p.USER_NAME as o_user,
       q.QUOTE_THEME,
       pr.PROJ_THEME,
       c.CUS_NAME,
       co.COTS_NAME,a.ADDRESS_NAME,a.ADDRESS_TEL,a.ADDRESS_PROVINCE,a.ADDRESS_CITY,a.ADDRESS_CONTENT,a.ADDRESS_CODE,a.Address_COUNTY,
       (select nvl(sum(d.DD_TOTAL),0) from TB_DELIVER_DETAIL d where d.ORDER_ID = o.ORDER_ID) sendMoney,
       (select nvl(sum(PBD_MONEY),0)  from TB_PAY_BACK_DETAIL p where p.ORDER_ID = o.ORDER_ID)  returnMoney,
       (select nvl(sum(RR_MONEY),0)  from TB_RECEIPT_RECORDS r where r.ORDER_ID = o.ORDER_ID) billMOney,
       (select nvl(sum(od_profit),0)  from TB_ORDER_DETAIL od where od.ORDER_ID = o.ORDER_ID) forecastMaori,
       (select nvl(sum(OD_TOTAL),0)  from TB_ORDER_DETAIL od where od.ORDER_ID = o.ORDER_ID) total,
       (select nvl(sum(PBD_MONEY),0)  from TB_PAY_BACK_DETAIL p where p.ORDER_ID = o.ORDER_ID)  -(select nvl(sum(d.DD_TOTAL),0)-nvl(sum(d.DD_COST),0) from TB_DELIVER_DETAIL d where d.ORDER_ID = o.ORDER_ID) maori
from TB_ORDER o left join TB_USER u on o.USER_ID = u.USER_ID
                left join TB_USER p on o.TB__USER_ID = p.USER_ID
                left join TB_QUOTE q on q.QUOTE_ID = o.QUOTE_ID
                left join tb_project pr on pr.PROJ_ID = o.product_id
                left join TB_CUSTOMER c on  c.CUS_ID = o.CUS_ID
                left join TB_CONTACTS co on co.COTS_ID = o.CONTACT_ID
                left join TB_ADDRESS a on a.ADDRESS_ID = o.ADDRESS_ID
where o.LOGIN_DELETE = 0;

--8月15
ALTER TABLE TB_DELIVER_DETAIL DROP CONSTRAINT FK_TB_DELIV_RELATIONS_TB_PRODU;
ALTER TABLE TB_DELIVER_DETAIL
ADD CONSTRAINT FK_TB_DELIV_RELATIONS_TB_PRODU
FOREIGN KEY (PRODUCT_ID) REFERENCES TB_PRODUCT_FORMAT (PF_ID);

--8月16
drop view out_storage_view;
create view out_storage_view as
select wp.*,u.USER_NAME userName,e.USER_NAME executName,
o.ORDER_TITLE orderName, w.WH_NAME wareHouseName, r.RG_THEME rgName,
c.CUS_ID cusId,c.CUS_NAME cusName from TB_WH_PULL wp
                left join TB_ORDER o on wp.ORDER_ID = o.ORDER_ID
                left join TB_USER u on wp.USER_ID = u.USER_ID
                left join TB_USER e on wp.TB__USER_ID = e.USER_ID
                left join TB_WAREHOUSE w on wp.WH_ID = w.WH_ID
                left join TB_RETURN_GOODS r on wp.RG_ID = r.RG_ID
                left join TB_CUSTOMER c on o.CUS_ID = c.CUS_ID
where wp.LOGIC_DELETE = 0;


drop view detail_warehouse_view;
create view detail_warehouse_view as
select wp.*,u.USER_NAME , w.WH_NAME ,
       c.CUS_NAME ,
pf.PF_UNIT,pf.PF_NAME,p.PRODUCT_NAME,p.PRODUCT_MODEL from TB_WP_DETAIL wp
                                                left join TB_USER u on wp.USER_ID = u.USER_ID
                                                left join TB_WAREHOUSE w on wp.WH_ID = w.WH_ID
                                                left join TB_CUSTOMER c on wp.CUS_ID = c.CUS_ID
  left join TB_PRODUCT_FORMAT pf on wp.PF_ID = pf.PF_ID
  left join tb_product p on p.PRODUCT_ID = pf.PRODUCT_ID
where wp.WD_STATE != -1;

ALTER TABLE TB_WP_DETAIL ADD in_number number(8) NULL;
ALTER TABLE TB_WP_DETAIL RENAME COLUMN WD_NUMBER TO out_NUMBER;
ALTER TABLE TB_WP_DETAIL MODIFY out_NUMBER NUMBER(8) DEFAULT NULL;

drop view in_storage_view;
create view in_storage_view as
  select wp.*,u.USER_NAME userName,e.USER_NAME executName,
         o.PUR_THEME purchaseName, w.WH_NAME wareHouseName, r.RG_THEME rgName,
         c.CUS_ID cusId,c.CUS_NAME cusName from TB_WH_PUT wp
                                                  left join TB_PURCHASE o on wp.PUR_ID = o.PUR_ID
                                                  left join TB_USER u on wp.USER_ID = u.USER_ID
                                                  left join TB_USER e on wp.TB__USER_ID = e.USER_ID
                                                  left join TB_WAREHOUSE w on wp.WH_ID = w.WH_ID
                                                  left join TB_RETURN_GOODS r on wp.RG_ID = r.RG_ID
                                                  left join TB_CUSTOMER c on o.CUS_ID = c.CUS_ID
  where wp.LOGIC_DELETE = 0;


--8.17
ALTER TABLE TB_WP_DETAIL ADD plan_number number(8) NULL;

--8.20
ALTER TABLE TB_DELIVER_GOODS ADD dg_number varchar2(20) NULL;

drop view send_goods_view;
create view send_goods_view as
select d.*,a.ADDRESS_NAME,a.ADDRESS_TEL,a.ADDRESS_PROVINCE,a.ADDRESS_CITY,a.ADDRESS_CONTENT,a.ADDRESS_CODE,a.Address_COUNTY,u.USER_NAME userName,o.ORDER_ID orderId,o.ORDER_TITLE orderTitle,
c.CUS_ID cusId, c.CUS_NAME cusName from TB_DELIVER_GOODS d
left join TB_ADDRESS a on a.ADDRESS_ID = d.ADDRESS_ID
left join TB_USER u on u.USER_ID = d.USER_ID
left join TB_WH_PULL p on p.WPULL_ID = d.WPULL_ID
left join tb_order o on o.ORDER_ID = p.ORDER_ID
left join TB_CUSTOMER c on c.CUS_ID = o.CUS_ID
where d.LOGIC_DELETE = 0

--8.21
ALTER TABLE TB_DELIVER_DETAIL ADD dd_price number(12,2) NULL;
ALTER TABLE TB_DELIVER_DETAIL MODIFY DD_TOTAL NUMBER(12,2) DEFAULT NULL;
ALTER TABLE TB_DELIVER_GOODS ADD dg_status int NULL;
ALTER TABLE TB_ADDRESS ADD address_county varchar2(30) NULL;
ALTER TABLE TB_DELIVER_GOODS MODIFY DG_MODEL VARCHAR2(30) DEFAULT NULL;
ALTER TABLE TB_DELIVER_DETAIL ADD dd_status number(1) NULL;
ALTER TABLE TB_WP_DETAIL ADD wd_price number(12,2) NULL;
ALTER TABLE TB_WP_DETAIL ADD wd_total number(12,2) NULL;


--8.23
ALTER TABLE TB_CUSTOMER MODIFY CUS_LIFECYCLE VARCHAR2(30) DEFAULT NULL;

ALTER TABLE TB_ORDER DROP CONSTRAINT FK_TB_ORDER_RELATIONS_TB_QUOTE;
ALTER TABLE TB_ORDER
ADD CONSTRAINT FK_TB_ORDER_RELATIONS_TB_QUOTE
FOREIGN KEY (QUOTE_ID) REFERENCES TB_SALE_OPP (OPP_ID);

--8.24
ALTER TABLE TB_ORDER_DETAIL ADD od_date date DEFAULT sysdate NULL;



create table TB_APPROVAL_LOGS
(
  LOGS_ID      NUMBER(10),
  LOGS_TIME    DATE,
  USER_ID      NUMBER(10)
    constraint TB__USER_ID_FK
    references TB_USER,
  APPROVAL_ID  NUMBER(10)
    constraint TB_S_APPROVAL_ID_FK
    references TB_APPROVAL,
  LOGS_CONTENT varchar2(100),
  logs_category varchar2(20)
)

ALTER TABLE TB_RETURN_GOODS ADD warehouse_id number(10) NULL;
ALTER TABLE TB_RETURN_GOODS
ADD CONSTRAINT TB__WH_ID_fk
FOREIGN KEY (warehouse_id) REFERENCES TB_WAREHOUSE (WH_ID);

ALTER TABLE TB_ORDER MODIFY ORDER_TOTAL DEFAULT 0;
