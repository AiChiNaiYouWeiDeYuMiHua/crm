/*权限相关
1、先顺序清空role_fc、tb_fc_type、tb_function运行这个文件的所有SQL
2、创建一个名为“BOSS”的部门
3、创建一个名为“黄大佬”的用户，指定为BOSS，默认密码为aaa123
4、创建一个名为“boss”的角色，授予所有权限，并分配用户
5、标签使用：先导入<%@ taglib prefix="g" uri="http://www.trkj.com/crm" %>，
            再<g:g id="23"><div></div></g:g>,加了自定义标签后再访问页面一定要先登录
6、需要记录操作日志的在controller方法上加上@SystemLog(module = "系统设置", methods = "新增用户")
 */
--添加功能分类
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(1,'系统权限');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(2,'客户管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(3,'客户关怀');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(4,'客户投诉');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(5,'个人设置');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(6,'项目管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(7,'联系人管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(8,'纪念日管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(9,'销售机会管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(10,'竞争对手管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(11,'报价管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(12,'订单管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(13,'合同管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(14,'库存管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(15,'产品管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(16,'回款管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(17,'退货管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(18,'产品分类管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(19,'计划回款');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(20,'付款管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(21,'任务行动');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(22,'公告管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(23,'出库管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(24,'入库管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(25,'发货管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(26,'客户服务');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(27,'开票记录');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(28,'系统日志');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(29,'数据日志');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(30,'员工日志');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(31,'消息管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(32,'采购管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(33,'系统参数管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(34,'维修管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(35,'需求管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(36,'解决方案管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(37,'计划付款管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(38,'退货明细管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(39,'发票管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(40,'采购明细管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(41,'产品分类管理');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(42,'菜单按钮');
INSERT INTO TB_FC_TYPE(FC_TYPE_ID,FC_TYPE_NAME) VALUES(43,'审批');

--添加功能权限
--系统权限
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(1,'登录',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(2,'注册',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(3,'修改密码',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(4,'重置密码',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(5,'刷新用户',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(6,'注销用户',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(7,'用户列表',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(8,'修改用户资料',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(9,'查找用户',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(10,'用户分配角色',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(11,'用户管理页面',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(12,'创建角色',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(13,'删除角色',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(14,'修改角色',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(15,'角色管理页面',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(16,'角色列表',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(17,'给角色授权',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(18,'给角色分配用户',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(19,'新建部门',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(20,'修改部门',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(21,'删除部门',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(22,'部门列表',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(23,'部门管理页面',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(24,'给部门指定员工',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(25,'查找部门',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(26,'系统操作日志页面',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(27,'日志列表',1);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(28,'日志查找',1);
--个人设置
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(29,'修改密码',5);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(30,'修改个人信息',5);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(31,'头像管理',5);
--审批
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(32,'审批列表',43);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(33,'审批操作',43);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(34,'审批查询',43);

--客户管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(41,'新建客户',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(42,'删除客户',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(43,'查看客户详情',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(44,'查找客户',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(45,'客户列表',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(46,'修改客户',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(47,'全部客户页面',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(48,'全部客户标签',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(49,'全部客户列表',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(50,'潜在客户标签',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(51,'潜在客户列表',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(52,'签约客户标签',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(53,'签约客户列表',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(54,'重复购买标签',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(55,'重复购买列表',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(56,'失效客户标签',2);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(57,'失效客户列表',2);

--客户关怀
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(58,'新建客户关怀',3);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(59,'修改客户关怀',3);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(60,'察看客户关怀详情',3);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(61,'查找客户关怀',3);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(62,'客户关怀列表',3);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(63,'删除客户关怀',3);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(64,'全部客户关怀页面',3);

--客户投诉
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(65,'新建投诉',4);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(66,'修改投诉',4);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(67,'察看投诉详情',4);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(68,'查找投诉',4);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(69,'投诉列表',4);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(70,'删除投诉',4);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(71,'全部投诉页面',4);

--项目管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(72,'新建项目',6);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(73,'修改项目',6);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(74,'察看项目详情',6);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(75,'查找项目',6);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(76,'项目列表',6);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(77,'删除项目',6);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(78,'全部项目页面',6);

--联系人
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(85,'新建联系人',7);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(86,'修改联系人',7);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(87,'察看联系人详情',7);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(88,'查找联系人',7);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(89,'联系人列表',7);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(90,'删除联系人',7);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(91,'全部联系人页面',7);

--纪念日
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(95,'新建纪念日',8);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(96,'修改纪念日',8);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(97,'察看纪念日详情',8);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(98,'查找纪念日',8);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(99,'纪念日列表',8);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(100,'删除纪念日',8);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(101,'全部纪念日页面',8);

--客户服务
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(110,'新建客户服务',26);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(111,'修改客户服务',26);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(112,'察看客户服务详情',26);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(113,'查找客户服务',26);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(114,'客户服务列表',26);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(115,'删除客户服务',26);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(116,'全部客户服务页面',26);


--添加功能权限
--回款管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(131,'新增回款',16);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(132,'删除回款',16);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(133,'修改回款',16);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(134,'查看回款详情',16);
--退货管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(141,'新增退货',17);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(142,'删除退货',17);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(143,'修改退货',17);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(144,'查看退货详情',17);
--计划回款管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(151,'新增计划回款',19);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(152,'删除计划回款',19);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(153,'修改计划回款',19);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(154,'查看计划回款详情',19);
--付款管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(161,'新增付款',20);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(162,'删除付款',20);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(163,'修改付款',20);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(164,'查看付款详情',20);
--开票管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(171,'新增开票',27);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(172,'删除开票',27);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(173,'修改开票',27);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(174,'查看开票详情',27);
--采购管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(181,'新增采购',32);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(182,'删除采购',32);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(183,'修改采购',32);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(184,'查看采购详情',32);
--维修管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(191,'新增维修',34);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(192,'删除维修',34);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(193,'修改维修',34);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(194,'查看维修详情',34);
--计划付款管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(201,'新增计划付款',37);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(202,'删除计划付款',37);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(203,'修改计划付款',37);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(204,'查看计划付款详情',37);
--退货明细管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(211,'新增退货明细',38);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(212,'删除退货明细',38);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(213,'修改退货明细',38);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(214,'查看退货明细详情',38);
--发票管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(221,'新增发票',39);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(222,'删除发票',39);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(223,'修改发票',39);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(224,'查看发票详情',39);
--采购明细管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(231,'新增采购明细',40);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(232,'删除采购明细',40);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(233,'修改采购明细',40);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(234,'查看采购明细详情',40);


--添加功能权限
--订单管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(241,'新增订单',12);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(242,'删除订单',12);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(243,'修改订单',12);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(244,'查看订单详情',12);
--合同管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(251,'新增合同',13);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(252,'删除合同',13);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(253,'修改合同',13);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(254,'查看合同详情',13);
--库存管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(261,'新增库存',14);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(262,'删除库存',14);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(263,'修改库存',14);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(264,'查看库存详情',14);
--产品管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(271,'新增产品',15);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(272,'删除产品',15);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(273,'修改产品',15);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(274,'查看产品详情',15);
--出库管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(281,'新增出库',23);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(282,'删除出库',23);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(283,'修改出库',23);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(284,'查看出库详情',23);
--入库管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(291,'新增入库',24);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(292,'删除入库',24);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(293,'修改入库',24);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(294,'查看入库详情',24);
--发货管理
  INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(301,'新增发货',25);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(302,'删除发货',25);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(303,'修改发货',25);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(304,'查看发货详情',25);
--产品分类管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(311,'新增产品分类',18);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(312,'删除产品分类',18);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(313,'修改产品分类',18);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(314,'查看产品分类详情',18);

--销售机会管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(321,'新建销售机会',9);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(322,'修改销售机会',9);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(323,'删除销售机会',9);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(324,'查看销售机会视图',9);

--详细需求管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(331,'新建详细需求',35);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(332,'修改详细需求',35);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(333,'删除详细需求',35);

--解决方案管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(341,'新建解决方案',36);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(342,'修改解决方案',36);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(343,'删除解决方案',36);

--竞争对手管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(351,'新建竞争对手',10);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(352,'修改竞争对手',10);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(353,'删除竞争对手',10);

--报价管理
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(361,'新建报价',11);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(362,'修改报价',11);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(363,'删除报价',11);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(364,'查看报价视图',11);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(365,'编辑报价明细',11);


--菜单按钮
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(371,'客户',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(372,'全部客户',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(373,'联系人',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(374,'纪念日',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(375,'服务',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(376,'客户服务',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(377,'客户关怀',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(378,'客户投诉',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(379,'维修工单',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(380,'营销',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(381,'售前',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(382,'销售机会',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(383,'需求',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(384,'竞争对手',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(385,'报价单',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(386,'报价明细',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(387,'售中',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(389,'合同订单',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(390,'交付计划',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(391,'交付记录',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(392,'发货',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(393,'订单退货',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(394,'审批',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(395,'产品',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(396,'产品管理',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(397,'产品类别',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(398,'仓储',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(399,'库存',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(400,'出库单',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(401,'入库单',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(402,'库存流水',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(403,'采购',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(404,'采购单',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(405,'采购明细',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(406,'采购退货',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(407,'财务',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(408,'回款',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(409,'回款记录',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(410,'回款计划',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(411,'付款',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(412,'付款记录',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(413,'付款计划',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(414,'票据',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(415,'开票',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(416,'发票',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(417,'设置',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(418,'个人设置',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(419,'个人信息',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(420,'修改密码',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(421,'系统设置',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(422,'部门管理',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(423,'用户管理',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(424,'角色管理',42);
INSERT INTO TB_FUNCTIONS(FC_ID,FC_NAME,FC_TYPE_ID) VALUES(425,'操作日志',42);