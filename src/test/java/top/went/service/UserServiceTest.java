package top.went.service;

import com.alibaba.fastjson.JSON;
import javafx.application.Application;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.exception.ServiceException;
import top.went.pojo.UserEntity;
import top.went.utils.Md5;
import top.went.utils.StringToDate;
import top.went.vo.PageEntity;
import top.went.vo.UserPassword;

import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * UserService Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>���� 3, 2018</pre>
 */
public class UserServiceTest {
    private UserService userService;

    @Before
    public void before() throws Exception {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext-db.xml");
        userService = context.getBean(UserService.class);
    }

    @After
    public void after() throws Exception {
        userService = null;
    }

    /**
     * Method: addUser(UserEntity userEntity)
     */
    @Test
    public void testAddUser() throws Exception {
        UserEntity userEntity = new UserEntity();
        userEntity.setHiredate((java.sql.Date) new Date());
        userEntity.setUserEmail("100003@qq.com");
        userEntity.setUserTel("18773381003");
        userEntity.setUserSex("女");
        userEntity.setUserName("零零叁");
        userEntity.setOpenid(null);
        userEntity.setUserPassword(Md5.encode("aaaaaa"));
        userEntity.setUserBirthday(StringToDate.convert("1997-02-06"));
        userEntity.setUserAddress("株洲天元区");
        userEntity.setUserIsDimission(1L);

        boolean isok = userService.addUser(userEntity);
        Assert.assertTrue(isok);
        //TODO: Test goes here...
    }

    /**
     * Method: modifyUser(UserEntity userEntity)
     */
    @Test
    public void testModifyUser() throws Exception {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(10L);
        userEntity.setHiredate( (java.sql.Date) new Date());
        userEntity.setUserEmail("100001@qq.com");
        userEntity.setUserTel("18773381001");
        userEntity.setUserSex("男");
        userEntity.setUserName("零零幺");
        userEntity.setOpenid(null);
        userEntity.setUserPassword(Md5.encode("bbbbbb"));
        userEntity.setUserBirthday(StringToDate.convert("1997-01-01"));
        userEntity.setUserAddress("株洲天元区");

        boolean isok = userService.modifyUser(userEntity);
        Assert.assertTrue(isok);
        //TODO: Test goes here...
    }

    /**
     * Method: dimissionUser(UserEntity userEntity)
     */
    @Test
    public void testDimissionUser() throws Exception {
//        UserEntity userEntity = new UserEntity();
//        userEntity.setUserId(2L);
//
//        boolean isok = userService.dimissionUser(userEntity);
//        Assert.assertTrue(isok);
        //TODO: Test goes here...
    }

    /**
     * Method: deleteUser(UserEntity userEntity)
     */
    @Test
    public void testDeleteUser() throws Exception {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(11L);

        boolean isok = userService.deleteUser(userEntity);
        Assert.assertTrue(isok);
        //TODO: Test goes here...
    }

    /**
     * Method: loginByPassword(UserEntity userEntity)
     */
    @Test
    public void testLoginByPassword() throws Exception {
//        UserEntity userEntity = new UserEntity();
//        userEntity.setUserName("零零幺");
//        userEntity.setUserPassword("bbbbbb");
//
//        boolean isok = userService.loginByPassword(userEntity);
//        Assert.assertTrue(isok);
//        //TODO: Test goes here...
    }

    /**
     * Method: modifyUserPassword(UserPassword userPassword)
     */
    @Test
    public void testModifyUserPassword() throws Exception {
        UserPassword userPassword = new UserPassword();
        userPassword.setUserId(10L);
        userPassword.setUserPassOld("bbbbbb");
        userPassword.setUserPassNew1("aaaaaa");
        userPassword.setUserPassNew2("aaaaaa");
        boolean isok = userService.modifyUserPassword(userPassword);
        Assert.assertTrue(isok);
        //TODO: Test goes here...
    }

    /**
     * Method: resetUserPassword(UserEntity userEntity)
     */
    @Test
    public void testResetUserPassword() throws Exception {
        System.out.println(JSON.toJSON(userService.queryAllUser(1, 1)));;
    }

    /**
     * Method: queryAllUser()
     */
    @Test
    public void testQueryAllUser() {
//        List<UserEntity> userEntities = userService.queryAllUser();
//        if (!userEntities.isEmpty()) {
//            System.out.println(userEntities);
//            boolean isok = true;
//            Assert.assertTrue(isok);
//        }
        //TODO: Test goes here...
    }

    /**
     * Method: findUserLikeName(String userName)
     */
    @Test
    public void testFindUserLikeName() throws ServiceException {

        Integer pageSize = 10;
        Integer pageNumber = 1;
        PageEntity<UserEntity> userEntity = userService.findUserLikeName("幺",pageNumber,pageSize);
        if (userEntity != null) {
            System.out.println(userEntity);
            boolean isok = true;
            Assert.assertTrue(isok);
        }


    }
}
