package top.went.service;

import com.github.pagehelper.PageInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.exception.ServiceException;
import top.went.pojo.CustomerEntity;
import top.went.vo.Code;
import top.went.vo.PageEntity;

import java.rmi.ServerException;
import java.sql.Date;
import java.util.List;
import java.util.Map;

public class CustomerServiceTest {
    private CustomerService customerService;
    private CustomerStatisticService customerStatisticService;
    private CustomerExcelInputService customerExcelInputService;
    private CustomerDetailService customerDetailService;

    @Before
    public void before() throws Exception {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext-db.xml");
        customerService = context.getBean(CustomerService.class);
        customerStatisticService = context.getBean(CustomerStatisticService.class);
        customerDetailService = context.getBean(CustomerDetailService.class);
    }

    @After
    public void after() throws Exception {
        customerService = null;
        customerStatisticService = null;
        customerDetailService=null;
    }

    /**
     * 增加客户
     *
     * @throws ServiceException
     */
    @Test
    public void addCustomer() throws ServiceException {
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setCusName("北齐网");
        customerEntity.setCusAbbreviation("北齐");
        customerEntity.setCusType("长期合作");
        customerEntity.setCusNet("www.beiqi.com");
        customerEntity.setCusIsDelete(1L);
        Code code = customerService.addCustomer(customerEntity);
    }


    /**
     * 删除客户
     *
     * @throws ServiceException
     */
    @Test
    public void deleteCus() throws ServiceException {
        //todo:have error
        Code code = customerService.deleteCus(1);
    }

    /**
     * @throws ServiceException
     */
    @Test
    public void getCusList() throws ServiceException {
        PageEntity pageEntity = customerService.getCusList(10, 1);
    }

    @Test
    public void getOneCus() throws ServiceException {
        CustomerEntity customerEntity = customerService.getOneCus(1);
        System.out.println(customerEntity);
    }

//    @Test
//    public void updateCus() throws ServiceException{
//        //todo:update
//        CustomerEntity customerEntity = new CustomerEntity();
//        customerEntity.setCusAdvanceMoney(1234L);
//        customerEntity.setCusAddr("湖南省株洲市");
//        customerService.updateCus(2,customerEntity);
//    }


    @Test
    public void fuzzy() throws ServiceException {
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setCusName("齐");
        customerEntity.setCusType("合");
        PageInfo pageInfo = customerService.fuzzySearch(10, 1, customerEntity);
    }

    @Test
    public void fgfg() throws ServiceException {
        List map = customerStatisticService.findAllForStatistic("客户类型分布统计");
        for(Object m:map){
            System.out.println(m);
        }
    }

    @Test
    public void getKid() throws ServiceException {
        String[] fileName = customerService.getFiles(108);
        for(int i = 0;i<fileName.length;i++){
            System.out.println(fileName[i]);
        }
    }


    @Test
    public void asdasd() throws ServiceException {
        Map<Date,List<Object>> map = customerDetailService.showAll(454);

    }

}
