package top.went.service; 

import org.junit.Test; 
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/** 
* WareHouseOutIn Tester. 
* 
* @author <Authors name> 
* @since <pre>八月 22, 2018</pre> 
* @version 1.0 
*/ 
public class WareHouseOutInTest {
    private WareHouseOutIn service;

@Before
public void before() throws Exception {
    ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-db.xml");
    service = ac.getBean(WareHouseOutIn.class);
} 

@After
public void after() throws Exception {
    service = null;
} 

/** 
* 
* Method: loadOutDetail(Long id) 
* 
*/ 
@Test
public void testLoadOutDetail() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: loadInDetail(Long id) 
* 
*/ 
@Test
public void testLoadInDetail() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: modifyDetail(WarehouseDetailVo warehouseDetailVo) 
* 
*/ 
@Test
public void testModifyDetail() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: enter(WarehouseDetailVo vos) 
* 
*/ 
@Test
public void testEnter() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: cancel(Long id, int type) 
* 
*/ 
@Test
public void testCancel() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: loadAllDetail(int i, Integer pullId) 
* 
*/ 
@Test
public void testLoadAllDetail() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: orderOut(Long id, Long warehouseId) 
* 
*/ 
@Test
public void testOrderOut() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: chooseWarehouse(Long id) 
* 
*/ 
@Test
public void testChooseWarehouse() throws Exception { 
//TODO: Test goes here...
    System.out.println(service.chooseWarehouse(118L));
} 

/** 
* 
* Method: haveNoPull(Long id) 
* 
*/ 
@Test
public void testHaveNoPull() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: canOut(Long id) 
* 
*/ 
@Test
public void testCanOut() throws Exception { 
//TODO: Test goes here... 
} 


/** 
* 
* Method: buildDetails(WarehouseDetailVo warehouseDetailVo, int id, Long warehouseId) 
* 
*/ 
@Test
public void testBuildDetails() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = WareHouseOutIn.getClass().getMethod("buildDetails", WarehouseDetailVo.class, int.class, Long.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: cancelOut(Long id) 
* 
*/ 
@Test
public void testCancelOut() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = WareHouseOutIn.getClass().getMethod("cancelOut", Long.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: cancelIn(Long id) 
* 
*/ 
@Test
public void testCancelIn() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = WareHouseOutIn.getClass().getMethod("cancelIn", Long.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: enterOut(WarehouseDetailVo vos) 
* 
*/ 
@Test
public void testEnterOut() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = WareHouseOutIn.getClass().getMethod("enterOut", WarehouseDetailVo.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: enterIn(WarehouseDetailVo vos) 
* 
*/ 
@Test
public void testEnterIn() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = WareHouseOutIn.getClass().getMethod("enterIn", WarehouseDetailVo.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: buildWpDetail(OrderEntity orderEntity, WarehouseEntity warehouseEntity, List<OrderDetail> orderDetails, List<WpDetailEntity> wpDetailEntities, WhPullEntity w) 
* 
*/ 
@Test
public void testBuildWpDetail() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = WareHouseOutIn.getClass().getMethod("buildWpDetail", OrderEntity.class, WarehouseEntity.class, List<OrderDetail>.class, List<WpDetailEntity>.class, WhPullEntity.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

} 
