package top.went.service; 

import com.alibaba.fastjson.JSON;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.pojo.ProductCategoryEntity;

/** 
* ProductCategoryService Tester. 
* 
* @author <Authors name> 
* @since <pre>八月 2, 2018</pre> 
* @version 1.0 
*/ 
public class ProductCategoryServiceTest {

    private ProductCategoryService service;

@Before
public void before() throws Exception {
    ApplicationContext context =
            new ClassPathXmlApplicationContext("applicationContext-db.xml");
    service = context.getBean(ProductCategoryService.class);
} 

@After
public void after() throws Exception {
    service = null;
} 

/** 
* 
* Method: addCategory(ProductCategoryEntity categoryEntity) 
* 
*/ 
@Test
public void testAddCategory() throws Exception {
    ProductCategoryEntity productCategoryEntity = new ProductCategoryEntity();
    productCategoryEntity.setLogicDelete(false);
    productCategoryEntity.setPcName("电视机");
    productCategoryEntity.setTbProductCategoryByTbPcId(service.load(13L));
    boolean isOk = service.addCategory(productCategoryEntity);
    Assert.assertTrue(isOk);
//TODO: Test goes here... 
} 

/** 
* 
* Method: modifyCategory(ProductCategoryEntity categoryEntity) 
* 
*/ 
@Test
public void testModifyCategory() throws Exception {

//TODO: Test goes here... 
} 

/** 
* 
* Method: deleteCategory(Long id) 
* 
*/ 
@Test
public void testDeleteCategory() throws Exception {
    System.out.println(service.deleteCategory(8L));
//TODO: Test goes here... 
} 

/** 
* 
* Method: load(Long id) 
* 
*/ 
@Test
public void testLoad() throws Exception { 
//TODO: Test goes here...
    System.out.println(service.load(8L));
} 

/** 
* 
* Method: loadList() 
* 
*/ 
@Test
public void testLoadList() throws Exception {
    System.out.println(service.loadListAndProduct());
//TODO: Test goes here... 
} 


/** 
* 
* Method: makeCategoryTree(ProductCategoryEntity categoryEntity, List<ProductCategoryEntity> categoryEntities) 
* 
*/ 
@Test
public void testMakeCategoryTree() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = ProductCategoryService.getClass().getMethod("makeCategoryTree", ProductCategoryEntity.class, List<ProductCategoryEntity>.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: checkParentCategory(ProductCategoryEntity categoryEntity) 
* 
*/ 
@Test
public void testCheckParentCategory() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = ProductCategoryService.getClass().getMethod("checkParentCategory", ProductCategoryEntity.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

} 
