package top.went.service;

import com.alibaba.fastjson.JSON;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.pojo.CustomerEntity;
import top.went.pojo.DemandEntity;
import top.went.pojo.SaleOppEntity;
import top.went.vo.DemandVo;

import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
* DemandService Tester.
*
* @author <Authors name>
* @version 1.0
*/
public class DemandServiceTest {

    private DemandService demandService;

@Before
public void before() throws Exception {
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-db.xml") ;
    demandService = context.getBean(DemandService.class);
}

@After
public void after() throws Exception {
    demandService = null;
}

/**
*
* Method: addDemand(DemandEntity demandEntity)
*
*/
@Test
public void testAddDemand() throws Exception {
    DemandEntity demandEntity = new DemandEntity();
    demandEntity.setDemandTheme("我想睡觉2");
    demandEntity.setRecordTime(new java.sql.Date(new Date().getTime()));
    demandEntity.setImportance(0L);
    demandEntity.setDemandContent("门窗关闭，熄灯2！");
    demandEntity.setIsDelete(0L);
    demandEntity.setTbSaleOppByOppId(null);
    boolean actual = demandService.addDemand(demandEntity);
    Assert.assertTrue(actual);
}

/**
*
* Method: modifyDemand(DemandEntity demandEntity)
*
*/
@Test
public void testModifyDemand() throws Exception {

}

/**
*
* Method: deleteDemand(Long id)
*
*/
@Test
public void testDeleteDemand() throws Exception {
//TODO: Test goes here...
}

/**
*
* Method: load(Long id)
*
*/
@Test
public void testLoad() throws Exception {
//TODO: Test goes here...
}

/**
*
* Method: loadList()
*
*/
@Test
public void testLoadList() throws Exception {
//TODO: Test goes here... 
} 

@Test
public void testFindAllSeniorSearch() throws Exception{
    DemandVo demandVo = new DemandVo();
    DemandEntity demandEntity = new DemandEntity();
    demandEntity.setDemandTheme("%帅%");
    demandEntity.setDemandContent("%%");
//    CustomerEntity customerEntity = new CustomerEntity();
//    SaleOppEntity saleOppEntity = new SaleOppEntity();
//    saleOppEntity.setTbCustomerByCusId(customerEntity);
//    demandEntity.setTbSaleOppByOppId(saleOppEntity);
    /*List<Long> list = new ArrayList<>();
    list.add(0L);
    demandVo.setList(list);*/
    demandVo.setDemandEntity(demandEntity);
    System.out.println(JSON.toJSON(demandService.findAllSeniorSearch(demandVo,1,3)));
}


} 
