package top.went.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.exception.ServiceException;
import top.went.pojo.ContactsEntity;
import top.went.pojo.MemorialDayEntity;
import top.went.vo.MemorialVo;

import java.util.List;

public class MemorialTest {
    private MemorialService service;
    @Before
    public void before() throws Exception {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext-db.xml");
        service = context.getBean(MemorialService.class);
    }

    @After
    public void after() throws Exception {
        service = null;
    }


    @Test
    public void addCots() throws ServiceException {
        MemorialDayEntity memorialDayEntity = new MemorialDayEntity();
        memorialDayEntity.setMedRemark("asdasd");
        service.addCots(memorialDayEntity);
    }

    @Test
    public void getOne() throws ServiceException {
        MemorialDayEntity memorialDayEntity = service.getOneCus(1);
        String cotsName = memorialDayEntity.getTbCustomerByCusId().getCusName();
        List<ContactsEntity> list = (List) memorialDayEntity.getTbCustomerByCusId().getTbContactsByCusId();
        for(ContactsEntity contactsEntity:list){
            System.out.println(contactsEntity.getCotsName());
        }
    }

    @Test
    public void ggggg() throws ServiceException {
        List<MemorialVo> memorialVos = service.getCotsByCusId(121);
        for(MemorialVo s:memorialVos){
            System.out.println(s.getTbCustomerByCusId()+"      "+s.getTbContactsByCotsId());
        }
    }

}
