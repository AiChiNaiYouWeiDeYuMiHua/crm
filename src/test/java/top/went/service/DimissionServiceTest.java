package top.went.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.exception.ServiceException;
import top.went.pojo.DimissionEntity;
import top.went.pojo.DimissionTypeEntity;
import top.went.pojo.ProductCategoryEntity;

public class DimissionServiceTest {

    private DimissionService service;

    @Before
    public void before() throws Exception {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext-db.xml");
        service = context.getBean(DimissionService.class);
    }

    @After
    public void after() throws Exception {
        service = null;
    }

    /**
     *
     * Method: findAll()
     *
     */
    @Test
    public void testFindAll() throws Exception {
        System.out.println(service.findAll());
//TODO: Test goes here...
    }

    /**
     *
     * Method: queryByType(Long dimiTypeId)
     *
     */
    @Test
    public void testQueryByType() throws Exception {
        System.out.println(service.queryByType(2L));
//TODO: Test goes here...
    }

    /**
     *
     * Method: insertDimission(DimissionEntity dimissionEntity)
     *
     */
    @Test
    public void testInsertDimission() throws Exception {
        DimissionEntity dimissionEntity = new DimissionEntity();
        dimissionEntity.setDimissionIsDel(0L);
        dimissionEntity.setDimissionName("年报");
        DimissionTypeEntity dimissionTypeEntity = service.findTypeByTypeId(4L);
        dimissionEntity.setTbDimissionTypeByDimiTypeId(dimissionTypeEntity);

        boolean rs = service.insertDimission(dimissionEntity);
        Assert.assertTrue(rs);
//TODO: Test goes here...
    }

    /**
     *
     * Method: insertDimission(DimissionEntity dimissionEntity)
     *
     */
    @Test
    public void testFindTypeByTypeId() throws ServiceException {
        DimissionTypeEntity dimissionTypeEntity = service.findTypeByTypeId(4L);
        System.out.println(dimissionTypeEntity.getDimiTypeName());
    }

    /**
     *
     * Method: deleteDimission(Long dimissionId)
     *
     */
    @Test
    public void testDeleteDimission() throws ServiceException {
        boolean rs = service.deleteDimission(45L);
        Assert.assertTrue(rs);
    }
}
