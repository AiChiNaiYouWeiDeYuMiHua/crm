package top.went.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.pojo.DeptEntity;

public class DeptServiceTest {
    private DeptService service;

    @Before
    public void before() throws Exception {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext-db.xml");
        service = context.getBean(DeptService.class);
    }

    @After
    public void after() throws Exception {
        service = null;
    }

    /**
     * Method: findAll()
     */
    @Test
    public void testAddDept() throws Exception {
        DeptEntity deptEntity = new DeptEntity();
        deptEntity.setDeptName("销售部");
        deptEntity.setDeptIsDel(0L);
        boolean rs = service.addDept(deptEntity);
        Assert.assertTrue(rs);
//TODO: Test goes here...
    }

    @Test
    public void testFindUserOnDept() throws Exception {
        System.out.println(service.getUserNodes());
//TODO: Test goes here...
    }
}
