package top.went.service; 

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.pojo.SaleOppEntity;
import top.went.vo.PageEntity;
import top.went.vo.SaleOppVo;

import javax.sound.midi.Soundbank;
import java.util.List;

/** 
* SaleOppService Tester. 
* 
* @author <Authors name> 
* @version 1.0 
*/ 
public class SaleOppServiceTest { 

    private SaleOppService saleOppService;
@Before
public void before() throws Exception {
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-db.xml") ;
    saleOppService = context.getBean(SaleOppService.class);
}

@After
public void after() throws Exception {
    saleOppService = null;
} 

/** 
* 
* Method: addSaleOpp(SaleOppEntity saleOppEntity) 
* 
*/ 
@Test
public void testAddSaleOpp() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: modifySaleOpp(SaleOppEntity saleOppEntity) 
* 
*/ 
@Test
public void testModifySaleOpp() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: deleteSaleOpp(Long id) 
* 
*/ 
@Test
public void testDeleteSaleOpp() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: load(Long id) 
* 
*/ 
@Test
public void testLoad() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: loadList() 
* 
*/ 
@Test
public void testLoadList() throws Exception {
//TODO: Test goes here... 
} 

/** 
* 
* Method: loadListWhere(Integer pageNumber, Integer pageSize, String stage, String oppStatus, String classification, String priority, String oppTheme, Long min, Long max) 
* 
*/ 
@Test
public void testLoadListWhere() throws Exception { 
    PageEntity<SaleOppEntity> pageEntity = saleOppService.loadListWhere(1,2,"","",
            "","","",1L,9999L);
    System.out.println(pageEntity.getRows());
    System.out.println(pageEntity.getTotal());
} 

/** 
* 
* Method: findAllSeniorSearch(SaleOppVo saleOppVo, Integer pageNumber, Integer pageSize) 
* 
*/ 
@Test
public void testFindAllSeniorSearch() throws Exception {
    SaleOppEntity saleOppEntity = new SaleOppEntity();
    saleOppEntity.setOppTheme("%机会%");
    SaleOppVo saleOppVo = new SaleOppVo();
    saleOppVo.setSaleOppEntity(saleOppEntity);
    PageEntity<SaleOppEntity> pageEntity = saleOppService.findAllSeniorSearch(saleOppVo,1,2);
    System.out.println(JSON.toJSON(pageEntity).toString());
} 


} 
