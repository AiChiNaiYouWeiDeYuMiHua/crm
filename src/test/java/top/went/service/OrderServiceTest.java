package top.went.service; 

import org.junit.Test; 
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.vo.OrderSearch;

/** 
* OrderService Tester. 
* 
* @author <Authors name> 
* @since <pre>八月 10, 2018</pre> 
* @version 1.0 
*/ 
public class OrderServiceTest {
    private OrderService service;

@Before
public void before() throws Exception {
    ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-db.xml");
    service = ac.getBean(OrderService.class);
} 

@After
public void after() throws Exception {
    service = null;
} 

/** 
* 
* Method: addOrder(OrderEntity orderEntity, boolean type) 
* 
*/ 
@Test
public void testAddOrder() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: deleteOrder(Long id) 
* 
*/ 
@Test
public void testDeleteOrder() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: load(Long id) 
* 
*/ 
@Test
public void testLoad() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: modifyOrder(OrderEntity orderEntity) 
* 
*/ 
@Test
public void testModifyOrder() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: findAllBySearch() 
* 
*/ 
@Test
public void testFindAllBySearch() throws Exception { 
//TODO: Test goes here...
    service.findAll(new OrderSearch());
} 

/** 
* 
* Method: modifyOrderDetail(List<OrderDetailEntity> detailEntities) 
* 
*/ 
@Test
public void testModifyOrderDetail() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: loadDeatil(Long id) 
* 
*/ 
@Test
public void testLoadDeatil() throws Exception { 
//TODO: Test goes here... 
} 


} 
