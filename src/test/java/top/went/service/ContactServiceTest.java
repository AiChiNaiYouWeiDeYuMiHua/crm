package top.went.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.exception.ServiceException;
import top.went.pojo.ContactsEntity;
import top.went.pojo.CustomerEntity;
import top.went.vo.ContactVo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class ContactServiceTest {
    private ContactsService contactsService;
    private CustomerService customerService;
    @Before
    public void before() throws Exception {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext-db.xml");
        contactsService = context.getBean(ContactsService.class);
        customerService = context.getBean(CustomerService.class);
    }

    @After
    public void after() throws Exception {
        contactsService = null;
        customerService = null;
    }

    @Test
    public void addCots() throws ServiceException {
        ContactsEntity contactsEntity = new ContactsEntity();
        contactsEntity.setCotsName("张三");
        contactsEntity.setCotsCardtype("居民身份证");
        contactsEntity.setTbCustomerByCusId(customerService.getOneCus(1));
        contactsEntity.setCotsCreateDate(new Date());
        contactsEntity.setCotsIsDelete(1);
    }

    @Test
    public void deleteCots() throws ServiceException{
        contactsService.deleteCots(48);
    }

    @Test
    public void updateCots() throws ServiceException{
        ContactsEntity contactsEntity = new ContactsEntity();
        contactsEntity.setCotsAnnualIncome(80000L);
        contactsEntity.setCotsTechnical("经理");
        contactsEntity.setCotsBusiness("财务管理");
//        contactsService.updateCots(48,contactsEntity);
    }

    @Test
    public void getCotsList() throws ServiceException{
        Map<String,String> map = contactsService.getAddrInfo(182);
        for (String s : map.keySet()) {
            System.out.println("key:" + s);
            System.out.println("values:" + map.get(s));
        }
    }


}
