package top.went.service; 

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.pojo.PayBackDetailEntity;
import top.went.vo.PayBackDetailVO;

/** 
* PayBackDetailService Tester. 
* 
* @author <Authors name> 
* @version 1.0 
*/ 
public class PayBackDetailServiceTest { 
private PayBackDetailService payBackDetailService;
@Before
public void before() throws Exception {
    ApplicationContext ac=new ClassPathXmlApplicationContext("applicationContext-db.xml");
    payBackDetailService=ac.getBean(PayBackDetailService.class);
} 

@After
public void after() throws Exception {
    payBackDetailService = null;
} 

/** 
* 
* Method: pbd_insert(PayBackDetailEntity payBackDetailEntity) 
* 
*/ 
@Test
public void testPbd_insert() throws Exception {
    PayBackDetailEntity payBackDetailEntity=new PayBackDetailEntity();
    payBackDetailEntity.setPbdMoney((double) 111);
    payBackDetailEntity.setMagicDelete(0l);
    payBackDetailEntity.setPbdTerms(8l);
    payBackDetailService.pbd_insert(payBackDetailEntity);
} 

/** 
* 
* Method: pbd_delete(int pbd_id) 
* 
*/ 
@Test
public void testPbd_delete() throws Exception { 
boolean b=payBackDetailService.pbd_delete(162);
    System.out.println(b);
} 

/** 
* 
* Method: pbd_logicDeleteAll(Long[] ids) 
* 
*/ 
@Test
public void testPbd_logicDeleteAll() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: pbd_update(PayBackDetailEntity payBackDetailEntity) 
* 
*/ 
@Test
public void testPbd_update() throws Exception { 
PayBackDetailEntity payBackDetailEntity=new PayBackDetailEntity();
payBackDetailEntity.setPbdId(163);
payBackDetailEntity.setPbdMoney((double) 888);
    System.out.println(payBackDetailService.pbd_update(payBackDetailEntity));
} 

/** 
* 
* Method: pbd_findAll(Integer pageSize, Integer pageNumber) 
* 
*/ 
@Test
public void testPbd_findAll() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: pbd_findAllByDate(String date, Integer pageSize, Integer pageNumber) 
* 
*/ 
@Test
public void testPbd_findAllByDate() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: findAllByManyConditions(PayBackDetailVO payBackDetailVO) 
* 
*/ 
@Test
public void testFindAllByManyConditions() throws Exception {
    PayBackDetailVO payBackDetailVO=new PayBackDetailVO();
    payBackDetailVO.setPbdId(163);
    System.out.println(JSON.toJSON(payBackDetailService.findAllByManyConditions(payBackDetailVO)));
} 

/** 
* 
* Method: load(Integer pbdId) 
* 
*/ 
@Test
public void testLoad() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: load1(Integer pbdId) 
* 
*/ 
@Test
public void testLoad1() throws Exception { 
//TODO: Test goes here... 
} 


/** 
* 
* Method: getPayBackDetailVos(List<PayBackDetailEntity> payBackDetailEntities) 
* 
*/ 
@Test
public void testGetPayBackDetailVos() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = PayBackDetailService.getClass().getMethod("getPayBackDetailVos", List<PayBackDetailEntity>.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: getPayBackDetailVos1(PayBackDetailEntity payBackDetailEntity) 
* 
*/ 
@Test
public void testGetPayBackDetailVos1() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = PayBackDetailService.getClass().getMethod("getPayBackDetailVos1", PayBackDetailEntity.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

} 
