package top.went.service;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PurchaseReceiptEntity;
import top.went.vo.PageEntity;
import top.went.vo.PaymentRecordVO;
import top.went.vo.PurchaseReceiptVO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
* PurchaseReceiptService Tester.
*
* @author <Authors name>
* @version 1.0
*/
public class PurchaseReceiptServiceTest {
       private PurchaseReceiptService purchaseReceiptService;
@Before
public void before() throws Exception {
    ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-db.xml");
    purchaseReceiptService = ac.getBean(PurchaseReceiptService.class);
}

@After
public void after() throws Exception {
    purchaseReceiptService = null;
}

/**
*
* Method: purr_insert(PurchaseReceiptEntity purchaseReceiptEntity)
*
*/
@Test
public void testPurr_insert() throws Exception {
    PurchaseReceiptEntity purchaseReceiptEntity=new PurchaseReceiptEntity();
    purchaseReceiptEntity.setMagicDelete(0l);
    purchaseReceiptEntity.setPurrIsPay("是");
    purchaseReceiptEntity.setPurrType("国税");
    purchaseReceiptEntity.setPurrTheme("123");
    purchaseReceiptEntity.setPurrDate(java.sql.Date.valueOf("1997-09-24"));
    purchaseReceiptService.purr_insert(purchaseReceiptEntity);
}

/**
*
* Method: purr_delete(int purr_id)
*
*/
//@Test
//public void testPurr_delete() throws Exception {
//boolean b=purchaseReceiptService.purr_delete(42);
//    System.out.println(b);
//}
//
///**
//*
//* Method: purr_update(PurchaseReceiptEntity purchaseReceiptEntity)
//*
//*/
//@Test
//public void testPurr_update() throws Exception {
//    PurchaseReceiptEntity purchaseReceiptEntity=purchaseReceiptService.findOne(42);
//    purchaseReceiptEntity.setMagicDelete(0l);
//    purchaseReceiptEntity.setPurrIsPay("是");
//    purchaseReceiptEntity.setPurrType("增值税");
//    purchaseReceiptEntity.setPurrTheme("666666");
//    purchaseReceiptEntity.setPurrDate(java.sql.Date.valueOf("2018-09-24"));
//    boolean b = purchaseReceiptService.purr_update(purchaseReceiptEntity);
//    System.out.println(b);
//}
//
///**
//*
//* Method: purr_findAll(Integer pageSize, Integer pageNumber)
//*
//*/
//@Test
//public void testPurr_findAll() throws Exception {
//    PageEntity<PurchaseReceiptEntity> page = purchaseReceiptService.purr_findAll(2, 1);
//    List<PurchaseReceiptEntity> list = page.getRows();
//    System.out.println(list.size());
//    System.out.println(list);
//}
//
///**
//*
//* Method: purr_findByPurrDate(String purr_date)
//*
//*/
//@Test
//public void testPurr_findByPurrDate() throws Exception {
//    PageEntity<PurchaseReceiptEntity> page = purchaseReceiptService.purr_findByPurrDate("1997");
//    List<PurchaseReceiptEntity> list = page.getRows();
//    System.out.println(list.size());
//    System.out.println(page.getTotal());
//}
//
///**
//*
//* Method: findOne(int purrId)
//*
//*/
//@Test
//public void testFindOne() throws Exception {
////TODO: Test goes here...
//}
//    @Test
//    public void testPr_findByManyConditions() throws Exception {
//        PurchaseReceiptVO purchaseReceiptVO=new PurchaseReceiptVO();
//        purchaseReceiptVO.setFrom(java.sql.Date.valueOf("1997-9-24"));
//        purchaseReceiptVO.setTo(java.sql.Date.valueOf("2017-9-24"));
////        purchaseReceiptVO.setTbPaymentRecordsByPrId();
//        purchaseReceiptVO.setPurrId(5);
//        List<PurchaseReceiptEntity> list=purchaseReceiptService.findAllByManyConditions(purchaseReceiptVO);
//        // System.out.println(list.get(0).getMagicDelete()+""+list.get(0).getPrMoney());
//        System.out.println(list);
//    }
@Test
public void testFindOne() throws Exception {
    System.out.println(JSON.toJSON( purchaseReceiptService.load1(153)));
}
}
