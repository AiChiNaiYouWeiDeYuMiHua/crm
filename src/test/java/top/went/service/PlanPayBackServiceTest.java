package top.went.service;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Page;
import top.went.pojo.PaymentRecordsEntity;
import top.went.pojo.PlanPayBackEntity;
import top.went.vo.PageEntity;
import top.went.vo.PaymentRecordVO;
import top.went.vo.PlanPayBackVO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/** 
* PlanPayBackService Tester. 
* 
* @author <Authors name> 
* @version 1.0 
*/ 
public class PlanPayBackServiceTest {
    private PlanPayBackService planPayBackService;
@Before
public void before() throws Exception {
    ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-db.xml");
    planPayBackService = ac.getBean(PlanPayBackService.class);
} 

@After
public void after() throws Exception {
    planPayBackService = null;
} 

/** 
* 
* Method: ppb_insert(PlanPayBackEntity planPayBackEntity) 
* 
*/ 
@Test
public void testPpb_insert() throws Exception {
    PlanPayBackEntity planPayBackEntity = new PlanPayBackEntity();
    planPayBackEntity.setPpbDate(java.sql.Date.valueOf("1997-09-24"));
    planPayBackEntity.setPpbMoney(new BigDecimal(500));
    planPayBackEntity.setPpbType(0l);//0表示订单，1表示维修工单
    planPayBackEntity.setPpbTerms(8l);
    planPayBackEntity.setMagicDelete(0l);
    boolean b = planPayBackService.ppb_insert(planPayBackEntity);
    System.out.println(b);
} 

/** 
* 
* Method: ppb_delete(int ppb_id) 
* 
*/ 
@Test
public void testPpb_delete() throws Exception { 
 boolean b = planPayBackService.ppb_delete(165);
    System.out.println(b);
} 

/** 
* 
* Method: ppb_update(PlanPayBackEntity planPayBackEntity) 
* 
*/ 
@Test
public void testPpb_update() throws Exception { 
PlanPayBackEntity planPayBackEntity=new PlanPayBackEntity();
planPayBackEntity.setPpbId(64);
planPayBackEntity.setPpbMoney(new BigDecimal(900));
boolean b=planPayBackService.ppb_update(planPayBackEntity);
    System.out.println(b);
} 

/** 
* 
* Method: ppb_findAll(Integer pageSize, Integer pageNumber) 
* 
*/ 
@Test
public void testPpb_findAll() throws Exception {
    PageEntity<PlanPayBackVO> page = planPayBackService.ppb_findAll(3, 1);
    System.out.println(JSON.toJSON(page));
//    List<PlanPayBackEntity> list = page.getRows();
//    System.out.println(list);
//    System.out.println(list.get(0).getPpbDate());
//    System.out.println(list.size());
//    System.out.println(page.getTotal());
//    System.out.println(JSON.toJSON(page));
} 

/** 
* 
* Method: ppb_findAllByDate(String date, Integer pageSize, Integer pageNumber) 
* 
*/ 
@Test
public void testPpb_findAllByDate() throws Exception {
    System.out.println(JSON.toJSON( planPayBackService.ppb_findAllByDate("1997", 3, 1)));
} 

/** 
* 
* Method: findAllByManyConditions(PlanPayBackVO planPayBackVO) 
* 
*/ 
@Test
public void testFindAllByManyConditions() throws Exception {
    PlanPayBackVO planPayBackVO=new PlanPayBackVO();
    planPayBackVO.setPpbMoney(new BigDecimal(1000));
    planPayBackVO.setPpbId(4);
    System.out.println(JSON.toJSON(planPayBackService.findAllByManyConditions(planPayBackVO)));
 //   PlanPayBackVO planPayBackVO=new PlanPayBackVO();
//        paymentRecordVO.setFrom(Date.valueOf("1998-09-20"));
//        paymentRecordVO.setTo(Date.valueOf("1999-9-24"));
//    planPayBackVO.setPpdMoney(new BigDecimal(600));

  //  List<PlanPayBackEntity> list=planPayBackService.findAllByManyConditions(planPayBackVO);
    // System.out.println(list.get(0).getMagicDelete()+""+list.get(0).getPrMoney());
   // System.out.println(list);
} 

/** 
* 
* Method: load(Integer ppbId) 
* 
*/ 
@Test
public void testLoad() throws Exception { 
PlanPayBackEntity planPayBackEntity=planPayBackService.load(165);
    System.out.println(planPayBackEntity==null);
} 


} 
