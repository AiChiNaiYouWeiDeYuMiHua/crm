package top.went.service;

import org.junit.Test; 
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.vo.ProductSearch;

/** 
* ProductService Tester. 
* 
* @author <Authors name> 
* @since <pre>八月 7, 2018</pre> 
* @version 1.0 
*/ 
public class ProductServiceTest {
    private ProductService service;

@Before
public void before() throws Exception {
    ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-db.xml");
    service = ac.getBean(ProductService.class);
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: addProduct(ProductFormatEntity productFormatEntity, boolean type) 
* 
*/ 
@Test
public void testAddProduct() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: deleteProduct(Integer id) 
* 
*/ 
@Test
public void testDeleteProduct() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: modifyProduct(ProductFormatEntity formatEntity) 
* 
*/ 
@Test
public void testModifyProduct() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: load(Integer id) 
* 
*/ 
@Test
public void testLoad() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: findAll(ProductSearch search) 
* 
*/ 
@Test
public void testFindAll() throws Exception {
    ProductSearch search = new ProductSearch();
    System.out.println(service.findAll(search));
//TODO: Test goes here... 
} 

/** 
* 
* Method: getNotDetails(Long id) 
* 
*/ 
@Test
public void testGetNotDetails() throws Exception { 
//TODO: Test goes here... 
} 


/** 
* 
* Method: loadProduct(ProductEntity productEntity) 
* 
*/ 
@Test
public void testLoadProduct() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = ProductService.getClass().getMethod("loadProduct", ProductEntity.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

} 
