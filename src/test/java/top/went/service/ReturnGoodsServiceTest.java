package top.went.service; 

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/** 
* ReturnGoodsService Tester. 
* 
* @author <Authors name> 
* @version 1.0 
*/ 
public class ReturnGoodsServiceTest {
    private ReturnGoodsService returnGoodsService;
@Before
public void before() throws Exception {
    ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-db.xml");
    returnGoodsService = ac.getBean(ReturnGoodsService.class);
} 

@After
public void after() throws Exception {
    returnGoodsService=null;
} 

/** 
* 
* Method: rg_insert(ReturnGoodsEntity returnGoodsEntity, String type) 
* 
*/ 
@Test
public void testRg_insert() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: rgDelete(int id) 
* 
*/ 
@Test
public void testRgDelete() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: load(Integer id) 
* 
*/ 
@Test
public void testLoad() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: loadBy(Integer Id) 
* 
*/ 
@Test
public void testLoadBy() throws Exception {
    System.out.println(JSON.toJSON(returnGoodsService.loadBy(1)));
} 

/** 
* 
* Method: findAll(ReturnGoodsVO returnGoodsVO) 
* 
*/ 
@Test
public void testFindAll() throws Exception { 
//TODO: Test goes here... 
} 


/** 
* 
* Method: rg_update(ReturnGoodsEntity returnGoodsEntity) 
* 
*/ 
@Test
public void testRg_update() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = ReturnGoodsService.getClass().getMethod("rg_update", ReturnGoodsEntity.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: getReturnGoodsVO1(ReturnGoodsEntity returnGoodsEntity) 
* 
*/ 
@Test
public void testGetReturnGoodsVO1() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = ReturnGoodsService.getClass().getMethod("getReturnGoodsVO1", ReturnGoodsEntity.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

} 
