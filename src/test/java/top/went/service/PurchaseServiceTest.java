package top.went.service; 

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/** 
* PurchaseService Tester. 
* 
* @author <Authors name> 
* @version 1.0 
*/ 
public class PurchaseServiceTest { 
private PurchaseService purchaseService;
@Before
public void before() throws Exception {
    ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-db.xml");
    purchaseService = ac.getBean(PurchaseService.class);
} 

@After
public void after() throws Exception {
    purchaseService=null;
} 

/** 
* 
* Method: pur_insert(PurchaseEntity purchaseEntity) 
* 
*/ 
@Test
public void testPur_insert() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: pur_logicDelete(int purId) 
* 
*/ 
@Test
public void testPur_logicDelete() throws Exception { 
boolean b = purchaseService.pur_logicDelete(224);
    System.out.println(b);
} 

/** 
* 
* Method: pur_logicDeleteAll(Long[] ids) 
* 
*/ 
@Test
public void testPur_logicDeleteAll() throws Exception { 

} 

/** 
* 
* Method: pur_findAll(Integer pageSize, Integer pageNumber) 
* 
*/ 
@Test
public void testPur_findAll() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: findAllByManyConditions(PurchaseVO purchaseVO) 
* 
*/ 
@Test
public void testFindAllByManyConditions() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: load1(Integer purId) 
* 
*/ 
@Test
public void testLoad1() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: load(Integer mtId) 
* 
*/ 
@Test
public void testLoad() throws Exception {
    System.out.println( purchaseService.load(225));
} 


/** 
* 
* Method: getPurchaseVO(List<PurchaseEntity> purchaseEntities) 
* 
*/ 
@Test
public void testGetPurchaseVO() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = PurchaseService.getClass().getMethod("getPurchaseVO", List<PurchaseEntity>.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

/** 
* 
* Method: getPurchaseVO1(PurchaseEntity purchaseEntity) 
* 
*/ 
@Test
public void testGetPurchaseVO1() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = PurchaseService.getClass().getMethod("getPurchaseVO1", PurchaseEntity.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

} 
