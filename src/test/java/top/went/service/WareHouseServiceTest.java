package top.went.service; 

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.went.vo.WareHouse;

/** 
* WareHouseService Tester. 
* 
* @author <Authors name> 
* @since <pre>八月 3, 2018</pre> 
* @version 1.0 
*/ 
public class WareHouseServiceTest {
    private WareHouseService service;

@Before
public void before() throws Exception {
    ApplicationContext context =
            new ClassPathXmlApplicationContext("applicationContext-db.xml");
    service = context.getBean(WareHouseService.class);
} 

@After
public void after() throws Exception {
    service = null;
} 

/** 
* 
* Method: addWareHouse(String name) 
* 
*/ 
@Test
public void testAddWareHouse() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: modifyWareHouseName(Long id, String name) 
* 
*/ 
@Test
public void testModifyWareHouseName() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: deleteWareHouse(Long id) 
* 
*/ 
@Test
public void testDeleteWareHouse() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: findAllSimpleSearch() 
* 
*/ 
@Test
public void testFindAllSimpleSearch() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: findAllSeniorSearch(WareHouse wareHouse) 
* 
*/ 
@Test
public void testFindAllSeniorSearch() throws Exception { 
//TODO: Test goes here...
    WareHouse wareHouse = new WareHouse();
    wareHouse.setId(1l);
    System.out.println(JSON.toJSON(service.findAllSeniorSearch(wareHouse,0,10)));
} 


/** 
* 
* Method: loadWareHouse(Long id) 
* 
*/ 
@Test
public void testLoadWareHouse() throws Exception { 
//TODO: Test goes here... 
/* 
try { 
   Method method = WareHouseService.getClass().getMethod("loadWareHouse", Long.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/ 
} 

} 
