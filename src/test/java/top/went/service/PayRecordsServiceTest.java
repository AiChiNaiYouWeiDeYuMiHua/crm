//package top.went.service;
//
//import com.alibaba.fastjson.JSON;
//import org.junit.Test;
//import org.junit.Before;
//import org.junit.After;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import top.went.pojo.PaymentRecordsEntity;
//import top.went.vo.PageEntity;
//import top.went.vo.PaymentRecordVO;
//
//import java.math.BigDecimal;
//import java.sql.Date;
//import java.sql.Time;
//import java.time.LocalTime;
//import java.util.List;
//
///**
// * PayRecordsService Tester.
// *
// * @author <Authors name>
// * @version 1.0
// */
//public class PayRecordsServiceTest {
//    private PayRecordsService payRecordsService;
//
//    @Before
//    public void before() throws Exception {
//        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-db.xml");
//        payRecordsService = ac.getBean(PayRecordsService.class);
//    }
//
//    @After
//    public void after() throws Exception {
//        payRecordsService = null;
//    }
//
//    /**
//     * Method: pr_insert(PaymentRecordsEntity paymentRecordsEntity)
//     */
//    @Test
//    public void testPr_insert() throws Exception {
//        PaymentRecordsEntity paymentRecordsEntity = new PaymentRecordsEntity();
//        paymentRecordsEntity.setPrIsTicket("是");
//        paymentRecordsEntity.setPrDate(Date.valueOf("1997-09-24"));
//        paymentRecordsEntity.setPrTerms(3l);
//        paymentRecordsEntity.setPrMoney(BigDecimal.valueOf(400.0));
//        paymentRecordsEntity.setMagicDelete(0l);
//        payRecordsService.pr_insert(paymentRecordsEntity);
//    }
//
//    /**
//     * Method: pr_delete(int pr_id)
//     */
//    @Test
//    public void testPr_delete() throws Exception {
//        boolean b = payRecordsService.pr_delete(12);
//        System.out.println(b);
//    }
//
//    /**
//     * Method: pr_update(int prId, long prMoney, Long prTerms, Time prDate, String prIsTicket, Integer userId, Integer ppdId)
//     */
//    @Test
//    public void testPr_update() throws Exception {
//        PaymentRecordsEntity paymentRecordsEntity = new PaymentRecordsEntity();
//        paymentRecordsEntity.setPrId(5);
//        paymentRecordsEntity.setPrIsTicket("不是");
//        paymentRecordsEntity.setPrDate(Date.valueOf("1997-09-24"));
//        paymentRecordsEntity.setPrTerms(3l);
////        paymentRecordsEntity.setPrMoney(BigDecimal.valueOf(800.0));
//        paymentRecordsEntity.setMagicDelete(0l);
//        boolean b=payRecordsService.pr_update(paymentRecordsEntity);
//        System.out.println(b);
//    }
//
//    /**
//     * Method: pr_findAll(Integer pageSize, Integer pageNumber)
//     */
//    @Test
//    public void testPr_findAll() throws Exception {
////        PageEntity<PaymentRecordsEntity> page = payRecordsService.pr_findAll(3, 0);
////        List<PaymentRecordsEntity> list = page.getRows();
////        System.out.println(list.get(0).getPrDate());
////        System.out.println(list.size());
////        System.out.println(page.getTotal());
//        System.out.println(JSON.toJSON(payRecordsService.pr_findAll(3, 1)));
//    }
//
//    /**
//     * Method: pr_findByPpdDate(Integer pageSize, Integer pageNumber, Time pr_date)
//     */
//    @Test
//    public void testPr_findByPpdDate() throws Exception {
//        System.out.println(JSON.toJSON(payRecordsService.pr_findByPpdDate("8", 10, 1)));
//    }
//    @Test
//    public void testAll() throws Exception {
//        PaymentRecordVO paymentRecordVO=new PaymentRecordVO();
////        paymentRecordVO.setPrMoney(new BigDecimal(100));
//        System.out.println(JSON.toJSON(payRecordsService.findAllByManyConditions(paymentRecordVO).getRows()));
//    }
//
//}
